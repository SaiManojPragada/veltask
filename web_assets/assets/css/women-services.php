<?php include 'includes/header.php'; ?>
    <!--Sliders Section-->
    <div>
      <div class="cover-image sptb-1 bg-background" data-image-src="../assets/images/banners/banner1.jpg">
        <div class="header-text1 mb-0">
          <div class="container">
            <div class="row">
              <div class="col-xl-10 col-lg-12 col-md-12 d-block ">
                <div class=" breadcrumb-banner text-left text-white ">
                  <h1 > Beauty & Salon 
                  </h1>
                </div>
                <div>
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                      <a href="#">Home
                      </a>
                    </li>
                    <li class="breadcrumb-item">
                      <a href="#">Services
                      </a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Beauty & Salon
                    </li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /header-text -->
      </div>
    </div>
    <!--/Sliders Section-->
    <!--Add listing-->
    <section class="sptb">
      <div class="container">
        <div class="row">
          <!--Left Side Content-->
          <div class="col-xl-3 col-lg-4 col-md-12">
            <div class="left-side-bar">
              <div class="card p-5">
                <div class="inner-class-card ">
                  <div class="card-header p-4">
                    <h3 class="card-title">Services
                    </h3>
                  </div>
                  <div class="card-body">
                    <div class="" id="container">
                      <div class="filter-product-checkboxs">
                        <div class="service-type-session">
                          <div class="d-flex justify-content-between">
                            <span class="service-name">
                              <a href="#">Women's
                              </a>
                            </span>
                            <span class="service-count">50
                            </span>
                          </div>
                        </div>
                        <div class="service-type-session">
                          <div class="d-flex justify-content-between">
                            <span class="service-name">
                              <a href="#">Men's
                              </a>
                            </span>
                            <span class="service-count">10
                            </span>
                          </div>
                        </div>
                        <div class="service-type-session">
                          <div class="d-flex justify-content-between">
                            <span class="service-name">
                              <a href="#">Kid's
                              </a>
                            </span>
                            <span class="service-count">05
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- <div class="card-footer">
<a href="#" class="btn btn-secondary btn-block">Apply Filter</a>
</div> -->
                </div>
              </div>
              <div class="card p-5">
                <div class="inner-class-card ">
                  <div class="card-header p-4">
                    <h3 class="card-title">Fill By Prices
                    </h3>
                  </div>
                  <div class="card-body">
                    <div class="" id="container">
                      <div class="filter-product-checkboxs">
                        <label class="custom-control custom-checkbox mb-3">
                          <input type="checkbox" class="custom-control-input" name="checkbox2" value="option2">
                          <span class="custom-control-label">
                            <a href="#" class="text-dark">Rs.499 & Below
                            </a>
                          </span>
                        </label>
                        <label class="custom-control custom-checkbox mb-3">
                          <input type="checkbox" class="custom-control-input" name="checkbox2" value="option2">
                          <span class="custom-control-label">
                            <a href="#" class="text-dark">Rs.500 - Rs.999
                            </a>
                          </span>
                        </label>
                        <label class="custom-control custom-checkbox mb-3">
                          <input type="checkbox" class="custom-control-input" name="checkbox2" value="option2">
                          <span class="custom-control-label">
                            <a href="#" class="text-dark">Rs.1000 - Rs.1499
                            </a>
                          </span>
                        </label>
                        <label class="custom-control custom-checkbox mb-3">
                          <input type="checkbox" class="custom-control-input" name="checkbox2" value="option2">
                          <span class="custom-control-label">
                            <a href="#" class="text-dark">Rs.1500 - Rs.1999
                            </a>
                          </span>
                        </label>
                        <label class="custom-control custom-checkbox mb-3">
                          <input type="checkbox" class="custom-control-input" name="checkbox2" value="option2">
                          <span class="custom-control-label">
                            <a href="#" class="text-dark">Rs.2000 - Rs.2999
                            </a>
                          </span>
                        </label>
                        <label class="custom-control custom-checkbox mb-3">
                          <input type="checkbox" class="custom-control-input" name="checkbox2" value="option2">
                          <span class="custom-control-label">
                            <a href="#" class="text-dark">Rs.3000 and Above
                            </a>
                          </span>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="card-footer">
                    <a href="#" class="btn btn-primary btn-block">Apply 
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!--/Left Side Content-->
          <div class="col-xl-9 col-lg-8 col-md-12">
            <!--Add Lists-->
            <div class="right-side-women-service">
            

              <div class="right-side-part">
              <div class=" mb-0">
                <div class="mb-3">
                  <div class="item2-gl-nav d-flex">
                    <h6 class="mb-0 mt-2">Showing 1 to 10 of 30 entries
                    </h6>
                    <ul class="nav item2-gl-menu ml-auto">
                    </ul>
                    <div class="d-flex">
                     
                      <select name="item" class="select-sm w-70 select2 select2-hidden-accessible" data-select2-id="1" tabindex="-1" aria-hidden="true">
                        <option value="1" data-select2-id="3">Default Sorting
                        </option>
                        <option value="2" >Latest
                        </option>
                        <option value="3">Oldest
                        </option>
                        <option value="4">Price:Low-to-High
                        </option>
                        <option value="5">Price:Hight-to-Low
                        </option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="">
                  <div class="item2-gl ">
                    <div class="card overflow-hidden ">
                      <div class="d-md-flex">
                        <div class="item-card9-img">
                          <div class="item-card2-img ">
                            <div class="arrow-ribbon ">
                              <i class="fa fa-star" aria-hidden="true">
                              </i>4.0
                            </div>
                            <a href="classified.html">
                            </a>
                            <img style="height: 200px;" src="assets/images/women-service-9.jpg" alt="img" class="cover-image">
                          </div>
                        </div>
                        <div class="card pl-4 pr-4 border-0 mb-0">

                          <div class="header-crd  pt-2 pb-1 ">
                 <h4 class="font-weight-bold mt-1 h4-tag"><a href="view-details.php">Full Chocolate | Chocolate Roll-on Waxing</a>
                                  </h4>

                                  <div class="service-price">
                                      <p class="mb-0"><span class="offer-price">₹800</span> <span class="mrp-price">₹900</span></p>
                                  </div><!--service-price-->
               </div>
                          <div class="card-body ">
                            <div class="item-card2">
                              <div class="item-card2-desc">

                               <ul class="filter-service-list">
                                <li>
                                   <span class="SelectedFilter">
                                  <span class="SelectedFilters-bold"><a href="view-details.php">Roll On Waxing</a>
                                  </span>
                                  <span>&nbsp;-&nbsp;</span>
                                Chocolate Roll On Full Arms (Including Underarms) + Full Legs</span>
                                </li>

                                <li>
                                  <span class="SelectedFilter">
                                  <span class="SelectedFilters-bold">Facial Hair Removal</span><span>&nbsp;-&nbsp;</span>
                                    Eyebrow Threading , Upper Lip Threading </span>
                                </li>
                               </ul>
                              </div>
                            </div>
                          </div>
                          <div class="card-footer pt-4 pb-4">
                            <div class="item-card2-footer d-sm-flex">
                              <div class="item-card2-rating">
                                <div class="rating-stars d-inline-flex">
                                  <ul class="footer-list">
                                   
                                    <li>
                                    <span> <a href="view-details.php">View Details <i class="fa fa-angle-right" aria-hidden="true"></i></a> </span>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                              <div class="ml-auto">
                               
                               <div class="F8dpS zj0R0 _3L1X9">
                              <div class="_1RPOp " id="addbtn" onclick="show();">ADD</div>
                              <div id="box1" style="display: none;"></div>
                              <div id="increament-div">
                                <div id="box" style="display: none;"></div>
                                    <div onclick="inccreament()" class="_1ds9T _2WdfZ _4aKW6 classList ">+</div>
                                    <div class="_2zAXs _2quy- _4aKW6">0</div>
                                <div onclick="deccreament()"  class="_29Y5Z _20vNm _4aKW6">-</div>
                                </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                   <div class="card overflow-hidden ">
                      <div class="d-md-flex">
                        <div class="item-card9-img">
                          <div class="item-card2-img ">
                            <div class="arrow-ribbon ">
                              <i class="fa fa-star" aria-hidden="true">
                              </i>4.0
                            </div>
                            <a href="classified.html">
                            </a>
                            <img src="assets/images/women-service-14.jpg" alt="img" class="cover-image">
                          </div>
                        </div>
                        <div class="card pl-4 pr-4 border-0 mb-0">

                          <div class="header-crd  pt-2 pb-1 ">
                 <h4 class="font-weight-bold mt-1 h4-tag"><a href="view-details.php">Glow Getter</a>
                                  </h4>

                                  <div class="service-price">
                                      <p class="mb-0"><span class="offer-price">₹600</span> <span class="mrp-price">₹750</span></p>
                                  </div><!--service-price-->
               </div>
                          <div class="card-body ">
                            <div class="item-card2">
                              <div class="item-card2-desc">

                               <ul class="filter-service-list">
                                <li>
                                   <span class="SelectedFilter">
                                  <span class="SelectedFilters-bold">Facial
                                  </span>
                                  <span>&nbsp;-&nbsp;</span>
                                Elysian Saundarya Multi Mask Facial</span>
                                </li>

                                <li>
                                  <span class="SelectedFilter">
                                  <span class="SelectedFilters-bold">Roll-On A-la-Carte</span><span>&nbsp;-&nbsp;</span>
                                    Full Arms +Underarms RICA</span>
                                </li>

                                  <li>
                                  <span class="SelectedFilter">
                                  <span class="SelectedFilters-bold">Hair Care</span><span>&nbsp;-&nbsp;</span>
                                    Head Massage (20 min)</span>
                                </li>
                               </ul>
                              </div>
                            </div>
                          </div>
                          <div class="card-footer pt-4 pb-4">
                            <div class="item-card2-footer d-sm-flex">
                              <div class="item-card2-rating">
                                <div class="rating-stars d-inline-flex">
                                  <ul class="footer-list">
                                   
                                    <li>
                                   <span> <a href="view-details.php">View Details <i class="fa fa-angle-right" aria-hidden="true"></i></a> </span>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                              <div class="ml-auto">
                                <div class="F8dpS zj0R0 _3L1X9">
                              <div class="_1RPOp " id="addbtn" onclick="show();">ADD</div>
                              <div id="box1" style="display: none;"></div>
                              <div id="increament-div">
                                <div id="box" style="display: none;"></div>
                                    <div onclick="inccreament()" class="_1ds9T _2WdfZ _4aKW6 classList ">+</div>
                                    <div class="_2zAXs _2quy- _4aKW6">0</div>
                                <div onclick="deccreament()"  class="_29Y5Z _20vNm _4aKW6">-</div>
                                </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="card overflow-hidden ">
                      <div class="d-md-flex">
                        <div class="item-card9-img">
                          <div class="item-card2-img ">
                            <div class="arrow-ribbon ">
                              <i class="fa fa-star" aria-hidden="true">
                              </i>4.0
                            </div>
                            <a href="classified.html">
                            </a>
                            <img src="assets/images/women-service-12.jpg" alt="img" class="cover-image">
                          </div>
                        </div>
                        <div class="card pl-4 pr-4 border-0 mb-0">

                          <div class="header-crd  pt-2 pb-1 ">
                 <h4 class="font-weight-bold mt-1 h4-tag"><a href="view-details.php">Elysian Pinacolada Fruit Cleanup</a>
                                  </h4>

                                  <div class="service-price">
                                      <p class="mb-0"><span class="offer-price">₹700</span> <span class="mrp-price">₹800</span></p>
                                  </div><!--service-price-->
               </div>
                          <div class="card-body ">
                            <div class="item-card2">
                              <div class="item-card2-desc">

                               <ul class="filter-service-list">
                                <li>
                                   <span class="SelectedFilter">
                                  <span class="SelectedFilters-bold">Roll On Waxing
                                  </span>
                                  <span>&nbsp;-&nbsp;</span>
                                Chocolate Roll On Full Arms (Including Underarms) + Full Legs</span>
                                </li>

                                <li>
                                  <span class="SelectedFilter">
                                  <span class="SelectedFilters-bold">Facial Hair Removal</span><span>&nbsp;-&nbsp;</span>
                                    Eyebrow Threading , Upper Lip Threading </span>
                                </li>
                               </ul>
                              </div>
                            </div>
                          </div>
                          <div class="card-footer pt-4 pb-4">
                            <div class="item-card2-footer d-sm-flex">
                              <div class="item-card2-rating">
                                <div class="rating-stars d-inline-flex">
                                  <ul class="footer-list">
                                   
                                    <li>
                                    <span> <a href="view-details.php">View Details <i class="fa fa-angle-right" aria-hidden="true"></i></a> </span>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                              <div class="ml-auto">
                              <div class="F8dpS zj0R0 _3L1X9">
                              <div class="_1RPOp " id="addbtn" onclick="show();">ADD</div>
                              <div id="box1" style="display: none;"></div>
                              <div id="increament-div">
                                <div id="box" style="display: none;"></div>
                                    <div onclick="inccreament()" class="_1ds9T _2WdfZ _4aKW6 classList ">+</div>
                                    <div class="_2zAXs _2quy- _4aKW6">0</div>
                                <div onclick="deccreament()"  class="_29Y5Z _20vNm _4aKW6">-</div>
                                </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                   <div class="card overflow-hidden ">
                      <div class="d-md-flex">
                        <div class="item-card9-img">
                          <div class="item-card2-img ">
                            <div class="arrow-ribbon ">
                              <i class="fa fa-star" aria-hidden="true">
                              </i>4.0
                            </div>
                            <a href="classified.html">
                            </a>
                            <img src="assets/images/women-service-11.jpg" alt="img" class="cover-image">
                          </div>
                        </div>
                        <div class="card pl-4 pr-4 border-0 mb-0">

                          <div class="header-crd  pt-2 pb-1 ">
                 <h4 class="font-weight-bold mt-1 h4-tag"><a href="view-details.php">Wax & Glow: RICA Regular Waxing </a>
                                  </h4>

                                  <div class="service-price">
                                      <p class="mb-0"><span class="offer-price">₹500</span> <span class="mrp-price">₹600</span></p>
                                  </div><!--service-price-->
               </div>
                          <div class="card-body ">
                            <div class="item-card2">
                              <div class="item-card2-desc">

                               <ul class="filter-service-list">
                                <li>
                                   <span class="SelectedFilter">
                                  <span class="SelectedFilters-bold"> Waxing
                                  </span>
                                  <span>&nbsp;-&nbsp;</span>
                               Full Arms + Underarms (RICA), Full Legs RICA</span>
                                </li>

                                <li>
                                  <span class="SelectedFilter">
                                  <span class="SelectedFilters-bold">Facial Hair Removal</span><span>&nbsp;-&nbsp;</span>
                                    Eyebrow Threading , Upper Lip Threading </span>
                                </li>
                               </ul>
                              </div>
                            </div>
                          </div>
                          <div class="card-footer pt-4 pb-4">
                            <div class="item-card2-footer d-sm-flex">
                              <div class="item-card2-rating">
                                <div class="rating-stars d-inline-flex">
                                  <ul class="footer-list">
                                   
                                    <li>
                                <span> <a href="view-details.php">View Details <i class="fa fa-angle-right" aria-hidden="true"></i></a> </span>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                              <div class="ml-auto">
                                <div class="F8dpS zj0R0 _3L1X9">
                              <div class="_1RPOp " id="addbtn" onclick="show();">ADD</div>
                              <div id="box1" style="display: none;"></div>
                              <div id="increament-div">
                                <div id="box" style="display: none;"></div>
                                    <div onclick="inccreament()" class="_1ds9T _2WdfZ _4aKW6 classList ">+</div>
                                    <div class="_2zAXs _2quy- _4aKW6">0</div>
                                <div onclick="deccreament()"  class="_29Y5Z _20vNm _4aKW6">-</div>
                                </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="center-block text-center">
                    <ul class="pagination mb-3">
                      <li class="page-item page-prev disabled">
                        <a class="page-link" href="#" tabindex="-1">Prev
                        </a>
                      </li>
                      <li class="page-item active">
                        <a class="page-link" href="#">1
                        </a>
                      </li>
                      <li class="page-item">
                        <a class="page-link" href="#">2
                        </a>
                      </li>
                      <li class="page-item">
                        <a class="page-link" href="#">3
                        </a>
                      </li>
                      <li class="page-item page-next">
                        <a class="page-link" href="#">Next
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            </div><!--riht-side-womenservie-->
            <!--right-side-part-->
            <!--/Add Lists-->
          </div>
        </div>
      </div>
    </section>

    

  <section>
  
      <div class="footer-sticky-bar">
        <div class="container">
    
    <div class="row">
      <div class="col-md-6 text-center">
          <div class="view-member-btn">
            <button class="btn btn-default"> <a href="#">View Membership
             </a></button>
          </div>
                        
      </div>
      <div class="col-md-6 text-right">
          <div class="Continue-btn d-flex justify-content-center">
          <div class="contine-parent">
              <span class="service-product-count">50
                            </span><span>Women's</span>
          </div>

          <div class="contine-parent">
          <span><a href="continue-page.php" target="_blank">Continue</a></span>

          </div>
          </div>
      </div>
    </div>
    </div><!--footer-sticky-bar-->
  </div>
  </section>

   
 <?php include ('includes/footer.php');?>
<script>
    function show(){
      document.getElementById("increament-div").style.display = "block";
      document.getElementById("addbtn").style.display = "none";
      document.getElementById("box").style.display = "block";
      setTimeout(function(){ document.getElementById("box").style.display = "none"; }, 500);
    }


    function inccreament()
    {
      document.getElementById("box").style.display = "block";
      setTimeout(function(){ document.getElementById("box").style.display = "none"; }, 500);
      
    }

    function deccreament() 
    {  
      
      document.getElementById("increament-div").style.display = "none";
        document.getElementById("addbtn").style.display = "block";
        document.getElementById("box1").style.display = "block";
      setTimeout(function(){ 
        document.getElementById("box1").style.display = "none"; 
        
      }, 500);
    }

</script> 