<?php

class My_model extends CI_Model {

    function get_data($table, $where = null, $order_by_column = null, $order_by = "asc", $check_status = false, $limit = null) {
        if ($where) {
            $this->db->where($where);
        }
        if ($order_by_column) {
            $this->db->order_by($order_by_column, $order_by);
        }
        if ($check_status) {
            $this->db->where("status", true);
        }
        if ($limit) {
            $this->db->limit($limit);
        }
        return $this->db->get($table)->result();
    }

    function get_data_row($table, $where = null, $check_status = false) {
        if ($where) {
            $this->db->where($where);
        }
        if ($check_status) {
            $this->db->where("status", true);
        }
        return $this->db->get($table)->row();
    }

    function insert_data($table, $data) {
        if ($this->db->insert($table, $data)) {
            return true;
        } else {
            return false;
        }
    }

    function insert_multi_data($table, $data) {
        foreach ($data as $ins) {
            if (!$this->db->insert($table, $ins)) {
                return false;
            }
        }
        return true;
    }

    function update_data($table, $where, $data) {
        if ($where) {
            $this->db->where($where);
        }
        $this->db->set($data);
        return $this->db->update($table);
    }

    function update_multi_data($table, $data, $where_key) {
        return $this->db->update_batch($table, $data, $where_key);
    }

    function delete_data($table, $where, $file = "") {
        if (!empty($file)) {
            unlink($file);
        }
        if (!empty($where)) {
            $this->db->where($where);
        }
        return $this->db->delete($table);
    }

    function get_total($table, $column, $where = "") {
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->select("SUM(" . $column . ") as " . $column);
        $res = $this->db->get($table)->row();
        return $res;
    }

}
