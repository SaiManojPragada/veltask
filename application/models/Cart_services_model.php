<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Cart_services_model extends CI_Model {

    function addUpdateCart($data) {
        $cat_id = $this->my_model->get_data_row("services", array("id" => $data['service_id']))->cat_id;
        $check_cart = $this->my_model->get_data_row("services_cart", array("user_id" => $data['user_id']));
        if (!empty($check_cart) && $cat_id != $check_cart->category_id) {
            return array(
                'status' => false,
                'type' => "req_delete",
                'message' => "You Already have services from another Category."
            );
        }
        $has_visit_and_quote = $data['has_visit_and_quote'];
        $this->db->where("service_id", $data['service_id']);
        $this->db->where("user_id", $data['user_id']);
        $check = $this->db->get("services_cart")->row();
        $user_id = $data['user_id'];
        $service_data = $this->my_model->get_data_row("services", array("id" => $data['service_id']));
        $data['category_id'] = $service_data->cat_id;
        $data['sub_category_id'] = $service_data->sub_cat_id;
//        $data['has_visit_and_quote'] = $service_data->has_visit_and_quote;
        $data['visiting_charges'] = $service_data->visiting_charges;
        $data['tax'] = $service_data->tax;
        $data['unit_price'] = $service_data->sale_price;
        $data['sub_total'] = ((float) $service_data->sale_price * (float) $data['quantity']);
        $data['grand_total'] = (float) $data['sub_total'] +
                ((float) $service_data->visiting_charges + (float) $service_data->tax) -
                ((float) $data['membership_discount'] + (float) $data['coupon_discount']);
        if (!empty($check)) {
//            $session_id = $data['session_id'];
            if ($data['quantity'] === 0 || $data['quantity'] === "0") {
//                $this->db->where("session_id", $session_id);
                $this->db->where("service_id", $data['service_id']);
                $this->db->where("user_id", $data['user_id']);
                $delete = $this->db->delete("services_cart");
                if ($delete) {
                    return array('status' => TRUE,
                        'message' => "Item Removed from Cart",
                        "data" => $this->count_and_calculate($user_id, $has_visit_and_quote)
                    );
                } else {
                    return array('status' => FALSE, 'message' => "Unable to Remove Item from Cart");
                }
            } else {
//                $this->db->where("session_id", $session_id);

                $this->db->where("service_id", $data['service_id']);
                $this->db->where("user_id", $data['user_id']);
                unset($data['session_id']);
                unset($data['user_id']);
                unset($data['service_id']);
                $this->db->set($data);

                $update = $this->db->update("services_cart");
                if ($update) {
                    return array('status' => TRUE,
                        'message' => "Item updated in Cart",
                        "data" => $this->count_and_calculate($user_id, $has_visit_and_quote)
                    );
                } else {
                    return array('status' => FALSE, 'message' => "Unable to Update Item in Cart",);
                }
            }
        } else {
            if ($data['quantity'] === 0 || $data['quantity'] === "0" || $data['quantity'] < 0) {
                return array('status' => FALSE, 'message' => "Quantity Cannot be Zero");
                die;
            }
            $insert = $this->db->insert("services_cart", $data);
            if ($insert) {
                return array('status' => TRUE, 'message' => "Item Added to Cart",
                    "data" => $this->count_and_calculate($user_id, $has_visit_and_quote));
            } else {
                return array('status' => FALSE,
                    'message' => "Unable to Add Item to Cart");
            }
        }
    }

    function count_and_calculate($user_id, $has_visit_and_quote) {
        if ($has_visit_and_quote == 1) {
            $this->db->select("count(id) as items_in_cart, sum(visiting_charges) as cart_total");
        } else {
            $this->db->select("count(id) as items_in_cart, sum(sub_total) as cart_total");
        }
        $this->db->where("user_id", $user_id);
        $res = $this->db->get("services_cart")->row();
        $res->cart_total = ($res->cart_total) ? $res->cart_total : "0";
        return $res;
    }

}
