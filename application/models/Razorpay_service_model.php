<?php

class Razorpay_service_model extends CI_Model {

    function create_order($amount) {

        $payment_gateway_access_key = RAZORPAY_KEY;
        $payment_gateway_secret = RAZORPAY_SECRET;

        $post_fields = [
            "amount" => $amount . "00",
            "currency" => "INR",
            "payment_capture" => 1
        ];

        $URL = "https://api.razorpay.com/v1/orders";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $URL);
        curl_setopt($ch, CURLOPT_USERPWD, "$payment_gateway_access_key:$payment_gateway_secret");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $capture_response = curl_exec($ch);
        $php_capture_response = json_decode($capture_response);
        curl_close($ch);
        if ($php_capture_response->status != "created") {
            return false;
        }
        return $php_capture_response;
    }

}
