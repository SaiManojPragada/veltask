<?php

class Send_push_notification_model extends CI_Model {

    function send($data) {

        unset($data["notification"]["channel_id"]);
        $data["channel_id"] = "all_i_need_ch_id";
        $data["priority"] = "high";
        if (isset($data["notification"])) {
            $data["notification"]["android_channel_id"] = "all_i_need_ch_id";
        }
        $data["category"] = "content_added_notification";
        $data["content_available"] = true;
        $data["mutable_content"] = true;

        $data_string = json_encode($data);
        #print_r($data_string);
        #die;
        $headers = array
            (
            'Authorization: key=' . 'AAAAA-WcmeM:APA91bGWOW4haweH0YAjnA5KrwC9drWtZ5_f1NgaqpufSNDAeBs7sPegXGQnaasD9VLseOA52CBSDhO191WNAzKsuDpwSIlKbOkXRLFkPPVSRyZQ5gC09be-vaXbGl73s-MsEGxyNLWQ',
            'Content-Type: application/json'
        );

        #print_r($data_string);


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        $result = curl_exec($ch);
        curl_close($ch);
        #print_r($result);
        return $result;
    }

    function send_push_notification($title, $message, $android_devices, $ios_devices, $image = null, $type = null, $order_id = null) {

        $android_response = [];
        $ios_response = [];

//        $this->db->where("id", 1);
//        $this->db->select("firebase_push_notification_token");
//        $firebase_push_notification_token = $this->db->get("site_settings")->row()->firebase_push_notification_token;
        //$ios_devices and $android_devices in array format
        $firebase_push_notification_token = 'AAAAA-WcmeM:APA91bGWOW4haweH0YAjnA5KrwC9drWtZ5_f1NgaqpufSNDAeBs7sPegXGQnaasD9VLseOA52CBSDhO191WNAzKsuDpwSIlKbOkXRLFkPPVSRyZQ5gC09be-vaXbGl73s-MsEGxyNLWQ';
        $headers = array
            (
            'Authorization: key=' . $firebase_push_notification_token,
            'Content-Type: application/json'
        );
        $chunk_value = 999;

        if ($android_devices) {
            $android_group_arr = [];
            $android_response = (object) [
                        "multicast_id" => "",
                        "success" => 0,
                        "failure" => 0,
                        "canonical_ids" => 0,
                        "results" => [],
            ];

            $android_group_arr = array_chunk($android_devices, $chunk_value);
            for ($i = 0, $l = count($android_group_arr); $i < $l; $i++) {
                $notification_data = [
                    "title" => $title,
                    "body" => $message,
                    "message" => $message,
                    'image' => $image,
                    'style' => "picture",
                    'picture' => $image,
                    "click_action" => "MessagingActivity",
                    "priority" => "high",
                    'vibrate' => 1,
                    "icon" => "app_icon",
                    'sound' => 'notification_sound',
                ];
                $payload = [];
                if (!empty($type)) {
                    $payload['intent_type'] = $type;
                }
                if (!empty($order_id)) {
                    $payload['order_id'] = $order_id;
                }
                if (!empty($payload)) {
                    $notification_data['payload'] = $payload;
                }
                $data_for_android = array(
                    //"to" => $token,
                    'registration_ids' => $android_group_arr[$i],
                    "data" => $notification_data,
                        /*
                          "notification" => [
                          "title" => $title,
                          "body" => $message,
                          'sound' => 'notification_sound',
                          'vibrate' => 1,
                          "priority" => "high",
                          'image' => $image,
                          'style' => "picture",
                          'picture' => $image,
                          "icon" => "app_icon"
                          ] */
                );

//                print_r($data_for_android); die;
                $data_string = json_encode($data_for_android);

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                $android_response_json = curl_exec($ch);
                curl_close($ch);

                $php_android_arr = json_decode($android_response_json);

                $android_response->multicast_id .= $php_android_arr->multicast_id . ",";
                $android_response->success += $php_android_arr->success;
                $android_response->failure += $php_android_arr->failure;
            }
        }

        if ($ios_devices) {
            $ios_group_arr = [];
            $ios_response = (object) [
                        "multicast_id" => "",
                        "success" => 0,
                        "failure" => 0,
                        "canonical_ids" => 0,
                        "results" => [],
            ];

            $ios_group_arr = array_chunk($ios_devices, $chunk_value);
            for ($i = 0, $l = count($ios_group_arr); $i < $l; $i++) {
                $data_for_ios = array(
                    //"to" => $token,
                    'registration_ids' => $ios_group_arr[$i],
                    "content_available" => true,
                    "mutable_content" => true,
                    "category" => "content_added_notification",
                    "data" => [
                        "title" => $title,
                        "body" => $message,
                        "message" => $message,
                        'image' => $image,
                        "click_action" => "MessagingActivity",
                        "priority" => "high",
                        'vibrate' => 1,
                        "icon" => "app_icon",
                        'sound' => 'notification_sound',
                    ],
                    "notification" => [
                        "title" => $title,
                        "body" => $message,
                        'sound' => 'notification_sound.wav',
                        'vibrate' => 1,
                        "priority" => "high",
                        'image' => $image,
                        "icon" => "app_icon"
                    ]
                );

                $data_string2 = json_encode($data_for_ios);

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string2);
                $ios_response_json = curl_exec($ch);
                curl_close($ch);

                $php_ios_arr = json_decode($ios_response_json);

                $ios_response->multicast_id .= $php_ios_arr->multicast_id . ",";
                $ios_response->success += $php_ios_arr->success;
                $ios_response->failure += $php_ios_arr->failure;
            }
        }

        $response = [
            "android_response" => json_encode($android_response),
            "ios_response" => json_encode($ios_response)
        ];
        return $response;
    }

}
