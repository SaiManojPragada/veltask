<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Service_provider_notifications
 *
 * @author Admin
 */
class Service_provider_notifications_model extends CI_Model {

    //put your code here

    function arrange_and_send($user_addresses_id, $order_id) {
        $order_data = $this->my_model->get_data_row("services_orders", array("order_id" => $order_id));
        $pincode_id = $this->my_model->get_data_row("user_address", array("id" => $user_addresses_id))->pincode;
        $providers_in_pin = $this->db->query("SELECT * FROM service_providers WHERE FIND_IN_SET('$pincode_id',sp_pincodes)")->result();
        $not_arr = array();
        foreach ($providers_in_pin as $tok) {

            /*$categories_ids = $tok->categories_ids;
            $provider_id = $tok->id;
            $serv_qry = $this->db->query("select * from service_providers_disabled_categories where service_providers_id='".$provider_id."' and find_in_set(cat_id,'".$categories_ids."')");
            $serv_result=$serv_qry->result();
            
            if($serv_qry->num_rows()>0)
            {
                        
            }
            else
            {
                $sub_service_qry = $this->db->query("SELECT * FROM `service_providers_disabled_services` WHERE service_providers_id='".$provider_id."' and find_in_set(cat_id,'".$categories_ids."') and service_id=34");
                $sub_service_result = $sub_service_qry->result();
            }*/

            
            $notification_data = array(
                "provider_id" => $tok->id,
                "title" => 'New Order (#' . $order_id . ')',
                "message" => 'Your have a new Order (#' . $order_id . ').',
                "created_at" => time(),
                "order_id" => $order_data->id
            );
            $this->my_model->insert_data("provider_notifications", $notification_data);
            if (!empty($tok->access_token)) {
                array_push($not_arr, $tok->access_token);
            }
        }
        $this->load->model('send_providers_push_notification_model');
        $noti = $this->send_providers_push_notification_model->send_push_notification('New Order',
                'Your have a new Order (#' . $order_id . ').',
                $not_arr,
                array(),
                '', 'view_order', $order_data->id);
        return true;
    }

    function send_amount_paid($order_id, $amount_paid) {
        $order_data = $this->my_model->get_data_row("services_orders", array("id" => $order_id));
        $provider = $this->my_model->get_data_row("service_providers", array("id" => $order_data->accepted_by_service_provider));
        $notification_data = array(
            "provider_id" => $provider->id,
            "title" => 'Order (#' . $order_data->order_id . ') Payment',
            "message" => 'Your Order (#' . $order_data->order_id . ') has recieved a Payment of Rs. ' . $amount_paid,
            "created_at" => time(),
            "order_id" => $order_data->id
        );
        $this->my_model->insert_data("provider_notifications", $notification_data);
        $this->load->model('send_providers_push_notification_model');
        $noti = $this->send_providers_push_notification_model->send_push_notification('Order Payment',
                'Your Order (#' . $order_data->order_id . ') has recieved a Payment of Rs. ' . $amount_paid,
                array($provider->access_token),
                array(),
                '', 'view_order', $order_data->id);
        return true;
    }

}
