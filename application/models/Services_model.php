<?php

class Services_model extends CI_Model {

    function get_services($cat_id, $user_id) {
        $data = array();
        $this->clear_prev_visit_and_quote($user_id);
        $this->db->where("user_id", $user_id);
        $cart_items = $this->db->get("services_cart")->result();
        $this->db->where("cat_id", $cat_id);
        $data['sub_categories'] = $this->db->get("services_sub_categories")->result();
        $max_price = 0;
        if ($data['sub_categories']) {
            foreach ($data['sub_categories'] as $item) {
                $this->db->where("cat_id", $cat_id);
                $this->db->where("sub_cat_id", $item->id);
                $this->db->where("status", true);
                $item->services = $this->db->get("services")->result();

                foreach ($item->services as $ser) {
                    $max_price = ($ser->sale_price > $max_price) ? $ser->sale_price : $max_price;
//                    $ser->quantity = 0;
//                    foreach ($cart_items as $cartt) {
//                        if ($cartt->service_id == $ser->id) {
//                            $ser->quantity = (int) $cartt->quantity;
//                        }
//                    }
//                    $this->db->where("service_id", $ser->id);
//                    $ser->images = $this->db->get("services_images")->result();
                }
                if (!empty($this->input->post('filter_max'))) {
                    $this->db->where("cat_id", $cat_id);
                    $this->db->where("sub_cat_id", $item->id);
                    $this->db->where("sale_price <= " . $this->input->post('filter_max'));
                    $this->db->where("sale_price >= " . $this->input->post('filter_min'));
                    $item->services = $this->db->get("services")->result();
                }
                foreach ($item->services as $ser) {
                    $ser->quantity = 0;
                    foreach ($cart_items as $cartt) {
                        if ($cartt->service_id == $ser->id) {
                            $ser->quantity = (int) $cartt->quantity;
                        }
                    }
                    $this->db->where("service_id", $ser->id);
                    $images = $this->db->get("services_images")->result();
                    $ser->image = base_url() . 'uploads/services/' . $images[0]->image;

                    $this->db->select("avg(rating) as rating");
                    $ser->rating = number_format($this->my_model->get_data_row("services_reviews", array("service_id" => $ser->id))->rating, 1);
                }
            }
        }
        $data['filters'] = array();
        $max_lite = floor($max_price / 6);
        $fill_val = 0;
//        $selected_filters = $this->input->post('selected_filters');
        for ($i = 0; $i < 6; $i++) {
//            $display_text = "Rs. " . ($fill_val + $max_lite);
//            $display_text .= ($fill_val == 0) ? " and Below" : " - Rs. " . $fill_val;
            if ($fill_val == 0) {
                $display_text = "Rs. " . ($fill_val + $max_lite) . " and Below";
            } else {
                $display_text = " Rs. " . $fill_val;
                $display_text .= " -" . ( ($i == 5) ? "Rs. " . round($max_price) : "Rs. " . ($fill_val + $max_lite));
            }
            $jid = array(
                "id" => $i,
                "min" => $fill_val,
                "max" => $fill_val + $max_lite,
                "display_text" => $display_text
//                "is_checked" => in_array(, $item)
            );
            $fill_val = $fill_val + $max_lite;
            array_push($data['filters'], $jid);
        }
        $this->db->where("cat_id", $cat_id);
        $this->db->where("sub_cat_id", 0);
        $data['no_sub_category_services'] = $this->db->get("services")->result();
        foreach ($data['no_sub_category_services'] as $item) {
            $this->db->where("service_id", $item->id);
            $item->images = $this->db->get("services_images")->result();
        }
        return array("status" => true, "message" => "Services Data", "data" => $data);
    }

    function get_filter_services($cat_id, $user_id) {
        $data = array();
        $this->clear_prev_visit_and_quote($user_id);
        $this->db->where("user_id", $user_id);
        $cart_items = $this->db->get("services_cart")->result();
        $this->db->where("cat_id", $cat_id);
        $data['sub_categories'] = $this->db->get("services_sub_categories")->result();
        $max_price = 0;
        if ($data['sub_categories']) {
            foreach ($data['sub_categories'] as $item) {
                $this->db->where("cat_id", $cat_id);
                $this->db->where("sub_cat_id", $item->id);
                $this->db->where("status", true);
                $item->services = $this->db->get("services")->result();

                foreach ($item->services as $ser) {
                    $max_price = ($ser->sale_price > $max_price) ? $ser->sale_price : $max_price;
                }
                if (!empty($this->input->post('price_filter'))) {
                    $filters = json_decode($this->input->post('price_filter'));
                    foreach ($filters as $filter) {
                        if ($filter->is_selected == true) {
                            $item->services = array();
                        }
                    }

                    foreach ($filters as $filter) {
                        if ($filter->is_selected == true) {
                            $this->db->where("cat_id", $cat_id);
                            $this->db->where("sub_cat_id", $item->id);
                            $this->db->where("sale_price <= " . (int) $filter->max);
                            $this->db->where("sale_price >= " . (int) $filter->min);
                            $servs = $this->db->get("services")->result();
                            if (!empty($servs)) {
                                foreach ($servs as $ss) {
                                    array_push($item->services, $ss);
                                }
                            }
                        }
                    }
                }
                foreach ($item->services as $ser) {
                    $ser->quantity = 0;
                    foreach ($cart_items as $cartt) {
                        if ($cartt->service_id == $ser->id) {
                            $ser->quantity = (int) $cartt->quantity;
                        }
                    }
                    $this->db->where("service_id", $ser->id);
                    $images = $this->db->get("services_images")->result();
                    $ser->image = base_url() . 'uploads/services/' . $images[0]->image;

                    $this->db->select("avg(rating) as rating");
                    $ser->rating = number_format($this->my_model->get_data_row("services_reviews", array("service_id" => $ser->id))->rating, 1);
                }
            }
        }
        $data['filters'] = array();
        if ($max_price != 0) {
            if (empty($this->input->post('price_filter'))) {
                $max_lite = floor($max_price / 6);
                $fill_val = 0;
                for ($i = 0; $i < 6; $i++) {
                    if ($fill_val == 0) {
                        $display_text = "Rs. " . ($fill_val + $max_lite) . " and Below";
                    } else {
                        $display_text = " Rs. " . $fill_val;
                        $display_text .= " -" . ( ($i == 5) ? "Rs. " . round($max_price) : "Rs. " . ($fill_val + $max_lite));
                    }
                    $jid = array(
                        "id" => $i,
                        "min" => $fill_val,
                        "max" => $fill_val + $max_lite,
                        "display_text" => $display_text,
                        "is_selected" => false
//                "is_checked" => in_array(, $item)
                    );
                    $fill_val = $fill_val + $max_lite;
                    array_push($data['filters'], $jid);
                }
            } else {
                $data['filters'] = json_decode($this->input->post('price_filter'));
            }
        }
        $this->db->where("cat_id", $cat_id);
        $this->db->where("sub_cat_id", 0);
        $data['no_sub_category_services'] = $this->db->get("services")->result();
        foreach ($data['no_sub_category_services'] as $item) {
            $this->db->where("service_id", $item->id);
            $item->images = $this->db->get("services_images")->result();
        }
        return array("status" => true, "message" => "Services Data", "data" => $data);
    }

    function clear_prev_visit_and_quote($user_id) {
        $check_cart_for_visit_and_quote = $this->my_model->get_data("services_cart", array("user_id" => $user_id, "has_visit_and_quote" => 1));
        if (!empty($check_cart_for_visit_and_quote)) {
            $this->my_model->delete_data("services_cart", array("user_id" => $user_id));
        }
        return true;
    }

}
