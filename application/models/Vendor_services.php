<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Vendor_services
 *
 * @author Admin
 */
class Vendor_services extends CI_Model {

    //put your code here
    function get_services($type, $user_id, $c_id, $today = null, $upcoming = null) {

        if ($type == 'order_placed') {
            $categories_ids = explode(',', $c_id);
            $contained_services = array();
            foreach ($categories_ids as $id) {
                $services = $this->my_model->get_data("services", array("cat_id" => $id));
                foreach ($services as $ser) {
                    array_push($contained_services, $ser->id);
                }
            }
            $inactive_categories_data = $this->my_model->get_data("service_providers_disabled_categories", array("service_providers_id" => $user_id));
            foreach ($inactive_categories_data as $item) {
                $pos = array_search($item->cat_id, $categories_ids);
                unset($categories_ids[$pos]);
            }
            $filtered_services = [];
            $inactive_services_data = $this->my_model->get_data("service_providers_disabled_services", array("service_providers_id" => $user_id));
            foreach ($inactive_services_data as $item) {
                $pos = array_search($item->service_id, $contained_services);
//                unset($contained_services[$pos]);
//                echo $item->service_id . ', ' . $contained_services[$pos] . '...';
                if ($item->service_id == $contained_services[$pos]) {
                    unset($contained_services[$pos]);
                }
            }
            foreach ($contained_services as $dd) {
                $filtered_services[] = $dd;
            }
            $categories_ids = implode(',', $categories_ids);
            if (empty($categories_ids)) {
                return false;
                die;
            }
            $today_date = date('Y-m-d');
            $this->db->where("ordered_categories IN (" . $categories_ids . ")");
            $this->db->where("time_slot_date >= '$today_date'");
            $where = array('order_status' => 'order_placed');
        } else if ($type == 'order_accepted') {
            $this->db->where("(order_status = '" . $type . "' OR order_status = 'order_started')");
            $where = array("accepted_by_service_provider" => $user_id);
        } else {
            $where = array("accepted_by_service_provider" => $user_id, 'order_status' => $type);
        }
        if (!empty($today)) {
            $this->db->where("time_slot_date", $today);
            $this->db->order_by("id", "desc");
        } else if (!empty($upcoming)) {
            $this->db->where("time_slot_date > '" . $upcoming . "'");
            $this->db->order_by("id", "desc");
        }
        $this->db->select("id,user_id,order_id,time_slot_date,time_slot,ordered_categories,user_addresses_id,"
                . "grand_total,amount_paid,created_at,order_status,has_visit_and_quote,payment_type");
        $data = $this->my_model->get_data("services_orders", $where, 'id', 'desc');
        if ($data) {
            if ($type == "order_placed") {
                $count = 0;
                $out = [];
//            echo json_encode($filtered_services);
                foreach ($data as $item) {
                    $item->time_slot_date = date('d-m-Y', strtotime($item->time_slot_date));
                    $item->category_name = $this->my_model->get_data_row('services_categories', array('id' => $item->ordered_categories))->name;
                    $item->ordered_on = date('d M Y, h:i A', $item->created_at);
                    unset($item->created_at);
                    $order_items = $this->my_model->get_data("service_orders_items", array("order_id" => $item->order_id));
                    $starter = true;
                    foreach ($order_items as $oitems) {
                        $pos = array_search($oitems->service_id, $filtered_services);
                        if (empty($pos) && $pos !== 0) {
                            $starter = false;
                        }
                    }
                    if ($starter) {
                        $out[] = $data[$count];
                    }
//                $item->order_items = $order_items;
                    $count++;
                }
                return $out;
            } else {
                foreach ($data as $item) {
                    $item->time_slot_date = date('d-m-Y', strtotime($item->time_slot_date));
                    $item->category_name = $this->my_model->get_data_row('services_categories', array('id' => $item->ordered_categories))->name;
                    $item->ordered_on = date('d M Y, h:i A', $item->created_at);
                    $order_items = $this->my_model->get_data("service_orders_items", array("order_id" => $item->order_id));
                }
                return $data;
            }
        } else {
            return false;
        }
    }

}
