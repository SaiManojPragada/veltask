<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Quotations_model
 *
 * @author Admin
 */
class Quotations_model extends CI_Model {

    //put your code here

    function post_quotation_and_milestones($data, $milestones_json) {
        $this->db->trans_start();
        $order_id = $data['service_order_id'];
        $visitng_charges = $this->my_model->get_data_row("services_orders", array("id" => $order_id))->visiting_charges;
        $data['quotation_amount'] = $data['quotation_amount'] - $visitng_charges;
        $data['balance_amount'] = $data['quotation_amount'];
        $this->db->insert('visit_and_quote_quotaions', $data);
        $last_id = $this->db->insert_id();
        $milestones = json_decode($milestones_json);
        foreach ($milestones as $key => $stone) {
            $stone->id = "";
            if ($key == 0) {
                $stone->amount = $stone->amount - $visitng_charges;
            }
            $stone->date = date('Y-m-d', strtotime($stone->date));
            $stone->quotation_id = $last_id;
            $stone->created_at = time();
            $stone->updated_at = time();
            $stone->status = 'unpaid';
        }
        $this->db->insert_batch("quotation_milestones", $milestones);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->my_model->update_data("services_orders", array("id" => $order_id), array("discount" => $visitng_charges));
            $this->db->trans_commit();
            return $last_id;
        }
    }

    function update_quotation_and_milestones($quotation_id, $data, $milestones_json) {
        $this->db->trans_start();
        $order_id = $data['service_order_id'];
        $visitng_charges = $this->my_model->get_data_row("services_orders", array("id" => $order_id))->visiting_charges;
//        $data['quotation_amount'] = $data['quotation_amount'] - $visitng_charges;
        $this->db->select("SUM(amount) as paid");
        $paid_amount = $this->my_model->get_data_row("quotation_milestones", array("quotation_id" => $quotation_id, "status" => "paid"))->paid;
        $data['quotation_amount'] = $data['quotation_amount'];
        $data['balance_amount'] = $data['quotation_amount'] - $paid_amount;
        $this->db->where("id", $quotation_id);
        $this->db->update("visit_and_quote_quotaions", $data);
        $milestones = json_decode($milestones_json);
        foreach ($milestones as $key => $stone) {
//            if ($key == 0) {
//                $stone->amount = $stone->amount - $visitng_charges;
//            }
            $stone->date = date('Y-m-d', strtotime($stone->date));
            if ($stone->status) {
                unset($stone->status);
            }
            if ($stone->active) {
                unset($stone->active);
            }
            $stone->updated_at = time();
        }
        $this->db->update_batch("quotation_milestones", $milestones, 'id');
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->my_model->update_data("services_orders", array("id" => $order_id), array("discount" => $visitng_charges));
            $this->db->trans_commit();
            return TRUE;
        }
    }

}
