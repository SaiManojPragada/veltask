
<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Homepage_model extends CI_Model {

    function getHomeScreen($user_id, $lat, $lng, $alt = "") {
        if (empty($lat) || empty($lng)) {
            $arr = array("status" => false, "message" => "Invalid Form Data");
            return $arr;
            die;
        }
        $data = [];
        if (empty($alt)) {
            if (empty($user_id)) {
                $arr = array("status" => false, "message" => "Invalid Form Data");
                return $arr;
                die;
            }
            $users = $this->db->query("select * from users where id='" . $user_id . "'");
            $users_row = $users->row();
            $address = $users_row->address_id;
        }
        $admin = $this->db->query("select * from admin where id=1");
        $search_distance = $admin->row()->distance;
        $shop_qry = $this->db->query("SELECT id, ( 3959 * acos ( cos ( radians('" . $lat . "') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians('" . $lng . "') ) + sin ( radians('" . $lat . "') ) * sin( radians( lat ) ) ) ) * 1.60934 AS distance FROM vendor_shop having distance<'" . $search_distance . "'");
        $shops = $shop_qry->result();

        $shop_ids = array_column($shops, 'id');
        $imp = implode(",", $shop_ids);
        $qry = $this->db->query("select * from banners WHERE find_in_set(shop_id,'" . $imp . "') and position=1 and type='shops'");
        $dat = $qry->result();
        if ($qry->num_rows() > 0) {
            $ar = [];
            foreach ($dat as $value) {


                if ($value->app_image != '') {
                    $img = base_url() . "uploads/banners/" . $value->app_image;
                } else {
                    $img = "";
                }

                if ($value->web_image != '') {
                    $web_image = base_url() . "uploads/banners/" . $value->web_image;
                } else {
                    $web_image = "";
                }

                if ($value->type == 'products') {
                    $prod_qry = $this->db->query("select * from products where id='" . $value->product_id . "' and delete_status=0");
                    $dat1 = $prod_qry->row();
                    $title = $dat1->name;
                    $shop_id = $dat1->shop_id;

                    $shp_qry = $this->db->query("select * from vendor_shop where id='" . $shop_id . "'");
                    $shp_row = $shp_qry->row();

                    $shop_status = $shp_row->status;
                    $product_details = array('product_title' => $title, 'product_id' => $dat1->id, 'shop_id' => $shop_id, 'shop_status' => $shop_status);
                } else {
                    $prod_qry = $this->db->query("select * from vendor_shop where id='" . $value->shop_id . "'");
                    $dat1 = $prod_qry->row();

                    $adm_qry = $this->db->query("select * from admin_comissions where shop_id='" . $value->shop_id . "' order by id desc");
                    $adm_row = $adm_qry->row();

                    $product_details = array('shop_name' => $dat1->shop_name, 'cat_id' => $adm_row->cat_id, 'shop_id' => $value->shop_id, 'shop_status' => $dat1->status);
                }

                $ar[] = array('id' => $value->id, 'title' => $value->title, 'image' => $img, 'web_image' => $web_image, 'type' => $value->type, 'product_details' => $product_details);
            }
            $data['sliders'] = $ar;
        } else {
            $data['sliders'] = array();
        }

        $qry = $this->db->query("select * from services_categories where status=1 order by priority asc");
        $dat = $qry->result();
        if ($qry->num_rows() > 0) {
            $ar = [];
            foreach ($dat as $value) {
                $img = base_url() . "uploads/services_categories/" . $value->app_image;
                $ar[] = array('id' => $value->id, 'title' => $value->name, 'image' => $img, 'seo_url' => $value->seo_url);
            }

            $data['services_categories'] = $ar;
        } else {
            $data['services_categories'] = array();
        }

//        $qry1 = $this->db->query("select * from banners where find_in_set(shop_id,'" . $imp . "') and position=2");
        $qry1 = $this->db->query("select id,title,app_image,web_image, ( 3959 * acos ( cos ( radians('" . $lat . "') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians('" . $lng . "') ) + sin ( radians('" . $lat . "') ) * sin( radians( lat ) ) ) ) * 1.60934 AS distance from services_banners where status = 1 having distance<'" . $search_distance . "'");
        $dat1 = $qry1->result();
        if ($qry1->num_rows() > 0) {
            $ar1 = [];
            foreach ($dat1 as $value1) {
                if ($value1->app_image != '') {
                    $img1 = base_url() . "uploads/services_banners/" . $value1->app_image;
                } else {
                    $img1 = "";
                }

                if ($value1->app_image != '') {
                    $web_image1 = base_url() . "uploads/services_banners/" . $value1->web_image;
                } else {
                    $web_image1 = "";
                }


                $ar1[] = array('id' => $value1->id, 'title' => $value1->title, 'image' => $img1, 'web_image' => $web_image1);
            }
            $data['banner'] = $ar1;
        } else {
            $data['banner'] = array();
        }

//products need to be done

        $data['products'] = array();

        $business_benefits = $this->my_model->get_data_row("business_profile_benifits");
        unset($business_benefits->created_at);
        unset($business_benefits->updated_at);
        unset($business_benefits->status);
        unset($business_benefits->id);
        $business_benefits->display_image = base_url('uploads/business_benifits_images/') . $business_benefits->display_image;
        $benefits_description = str_replace('<li>', '<li><span style="color:#f26e6a">✓</span> ', $business_benefits->benefits_description);
        $benefits_description = str_replace('<li>', '', $benefits_description);
        $benefits_description = str_replace('</li>', '', $benefits_description);
        $benefits_description = str_replace('<ul>', '', $benefits_description);
        $benefits_description = str_replace('</ul>', '', $benefits_description);
        $data['business_benefits_data'] = $business_benefits;

        $check_bussiness_account = $this->my_model->get_data_row("users_bussiness_details", array("user_id" => $user_id));
        if (empty($check_bussiness_account)) {
            $business_benefits->has_business_profile = "no";
        } else {
            $business_benefits->has_business_profile = "yes";
        }
        $category_qry = $this->db->query("select id,category_name,app_image from categories where status=1");
        $dat = $category_qry->result();
        foreach ($dat as $value) {
            $value->img = base_url() . "uploads/categories/" . $value->app_image;
        }
        $data['categories'] = $dat;
        $data['notifications_count'] = sizeof($this->my_model->get_data("user_notifications", array("status" => 1)));
        return array('status' => true, 'message' => 'Home Screen Data', 'data' => $data);
    }

}
