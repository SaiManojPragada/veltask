<?php

class Razorpay_model extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function create_razor_pay_order($user_id) {

        $this->db->select("sum(grand_total) as grand_total");
        $this->db->where("user_id", $user_id);
        $cart = $this->db->get("cart")->row();

        $post_fields = [
            "amount" => $cart->grand_total . "00",
            "currency" => "INR",
            "payment_capture" => 1
        ];

        $URL = "https://api.razorpay.com/v1/orders";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $URL);
        curl_setopt($ch, CURLOPT_USERPWD, "$payment_gateway_access_key:$payment_gateway_secret");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $capture_response = curl_exec($ch);
        $php_capture_response = json_decode($capture_response);
        curl_close($ch);
        if ($php_capture_response->status == "created") {
            $this->db->where("user_id", $user_id);
            $cart = $this->db->get("cart")->row();
            $order_id = $php_capture_response->id;
            $this->db->set("razor_pay_order_id", $order_id);
            $this->db->where("ref_id", $booking_ref_number);
            $this->db->update($table_name);
        }
    }

}
