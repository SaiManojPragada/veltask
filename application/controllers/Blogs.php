<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Blogs
 *
 * @author Admin
 */
class Blogs extends MY_Controller {

    //put your code here
    public $data;

    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->data['blogs'] = $this->my_model->get_data("blogs", null, 'priority', 'asc', true);
        $this->my_view('blogs', $this->data);
    }

    function view($slug = '') {
        $this->data['blogs'] = $this->my_model->get_data("blogs", "seo_url !='$slug'", 'priority', 'asc', true, 4);
        $this->data['blog_data'] = $this->my_model->get_data_row("blogs", array("seo_url" => $slug), 'priority', 'asc', true);
        $this->my_view('blog-view', $this->data);
    }

}
