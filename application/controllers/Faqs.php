<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Blogs
 *
 * @author Admin
 */
class Faqs extends MY_Controller {

    //put your code here
    public $data;

    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->data['faqs'] = $this->my_model->get_data("faqs", null, 'priority', 'asc', true);
        $this->my_view('faq', $this->data);
    }

}
