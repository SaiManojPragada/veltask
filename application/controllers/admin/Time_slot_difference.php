<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Time_slot_difference
 *
 * @author Admin
 */
class Time_slot_difference extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            redirect('admin/login');
        }
    }

    public function index() {
        $this->data['page_name'] = 'time_slot_difference';
        $this->data['time_slots_difference'] = $this->my_model->get_data_row("time_slots_difference");
        $this->data['title'] = 'Manage Minimum Time Slot Differnces For Services';
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/time_slot_difference', $this->data);
        $this->load->view('admin/includes/footer');
    }

    public function update() {
        $data = array(
            "difference" => $this->input->post("difference"),
            "updated_at" => time()
        );
        $check = $this->my_model->get_data_row("time_slots_difference");
        if (!empty($check)) {
            $ins = $this->my_model->update_data("time_slots_difference", array("id" => $check->id), $data);
        } else {
            $ins = $this->my_model->insert_data("time_slots_difference", $data);
        }

        if ($ins) {
            $this->session->set_flashdata('success_message', "Time Slot Interval has been Updated");
            redirect('admin/time_slot_difference');
        } else {
            $this->session->set_flashdata('error_message', "Failed to Update Time Slot Interval");
            redirect('admin/time_slot_difference');
        }
    }

}
