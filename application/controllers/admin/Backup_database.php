<?php

class Backup_database extends CI_Controller {

    function __construct() {
        parent::__construct();

        if ($this->session->userdata('admin_login')['logged_in'] != true) {

            //$this->session->set_flashdata('error', 'Session Timed Out');

            redirect('admin/login');
        }
        ini_set('memory_limit', 0);
    }

    function index() {
        // Enter the name of directory
        $pathdir = realpath(APPPATH . "../");
        $zipname = "site_backup_" . date('d_M_Y__H_i_') . ".zip";
        $this->zip->compression_level = 0;
        $this->zip->read_dir($pathdir, FALSE);
        // $this->zip->archive($pathdir.$zipname);
        $this->zip->download($zipname);
    }

}
