<?php



defined('BASEPATH') OR exit('No direct script access allowed');



class Franchise_settlements extends MY_Controller {



    public $data;



    function __construct() {

        parent::__construct();

        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            //$this->session->set_flashdata('error', 'Session Timed Out');
            redirect('admin/login');
        }
    }


    function index() {
        $this->data['title'] = 'Franchise Settlements';
        $this->data['page_name'] = 'franchise_settlements';
        $this->data['show'] = 'online';

         $user_type = $_SESSION['admin_login']['user_type'];

    if($user_type=='franchise') 
    {
        $user_id = $_SESSION['admin_login']['id']; 
       
            $shop_qry = $this->db->query("SELECT * FROM `vendor_shop` WHERE frachise_id='".$user_id."'");
            $shop_result = $shop_qry->result();
            foreach ($shop_result as $value) 
            {
                $shop_ar[]=$value->id;
            }

            $vendor_ids = implode(",", $shop_ar);

           

            $online_qry = $this->db->query("select SUM(franchise_commission) as total_franchise_commission from orders where order_status in (5,7) and find_in_set(vendor_id,'".$vendor_ids."')  order by id desc");
        $orrder_row = $online_qry->row();
        if($orrder_row->total_franchise_commission!='')
        {
            $total_franchise_commission=round($orrder_row->total_franchise_commission,2);
        }
        else
        {
           $total_franchise_commission=0; 
        }


        $franchise_request_qry=$this->db->query("select SUM(request_amount) as total_requested_amount from ecom_franchise_request_payment where franchise_id='".$user_id."'");
         $franchise_request_row = $franchise_request_qry->row();
         if($franchise_request_row->total_requested_amount!='')
         {
            $total_requested_amount=$franchise_request_row->total_requested_amount;
         }
         else
         {
            $total_requested_amount=0;
         }


         $this->data['total_franchise_commission']=round($total_franchise_commission,2);
         $this->data['pending_franchise_commission']=round($total_franchise_commission-$total_requested_amount,2);
        $vendor_req = $this->db->query("select * from ecom_franchise_request_payment where franchise_id='".$user_id."'");
    }
    else
    {
        $online_qry = $this->db->query("select SUM(franchise_commission) as total_franchise_commission from orders where order_status in (5,7) order by id desc");
        $orrder_row = $online_qry->row();
        if($orrder_row->total_franchise_commission!='')
        {
            $total_franchise_commission=round($orrder_row->total_franchise_commission,2);
        }
        else
        {
           $total_franchise_commission=0; 
        }

         $franchise_request_qry=$this->db->query("select SUM(request_amount) as total_requested_amount from ecom_franchise_request_payment");
         $franchise_request_row = $franchise_request_qry->row();
         if($franchise_request_row->total_requested_amount!='')
         {
            $total_requested_amount=$franchise_request_row->total_requested_amount;
         }
         else
         {
            $total_requested_amount=0;
         }


         $this->data['total_franchise_commission']=round($total_franchise_commission,2);
         $this->data['pending_franchise_commission']=round($total_franchise_commission-$total_requested_amount,2);

        $vendor_req = $this->db->query("select * from ecom_franchise_request_payment");
        
    }

        


        
        $this->data['franchise_requests']=$vendor_req->result();

        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/franchise_settlements', $this->data);
        $this->load->view('admin/includes/footer');
    }


    function request()
    {
        $this->data['page_name'] = 'settlements';
        $this->data['title'] = 'Settlements';

       

        $user_id = $_SESSION['admin_login']['id']; 
       
            $shop_qry = $this->db->query("SELECT * FROM `vendor_shop` WHERE frachise_id='".$user_id."'");
            $shop_result = $shop_qry->result();
            foreach ($shop_result as $value) 
            {
                $shop_ar[]=$value->id;
            }

            $vendor_ids = implode(",", $shop_ar);

         $vendor_req = $this->db->query("select SUM(franchise_commission) as total_franchise_commission from orders where order_status in (5,7) and find_in_set(vendor_id,'".$vendor_ids."') order by id desc");
         $vendror_row = $vendor_req->row();
         if($vendror_row->total_franchise_commission!='')
         {
            $franchise_total = $vendror_row->total_franchise_commission;
         }
         else
         {
            $franchise_total = 0;
         }
         
         $franchise_request_qry=$this->db->query("select SUM(request_amount) as total_requested_amount from ecom_franchise_request_payment where franchise_id='".$user_id."'");
         $franchise_request_row = $franchise_request_qry->row();
         if($franchise_request_row->total_requested_amount!='')
         {
            $total_requested_amount=$franchise_request_row->total_requested_amount;
         }
         else
         {
            $total_requested_amount=0;
         }

          $f_qry = $this->db->query("SELECT * FROM `franchises` WHERE id='".$user_id."'");
          $f_row = $f_qry->row();

         $this->data['name']=$f_row->name;
        $this->data['franchise_wallet_amount']=round($franchise_total-$total_requested_amount,2);

        $this->load->view('admin/includes/header', $this->data);

        $this->load->view('admin/request_franchise_settlement', $this->data);

        $this->load->view('admin/includes/footer');
    }


    function add($id) {
        $this->data['page_name'] = 'settlements';
        $this->data['title'] = 'Settlements';

       

        $chk_vend = $this->db->query("select * from ecom_franchise_request_payment where id='".$id."'");
        $vendor_row = $chk_vend->row();


        $franchise_qry = $this->db->query("select * from franchises where id='".$vendor_row->franchise_id."'");
        $franchise_row = $franchise_qry->row();
        $this->data['franchise_name']=$franchise_row->name;

        $this->data['id']=$id;
        $this->data['franchise_commission']=$vendor_row->frachise_amount;
        $this->data['request_amount']=$vendor_row->request_amount;

        $this->load->view('admin/includes/header', $this->data);

        $this->load->view('admin/add_franchise_settlement', $this->data);

        $this->load->view('admin/includes/footer');

    }



    function insert() 
    {
         $franchis_id = $this->input->post('franchis_id');
         $franchise_wallet_amount = $this->input->post('franchise_wallet_amount');
         $requested_amount = $this->input->post('requested_amount');
         $description = $this->input->post('description');

        $data = array(
            'franchise_id'=>$franchis_id,
            'request_amount'=> $requested_amount,
            'frachise_amount'=> $franchise_wallet_amount,
            'description'=> $description,
            'status'=>0,
            'created_at' => time()
        );
        $wr = array('id'=>$id);
        $insert_query = $this->db->insert('ecom_franchise_request_payment', $data);
        if ($insert_query) {
            $this->session->set_flashdata('success_message', 'Request sent Successfully');
            redirect('admin/franchise_settlements');
            die();
        } else {
            redirect('admin/franchise_settlements/add');
            die();
        }
    }

    function updatesettlement()
    {

         $id = $this->input->post('id');
         $franchise_amount = $this->input->post('franchise_amount');
         $requested_amount = $this->input->post('requested_amount');
         $mode_payment = $this->input->post('mode_payment');
         $sender_name = $this->input->post('sender_name');
         $receiver_name = $this->input->post('receiver_name');
         $transaction_id = $this->input->post('transaction_id');
         $description = $this->input->post('description');

        if($this->upload_file('image')!='')
        {
            $transation_img=$this->upload_file('image');
        }
        else
        {
            $transation_img="";
        }

        $data = array(
            'transaction_id'=>$transaction_id,
            'sender_name'=> $sender_name,
            'receiver_name'=> $receiver_name,
            'mode_payment'=> $mode_payment,
            'admin_description'=> $description,
            'image'=>$transation_img,
            'status'=>1,
            'updated_at' => time()
        );
        $wr = array('id'=>$id);
        $insert_query = $this->db->update('ecom_franchise_request_payment', $data,$wr);
        //echo $this->db->last_query(); die;
        if ($insert_query) 
        {
            $this->session->set_flashdata('success_message', 'Settlement added Successfully');
            redirect('admin/franchise_settlements');
            die();
        } else {
            redirect('admin/franchise_settlements/add');
            die();
        }
    }



    private function upload_file($file_name) {
// echo $file_ext = pathinfo($_FILES[$file_name]["name"], PATHINFO_EXTENSION);
// die;
    if($_FILES[$file_name]['name']!='')
    {

        if($_FILES[$file_name]["size"]<'5114374')
        {
            $upload_path1 = "./uploads/payments/";
            $config1['upload_path'] = $upload_path1;
            $config1['allowed_types'] = "*";
            // $config1['allowed_types'] = "*";
            $config1['max_size'] = "204800000";
            $img_name1 = strtolower($_FILES[$file_name]['name']);
            $img_name1 = preg_replace('/[^a-zA-Z0-9\.]/', "_", $img_name1);
            $config1['file_name'] = date("YmdHis") . rand(0, 9999999) . "_" . $img_name1;
            $this->load->library('upload', $config1);
            $this->upload->initialize($config1);
            $this->upload->do_upload($file_name);
            $fileDetailArray1 = $this->upload->data();
            // echo $this->upload->display_errors();
            // die;
            return $fileDetailArray1['file_name'];
        }
        else
        {
            return 'false';
        }
    }
    else
    {
        return '';
    }
    }



    function delete($id) {

        $this->db->where('id', $id);
        if ($this->db->delete('request_payment')) {
                $this->session->set_flashdata('error_message', 'Request Payment Deleted Successfully');
                redirect('admin/request_payment');
         } 
         else 
         {
                $this->session->set_flashdata('error_message', 'Unable to delete');
                redirect('admin/request_payment');
         }

    }



}

