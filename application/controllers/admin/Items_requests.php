<?php

class Items_requests extends MY_Controller {

    function __construct() {
        parent::__construct();

        if ($user_type = $this->session->userdata('admin_login')['user_type'] !== 'franchise') {
            redirect('admin/login');
        }
    }

    function index() {
        $this->data['title'] = 'Items/kits Requests';
        $this->data['page_name'] = 'my_requests';
        $session_data = $this->session->userdata('admin_login');
        $this->db->order_by("id", "desc");
        $this->data['requests'] = $this->my_model->get_data("franchise_requests", array("franchise_id" => $session_data['id']));
        foreach ($this->data['requests'] as $req) {
            $this->db->where("request_id", $req->id);
            $req->requested_items = $this->db->get("franchise_requests_items")->result();
        }
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/items_requests', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function send_request() {
        $this->data['title'] = 'Items/kits Requests';
        $this->data['page_name'] = 'send_request';
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_items_request', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function insert() {
        $session_data = $this->session->userdata('admin_login');
        $data = array(
            "franchise_id" => $session_data['id'],
            "created_at" => time(),
            "request_status" => "In Progress",
            "request_id" => $this->generate_random_key("franchise_requests", "request_id", "REQ")
        );
        $ins = $this->db->insert("franchise_requests", $data);
        if ($ins) {
            $insert_id = $this->db->insert_id();
            $size = sizeof($this->input->post('item'));
            $items = $this->input->post('item');
            $quantities = $this->input->post('quantity');
            for ($i = 0; $i < $size; $i++) {
                $ins_data = array(
                    "franchise_id" => $session_data['id'],
                    "request_id" => $insert_id,
                    "item" => $items[$i],
                    "quantity" => $quantities[$i]
                );
                $this->db->insert("franchise_requests_items", $ins_data);
            }
            $this->session->set_flashdata('success_message', "Requested Succesfully");
            redirect('admin/items_requests');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/items_requests');
        }
    }

}
