<?php

class Testimonials extends MY_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            redirect('admin/login');
        }
    }

    public function index() {
        $this->data['page_name'] = 'testimonials';
        $this->data['title'] = 'Testimonials';
        $this->db->order_by('priority', 'asc');
        $this->data['testimonials'] = $this->my_model->get_data('testimonials');

        $this->load->view('admin/includes/header', $this->data);

        $this->load->view('admin/testimonials', $this->data);

        $this->load->view('admin/includes/footer');
    }

    function add() {

        $this->data['page_name'] = 'testimonials';
        $this->data['func'] = "insert";
        $this->data['title'] = 'Add Testimonial';
        $this->data['data'] = array();
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_testimonial', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function edit($id) {
        $this->data['func'] = "update/";
        $this->data['title'] = 'Update Testimonial';
        $this->data['data'] = $this->my_model->get_data_row("testimonials", array("id" => $id));
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_testimonial', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function delete($id) {
        $delete = $this->my_model->delete_data("testimonials", array("id" => $id));
        if ($delete) {
            $this->session->set_flashdata('success_message', "Testimonial Deleted Succesfully");
            redirect('admin/testimonials');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/testimonials');
        }
    }

    function insert() {
        $_POST['created_at'] = time();
        $_POST['updated_at'] = time();
        $insert = $this->my_model->insert_data("testimonials", $_POST);
        if ($insert) {
            $this->session->set_flashdata('success_message', "Testimonial Added Succesfully");
            redirect('admin/testimonials');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/testimonials');
        }
    }

    function update($id) {
        $_POST['updated_at'] = time();

        $update = $this->my_model->update_data("testimonials", array("id" => $id), $_POST);
        if ($update) {
            $this->session->set_flashdata('success_message', "Testimonial Updated Succesfully");
            redirect('admin/testimonials');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/testimonials');
        }
    }

}
