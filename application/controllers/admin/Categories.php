<?php



defined('BASEPATH') OR exit('No direct script access allowed');



class Categories extends CI_Controller {



    public $data;



    function __construct() {

        parent::__construct();

        if ($this->session->userdata('admin_login')['logged_in'] != true) {

            //$this->session->set_flashdata('error', 'Session Timed Out');

            redirect('admin/login');

        }

    }



    function index() {
        $this->data['page_name'] = 'categories';
        $this->data['title'] = 'Categories';

        $this->db->order_by('priority', 'asc');

        $this->data['categories'] = $this->db->get('categories')->result();

        foreach ($this->data['categories'] as $cat) {

            $this->db->where('cat_id', $cat->id);

            $subcatRow = $this->db->get('sub_categories')->result();

            if ($subcatRow) {

                $cat->sub_categories = $subcatRow;

            } else {

                $cat->sub_categories = array();

            }

        }

        $this->load->view('admin/includes/header', $this->data);

        $this->load->view('admin/categories', $this->data);

        $this->load->view('admin/includes/footer');

    }



    function add() {

        $this->data['title'] = 'Add Category';

        $this->load->view('admin/includes/header', $this->data);

        $this->load->view('admin/add_category', $this->data);

        $this->load->view('admin/includes/footer');

    }

function make_seo_name($title) {
        return preg_replace('/[^a-z0-9_-]/i', '', strtolower(str_replace(' ', '-', trim($title))));
    }

    function insert() {

        $universal_store = $this->input->get_post('universal_store');
        $category_name = $this->input->get_post('category_name');

        $description = $this->input->get_post('description');

        $status = $this->input->get_post('status');
        $priority = $this->input->get_post('priority');


       


        if(!empty($_FILES["app_image"]["name"]))
            {
                $img_k_image = "Category_".$i."_".date('YmdHis').".jpg";
                if (file_exists("./uploads/categories/" . $img_k_image))
                {
                     $k_image= $img_k_image;
                }
                else
                {
                  move_uploaded_file($_FILES["app_image"]["tmp_name"], "./uploads/categories/" . $img_k_image);
                 $k_image=$img_k_image;
                }
            }
            else
            {
                $k_image="";
            }

            $seo_url = $this->make_seo_name($category_name);
        $data = array(
            'universal_store' => $universal_store,

            'category_name' => $category_name,

            'description' => $description,

            'status' => $status,

            'created_at' => time(),
            'seo_url'=>$seo_url,
            'app_image' => $k_image,
            'priority'=>$priority

        );

        $insert_query = $this->db->insert('categories', $data);

        if ($insert_query) {

            redirect('admin/categories');

            die();

        } else {

            redirect('admin/categories/add');

            die();

        }

    }



    function edit_category($cat_id) {

        $this->data['title'] = 'Edit Category';

        $this->data['category'] = $this->db->get_where('categories', ['id' => $cat_id])->row();

        $this->load->view('admin/includes/header');

        $this->load->view('admin/edit_category', $this->data);

        $this->load->view('admin/includes/footer');

    }



    function update() {

        $cat_id = $this->input->get_post('cat_id');

        $universal_store = $this->input->get_post('universal_store');

        $category_name = $this->input->get_post('category_name');

        $description = $this->input->get_post('description');

        $status = $this->input->get_post('status');

        $priority = $this->input->get_post('priority');
         $seo_url = $this->make_seo_name($category_name);
         
        $data = array(
            'universal_store' => $universal_store,

            'category_name' => $category_name,

            'description' => $description,
            'seo_url'=>$seo_url,
            'status' => $status,

            'updated_at' => time(),
             'priority'=>$priority

        );

     

           /* $config['upload_path'] = './uploads/categories/';

            $config['allowed_types'] = 'gif|jpg|png';

            $config['max_size'] = 5000;

            $config['max_width'] = 3000;

            $config['max_height'] = 3000;



            $this->load->library('upload', $config);

            if ($this->upload->do_upload('app_image')) {

                $imageDetailArray2 = $this->upload->data();

                $app_image_name = $imageDetailArray2['file_name'];

            } else {

                $app_image_name = NULL;

            }*/
            if(!empty($_FILES["app_image"]["name"]))
            {
                $img_k_image = "Category_".date('YmdHis').".jpg";

                if (file_exists("./uploads/categories/" . $img_k_image))
                {
                     $k_image= $img_k_image;
                }
                else
                {
                  move_uploaded_file($_FILES["app_image"]["tmp_name"], "./uploads/categories/" . $img_k_image);
                  $k_image=$img_k_image;
                }
                $data['app_image'] = $k_image;
                $this->db->where('id', $cat_id);
                $update_query = $this->db->update('categories', $data);
            }
            else
            {
                $this->db->where('id', $cat_id);
                $update_query = $this->db->update('categories', $data);
            }
        if ($update_query) {
            redirect('admin/categories');

            die();

        } else {

            redirect('admin/categories/edit_category/' . $cat_id);

            die();

        }

    }



    function delete($cat_id) {

        $check = $this->db->query("select * from products where cat_id='".$cat_id."'");

        if($check->num_rows()>0)
        {
                 $this->session->set_flashdata('error_message', "Some products are assigned, Unable to delete");
                redirect('admin/categories');
        }
        else
        {

        $this->db->where('cat_id', $cat_id);

        $subcatFound = $this->db->get('sub_categories')->result();

        if (count($subcatFound) > 0) {

            $this->session->set_flashdata('error_message', 'Some subcategories are assigned, Unable to delete');

            redirect('admin/categories');

        } else {

            $this->db->where('id', $cat_id);

            if ($this->db->delete('categories')) {

                $this->session->set_flashdata('success_message', 'Category Deleted Successfully');

                redirect('admin/categories');

            } else {

                $this->session->set_flashdata('error_message', 'Unable to delete');

                redirect('admin/categories');

            }

        }
    }

    }



}

