<?php

class Services_sub_categories extends MY_Controller {

    public $data;
    public $table_name = "services_sub_categories";

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            redirect('admin/login');
        }
    }

    public function index() {
        $this->data['page_name'] = 'services_sub_categories';
        $this->data['title'] = 'Services Sub Categories';
        $this->data['sub_categories'] = $this->db->get($this->table_name)->result();
        foreach ($this->data['sub_categories'] as $item) {
            $item->category_name = $this->my_model->get_data_row("services_categories", array("id" => $item->cat_id))->name;
        }
        $this->load->view('admin/includes/header', $this->data);

        $this->load->view('admin/services_sub_categories', $this->data);

        $this->load->view('admin/includes/footer');
    }

    function add() {
        $this->data['categories'] = $this->db->get("services_categories")->result();
        $this->data['func'] = "insert";
        $this->data['title'] = 'Add Services Sub Category';
        $this->data['data'] = array();
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_services_sub_category', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function edit($id) {
        $this->data['categories'] = $this->db->get("services_categories")->result();
        $this->data['func'] = "update/";
        $this->data['title'] = 'Update Services Category';
        $this->data['data'] = $this->my_model->get_data_row($this->table_name, array("id" => $id));
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_services_sub_category', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function delete($id) {
        $delete = $this->my_model->delete_data($this->table_name, array("id" => $id));
        if ($delete) {
            $this->session->set_flashdata('success_message', "Service Category Deleted Succesfully");
            redirect('admin/services_sub_categories');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/services_sub_categories');
        }
    }

    function insert() {
        $_POST["app_image"] = $this->image_upload("app_image", "services_sub_categories");
        $_POST["web_image"] = $this->image_upload("web_image", "services_sub_categories");
        $_POST['created_at'] = time();
        $_POST['updated_at'] = time();
        $_POST['seo_url'] = $this->make_seo_name($this->input->post('sub_category_name'));
        $insert = $this->my_model->insert_data($this->table_name, $_POST);
        if ($insert) {
            $this->session->set_flashdata('success_message', "Service Sub Category Added Succesfully");
            redirect('admin/services_sub_categories');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/services_sub_categories');
        }
    }

    function update($id) {
        $_POST["app_image"] = $this->image_upload("app_image", "services_sub_categories");
        $_POST["web_image"] = $this->image_upload("web_image", "services_sub_categories");
        $_POST['updated_at'] = time();
        $_POST['seo_url'] = $this->make_seo_name($this->input->post('sub_category_name'));
        if (empty($_POST["app_image"])) {
            unset($_POST["app_image"]);
        }
        if (empty($_POST["web_image"])) {
            unset($_POST["web_image"]);
        }
        $update = $this->my_model->update_data($this->table_name, array("id" => $id), $_POST);
        if ($update) {
            $this->session->set_flashdata('success_message', "Service Sub Category Updated Succesfully");
            redirect('admin/services_sub_categories');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/services_sub_categories');
        }
    }

}
