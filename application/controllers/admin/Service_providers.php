<?php

class Service_providers extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            redirect('admin/login');
        }
    }

    function index() {
        $status = true;
        $this->data['page_name'] = 'active_service_provides';
        if (!empty($this->input->get('status'))) {
            $status = false;
            $this->data['page_name'] = 'inactive_service_provides';
        }
        $this->data['title'] = 'Service Providers';
        $this->db->where("status", $status);
        $this->db->where("owner_id", 0);
        $this->db->order_by("id", "desc");
        $this->data['providers'] = $this->db->get('service_providers')->result();
        foreach ($this->data['providers'] as $item) {
            $this->db->select("AVG(rating) as rating");
            $item->provider_rating = round($this->my_model->get_data_row("services_reviews", array("service_provider_id" => $item->id))->rating, 1);
            if ($item->owner_id) {
                $item->owner_name = $this->my_model->get_data_row('service_providers')->name;
            }
            $item->selected_franchise = $this->my_model->get_data_row("franchises", array("id" => $item->franchise_id))->name;
            $item->comissions = $this->my_model->get_data("provider_comissions", array("providers_id" => $item->id));
            $this->db->where("id", $item->location_id);
            $item->location_name = $this->db->get("cities")->row()->city_name;
            $categories = $this->db->where('id IN (' . rtrim($item->categories_ids, ',') . ')')->get('services_categories')->result();
            foreach ($categories as $cat) {
                $item->categories_names .= $cat->name . ', ';
            }
            $item->categories_names = rtrim($item->categories_names, ', ');
        }
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/service_providers', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function add() {
        $this->data['func'] = "insert";
        $this->data['page_name'] = 'active_service_provides';
        $this->data['title'] = 'Add Service Provider';

        $this->data['states'] = $this->db->get("states")->result();
        $this->data['pincodes'] = $this->db->get('pincodes')->result();

        $this->data['categories'] = $this->db->get("services_categories")->result();
        $this->data['franchises'] = $this->db->get("franchises")->result();
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_service_provider', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function edit($id) {
        $this->data['func'] = "update/";
        $this->data['page_name'] = 'active_service_provides';
        $this->data['title'] = 'Add Service Provider';
        $this->data['data'] = $this->my_model->get_data_row("service_providers", array("id" => $id));
        $this->data['states'] = $this->db->get("states")->result();
        $this->db->where("state_id", $this->data['data']->state_id);
        $this->data['cities'] = $this->db->get('cities')->result();
        $this->db->where("city_id", $this->data['data']->location_id);
        $this->data['pincodes'] = $this->db->get('pincodes')->result();
        $this->data['categories'] = $this->db->get("services_categories")->result();
        $this->data['franchises'] = $this->db->get("franchises")->result();
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_service_provider', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function insert() {
        $_POST['categories_ids'] = implode(',', $_POST['categories_ids']) . ',';
        $_POST['sp_pincodes'] = implode(',', $_POST['sp_pincodes']);
        $_POST['created_at'] = time();
        $_POST['updated_at'] = time();
        $_POST['password'] = md5($_POST['password']);
        $_POST["photo"] = $this->image_upload("photo", "service_providers");
        $_POST["aadhar_photo"] = $this->image_upload("aadhar_photo", "service_providers");
        $_POST["pcc_photo"] = $this->image_upload("pcc_photo", "service_providers");
        $_POST["vaccination_certificate"] = $this->image_upload("vaccination_certificate", "service_providers");
        $check_email = $this->db->where("email", $this->input->post('email'))->get("service_providers")->result();
        $check = $this->db->where("phone", $this->input->post("phone"))->get("service_providers")->result();
        if (sizeof($check_email) > 0) {
            $this->session->set_flashdata('error_message', "Account with this Email Already Exists");
            redirect('admin/service_providers');
            die;
        } else if (sizeof($check) > 0) {
            $this->session->set_flashdata('error_message', "Account with this Phone Number Already Exists");
            redirect('admin/service_providers');
            die;
        } else {
            $insert = $this->my_model->insert_data("service_providers", $_POST);
            if ($insert) {
                $this->session->set_flashdata('success_message', "Service Provider Added Succesfully");
                redirect('admin/service_providers');
            } else {
                $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
                redirect('admin/service_providers');
            }
        }
    }

    function update($id) {
        $_POST['updated_at'] = time();
        $_POST['categories_ids'] = implode(',', $_POST['categories_ids']) . ',';
        $_POST['sp_pincodes'] = implode(',', $_POST['sp_pincodes']);
        if (empty($this->input->post("password"))) {
            unset($_POST['password']);
        } else {
            $_POST['password'] = md5($_POST['password']);
        }

        $_POST["photo"] = $this->image_upload("photo", "service_providers");
        $_POST["aadhar_photo"] = $this->image_upload("aadhar_photo", "service_providers");
        $_POST["pcc_photo"] = $this->image_upload("pcc_photo", "service_providers");
        $_POST["vaccination_certificate"] = $this->image_upload("vaccination_certificate", "service_providers");
        if (empty($_POST["photo"])) {
            unset($_POST["photo"]);
        }
        if (empty($_POST["aadhar_photo"])) {
            unset($_POST["aadhar_photo"]);
        }
        if (empty($_POST["pcc_photo"])) {
            unset($_POST["pcc_photo"]);
        }
        if (empty($_POST["vaccination_certificate"])) {
            unset($_POST["vaccination_certificate"]);
        }
        $this->db->where("id !=", $id);
        $check = $this->db->where("phone", $this->input->post("phone"))->get("service_providers")->result();
        $this->db->where("id !=", $id);
        $check_email = $this->db->where("email", $this->input->post('email'))->get("service_providers")->result();
        if (sizeof($check_email) > 0) {
            $this->session->set_flashdata('error_message', "Account with this Email Already Exists");
            redirect('admin/service_providers');
            die;
        } else if (sizeof($check) > 0) {
            $this->session->set_flashdata('error_message', "Account with this Phone Number Already Exists");
            redirect('admin/service_providers');
            die;
        } else {
            $update = $this->my_model->update_data("service_providers", array("id" => $id), $_POST);
            if ($update) {
                $this->session->set_flashdata('success_message', 'Service Provider Updated Successfully.');
                redirect(base_url('admin/service_providers'));
            } else {
                $this->session->set_flashdata('error_message', 'Failed to Update Service Provider.');
                redirect(base_url('admin/service_providers'));
            }
        }
    }

    function delete($id) {
        $delete = $this->my_model->delete_data("service_providers", array("id" => $id));
        if ($delete) {
            $this->session->set_flashdata('success_message', "Service provider Deleted Succesfully");
            redirect('admin/service_providers');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/service_providers');
        }
    }

    function manage_categories($id, $func_status = 'add', $comm_id = "") {
        if (!empty($comm_id)) {
            $this->data['admin_edit_comissions'] = $this->my_model->get_data_row("provider_comissions", array("id" => $comm_id));
        }
        $this->data['func_status'] = $func_status;
        $this->data['page_name'] = 'active_service_provides';
        $this->data['data'] = $this->my_model->get_data_row("service_providers", array("id" => $id));
        $this->data['categories'] = $this->db->where('id IN (' . rtrim($this->data['data']->categories_ids, ',') . ')')->get('services_categories')->result();
        $this->data['comissions'] = $this->my_model->get_data("provider_comissions", array("providers_id" => $id));
        foreach ($this->data['comissions'] as $item) {
            $item->category_name = $this->my_model->get_data_row("services_categories", array("id" => $item->cat_id))->name;
        }
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/provider_manage_categories', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function insert_cat_comission() {
        $_POST['created_at'] = time();
        $_POST['updated_at'] = time();
        $insert = $this->my_model->insert_data("provider_comissions", $this->input->post());
        if ($insert) {
            $this->session->set_flashdata('success_message', "Service Provider Comission Added Succesfully");
            redirect('admin/service_providers/manage_categories/' . $this->input->post('providers_id'));
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/service_providers/manage_categories/' . $this->input->post('providers_id'));
        }
    }

    function update_cat_comission($id, $providers_id) {
        $_POST['updated_at'] = time();
        $update = $this->my_model->update_data("provider_comissions", array("id" => $id), $this->input->post());
        if ($update) {
            $this->session->set_flashdata('success_message', 'Service Provider Comission Updated Successfully.');
            redirect(base_url('admin/service_providers/manage_categories/' . $providers_id));
        } else {
            $this->session->set_flashdata('error_message', 'Failed to Update Service Provider.');
            redirect(base_url('admin/service_providers/manage_categories/' . $providers_id));
        }
    }

    function delete_comission($id, $providers_id) {
        $delete = $this->my_model->delete_data("provider_comissions", array("id" => $id, "providers_id" => $providers_id));
        if ($delete) {
            $this->session->set_flashdata('success_message', "Service provider Comission Deleted Succesfully");
            redirect('admin/service_providers/manage_categories/' . $providers_id);
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/service_providers/manage_categories/' . $providers_id);
        }
    }

    function view_technicians($id) {
        $this->db->where("id", $id);
        $this->data['owner'] = $this->db->get("service_providers")->row();
        $this->db->where("owner_id", $id);
        $this->db->order_by("id", "desc");
        $this->data['techs'] = $this->db->get("service_providers")->result();
        foreach ($this->data['techs'] as $item) {
            $this->db->where("id", $item->location_id);
            $item->location_name = $this->db->get("cities")->row()->city_name;
            $categories = $this->db->where('id IN (' . rtrim($item->categories_ids, ',') . ')')->get('services_categories')->result();
            foreach ($categories as $cat) {
                $item->categories_names .= $cat->name . ', ';
            }
            $item->categories_names = rtrim($item->categories_names, ', ');
        }
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/view_technicians', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function login_service_provider($service_provider_id) {
        $service_provider = $this->my_model->get_data_row("service_providers", array("id" => $service_provider_id));
        $sess_arr = array(
            'vendor_type' => 'services',
            'vendor_id' => $service_provider->id,
            'owner_name' => $service_provider->name,
            'provider_type' => $service_provider->type,
            'vendors_logged_in' => true
        );
        $this->session->set_flashdata('msg', 'Welcome');
        $this->session->set_userdata('vendors', $sess_arr);
        redirect('vendors/dashboard');
    }

    function ratings($id = null) {
        $this->data['service_provider'] = $this->my_model->get_data_row("service_providers", array("id" => $id));
        $this->data['title'] = $this->data['service_provider']->name . " Reviews";
        $this->db->select("AVG(rating) as rating");
        $this->data['provider_rating'] = $this->my_model->get_data_row("services_reviews", array("service_provider_id" => $id))->rating;
        if ($id == null || empty($id)) {
            redirect(base_url() . "admin/service_providers");
        }
        $ratings = $this->my_model->get_data("services_reviews", array("service_provider_id" => $id));
        foreach ($ratings as $rat) {
            $gen_order_id = $this->my_model->get_data_row("services_orders", array("id" => $rat->order_id))->order_id;
            $this->db->select("service_id,sub_total,quantity");
            $rat->order_details = $this->my_model->get_data_row("service_orders_items", array("order_id" => $gen_order_id, "service_id" => $rat->service_id));
            $rat->order_details->service_name = $this->my_model->get_data_row("services", array("id" => $rat->service_id))->service_name;
        }
        $this->data['ratings'] = $ratings;
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/view_service_provider_ratings', $this->data);
        $this->load->view('admin/includes/footer');
    }

}
