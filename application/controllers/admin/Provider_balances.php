<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of About_us
 *
 * @author Admin
 */
class Provider_balances extends MY_Controller {

    //put your code here
    public $data;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            redirect('admin/login');
        }
    }

    public function index() {
        if (!empty($this->input->post())) {
            $amount_paid = $this->input->post("amount");
            $provider_id = $this->input->post("provider_id");
            if ($amount_paid > 0 && !empty($provider_id)) {
                $wallet_amount = $this->my_model->get_data_row("service_providers_payments", array("provider_id" => $provider_id))->total_amount;
                if ($wallet_amount < 0) {
                    $new_wallet_amount = $wallet_amount + $amount_paid;
                    $update = $this->my_model->update_data("service_providers_payments", array("provider_id" => $provider_id), array("total_amount" => $new_wallet_amount));
                    if ($update) {
                        $ins_data = array(
                            "during_order_id" => "0",
                            "service_provider_id" => $provider_id,
                            "credit_or_debit_amount" => $amount_paid,
                            "type" => "credit",
                            "message" => "Credited from Admin",
                            "created_at" => time()
                        );
                        $this->my_model->insert_data("provider_wallet_transactions", $ins_data);
                        $this->session->set_flashdata('success_message', 'Service Provoder wallet updated Successfully');
                    } else {
                        $this->session->set_flashdata('error_message', 'Something went wrong, Please Try again');
                    }
                } else {
                    $this->session->set_flashdata('error_message', 'No Balance Amount found for this Provider');
                }
            } else {
                $this->session->set_flashdata('error_message', 'Please Enter Valid Details');
            }
            unset($_POST);
            redirect(base_url('admin/provider_balances'));
        }
        $data['page_name'] = "provider_balances";
        $this->db->where("total_amount <", 0);
        $this->db->select("provider_id,total_amount");
        $data['providers'] = $this->my_model->get_data("service_providers_payments");
        foreach ($data['providers'] as $pro) {
            $pro->provider_details = $this->my_model->get_data_row("service_providers", array("id" => $pro->provider_id));
        }
        $data['title'] = "Providers Balances";
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/view_return_amount_providers', $data);
        $this->load->view('admin/includes/footer');
    }

}
