<?php

class Faq extends MY_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            redirect('admin/login');
        }
    }

    public function index() {
        $this->data['page_name'] = 'faqs';
        $this->data['title'] = 'Faq`s';
        $this->db->order_by('id', 'desc');
        $this->data['faqs'] = $this->my_model->get_data('faqs');

        $this->load->view('admin/includes/header', $this->data);

        $this->load->view('admin/faqs', $this->data);

        $this->load->view('admin/includes/footer');
    }

    function add() {

        $this->data['page_name'] = 'blogs';
        $this->data['func'] = "insert";
        $this->data['title'] = 'Add Blog';
        $this->data['data'] = array();
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_faq', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function edit($id) {
        $this->data['func'] = "update/";
        $this->data['title'] = 'Update Blog';
        $this->data['data'] = $this->my_model->get_data_row("faqs", array("id" => $id));
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_faq', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function delete($id) {
        $delete = $this->my_model->delete_data("faqs", array("id" => $id));
        if ($delete) {
            $this->session->set_flashdata('success_message', "faq Deleted Succesfully");
            redirect('admin/faq');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/faq');
        }
    }

    function insert() {
        $_POST['created_at'] = time();
        $_POST['updated_at'] = time();
        $insert = $this->my_model->insert_data("faqs", $_POST);
        if ($insert) {
            $this->session->set_flashdata('success_message', "faqs Added Succesfully");
            redirect('admin/faq');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/faq');
        }
    }

    function update($id) {
        $_POST['updated_at'] = time();
        $update = $this->my_model->update_data("faqs", array("id" => $id), $_POST);
        if ($update) {
            $this->session->set_flashdata('success_message', "faqs Updated Succesfully");
            redirect('admin/faq');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/faq');
        }
    }

}
