<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Manage_referrals
 *
 * @author Admin
 */
class Business_benefits extends MY_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            redirect('admin/login');
        }
    }

    public function index() {

        if (!empty($this->input->post('btn_ref'))) {
            unset($_POST['btn_ref']);
            $_POST['created_at'] = time();
            $_POST['updated_at'] = time();
            $_POST['display_image'] = $this->image_upload('display_image', 'business_benifits_images', true, $this->input->post('prev_image'));
            if (empty($_POST['display_image'])) {
                $_POST['display_image'] = $this->input->post('prev_image');
            }
            unset($_POST['prev_image']);
            $this->db->empty_table('business_profile_benifits');
            $ins = $this->my_model->insert_data('business_profile_benifits', $this->input->post());
            if ($ins) {
                $this->session->set_flashdata('success_message', "Business profile benifits updated");
            } else {
                $this->session->set_flashdata('error_message', "Unable to update Business profile");
            }
            redirect(base_url() . 'admin/business_benefits');
            unset($_POST);
        }

        $this->data['page_name'] = 'business_benefits';
        $this->data['title'] = 'Manage Business Benefits';
        $this->data['benefits'] = $this->my_model->get_data_row("business_profile_benifits");

        $this->load->view('admin/includes/header', $this->data);

        $this->load->view('admin/business_benefits', $this->data);

        $this->load->view('admin/includes/footer');
    }

}
