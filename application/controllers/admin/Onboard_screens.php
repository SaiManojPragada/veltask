<?php

class Onboard_screens extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            redirect('admin/login');
        }
    }

    function index() {
        $this->data['page_name'] = 'onboard_screens';
        $this->data['title'] = 'Onboard Screens';
        $qry = $this->db->get("onboard_screens");
        $this->data['onboards'] = $qry->result();
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/onboard_images', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function add() {

        $this->data['page_name'] = 'onboard_screens';
        $this->data['func'] = "insert";
        $this->data['title'] = 'Add Onboard Screen';
        $this->data['data'] = array();
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_onboard_screens', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function edit($id) {
        $this->data['func'] = "update/";
        $this->data['page_name'] = '';
        $this->data['title'] = 'Update Onboard Screen';
        $this->data['data'] = $this->my_model->get_data_row("onboard_screens", array("id" => $id));
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_onboard_screens', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function insert() {
        $_POST["image"] = $this->image_upload("image", "onboard_screens");
        $_POST['updated_at'] = time();
        $insert = $this->my_model->insert_data("onboard_screens", $_POST);
        if ($insert) {
            $this->session->set_flashdata('success_message', "Onboard Screen Added Succesfully");
            redirect('admin/onboard_screens');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/onboard_screens');
        }
    }

    function update($id) {
        $_POST["image"] = $this->image_upload("image", "onboard_screens");
        $_POST['updated_at'] = time();
        if (empty($_POST["image"])) {
            unset($_POST["image"]);
        }
        $update = $this->my_model->update_data("onboard_screens", array("id" => $id), $_POST);
        if ($update) {
            $this->session->set_flashdata('success_message', "Onboard Screen Updated Succesfully");
            redirect('admin/onboard_screens');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/onboard_screens');
        }
    }

    function delete($id) {
        $delete = $this->my_model->delete_data("onboard_screens", array("id" => $id));
        if ($delete) {
            $this->session->set_flashdata('success_message', "Onboard Screen Deleted Succesfully");
            redirect('admin/onboard_screens');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/onboard_screens');
        }
    }

}
