<?php

class Company_profiles extends MY_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            redirect('admin/login');
        }
    }

    function index() {
        $this->data['page_name'] = 'company_profiles';
        $this->data['title'] = 'Manage Business Profiles';

        $this->data['profiles'] = $this->my_model->get_data("users_bussiness_details");

        foreach ($this->data['profiles'] as $item) {
            $item->user_details = $this->my_model->get_data_row("users", array("id" => $item->user_id));
        }

        $this->load->view('admin/includes/header', $this->data);

        $this->load->view('admin/company_profiles', $this->data);

        $this->load->view('admin/includes/footer');
    }

    function approveOrDisapprove($id) {
        $prev_status = $this->my_model->get_data_row("users_bussiness_details", array("id" => $id))->status;
        $status = true;
        if ($prev_status) {
            $status = false;
        }
        $update = array("status" => $status);
        $this->my_model->update_data("users_bussiness_details", array("id" => $id), $update);
        redirect(base_url('admin/company_profiles'));
    }

}
