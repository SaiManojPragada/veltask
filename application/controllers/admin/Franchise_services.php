<?php

class Franchise_services extends MY_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            redirect('admin/login');
        }
    }

    public function index() {
        $this->data['page_name'] = 'services';
        $this->data['title'] = 'Manage Services';
//        $this->data['services'] = $this->db->get('franchises_services_prices')->result();
//        foreach ($this->data['services'] as $item) {
//            $this->db->where("id", $item->category_id);
//            $item->category_name = $this->db->get("services_categories")->row()->name;
//            $this->db->where("id", $item->sub_category_id);
//            $item->sub_category_name = $this->db->get("services_sub_categories")->row()->sub_category_name;
//            $this->db->where("id", $item->service_id);
//            $item->service_name = $this->db->get("services")->row()->service_name;
//        }
        $this->data['services'] = $this->db->get("services")->result();
        $this->load->view('admin/includes/header', $this->data);

        $this->load->view('admin/franchise_services_prices', $this->data);

        $this->load->view('admin/includes/footer');
    }

    function add() {

        $this->data['page_name'] = 'services';
        $this->data['func'] = "insert";
        $this->data['title'] = 'Add Services';
        $this->data['data'] = array();
        $this->data['time_slots'] = $this->db->get('services_time_slots')->result();
        $this->data['categories'] = $this->db->get('services_categories')->result();
        $this->data['sub_categories'] = $this->db->get('services_sub_categories')->result();
        $this->data['services'] = $this->db->get('services')->result();
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_franchise_service', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function edit($id) {
        $this->data['func'] = "update/";
        $this->data['title'] = 'Update Services Category';
        $this->data['data'] = $this->my_model->get_data_row("franchises_services_prices", array("id" => $id));
        $this->data['time_slots'] = $this->db->get('services_time_slots')->result();
        $this->data['categories'] = $this->db->get('services_categories')->result();
        $this->data['sub_categories'] = $this->db->get('services_sub_categories')->result();
        $this->data['services'] = $this->db->get('services')->result();
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_franchise_service', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function delete($id) {
        $delete = $this->my_model->delete_data("services", array("id" => $id));
        if ($delete) {
            $this->session->set_flashdata('success_message', "Service Deleted Succesfully");
            redirect('admin/services');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/services');
        }
    }

    function insert() {
        $_POST['created_at'] = time();
        $_POST['updated_at'] = time();
        $insert = $this->my_model->insert_data("franchises_services_prices", $_POST);
        if ($insert) {
            $this->session->set_flashdata('success_message', "Service Added Succesfully");
            redirect('admin/franchise_services');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/franchise_services');
        }
    }

    function update($id) {
        $_POST['updated_at'] = time();
        $update = $this->my_model->update_data("franchises_services_prices", array("id" => $id), $_POST);
        if ($update) {
            $this->session->set_flashdata('success_message', "Service Updated Succesfully");
            redirect('admin/franchise_services');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/franchise_services');
        }
    }

    function get_sub_categories() {
        $where = array(
            "cat_id" => $this->input->post('cat_id')
        );
        $this->db->where($where);
        $subs = $this->db->get("services_sub_categories")->result();
        $op = "<option value=''>Select Sub Categories</option>";
        foreach ($subs as $ss) {
            $op .= "<option value='" . $ss->id . "'>" . $ss->sub_category_name . "</option>";
        }
        echo $op;
    }

    function get_services() {
        $where["cat_id"] = $this->input->post('cat_id');
        if (!empty($this->input->post('sub_cat_id'))) {
            $where["sub_cat_id"] = $this->input->post('sub_cat_id');
        }
        $this->db->where($where);
        $subs = $this->db->get("services")->result();
        $op = "<option value=''>Select Service</option>";
        foreach ($subs as $ss) {
            $op .= "<option value='" . $ss->id . "'>" . $ss->service_name . "</option>";
        }
        echo $op;
    }

}
