<?php

class Franchise_tabs extends MY_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            redirect('admin/login');
        }
    }

    public function index() {
        $this->data['page_name'] = 'franchise_tabs';
        $this->data['title'] = 'Franchise Counts';
        $this->db->order_by('priority', 'asc');
        $this->data['franchise_tabs'] = $this->my_model->get_data('franchise_tabs');

        $this->load->view('admin/includes/header', $this->data);

        $this->load->view('admin/franchise_tabs', $this->data);

        $this->load->view('admin/includes/footer');
    }

    function add() {

        $this->data['page_name'] = 'franchise_tabs';
        $this->data['func'] = "insert";
        $this->data['title'] = 'Add Franchise Count';
        $this->data['data'] = array();
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_franchise_tab', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function edit($id) {
        $this->data['func'] = "update/";
        $this->data['title'] = 'Update Franchise Count';
        $this->data['data'] = $this->my_model->get_data_row("franchise_tabs", array("id" => $id));
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_franchise_tab', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function delete($id) {
        $delete = $this->my_model->delete_data("franchise_tabs", array("id" => $id));
        if ($delete) {
            $this->session->set_flashdata('success_message', "Franchise Tab Deleted Succesfully");
            redirect('admin/franchise_tabs');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/franchise_tabs');
        }
    }

    function insert() {
        $_POST['created_at'] = time();
        $_POST['updated_at'] = time();
        $_POST['image'] = $this->image_upload('image', 'franchise_tabs');
        $insert = $this->my_model->insert_data("franchise_tabs", $_POST);
        if ($insert) {
            $this->session->set_flashdata('success_message', "Franchise Tab Added Succesfully");
            redirect('admin/franchise_tabs');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/franchise_tabs');
        }
    }

    function update($id) {
        $_POST['updated_at'] = time();
        $_POST['image'] = $this->image_upload('image', 'franchise_tabs');
        if (empty($_POST['image'])) {
            unset($_POST['image']);
        }
        $update = $this->my_model->update_data("franchise_tabs", array("id" => $id), $_POST);
        if ($update) {
            $this->session->set_flashdata('success_message', "Franchise Tab Updated Succesfully");
            redirect('admin/franchise_tabs');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/franchise_tabs');
        }
    }

}
