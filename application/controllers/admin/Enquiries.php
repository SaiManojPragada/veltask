<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Enquiries
 *
 * @author Admin
 */
class Enquiries extends MY_Controller {

    //put your code here

    public $data;

    function __construct() {

        parent::__construct();

        if ($this->session->userdata('admin_login')['logged_in'] != true) {

            redirect('admin/login');
        }


        $this->load->model('admin_model');
    }

    function index() {
        $this->data['page_name'] = 'enquiries';
        $this->db->order_by("id", "desc");
        $this->data['enquiries'] = $this->my_model->get_data("enquiries");
        foreach ($this->data['enquiries'] as $item) {
            $item->user_details = $this->my_model->get_data_row("users", array("id" => $item->user_id));
        }
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/enquiries', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function delete($id) {
        $delete = $this->my_model->delete_data("enquiries", array("id" => $id));
        if ($delete) {
            $this->session->set_flashdata('success_message', 'Enquiry has been deleted');
        } else {
            $this->session->set_flashdata('error_message', 'Unable to delete the enquiry');
        }
        redirect(base_url('admin/enquiries'));
    }

}
