<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Bid_orders extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            //$this->session->set_flashdata('error', 'Session Timed Out');
            redirect('admin/login');
        }
        $this->load->model("admin_model");
    }

    function index() {
        $data['page_name'] = 'bidorders';
        $qry = $this->db->query("select * from user_bids where admin_assigned_status=0 order by id desc");
        $data['bids'] = $qry->result();
        
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/bidorders', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function assign_vendors($bid)
    {
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/bidorders', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function assign_bid_to_vendor()
    {
        $bidid = $this->input->get_post('bidid');
        $vendors = $this->input->get_post('vendors');
        $admin_commission = $this->input->get_post('admin_commission');
        foreach ($vendors as $value) 
        {
            //print_r($value); 
            $inss = $this->db->insert("vendor_bids",array('vendor_id'=>$value,'bid_id'=>$bidid,'admin_commission'=>$admin_commission,'created_date'=>date('Y-m-d H:i:i')));
            $title = "New Order BID";
            $message = "You Have new BID";
            $this->onesignalnotification($value,$title,$message);
        }
        if($inss)
        {
            $this->session->set_flashdata('success_message', 'Bid Assigned to vendor');
            redirect('admin/bid_orders');
        }
        //print_r($vendors); 
       
    }


    function onesignalnotification($vendor_id,$title,$message)
    {
        $qr = $this->db->query("select * from vendor_shop where id='".$vendor_id."'");
        $res = $qr->row();
            if($res->token!='')
            {
                       
                        $user_id = $res->token;
                        
                        $fields = array(
        'app_id' => '060fc2b8-3d44-4c82-b285-1d3f058f3e2b',
        'include_player_ids' => [$user_id],
        'contents' => array("en" =>$message),
        'headings' => array("en"=>$title),
        'android_channel_id' => '0646f8b2-a151-443f-ac3a-547061134d54'
    );
    
    $fields = json_encode($fields);
    //print("\nJSON sent:\n");
    //print($fields);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json; charset=utf-8', 
        'Authorization: Basic NzhjMmI5YjItZmViMy00YjNlLWFlMDItY2ZiZTI3OTY0YzYz'
    ));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                                           
    $response = curl_exec($ch);
    curl_close($ch);
    //print_r($response); die;

                  
            }  
    }


    function vendors_bids($bid)
    {
        $data['page_name'] = '';
        $qry = $this->db->query("select * from vendor_bids where bid_id='".$bid."' order by id desc");
        $data['bids_list'] = $qry->result();
        
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/assign_bid_to_vendors', $this->data);
        $this->load->view('admin/includes/footer');
    }

   
    function assignbids($bid)
    {
        $data['page_name'] = '';
        $data['bid'] =$bid;
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/assignbids', $this->data);
        $this->load->view('admin/includes/footer');
    }




}
