<?php



defined('BASEPATH') OR exit('No direct script access allowed');



class Cashcoupons extends CI_Controller {



    public $data;



    function __construct() {

        parent::__construct();
 if ($this->session->userdata('admin_login')['logged_in'] != true) {
            //$this->session->set_flashdata('error', 'Session Timed Out');
            redirect('admin/login');
        }
        $this->load->model("admin_model");

    }



    function index() {
        $this->data['page_name'] = 'cashcoupons';

        $qry = $this->db->query("select * from cash_coupons");
        $this->data['coupons'] = $qry->result();
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/cashcoupons', $this->data);
        $this->load->view('admin/includes/footer');
    }



    function add() {
        $this->data['page_name'] = 'coupons';
        $this->data['title'] = 'Add Cash Coupon';

        $this->load->view('admin/includes/header', $this->data);

        $this->load->view('admin/addcashcoupons', $this->data);

        $this->load->view('admin/includes/footer');

    }



    function insert() {
        $data = array(
            'user_id' => $this->input->post('user_id'),
            'coupon_code' => $this->input->post('coupon_code'),
            'start_date' => date("Y-m-d",strtotime($this->input->post('start_date'))),
            'expiry_date' => date("Y-m-d",strtotime($this->input->post('expiry_date'))),
            'amount' => $this->input->post('amount'),
            'description' => $this->input->post('description')
        );

        $insert_query = $this->db->insert('cash_coupons', $data);
        if ($insert_query) {
            $this->session->set_flashdata('success_message', 'Cash Coupon added Successfully');
            redirect('admin/cashcoupons');
            die();
        } else {
            redirect('admin/cashcoupons/add');
            die();
        }

    }



    function edit($id) {
        $this->data['page_name'] = 'coupons';
        $this->data['title'] = 'Edit Cash Coupon';

        $data['coupons'] = $this->db->get_where('cash_coupons', ['id' => $id])->row();

        $this->load->view('admin/includes/header');

        $this->load->view('admin/editcashcoupon', $data);

        $this->load->view('admin/includes/footer');

    }



    function update() {
        $id=$this->input->post('cid');

        $data = array(
            'user_id' => $this->input->post('user_id'),
            'coupon_code' => $this->input->post('coupon_code'),
            'start_date' => date("Y-m-d",strtotime($this->input->post('start_date'))),
            'expiry_date' => date("Y-m-d",strtotime($this->input->post('expiry_date'))),
            'amount' => $this->input->post('amount'),
            'description' => $this->input->post('description')
        );

        $this->db->where('id', $id);
        $update_query = $this->db->update('cash_coupons', $data);
        //echo $this->db->last_query(); die;
        if ($update_query) {
            $this->session->set_flashdata('success_message', 'Cash Coupon updated Successfully');
            redirect('admin/cashcoupons');
            die();
        } else {
            redirect('admin/cashcoupons/edit/' . $id);
            die();
        }

    }




    function delete($id) 
    {
        $this->db->where('id', $id);
        if ($this->db->delete('coupon_codes')) {
                $this->session->set_flashdata('success_message', 'Coupon Deleted Successfully');
                redirect('admin/coupons');
         } 
         else 
         {
                $this->session->set_flashdata('error_message', 'Unable to delete');
                redirect('admin/coupons');
         }
    }



}

