<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            //$this->session->set_flashdata('error', 'Session Timed Out');
            redirect('admin/login');
        }
    }

    function index() {
        $data['page_name'] = 'users';
        $data['users'] = $this->my_model->get_data("users", null, 'id', 'desc');
        foreach ($data['users'] as $item) {
            $item->has_bussiness_profile = false;
            $check = $this->my_model->get_data_row("users_bussiness_details", array("user_id" => $item->id));
            if (!empty($check)) {
                $item->has_bussiness_profile = true;
            }
        }

        $this->load->view('admin/includes/header', $data);

        $this->load->view('admin/users', $data);
        $this->load->view('admin/includes/footer');
    }

    function delete($user_id) {
        $this->db->where('id', $user_id);
        $del = $this->db->delete('users');

        //echo$del = $this->db->last_query(); die;
        if ($del) {
            $this->session->set_flashdata('success_message', 'User Deleted Successfully');
            redirect('admin/users');
        } else {
            $this->session->set_flashdata('error_message', 'Something went wrong, Unable to delete');
            redirect('admin/users');
        }
    }

    function view($user_id) {
        $data['page_name'] = 'users';
        $qry = $this->db->query("select * from users where id='" . $user_id . "'");
        $data['users'] = $qry->row();
        $this->db->where("user_id", $user_id);
        $this->db->order_by("id", "desc");
        $data['service_orders'] = $this->my_model->get_data("services_orders");
        foreach ($data['service_orders'] as $item) {
            $item->customer_details = $this->my_model->get_data_row("users", array("id" => $item->user_id));
            $customer_address = $this->my_model->get_data_row("user_address", array("id" => $item->user_addresses_id));
            $customer_address->city_name = $this->my_model->get_data_row("cities", array("id" => $customer_address->city))->city_name;
            $customer_address->state_name = $this->my_model->get_data_row("states", array("id" => $customer_address->state))->state_name;
            $customer_address->pincode = $this->my_model->get_data_row("pincodes", array("id" => $customer_address->pincode))->pincode;
            $item->customer_address = $customer_address;
            $item->selected_categories = $this->my_model->get_data("services_categories", "id IN ($item->ordered_categories)");
        }
        $this->load->view('admin/includes/header', $data);

        $this->load->view('admin/viewusers', $data);
        $this->load->view('admin/includes/footer');
    }

    function view_bussiness_profile($id) {
        $data['page_name'] = 'users';
        $data['business_profile'] = $this->my_model->get_data_row("users_bussiness_details", array("user_id" => $id));
//        print_r($data['business_profile']);
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/view_user_business_profile', $data);
        $this->load->view('admin/includes/footer');
    }

    function view_user_membership_details($id) {
        $data['page_name'] = 'users';
        $data['user_data'] = $this->check_user_id($id);
        $data['membership_transactions'] = $this->my_model->get_data("membership_transactions", array("user_id" => $id));
        foreach ($data['membership_transactions'] as $item) {
            $item->membership_details = $this->my_model->get_data_row("memberships", array("id" => $item->membership_id));
        }
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/view_user_membership_transactions', $data);
        $this->load->view('admin/includes/footer');
    }

}
