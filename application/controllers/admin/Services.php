<?php

class Services extends MY_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            redirect('admin/login');
        }
    }

    public function index() {
        $this->data['page_name'] = 'services';
        $this->data['title'] = 'Services';
        $this->db->order_by('priority', 'asc');
        $this->data['services'] = $this->db->get('services')->result();
        foreach ($this->data['services'] as $item) {
            $this->db->where("id", $item->cat_id);
            $item->category_name = $this->db->get("services_categories")->row()->name;
            $this->db->where("id", $item->sub_cat_id);
            $item->sub_category_name = $this->db->get("services_sub_categories")->row()->sub_category_name;
        }

        $this->load->view('admin/includes/header', $this->data);

        $this->load->view('admin/services', $this->data);

        $this->load->view('admin/includes/footer');
    }

    function add() {

        $this->data['page_name'] = 'services';
        $this->data['func'] = "insert";
        $this->data['title'] = 'Add Services';
        $this->data['data'] = array();
        $this->data['time_slots'] = $this->db->get('services_time_slots')->result();
        $this->data['categories'] = $this->db->get('services_categories')->result();
        $this->data['sub_categories'] = $this->db->get('services_sub_categories')->result();
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_service', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function edit($id) {
        $this->data['func'] = "update/";
        $this->data['title'] = 'Update Services Category';
        $this->data['data'] = $this->my_model->get_data_row("services", array("id" => $id));
        $this->data['time_slots'] = $this->db->get('services_time_slots')->result();
        $this->data['categories'] = $this->db->get('services_categories')->result();
        $this->data['sub_categories'] = $this->db->get('services_sub_categories')->result();
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_service', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function delete($id) {
        $delete = $this->my_model->delete_data("services", array("id" => $id));
        if ($delete) {
            $this->session->set_flashdata('success_message', "Service Deleted Succesfully");
            redirect('admin/services');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/services');
        }
    }

    function insert() {
        $_POST['created_at'] = time();
        $_POST['updated_at'] = time();
        $_POST['time_slots'] = implode(',', $this->input->post('time_slots'));
        $_POST['seo_url'] = make_seo_name($this->input->post('service_name'));
        $insert = $this->my_model->insert_data("services", $_POST);
        if ($insert) {
            $this->session->set_flashdata('success_message', "Service Added Succesfully");
            redirect('admin/services');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/services');
        }
    }

    function update($id) {
        $_POST['updated_at'] = time();
        $_POST['time_slots'] = implode(',', $this->input->post('time_slots'));
        $_POST['seo_url'] = make_seo_name($this->input->post('service_name'));
        if ($this->input->post("has_visit_and_quote") == 1) {
            $_POST['price'] = $this->input->post("visiting_charges");
            $_POST['sale_price'] = $this->input->post("visiting_charges");
        }
        $update = $this->my_model->update_data("services", array("id" => $id), $_POST);
        if ($update) {
            $this->session->set_flashdata('success_message', "Service Updated Succesfully");
            redirect('admin/services');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/services');
        }
    }

    function images($id) {
        if (isset($_POST['submit'])) {
            $ups = $this->multiple_images_upload("images", "services", $id);
            if (empty($ups)) {
                $this->session->set_flashdata('error_message', "No Images Selected");
                redirect('admin/services/images/' . $id);
            }
            $insert = $this->my_model->insert_multi_data("services_images", $ups);
            if ($insert) {
                $this->session->set_flashdata('success_message', "Images Uploaded Succesfully");
                redirect('admin/services/images/' . $id);
            } else {
                $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
                redirect('admin/services/images/' . $id);
            }
        }


        $this->db->where("id", $id);
        $this->data['service'] = $this->db->get("services")->row();
        $this->data['title'] = 'Manage Services Images';
        $this->db->where("service_id", $id);
        $this->data['images'] = $this->db->get("services_images")->result();
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/service_images', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function image_delete() {
        $del = $this->my_model->delete_data("services_images", array(
            "id" => $this->input->post("id")
                ), 'uploads/services/' . $this->input->post('file'));
        if ($del) {
            $this->session->set_flashdata('success_message', "Image deleted Succesfully");
        } else {
            $this->session->set_flashdata('error_message', "Unable to Image Succesfully");
        }
    }

    function manage_reviews($id) {
        $service_name = $this->my_model->get_data_row("services", array("id" => $id))->service_name;
        $reviews = $this->my_model->get_data("services_reviews", array("service_id" => $id));
        foreach ($reviews as $rev) {
            $rev->user_data = $this->my_model->get_data_row("users", array("id" => $rev->user_id));
        }
        $this->data['reviews'] = $reviews;
        $this->data['service_name'] = $service_name;
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/service_reviews', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function disable($id) {
        $disable = $this->my_model->update_data("services_reviews", array("id" => $id), array("status" => 0));
        if ($disable) {
            $this->session->set_flashdata("success_message", "Review Disabled");
        } else {
            $this->session->set_flashdata("error_message", "Unable to Disable the Review");
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    function enable($id) {
        $enable = $this->my_model->update_data("services_reviews", array("id" => $id), array("status" => 1));
        if ($enable) {
            $this->session->set_flashdata("success_message", "Review Enabled");
        } else {
            $this->session->set_flashdata("error_message", "Unable to Enable the Review");
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    function get_sub_categories() {
        $cat_id = $this->input->post("cat_id");
        $data = $this->my_model->get_data("services_sub_categories", array("cat_id" => $cat_id));
        $op = "<option>select sub categories</option>";
        foreach ($data as $o) {
            $op .= "<option value='$o->id'>$o->sub_category_name</option>";
        }
        echo $op;
    }

}
