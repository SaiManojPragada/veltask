<?php

class Services_categories extends MY_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            redirect('admin/login');
        }
    }

    public function index() {
        $this->data['page_name'] = 'services_categories';
        $this->data['title'] = 'Services Categories';
        $this->db->order_by('priority', 'asc');
        $this->data['categories'] = $this->db->get('services_categories')->result();

        foreach ($this->data['categories'] as $item) {
            $subs = $this->my_model->get_data("services_sub_categories", array("cat_id" => $item->id));
            $item->sub_categories = ($subs) ? $subs : array();
        }

        $this->load->view('admin/includes/header', $this->data);

        $this->load->view('admin/services_categories', $this->data);

        $this->load->view('admin/includes/footer');
    }

    function add() {

        $this->data['func'] = "insert";
        $this->data['title'] = 'Add Services Category';
        $this->data['data'] = array();
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_service_category', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function edit($id) {
        $this->data['func'] = "update/";
        $this->data['title'] = 'Update Services Category';
        $this->data['data'] = $this->my_model->get_data_row("services_categories", array("id" => $id));
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_service_category', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function delete($id) {
        $delete = $this->my_model->delete_data("services_categories", array("id" => $id));
        if ($delete) {
            $this->session->set_flashdata('success_message', "Service Category Deleted Succesfully");
            redirect('admin/services_categories');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/services_categories');
        }
    }

    function insert() {
        $_POST["app_image"] = $this->image_upload("app_image", "services_categories");
        $_POST["web_image"] = $this->image_upload("web_image", "services_categories");
        $_POST['created_at'] = time();
        $_POST['updated_at'] = time();
        $_POST['seo_url'] = make_seo_name($this->input->post('name'));
        $_POST['priority'] = make_seo_name($this->input->post('priority'));
        $insert = $this->my_model->insert_data("services_categories", $_POST);
        if ($insert) {
            $this->session->set_flashdata('success_message', "Service Category Added Succesfully");
            redirect('admin/services_categories');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/services_categories');
        }
    }

    function update($id) {
        $_POST["app_image"] = $this->image_upload("app_image", "services_categories");
        $_POST["web_image"] = $this->image_upload("web_image", "services_categories");
        $_POST['updated_at'] = time();
        $_POST['seo_url'] = make_seo_name($this->input->post('name'));
        $_POST['priority'] = make_seo_name($this->input->post('priority'));
        if (empty($_POST["app_image"])) {
            unset($_POST["app_image"]);
        }
        if (empty($_POST["web_image"])) {
            unset($_POST["web_image"]);
        }
        $update = $this->my_model->update_data("services_categories", array("id" => $id), $_POST);
        if ($update) {
            $this->session->set_flashdata('success_message', "Service Category Updated Succesfully");
            redirect('admin/services_categories');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/services_categories');
        }
    }

}
