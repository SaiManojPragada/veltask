<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Services_orders
 *
 * @author Admin
 */
class Franchise_services_orders extends MY_Controller {

    //put your code here
    public $data;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            redirect('admin/login');
        }
        if ($_SESSION['admin_login']['user_type'] != 'franchise') {
            redirect('admin/login');
        }
    }

    public function index() {
        $this->data['page_name'] = 'services_orders';
        $this->data['title'] = 'Services Orders';
        $session_data = $this->session->userdata('admin_login');
        $this->db->order_by("id", "desc");
        $this->db->where(array("accepted_franchise_id" => $session_data['id'], "order_status" => "order_completed"));
        $this->data['data'] = $this->my_model->get_data("services_orders");
        foreach ($this->data['data'] as $item) {
            $item->customer_details = $this->my_model->get_data_row("users", array("id" => $item->user_id));
            $customer_address = $this->my_model->get_data_row("user_address", array("id" => $item->user_addresses_id));
            $customer_address->city_name = $this->my_model->get_data_row("cities", array("id" => $customer_address->city))->city_name;
            $customer_address->state_name = $this->my_model->get_data_row("states", array("id" => $customer_address->state))->state_name;
            $customer_address->pincode = $this->my_model->get_data_row("pincodes", array("id" => $customer_address->pincode))->pincode;
            $item->customer_address = $customer_address;
            $item->selected_categories = $this->my_model->get_data("services_categories", "id IN ($item->ordered_categories)");
        }

        $this->load->view('admin/includes/header', $this->data);

        $this->load->view('admin/services_orders', $this->data);

        $this->load->view('admin/includes/footer');
    }

    function view($id) {
        $this->data['page_name'] = 'services_orders';
        $this->data['title'] = 'Services Orders';
        $data = $this->my_model->get_data_row("services_orders", array("id" => $id));
        $data->customer_details = $this->my_model->get_data_row("users", array("id" => $data->user_id));
        $customer_address = $this->my_model->get_data_row("user_address", array("id" => $data->user_addresses_id));
        $customer_address->city_name = $this->my_model->get_data_row("cities", array("id" => $customer_address->city))->city_name;
        $customer_address->state_name = $this->my_model->get_data_row("states", array("id" => $customer_address->state))->state_name;
        $customer_address->pincode = $this->my_model->get_data_row("pincodes", array("id" => $customer_address->pincode))->pincode;
        $data->customer_address = $customer_address;
        $data->selected_categories = $this->my_model->get_data("services_categories", "id IN (" . $data->ordered_categories . ")");
        $data->service_orders_items = $this->my_model->get_data("service_orders_items", array("order_id" => $data->order_id));
        foreach ($data->service_orders_items as $prod) {
            $prod->service_details = $this->my_model->get_data_row("services", array("id" => $prod->service_id));
        }
        if (!empty($data->accepted_by_service_provider)) {
            $data->service_provider_details = $this->my_model->get_data_row("service_providers", array('id' => $data->accepted_by_service_provider));
        }

        $this->data['data'] = $data;

        $this->load->view('admin/includes/header', $this->data);

        $this->load->view('admin/services_order_view', $this->data);

        $this->load->view('admin/includes/footer');
    }

}
