<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Franchise_services_withdraw_requests extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            redirect('admin/login');
        }
        if ($_SESSION['admin_login']['user_type'] != 'franchise') {
            redirect('admin/login');
        }
        $this->load->model('vendor_services');
    }

    function index() {
        $this->data['page_name'] = 'franchise_services_withdraw_requests';
        $session_data = $this->session->userdata('admin_login');
        $user_id = $session_data['id'];

        $wallet = $this->my_model->get_data_row("franchises_services_payments", array("franchise_id" => $user_id));
        $total_earnings = $wallet->total_amount;
        $total_withdrawls = $wallet->total_requested_amount;
        $this->db->select("SUM(requested_amount) as total_onhold_amount");
        $onhold_amount = $this->my_model->get_data_row("franchise_services_withdraw_request", array("franchise_id" => $user_id, "status" => 0))->total_onhold_amount;
        $current_balance = ($total_earnings - $total_withdrawls) - $onhold_amount;
        $this->data['onhold_amount'] = ($onhold_amount) ? $onhold_amount : 0;
        $this->data['current_balance'] = $current_balance;
        $this->data['total_withdrawls'] = ($total_withdrawls) ? $total_withdrawls : 0;
        $this->data['total_earnings'] = $total_earnings;
        $this->data['vendor_id'] = $user_id;
        $this->db->order_by("id", "desc");
        $this->data['data'] = $this->my_model->get_data("franchise_services_withdraw_request", array("franchise_id" => $user_id));
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/franchise_services_withdraw_requests', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function request() {
        $session_data = $this->session->userdata('admin_login');
        $user_id = $session_data['id'];
        $check_bank = $this->my_model->get_data_row("franchise_bank_details", array("franchise_id" => $user_id));
        if (empty($check_bank)) {
            $this->session->set_flashdata('error_message', 'No Bank Details Found.');
            redirect(base_url('admin/franchise_services_withdraw_requests'));
            die;
        }
        $requested_amount = $this->input->post("requested_amount");
        $description = $this->input->post("description");
        $wallet = $this->my_model->get_data_row("franchises_services_payments", array("franchise_id" => $user_id));
        $total_earnings = $wallet->total_amount;
        $total_withdrawls = $wallet->total_requested_amount;
        $this->db->select("SUM(requested_amount) as total_onhold_amount");
        $onhold_amount = $this->my_model->get_data_row("franchise_services_withdraw_request", array("franchise_id" => $user_id, "status" => 0))->total_onhold_amount;
        $current_balance = ($total_earnings - $total_withdrawls) - $onhold_amount;
        if ($requested_amount > $current_balance) {
            $this->session->set_flashdata('error_message', 'Insufficient Balance.');
            redirect(base_url('admin/franchise_services_withdraw_requests'));
            die;
        }
        $inp_data = array(
            "franchise_id" => $user_id,
            "requested_amount" => $requested_amount,
            "description" => $description,
            "created_at" => time(),
            "status" => 0
        );
        $insert = $this->my_model->insert_data("franchise_services_withdraw_request", $inp_data);
        if ($insert) {
            $this->session->set_flashdata('success_message', 'Withdrawl request succesfully sent.');
            redirect(base_url('admin/franchise_services_withdraw_requests'));
            die;
        } else {
            $this->session->set_flashdata('error_message', 'Unable to send Withdrawl request.');
            redirect(base_url('admin/franchise_services_withdraw_requests'));
            die;
        }
    }

}
