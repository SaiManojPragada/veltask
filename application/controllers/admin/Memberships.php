<?php

class Memberships extends MY_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            redirect('admin/login');
        }
    }

    public function index() {
        $this->data['page_name'] = 'memberships';
        $this->data['title'] = 'Memberships';

        $this->data['data'] = $this->my_model->get_data("memberships");

        foreach ($this->data['data'] as $item) {
            $dt = $this->my_model->get_data_row("term_months", array("id" => $item->duration_id));
            $item->duration_in_days = $dt->duration;
            $item->duration_in_words = $dt->duration_in_words;
        }


        $this->load->view('admin/includes/header', $this->data);

        $this->load->view('admin/memberships', $this->data);

        $this->load->view('admin/includes/footer');
    }

    function add() {

        $this->data['page_name'] = 'memberships';
        $this->data['func'] = "insert";
        $this->data['title'] = 'Add Membership';
        $this->data['data'] = array();
        $this->data['terms'] = $this->my_model->get_data("term_months");
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_membership', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function edit($id) {
        $this->data['func'] = "update/";
        $this->data['title'] = 'Update Services Category';
        $this->data['data'] = $this->my_model->get_data_row("memberships", array("id" => $id));
        $this->data['terms'] = $this->my_model->get_data("term_months");
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_membership', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function delete($id) {
        $delete = $this->my_model->delete_data("services", array("id" => $id));
        if ($delete) {
            $this->session->set_flashdata('success_message', "Membership Deleted Succesfully");
            redirect('admin/memberships');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/memberships');
        }
    }

    function insert() {
        $_POST['created_at'] = time();
        $_POST['updated_at'] = time();
        $insert = $this->my_model->insert_data("memberships", $_POST);
        if ($insert) {
            $this->session->set_flashdata('success_message', "Membership Added Succesfully");
            redirect('admin/memberships');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/memberships');
        }
    }

    function update($id) {
        $_POST['updated_at'] = time();
        $update = $this->my_model->update_data("memberships", array("id" => $id), $_POST);
        if ($update) {
            $this->session->set_flashdata('success_message', "Membership Updated Succesfully");
            redirect('admin/memberships');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/memberships');
        }
    }

}
