<?php

class Services_banners extends MY_Controller {

    function __construct() {

        parent::__construct();

        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            redirect('admin/login');
        }
    }

    function index() {
        $this->data['page_name'] = 'services_banners';
        $this->data['title'] = 'Services Banners';

        $this->data['data'] = $this->my_model->get_data("services_banners");
        foreach ($this->data['data'] as $item) {
            $item->cities_id = $this->my_model->get_data_row("cities", array("id" => $item->cities_id))->city_name;
        }

        $this->load->view('admin/includes/header', $this->data);

        $this->load->view('admin/services_banners', $this->data);

        $this->load->view('admin/includes/footer');
    }

    function add() {

        $this->data['page_name'] = 'services_banners';
        $this->data['func'] = "insert";
        $this->data['title'] = 'Add Services Banners';
        $this->data['data'] = array();
        $this->data['cities'] = $this->db->get('cities')->result();
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_service_banner', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function edit($id) {
        $this->data['func'] = "update/";
        $this->data['title'] = 'Update Services Banners';
        $this->data['data'] = $this->my_model->get_data_row("services_banners", array("id" => $id));
        $this->data['cities'] = $this->db->get('cities')->result();
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_service_banner', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function insert() {
        $_POST["app_image"] = $this->image_upload("app_image", "services_banners");
        $_POST["web_image"] = $this->image_upload("web_image", "services_banners");
        $_POST['created_at'] = time();
        $_POST['updated_at'] = time();
        $insert = $this->my_model->insert_data("services_banners", $_POST);
        if ($insert) {
            $this->session->set_flashdata('success_message', "Service Banner Added Succesfully");
            redirect('admin/services_banners');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/services_banners');
        }
    }

    function update($id) {
        $_POST["app_image"] = $this->image_upload("app_image", "services_banners");
        $_POST["web_image"] = $this->image_upload("web_image", "services_banners");
        $_POST['updated_at'] = time();
        if (empty($_POST["app_image"])) {
            unset($_POST["app_image"]);
        }
        if (empty($_POST["web_image"])) {
            unset($_POST["web_image"]);
        }
        $update = $this->my_model->update_data("services_banners", array("id" => $id), $_POST);
        if ($update) {
            $this->session->set_flashdata('success_message', "Service Banner Updated Succesfully");
            redirect('admin/services_banners');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/services_banners');
        }
    }

    function delete($id) {
        $delete = $this->my_model->delete_data("services_banners", array("id" => $id));
        if ($delete) {
            $this->session->set_flashdata('success_message', "Service Banner Deleted Succesfully");
            redirect('admin/services_banners');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/services_banners');
        }
    }

}
