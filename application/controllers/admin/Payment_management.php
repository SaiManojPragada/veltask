<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_management extends MY_Controller {

    public $data;

    function __construct() {

        parent::__construct();

        if ($this->session->userdata('admin_login')['logged_in'] != true) {

//$this->session->set_flashdata('error', 'Session Timed Out');

            redirect('admin/login');
        }


        $this->load->model('admin_model');
    }

    function index() {
        $this->data['page_name'] = 'payments_management';
        $this->data['title'] = 'Payments Managements';
        $data['payments'] = $this->my_model->get_data("payment_methods_enables");
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/payment_management', $data);
        $this->load->view('admin/includes/footer');
    }

    function edit($id) {
        $this->data['page_name'] = 'payments_management';
        $this->data['title'] = 'Edit Management';
        $this->data['payment'] = $this->my_model->get_data_row("payment_methods_enables", array("id" => $id));
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/edit_payment_management', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function update($id) {
        $_POST['updated_at'] = time();
        $update = $this->admin_model->update_data("payment_methods_enables", $this->input->post(), array("id" => $id));
        if ($update) {
            $this->session->set_flashdata('success_message', 'Payment Method Updated Successfully.');
            redirect(base_url('admin/payment_management'));
        } else {
            $this->session->set_flashdata('error_message', 'Failed to Update Payment Method.');
            redirect(base_url('admin/payment_management'));
        }
    }

}
