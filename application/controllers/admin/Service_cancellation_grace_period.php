<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Time_slot_difference
 *
 * @author Admin
 */
class Service_cancellation_grace_period extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            redirect('admin/login');
        }
    }

    public function index() {
        $this->data['page_name'] = 'service_cancellation_grace_period';
        $this->data['service_cancellation_grace_period'] = $this->my_model->get_data_row("service_cancellation_grace_period");
        $this->data['title'] = 'Manage Cancellation Grace Period For Services';
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/service_cancellation_grace_period', $this->data);
        $this->load->view('admin/includes/footer');
    }

    public function update() {
        $data = array(
            "duration" => $this->input->post("duration"),
            "updated_at" => time()
        );
        $check = $this->my_model->get_data_row("service_cancellation_grace_period");
        if (!empty($check)) {
            $ins = $this->my_model->update_data("service_cancellation_grace_period", array("id" => $check->id), $data);
        } else {
            $ins = $this->my_model->insert_data("service_cancellation_grace_period", $data);
        }

        if ($ins) {
            $this->session->set_flashdata('success_message', "Cancellation grace Period has been Updated");
            redirect('admin/service_cancellation_grace_period');
        } else {
            $this->session->set_flashdata('error_message', "Failed to Update Cancellation grace Period");
            redirect('admin/service_cancellation_grace_period');
        }
    }

}
