<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Business_document_types extends MY_Controller {

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            redirect('admin/login');
        }
    }

    function index() {
        $this->data['page_name'] = 'business_document_types';
        $this->data['title'] = 'Manage Business Document Types';
        $this->data['types'] = $this->db->get('business_document_types')->result();
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/business_document_types', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function add() {
        $this->data['page_name'] = 'business_document_types';
        $this->data['title'] = 'Add Business Document Types';
        $this->data['func'] = 'insert';
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_business_document_types', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function insert() {
        $_POST['created_at'] = time();
        $_POST['updated_at'] = time();
        $insert = $this->my_model->insert_data('business_document_types', $this->input->post());
        if ($insert) {
            $this->session->set_flashdata('success_message', 'New Document Type Added');
        } else {
            $this->session->set_flashdata('error_message', 'Unable to add new Document Type');
        }
        redirect(base_url() . 'admin/business_document_types');
    }

    function edit($id) {
        $this->data['page_name'] = 'business_document_types';
        $this->data['title'] = 'Update Business Document Types';
        $this->data['func'] = 'update/' . $id;
        $this->data['prev_data'] = $this->my_model->get_data_row("business_document_types", array("id" => $id));
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_business_document_types', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function update($id) {
        $_POST['updated_at'] = time();
        $update = $this->my_model->update_data('business_document_types', array("id" => $id), $this->input->post());
        if ($update) {
            $this->session->set_flashdata('success_message', 'Document Type Updated');
        } else {
            $this->session->set_flashdata('error_message', 'Unable to update Document Type');
        }
        redirect(base_url() . 'admin/business_document_types');
    }

    function delete($id) {
        $delete = $this->my_model->delete_data("business_document_types", array("id" => $id));
        if ($delete) {
            $this->session->set_flashdata('success_message', 'Document Type Deleted');
        } else {
            $this->session->set_flashdata('error_message', 'Unable to delete new Document Type');
        }
        redirect(base_url() . 'admin/business_document_types');
    }

}
