<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Services_orders
 *
 * @author Admin
 */
class Services_orders extends MY_Controller {

    //put your code here
    public $data;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            redirect('admin/login');
        }
    }

    public function index() {
        $this->data['page_name'] = 'services_orders';
        $this->data['title'] = 'Services Orders';
        $filter = $this->input->get("stat");
        if (!empty($filter)) {
            if ($filter == "ongoing") {
                $this->db->where("order_status='order_placed'");
            } else if ($filter == "accepted") {
                $this->db->where("order_status='order_accepted' || order_status='order_started'");
            } else if ($filter == "completed") {
                $this->db->where("order_status='order_completed'");
            } elseif ($filter == "cancelled") {
                $this->db->where("order_status='order_rejected' || order_status='order_cancelled' || order_status='refunded'");
            }
        }

        if (!empty($this->input->get('from_date'))) {
            $from_date = strtotime($this->input->get('from_date'));
            $this->db->where("created_at >=", $from_date);
        }

        if (!empty($this->input->get('to_date'))) {
            $to_date = strtotime($this->input->get('to_date'));
            $this->db->where("created_at <=", strtotime("+1 day", $to_date));
        }

        if (!empty($this->input->get('time_slot_date'))) {
            $this->db->where("time_slot_date", $this->input->get('time_slot_date'));
        }


        if (!empty($this->input->get('visit_and_quote'))) {
            $this->db->where("has_visit_and_quote", $this->input->get('visit_and_quote'));
        }
        if ($this->input->get('missed') == 'yes' && !empty($this->input->get('time_slot_date'))) {
            $this->db->where("time_slot_date < '" . $this->input->get('time_slot_date') . "' AND order_status='order_placed'");
            $this->session->set_flashdata('success_message', "All Missed Orders");
        }

        $this->db->order_by("id", "desc");
        $this->data['data'] = $this->my_model->get_data("services_orders");
//        echo $this->db->last_query();
        $this->db->select("count(id) as total_count");
        $this->data['inprogress_count'] = $this->my_model->get_data_row("services_orders", "order_status='order_placed' OR order_status='order_accepted' OR order_status='order_started'")->total_count;
        $this->db->select("count(id) as total_count");
        $this->data['completed_count'] = $this->my_model->get_data_row("services_orders", array("order_status" => "order_completed"))->total_count;
        $this->db->select("sum(grand_total) as total_sale, sum(admin_comission) as admin_earnings, sum(franchise_comission) as franchise_earnings, sum(service_provider_comission) as provider_earnings, sum(tax) as tax_earnings");
        $this->data['earnings'] = $this->my_model->get_data_row("services_orders", array("order_status" => "order_completed"));
        foreach ($this->data['data'] as $item) {
            $item->customer_details = $this->my_model->get_data_row("users", array("id" => $item->user_id));
            $customer_address = $this->my_model->get_data_row("user_address", array("id" => $item->user_addresses_id));
            $customer_address->city_name = $this->my_model->get_data_row("cities", array("id" => $customer_address->city))->city_name;
            $customer_address->state_name = $this->my_model->get_data_row("states", array("id" => $customer_address->state))->state_name;
            $customer_address->pincode = $this->my_model->get_data_row("pincodes", array("id" => $customer_address->pincode))->pincode;
            $item->customer_address = $customer_address;
            $item->selected_categories = $this->my_model->get_data("services_categories", "id IN ($item->ordered_categories)");
        }
        $this->data['total_orders'] = sizeof($this->my_model->get_data("services_orders"));
        $this->data['today_orders'] = sizeof($this->my_model->get_data("services_orders", "created_at >= " . strtotime(date('Y-m-d'))));
        $this->data['today_scheduled_orders'] = sizeof($this->my_model->get_data("services_orders", "time_slot_date = '" . date('Y-m-d') . "'"));
        $this->data['total_visit_and_quote_orders'] = sizeof($this->my_model->get_data("services_orders", array("has_visit_and_quote" => 1)));
        $this->data['total_normal_orders'] = sizeof($this->my_model->get_data("services_orders", array("has_visit_and_quote" => 0)));
        $this->db->group_by("service_order_id");
        $this->data['milestone_orders'] = sizeof($this->my_model->get_data("visit_and_quote_quotaions"));
        $this->db->where("time_slot_date <'" . date('Y-m-d') . "'");
        $this->data['missed_service_orders'] = sizeof($this->my_model->get_data("services_orders", array("order_status" => "order_placed")));
        $this->load->view('admin/includes/header', $this->data);

        $this->load->view('admin/services_orders', $this->data);

        $this->load->view('admin/includes/footer');
    }

    function view($id) {
        $this->data['page_name'] = 'services_orders';
        $this->data['title'] = 'Services Orders';
        $data = $this->my_model->get_data_row("services_orders", array("id" => $id));
        $data->customer_details = $this->my_model->get_data_row("users", array("id" => $data->user_id));
        $customer_address = $this->my_model->get_data_row("user_address", array("id" => $data->user_addresses_id));
        $customer_address->city_name = $this->my_model->get_data_row("cities", array("id" => $customer_address->city))->city_name;
        $customer_address->state_name = $this->my_model->get_data_row("states", array("id" => $customer_address->state))->state_name;
        $pincode_id = $customer_address->pincode;
        $customer_address->pincode = $this->my_model->get_data_row("pincodes", array("id" => $customer_address->pincode))->pincode;
        $data->customer_address = $customer_address;
        $data->selected_categories = $this->my_model->get_data("services_categories", "id IN (" . $data->ordered_categories . ")");
        $data->service_orders_items = $this->my_model->get_data("service_orders_items", array("order_id" => $data->order_id));
        $quotation = $this->my_model->get_data_row("visit_and_quote_quotaions", array("service_order_id" => $id));
        if (!empty($quotation)) {
            $quotation->milestones = $this->my_model->get_data("quotation_milestones", array("quotation_id" => $quotation->id));
        }
        $this->data['quotation'] = $quotation;
        foreach ($data->service_orders_items as $prod) {
            $prod->service_details = $this->my_model->get_data_row("services", array("id" => $prod->service_id));
        }
        if (!empty($data->accepted_by_service_provider)) {
            $data->service_provider_details = $this->my_model->get_data_row("service_providers", array('id' => $data->accepted_by_service_provider));
        }

        $grand_total = $data->grand_total - ($data->coupon_discount + $data->membership_discount + $data->bussiness_discount);
        if ($data->amount_paid == $grand_total) {
            $this->data['show_payment_status'] = "<span style='color: green'>Paid</span>";
        }
        if ($data->amount_paid > 0 && $data->amount_paid < $grand_total) {
            $this->data['show_payment_status'] = "<span style='color: orange'>Partially Paid</span>";
        }
        if ($data->amount_paid == 0) {
            $this->data['show_payment_status'] = "<span style='color: red'>Unpaid</span>";
        }
        $franchise_data = $this->my_model->get_data_row("franchises", "FIND_IN_SET ('" . $pincode_id . "',pincode_ids)");
        $service_providers = $this->my_model->get_data("service_providers", array("franchise_id" => $franchise_data->id));
        $sp_list = array();
        $prvd_ids = array();
        foreach ($service_providers as $prvd) {
            $provider_pincodes = explode(",", $prvd->sp_pincodes);
            if (in_array($pincode_id, $provider_pincodes) && !in_array($prvd->id, $prvd_ids) && $prvd->id != $data->accepted_by_service_provider) {
                array_push($prvd_ids, $prvd->id);
                array_push($sp_list, $prvd);
            }
        }
        $this->data['order_provider_list'] = $sp_list;

        $this->data['data'] = $data;

        $this->load->view('admin/includes/header', $this->data);

        $this->load->view('admin/services_order_view', $this->data);

        $this->load->view('admin/includes/footer');
    }

}
