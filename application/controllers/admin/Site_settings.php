<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Site_settings extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            redirect('admin/login');
        }
    }

    function index() {
        $this->data['page_name'] = 'site_settings';
        $qry = $this->db->query("select * from site_settings where id=1");
        $data['settings'] = $qry->row();
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/site_settings', $data);
        $this->load->view('admin/includes/footer');
    }

    function update() {
        $site_name = $this->input->post('site_name');
        $logo = $this->image_upload("logo", "");
        $fav_icon = $this->image_upload("fav_icon", "");
        $footer_logo = $this->image_upload("footer_logo", "");
        $data['site_name'] = $site_name;
        if (!empty($logo)) {
            $_POST['logo'] = $logo;
        }

        if (!empty($fav_icon)) {
            $_POST['fav_icon'] = $fav_icon;
        }
        if (!empty($footer_logo)) {
            $_POST['footer_logo'] = $footer_logo;
        }
        $update = $this->my_model->update_data("site_settings", array("id" => 1), $this->input->post());
        if ($update) {
            $this->session->set_flashdata('success_message', "Site Settings Updated");
            redirect('admin/site_settings');
        } else {
            $this->session->set_flashdata('error_message', "to Update Site Settings Please try Again");
            redirect('admin/site_settings');
        }
    }

}
