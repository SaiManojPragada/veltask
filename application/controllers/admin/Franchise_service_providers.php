<?php

class Franchise_service_providers extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            redirect('admin/login');
        }
        if ($_SESSION['admin_login']['user_type'] != 'franchise') {
            redirect('admin/login');
        }
    }

    function index() {

        $session_data = $this->session->userdata('admin_login');

        $status = true;
        $this->data['page_name'] = 'service_providers';
        if (!empty($this->input->get('status'))) {
            $status = false;
            $this->data['page_name'] = 'inactive_service_provides';
        }
        $this->data['title'] = 'Service Providers';
        $this->db->where("status", $status);
        $this->db->where("owner_id", 0);
        $this->db->where("franchise_id", $session_data['id']);
        $this->db->order_by("id", "desc");
        $this->data['providers'] = $this->db->get('service_providers')->result();
        foreach ($this->data['providers'] as $item) {
            $item->comissions = $this->my_model->get_data("provider_comissions", array("providers_id" => $item->id));
            $this->db->where("id", $item->location_id);
            $item->location_name = $this->db->get("cities")->row()->city_name;
            $categories = $this->db->where('id IN (' . rtrim($item->categories_ids, ',') . ')')->get('services_categories')->result();
            foreach ($categories as $cat) {
                $item->categories_names .= $cat->name . ', ';
            }
            $item->categories_names = rtrim($item->categories_names, ', ');
        }
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/franchise_service_providers', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function add() {
        $session_data = $this->session->userdata('admin_login');
        $this->db->where("id", $session_data['id']);
        $u_data = $this->db->get("franchises")->row();
        $this->data['franc'] = $u_data;
        $this->data['func'] = "insert";
        $this->data['page_name'] = 'active_service_provides';
        $this->data['title'] = 'Add Service Provider';
        $this->db->where("id", $u_data->location_id);
        $this->data['cities'] = $this->db->get("cities")->row();
        $this->data['categories'] = $this->db->get("services_categories")->result();
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/franchise_add_service_provider', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function edit($id) {
        $session_data = $this->session->userdata('admin_login');
        $this->db->where("id", $session_data['id']);
        $u_data = $this->db->get("franchises")->row();
        $this->data['franc'] = $u_data;
        $this->data['func'] = "update/";
        $this->data['page_name'] = 'active_service_provides';
        $this->data['title'] = 'Add Service Provider';
        $this->data['data'] = $this->my_model->get_data_row("service_providers", array("id" => $id));
        $this->db->where("id", $u_data->location_id);
        $this->data['cities'] = $this->db->get("cities")->row();
        $this->data['categories'] = $this->db->get("services_categories")->result();
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/franchise_add_service_provider', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function insert() {
        $session_data = $this->session->userdata('admin_login');
        $this->db->where("id", $session_data['id']);
        $u_data = $this->db->get("franchises")->row();
        $_POST['location_id'] = $u_data->location_id;
        $_POST['franchise_id'] = $session_data['id'];
        $_POST['categories_ids'] = implode(',', $_POST['categories_ids']) . ',';
        $_POST['sp_pincodes'] = implode(',', $_POST['sp_pincodes']);
        $_POST['created_at'] = time();
        $_POST['updated_at'] = time();
        $_POST['password'] = md5($_POST['password']);
        $insert = $this->my_model->insert_data("service_providers", $_POST);
        if ($insert) {
            $this->session->set_flashdata('success_message', "Service Provider Added Succesfully");
            redirect('admin/franchise_service_providers');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/franchise_service_providers');
        }
    }

    function update($id) {
        $_POST['updated_at'] = time();
        $_POST['categories_ids'] = implode(',', $_POST['categories_ids']) . ',';
        $_POST['sp_pincodes'] = implode(',', $_POST['sp_pincodes']);
        if (empty($this->input->post("password"))) {
            unset($_POST['password']);
        } else {
            $_POST['password'] = md5($_POST['password']);
        }
        $update = $this->my_model->update_data("service_providers", array("id" => $id), $_POST);
        if ($update) {
            $this->session->set_flashdata('success_message', 'Service Provider Updated Successfully.');
            redirect(base_url('admin/franchise_service_providers'));
        } else {
            $this->session->set_flashdata('error_message', 'Failed to Update Service Provider.');
            redirect(base_url('admin/franchise_service_providers'));
        }
    }

    function delete($id) {
        $delete = $this->my_model->delete_data("service_providers", array("id" => $id));
        if ($delete) {
            $this->session->set_flashdata('success_message', "Service provider Deleted Succesfully");
            redirect('admin/franchise_service_providers');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/franchise_service_providers');
        }
    }

    function manage_categories($id, $func_status = 'add', $comm_id = "") {
        if (!empty($comm_id)) {
            $this->data['admin_edit_comissions'] = $this->my_model->get_data_row("provider_comissions", array("id" => $comm_id));
        }
        $this->data['func_status'] = $func_status;
        $this->data['page_name'] = 'active_service_provides';
        $this->data['data'] = $this->my_model->get_data_row("service_providers", array("id" => $id));
        $this->data['categories'] = $this->db->where('id IN (' . rtrim($this->data['data']->categories_ids, ',') . ')')->get('services_categories')->result();
        $this->data['comissions'] = $this->my_model->get_data("provider_comissions", array("providers_id" => $id));
        foreach ($this->data['comissions'] as $item) {
            $item->category_name = $this->my_model->get_data_row("services_categories", array("id" => $item->cat_id))->name;
        }
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/provider_manage_categories', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function insert_cat_comission() {
        $_POST['created_at'] = time();
        $_POST['updated_at'] = time();
        $insert = $this->my_model->insert_data("provider_comissions", $this->input->post());
        if ($insert) {
            $this->session->set_flashdata('success_message', "Service Provider Comission Added Succesfully");
            redirect('admin/franchise_service_providers/manage_categories/' . $this->input->post('providers_id'));
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/franchise_service_providers/manage_categories/' . $this->input->post('providers_id'));
        }
    }

    function update_cat_comission($id, $providers_id) {
        $_POST['updated_at'] = time();
        $update = $this->my_model->update_data("provider_comissions", array("id" => $id), $this->input->post());
        if ($update) {
            $this->session->set_flashdata('success_message', 'Service Provider Comission Updated Successfully.');
            redirect(base_url('admin/franchise_service_providers/manage_categories/' . $providers_id));
        } else {
            $this->session->set_flashdata('error_message', 'Failed to Update Service Provider.');
            redirect(base_url('admin/franchise_service_providers/manage_categories/' . $providers_id));
        }
    }

    function delete_comission($id, $providers_id) {
        $delete = $this->my_model->delete_data("provider_comissions", array("id" => $id, "providers_id" => $providers_id));
        if ($delete) {
            $this->session->set_flashdata('success_message', "Service provider Comission Deleted Succesfully");
            redirect('admin/franchise_service_providers/manage_categories/' . $providers_id);
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/franchise_service_providers/manage_categories/' . $providers_id);
        }
    }

    function view_technicians($id) {
        $this->db->where("id", $id);
        $this->data['owner'] = $this->db->get("service_providers")->row();
        $this->db->where("owner_id", $id);
        $this->db->order_by("id", "desc");
        $this->data['techs'] = $this->db->get("service_providers")->result();
        foreach ($this->data['techs'] as $item) {
            $this->db->where("id", $item->location_id);
            $item->location_name = $this->db->get("cities")->row()->city_name;
            $categories = $this->db->where('id IN (' . rtrim($item->categories_ids, ',') . ')')->get('services_categories')->result();
            foreach ($categories as $cat) {
                $item->categories_names .= $cat->name . ', ';
            }
            $item->categories_names = rtrim($item->categories_names, ', ');
        }
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/view_technicians', $this->data);
        $this->load->view('admin/includes/footer');
    }

}
