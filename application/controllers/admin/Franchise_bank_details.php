<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Franchise_bank_details extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            redirect('admin/login');
        }
        if ($_SESSION['admin_login']['user_type'] != 'franchise') {
            redirect('admin/login');
        }
    }

    function index() {
        $this->data['page_name'] = 'franchise_bank_details';
        $session_data = $this->session->userdata('admin_login');
        $user_id = $session_data['id'];
        $this->data['vendor_id'] = $user_id;
        $this->data['data'] = $this->my_model->get_data_row("franchise_bank_details", array("franchise_id" => $user_id));
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/franchise_bank_details', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function update($user_id) {
        $input_data = array(
            "franchise_id" => $user_id,
            "bank_name" => $this->input->post('bank_name'),
            "account_number" => $this->input->post('account_number'),
            "ifsc_code" => $this->input->post('ifsc_code'),
            "branch_name" => $this->input->post('branch_name'),
            "created_at" => time()
        );
        $this->my_model->delete_data("franchise_bank_details", array("franchise_id" => $user_id));
        $ins = $this->my_model->insert_data("franchise_bank_details", $input_data);
        if ($ins) {
            $this->session->set_flashdata('success_message', 'Bank Details Updated Successfully.');
            redirect(base_url('admin/franchise_bank_details'));
        } else {
            $this->session->set_flashdata('error_message', 'Failed to Update Bank Details.');
            redirect(base_url('admin/franchise_bank_details'));
        }
    }

}
