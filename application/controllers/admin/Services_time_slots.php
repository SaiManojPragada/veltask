<?php

class Services_time_slots extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->data['page_name'] = 'services_time_slots';
        $this->data['title'] = 'Services Time Slots';
        $this->data['slots'] = $this->db->get('services_time_slots')->result();

        $this->load->view('admin/includes/header', $this->data);

        $this->load->view('admin/services_time_slots', $this->data);

        $this->load->view('admin/includes/footer');
    }

}
