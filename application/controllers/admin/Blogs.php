<?php

class Blogs extends MY_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            redirect('admin/login');
        }
    }

    public function index() {
        $this->data['page_name'] = 'blogs';
        $this->data['title'] = 'Blogs';
        $this->db->order_by('id', 'desc');
        $this->data['blogs'] = $this->my_model->get_data('blogs');

        $this->load->view('admin/includes/header', $this->data);

        $this->load->view('admin/blogs', $this->data);

        $this->load->view('admin/includes/footer');
    }

    function add() {

        $this->data['page_name'] = 'blogs';
        $this->data['func'] = "insert";
        $this->data['title'] = 'Add Blog';
        $this->data['data'] = array();
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_blog', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function edit($id) {
        $this->data['func'] = "update/";
        $this->data['title'] = 'Update Blog';
        $this->data['data'] = $this->my_model->get_data_row("blogs", array("id" => $id));
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_blog', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function delete($id) {
        $delete = $this->my_model->delete_data("blogs", array("id" => $id));
        if ($delete) {
            $this->session->set_flashdata('success_message', "Blog Deleted Succesfully");
            redirect('admin/blogs');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/blogs');
        }
    }

    function insert() {
        $_POST['created_at'] = time();
        $_POST['updated_at'] = time();
        $_POST['image'] = $this->image_upload('image', 'blogs');
        $_POST['seo_url'] = make_seo_name($this->input->post('title'));
        $insert = $this->my_model->insert_data("blogs", $_POST);
        if ($insert) {
            $this->session->set_flashdata('success_message', "Blog Added Succesfully");
            redirect('admin/blogs');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/blogs');
        }
    }

    function update($id) {
        $_POST['updated_at'] = time();
        $_POST['image'] = $this->image_upload('image', 'blogs');
        if (empty($_POST['image'])) {
            unset($_POST['image']);
        }
        $_POST['seo_url'] = make_seo_name($this->input->post('title'));
        $update = $this->my_model->update_data("blogs", array("id" => $id), $_POST);
        if ($update) {
            $this->session->set_flashdata('success_message', "Blog Updated Succesfully");
            redirect('admin/blogs');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/blogs');
        }
    }

}
