<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        //echo $this->session->userdata(); 

        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            redirect('admin/login');
        }
        $this->load->model("admin_model");
    }

    function index() {
        $this->data['title'] = 'Dashboard';
        $this->data['page_name'] = 'dashboard';
        if ($user_type = $this->session->userdata('admin_login')['user_type'] === 'franchise') {
            $session_data = $this->session->userdata('admin_login');
            $this->data['details'] = $this->my_model->get_data_row("franchises", array("id" => $session_data['id']));
            $this->data['service_providers'] = $this->my_model->get_data("service_providers", array("franchise_id" => $session_data['id'], "status" => true));
            $this->data['service_orders'] = $this->my_model->get_data("services_orders", array("accepted_franchise_id" => $session_data['id'], "order_status" => "order_completed"));
            foreach ($this->data['service_providers'] as $item) {
                $this->db->where("id", $item->location_id);
                $item->location_name = $this->db->get("cities")->row()->city_name;
                $categories = $this->db->where('id IN (' . rtrim($item->categories_ids, ',') . ')')->get('services_categories')->result();
                foreach ($categories as $cat) {
                    $item->categories_names .= $cat->name . ', ';
                }
                $item->categories_names = rtrim($item->categories_names, ', ');
            }
            $this->load->view('admin/includes/header', $this->data);
            $this->load->view('admin/home', $this->data);
            $this->load->view('admin/includes/footer');
        } else {
            $prod = $this->db->query("select * from products where delete_status=0");

            $this->data['total_products'] = $prod->num_rows();  //$this->admin_model->get_table_rows_count('products');
            $this->data['total_shops'] = $this->admin_model->get_table_rows_count('vendor_shop');
            $this->data['total_visual_merchants'] = $this->admin_model->get_table_rows_count('visual_merchant');
            $this->data['total_orders'] = $this->admin_model->get_table_rows_count('visual_merchant');
            $this->data['total_users'] = $this->admin_model->get_table_rows_count('users');

            $qry1 = $this->db->query("select SUM(total_price) as today_totalorder_amount,SUM(vendor_commission) as vendor_commission,SUM(admin_commission) as admin_commission,SUM(gst) as tgst,SUM(deliveryboy_commission) as tdeliveryboy_commission,SUM(coupon_disount) as coupontotal from orders where order_status=5");
            $tresult = $qry1->row();
            $this->data['today_totalorder_amount'] = number_format($tresult->today_totalorder_amount, 2);
            $this->data['admin_commission'] = number_format($tresult->admin_commission, 2);
            $this->data['vendor_commission'] = number_format($tresult->vendor_commission, 2);
            $this->data['tgst'] = number_format($tresult->tgst, 2);
            $this->data['sales_data'] = array();
            //Services Charts*************
            $act_mnth = (int) date('m');
            $act_year = (int) date('Y');
            $current_month = (int) date('m');
            $current_year = date('Y');
            for ($i = 0; $i < 12; $i++) {
                if (strlen($current_month) < 2) {
                    $current_month = "0" . $current_month;
                }
                if ($current_month > $act_mnth) {
                    $current_year = $act_year - 1;
                }
                $this->db->select("count(id) as no_of_sales, sum(grand_total) as income");
                $this->db->where("time_slot_date LIKE '%" . $current_year . '-' . $current_month . "%'");
                $sale_data = $this->my_model->get_data_row("services_orders", array("order_status" => "order_completed"));
                $sale_data->month = date('M', strtotime('1997-' . $current_month . '-01'));
                $sale_data->year = $current_year;
                $sale_data->income = ($sale_data->income) ? round($sale_data->income) : 0;
                array_push($this->data['sales_data'], $sale_data);

                $current_month = $current_month - 1;
                if ($current_month == 0) {
                    $current_month = 12;
                }
            }
            $this->data['sales_data'] = array_reverse($this->data['sales_data']);
            $this->db->select("count(id) as visit_and_quote_count");
            $this->data['visit_and_quote_count'] = $this->my_model->get_data_row("services_orders", array("order_status" => "order_completed", "has_visit_and_quote" => 1))->visit_and_quote_count;

            $this->db->select("count(id) as regular_service_count");
            $this->data['regular_service_count'] = $this->my_model->get_data_row("services_orders", array("order_status" => "order_completed", "has_visit_and_quote" => 0))->regular_service_count;

//            die;
            $this->db->select("count(id) as total_count");
            $this->data['inprogress_count'] = $this->my_model->get_data_row("services_orders", "order_status='order_placed' OR order_status='order_accepted' OR order_status='order_started'")->total_count;
            $this->db->select("count(id) as total_count");
            $this->data['completed_count'] = $this->my_model->get_data_row("services_orders", array("order_status" => "order_completed"))->total_count;
            $this->db->select("sum(grand_total) as total_sale, sum(admin_comission) as admin_earnings, sum(franchise_comission) as franchise_earnings, sum(service_provider_comission) as provider_earnings, sum(tax) as tax_earnings");
            $this->data['earnings'] = $this->my_model->get_data_row("services_orders", array("order_status" => "order_completed"));
            $this->data['total_orders_services'] = sizeof($this->my_model->get_data("services_orders"));
            $this->data['today_orders'] = sizeof($this->my_model->get_data("services_orders", "created_at >= " . strtotime(date('Y-m-d'))));
            $this->data['today_scheduled_orders'] = sizeof($this->my_model->get_data("services_orders", "time_slot_date = '" . date('Y-m-d') . "'"));
            $this->data['total_visit_and_quote_orders'] = sizeof($this->my_model->get_data("services_orders", array("has_visit_and_quote" => 1)));
            $this->data['total_normal_orders'] = sizeof($this->my_model->get_data("services_orders", array("has_visit_and_quote" => 0)));
            $this->db->group_by("service_order_id");
            $this->data['milestone_orders'] = sizeof($this->my_model->get_data("visit_and_quote_quotaions"));
            $this->db->where("time_slot_date <'" . date('Y-m-d') . "'");
            $this->data['missed_service_orders'] = sizeof($this->my_model->get_data("services_orders", array("order_status" => "order_placed")));

            $this->load->view('admin/includes/header', $this->data);
            $this->load->view('admin/home', $this->data);
            $this->load->view('admin/includes/footer');
        }
    }

}
