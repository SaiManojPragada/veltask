<?php

class Price_points extends MY_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            redirect('admin/login');
        }
    }

    public function index() {
        $this->data['page_name'] = 'price_points';
        $this->data['title'] = 'Price Points';

        $this->data['data'] = $this->my_model->get_data("price_points");

        $this->load->view('admin/includes/header', $this->data);

        $this->load->view('admin/price_points', $this->data);

        $this->load->view('admin/includes/footer');
    }

    function add() {

        $this->data['page_name'] = 'price_points';
        $this->data['func'] = "insert";
        $this->data['title'] = 'Add Price Points';
        $this->data['data'] = array();
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_price_points', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function edit($id) {
        $this->data['func'] = "update/";
        $this->data['title'] = 'Update Price Point';
        $this->data['data'] = $this->my_model->get_data_row("price_points", array("id" => $id));
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_price_points', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function delete($id) {
        $delete = $this->my_model->delete_data("services", array("id" => $id));
        if ($delete) {
            $this->session->set_flashdata('success_message', "Membership Deleted Succesfully");
            redirect('admin/memberships');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/memberships');
        }
    }

    function insert() {
        $insert = $this->my_model->insert_data("price_points", $_POST);
        if ($insert) {
            $this->session->set_flashdata('success_message', "Price Point Added Succesfully");
            redirect('admin/price_points');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/price_points');
        }
    }

    function update($id) {
        $update = $this->my_model->update_data("price_points", array("id" => $id), $_POST);
        if ($update) {
            $this->session->set_flashdata('success_message', "Price Point Updated Succesfully");
            redirect('admin/price_points');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/price_points');
        }
    }

}
