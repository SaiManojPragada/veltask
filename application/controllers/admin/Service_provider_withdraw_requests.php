<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Service_provider_withdraw_requests extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {

            redirect('admin/login');
        }


        $this->load->model('admin_model');
    }

    function index() {
        $this->data['page_name'] = 'service_provider_withdraw_requests';
        if ($this->input->get("type") == "settled") {
            $where = array("status" => 1);
        } else {
            $where = array("status" => 0);
        }
        $transactions = $this->my_model->get_data("service_provider_withdraw_request", $where, "id", "desc");
        foreach ($transactions as $item) {
            $item->service_provider_details = $this->my_model->get_data_row("service_providers", array("id" => $item->provider_id));
            $item->admin_description = (!empty($item->admin_description)) ? $item->admin_description : "N/A";
            $item->mode_of_payment = (!empty($item->mode_of_payment)) ? $item->mode_of_payment : "N/A";
            $item->sender_name = (!empty($item->sender_name)) ? $item->sender_name : "N/A";
            $item->reciever_name = (!empty($item->reciever_name)) ? $item->reciever_name : "N/A";
            $item->transaction_id = (!empty($item->transaction_id)) ? $item->transaction_id : "N/A";
            $item->transaction_image = (!empty($item->transaction_image)) ? $item->transaction_image : "";
            $item->created_at = (!empty($item->created_at)) ? date('d M Y, h:i A', $item->created_at) : "N/A";
            $item->updated_at = (!empty($item->updated_at)) ? date('d M Y, h:i A', $item->updated_at) : "N/A";
            $item->status_text = (!$item->status) ? "Pending" : "Settled";
        }
        $this->data['transaction'] = $transactions;
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/service_provider_withdraw_requests', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function settle($id) {
        $this->data['id'] = $id;
        $this->data['title'] = "Settle Service Provider Amount";
        $transaction = $this->my_model->get_data_row("service_provider_withdraw_request", array("id" => $id));
        $provider_id = $transaction->provider_id;
        $this->data['provider_id'] = $provider_id;
        $this->data['requested_amount'] = $transaction->requested_amount;
        $wallet = $this->my_model->get_data_row("service_providers_payments", array("provider_id" => $provider_id));
        $total_earnings = $wallet->total_amount;
        $total_withdrawls = $wallet->total_requested_amount;
        $current_balance = ($total_earnings - $total_withdrawls);
        $this->data['current_balance'] = $current_balance;
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_service_provider_settlement', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function settlement_update() {
        $image = $this->image_upload("transaction_image", "service_providers");
        $id = $this->input->post("id");
        $provider_id = $this->input->post("provider_id");
        $data = array(
            "mode_of_payment" => $this->input->post('mode_of_payment'),
            "sender_name" => $this->input->post('sender_name'),
            "reciever_name" => $this->input->post('reciever_name'),
            "transaction_id" => $this->input->post('transaction_id'),
            "admin_description" => $this->input->post('admin_description'),
            "updated_at" => time(),
            "status" => 1
        );
        if (!empty($image)) {
            $data['transaction_image'] = $image;
        }
        $update = $this->my_model->update_data("service_provider_withdraw_request", array("id" => $id), $data);
        if ($update) {
            $this->db->select("SUM(requested_amount) as withdraw_amount");
            $withdraw_amount = $this->my_model->get_data_row("service_provider_withdraw_request", array("provider_id" => $provider_id, "status" => 1))->withdraw_amount;
            $up_data = array(
                "total_requested_amount" => $withdraw_amount
            );
            $this->my_model->update_data("service_providers_payments", array("provider_id" => $provider_id), $up_data);
            $this->session->set_flashdata("success_message", "Amount Request Settled");
        } else {
            $this->session->set_flashdata("error_message", "Unable to settle Requested Amount");
        }
        redirect(base_url('admin/service_provider_withdraw_requests'));
    }

}
