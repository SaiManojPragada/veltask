<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Franchises extends MY_Controller {

    public $data;

    function __construct() {

        parent::__construct();

        if ($this->session->userdata('admin_login')['logged_in'] != true) {

//$this->session->set_flashdata('error', 'Session Timed Out');

            redirect('admin/login');
        }


        $this->load->model('admin_model');
    }

    function index() {
        $this->data['page_name'] = 'franchises';
        $this->db->order_by("id", "desc");
        $qry = $this->db->get("franchises");
        $data['franchises'] = $qry->result();
        foreach ($data['franchises'] as $item) {
            $services_arr = explode(',', $item->services);
            $this->db->where("id", $item->location_id);
            $item->city_name = $this->db->get("cities")->row()->city_name;
            if (!empty($item->pincode_ids)) {
                $this->db->where("id IN (" . $item->pincode_ids . ")");
                $pincodes = $this->db->get("pincodes")->result();
                foreach ($pincodes as $pin) {
                    $item->pincode .= $pin->pincode . ", ";
                }
            }
            $item->pincode = rtrim($item->pincode, ", ");
            foreach ($services_arr as $ss) {
                $this->db->where("id", $ss);
                $gs = $this->db->get("services")->row();
                $item->services_names .= $gs->service_name . ',';
            }
            $item->services_names = rtrim($item->services_names, ',');
        }
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/franchises', $data);
        $this->load->view('admin/includes/footer');
    }

    function add() {
        $this->data['page_name'] = 'franchises';
        $this->data['title'] = 'Add Franchise';
        $data['services'] = $this->db->get('services')->result();
        $data['states'] = $this->db->get('states')->result();
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_franchise', $data);
        $this->load->view('admin/includes/footer');
    }

    function edit($id) {

        $this->data['page_name'] = 'franchises';
        $this->data['title'] = 'Add Franchise';
        $this->db->where("id", $id);
        $qry = $this->db->get("franchises");
        $data['franchises'] = $qry->row();
        $data['services'] = $this->db->get('services')->result();
        $data['states'] = $this->db->get('states')->result();
        $this->db->where("state_id", $data['franchises']->state_id);
        $data['cities'] = $this->db->get('cities')->result();
        $this->db->where("city_id", $data['franchises']->location_id);
        $data['pincodes'] = $this->db->get('pincodes')->result();
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/edit_franchise', $data);
        $this->load->view('admin/includes/footer');
    }

    function insert() {
        $_POST["franchise_id"] = $this->generate_random_key("franchises", "franchise_id", "FR");
        $_POST['created_at'] = time();
        $_POST['password'] = md5($_POST['password']);
        $_POST["photo"] = $this->image_upload("photo", "franchise");
        $_POST["aadhar_photo"] = $this->image_upload("aadhar_photo", "franchise");
        $_POST["vaccination_certificate"] = $this->image_upload("vaccination_certificate", "franchise");
        $check = $this->db->where("mobile_number", $this->input->post("mobile_number"))->get("franchises")->result();
//        $check_loc = $this->db->where("location_id", $this->input->post('location_id'))->get("franchises")->result();
        $pincodes = explode(',', $this->input->post('pincode_ids'));
        foreach ($pincodes as $pin) {
            $check_loc = $this->db->where("pincode_ids", $pin)->get("franchises")->result();
        }
        $_POST['pincode_ids'] = implode(',', $this->input->post('pincode_ids'));
        $check_loc = $this->db->where("pincode_id", $this->input->post('pincode_id'))->get("franchises")->result();
        $check_email = $this->db->where("email", $this->input->post('email'))->get("franchises")->result();
        $this->data['cities'] = $this->db->get('cities')->result();
        $this->data['prev_data'] = $this->input->post();
        if (sizeof($check) > 0) {
            $this->session->set_flashdata('error_message', 'Franchise with this Mobile Number Exists.');

            $this->load->view('admin/includes/header', $this->data);
            $this->load->view('admin/add_franchise', $this->data);
            $this->load->view('admin/includes/footer');

            unset($_POST);
            return false;
        } else if (sizeof($check_loc) > 0) {
            $this->session->set_flashdata('error_message', 'Franchise in this Pincode Location Already Exists.');

            $this->load->view('admin/includes/header', $this->data);
            $this->load->view('admin/add_franchise', $this->data);
            $this->load->view('admin/includes/footer');

            unset($_POST);
            return false;
        } else if (sizeof($check_email) > 0) {
            $this->session->set_flashdata('error_message', 'Franchise with this E-mail Already Exists.');

            $this->load->view('admin/includes/header', $this->data);
            $this->load->view('admin/add_franchise', $this->data);
            $this->load->view('admin/includes/footer');

            unset($_POST);
            return false;
        } else {
            $insert = $this->admin_model->insert_data("franchises", $this->input->post());
            if ($insert) {
                $this->session->set_flashdata('success_message', 'New Franchise Added Successfully.');
            } else {
                $this->session->set_flashdata('error_message', 'Failed to add New Franchise.');
            }
            redirect(base_url('admin/franchises'));
        }
    }

    function update($id) {
        $_POST['updated_at'] = time();
        if (empty($this->input->post("password"))) {
            unset($_POST['password']);
        } else {
            $_POST['password'] = md5($_POST['password']);
        }
        $_POST["photo"] = $this->image_upload("photo", "franchise");
        $_POST["aadhar_photo"] = $this->image_upload("aadhar_photo", "franchise");
        $_POST["vaccination_certificate"] = $this->image_upload("vaccination_certificate", "franchise");
        $_POST['updated_at'] = time();
        if (empty($_POST["photo"])) {
            unset($_POST["photo"]);
        }
        if (empty($_POST["aadhar_photo"])) {
            unset($_POST["aadhar_photo"]);
        }
        if (empty($_POST["vaccination_certificate"])) {
            unset($_POST["vaccination_certificate"]);
        }
        $this->db->where("id !=", $id);
        $check = $this->db->where("mobile_number", $this->input->post("mobile_number"))->get("franchises")->result();
        $this->db->where("id !=", $id);
        $check_loc = $this->db->where("location_id", $this->input->post('location_id'))->get("franchises")->result();
        $pincodes = explode(',', $this->input->post('pincode_ids'));
        foreach ($pincodes as $pin) {
            $this->db->where("id !=", $id);
            $check_loc = $this->db->where("pincode_ids", $pin)->get("franchises")->result();
        }
        $_POST['pincode_ids'] = implode(',', $this->input->post('pincode_ids'));
        $this->db->where("id !=", $id);
        $check_email = $this->db->where("email", $this->input->post('email'))->get("franchises")->result();
        if (sizeof($check) > 0) {
            $this->session->set_flashdata('error_message', 'Franchise with this Mobile number Exists.');
            redirect(base_url('admin/franchises/edit/' . $id));
            die;
//        } else if (sizeof($check_loc) > 0) {
//            $this->session->set_flashdata('error_message', 'Franchise in this Location Already Exists.');
//            redirect(base_url('admin/franchises/edit/' . $id));
//            die;
        } else if (sizeof($check_loc) > 0) {
            $this->session->set_flashdata('error_message', 'Franchise in this Pincode Location Already Exists.');
            redirect(base_url('admin/franchises/edit/' . $id));
            die;
        } else if (sizeof($check_email) > 0) {
            $this->session->set_flashdata('error_message', 'Franchise with this email Already Exists.');
            redirect(base_url('admin/franchises/edit/' . $id));
            die;
        }
        $update = $this->admin_model->update_data("franchises", $_POST, array("id" => $id));
        if ($update) {
            $this->session->set_flashdata('success_message', 'Franchise Updated Successfully.');
            redirect(base_url('admin/franchises'));
        } else {
            $this->session->set_flashdata('error_message', 'Failed to Update Franchise.');
            redirect(base_url('admin/franchises'));
        }
    }

    function delete($id) {
        if ($this->admin_model->delete_data("franchises", array("id" => $id))) {
            $this->session->set_flashdata('success_message', 'Franchise Deleted Successfully.');
        } else {
            $this->session->flashdata('error_message', 'Failed to Delete Franchise.');
        }
        redirect(base_url('admin/franchises'));
    }

    function change_password() {
        $this->data['page_name'] = 'franchise_change_pass';
        if (isset($_POST['update_pass'])) {
            $session_data = $this->session->userdata('admin_login');
            $this->db->where("id", $session_data['id']);
            $this->db->where("password", md5($this->input->post("current_password")));
            $check_password = $this->db->get("franchises")->row();
            if (!empty($check_password)) {
                $this->db->set(array("password" => md5($this->input->post("password"))));
                $this->db->where("id", $session_data['id']);
                $update = $this->db->update("franchises");
                if ($update) {
                    $this->session->set_flashdata('success_message', 'Password Successfully.');
                } else {
                    $this->session->set_flashdata('error_message', 'Failed to Update Password. ');
                }
            } else {
                $this->session->set_flashdata('error_message', 'Invalid Current Password.');
            }
            unset($_POST);
            redirect('admin/franchises/change_password');
        }
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/change_franchise_password', $data);
        $this->load->view('admin/includes/footer');
    }

    function franchises_requests() {
        $this->data['page_name'] = 'franchises_requests';
        $this->data['title'] = 'Franchise Requests';
        $this->db->order_by("id", "desc");
        $this->data['requests'] = $this->my_model->get_data("franchise_requests");
        foreach ($this->data['requests'] as $req) {
            $this->db->where("id", $req->franchise_id);
            $req->franchise = $this->db->get("franchises")->row();

            $this->db->where("request_id", $req->id);
            $req->requested_items = $this->db->get("franchise_requests_items")->result();
        }
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/franchises_requests', $data);
        $this->load->view('admin/includes/footer');
    }

    function change_status() {
        sleep(0.5);
        $status = $this->input->get_post("status");
        $this->db->set(array("request_status" => $status));
        $this->db->where("id", $this->input->get_post("id"));
        if ($this->db->update(franchise_requests)) {
            $this->session->set_flashdata('success_message', 'Request Status Updated Successfully.');
        } else {
            $this->session->set_flashdata('error_message', 'Failed to Update Request Status. ');
        }
        return true;
    }

    function manage_percentages($franchise_id) {

        if (!empty($_POST['btn_francises'])) {
            $price_point_ids = $this->input->post("price_point_id");
            $percentages = $this->input->post("percentage");
            $count = 0;
            unset($_POST);
            $inp = array();
            foreach ($price_point_ids as $pid) {
                array_push($inp, array("franchise_id" => $franchise_id, "price_point_id" => $pid, "commission" => $percentages[$count++]));
            }
            $this->db->trans_start();
            $this->db->where("franchise_id", $franchise_id);
            $this->db->delete("franchises_commissions");

            $this->db->insert_batch("franchises_commissions", $inp);
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $this->session->set_flashdata('error_message', 'Failed to Update Commissions.');
            } else {
                $this->db->trans_commit();
                $this->session->set_flashdata('success_message', 'Commissions Updated Successfully.');
            }
            redirect(base_url('admin/franchises/manage_percentages/' . $franchise_id));
        }
        $this->data['franchise_id'] = $franchise_id;
        $this->data['page_name'] = 'franchises';
        $this->data['title'] = 'Mange Franchise Percentages';
        $this->data['price_points'] = $this->my_model->get_data("price_points");
        $this->data['franchises_commissions'] = $this->my_model->get_data("franchises_commissions", array("franchise_id" => $franchise_id));
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/franchise_percentages', $this->data);
        $this->load->view('admin/includes/footer', $this->data);
    }

}
