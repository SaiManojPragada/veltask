<?php

class Services_coupons extends MY_Controller {

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            redirect('admin/login');
        }
    }

    function index() {
        $this->data['page_name'] = 'services_coupons';
        $this->data['title'] = 'Services Coupons';

        $qry = $this->db->get("services_coupon_codes");
        $data['coupons'] = $qry->result();
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/services_coupons', $data);
        $this->load->view('admin/includes/footer');
    }

    function add() {
        $this->data['page_name'] = 'services_coupons';
        $this->data['func'] = "insert";
        $this->data['page_name'] = 'coupons';
        $this->data['title'] = 'Add Coupon';

        $this->load->view('admin/includes/header', $this->data);

        $this->load->view('admin/add_services_coupon', $this->data);

        $this->load->view('admin/includes/footer');
    }

    function edit($id) {
        $this->data['func'] = "update/";
        $this->data['page_name'] = 'services_coupons';
        $this->data['title'] = 'Edit Coupon';

        $this->data['data'] = $this->db->get_where('services_coupon_codes', ['id' => $id])->row();

        $this->load->view('admin/includes/header', $this->data);

        $this->load->view('admin/add_services_coupon', $this->data);

        $this->load->view('admin/includes/footer');
    }

    function delete($id) {
        $this->db->where('id', $id);
        if ($this->db->delete('services_coupon_codes')) {
            $this->session->set_flashdata('success_message', 'Coupon Deleted Successfully');
            redirect('admin/services_coupons');
        } else {
            $this->session->set_flashdata('error_message', 'Unable to delete');
            redirect('admin/services_coupons');
        }
    }

    function insert() {
        $_POST['start_date'] = date('Y-m-d', strtotime($this->input->post('start_date')));
        $_POST['expiry_date'] = date('Y-m-d', strtotime($this->input->post('expiry_date')));
        $_POST['created_at'] = time();
        $_POST['updated_at'] = time();
        $insert = $this->my_model->insert_data("services_coupon_codes", $_POST);
        if ($insert) {
            $this->session->set_flashdata('success_message', "Coupon Added Succesfully");
            redirect('admin/services_coupons');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/services_coupons');
        }
    }

    function update($id) {
        $_POST['start_date'] = date('Y-m-d', strtotime($this->input->post('start_date')));
        $_POST['expiry_date'] = date('Y-m-d', strtotime($this->input->post('expiry_date')));
        $_POST['updated_at'] = time();
        $update = $this->my_model->update_data("services_coupon_codes", array("id" => $id), $_POST);
        if ($update) {
            $this->session->set_flashdata('success_message', "Coupon Updated Succesfully");
            redirect('admin/services_coupons');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/services_coupons');
        }
    }

}
