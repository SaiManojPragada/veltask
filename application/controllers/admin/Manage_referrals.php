<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Manage_referrals
 *
 * @author Admin
 */
class Manage_referrals extends MY_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            redirect('admin/login');
        }
    }

    public function index() {

        if (!empty($this->input->post('btn_ref'))) {
            unset($_POST['btn_ref']);
            $_POST['updated_at'] = time();
            $this->db->empty_table('referrals');
            $_POST["image"] = $this->image_upload("image", "referrals");
            $ins = $this->my_model->insert_data('referrals', $this->input->post());
            if ($ins) {
                $this->session->set_flashdata('success_message', "Referrals Info updated");
            } else {
                $this->session->set_flashdata('error_message', "Unable to update Referrals");
            }
            redirect(base_url() . 'admin/manage_referrals');
            unset($_POST);
        }

        $this->data['page_name'] = 'referrals';
        $this->data['title'] = 'Manage Referrals';
        $this->data['referral'] = $this->my_model->get_data_row("referrals");

        $this->load->view('admin/includes/header', $this->data);

        $this->load->view('admin/referrals', $this->data);

        $this->load->view('admin/includes/footer');
    }

}
