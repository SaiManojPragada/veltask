<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pushnotifications extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            //$this->session->set_flashdata('error', 'Session Timed Out');
            redirect('admin/login');
        }
    }

    function index() {

         $this->data['page_name'] = 'pushnotifications';
        $this->data['title'] = 'Send Pushnotification';
        $push_qry = $this->db->query("select * from promotion_notifications");
        $this->data['push_notifications'] =$push_qry->result();
        $this->load->view('admin/includes/header', $this->data);

        $this->load->view('admin/pushnotifications_list', $this->data);

        $this->load->view('admin/includes/footer');

    }

    function send() {

         $this->data['page_name'] = 'pushnotifications';
        $this->data['title'] = 'Send Pushnotification';
        $push_qry = $this->db->query("select * from promotion_notifications");
        $this->data['push_notifications'] =$push_qry->result();
        $this->load->view('admin/includes/header', $this->data);

        $this->load->view('admin/pushnotifications', $this->data);

        $this->load->view('admin/includes/footer');

    }

    function insert()
    {

        $user_type = $this->input->get_post('user_type'); 

        $user_id = $this->input->get_post('user_id');
                $title = $this->input->get_post('title');
                $message = $this->input->get_post('description');

        if($user_type=='all')
        {
            $this->db->insert("promotion_notifications",array('select_user_type'=>$user_type,'title'=>$title,'description'=>$message,'user_id'=>"0",'created_date'=>date('Y-m-d H:i:s')));
                   $user_qry1 = $this->db->query("select * from users");
                   $user_result = $user_qry1->result();
                   $i=1;
                   foreach ($user_result as $value) 
                   {
                      //$ins = $this->push_notification_android1($device_id1,$message,$title);
                      
                      
                            $user_id = $value->token;
                            //API URL of FCM
                                $url = 'https://fcm.googleapis.com/fcm/send';

                                /*api_key available in:
                                Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key*/    
                                $api_key = 'AAAAA-WcmeM:APA91bGWOW4haweH0YAjnA5KrwC9drWtZ5_f1NgaqpufSNDAeBs7sPegXGQnaasD9VLseOA52CBSDhO191WNAzKsuDpwSIlKbOkXRLFkPPVSRyZQ5gC09be-vaXbGl73s-MsEGxyNLWQ';
                                            
                                $fields = array (
                                    'registration_ids' => array (
                                            $user_id
                                    ),
                                    'data' => array (
                                            "title" => $title,
                                            "body" => $message,
                                             'sound'=>'vendor_delivery_notification'
                                    )
                                );

                                //header includes Content type and api key
                                $headers = array(
                                    'Content-Type:application/json',
                                    'Authorization:key='.$api_key
                                );
                                            
                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_URL, $url);
                                curl_setopt($ch, CURLOPT_POST, true);
                                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                                $result = curl_exec($ch);

                                //print_r($result); die;
                                if ($result === FALSE) {
                                    die('FCM Send Error: ' . curl_error($ch));
                                }
                                curl_close($ch);
                            
                            if($user_qry1->num_rows()==$i)
                            {
                                $this->session->set_flashdata('success_message', 'Notification sent Successfully');
                                redirect('admin/pushnotifications');
                                die();
                            }
                      
                      

                        $i++;
                   }
                 
                    
               

        }
        else
        {
                $this->db->insert("promotion_notifications",array('select_user_type'=>$user_type,'title'=>$title,'description'=>$message,'user_id'=>$user_id,'created_date'=>date('Y-m-d H:i:s')));

                            $user_qry = $this->db->query("select * from users where id='".$user_id."'");
                            $user_row = $user_qry->row();

                            $device_id = $user_row->token;

                $ins = $this->push_notification_android($device_id,$message,$title);
                if ($ins) {
                    $this->session->set_flashdata('success_message', 'Notification sent Successfully');
                    redirect('admin/pushnotifications');
                    die();
                } else {
                   $this->session->set_flashdata('success_message', 'Notification sent Successfully');
                    redirect('admin/pushnotifications');
                    die();
                }
        }
        

        
    }

    function push_notification_android($device_id,$message,$title){
                        if($device_id!='')
                        {  
                         $user_id = $device_id; 
                        //$user_id="e01f3cce-cd1e-4426-98b0-2661a2582c63";
                       $url = 'https://fcm.googleapis.com/fcm/send';

                                /*api_key available in:
                                Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key*/    
                                $api_key = 'AAAAA-WcmeM:APA91bGWOW4haweH0YAjnA5KrwC9drWtZ5_f1NgaqpufSNDAeBs7sPegXGQnaasD9VLseOA52CBSDhO191WNAzKsuDpwSIlKbOkXRLFkPPVSRyZQ5gC09be-vaXbGl73s-MsEGxyNLWQ';
                                            
                                $fields = array (
                                    'registration_ids' => array (
                                            $device_id
                                    ),
                                    'data' => array (
                                            "title" => $title,
                                            "body" => $message,
                                             'sound'=>'vendor_delivery_notification'
                                    )
                                );

                                //header includes Content type and api key
                                $headers = array(
                                    'Content-Type:application/json',
                                    'Authorization:key='.$api_key
                                );
                                            
                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_URL, $url);
                                curl_setopt($ch, CURLOPT_POST, true);
                                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                                $result = curl_exec($ch);
                                if ($result === FALSE) {
                                    die('FCM Send Error: ' . curl_error($ch));
                                }
                                curl_close($ch);
	    //print_r($response); die;
                    }
 
} 


function push_notification_android1($device_id,$message,$title){

                        if($device_id!='')
                        {  
                         
	    //print_r($response); die;
                    }
 
} 



}

