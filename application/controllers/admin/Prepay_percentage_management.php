<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Prepay_percentage_management extends MY_Controller {

    public $data;

    function __construct() {

        parent::__construct();

        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            redirect('admin/login');
        }
        $this->load->model('admin_model');
    }

    function index() {
        if (isset($_POST['id'])) {
            $id = $this->input->post("id");
            unset($_POST['id']);
            $this->my_model->update_data("prepay_percentage", array("id" => $id), $this->input->post());
            unset($_POST);
            redirect(base_url() . 'admin/prepay_percentage_management');
        }
        $this->data['page_name'] = 'prepay';
        $this->data['title'] = 'Prepay Percentage Management';
        $data['pre'] = $this->my_model->get_data_row("prepay_percentage");
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/prepay_percentage', $data);
        $this->load->view('admin/includes/footer');
    }

}
