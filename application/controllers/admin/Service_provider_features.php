<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Vendor Features
 *
 * @author Admin
 */
class Service_provider_features extends MY_Controller {

    //put your code here

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data['page_name'] = 'service_provider_features';
        $this->data['title'] = 'Service Provider Features';
        $this->db->order_by('view_order', 'asc');
        $this->data['data'] = $this->db->get('service_provider_features')->result();

        $this->load->view('admin/includes/header', $this->data);

        $this->load->view('admin/service_provider_features', $this->data);

        $this->load->view('admin/includes/footer');
    }

    function add() {

        $this->data['func'] = "insert";
        $this->data['title'] = 'Add Services Provider Features';
        $this->data['data'] = array();
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_service_provider_features', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function edit($id) {
        $this->data['func'] = "update/";
        $this->data['title'] = 'Update Services Category';
        $this->data['data'] = $this->my_model->get_data_row("service_provider_features", array("id" => $id));
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/add_service_provider_features', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function delete($id) {
        $delete = $this->my_model->delete_data("services_categories", array("id" => $id));
        if ($delete) {
            $this->session->set_flashdata('success_message', "Service Category Deleted Succesfully");
            redirect('admin/services_categories');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/services_categories');
        }
    }

    function insert() {
        $_POST["image"] = $this->image_upload("image", "service_provider_features", true);
        $_POST['created_at'] = time();
        $_POST['updated_at'] = time();
        $insert = $this->my_model->insert_data("service_provider_features", $_POST);
        if ($insert) {
            $this->session->set_flashdata('success_message', "Service Provider Feature Added Succesfully");
            redirect('admin/service_provider_features');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/service_provider_features');
        }
    }

    function update($id) {

        $_POST["image"] = $this->image_upload("image", "service_provider_features", true, $_POST['prev_image']);
        $_POST['updated_at'] = time();
        if (empty($_POST["image"])) {
            unset($_POST["image"]);
        }
        unset($_POST["prev_image"]);
        $update = $this->my_model->update_data("service_provider_features", array("id" => $id), $_POST);
        if ($update) {
            $this->session->set_flashdata('success_message', "Service Provider Feature Updated Succesfully");
            redirect('admin/service_provider_features');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('admin/service_provider_features');
        }
    }

}
