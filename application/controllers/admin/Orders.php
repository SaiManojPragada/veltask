<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            //$this->session->set_flashdata('error', 'Session Timed Out');
            redirect('admin/login');
        }
        $this->load->model("admin_model");
    }

    function index() {
        $data['page_name'] = 'orders';
        
        $user_type = $_SESSION['admin_login']['user_type']; 
         if($user_type=='francise')
         { 
            $id = $_SESSION['admin_login']['id'];
            $qry = $this->db->query("SELECT orders.* FROM orders INNER JOIN vendor_shop ON orders.vendor_id=vendor_shop.id WHERE vendor_shop.frachise_id='".$id."' and orders.order_status=1 order by orders.id desc");

        }
        else
        {
            $qry = $this->db->query("select * from orders where order_status=1 order by id desc");
        }
        
        $data['orders'] = $qry->result();
        
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/orders', $this->data);
        $this->load->view('admin/includes/footer');
    }

    function pending($value)
    {
        
       // echo $this->uri->segment(3);;
        $data['page_name'] = 'orders';
        $shop_id = $_SESSION['vendors']['vendor_id'];

        $user_type = $_SESSION['admin_login']['user_type']; 
         if($user_type=='francise')
         { 
            $id = $_SESSION['admin_login']['id'];
           



                if($value=='pending')
                {
                     $qry = $this->db->query("SELECT orders.* FROM orders INNER JOIN vendor_shop ON orders.vendor_id=vendor_shop.id WHERE vendor_shop.frachise_id='".$id."' and orders.order_status=1 order by orders.id desc");
                }
                else if($value=='accepted')
                {

                     $qry = $this->db->query("SELECT orders.* FROM orders INNER JOIN vendor_shop ON orders.vendor_id=vendor_shop.id WHERE vendor_shop.frachise_id='".$id."' and orders.order_status=2 and orders.accept_status=0 order by orders.id desc");
                }
                else if($value=='accepted_vendor')
                {
                    $qry = $this->db->query("SELECT orders.* FROM orders INNER JOIN vendor_shop ON orders.vendor_id=vendor_shop.id WHERE vendor_shop.frachise_id='".$id."' and orders.order_status=2 and orders.accept_status=1 order by orders.id desc");

                }
                else if($value=='pickuorders')
                {
                    $qry = $this->db->query("SELECT orders.* FROM orders INNER JOIN vendor_shop ON orders.vendor_id=vendor_shop.id WHERE vendor_shop.frachise_id='".$id."' and orders.order_status=3 order by orders.id desc");
                }
                else if($value=='ondelivery')
                {
                    $qry = $this->db->query("SELECT orders.* FROM orders INNER JOIN vendor_shop ON orders.vendor_id=vendor_shop.id WHERE vendor_shop.frachise_id='".$id."' and orders.order_status=4 order by orders.id desc");
                }
                else if($value=='completed')
                {
                    $qry = $this->db->query("SELECT orders.* FROM orders INNER JOIN vendor_shop ON orders.vendor_id=vendor_shop.id WHERE vendor_shop.frachise_id='".$id."' and orders.order_status=5 order by orders.id desc");
                }
                else if($value=='cancelled')
                {
                    $qry = $this->db->query("SELECT orders.* FROM orders INNER JOIN vendor_shop ON orders.vendor_id=vendor_shop.id WHERE vendor_shop.frachise_id='".$id."' and orders.order_status=6 order by orders.id desc");
                }
                else if($value=='return')
                {
                    $qry = $this->db->query("SELECT orders.* FROM orders INNER JOIN vendor_shop ON orders.vendor_id=vendor_shop.id WHERE vendor_shop.frachise_id='".$id."' and orders.order_status=7 order by orders.id desc");
                }
                else if($value=='allorders')
                {
                    $qry = $this->db->query("SELECT orders.* FROM orders INNER JOIN vendor_shop ON orders.vendor_id=vendor_shop.id WHERE vendor_shop.frachise_id='".$id."' order by orders.id desc");
                }


        }
        else
        {

                if($value=='pending')
                {
                    $qry = $this->db->query("select * from orders where order_status=1 order by id desc");
                }
                else if($value=='accepted')
                {
                    $qry = $this->db->query("select * from orders where order_status=2 and accept_status=0 order by id desc");
                }
                else if($value=='accepted_vendor')
                {
                    $qry = $this->db->query("select * from orders where order_status=2 and accept_status=1 order by id desc");
                }
                else if($value=='pickuorders')
                {
                    $qry = $this->db->query("select * from orders where order_status=3 order by id desc");
                }
                else if($value=='ondelivery')
                {
                    $qry = $this->db->query("select * from orders where order_status=4 order by id desc");
                }
                else if($value=='completed')
                {
                    $qry = $this->db->query("select * from orders where order_status=5 order by id desc");
                }
                else if($value=='cancelled')
                {
                    $qry = $this->db->query("select * from orders where order_status=6 order by id desc");
                }
                else if($value=='return')
                {
                    $qry = $this->db->query("select * from orders where order_status=7 order by id desc");
                }
                else if($value=='allorders')
                {
                    $qry = $this->db->query("select * from orders order by id desc");
                }
        }
        
        $data['orders'] = $qry->result();
        
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/orders', $this->data);
        $this->load->view('admin/includes/footer'); 
    
    }



    

    function orderCancel($orderid,$user_id)
    {
        $upd = $this->db->update("orders",array('order_status'=>6),array('id'=>$orderid));
        if($upd)
        {
            $ord_qry = $this->db->query("select * from orders where id='".$orderid."'");
            $ord_rows = $ord_qry->row();

             $qry = $this->db->query("select * from cart where session_id='".$ord_rows->session_id."'");
        $result = $qry->result();
        $ar=[];
            foreach ($result as $value) 
            {
                $link = $this->db->query("select * from  link_variant where id='".$value->variant_id."'");
                $link_variants = $link->row();

                $pro = $this->db->query("select * from  products where id='".$link_variants->product_id."' and delete_status=0");
                $product = $pro->row();

                $adm = $this->db->query("select * from  admin_comissions where shop_id='".$vendor_id."' and cat_id='".$product->cat_id."' and find_in_set('".$product->sub_cat_id."',subcategory_ids)");
                $admin = $adm->row();
                
                $admin_price = ($admin->admin_comission /100)*$value->price;
                

               $total=$link_variants->stock-$value->quantity;
               

                    $ar = array('varient_id'=>$value->variant_id,'product_id'=>$link_variants->product_id,'quantity'=>$value->quantity,'paid_status'=>'Credit','message'=>'Order cancelled by admin','total_stock'=>$total,'created_at'=>time());
                    $ins11 = $this->db->insert("stock_management",$ar);

                    if($ins11)
                    {
                        $qty = $link_variants->stock-$value->quantity;
                       $this->db->update("link_variant",array('stock'=>$qty),array('id'=>$value->variant_id));
                    }
            }


            $msg = "Admin Cancel the order";
            $this->db->insert("sms_notifications",array('order_id'=>$orderid,'sender_id'=>0,'receiver_id'=>$user_id,'message'=>$msg,'created_at'=>time(),'order_status'=>$msg,'action_person'=>3));
            $this->session->set_flashdata('success_message', 'Order Cancelled Successfully');
            redirect('admin/orders');

        }
    }

    function orderlogs($id)
    {
        $or = $this->db->query("select * from sms_notifications where order_id='".$id."' and order_status!='' order by id asc");
        $result = $or->result();
        
        $data['orderlogs']=$result;
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/order_logs', $this->data);
        $this->load->view('admin/includes/footer');
    }
    
    function orderDetails($session_id)
    {
        $this->data['page_name'] = 'orders';
        $qry = $this->db->query("select * from orders  where session_id='".$session_id."'");
        $data['orders'] = $qry->row();
        
        $shop_id = $_SESSION['vendors']['vendor_id'];
        $qry = $this->db->query("select * from cart where session_id='".$session_id."'");
        $orders = $qry->result();
        $ar=[];
        foreach($orders as $ord)
        {
               $var = $this->db->query("select * from link_variant where id='".$ord->variant_id."'");
               $variant = $var->row(); 
               
               $pro = $this->db->query("select * from products where id='".$variant->product_id."'");
               $prod = $pro->row(); 
               
               $jsondata = json_decode($variant->jsondata);
               //print_r($jsondata); die;
               $attributes=[];
               foreach($jsondata as $row)
               {
                    $att_title = $this->db->query("select * from attributes_title where id='".$row->attribute_type."'");
                    $att_title_row = $att_title->row();
                    
                    $att_value = $this->db->query("select * from attributes_values where id='".$row->attribute_value."'");
                    $att_value_row = $att_value->row();
                    $attributes[]=array('type'=>$att_title_row->title,'value'=>$att_value_row->value);
               }
               
            $ar[]=array('id'=>$ord->id,'product_name'=>$prod->name,'price'=>$ord->price,'quantity'=>$ord->quantity,'unit_price'=>$ord->unit_price,'attributes'=>$attributes,'variant_id'=>$ord->variant_id,'cat_id'=>$prod->cat_id,'vendor_id'=>$ord->vendor_id);
        }
        $data['order_cart']=$ar;
        
        $or = $this->db->query("select * from orders where session_id='".$session_id."'");
        $ord = $or->row();
        
        $data['orderdetails']=$ord;
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/order_details', $this->data);
        $this->load->view('admin/includes/footer');
    }
  
     function delivery($session_id)
    {
        $data['order_id']=$session_id;
        $del = $this->db->query("select id,name,phone from deliveryboy");
        $data['deliverypersons']=$del->result();
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/deliverypage', $data);
        $this->load->view('admin/includes/footer');
    }

    function assignDeliveryBoy()
    {
        $check = $this->db->query("select * from orders where id='".$this->input->get_post('order_id')."' and delivery_boy=0");
        $ar = array('delivery_boy'=>$this->input->get_post('db_id'),'order_status'=>'3');
        $wr = array('id'=>$this->input->get_post('order_id'));
        $upd = $this->db->update("orders",$ar,$wr);
        if($upd)
        { 
            $ad_ar = array('status'=>1);
            $ad_wr = array('order_id'=>$this->input->get_post('order_id'));
            $this->db->update("admin_notifications",$ad_ar,$ad_wr);

            $chk = $this->db->query("select * from deliveryboy where id='".$this->input->get_post('db_id')."'");
            $delivery = $chk->row();
           
            $device_id = $delivery->token;
            $title = "New Order";
            $message = "New Order Assigned From VELTASK";
			
			

            //$this->send_push_notification($device_id,$message,$title);
            
			
            $this->push_notification_android($device_id,$message,$title);
            $data['page_name'] = 'orders';
            $shop_id = $_SESSION['vendors']['vendor_id'];
            $qry = $this->db->query("select * from orders where vendor_id='".$shop_id."' order by id desc");
            $data['orders'] = $qry->result();
             //$this->mailsenttoCustomer($this->input->get_post('db_id'),$this->input->get_post('order_id'));

        $qry = $this->db->query("select * from orders where id='".$this->input->get_post('order_id')."'");
        $order_row = $qry->row();

        $user_qry = $this->db->query("select * from users where id='".$order_row->user_id."'");
        $user_row = $user_qry->row();

        $del_qry = $this->db->query("select * from deliveryboy where id='".$this->input->get_post('db_id')."'");
        $delivery_row = $del_qry->row();


$otp_message = "Dear ".$delivery_row->name." you have been assigned to pick up order no ".$order_row->id." from vendor ".$vendor_name." and deliver to ".$user_row->first_name.". Pls acknowledge notification in app.";
            
                    $template_id = "1407161684122274071";
                    $this->send_message($otp_message,$delivery_row->phone,$template_id);

                    $otp_message = "Dear ".$user_row->first_name." ur order no ".$this->input->get_post('order_id')." is assigned to ".$delivery_row->name." and he/she will deliver to you shortly. Thank u for shopping with us.";
            
                    $template_id = "1407161683247844364";
                    if($this->send_message($otp_message,$user_row->phone,$template_id))
                    {

$user_id = $order_row->user_id;
            $user_title = "Delivery Boy Assigned";
            $user_message = $otp_message;
			$this->onesignalnotification($user_id,$user_message,$user_title);


        $to_mail = $user_row->email;


        
        if($check->num_rows()>0)
        {
                $user_order_message1 = "Delivery Boy Assigned";
                $this->db->insert("sms_notifications",array('order_id'=>$this->input->get_post('order_id'),'receiver_id'=>$this->input->get_post('db_id'),'sender_id'=>1,'created_at'=>time(),'message'=>$user_order_message1,'action_person'=>3,'order_status'=>$user_order_message1)); 
                 
        }
        else
        {
               $user_order_message1 = "Delivery Boy ReAssigned";
               $this->db->insert("sms_notifications",array('order_id'=>$this->input->get_post('order_id'),'receiver_id'=>$this->input->get_post('db_id'),'sender_id'=>1,'created_at'=>time(),'message'=>$user_order_message1,'action_person'=>3,'order_status'=>$user_order_message1));  
        }
       
        }
            redirect('admin/orders');
        }
    }
	
	function onesignalnotification($user_id,$message,$title)
    {
        $qr = $this->db->query("select * from users where id='".$user_id."'");
        $res = $qr->row();
            if($res->token!='')
            {                 
                       
                        $user_id = $res->token;
                        
                        $fields = array(
        'app_id' => 'e072cc7b-595d-4c4c-a451-b07832b073f9',
        'include_player_ids' => [$user_id],
        'contents' => array("en" =>$message),
        'headings' => array("en"=>$title),
		'android_channel_id' => 'ea6c19aa-e55f-4243-af28-605a32901234'
    );
    
    $fields = json_encode($fields);
    //print("\nJSON sent:\n");
    //print($fields);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json; charset=utf-8', 
        'Authorization: Basic NzhjMmI5YjItZmViMy00YjNlLWFlMDItY2ZiZTI3OTY0YzYz'
    ));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                                           
    $response = curl_exec($ch);
    curl_close($ch);
    //print_r($response); die;

                  
            }  
    }
	
function send_push_notification($userid,$message,$title){

$data = array("to" => $userid, "priority" => 'high', "data" => array("title" => $title,"body" => $message, 'sound'=>'vendor_delivery_notification','msg_type'=>'Notificaton'));


	  $data_string = json_encode($data);
		//print_r($data_string);die;
      $headers = array
      (
       'Authorization: key=AAAA0bshvm4:APA91bESYbazUcHNIp9RpvqX2tjzYfajYyCtH_C1rXsLhTIyjYRnPg_hI6igO-luMj65UqGvILKtUFld0BMlncfa31JV6pNt6B_lXVszpST_iI3mDdC0_cXeuJ9VTDxoTkJM1qhAcaLJ',
       'Content-Type: application/json'
      );

      $ch = curl_init();

      curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
      curl_setopt( $ch,CURLOPT_POST, true );
      curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
      curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
      curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string);

      $result = curl_exec($ch);

      curl_close ($ch);

      $arr = array('status' => "valid",'data' => strip_tags($result));

      return $arr;

    }

function send_message($message = "", $mobile_number,$template_id) {


         $message = urlencode($message);

         $URL = "http://login.smsmoon.com/API/sms.php"; // connecting url 

         $post_fields = ['username' => 'veltask', 'password' => 'vizag@123', 'from' => 'veltask', 'to' => $mobile_number, 'msg' => $message, 'type' => 1, 'dnd_check' => 0,'template_id'=>$template_id];

         //file_get_contents("http://login.smsmoon.com/API/sms.php?username=colourmoonalerts&password=vizag@123&from=WEBSMS&to=$mobile_number&msg=$message&type=1&dnd_check=0");
         $ch = curl_init();
         curl_setopt($ch, CURLOPT_URL, $URL);
         curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         curl_exec($ch);
         return true;
      }



     function push_notification_android($device_id,$message,$title){

    //API URL of FCM
    $url = 'https://fcm.googleapis.com/fcm/send';

    /*api_key available in:
    Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key*/    
    $api_key = 'AAAAkpaJlU0:APA91bFtR5W87oDNlnaW4dgXbTXZAENAiPOx7D9to-h-GujBPC0Eo5bKsvVz2RkdNA5pTs8ffAZXgyOESL59o6IDwdci5dvmHJCd6B4ppbg5vPHxxw6tr3wnaEB9sbtsOcPIk9E8J9mf';
                
    $fields = array (
        'registration_ids' => array (
                $device_id
        ),
        'data' => array (
                "title" => $title,
                "body" => $message,
				 'sound'=>'vendor_delivery_notification'
        )
    );

    //header includes Content type and api key
    $headers = array(
        'Content-Type:application/json',
        'Authorization:key='.$api_key
    );
                
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    if ($result === FALSE) {
        die('FCM Send Error: ' . curl_error($ch));
    }
    curl_close($ch);
    //print_r($result); die;
    return $result;
} 



}
