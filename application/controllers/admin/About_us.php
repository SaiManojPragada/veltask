<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of About_us
 *
 * @author Admin
 */
class About_us extends MY_Controller {

    //put your code here
    public $data;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_login')['logged_in'] != true) {
            redirect('admin/login');
        }
    }

    function index() {

        if (!empty($this->input->post())) {
            $_POST['created_at'] = time();
            $_POST['updated_at'] = time();
            $qry = $this->my_model->update_data("aboutus", array("id" => 1), $this->input->post());
            if ($qry) {
                $this->session->set_flashdata('success_message', 'About Us updated Successfully');
            } else {
                $this->session->set_flashdata('error_message', 'Unable to update About Us');
            }
            redirect(base_url('admin/about_us'));
        }


        $this->data['page_name'] = 'about_us';
        $this->data['title'] = 'About Us';
        $this->data['about'] = $this->my_model->get_data_row('aboutus');
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/about_us', $this->data);
        $this->load->view('admin/includes/footer');
    }

}
