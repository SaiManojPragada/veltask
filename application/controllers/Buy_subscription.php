<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Buy_subscription extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->load->model('razorpay_service_model');
    }

    function index() {
        $subs_id = $this->session->userdata("selected_subscription_id");
        if (empty($subs_id)) {
            redirect($this->agent->referrer());
        }
        $membership_data = $this->my_model->get_data_row("memberships", array("id" => $subs_id));
        $raorpay_order = $this->razorpay_service_model->create_order((float) $membership_data->sale_price);
        $this->data['razorpay_order_data'] = $raorpay_order;
        $this->data['membership_data'] = $membership_data;
        $this->my_view('membership_payment_page', $this->data);
    }

}
