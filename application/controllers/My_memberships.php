<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class My_memberships extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->check_user_log(USER_ID);
    }

    function index() {
        $this->data['page_name'] = "my_memberships";
        $this->data['my_memberships_transactions'] = $this->my_model->get_data("membership_transactions", array("user_id" => USER_ID), 'id', 'desc');
        foreach ($this->data['my_memberships_transactions'] as $item) {
            $item->membership_details = $this->my_model->get_data_row("memberships", array("id" => $item->membership_id));
        }
        $this->my_view('my-memberships', $this->data);
    }

}
