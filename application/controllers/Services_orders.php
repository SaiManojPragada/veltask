<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Services_orders extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->data['page_name'] = 'services_orders';
        $this->check_user_log(USER_ID);
    }

    function ongoing() {
        $this->data['view_filter'] = 'ongoing';

        $this->get_req_data("(order_status = 'order_placed' OR order_status = 'order_accepted' OR order_status = 'order_started')");
        $this->my_view('my-services-orders', $this->data);
    }

    function completed() {
        $this->data['view_filter'] = 'completed';
        $this->get_req_data("order_status = 'order_completed'");
        $this->my_view('my-services-orders', $this->data);
    }

    function cancelled() {
        $this->data['view_filter'] = 'cancelled';
        $this->get_req_data("(order_status = 'order_cancelled' || order_status='refunded')");
        $this->my_view('my-services-orders', $this->data);
    }

    function get_req_data($where) {
        $this->db->select("wallet_amount");
        $this->data['wallet_amount'] = $this->my_model->get_data_row("users", array("id" => USER_ID))->wallet_amount;
        $this->db->where(array("user_id" => USER_ID));
        $this->data['all_my_orders'] = $this->my_model->get_data("services_orders");
        $this->db->where(array("user_id" => USER_ID));
        $this->db->where($where);
        $this->data['services_orders'] = $this->my_model->get_data("services_orders", "", 'id', 'desc');
        foreach ($this->data['services_orders'] as $item) {
            $item->order_status_text = ucwords(str_replace("_", " ", $item->order_status));
            if ($item->order_status == 'order_placed' || $item->order_status == 'order_accepted' || $item->order_status == 'order_started') {
                $item->show_status = "Pending";
                $item->status_color = "bg-warning-light";
            } else if ($item->order_status == 'order_cancelled' || $item->order_status == 'refunded') {
                $item->show_status = "Cancelled";
                $item->status_color = "bg-danger-light";
            } else if ($item->order_status == 'order_rejected') {
                $item->show_status = "Rejected";
                $item->status_color = "bg-danger-light";
            } else if ($item->order_status == 'order_completed') {
                $item->show_status = "Completed";
                $item->status_color = "bg-success-light";
            }
            if ($item->has_visit_and_quote == "Yes") {
                $item->visit_and_quote_val = "<span style='color: green'>(Visit and Quote)</span>";
            } else {
                $item->visit_and_quote_val = "";
            }
            $item->service_category = $this->my_model->get_data_row("services_categories", array("id" => $item->ordered_categories));
            $order_items = $this->my_model->get_data_row("service_orders_items", array("order_id" => $item->order_id));
            foreach ($order_items as $oi) {
                $oi->service_name = $this->my_model->get_data_row("services", array("id" => $oi->service_id))->service_name;
            }
            $item->order_items = $order_items;
        }
    }

    function view($order_id) {
        $this->data['order_id'] = $order_id;
        $this->data['order'] = $this->my_model->get_data_row('services_orders', array("order_id" => $order_id));
        $this->data['order']->order_status_text = ucwords(str_replace("_", " ", $this->data['order']->order_status));
        $this->data['order']->cancellation_message = "Cancellation will be allowed untill order is Accepted.";
        $this->db->select("id,name,phone,email,photo");
        $this->data['service_provider'] = $this->my_model->get_data_row('service_providers', array("id" => $this->data['order']->accepted_by_service_provider));
        $this->data['service_category'] = $this->my_model->get_data_row("services_categories", array("id" => $this->data['order']->ordered_categories));
        $this->data['user_address'] = $this->my_model->get_data_row('user_address', array("id" => $this->data['order']->user_addresses_id));
        $order_services = $this->my_model->get_data('service_orders_items', array('order_id' => $this->data['order']->order_id));
        if ($this->data['order']->has_visit_and_quote == "Yes") {
            $this->data['quotation'] = $this->my_model->get_data_row("visit_and_quote_quotaions", array("service_order_id" => $this->data['order']->id));
            $this->data['milestones'] = $this->my_model->get_data("quotation_milestones", array("quotation_id" => $this->data['quotation']->id));
        }
        if ($this->data['order']->has_visit_and_quote == "Yes") {
            $this->data['order']->visit_and_quote_val = "<span style='color: green'>(Visit and Quote)</span>";
        } else {
            $this->data['order']->visit_and_quote_val = "";
        }
        foreach ($order_services as $item) {
            $item->service_item = $this->my_model->get_data_row("services", array('id' => $item->service_id));
        }
        $order = $this->data['order'];
        $grand_total = $order->grand_total - ($order->coupon_discount + $order->membership_discount + $order->bussiness_discount);
        if ($order->amount_paid == $order->grand_total || $order->order_status == "order_completed") {
            $this->data['show_payment_status'] = "<span style='color: green'>Paid</span>";
        }
        if ($order->amount_paid > 0 && $order->amount_paid < $order->grand_total) {
            $this->data['show_payment_status'] = "<span style='color: orange'>Partially Paid</span>";
        }
        if ($order->amount_paid == 0) {
            $this->data['show_payment_status'] = "<span style='color: red'>Unpaid</span>";
        }
        if ($order->order_status) {
            $this->data['show_payment_status'] = "<span style='color: green'>Paid</span>";
        }
        $this->data['order_services'] = $order_services;
        $this->my_view('view-service-order', $this->data);
    }

    function write_review($order_id) {
        $this->data['order_id'] = $order_id;
        $this->data['order'] = $this->my_model->get_data_row('services_orders', array("order_id" => $order_id));
        $this->data['order_items'] = $this->my_model->get_data("service_orders_items", array("order_id" => $order_id));
        $order_real_id = $this->data['order']->id;
        $this->data['order_real_id'] = $order_real_id;
        foreach ($this->data['order_items'] as $item) {
            $item->service_data = $this->my_model->get_data_row("services", array("id" => $item->service_id));
            $item->service_image = $this->my_model->get_data_row("services_images", array("service_id" => $item->service_id));
            $item->review = $this->my_model->get_data_row("services_reviews", array("order_id" => $order_real_id, "service_id" => $item->service_id));
        }
        $this->my_view('write-review', $this->data);
    }

}
