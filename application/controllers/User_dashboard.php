<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class User_dashboard extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->check_user_log(USER_ID);
    }

    function index() {
        $this->data['page_name'] = "dashboard";
        $this->db->select("wallet_amount");
        $this->data['wallet_amount'] = $this->my_model->get_data_row("users", array("id" => USER_ID))->wallet_amount;
        $this->db->order_by('id', 'desc');
        $this->data['services_orders'] = $this->my_model->get_data("services_orders", array("user_id" => USER_ID));
        foreach ($this->data['services_orders'] as $item) {
            $item->order_status_text = ucwords(str_replace("_", " ", $item->order_status));
            if ($item->order_status == 'order_placed' || $item->order_status == 'order_accepted' || $item->order_status == 'order_started') {
                $item->show_status = "Pending";
                $item->status_color = "bg-warning-light";
            } else if ($item->order_status == 'order_cancelled') {
                $item->show_status = "Cancelled";
                $item->status_color = "bg-danger-light";
            } else if ($item->order_status == 'order_rejected') {
                $item->show_status = "Rejected";
                $item->status_color = "bg-danger-light";
            } else if ($item->order_status == 'order_completed') {
                $item->show_status = "Completed";
                $item->status_color = "bg-success-light";
            }
            if ($item->has_visit_and_quote == "Yes") {
                $item->visit_and_quote_val = "<span style='color: green'>(Visit and Quote)</span>";
            } else {
                $item->visit_and_quote_val = "";
            }
            $item->service_category = $this->my_model->get_data_row("services_categories", array("id" => $item->ordered_categories));
            $order_items = $this->my_model->get_data_row("service_orders_items", array("order_id" => $item->order_id));
            foreach ($order_items as $oi) {
                $oi->service_name = $this->my_model->get_data_row("services", array("id" => $oi->service_id))->service_name;
            }
            $item->order_items = $order_items;
        }
        $this->my_view('user-dashboard', $this->data);
    }

}
