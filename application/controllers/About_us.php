<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of About_us
 *
 * @author Admin
 */
class About_us extends MY_Controller {

    public $data;

    //put your code here
    function __construct() {
        parent::__construct();
    }

    function index() {
//        $this->db->select('description');
        $data = $this->my_model->get_data_row("aboutus");
        $data->title = "About Us";
        $this->data['data'] = $data;
//        $this->my_view('about_us', $this->data);
        $this->load->view("web/includes/header-copy", $this->data);
        $this->load->view('web/about_us', $this->data);
        $this->load->view("web/includes/footer", $this->data);
    }

}
