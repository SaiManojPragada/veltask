<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Payment extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->check_user_log(USER_ID);
        $this->load->model('razorpay_service_model');
        $this->data['coupon_and_wallet'] = $this->session->userdata("service_coupon_data");
        $this->data['wallet_amount'] = $this->my_model->get_data_row("users", array("id" => USER_ID))->wallet_amount;
    }

    function index() {
        $pre_amount = $this->session->userdata("service_coupon_data");
        $cart = $this->my_model->get_data("services_cart", array("user_id" => USER_ID));
        $min_cart_amount = $this->my_model->get_data_row('cart_min_amount')->services_min_amount;
        $cc = $this->get_cart_totals($cart[0]->has_visit_and_quote);
        if (empty($cart)) {
            redirect(base_url('cart'));
        }
        if ($cc->total < $min_cart_amount) {
            redirect(base_url('cart'));
        }
        $cart_totals = $this->get_cart_totals($cart[0]->has_visit_and_quote);
        $amount_to_be_paid = str_replace(",", "", (!empty($pre_amount)) ? $pre_amount['grand_total'] : $cart_totals->total);
//        echo round($amount_to_be_paid);
//        die;
        $raorpay_order = $this->razorpay_service_model->create_order((float) str_replace(",", "", round($amount_to_be_paid)));
        $this->data['razorpay_order_data'] = $raorpay_order;
//        echo RAZORPAY_KEY . '   ***    ';
//        echo RAZORPAY_SECRET . '  *****    ';
//        print_r($this->data['razorpay_order_id']);

        $this->data['order_id'] = $this->generate_random_key("service_orders_items", "order_id", "VT");
        $this->get_cart_totals($cart[0]->has_visit_and_quote);
        $selected_address_id = $this->my_model->get_data_row("user_address", array("user_id" => USER_ID, "isdefault" => "yes"))->id;
        $out_data = array(
            "user_id" => USER_ID,
            "coupon_code_id" => (!empty($pre_amount['coupon_id'])) ? $pre_amount['coupon_id'] : "",
            "coupon_code" => (!empty($pre_amount['coupon_code'])) ? $pre_amount['coupon_code'] : "",
            "coupon_discount" => (!empty($pre_amount['coupon_discount'])) ? $pre_amount['coupon_discount'] : "",
            "membership_discount" => (float) $this->data['cart_totals']->membership_discount,
            "business_discount" => (float) $this->data['cart_totals']->business_discount,
            "user_addresses_id" => $selected_address_id,
            "time_slot_date" => $this->session->userdata("time_slots_date"),
            "time_slot" => $this->session->userdata('time_slots_time'),
            "payment_type" => $this->session->userdata('payment_method'),
//            "amount_paid" => $this->input->post('amount_paid'),
//            "razorpay_order_id" => $this->input->post('razorpay_order_id'),
//            "razorpay_transaction_id" => $this->input->post("razorpay_transaction_id"),
            "order_id" => $this->data['order_id'],
            "used_wallet_amount" => (!empty($pre_amount['wallet_applied_amount'])) ? $pre_amount['wallet_applied_amount'] : "",
        );
        $this->data['out_data'] = json_encode($out_data);
        $this->my_view('payment_page', $this->data);
    }

//    function failed() {
//        $order_id = $this->input->post("order_id");
//    }

    function pre_pay() {
        $pre_amount = $this->session->userdata("service_coupon_data");
        $cart = $this->my_model->get_data("services_cart", array("user_id" => USER_ID));
        $cart_totals = $this->get_cart_totals($cart[0]->has_visit_and_quote);
        $amount_to_be_paid = (!empty($pre_amount)) ? $pre_amount['grand_total'] : $cart_totals->total;
        $amount_to_be_paid = (float) str_replace(",", "", $amount_to_be_paid);
        $pre_pay_percentage = $this->my_model->get_data_row("prepay_percentage")->prepay_percentage;
        $amount_to_be_paid = round(($amount_to_be_paid * $pre_pay_percentage) / 100);
//        print_r($amount_to_be_paid);
//        die;
        $raorpay_order = $this->razorpay_service_model->create_order($amount_to_be_paid);
        $this->data['razorpay_order_data'] = $raorpay_order;
//        echo RAZORPAY_KEY . '   ***    ';
//        echo RAZORPAY_SECRET . '  *****    ';
//        print_r($this->data['razorpay_order_id']);

        $this->data['order_id'] = $this->generate_random_key("service_orders_items", "order_id", "VT");
        $this->get_cart_totals($cart[0]->has_visit_and_quote);
        $selected_address_id = $this->my_model->get_data_row("user_address", array("user_id" => USER_ID, "isdefault" => "yes"))->id;
        $out_data = array(
            "user_id" => USER_ID,
            "coupon_code_id" => (!empty($pre_amount['coupon_id'])) ? $pre_amount['coupon_id'] : "",
            "coupon_code" => (!empty($pre_amount['coupon_code'])) ? $pre_amount['coupon_code'] : "",
            "coupon_discount" => (!empty($pre_amount['coupon_discount'])) ? $pre_amount['coupon_discount'] : "",
            "membership_discount" => $this->data['cart_totals']->membership_discount,
            "business_discount" => $this->data['cart_totals']->business_discount,
            "user_addresses_id" => $selected_address_id,
            "time_slot_date" => $this->session->userdata("time_slots_date"),
            "time_slot" => $this->session->userdata('time_slots_time'),
            "payment_type" => $this->session->userdata('payment_method'),
//            "amount_paid" => $this->input->post('amount_paid'),
//            "razorpay_order_id" => $this->input->post('razorpay_order_id'),
//            "razorpay_transaction_id" => $this->input->post("razorpay_transaction_id"),
            "order_id" => $this->data['order_id'],
            "used_wallet_amount" => (!empty($pre_amount['wallet_applied_amount'])) ? $pre_amount['wallet_applied_amount'] : "",
        );
        $this->data['out_data'] = json_encode($out_data);
        $this->my_view('payment_page', $this->data);
    }

    function pre_pay_balance($order_id) {
        $order_data = $this->my_model->get_data_row("services_orders", array("order_id" => $order_id));
        $amount_to_be_paid = $order_data->balance_amount;
        $raorpay_order = $this->razorpay_service_model->create_order(round($amount_to_be_paid));
        $this->data['razorpay_order_data'] = $raorpay_order;
        $this->data['custom_url'] = "pre_pay_balance_payment";
        $out_data = array(
            "user_id" => USER_ID,
            "order_id" => $order_data->id
        );
        $this->data['out_data'] = json_encode($out_data);
        $this->my_view('payment_page', $this->data);
    }

    function milestone($quotaion_id, $milestone_id) {
        $this->data['quotation_id'] = $quotaion_id;
        $this->data['milestone_id'] = $milestone_id;
        if ($milestone_id == "all") {
            $check = $this->my_model->get_data_row("visit_and_quote_quotaions", array("id" => $quotaion_id));
            $amount_to_be_paid = round($check->balance_amount);
            $this->data['err'] = ($amount_to_be_paid != 0) ? "true" : "false";
        } else {
            $milestone = $this->my_model->get_data_row("quotation_milestones", array("id" => $milestone_id));
            $amount_to_be_paid = round($milestone->amount);
            $this->data['err'] = ($milestone->status == 'unpaid') ? "true" : "false";
        }
        $raorpay_order = $this->razorpay_service_model->create_order((float) $amount_to_be_paid);
        $this->data['razorpay_order_data'] = $raorpay_order;
        $this->my_view('milestone_payment', $this->data);
    }

}
