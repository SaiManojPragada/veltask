<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Website
 *
 * @author Admin
 */
class Website extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->load->model('services_model');
        $this->load->model('homepage_model');
        $this->session->unset_userdata("services_filters");
    }

    function index() {
        $user_id = USER_ID;
        $this->clear_prev_visit_and_quote($user_id);
        $lat = $this->session->userdata('USER_CURRENT_LOCATION_LAT');
        $lng = $this->session->userdata('USER_CURRENT_LOCATION_LNG');
        $homepage = $this->homepage_model->getHomeScreen($user_id, $lat, $lng, 'web');
        $this->db->order_by('priority', 'asc');
        $this->data['testimonials'] = $this->my_model->get_data('testimonials', array('status' => true));
        $this->db->order_by('priority', 'asc');
        $this->data['franchise_tabs'] = $this->my_model->get_data('franchise_tabs');
        $homepage_data = $homepage['data'];
        $this->data['sliders'] = $homepage_data['sliders'];
        $this->data['services_categories'] = $homepage_data['services_categories'];
        $this->data['banners'] = $homepage_data['banner'];
        $this->data['admin'] = $this->my_model->get_data_row('admin');

        $this->data['ecommerce_categories'] = $this->my_model->get_data('categories', array('status' => 1));
        //echo "<pre>"; print_r($this->data['ecommerce_categories']);die;
        $this->my_view('index', $this->data);
    }

    function clear_prev_visit_and_quote($user_id) {
        $check_cart_for_visit_and_quote = $this->my_model->get_data("services_cart", array("user_id" => $user_id, "has_visit_and_quote" => 1));
        if (!empty($check_cart_for_visit_and_quote)) {
            $this->my_model->delete_data("services_cart", array("user_id" => $user_id));
        }
        return true;
    }

    function logout() {
        $this->session->unset_userdata("user");
        redirect(base_url());
    }

    function business_registration() {

        if (!empty($this->input->post('franchise_submit'))) {
//            print_r($_FILES);
//            die;
            unset($_POST['re_password']);
            unset($_POST['franchise_submit']);
            $_POST['password'] = md5($this->input->post('password'));
            $_POST['status'] = 0;
            $_POST['franchise_id'] = $this->generate_random_key("franchises", "franchise_id", "FR");
            $_POST['created_at'] = time();
            $_POST["photo"] = $this->image_upload("photo", "franchise");
            $_POST["aadhar_photo"] = $this->image_upload("aadhar_photo", "franchise");
            $_POST["vaccination_certificate"] = $this->image_upload("vaccination_certificate", "franchise");
            $check = $this->db->where("mobile_number", $this->input->post("mobile_number"))->get("franchises")->result();
//        $check_loc = $this->db->where("location_id", $this->input->post('location_id'))->get("franchises")->result();
            $check_loc = $this->db->where("pincode_ids", $this->input->post('pincode_id'))->get("franchises")->result();
            $check_email = $this->db->where("email", $this->input->post('email'))->get("franchises")->result();
            if (sizeof($check) > 0) {
                $this->session->set_flashdata('popup_error', 'Franchise with this Mobile Number Exists.');
            } else if (sizeof($check_loc) > 0) {
                $this->session->set_flashdata('popup_error', 'Franchise in this Pincode Location Already Exists.');
            } else if (sizeof($check_email) > 0) {
                $this->session->set_flashdata('popup_error', 'Franchise with this E-mail Already Exists.');
            } else {
                $insert = $this->my_model->insert_data("franchises", $this->input->post());
                if ($insert) {
                    $this->session->set_userdata("popup_success", "<b>Franchise </b>registration Successful.");
                } else {
                    $this->session->set_userdata("popup_error", "<b>Franchise </b>registration Failed.");
                }
            }

            unset($_POST);
            redirect(base_url('business-registration'));
        }
        if (!empty($this->input->post('service_provider_submit'))) {
            unset($_POST['re_password']);
            unset($_POST['service_provider_submit']);
            $_POST["categories_ids"] = $this->input->post('categories_ids') . ',';
            $_POST['password'] = md5($this->input->post('password'));
            $_POST['status'] = 0;
            $_POST['created_at'] = time();
            $_POST["photo"] = $this->image_upload("photo", "service_providers");
            $_POST["aadhar_photo"] = $this->image_upload("aadhar_photo", "service_providers");
            $_POST["pcc_photo"] = $this->image_upload("pcc_photo", "service_providers");
            $_POST["vaccination_certificate"] = $this->image_upload("vaccination_certificate", "service_providers");
            $check_email = $this->db->where("email", $this->input->post('email'))->get("service_providers")->result();
            $check = $this->db->where("phone", $this->input->post("phone"))->get("service_providers")->result();
            if (sizeof($check_email) > 0) {
                $this->session->set_flashdata('popup_error', "Account with this Email Already Exists");
            } else if (sizeof($check) > 0) {
                $this->session->set_flashdata('popup_error', "Account with this Phone Number Already Exists");
            } else {
                $insert = $this->my_model->insert_data("service_providers", $this->input->post());
                if ($insert) {
                    $this->session->set_userdata("popup_success", "<b>Service Provider </b>registration Successful.");
                } else {
                    $this->session->set_userdata("popup_error", "<b>Service Provider </b>registration Failed.");
                }
            }
            unset($_POST);
            redirect(base_url('business-registration'));
        }
        $data['states'] = $this->my_model->get_data("states");
        $data['cities'] = $this->my_model->get_data("cities");
        $data['services_categories'] = $this->my_model->get_data("services_categories");
        $this->load->view("bussiness_registrations", $data);
    }

    function getcities() {
        $state_id = $this->input->post('state_id');
        $this->db->where('state_id', $state_id);
        $query = $this->db->get('cities');
        $output = '<option value="">Select Cities *</option>';
        foreach ($query->result() as $row) {
            $output .= '<option value="' . $row->id . '">' . $row->city_name . '</option>';
        }
        print_r($output);
        exit;
    }

    function getPincodes() {
        $city_id = $this->input->post('city_id');
        $this->db->where('city_id', $city_id);
        $query = $this->db->get('pincodes');
        $output = '<option value="">Select Pincode *</option>';
        foreach ($query->result() as $row) {
            $output .= '<option value="' . $row->id . '">' . $row->pincode . '</option>';
        }
        print_r($output);
        exit;
    }

}
