<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Policies extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
    }

    function index() {
        redirect(base_url());
    }

    function terms_of_use() {
        $this->data['data'] = $this->my_model->get_data_row('content', array("id" => 1));
        $this->load->view("web/includes/header-copy", $this->data);
        $this->load->view('web/policies', $this->data);
        $this->load->view("web/includes/footer", $this->data);
    }

    function terms_of_sale() {
        $this->data['data'] = $this->my_model->get_data_row('content', array("id" => 36));
        $this->load->view("web/includes/header-copy", $this->data);
        $this->load->view('web/policies', $this->data);
        $this->load->view("web/includes/footer", $this->data);
    }

    function privacy_policy() {
        $this->data['data'] = $this->my_model->get_data_row('content', array("id" => 2));
        $this->load->view("web/includes/header-copy", $this->data);
        $this->load->view('web/policies', $this->data);
        $this->load->view("web/includes/footer", $this->data);
    }

    function core_values() {
        $this->data['data'] = $this->my_model->get_data_row('content', array("id" => 37));
        $this->load->view("web/includes/header-copy", $this->data);
        $this->load->view('web/policies', $this->data);
        $this->load->view("web/includes/footer", $this->data);
    }

}
