<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Contact extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
    }

    function index() {
        if (!empty($this->input->post())) {
            $_POST['user_id'] = USER_ID;
            $_POST['created_at'] = time();
            $ins = $this->my_model->insert_data("enquiries", $this->input->post());
            if ($ins) {
                $this->session->set_flashdata('success', "Enquiry Submitted, Thankyou for Contacting Us.");
            } else {
                $this->session->set_flashdata('error', "Failed to Submit Enquiry, try again.");
            }
            unset($_POST);
            redirect(base_url('contact'));
        }
        $this->my_view('contact-us', $this->data);
    }

}
