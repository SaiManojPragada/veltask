<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Wallet extends MY_Controller {

    public $data;

    function __construct() {
        $this->check_user_log(USER_ID);
        parent::__construct();
    }

    function index() {
        $this->data['page_name'] = "my_wallet";
        $user_data = $this->check_user_id(USER_ID);
        $this->db->order_by("id", "desc");
        $wallet_transactions = $this->my_model->get_data("wallet_transactions", "price != 0 and user_id = '" . USER_ID . "'");
        $this->data['wallet_amount'] = $user_data->wallet_amount;
        $this->data['wallet_transactions'] = $wallet_transactions;
        $this->my_view('user-wallet', $this->data);
    }

}
