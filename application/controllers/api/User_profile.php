<?php

class User_profile extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function profile_details() {
        $user_id = $this->input->post('user_id');
        $chk = $this->user->profileDetails($user_id);
        echo json_encode($chk);
    }

    function profile_image() {
        $user_id = $this->input->post("user_id");
        $check = $this->user->check_users($user_id);
        if (!$check['status']) {
            $resp = array("status" => false, "message" => "User Does not exists.");
            echo json_encode($resp);
            die;
        }
        $prev_file_image = $check['data']->image;
        $up_file = $this->image_upload('image', 'users', true, $prev_file_image);
        if ($up_file) {
            $this->my_model->update_data('users', array("id" => $user_id), array("image" => $up_file));
            $resp = array("status" => true, "message" => "Profile Image Updated.");
        } else {
            $resp = array("status" => false, "message" => "Something Went Wrong, Please Try Again.");
        }
        echo json_encode($resp);
    }

    function update_profile() {
        $user_id = $this->post('user_id');
        $data = array(
            "first_name" => $this->post('name'),
            "email" => $this->post('email'),
            "phone" => $this->post('mobile'),
            "landmark" => $this->post('landmark'),
            "city_id" => $this->post('city'),
            "state_id" => $this->post('state'),
            "pincode" => $this->post('zip_code')
        );
        $chk = $this->user->updateUserProfile($user_id, $data);
        echo json_encode($chk);
    }

    function check_user() {
        $user_id = $this->post('user_id');
        $chk = $this->user->check_users($user_id);
        echo json_encode($chk);
    }

    function add_address() {
        $user_data = $this->my_model->get_data_row("users", array("id" => $this->post('user_id')));
        if (empty($user_data)) {
            $resp = array("status" => false, "message" => "You are not authorised to perform this action.");
            $this->response($resp, REST_Controller::HTTP_OK);
            die;
        }
        $_POST['name'] = $user_data->first_name;
        $_POST['mobile'] = $user_data->phone;
        $_POST['created_at'] = time();
        $_POST['updated_at'] = time();
        $ins = $this->user->addUserAddress($this->post());
        echo json_encode($ins);
    }

    function get_addresses() {
        $user_data = $this->my_model->get_data_row("users", array("id" => $this->post('user_id')));
        if (empty($user_data)) {
            $resp = array("status" => false, "message" => "You does not exist.");
            echo json_encode($resp);
            die;
        }
        $resp = $this->user->getUserAddresses($this->post('user_id'));
        echo json_encode($resp);
    }

    function get_address() {
        $user_data = $this->my_model->get_data_row("users", array("id" => $this->post('user_id')));
        if (empty($user_data)) {
            $resp = array("status" => false, "message" => "You does not exist.");
            echo json_encode($resp);
            die;
        }
        $resp = $this->user->getUserAddress($this->post('user_id'), $this->post('id'));
        echo json_encode($resp);
    }

    function update_address() {
        $user_data = $this->my_model->get_data_row("users", array("id" => $this->post('user_id')));
        if (empty($user_data)) {
            $resp = array("status" => false, "message" => "You are not authorised to perform this action.");
            echo json_encode($resp);
            die;
        }
        $id = $this->post('id');
        $user_id = $this->post('user_id');
        unset($_POST['id']);
        unset($_POST['user_id']);
        $_POST['name'] = $user_data->first_name;
        $_POST['mobile'] = $user_data->phone;
        $_POST['updated_at'] = time();
        $ins = $this->user->updateAddressUser($this->post(), $id, $user_id);
        echo json_encode($ins);
    }

    function delete_address() {
        $user_data = $this->my_model->get_data_row("users", array("id" => $this->post('user_id')));
        if (empty($user_data)) {
            $resp = array("status" => false, "message" => "You are not authorised to perform this action.");
            echo json_encode($resp);
            die;
        }
        $resp = $this->user->deleteUserAddress($this->post('user_id'), $this->post('id'));
        echo json_encode($resp);
    }

}
