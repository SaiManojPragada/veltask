<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Services
 *
 * @author Admin
 */
class Services extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $user_id = $this->post('user_id');
        $this->check_user_id($user_id);
        $service_id = $this->post('service_id');

        $service = $this->my_model->get_data_row("services", array("id" => $service_id));
        if ($service) {
            $service->quantity = 0;
            $this->db->where("user_id", $user_id);
            $cart_items = $this->db->get("services_cart")->result();
            foreach ($cart_items as $citems) {
                if ($citems->service_id == $service->id) {
                    $service->quantity = $citems->quantity;
                }
            }
            $service->visit_and_quote_total = (float) $service->visiting_charges + $service->tax;
            $service_images = $this->my_model->get_data('services_images', array("service_id" => $service->id));
            foreach ($service_images as $image) {
                $image->image = base_url() . 'uploads/services/' . $image->image;
            }
            $this->db->select("avg(rating) as rating");
            $service->rating = number_format($this->my_model->get_data_row("services_reviews", array("service_id" => $service->id))->rating, 1);
            $reviews = $this->my_model->get_data("services_reviews", array("service_id" => $service->id, "status" => 1));
            foreach ($reviews as $rev) {
                $rev->rating = number_format($rev->rating, 1);
                $this->db->select("first_name,image");
                $user_data = $this->my_model->get_data_row("users", array("id" => $rev->user_id));
                $rev->user_name = $user_data->first_name;
                $rev->user_image = ($user_data->image != null) ? base_url() . "uploads/users/" . $user_data->image : "";
                $rev->created_at = date('d M Y, h:i A', $rev->created_at);
            }
            $this->db->select("count(id) as star_count");
            $this->db->where("rating = '5'");
            $five_stars = $this->my_model->get_data_row("services_reviews", array("service_id" => $service->id))->star_count;

            $this->db->select("count(id) as star_count");
            $this->db->where("(rating = '4.5' OR rating = '4')");
            $four_stars = $this->my_model->get_data_row("services_reviews", array("service_id" => $service->id))->star_count;

            $this->db->select("count(id) as star_count");
            $this->db->where("(rating = '3.5' OR rating = '3')");
            $three_stars = $this->my_model->get_data_row("services_reviews", array("service_id" => $service->id))->star_count;

            $this->db->select("count(id) as star_count");
            $this->db->where("(rating = '2.5' OR rating = '2')");
            $two_stars = $this->my_model->get_data_row("services_reviews", array("service_id" => $service->id))->star_count;

            $this->db->select("count(id) as star_count");
            $this->db->where("(rating = '1.5' OR rating = '1')");
            $one_star = $this->my_model->get_data_row("services_reviews", array("service_id" => $service->id))->star_count;
            $stars = array(
                "five_stars_count" => $five_stars,
                "five_stars_percentage" => (!$reviews) ? 0 : ($five_stars / sizeof($reviews)) * 100,
                "four_stars_count" => $four_stars,
                "four_stars_percentage" => (!$reviews) ? 0 : ($four_stars / sizeof($reviews)) * 100,
                "three_stars_count" => $three_stars,
                "three_stars_percentage" => (!$reviews) ? 0 : ($three_stars / sizeof($reviews)) * 100,
                "two_stars_count" => $two_stars,
                "two_stars_percentage" => (!$reviews) ? 0 : ($two_stars / sizeof($reviews)) * 100,
                "one_stars_count" => $one_star,
                "one_stars_percentage" => (!$reviews) ? 0 : ($one_star / sizeof($reviews)) * 100,
            );
            $service->stars = ($stars) ? $stars : array();
            $service->total_reviews = sizeof($reviews);
            $service->reviews = ($reviews) ? $reviews : array();
            $service->service_images = $service_images;
            $arr = array(
                'status' => true,
                "data" => $service
            );
        } else {
            $arr = array(
                'status' => false,
                "message" => "No Service Found"
            );
        }
        echo json_encode($arr);
    }

}
