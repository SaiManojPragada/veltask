<?php

class App_data extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('services_model');
        $this->load->model('homepage_model');
    }

    function homepage() {
        $user_id = $this->input->post('user_id');
        $this->clear_prev_visit_and_quote($user_id);
        $lat = $this->input->post('lat');
        $lng = $this->input->post('lng');
        $chk = $this->homepage_model->getHomeScreen($user_id, $lat, $lng);
        echo json_encode($chk);
    }

    function services() {
        $cat_id = $this->input->post('category_id');
        $user_id = $this->input->post('user_id');
        $data = $this->services_model->get_filter_services($cat_id, $user_id);
        echo json_encode($data);
    }

    function clear_prev_visit_and_quote($user_id) {
        $check_cart_for_visit_and_quote = $this->my_model->get_data("services_cart", array("user_id" => $user_id, "has_visit_and_quote" => 1));
        if (!empty($check_cart_for_visit_and_quote)) {
            $this->my_model->delete_data("services_cart", array("user_id" => $user_id));
        }
        return true;
    }

}
