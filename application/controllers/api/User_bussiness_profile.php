<?php

class User_bussiness_profile extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $user_id = $this->post("user_id");
        $this->check_user_id($user_id);
        $bussiness_profile = $this->my_model->get_data_row("users_bussiness_details", array("user_id" => $user_id));
        if (!empty($bussiness_profile)) {
            $bussiness_profile->document = base_url() . 'uploads/company_proofs/' . $bussiness_profile->document;
            $bussiness_profile->submitted_at = date('d M Y, h:i A', $bussiness_profile->updated_at);
            $bussiness_profile->profile_status = ($bussiness_profile->status) ? "Approved" : "Under Review";
            unset($bussiness_profile->status);
            unset($bussiness_profile->created_at);
            unset($bussiness_profile->updated_at);

            $arr = array(
                "status" => true,
                "data" => $bussiness_profile
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "No bussiness profile Found."
            );
        }
        echo json_encode($arr);
    }

    function add_profile() {
        $user_id = $this->input->post("user_id");
        $this->check_user_id($user_id);
        $company_name = $this->post("company_name");
        $company_address = $this->post("company_address");
        $proof_type = $this->post("proof_type");

        if (empty($company_name)) {
            $arr = array(
                "status" => false,
                "message" => "Company Name is Required"
            );
            echo json_encode($arr);
            die;
        }
        if (empty($company_address)) {
            $arr = array(
                "status" => false,
                "message" => "Company Address is Required"
            );
            echo json_encode($arr);
            die;
        }
        if (empty($proof_type)) {
            $arr = array(
                "status" => false,
                "message" => "Proof Type is Required"
            );
            echo json_encode($arr);
            die;
        }

        $document = $this->image_upload("document", "company_proofs");
        $data = array(
            "user_id" => $user_id,
            "company_name" => $company_name,
            "company_address" => $company_address,
            "proof_type" => $proof_type,
            "document" => $document,
            "created_at" => time(),
            "updated_at" => time()
        );

        $check = $this->my_model->get_data_row("users_bussiness_details", array("user_id" => $user_id));
        if (!empty($check)) {
            if (empty($data['document'])) {
                unset($data['document']);
            } else {
                unlink('uploads/company_proofs/' . $check->document);
            }
            $update = $this->my_model->update_data("users_bussiness_details", array("user_id" => $user_id), $data);
            if ($update) {
                $arr = array(
                    "status" => true,
                    "message" => "Bussiness Profile updated"
                );
            } else {
                $arr = array(
                    "status" => false,
                    "message" => "Unable to Update Bussiness Profile"
                );
            }
        } else {
            if (empty($data['document'])) {
                $arr = array(
                    "status" => false,
                    "message" => "Unable to Upload document please try again."
                );
                echo json_encode($arr);
                die;
            }
            $ins = $this->my_model->insert_data("users_bussiness_details", $data);
            if ($ins) {
                $arr = array(
                    "status" => true,
                    "message" => "Bussiness Profile submitted"
                );
            } else {
                $arr = array(
                    "status" => false,
                    "message" => "Unable to Submit Bussiness Profile"
                );
            }
        }
        echo json_encode($arr);
    }

}
