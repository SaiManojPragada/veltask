<?php

class Authentication extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function user_login() {
        $phone = $this->input->post('phone');
        $token = $this->input->post('token');
        $login_type = $this->input->post('login_type');
        unset($_POST['login_type']);
        if (empty($phone)) {
            $arr = array(
                "status" => false,
                "message" => "Phone Number is Required"
            );
            echo json_encode($arr);
            die;
        }
        if ($login_type !== 'web') {
            if (empty($token)) {
                $arr = array(
                    "status" => false,
                    "message" => "Token is Required"
                );
                echo json_encode($arr);
                die;
            }
        }
        $data = array('phone' => $phone, 'token' => $token);
        $chk = $this->user->doRegister($data);
        if ($chk == 'error') {
            echo json_encode($chk);
        } else {
            echo json_encode($chk);
        }
    }

    function profile_and_referral() {
        $user_id = $this->input->post('user_id');
        $this->check_user_id($user_id);
        $referral_code = $this->input->post("referral_code");
        $name = $this->input->post("name");
        $email = $this->input->post("email");
        $inp_data = array(
            "first_name" => $name,
            "email" => $email
        );
        if (!empty($referral_code)) {
            $referel_user = $this->my_model->get_data_row("users", array("referral_code" => $referral_code));
            if (empty($referel_user)) {
                $arr = array(
                    "status" => false,
                    "message" => "Invalid Referral Code"
                );
                echo json_encode($arr);
                die;
            }
            $referal_user_id = $referel_user->id;
            $inp_data['reffered_by'] = $referal_user_id;
        }
        $update = $this->my_model->update_data("users", array("id" => $user_id), $inp_data);
        if ($update) {
            $arr = array(
                "status" => true,
                "message" => "User Profile updated Succesfully"
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "User Profile Updating Failed"
            );
        }
        echo json_encode($arr);
    }

}
