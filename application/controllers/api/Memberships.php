<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Memberships
 *
 * @author Admin
 */
class Memberships extends MY_Controller {

    //put your code here
    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->check_user_id($this->post('user_id'));
        $data = $this->my_model->get_data("memberships", "", "", "", true);
        if ($data) {
            foreach ($data as $item) {
                unset($item->created_at);
                unset($item->updated_at);
                $duration = $this->my_model->get_data_row("term_months", array("id" => $item->duration_id));
                $item->duration_in_days_count = $duration->duration;
                $item->duration_in_words = $duration->duration_in_words;

                $description = str_replace('<li>', '<li>✓ ', $item->description);
                $item->description = strip_tags($description);
            }
            $arr = array(
                "status" => true,
                "data" => $data
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "No Memberships Found"
            );
        }
        echo json_encode($arr);
    }

    function view_membership() {
        $this->check_user_id($this->post('user_id'));
        $membership_id = $this->input->post("membership_id");
        $data = $this->my_model->get_data_row("memberships", array("id" => $membership_id));
        if ($data) {
            unset($data->created_at);
            unset($data->updated_at);
            $duration = $this->my_model->get_data_row("term_months", array("id" => $data->duration_id));
            $data->duration_in_days_count = $duration->duration;
            $data->duration_in_words = $duration->duration_in_words;
            $arr = array(
                "status" => true,
                "data" => $data
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "No Memberships Found"
            );
        }
        echo json_encode($arr);
    }

    function subscribe() {
        $user_id = $this->post('user_id');
        $this->check_user_id($user_id);
        $membership_id = $this->post('membership_id');
        if (!empty($membership_id)) {
            $arr = array(
                "status" => false,
                "messgae" => "Invalid Membership Id"
            );
        }
        $membership_transaction_id = generateRandomString(20);
        $razorpay_order_id = $this->post('razorpay_order_id');
        if (!empty($razorpay_order_id)) {
            $arr = array(
                "status" => false,
                "messgae" => "Invalid Razorpay Id"
            );
        }
        $razorpay_transaction_id = $this->post('razorpay_transaction_id');
        if (!empty($razorpay_transaction_id)) {
            $arr = array(
                "status" => false,
                "messgae" => "Invalid Razorpay Transaction Id"
            );
        }
        $amount_paid = $this->post('amount_paid');
        if (!empty($amount_paid)) {
            $arr = array(
                "status" => false,
                "messgae" => "Invalid Amount"
            );
        }
        $membership_data = $this->my_model->get_data_row("memberships", array("id" => $membership_id));
        $duration = $this->my_model->get_data_row("term_months", array("id" => $membership_data->duration_id))->duration;
        $created_at = time();
        $expiry_date = strtotime('+' . $duration . ' days');
        $data = array(
            "user_id" => $user_id,
            "membership_id" => $membership_id,
            "membership_discount" => $membership_data->discount_percentage,
            "max_discount" => $membership_data->max_discount,
            "membership_transaction_id" => $membership_transaction_id,
            "razorpay_order_id" => $razorpay_order_id,
            "razorpay_transaction_id" => $razorpay_transaction_id,
            "amount_paid" => $amount_paid,
            "created_at" => $created_at,
            "expiry_date" => $expiry_date,
            "status" => true
        );
        $this->my_model->update_data("membership_transactions", array("user_id" => $user_id), array("status" => 0));
        $ins = $this->my_model->insert_data("membership_transactions", $data);
        if ($ins) {
            $arr = array(
                "status" => true,
                "message" => "Subsription Purchase Successfull"
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "Subsription Purchase Failed"
            );
        }
        echo json_encode($arr);
    }

    public function check_active_subscription() {
        $user_id = $this->post('user_id');
        $this->check_user_id($user_id);
        $this->db->where("expiry_date >=", time());
        $this->db->order_by("id", "desc");
        $check = $this->my_model->get_data_row("membership_transactions", array("user_id" => $user_id));
        if (!empty($check)) {
            if ($check->expiry_date >= time()) {
                $arr = array(
                    "status" => false,
                    "message" => "You have an Active Subscription"
                );
            } else {
                $arr = array(
                    "status" => true,
                    "message" => "You Subscription has Expired"
                );
            }
        } else {
            $arr = array(
                "status" => true,
                "message" => "No Active Subscription found"
            );
        }
        echo json_encode($arr);
    }

    public function get_user_subscriptions() {
        $user_id = $this->post('user_id');
        $this->check_user_id($user_id);
        $this->db->order_by("id","desc");
        $user_transactions = $this->my_model->get_data("membership_transactions", array("user_id" => $user_id));
        if ($user_transactions) {
            foreach ($user_transactions as $transaction) {
                if ($transaction->expiry_date >= strtotime("+ 7 days")) {
                    $transaction->subscription_status = "Active";
                } else if($transaction->expiry_date <= strtotime("+ 7 days") && $transaction->expiry_date >= time()){
                    $transaction->subscription_status = "Expiring Soon";
                }else {
                    $transaction->subscription_status = "Expired";
                }
                $transaction->created_at = date('d M Y, h:i A', $transaction->created_at);
                $transaction->expiry_date = date('d M Y', $transaction->expiry_date);
                $membership_details = $this->my_model->get_data_row("memberships", array("id" => $transaction->membership_id));
                $membership_details->description = str_replace('<li>', '<li>✓ ', $membership_details->description);
                $membership_details->description = strip_tags($membership_details->description);
                $duration = $this->my_model->get_data_row("term_months", array("id" => $membership_details->duration_id));
                $membership_details->duration_in_days_count = $duration->duration;
                $membership_details->duration_in_words = $duration->duration_in_words;

                $transaction->title = $membership_details->title;
                $transaction->description = $membership_details->description;
                $transaction->sale_price = $membership_details->sale_price;
                $transaction->price = $membership_details->price;
                $transaction->discount_percentage = $membership_details->discount_percentage;
                $transaction->duration_in_words = $membership_details->duration_in_words;
                // $transaction->membership_details = $membership_details;
                unset($membership_details->duration_id);
                unset($membership_details->created_at);
                unset($membership_details->updated_at);
                unset($membership_details->status);
            }
            $arr = array(
                "status" => true,
                "data" => $user_transactions
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "No Subscriptions History found"
            );
        }
        echo json_encode($arr);
    }

}
