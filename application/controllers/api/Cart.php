<?php

class Cart extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('cart_services_model');
    }

    function cart_service() {
        $this->check_user_id($this->post('user_id'));
        $resp = $this->cart_services_model->addUpdateCart($this->post());
        echo json_encode($resp);
    }

    function get_cart($internal = "") {
        $user_id = $this->post('user_id');
        $has_visit_and_quote = $this->post('has_visit_and_quote');
        $user_data = $this->check_user_id($this->post('user_id'));
        $cart = $this->my_model->get_data("services_cart", array("user_id" => $this->post('user_id')), "id", "desc");
        if ($cart) {
            $related_where = "";
            foreach ($cart as $cc) {
                $this->db->where("id", $cc->service_id);
                $product_details = $this->db->get("services")->row();
                $cc->service_name = $product_details->service_name;
                $cc->description = $product_details->description;
                $this->db->where("service_id", $cc->variant_id);
                $images = $this->db->get("services_images")->result();
                foreach ($images as $img) {
                    $img->image = base_url('uploads/services/') . $img->image;
                }
                $cc->product_images = $images;
                $related_where .= "id != '" . $cc->service_id . "' AND ";
            }
            $related_where = "(" . rtrim($related_where, 'AND ') . ")";
            $this->db->where($related_where);
//            $this->db->order_by("RAND()");
            $this->db->where("cat_id", $cart[0]->category_id);
            $this->db->order_by("id", "desc");
            $this->db->limit(10);
            $related_services = $this->db->get("services")->result();

            foreach ($related_services as $item) {

                foreach ($cart as $cc) {
                    if ($cc->service_id == $item->id) {
                        $item->quantity = $cc->quantity;
                    } else {
                        $item->quantity = "0";
                    }
                }
                $this->db->where("service_id", $item->id);
                $this->db->order_by("id", "asc");
                $images = $this->db->get("services_images")->row();
//                foreach ($images as $img) {
                $images->image = base_url('uploads/services/') . $images->image;
//                }
                $item->image = $images->image;
            }
            $membership_discount = 0;
            $business_discount = 0;
            $this->db->where("user_id", $this->post("user_id"));
            $this->db->select("SUM(grand_total) as total, SUM(sub_total)as sub_total, SUM(tax) as tax,SUM(visiting_charges) as visiting_charges");
            $cart_totals = $this->db->get("services_cart")->row();

            $businees_profile = $this->my_model->get_data_row("users_bussiness_details", array("user_id" => $user_id, 'status' => 1));
            if ($has_visit_and_quote != 1) {
                if (empty($businees_profile)) {
                    $this->db->order_by("id", "desc");
                    $this->db->where("expiry_date >=", time());
                    $user_membership_data = $this->my_model->get_data_row("membership_transactions", array("user_id" => $this->post('user_id')));
                    if (!empty($user_membership_data)) {
                        $membership_discount = (float) ($cart_totals->sub_total / 100) * $user_membership_data->membership_discount;
                        if ($membership_discount > $user_membership_data->max_discount) {
                            $membership_discount = $user_membership_data->max_discount;
                        }
                    }
                } else {
                    $business__benifits = $this->my_model->get_data_row("business_profile_benifits");
                    $discount_percentage = !empty($business__benifits) ? $business__benifits->services_discount : 0;
                    if ($discount_percentage > 0) {
                        $business_discount = floor((float) ($cart_totals->sub_total / 100) * $discount_percentage);
                    }
                }
            }
            $complete_discount = $membership_discount + $business_discount;
            $cart_totals->total = round((float) $cart_totals->total - $complete_discount, 2);
//            $cart_totals->sub_total = (float) $cart_totals->total - ($cart_totals->visiting_charges + $cart_totals->tax);
            $cart_totals->sub_total = round((float) $cart_totals->sub_total, 2);
            $cart_totals->visiting_charges = round((float) $cart_totals->visiting_charges, 2);
            $cart_totals->tax = round((float) $cart_totals->tax, 2);
            $cart_totals->membership_discount = round($membership_discount, 2);
            $cart_totals->business_discount = round($business_discount, 2);

            $wallet_amount = $user_data->wallet_amount;
            if ($has_visit_and_quote == 1) {
                $cart_totals->sub_total = $cart_totals->visiting_charges;
                $cart_totals->total = $cart_totals->visiting_charges + $cart_totals->tax;
            }
            $this->db->where("expiry_date >='" . date('Y-m-d') . "'");
            $coupon_codes = $this->my_model->get_data("services_coupon_codes", array("status" => true));
            $min_cart_amount = $this->my_model->get_data_row('cart_min_amount')->services_min_amount;
            $arr = array(
                "status" => true,
                "message" => "Cart Item",
                "data" => array(
                    "wallet_amount" => $wallet_amount,
                    "no_of_coupons_available" => sizeof($coupon_codes),
                    "item_count" => sizeof($cart),
                    "cart_total_amount" => $cart_totals->total,
                    "items" => $cart,
                    "related_services" => $related_services,
                    "cart_totals" => $cart_totals,
                    "enable_cart" => ($cart_totals->total > $min_cart_amount) ? true : false
                )
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "No Items Found in your Cart"
            );
        }
        if (empty($internal)) {
            echo json_encode($arr);
        }
        return $arr;
    }

    function apply_coupon_and_wallet() {
        $user_id = $this->post('user_id');
        $user_data = $this->check_user_id($user_id);
        $use_wallet = $this->input->post("use_wallet");
        $coupon_code = $this->input->post("coupon_code");
        $wallet_amount = $user_data->wallet_amount;
        $current_wallet_amount = 0;
        $balance_wallet_amount = 0;
        $balance_cart_amount = 0;
        if (empty($use_wallet)) {
            $arr = array(
                "status" => false,
                "message" => "Enter Use Wallet"
            );
            echo json_encode($arr);
            die;
        }
        unset($_POST['user_wallet']);
        unset($_POST['coupon_code']);
        $user_cart_total = $this->get_cart("YES");
        $message = "";
        $arr = array();
        $arr['status'] = true;
        if (!empty($user_cart_total)) {
            $data = $user_cart_total['data'];
            $cart_totals = $data['cart_totals'];
            $total = $cart_totals->total;
            $sub_total = $cart_totals->sub_total;
            $tax = $cart_totals->tax;
            $visiting_charges = $cart_totals->visiting_charges;
            $bussiness_discount = $cart_totals->business_discount;
            $membership_discount = $cart_totals->membership_discount;
            if (!empty($coupon_code)) {
                $today = date('Y-m-d');
                $where = "(coupon_code = '" . $coupon_code . "' AND start_date <= '" . $today . "' AND expiry_date >= '" . $today . "') AND status = 1";
                $coupon_data = $this->my_model->get_data_row("services_coupon_codes", $where);
                if (!empty($coupon_data)) {
                    $where = array(
                        "coupon_code" => $coupon_code,
                        "user_id" => $user_id
                    );
                    $check_uitlization = $this->my_model->get_data("services_orders", $where);
//                    echo json_encode($coupon_data->utilization);
//                    die;
                    if (sizeof($check_uitlization) < $coupon_data->utilization) {
                        if ((float) $sub_total < (float) $coupon_data->minimum_order_amount) {
                            $arr['status'] = false;
                            $message = "This Coupon Code can only applied on orders above Rs." . $coupon_data->minimum_order_amount;
                        } else {
                            $cp_ds = floor(($sub_total * $coupon_data->percentage) / 100);
                            $coupon_discount = ($cp_ds < $coupon_data->maximum_amount) ? $cp_ds : $coupon_data->maximum_amount;
                            $message = "Yay you saved Rs." . $coupon_discount;
                            $coupon_id = $coupon_data->id;

                            $total = $total - $coupon_discount;
                        }
                    } else {
                        $arr['status'] = false;
                        $message = "Coupon usage limit has been reached";
                    }
                } else {
                    $arr['status'] = false;
                    $message = "Invalid Coupon Code";
                }
            } else {
                $message = "";
            }
            $applied_amount = 0;
            $balance_wallet_amount = (float) $wallet_amount;
            if ($use_wallet == "Yes") {
                $temp_total = $total;
                $total = $total - $wallet_amount;
                $total = ($total > 0) ? $total : 0;
                $balance_wallet_amount = $wallet_amount - $temp_total;
                $balance_wallet_amount = ($balance_wallet_amount > 0) ? $balance_wallet_amount : 0;
                $applied_amount = $wallet_amount - $balance_wallet_amount;
                $applied_amount = ($applied_amount > 0) ? $applied_amount : $temp_total;
            }
            $arr['message'] = $message;
            $arr['data'] = array(
                "message" => $message,
                "balance_wallet_amount" => round($balance_wallet_amount, 2),
                "wallet_applied_amount" => round($applied_amount, 2),
                "coupon_id" => ($coupon_id) ? $coupon_id : "",
                "coupon_code" => $coupon_code,
                "coupon_discount" => (string) round($coupon_discount, 2),
                "sub_total" => $sub_total,
                "tax" => round($tax, 2),
                "visiting_charges" => round($visiting_charges, 2),
                "membership_discount" => round($membership_discount, 2),
                "business_discount" => round($bussiness_discount, 2),
                "grand_total" => ($total < 0) ? 0 : round($total, 2)
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "No Items In Your Cart"
            );
        }
        echo json_encode($arr);
    }

    function delete_cart() {
        $this->check_user_id($this->post('user_id'));
        $this->db->where("user_id", $this->post('user_id'));
        if ($this->db->delete("services_cart")) {
            $arr = array(
                "status" => true,
                "message" => "Items in your cart has been Deleted"
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "Unable to Delete the Items in your Cart"
            );
        }
        echo json_encode($arr);
    }

    function apply_coupon() {
        $user_id = $this->input->post("user_id");
        $this->check_user_id($user_id);
        $coupon_code = $this->input->post("coupon_code");
        $where = array(
            "coupon_code" => $coupon_code,
            "user_id" => $user_id
        );
        $cart = $this->my_model->get_data_row("services_orders", $where);
        $today = date('Y-m-d');
        $where = "(coupon_code = '" . $coupon_code . "' AND start_date <= '" . $today . "' AND expiry_date >= '" . $today . "') AND status = 1";
        $coupon_data = $this->my_model->get_data_row("services_coupon_codes", $where);
        if ($coupon_data) {
            if (sizeof($cart) < $coupon_data->utilization) {
                $this->db->select("SUM(grand_total) as grand_total,SUM(sub_total) as subtotal, SUM(tax) as tax, SUM(visiting_charges) as visiting_charges");
                $this->db->where("user_id", $user_id);
                $cart_totals = $this->db->get("services_cart")->row();
                if ((float) $cart_totals->subtotal < (float) $coupon_data->minimum_order_amount) {
                    $arr = array(
                        "status" => false,
                        "message" => "This Coupon Code can only applied on orders above Rs." . $coupon_data->minimum_order_amount
                    );
                } else {
                    $cp_ds = ($cart_totals->subtotal * $coupon_data->percentage) / 100;
                    $coupon_discount = ($cp_ds < $coupon_data->maximum_amount) ? $cp_ds : $coupon_data->maximum_amount;
                    $arr = array(
                        "status" => true,
                        "message" => "Coupon Applied",
                        "data" => array(
                            "coupon_id" => $coupon_data->id,
                            "coupon_code" => $coupon_code,
                            "coupon_discount" => (string) $coupon_discount,
                            "sub_total" => $cart_totals->subtotal,
                            "tax" => $cart_totals->tax,
                            "visiting_charges" => $cart_totals->visiting_charges,
                            "grand_total" => (string) (($cart_totals->subtotal - $coupon_discount) + ($cart_totals->visiting_charges +
                            $cart_totals->tax))
                        )
                    );
                }
            } else {
                $arr = array(
                    "status" => false,
                    "message" => "Coupon usage limit has been reached"
                );
            }
        } else {
            $arr = array(
                "status" => false,
                "message" => "Invalid Coupon Code"
            );
        }
        echo json_encode($arr);
    }

    function use_wallet_amount() {
        $user_id = $this->post('user_id');
        $user_data = $this->check_user_id($user_id);
        $wallet_amount = $user_data->wallet_amount;
        $cart_total = $this->my_model->get_total("services_cart", "grand_total", array("user_id" => $user_id))->grand_total;
        if ($cart_total > 0) {
            $balance_wallet_amount = $wallet_amount - $cart_total;
            $balance_cart_amount = $cart_total - $wallet_amount;
            $balance_wallet_amount = ($balance_wallet_amount > 0) ? $balance_wallet_amount : 0;
            $arr = array(
                "status" => true,
                "data" => array(
                    "balance_wallet_amount" => $balance_wallet_amount,
                    "applied_amount" => $wallet_amount - $balance_wallet_amount,
                    "balance_amount" => ($balance_cart_amount < 0) ? 0 : $balance_cart_amount
                )
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "Your cart is Empty"
            );
        }
        echo json_encode($arr);
    }

    function cart_counts() {
        $user_id = $this->input->post("user_id");
        $sid = $this->input->post("sid");
        $this->check_user_id($user_id);
        $service_cart = $this->my_model->get_data("services_cart", array("user_id" => $user_id));
        $e_comm_cart = $this->my_model->get_data("cart", array("user_id" => $user_id, 'session_id' => $sid));
        $data = array(
            "status" => true,
            "data" => array(
                "service_cart_total" => !empty($service_cart) ? sizeof($service_cart) : 0,
                "e_commerce_cart_total" => !empty($e_comm_cart) ? sizeof($e_comm_cart) : 0
            )
        );
        echo json_encode($data);
        die;
    }

//    function use_wallet_amount() {
//        $user_id = $this->post('user_id');
//        $user_data = $this->check_user_id($user_id);
//        $grand_total = $this->post('grand_to');
//        $wallet_amount = $user_data->wallet_amount;
//    }
}
