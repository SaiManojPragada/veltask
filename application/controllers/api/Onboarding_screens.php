<?php

class Onboarding_screens extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->db->where("status", true);
        $this->db->order_by("priority", "asc");
        $result = $this->db->get("onboard_screens")->result();
        foreach ($result as $item) {
            $item->image = base_url('uploads/onboard_screens/') . $item->image;
        }
        if ($result) {
            $resp = array("status" => true, "message" => "Onboard Screens.", "data" => $result);
        } else {
            $resp = array("status" => false, "message" => "Something Went Wrong, Please Try Again.");
        }
        echo json_encode($resp);
    }

}
