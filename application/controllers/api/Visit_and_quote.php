<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Visit_and_quote
 *
 * @author vincent
 */
class Visit_and_quote extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function get_quotation_with_milestones() {
        $user_id = $this->post('user_id');
        $this->check_user_id($user_id);
        $quotation_id = $this->post('quotation_id');
        if (empty($quotation_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid Quotation Id."
            );
            echo json_encode($arr);
            die;
        }
        $quotation = $this->my_model->get_data_row("visit_and_quote_quotaions", array("id" => $quotation_id));
        if (!empty($quotation)) {
            $quotation->amount_paid = (float) ($quotation->quotation_amount - $quotation->balance_amount);
            $quotation->milestones = $this->my_model->get_data("quotation_milestones", array("quotation_id" => $quotation_id));
            foreach ($quotation->milestones as $stone) {
                $stone->date = date('d-m-Y', strtotime($stone->date));
            }
            $quotation->created_at = date('d M Y, h:i A', $quotation->created_at);
            $quotation->updated_at = date('d M Y, h:i A', $quotation->updated_at);
            $arr = array(
                "status" => true,
                "data" => $quotation
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "Quotation not Found."
            );
        }
        echo json_encode($arr);
    }

    function accept_quotation() {
        $user_id = $this->post('user_id');
        $this->check_user_id($user_id);
        $quotation_id = $this->post('quotation_id');
        if (empty($quotation_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid Quotation Id."
            );
            echo json_encode($arr);
            die;
        }
        $data = array(
            "is_selected_by_user" => 1
        );
        $update = $this->my_model->update_data("visit_and_quote_quotaions", array("id" => $quotation_id), $data);
        if ($update) {
            $arr = array(
                "status" => true,
                "message" => "This Quotation has been accepted."
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "Unable to process your request, Try Again."
            );
        }
        echo json_encode($arr);
    }

    function pay_milestones() {
        $user_id = $this->post('user_id');
        $this->check_user_id($user_id);
        $quotation_id = $this->post('quotation_id');
        $milestone_id = $this->input->post('milestone_id');
        if (empty($quotation_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid Quotation Id."
            );
            echo json_encode($arr);
            die;
        }
        $milestone_data = $this->my_model->get_data_row("quotation_milestones", array("id" => $milestone_id));
        if ($milestone_data->status == 'paid') {
            $arr = array(
                "status" => false,
                "message" => "This milestone is already paid."
            );
            echo json_encode($arr);
            die;
        }
        $where = array(
            "quotation_id" => $quotation_id
        );
        $check = $this->my_model->get_data_row("visit_and_quote_quotaions", array("id" => $quotation_id));
        $grand_total = $check->quotation_amount;
        $existing_balance_amount = $check->balance_amount;
        if (empty($check)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid Quoation Id"
            );
            echo json_encode($arr);
            die;
        }
        if ($milestone_id != "all") {
            $where['id'] = $milestone_id;
            $no_of_milestones_paid = $check->no_of_milestones_paid + 1;
            $paid_milestones_amount = $this->my_model->get_total("quotation_milestones", 'amount', array("quotation_id" => $quotation_id, "status" => "paid"));
            $amount_paid = $paid_milestones_amount + $milestone_data->amount - 1;
        } else {
            $where['status'] = 'unpaid';
            $no_of_milestones_paid = $check->no_of_milestones;
            $amount_paid = $existing_balance_amount;
        }
        $balance_amount = $existing_balance_amount - $amount_paid;
        $data = array("status" => 'paid', 'updated_at' => time(), 'razorpay_transaction_id' => $this->post('razorpay_transaction_id'),
            'razorpay_order_id' => $this->post('razorpay_order_id'));
        $update = $this->my_model->update_data("quotation_milestones", $where, $data);
        if ($update) {
            $service_order = $this->my_model->get_data_row("services_orders", array("id" => $check->service_order_id));
            $this->my_model->update_data("visit_and_quote_quotaions", array("id" => $quotation_id), array("balance_amount" => $balance_amount, "no_of_milestones_paid" => $no_of_milestones_paid));
            $amount_paid = $service_order->amount_paid + $amount_paid;
            $this->my_model->update_data("services_orders", array("id" => $service_order->id), array("balance_amount" => $balance_amount, "grand_total" => $grand_total, "amount_paid" => $amount_paid));

            $this->load->model('service_provider_notifications_model');
            $this->service_provider_notifications_model->send_amount_paid($service_order->id, $amount_paid);
            $arr = array(
                "status" => true,
                "message" => "Quotation Milestones Succesfully updated"
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "Unable to update the Quotation Milestones."
            );
        }
        echo json_encode($arr);
        die;
    }

}
