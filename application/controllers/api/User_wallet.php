<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class User_wallet extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $user_id = $this->input->post("user_id");
        $user_data = $this->check_user_id($user_id);
        $wallet_amount = $user_data->wallet_amount;
        $this->db->select("id,transaction_id,user_id,price,message,status,created_at");
        $this->db->where("(status = 'credit' OR status = 'debit') AND price != 0");
        $this->db->order_by("id", "desc");
        $wallet_transactions = $this->my_model->get_data("wallet_transactions", array("user_id" => $user_id));
        if ($wallet_transactions) {
            foreach ($wallet_transactions as $trans) {
                $trans->created_at = date('d M Y, h:i A');
            }
            $arr = array(
                "status" => true,
                "data" => array(
                    "wallet_amount" => $wallet_amount,
                    "wallet_transactions" => $wallet_transactions
                )
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "No Wallet transaction History Found.",
                "data" => array(
                    "wallet_amount" => $wallet_amount
                )
            );
        }
        echo json_encode($arr);
    }

}
