<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Contact extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
    }

    function index() {
        $user_id = $this->input->post("user_id");
        $this->check_user_id($user_id);
        $name = $this->input->post("name");
        $mobile = $this->input->post("mobile");
        $email = $this->input->post("email");
        $message = $this->input->post("message");
        $inp_data = array(
            "user_id" => $user_id,
            "name" => $name,
            "mobile" => $mobile,
            "email" => $email,
            "message" => $message,
            "created_at" => time()
        );
        $ins = $this->my_model->insert_data("enquiries", $inp_data);
        if ($ins) {
            $arr = array(
                "status" => true,
                "message" => "Your message has been submitted"
            );
        } else {
            $arr = array(
                "status" => true,
                "message" => "Unable to submit the message"
            );
        }
        echo json_encode($arr);
    }

}
