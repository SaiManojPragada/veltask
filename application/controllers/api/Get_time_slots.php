<?php

class Get_time_slots extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $date = $this->input->post("date");
        $today = date('d-m-Y');

        if (strtotime($date) < strtotime($today)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid Date"
            );
            echo json_encode($arr);
            die;
        }

        if ($date == $today) {
            $difference = $this->my_model->get_data_row("time_slots_difference")->difference;
            $time = date("H:i", (strtotime(date('h:i A')) + ($difference * 60 * 60)));
            if (strtotime($time) < strtotime(date('H:i'))) {
                $arr = array(
                    "status" => false,
                    "message" => "No Time Slots For this Date"
                );

                echo json_encode($arr);
                die;
            }
            $this->db->where('time_slot > "' . $time . '"');
        }
        $res = $this->db->get("services_time_slots")->result();
        if ($res && sizeof($res) > 1) {
            foreach ($res as $item) {
                $item->time_slot = date('h:i A', strtotime($item->time_slot));
            }
            $arr = array(
                "status" => true,
                "message" => "Time Slots Found",
                "data" => $res
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "No Time Slots For this Date"
            );
        }

        echo json_encode($arr);
    }

}
