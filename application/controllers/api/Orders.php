<?php

class Orders extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $user_id = $this->input->post('user_id');
        $type = $this->input->post('type');
        $this->check_user_id($user_id);
        if (empty($type)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid Order Type Provided"
            );

            echo json_encode($arr);
            die;
        }
        if ($type == "ongoing") {
            $this->db->where("order_status !='order_rejected' AND order_status !='order_completed'  AND order_status !='order_cancelled' AND order_status !='refunded'");
        } else if ($type == "completed") {
            $this->db->where("order_status ", 'order_completed');
        } else if ($type == "cancelled") {
            $this->db->where("order_status ='order_rejected' OR order_status ='order_cancelled' OR order_status ='refunded'");
        }
        $orders = $this->my_model->get_data("services_orders", array("user_id" => $user_id), "id", "desc");
        if ($orders) {
            foreach ($orders as $od) {
                $od->quotation_id = "";
                if ($od->has_visit_and_quote == "Yes") {
                    $check_quotation = $this->my_model->get_data_row("visit_and_quote_quotaions", array("service_order_id" => $od->id));
                    if (!empty($check_quotation)) {
                        $od->quotation_id = $check_quotation->id;
                    }
                }
                $od->payment_type_name = $this->my_model->get_data_row("payment_methods_enables", array("id" => $od->payment_type))->payment_method_title;
                $od->created_at = ($od->created_at) ? date('d M Y, h:i A', $od->created_at) : '';
                $od->accepted_at = ( $od->accepted_at) ? date('d M Y, h:i A', $od->accepted_at) : '';
                $od->rejected_at = ( $od->rejected_at) ? date('d M Y, h:i A', $od->rejected_at) : '';
                $od->completed_at = ( $od->completed_at) ? date('d M Y, h:i A', $od->completed_at) : '';
                $od->cancelled_at = ( $od->cancelled_at) ? date('d M Y, h:i A', $od->cancelled_at) : '';
                $od->time_slot_date = date("d M Y", strtotime($od->time_slot_date));
                $od->category_name = $this->my_model->get_data_row("services_categories", array("id" => $od->ordered_categories))->name;
                $od->order_items = sizeof($this->my_model->get_data("service_orders_items", array("order_id" => $od->order_id)));
                $od->order_status_text = ucwords(str_replace("_", " ", $od->order_status));
                if ((int) $od->grand_total <= (int) $od->amount_paid) {
                    $od->payment_status = "Paid";
                } else if ($od->amount_paid == 0) {
                    $od->payment_status = "Un Paid";
                } else {
                    $od->payment_status = "Partially Paid";
                }
            }
            $arr = array(
                "status" => true,
                "data" => $orders
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "No Orders Found"
            );
        }
        echo json_encode($arr);
    }

    function get_order() {
        $user_id = $this->input->post('user_id');
        $order_id = $this->input->post('order_id');
        if (empty($order_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid Order Id Provided"
            );

            echo json_encode($arr);
            die;
        }
        $this->check_user_id($user_id);
        $order = $this->my_model->get_data_row("services_orders", array("user_id" => $user_id, "id" => $order_id));
        if ($order) {
            $order->quotation_id = "";
            if ($order->has_visit_and_quote == "Yes") {
                $check_quotation = $this->my_model->get_data_row("visit_and_quote_quotaions", array("service_order_id" => $order->id));
                if (!empty($check_quotation)) {
                    $order->quotation_id = $check_quotation->id;
                }
            }
            $order->cancellation_message = "Cancellation will be allowed untill order is Accepted.";
            $order->created_at = ( $order->created_at) ? date('d M Y, h:i A', $order->created_at) : '';
            $order->accepted_at = ( $order->accepted_at) ? date('d M Y, h:i A', $order->accepted_at) : '';
            $order->rejected_at = ( $order->rejected_at) ? date('d M Y, h:i A', $order->rejected_at) : '';
            $order->started_at = ( $order->started_at) ? date('d M Y, h:i A', $order->started_at) : '';
            $order->completed_at = ( $order->completed_at) ? date('d M Y, h:i A', $order->completed_at) : '';
            $order->cancelled_at = ( $order->cancelled_at) ? date('d M Y, h:i A', $order->cancelled_at) : '';
            $order->time_slot_date = date("d M Y", strtotime($order->time_slot_date));
            $user_address = $this->my_model->get_data_row("user_address", array("id" => $order->user_addresses_id));
            $user_address->state_name = $this->my_model->get_data_row("states", array("id" => $user_address->state))->state_name;
            $user_address->city_name = $this->my_model->get_data_row("cities", array("id" => $user_address->city))->city_name;
            $user_address->pincode_pin = $this->my_model->get_data_row("pincodes", array("id" => $user_address->pincode))->pincode;
            $order->user_address = $user_address;
            $order->category_name = $this->my_model->get_data_row("services_categories", array("id" => $order->ordered_categories))->name;
            $order->order_items = $this->my_model->get_data("service_orders_items", array("order_id" => $order->order_id));
            $this->db->select("id,name,phone,email,photo");
            $service_provider_details = $this->my_model->get_data_row('service_providers', array("id" => $order->accepted_by_service_provider));
            $service_provider_details->photo = ($service_provider_details->photo) ? base_url('uploads/service_providers/') . $service_provider_details->photo : "";
            $order->service_provider = ($service_provider_details) ? $service_provider_details : '';
            $order->order_status_text = ucwords(str_replace("_", " ", $order->order_status));
            $order->quotation_amount = "";
            $order->quotation_balance_amount = "";
            if ($order->has_visit_and_quote == "Yes") {
                $quotation = $this->my_model->get_data_row("visit_and_quote_quotaions", array("service_order_id" => $order->id));
                if (!empty($quotation)) {
                    $order->quotation_amount = $quotation->quotation_amount;
                    $order->quotation_balance_amount = $quotation->balance_amount;
                    $order->quotation_paid_amount = $quotation->quotation_amount - $quotation->balance_amount;
                }
            }
            foreach ($order->order_items as $itt) {
                $itt->has_rating = "no";
                $rating = $this->my_model->get_data_row("services_reviews", array("user_id" => $user_id, "order_id" => $order_id, "service_id" => $itt->service_id));
                $itt->rating_id = $rating->id;
                $itt->rating = $rating->rating;
                $itt->rating_comment = $rating->comment;

                if ($rating) {
                    $itt->has_rating = "yes";
                }
                $itt->service_name = $this->my_model->get_data_row("services", array("id" => $itt->service_id))->service_name;
                $itt->service_image = $this->my_model->get_data_row("services_images", array("service_id" => $itt->service_id))->image;
                $itt->service_image = ($itt->service_image) ? base_url('uploads/services/') . $itt->service_image : "";
            }
            $arr = array(
                "status" => true,
                "data" => $order
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "No Orders Found with this Order Id"
            );
        }
        echo json_encode($arr);
    }

    function check_cancellation_grace() {
        $user_id = $this->input->post('user_id');
        $order_id = $this->input->post('order_id');
        $order_data = $this->my_model->get_data_row("services_orders", array("id" => $order_id));
        $grace_period = $this->my_model->get_data_row("service_cancellation_grace_period")->duration;
        if ($order_data->created_at > strtotime("-$grace_period minutes", strtotime(date('Y-m-d h:i A')))) {
            $arr = array(
                "status" => true,
                "message" => "This is in the Grace Period"
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "This order is Past $grace_period mins, Cancellation charges will be applied."
            );
        }
        echo json_encode($arr);
        die;
    }

    function cancel_order() {
        $user_id = $this->input->post('user_id');
        $order_id = $this->input->post('order_id');
        $cancellation_reason = $this->input->post('cancellation_reason');
        if (empty($order_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid Order Id Provided"
            );

            echo json_encode($arr);
            die;
        }
        $grace_period = $this->my_model->get_data_row("service_cancellation_grace_period")->duration;
        $order_data = $this->my_model->get_data_row("services_orders", array("id" => $order_id));
        $cancelled_in_grace_period = "no";
        if ($order_data->created_at > strtotime("-$grace_period minutes", strtotime(date('Y-m-d h:i A')))) {
            $cancelled_in_grace_period = "yes";
        }
        $user_data = $this->check_user_id($user_id);
        $update = $this->my_model->update_data("services_orders", array("id" => $order_id), array("order_status" => "order_cancelled", "cancellation_reason" => $cancellation_reason, "cancelled_in_grace_period" => $cancelled_in_grace_period));
        if ($update) {
            $this->load->model('send_push_notification_model');
            $this->send_push_notification_model->send_push_notification('Order Cancellation Requested',
                    'Your Order (#' . $order_data->order_id . ') has been requested for Cancellation. Thankyou for shopping with us.',
                    array($user_data->token),
                    array(),
                    '', 'view_order', $order_id);
            $notification_data = array(
                "user_id" => $user_data->id,
                "title" => 'Order Cancellation Requested (#' . $order_data->order_id . ')',
                "message" => 'Your Order (#' . $order_data->order_id . ') has been requested for Cancellation. Thankyou for shopping with us.',
                "created_at" => time(),
                "order_id" => $order_id
            );
            $this->my_model->insert_data("user_notifications", $notification_data);
            $notification_data['order_type'] = "Service";
            $notification_data['order_id'] = $order_data->id;
            unset($notification_data['title']);
            unset($notification_data['created_at']);
            $this->my_model->insert_data("admin_notifications", $notification_data);
            $arr = array(
                "status" => true,
                "message" => "Order Cancelled Requested Succesfully"
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "Failed to Cancel Order."
            );
        }
        echo json_encode($arr);
    }

}
