<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Razorpay_create_order extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $user_id = $this->post('user_id');
        $this->check_user_id($user_id);
        $creds = $this->my_model->get_data_row("paymentgateway_and_smtp");
        $razorpay_keyid = $creds->razorpay_key;
        $razorpay_secret = $creds->razorpay_secret;
        $total_amount = $this->post('grand_total');

        $explode = explode(".", $total_amount);
        if ($explode[1] != '') {
            if (strlen($explode[1]) == 1) {
                $final = $explode[0] . "" . $explode[1] . "0";
            } else {
                $final = $explode[0] . "" . $explode[1];
            }
        } else {
            $final = $explode[0] . "00";
        }

        $data = array(
            'amount' => $final,
            'currency' => 'INR'
        );
        $payload = json_encode($data);
        $ch = curl_init('https://api.razorpay.com/v1/orders');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_USERPWD, "$razorpay_keyid:$razorpay_secret");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($payload))
        );
        $result = curl_exec($ch);
        $order_id = json_decode($result)->id;
        if ($order_id) {
            $user_data = array();
            if (!empty($user_id)) {
                $user_id = $this->input->post("user_id");
                $this->db->select("id, first_name, phone, email");
                $user_data = $this->my_model->get_data_row("users", array("id" => $user_id));
                if (!empty($user_data)) {
                    $user_data->first_name = !empty($user_data->first_name) ? $user_data->first_name : "";
                    $user_data->phone = !empty($user_data->phone) ? $user_data->phone : "";
                    $user_data->email = !empty($user_data->email) ? $user_data->email : "";
                }
            }
            $arr = array('status' => true, 'data' => array("razorpay_key" => $razorpay_keyid, "razorpay_secret" => $razorpay_secret, "order_id" => $order_id, "user_data" => $user_data));
        } else {
            $arr = array('status' => false, 'message' => "Something Went Wrong Please try Again");
        }
        echo json_encode($arr);
    }

}
