<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Referals
 *
 * @author Admin
 */
class Referrals extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $user_id = $this->input->post('user_id');
        if (empty($user_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid User Id"
            );
            echo json_encode($arr);
            die;
        }
        $this->user->check_users($user_id);
        $referral_data = $this->my_model->get_data_row("referrals");
        if ($referral_data) {
            $referral_code = $this->my_model->get_data_row("users", array("id" => $user_id))->referral_code;
            $referral_data->referral_code = $referral_code;
            $arr = array(
                "status" => true,
                "data" => $referral_data
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "No Referal Data Found"
            );
        }

        echo json_encode($arr);
    }

    function use_referral() {
        $user_id = $this->input->post('user_id');
        $referral_code = $this->input->post('referral_code');
        if (empty($user_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid User Id"
            );
            echo json_encode($arr);
            die;
        }
        if (empty($referral_code)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid Referral Code"
            );
            echo json_encode($arr);
            die;
        }
        $this->user->check_users($user_id);
        $referel_user = $this->my_model->get_data_row("users", array("referral_code" => $referral_code));
        if (empty($referel_user)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid Referral Code"
            );
            echo json_encode($arr);
            die;
        }
        $referal_user_id = $referel_user->id;
        $data = array("reffered_by" => $referal_user_id);
        $update = $this->my_model->update_data("users", array("id" => $user_id), $data);
        if ($update) {
            $arr = array(
                "status" => true,
                "message" => "Referral Code Updated Succesfully"
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "Referral Code Updating Failed"
            );
        }
        echo json_encode($arr);
    }

}
