<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Payment_methods
 *
 * @author Admin
 */
class Payment_methods extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $user_id = $this->post("user_id");
        $this->check_user_id($user_id);
        $this->db->select("id,payment_method_title");
        $has_visit_and_quote = $this->input->post("has_visit_and_quote");
        $payment_methods = $this->my_model->get_data("payment_methods_enables", null, null, null, true);
        if ($payment_methods) {
            $pos = sizeof($payment_methods) - 1;
            $percentage = $this->my_model->get_data_row("prepay_percentage");
            $payment_methods[$pos]->payment_method_title .= " - (you can pay just " . (int) $percentage->prepay_percentage . " % now and the rest later)";
            if ($has_visit_and_quote == 1) {
                unset($payment_methods[1]);
                unset($payment_methods[2]);
            }
            $arr = array(
                "status" => true,
                "data" => $payment_methods
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "No payment methods found."
            );
        }
        echo json_encode($arr);
    }

    function get_pre_pay_payable() {
        $user_id = $this->post("user_id");
        $this->check_user_id($user_id);
        $percentage = $this->my_model->get_data_row("prepay_percentage")->prepay_percentage;
//        $grand_total = $this->my_model->get_total("services_cart", "grand_total", array("user_id" => $user_id))->grand_total;
        $cart = $this->my_model->get_data("services_cart", array("user_id" => $user_id));
        $cart_totals = $this->get_cart_totals_mob($cart[0]->has_visit_and_quote, $user_id);
        $grand_total = $cart_totals->total;
        if ($grand_total > 0) {
            $amount_to_pay = ($grand_total / 100) * $percentage;
            $arr = array(
                "status" => true,
                "data" => array(
                    "amount_to_pay" => round($amount_to_pay)
                )
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "There is no items in the cart"
            );
        }
        echo json_encode($arr);
    }

    function get_pre_pay_percentage() {
        $user_id = $this->post("user_id");
        $this->check_user_id($user_id);
        $percentage = $this->my_model->get_data_row("prepay_percentage");
        if ($percentage) {
            unset($percentage->updated_at);
            $arr = array(
                "status" => true,
                "message" => "In this payment"
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "This Payment method is unavailable, please select another Payment type."
            );
        }
        echo json_encode($arr);
    }

}
