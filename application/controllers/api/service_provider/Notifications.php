<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Notifications
 *
 * @author Admin
 */
class Notifications extends MY_Controller {

    //put your code here
    function index() {
        $user_id = $this->input->post("user_id");
        $this->check_service_provider($user_id);
        $this->db->order_by("id", "desc");
        $data = $this->my_model->get_data("provider_notifications", array("provider_id" => $user_id));
        if (!empty($data)) {
            foreach ($data as $item) {
                $item->intent_type = "view_order";
                $item->order_status = $this->my_model->get_data_row("services_orders", array("id" => $item->order_id))->order_status;
                $item->created_at = date('d M Y,h:i A', $item->created_at);
            }
            $arr = array("status" => true, "data" => $data);
        } else {
            $arr = array("status" => false, "message" => "No Notifications Found");
        }
        echo json_encode($arr);
        die;
    }

    function mark_as_read() {
        $user_id = $this->input->post("user_id");
        $update = $this->my_model->update_data("provider_notifications", array("provider_id" => $user_id), array("status" => 0));
        if ($update) {
            $arr = array("status" => true, "message" => "Notifications status Updated");
        } else {
            $arr = array("status" => false, "message" => "Notifications status not updated");
        }
        echo json_encode($arr);
        die;
    }

}
