<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Addon_services extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('send_push_notification_model');
    }

    function index() {
        $service_provider_id = $this->post('user_id');
        $order_id = $this->post("order_id");
        $this->check_service_provider($service_provider_id);
        $category_id = $this->input->post("category_id");
        $this->db->select("service_id");
        $disabled_services = $this->my_model->get_data("service_providers_disabled_services", array("service_providers_id" => $service_provider_id));
        $disabled_services_string = "";
        foreach ($disabled_services as $ds) {
            $disabled_services_string .= $ds->service_id . ",";
        }
        $disabled_services_string = rtrim($disabled_services_string, ',');
        $this->db->select("id,sub_category_name,seo_url,description");
        $this->db->where("status", true);
        $sub_cats = $this->my_model->get_data("services_sub_categories", array("cat_id" => $category_id));
        $order_data = $this->my_model->get_data_row("services_orders", array("id" => $order_id));
        foreach ($sub_cats as $subs) {
            $this->db->select("id,service_name,description,duration,duration,sale_price,visiting_charges,tax,has_visit_and_quote");
            if (!empty($disabled_services_string)) {
                $this->db->where("(id NOT IN (" . $disabled_services_string . "))");
            }
            $this->db->where("status", true);
            $subs->services = $this->my_model->get_data("services", array("cat_id" => $category_id, "sub_cat_id" => $subs->id, "has_visit_and_quote" => 0));
            foreach ($subs->services as $servs) {
                $this->db->select("avg(rating) as rating");
                $rating = $this->my_model->get_data_row("services_reviews", array("service_id" => $servs->id))->rating;
                $servs->rating = ($rating) ? round($rating, 1) : 0;
                $servs->quantity = 0;
                $check_in_order = $this->my_model->get_data_row("service_orders_items", array("order_id" => $order_data->order_id, "service_id" => $servs->id, "is_addon" => 1));
//                print_r($this->db->last_query());
//                die;
                if (!empty($check_in_order) && $order_data->balance_amount > 0) {
                    $servs->quantity = $check_in_order->quantity;
                }
                $image = $this->my_model->get_data_row("services_images", array("service_id" => $servs->id));
                if (!empty($image)) {
                    $servs->image = base_url('uploads/services/') . $image->image;
                } else {
                    $servs->image = "";
                }
            }
        }
        if ($sub_cats) {
            $arr = array(
                "status" => true,
                "message" => "sub category and its services",
                "data" => $sub_cats
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "No services Found"
            );
        }
        echo json_encode($arr);
    }

    function add_update_quantity() {
        $this->db->trans_start();
        $service_provider_id = $this->post('user_id');
        $this->check_service_provider($service_provider_id);
        $service_id = $this->post('service_id');
        $order_id = $this->post('order_id');
        $quantity = $this->post("quantity");
        $service_data = $this->my_model->get_data_row("services", array("id" => $service_id));
        $order_data = $this->my_model->get_data_row("services_orders", array("order_id" => $order_id));
        $sub_total = $service_data->sale_price * $quantity;
        $grand_total = $sub_total + $service_data->visiting_charges + $service_data->tax;
        $prep_data = array(
            "order_id" => $order_id,
            "service_id" => $service_data->id,
            "category_id" => $service_data->cat_id,
            "sub_category_id" => $service_data->sub_cat_id,
            "user_id" => $order_data->user_id,
            "accepted_by_service_provider_id" => $service_provider_id,
            "price" => 0,
            "quantity" => $quantity,
            "unit_price" => $service_data->sale_price,
            "visiting_charges" => $service_data->visiting_charges,
            "has_visit_and_quote" => 0,
            "sub_total" => $sub_total,
            "tax" => $service_data->tax,
            "grand_total" => $grand_total,
            "order_status" => "order_placed",
            "is_addon" => 1
        );
        $check = $this->my_model->get_data_row("service_orders_items", array("order_id" => $order_id, "service_id" => $service_id, "is_addon" => 1, "is_paid" => 0));
        $user_data = $this->my_model->get_data_row("users", array("id" => $order_data->user_id));
        if ($quantity == 0) {
            $delete = $this->my_model->delete_data("service_orders_items", array("order_id" => $order_id, "service_id" => $service_id, "is_addon" => 1, "is_paid" => 0));
            if ($delete) {
                $this->send_push_notification_model->send_push_notification('Order Updated',
                        'Your Order (#' . $order_id . ') addon item has been deleted.',
                        array($user_data->token),
                        array(),
                        '', 'view_order', $order_data->id);
                $notification_data = array(
                    "user_id" => $user_data->id,
                    "title" => 'Order (#' . $order_data->order_id . ') Updated',
                    "message" => 'Your Order (#' . $order_data->order_id . ') addon item has been deleted.',
                    "created_at" => time(),
                    "order_id" => $order_data->id
                );
                $this->my_model->insert_data("user_notifications", $notification_data);
                $arr = array(
                    "status" => true,
                    "message" => "Item removed from the Order"
                );
            } else {
                $arr = array(
                    "status" => false,
                    "message" => "Unable to Remove Item from the Order"
                );
            }
        } else if (!empty($check)) {
            $update = $this->my_model->update_data("service_orders_items", array("order_id" => $order_id, "service_id" => $service_id, "is_addon" => 1, "is_paid" => 0), $prep_data);
            if ($update) {
                $this->send_push_notification_model->send_push_notification('Order Updated',
                        'Your Order (#' . $order_id . ') addon item has been Updated.',
                        array($user_data->token),
                        array(),
                        '', 'view_order', $order_data->id);
                $notification_data = array(
                    "user_id" => $user_data->id,
                    "title" => 'Order (#' . $order_data->order_id . ') Updated',
                    "message" => 'Your Order (#' . $order_data->order_id . ') addon item has been Updated.',
                    "created_at" => time(),
                    "order_id" => $order_data->id
                );
                $this->my_model->insert_data("user_notifications", $notification_data);
                $arr = array(
                    "status" => true,
                    "message" => "Item updated in the Order Successfully"
                );
            } else {
                $arr = array(
                    "status" => false,
                    "message" => "Unable to Update Item in the Order"
                );
            }
        } else {
            $ins = $this->my_model->insert_data("service_orders_items", $prep_data);
            if ($ins) {
                $this->send_push_notification_model->send_push_notification('Order Updated',
                        'Your Order (#' . $order_id . ') addon item has been added.',
                        array($user_data->token),
                        array(),
                        '', 'view_order', $order_data->id);
                $notification_data = array(
                    "user_id" => $user_data->id,
                    "title" => 'Order (#' . $order_data->order_id . ') Updated',
                    "message" => 'Your Order (#' . $order_data->order_id . ') addon item has been added.',
                    "created_at" => time(),
                    "order_id" => $order_data->id
                );
                $this->my_model->insert_data("user_notifications", $notification_data);
                $arr = array(
                    "status" => true,
                    "message" => "Item Added to the Order Successfully"
                );
            } else {
                $arr = array(
                    "status" => false,
                    "message" => "Unable to Add Item to the Order"
                );
            }
        }
        $this->update_order_total_price($order_id);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $arr = array(
                "status" => false,
                "message" => "Something went wrong please try again"
            );
            echo json_encode($arr);
        } else {
            $this->db->trans_commit();
            echo json_encode($arr);
        }
        die;
    }

    function update_order_total_price($order_id) {
        $order_data = $this->my_model->get_data_row("services_orders", array("order_id" => $order_id));
        $this->db->select("sum(sub_total) as sub_total, sum(tax) as tax, sum(visiting_charges) as visiting_charges, sum(grand_total) as grand_total, count(id) as no_of_services");
        $order_items_totals = $this->my_model->get_data_row("service_orders_items", array("order_id" => $order_id));
        $balance_amount = ($order_items_totals->grand_total - $order_data->coupon_discount - $order_data->membership_discount - $order_data->bussiness_discount) - $order_data->amount_paid;
        $up_data = array(
            "sub_total" => $order_items_totals->sub_total,
            "tax" => $order_items_totals->tax,
            "visiting_charges" => $order_items_totals->visiting_charges,
            "grand_total" => $order_items_totals->grand_total,
            "balance_amount" => $balance_amount,
            "no_of_services" => $order_items_totals->no_of_services
        );
        $this->my_model->update_data("services_orders", array("order_id" => $order_id), $up_data);
    }

}
