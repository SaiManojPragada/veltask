<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Visit_and_quote
 *
 * @author vincent
 */
class Visit_and_quote extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $service_provider_id = $this->post('user_id');
        $this->check_service_provider($service_provider_id);
        $quotations = $this->my_model->get_data("visit_and_quote_quotaions", array("service_provider_id" => $service_provider_id));
        if (!empty($quotations)) {
            foreach ($quotations as $quote) {
                $quote->order_details = $this->my_model->get_data_row("services_orders", array("id" => $quote->service_order_id));
            }
            $arr = array(
                "status" => true,
                "data" => $quotations
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "No Quotations Found."
            );
        }

        echo json_encode($arr);
        die;
    }

    function get_quotation() {
        $service_provider_id = $this->post('user_id');
        $order_id = $this->post("order_id");
        $this->check_service_provider($service_provider_id);
        $quotation = $this->my_model->get_data_row("visit_and_quote_quotaions", array("service_provider_id" => $service_provider_id, "service_order_id" => $order_id));
        if (!empty($quotation)) {
            $arr = array(
                "status" => true,
                "data" => $quotation
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "No Quotations Found."
            );
        }

        echo json_encode($arr);
        die;
    }

    function get_milestones() {
        $service_provider_id = $this->post('user_id');
        $this->check_service_provider($service_provider_id);
        $quotation_id = $this->post('quotation_id');
        $quotation_data = $this->my_model->get_data_row("visit_and_quote_quotaions", array("id" => $quotation_id));
        if (!empty($quotation_data)) {
            $quotation_amount = $quotation_data->quotation_amount;
            $no_of_milestones = $quotation_data->no_of_milestones;
            $milestones = $this->my_model->get_data('quotation_milestones', array("quotation_id" => $quotation_id));
//            $milestone_share = $quotation_amount / $no_of_milestones;
//            foreach ($milestones as $mm) {
//                $mm->amount = ($mm->amount > 0) ? round($mm->amount) : $milestone_share;
//                $mm->description = empty($mm->description) ? "" : $mm->description;
//            }
            $arr = array(
                "status" => true,
                "data" => $milestones
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "No Quotation found."
            );
        }
        echo json_encode($arr);
        die;
    }

    function add_milestone() {
        $service_provider_id = $this->post('user_id');
        $this->check_service_provider($service_provider_id);
        $quotation_id = $this->post('quotation_id');
        $quotation_data = $this->my_model->get_data_row("visit_and_quote_quotaions", array("id" => $quotation_id));
        $existing_milestones = $quotation_data->no_of_milestones;
        if ($existing_milestones > 4) {
            $arr = array(
                "status" => false,
                "message" => "Only Maximum of $existing_milestones milestones are Allowed."
            );
            echo json_encode($arr);
            die;
        }
        $up_mil = array(
            "no_of_milestones" => $existing_milestones + 1
        );
        $update = $this->my_model->update_data("visit_and_quote_quotaions", array("id" => $quotation_id),
                $up_mil);
        if ($update) {
            $milestone = array(
                "quotation_id" => $quotation_id,
                "amount" => 0,
                "duration_type" => "",
                "duration" => "",
                "date" => "",
                "created_at" => time(),
                "updated_at" => time(),
                "status" => "unpaid"
            );
            $this->my_model->insert_data("quotation_milestones", $milestone);
            $arr = array(
                "status" => true,
                "message" => "New Milestone for the Quotation has been added."
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "Unable to add Milestone for the Quotation."
            );
        }
        echo json_encode($arr);
        die;
    }

    function remove_milestone() {
        $service_provider_id = $this->post('user_id');
        $quotation_id = $this->post('quotation_id');
        $this->check_service_provider($service_provider_id);
        $quotation_data = $this->my_model->get_data_row("visit_and_quote_quotaions", array("id" => $quotation_id));
        $existing_milestones = $quotation_data->no_of_milestones;
        $up_mil = array(
            "no_of_milestones" => ($existing_milestones < 2) ? 1 : $existing_milestones - 1
        );
        $update = $this->my_model->update_data("visit_and_quote_quotaions", array("id" => $quotation_id),
                $up_mil);
        if ($update) {
            if ($existing_milestones > 1) {
                $this->db->order_by("id", "desc");
                $this->db->limit(1);
                $this->my_model->delete_data("quotation_milestones", array("quotation_id" => $quotation_id));
            }
            $arr = array(
                "status" => true,
                "message" => "The Milestone for the Quotation has been Removed."
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "Unable to Remove Milestone for the Quotation."
            );
        }
        echo json_encode($arr);
        die;
    }

    function update_milestone_prices() {
        $service_provider_id = $this->post('user_id');
        $service_provider_data = $this->check_service_provider($service_provider_id);
        $milestone_json = json_decode($this->post('milestone_json'));
//        print_r($milestone_json);
//        die;
        $total_amount = 0;
        $quotation_id = 0;
        foreach ($milestone_json as $item) {
            $total_amount += $item->amount;
            $where = array("id" => $item->quotation_id);
            $quotation_id = $item->quotation_id;
            $item->updated_at = time();
        }
        $update = $this->my_model->update_multi_data("quotation_milestones", $milestone_json, 'id');
        if ($update) {
            $this->db->where("status", "paid");
            $paid_milestones_amount = $this->my_model->get_total("quotation_milestones", "amount", array("quotation_id" => $quotation_id));
            $balance_amount = $total_amount - (($paid_milestones_amount) < 2 ? 0 : $paid_milestones_amount);
            $this->my_model->update_data("visit_and_quote_quotaions", $where, array("quotation_amount" => $total_amount, "balance_amount" => $balance_amount));
            $service_order = $this->my_model->get_data_row("visit_and_quote_quotaions", $where);
            $service_order_id = $service_order->service_order_id;
            $user_id = $service_order->user_id;
            $order_data = $this->my_model->get_data_row('services_orders', array('id' => $service_order_id));
            $user_data = $this->my_model->get_data_row('users', array('id' => $user_id));
            $this->send_push_notification_model->send_push_notification('Order Quotation', 'Your Order (' . $order_data->order_id . ') has recieved quotation of Rs.' . $quotation_amount . '.', array($user_data->token), array(), '');

            $arr = array(
                "status" => true,
                "message" => "Milestones Updated."
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "Unable to Update Milestone for the Quotation."
            );
        }
        echo json_encode($arr);
        die;
    }

    function submit_quotation() {
        $service_provider_id = $this->post('user_id');
        $service_order_id = $this->post('service_order_id');

        $check_for_duplicate = $this->my_model->get_data_row("visit_and_quote_quotaions", array(
            "service_order_id" => $service_order_id, "service_provider_id" => $service_provider_id));
        if (!empty($check_for_duplicate)) {
            $arr = array(
                "status" => false,
                "message" => "You've already submitted the Quotation amount for this order.",
                "data" => array(
                    "quotation_id" => $check_for_duplicate->id
                )
            );
            echo json_encode($arr);
            die;
        }

        $service_provider_data = $this->check_service_provider($service_provider_id);

        $quotation_id = $this->generate_random_key('visit_and_quote_quotaions', 'quotation_id', 'VTQT');
        $order_data = $this->my_model->get_data_row('services_orders');
        $user_id = $order_data->user_id;
        $service_provider_id = $service_provider_data->id;
        $quotation_amount = $this->post('quotation_amount');
        $no_of_milestones = ($this->post('no_of_milestones') < 1) ? 1 : $this->post('no_of_milestones');
        $created_at = time();
        $updated_at = time();
        if ($quotation_amount < 1) {
            $arr = array(
                "status" => false,
                "message" => "Quotation Amount Cannot be Zero"
            );
            echo json_encode($arr);
            die;
        }

        $prep_data = array(
            "quotation_id" => $quotation_id,
            "service_order_id" => $service_order_id,
            "user_id" => $user_id,
            "service_provider_id" => $service_provider_id,
            "quotation_amount" => $quotation_amount,
            "balance_amount" => $quotation_amount,
            "no_of_milestones_paid" => 0,
            "created_at" => $created_at,
            "updated_at" => $updated_at,
            "no_of_milestones" => $no_of_milestones
        );
        $insert = $this->my_model->insert_data('visit_and_quote_quotaions', $prep_data);
        if ($insert) {
            $last_insert_id = $this->db->insert_id();
            #create_milestones
            for ($i = 0; $i < $no_of_milestones; $i++) {
                $milestone = array(
                    "quotation_id" => $last_insert_id,
                    "amount" => 0,
                    "duration_type" => "",
                    "duration" => "",
                    "date" => "",
                    "created_at" => time(),
                    "updated_at" => time(),
                    "status" => "unpaid"
                );
                $this->my_model->insert_data("quotation_milestones", $milestone);
            }
            $order_data = $this->my_model->get_data_row('services_orders', array('id' => $service_order_id));
            $user_data = $this->my_model->get_data_row('users', array('id' => $user_id));
            $this->send_push_notification_model->send_push_notification('Order Quotation', 'Your Order (' . $order_data->order_id . ') has recieved quotation of Rs.' . $quotation_amount . '.', array($user_data->token), array(), '');
            $arr = array(
                "status" => true,
                "message" => "Quotation has been sent to the User",
                "data" => array(
                    "quotation_id" => $last_insert_id
                )
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "Cannot send the Quotation to the user, Please Try Again."
            );
        }
        echo json_encode($arr);
    }

    function showcompleteButton() {
        $user_id = $this->post('user_id');
        $service_order_id = $this->post('service_order_id');
        $qry = $this->db->query("SELECT visit_and_quote_quotaions.id FROM services_orders INNER JOIN visit_and_quote_quotaions ON services_orders.id=visit_and_quote_quotaions.service_order_id WHERE services_orders.id='" . $service_order_id . "'");
        $value = $qry->row();
        $quotation_qry = $this->db->query("SELECT * FROM quotation_milestones WHERE quotation_id='" . $value->id . "'");
        $quotation_result = $quotation_qry->result();
        foreach ($quotation_result as $q_value) {
            if ($q_value->status == 'unpaid') {
                $paystatus = 'false';
            }
        }

        if ($paystatus == 'false') {
            $arr = array(
                "status" => false,
                "message" => "Payment pending"
            );
            echo json_encode($arr);
        }
        if ($paystatus != 'false') {
            $arr = array(
                "status" => true,
                "message" => "you can show the payment button"
            );
            echo json_encode($arr);
        }
    }

}
