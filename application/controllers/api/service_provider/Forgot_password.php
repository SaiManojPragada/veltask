<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Forgot_password
 *
 * @author Admin
 */
class Forgot_password extends MY_Controller {

    //put your code here
    public $data;

    function __construct() {
        parent::__construct();
    }

    function index() {
        $email = $this->input->post("email");
        $check = $this->my_model->get_data_row("service_providers", array("email" => $email));
        if (!empty($check)) {
            //sends email regarding the password change otp.
//            $otp = rand(0000,9999);
            $otp = 1234;
            $update = $this->my_model->update_data("service_providers", array("id" => $check->id), array("otp" => $otp, "forgot_password_token" => generateRandomString(150)));
            if ($update) {
                $arr = array(
                    "status" => true,
                    "message" => "Otp Sent.",
                    "data" => array(
                        "user_id" => $check->id
                    )
                );
            } else {
                $arr = array(
                    "status" => false,
                    "message" => "Unable to proccess your request, try again."
                );
            }
        } else {
            $arr = array(
                "status" => false,
                "message" => "No Accounts found related to this email."
            );
        }
        echo json_encode($arr);
    }

    function resend_otp(){
        $user_id = $this->input->post("user_id");
        $this->check_service_provider($user_id);
        //sends email regarding the password change otp.
           // $otp = rand(0000,9999);
            $otp = 1234;
            $update = $this->my_model->update_data("service_providers", array("id" => $user_id), array("otp" => $otp));
            if($update){
                $arr = array(
                    "status" => true,
                    "message" => "Otp Sent to Email.",
                    "data" => array(
                        "user_id" => $user_id
                    )
                );
            }else {
                $arr = array(
                    "status" => false,
                    "message" => "Unable to proccess your request, try again."
                );
            }
            echo json_encode($arr);
    }

    function verify_otp() {
        $user_id = $this->input->post("user_id");
        $this->check_service_provider($user_id);
        $otp = $this->input->post("otp");
        $check = $this->my_model->get_data_row("service_providers", array("id" => $user_id, "otp" => $otp));
        if (!empty($check)) {
            $arr = array(
                "status" => true,
                "message" => "OTP Verified.",
                "data" => array(
                    "user_id" => $check->id,
                    "forgot_password_token" => $check->forgot_password_token
                )
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "Invalid OTP."
            );
        }
        echo json_encode($arr);
    }

    function change_password() {
        $user_id = $this->input->post("user_id");
        $this->check_service_provider($user_id);
        $forgot_password_token = $this->input->post("forgot_password_token");
        $new_password = $this->input->post("new_password");
        $confirm_password = $this->input->post("confirm_password");
        $check = $this->my_model->get_data_row("service_providers", array("id" => $user_id, "forgot_password_token" => $forgot_password_token));
        if (empty($check)) {
            $arr = array(
                "status" => false,
                "message" => "You are not Authorised to perform this action."
            );
            echo json_encode($arr);
            die;
        }
        if ($new_password == $confirm_password) {
            $update = $this->my_model->update_data("service_providers", array("id" => $user_id), array("password" => md5($new_password), "otp" => null, "forgot_password_token" => null));
            if ($update) {
                $arr = array(
                    "status" => true,
                    "message" => "Password Updated"
                );
            } else {
                $arr = array(
                    "status" => false,
                    "message" => "Unable to update Password"
                );
            }
        } else {
            $arr = array(
                "status" => false,
                "message" => "Passwords does not Match."
            );
        }
        echo json_encode($arr);
    }

}
