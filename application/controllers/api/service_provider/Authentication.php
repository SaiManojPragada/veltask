<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Authentication
 *
 * @author Admin
 */
class Authentication extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function login() {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $access_token = $this->input->post('access_token');
        if (empty($email)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid Email Id"
            );
            echo json_encode($arr);
            die;
        }
        if (empty($password)) {
            $arr = array(
                "status" => false,
                "message" => "Password Cannot be Empty"
            );
            echo json_encode($arr);
            die;
        }
        $check = array(
            "email" => $email
        );
        $check_user = $this->my_model->get_data_row("service_providers", $check);
        if ($check_user) {
            if ($check_user->password === md5($password)) {
                if ($check_user->status) {
                    unset($check_user->password);
                    if (!empty($access_token)) {
                        $this->my_model->update_data("service_providers", array("id" => $check_user->id), array("access_token" => $access_token));
                    }
                    $arr = array(
                        "status" => true,
                        "message" => "Service Provider Login Successful",
                        "data" => $check_user
                    );
                } else {
                    $arr = array(
                        "status" => false,
                        "message" => "Your Account has been Deactivated. Please Contact Admin"
                    );
                }
            } else {
                $arr = array(
                    "status" => false,
                    "message" => "Incorrect Password"
                );
            }
        } else {
            $arr = array(
                "status" => false,
                "message" => "User Does not Exists"
            );
        }
        echo json_encode($arr);
    }

    function check_user() {
        $user_id = $this->input->post('user_id');
        if (empty($user_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid User Id"
            );
            echo json_encode($arr);
            die;
        }
        if ($this->check_service_provider($user_id)) {
            $resp = array("status" => true, "message" => "User Exists");
            echo json_encode($resp);
            die;
        }
    }

}
