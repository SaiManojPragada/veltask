<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Payouts extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
    }

    function index() {
        $user_id = $this->input->post("user_id");
        $this->check_service_provider($user_id);
        $wallet = $this->my_model->get_data_row("service_providers_payments", array("provider_id" => $user_id));
        $total_earnings = $wallet->total_amount;
        $total_withdrawls = $wallet->total_requested_amount;
        $this->db->select("SUM(requested_amount) as total_onhold_amount");
        $onhold_amount = $this->my_model->get_data_row("service_provider_withdraw_request", array("provider_id" => $user_id, "status" => 0))->total_onhold_amount;
        $current_balance = ($total_earnings - $total_withdrawls) - $onhold_amount;
        $data = array(
            "status" => true,
            "data" => array(
                "total_earnings" => round((float) $total_earnings),
                "total_withdrawls" => round((float) $total_withdrawls),
                "balance_amount" => round($current_balance, 2),
                "onhold_amount" => round((float) $onhold_amount)
            )
        );
        echo json_encode($data);
    }

    function request_withdrawl() {
        $user_id = $this->input->post("user_id");
        $this->check_service_provider($user_id);
        $requested_amount = $this->input->post("requested_amount");
        $description = $this->input->post("description");
        $wallet = $this->my_model->get_data_row("service_providers_payments", array("provider_id" => $user_id));
        $total_earnings = $wallet->total_amount;
        $total_withdrawls = $wallet->total_requested_amount;
        $this->db->select("SUM(requested_amount) as total_onhold_amount");
        $onhold_amount = $this->my_model->get_data_row("service_provider_withdraw_request", array("provider_id" => $user_id, "status" => 0))->total_onhold_amount;
        $current_balance = ($total_earnings - $total_withdrawls) - $onhold_amount;
        if ($requested_amount > $current_balance) {
            $arr = array(
                "status" => false,
                "message" => "Insufficient Balance"
            );
            echo json_encode($arr);
            die;
        }
        $inp_data = array(
            "provider_id" => $user_id,
            "requested_amount" => $requested_amount,
            "description" => $description,
            "created_at" => time(),
            "status" => 0
        );
        $insert = $this->my_model->insert_data("service_provider_withdraw_request", $inp_data);
        if ($insert) {
            $arr = array(
                "status" => true,
                "message" => "Withdraw request succesfully sent"
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "Unable to send Withdraw request"
            );
        }
        echo json_encode($arr);
    }

    function get_withdrawls() {
        $user_id = $this->input->post("user_id");
        $this->check_service_provider($user_id);
        $type = $this->input->post("type");
        $transactions = $this->my_model->get_data("service_provider_withdraw_request", array("provider_id" => $user_id, "status" => $type), "id", "desc");
        foreach ($transactions as $item) {
            $item->admin_description = (!empty($item->admin_description)) ? $item->admin_description : "";
            $item->mode_of_payment = (!empty($item->mode_of_payment)) ? $item->mode_of_payment : "";
            $item->sender_name = (!empty($item->sender_name)) ? $item->sender_name : "";
            $item->reciever_name = (!empty($item->reciever_name)) ? $item->reciever_name : "";
            $item->transaction_id = (!empty($item->transaction_id)) ? $item->transaction_id : "";
            $item->transaction_image = (!empty($item->transaction_image)) ? $item->transaction_image : "";
            $item->created_at = (!empty($item->created_at)) ? date('d M Y, h:i A', $item->created_at) : "";
            $item->updated_at = (!empty($item->updated_at)) ? date('d M Y, h:i A', $item->updated_at) : "";
            $item->status_text = (!$item->status) ? "Pending" : "Settled";
        }
        if (!empty($transactions)) {
            $arr = array(
                "status" => true,
                "data" => $transactions
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "No Withdraw Requests Found"
            );
        }
        echo json_encode($arr);
    }

}
