<?php

class Services_switch extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function categories() {
        $user_id = $this->input->post('user_id');
        $category_id = $this->input->post('category_id');
        if (empty($user_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid User Id"
            );
            echo json_encode($arr);
            die;
        }
        if (empty($category_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid Category Id"
            );
            echo json_encode($arr);
            die;
        }
        $user_data = $this->check_service_provider($user_id);
        $check = $this->my_model->get_data_row("service_providers_disabled_categories", array("service_providers_id" => $user_id, "cat_id" => $category_id));
        if (!empty($check)) {
            $delete = $this->my_model->delete_data("service_providers_disabled_categories", array("service_providers_id" => $user_id, "cat_id" => $category_id));
            if ($delete) {
                $arr = array(
                    "status" => true,
                    "message" => "Service Category Turned On"
                );
            } else {
                $arr = array(
                    "status" => false,
                    "message" => "Unable to Turn On Service Category"
                );
            }
        } else {
            $insert = $this->my_model->insert_data("service_providers_disabled_categories", array("service_providers_id" => $user_id, "cat_id" => $category_id, "disabled_at" => time()));
            if ($insert) {
                $arr = array(
                    "status" => true,
                    "message" => "Service Category Turned Off"
                );
            } else {
                $arr = array(
                    "status" => false,
                    "message" => "Unable to Turn Off Service Category"
                );
            }
        }
        echo json_encode($arr);
    }

    function services() {
        $user_id = $this->input->post('user_id');
        $category_id = $this->input->post('category_id');
        $sub_category_id = $this->input->post('sub_category_id');
        $service_id = $this->input->post('service_id');
        if (empty($user_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid User Id"
            );
            echo json_encode($arr);
            die;
        }
        if (empty($category_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid Category Id"
            );
            echo json_encode($arr);
            die;
        }
        if (empty($sub_category_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid Sub Category Id"
            );
            echo json_encode($arr);
            die;
        }
        if (empty($service_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid Service Id"
            );
            echo json_encode($arr);
            die;
        }
        $user_data = $this->check_service_provider($user_id);
        $data = array(
            "service_providers_id" => $user_id,
            "cat_id" => $category_id,
            "sub_cat_id" => $sub_category_id,
            "service_id" => $service_id
        );
        $check = $this->my_model->get_data_row("service_providers_disabled_services", $data);
        if (!empty($check)) {
            $delete = $this->my_model->delete_data("service_providers_disabled_services", $data);
            if ($delete) {
                $arr = array(
                    "status" => true,
                    "message" => "Service Turned On"
                );
            } else {
                $arr = array(
                    "status" => false,
                    "message" => "Unable to Turn On Service"
                );
            }
        } else {
            $data['updated_at'] = time();
            $insert = $this->my_model->insert_data("service_providers_disabled_services", $data);
            if ($insert) {
                $arr = array(
                    "status" => true,
                    "message" => "Service Turned Off"
                );
            } else {
                $arr = array(
                    "status" => false,
                    "message" => "Unable to Turn Off Service"
                );
            }
        }
        echo json_encode($arr);
    }

}
