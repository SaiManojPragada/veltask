<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Homepage
 *
 * @author Admin
 */
class Services extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('vendor_services');
    }

    //curren_jobs, accepted_jobs, completed_jobs counts

    function index() {
        $type = $this->input->post('type');             //ongoing, accepted, completed
        $user_id = $this->input->post('user_id');
        if (empty($user_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid User Id"
            );
            echo json_encode($arr);
            die;
        }
        if (empty($type)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid Type"
            );
            echo json_encode($arr);
            die;
        }
        $user_data = $this->check_service_provider($user_id);
        //$franchise_pincodes = explode(',', $this->my_model->get_data_row("franchises", array("id" => $user_data->franchise_id))->pincode_ids);


        $c_id = rtrim($user_data->categories_ids, ',');

        $data = $this->vendor_services->get_services($type, $user_id, $c_id);
        $result = array();
        //print_r($data); die;
        $order_selected_id = array();
        foreach ($data as $d) {
            $d->payment_type = $this->my_model->get_data_row("payment_methods_enables", array("id" => $d->payment_type))->payment_method_title;
            $d->created_at = date('d M Y, h:i A', $d->created_at);
            $d->accepted_at = date('d M Y, h:i A', $d->accepted_at);
            $d->rejected_at = date('d M Y, h:i A', $d->rejected_at);
            $d->started_at = date('d M Y, h:i A', $d->started_at);
            $d->completed_at = date('d M Y, h:i A', $d->completed_at);
            $d->cancelled_at = date('d M Y, h:i A', $d->cancelled_at);
            $d->refunded_at = date('d M Y, h:i A', $d->refunded_at);
            $order_user_data = $this->my_model->get_data_row("users", array("id" => $d->user_id));
            $d->user_name = $order_user_data->first_name;
            $user_pincode = $this->my_model->get_data_row("user_address", array("id" => $d->user_addresses_id))->pincode;

            if ($type == "order_placed") {
                $francise_qry = $this->db->query("select * from service_providers where franchise_id='" . $user_data->franchise_id . "' and id = '$user_id'");
                $francise_result = $francise_qry->result();
                foreach ($francise_result as $frncise_value) {
                    $check_for_admin_comission = $this->my_model->get_data_row("provider_comissions",
                            array("providers_id" => $frncise_value->id, "cat_id" => $d->ordered_categories, "status" => 1));
                    if (!empty($check_for_admin_comission)) {

                        if ($frncise_value->sp_pincodes != '') {
                            $franchise_pincodes = explode(",", $frncise_value->sp_pincodes);

                            if (in_array($user_pincode, $franchise_pincodes) && !in_array($d->id, $order_selected_id)) {
                                array_push($order_selected_id, $d->id);
                                array_push($result, $d);
                            }
                        }
                    }
                }
            } else {
                $result = $data;
            }
        }
        if (!empty($result)) {
            $arr = array(
                "status" => true,
                "data" => $result
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "No Orders Found"
            );
        }

//        if ($data) {
//            foreach ($data as $item) {
//                $item->category_name = $this->my_model->get_data_row('services_categories', array('id' => $item->ordered_categories))->name;
//                $item->ordered_on = date('d M Y, h:i A');
//            }
//            $arr = array(
//                "status" => true,
//                "data" => $data
//            );
//        } else {
//            $arr = array(
//                "status" => false,
//                "message" => "No Orders Found"
//            );
//        }
        echo json_encode($arr);
        die;
    }

    function homepage() {
        $user_id = $this->input->post('user_id');
        if (empty($user_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid User Id"
            );
            echo json_encode($arr);
            die;
        }
        $user_data = $this->check_service_provider($user_id);

        $categories_ids = rtrim($user_data->categories_ids, ',');

        $this->db->order_by('view_order', 'asc');
        $features = $this->my_model->get_data("service_provider_features");
        if ($features) {
            foreach ($features as $type) {
                $type->image = base_url('uploads/service_provider_features/') . $type->image;
                unset($type->view_order);
                unset($type->created_at);
                unset($type->updated_at);
                unset($type->status);
                if ($type->type_enum == 'order_placed') {
                    $result = array();
                    $order_selected_id = array();
                    $c_id = rtrim($user_data->categories_ids, ',');

                    $data = $this->vendor_services->get_services($type->type_enum, $user_id, $c_id);
                    // print_r($data); die;
                    foreach ($data as $d) {
                        $d->created_at = date('d M Y, h:i A', $d->created_at);
                        $d->accepted_at = date('d M Y, h:i A', $d->accepted_at);
                        $d->rejected_at = date('d M Y, h:i A', $d->rejected_at);
                        $d->started_at = date('d M Y, h:i A', $d->started_at);
                        $d->completed_at = date('d M Y, h:i A', $d->completed_at);
                        $d->cancelled_at = date('d M Y, h:i A', $d->cancelled_at);
                        $d->refunded_at = date('d M Y, h:i A', $d->refunded_at);
                        $order_user_data = $this->my_model->get_data_row("users", array("id" => $d->user_id));
                        $d->user_name = $order_user_data->first_name;
                        $user_pincode = $this->my_model->get_data_row("user_address", array("id" => $d->user_addresses_id))->pincode;

                        $francise_qry = $this->db->query("select * from service_providers where franchise_id='" . $user_data->franchise_id . "' and id = '$user_id'");
                        $francise_result = $francise_qry->result();
                        foreach ($francise_result as $frncise_value) {
                            $check_for_admin_comission = $this->my_model->get_data_row("provider_comissions",
                                    array("providers_id" => $frncise_value->id, "cat_id" => $d->ordered_categories, "status" => 1));
                            if (!empty($check_for_admin_comission)) {

                                if ($frncise_value->sp_pincodes != '') {
                                    $franchise_pincodes = explode(",", $frncise_value->sp_pincodes);

                                    if (in_array($user_pincode, $franchise_pincodes) && !in_array($d->id, $order_selected_id)) {
                                        array_push($order_selected_id, $d->id);
                                        array_push($result, $d);
                                    }
                                }
                            }
                        }
                    }
                    $type->count = ($result) ? (string) sizeof($result) : "0";
                } else if (empty($type->type_enum)) {
                    $type->count = "";
                } else if ($type->type_enum == 'order_accepted') {
                    /* $this->db->where("order_status = 'order_accepted' OR order_status = 'order_started' and accepted_by_service_provider='".$user_id."'");
                      echo $type->count = (string) sizeof($this->my_model->get_data("services_orders")); */

                    $accept_qry = $this->db->query("select * from services_orders where (order_status = 'order_accepted' OR order_status = 'order_started' ) AND accepted_by_service_provider='" . $user_id . "'");
                    $type->count = $accept_qry->num_rows();
                } else {
                    $type->count = (string) sizeof($this->my_model->get_data("services_orders", array("accepted_by_service_provider" => $user_id, 'order_status' => $type->type_enum)));
                }
            }
            $arr = array(
                "status" => true,
                "data" => $features
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "No data Found"
            );
        }

        echo json_encode($arr);
        die;
    }

    function start_service() {
        $user_id = $this->input->post('user_id');
        $order_id = $this->input->post('order_id');
        if (empty($user_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid User Id"
            );
            echo json_encode($arr);
            die;
        }

        if (empty($user_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid Order Id"
            );
            echo json_encode($arr);
            die;
        }
        $service_provider_data = $this->check_service_provider($user_id);
        $where = array("id" => $order_id, 'order_status' => 'order_started');
        $check = $this->my_model->get_data_row('services_orders', $where);
        if (!empty($check)) {
            $arr = array(
                "status" => false,
                "message" => "This Service has been Already Started"
            );
            echo json_encode($arr);
            die;
        }
        $where = array("id" => $order_id);
        $check = $this->my_model->get_data_row('services_orders', $where);
        $today = date('Y-m-d');
        if ($check->time_slot_date != $today) {
            $arr = array(
                "status" => false,
                "message" => "This Service cannot be Started, This is Scheduled on " . date('d-m-Y', strtotime($check->time_slot_date)) . "."
            );
            echo json_encode($arr);
            die;
        }
        $update = $this->my_model->update_data("services_orders", array("id" => $order_id), array("order_status" => 'order_started', 'started_at' => time()));
        if ($update) {
            $order_data = $this->my_model->get_data_row("services_orders", array("id" => $order_id));
            $user_id = $order_data->user_id;
            $user_data = $this->my_model->get_data_row("users", array("id" => $user_id));
            $this->send_push_notification_model->send_push_notification('Order Started', 'Your Order #' . $order_data->order_id . ' has been Started by ' . $service_provider_data->name . '.', array($user_data->token), array(), "", 'view_order', $order_id);
            $notification_data = array(
                "user_id" => $user_data->id,
                "title" => 'Order (#' . $order_data->order_id . ') Started',
                "message" => 'Your Order (#' . $order_data->order_id . ') has been started',
                "created_at" => time(),
                "order_id" => $order_data->id
            );
            $notification_data['order_type'] = "Service";
            $notification_data['order_id'] = $order_data->id;
            unset($notification_data['title']);
            unset($notification_data['created_at']);
            $this->my_model->insert_data("admin_notifications", $notification_data);
            $this->my_model->insert_data("user_notifications", $notification_data);
            $arr = array(
                "status" => true,
                "message" => "Order Started"
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "Unable to Start Order"
            );
        }
        echo json_encode($arr);
        die;
    }

    function accept_order() {
        $user_id = $this->input->post('user_id');
        $order_id = $this->input->post('order_id');

        if (empty($user_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid User Id"
            );
            echo json_encode($arr);
            die;
        }

        if (empty($user_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid Order Id"
            );
            echo json_encode($arr);
            die;
        }
        $service_provider_data = $this->check_service_provider($user_id);
        $accepted_franchise_id = $service_provider_data->franchise_id;

        $where = array("id" => $order_id, 'order_status' => 'order_accepted');
        $check = $this->my_model->get_data_row('services_orders', $where);
        if (!empty($check)) {
            $arr = array(
                "status" => false,
                "message" => "This Order has been Already Accepted"
            );
            echo json_encode($arr);
            die;
        }
        $update = $this->my_model->update_data("services_orders", array("id" => $order_id), array("order_status" => 'order_accepted', 'accepted_by_service_provider' => $user_id, 'accepted_franchise_id' => $accepted_franchise_id, 'accepted_at' => time()));
        if ($update) {
            $order_data = $this->my_model->get_data_row("services_orders", array("id" => $order_id));
            $user_id = $order_data->user_id;
            $user_data = $this->my_model->get_data_row("users", array("id" => $user_id));
            $this->send_push_notification_model->send_push_notification('Order Accepted', 'Your Order #' . $order_data->order_id . ' has been Accepted by ' . $service_provider_data->name . '.', array($user_data->token), array(), "", 'view_order', $order_id);
            $notification_data = array(
                "user_id" => $user_data->id,
                "title" => 'Order Accepted (#' . $order_data->order_id . ')',
                "message" => 'Your Order #' . $order_data->order_id . ' has been Accepted by ' . $service_provider_data->name . '.',
                "created_at" => time(),
                "order_id" => $order_data->id
            );
            $this->my_model->insert_data("user_notifications", $notification_data);
            $notification_data['order_type'] = "Service";
            $notification_data['order_id'] = $order_data->id;
            unset($notification_data['title']);
            unset($notification_data['created_at']);
            $this->my_model->insert_data("admin_notifications", $notification_data);
            $arr = array(
                "status" => true,
                "message" => "Order Accepted"
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "Unable to Accept Order"
            );
        }
        echo json_encode($arr);
        die;
    }

    function send_otp_for_completing_order() {
        $user_id = $this->input->post('user_id');
        $order_id = $this->input->post('order_id');
        if (empty($user_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid User Id"
            );
            echo json_encode($arr);
            die;
        }

        if (empty($user_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid Order Id"
            );
            echo json_encode($arr);
            die;
        }
        $this->check_service_provider($user_id);
//        $otp = rand(0000, 1111);
        $otp = "1234";
        $where = array("id" => $order_id, 'order_status' => 'order_completed');
        $check = $this->my_model->get_data_row('services_orders', $where);
        if (!empty($check)) {
            $arr = array(
                "status" => false,
                "message" => "This Order has been Already Completed"
            );
            echo json_encode($arr);
            die;
        }
        $where = array("id" => $order_id);
        $check = $this->my_model->get_data_row('services_orders', $where);
        if ($check->has_visit_and_quote == "Yes" && $check->balance_amount > 0) {
            $arr = array(
                "status" => false,
                "message" => "Please Request User to pay the remaining Rs." . $check->balance_amount . " to Complete this Order"
            );
            echo json_encode($arr);
            die;
        }
        //send otp functionality here
        $update = $this->my_model->update_data("services_orders", array("id" => $order_id), array("otp" => $otp));
        if ($update) {
            $arr = array(
                "status" => true,
                "message" => "OTP Sent"
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "Unable to Send OTP try Again"
            );
        }
        echo json_encode($arr);
        die;
    }

    function complete_order() {
        $user_id = $this->input->post('user_id');
        $order_id = $this->input->post('order_id');
        $otp = $this->input->post('otp');

        if (empty($user_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid User Id"
            );
            echo json_encode($arr);
            die;
        }

        if (empty($user_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid Order Id"
            );
            echo json_encode($arr);
            die;
        }
        $service_provider_data = $this->check_service_provider($user_id);
        $where = array("id" => $order_id, 'order_status' => 'order_completeds');
        $check = $this->my_model->get_data_row('services_orders', $where);
        if (!empty($check)) {
            $arr = array(
                "status" => false,
                "message" => "This Order has been Already Completed"
            );
            echo json_encode($arr);
            die;
        }
        $where = array("id" => $order_id);
        $check = $this->my_model->get_data_row('services_orders', $where);
        if ($check->has_visit_and_quote == "Yes" && $check->balance_amount > 0) {
            $arr = array(
                "status" => false,
                "message" => "Please Request User to pay the remaining Rs." . $check->balance_amount . " to Complete this Order"
            );
            echo json_encode($arr);
            die;
        }

        $where = array("id" => $order_id, 'otp' => $otp);
        $check = $this->my_model->get_data_row('services_orders', $where);
        if (empty($check)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid OTP"
            );
            echo json_encode($arr);
            die;
        }
        $update = $this->my_model->update_data("services_orders", array("id" => $order_id), array('otp' => "", "order_status" => 'order_completed', 'completed_at' => time()));
        if ($update) {
            $order_data = $this->my_model->get_data_row("services_orders", array("id" => $order_id));
            $coupon_discount = ($order_data->coupon_discount) ? $order_data->coupon_discount : 0;
            $business_discount = ($order_data->bussiness_discount) ? $order_data->bussiness_discount : 0;
            $membership_discount = ($order_data->membership_discount) ? $order_data->membership_discount : 0;
            $used_wallet_amount = ($order_data->used_wallet_amount) ? $order_data->used_wallet_amount : 0;
//            $visiting_charges = ($order_data->visiting_charges) ? $order_data->visiting_charges : 0;
            $user_id = $order_data->user_id;
            $user_data = $this->my_model->get_data_row("users", array("id" => $user_id));
            $this->send_push_notification_model->send_push_notification('Order Completed',
                    'Your Order #' . $order_data->order_id . ' has been Completed by ' . $service_provider_data->name . '.',
                    array($user_data->token),
                    array(),
                    '', 'view_order', $order_data->id);
            $notification_data = array(
                "user_id" => $user_data->id,
                "title" => 'Order (#' . $order_data->order_id . ') Completed',
                "message" => 'Your Order (#' . $order_data->order_id . ') has been Completed',
                "created_at" => time(),
                "order_id" => $order_data->id
            );
            $this->my_model->insert_data("user_notifications", $notification_data);
            $notification_data['order_type'] = "Service";
            $notification_data['order_id'] = $order_data->id;
            unset($notification_data['title']);
            unset($notification_data['created_at']);
            $this->my_model->insert_data("admin_notifications", $notification_data);
            $provider_id = $order_data->accepted_by_service_provider;
            $order_items = $this->my_model->get_data("service_orders_items", array("order_id" => $order_data->order_id));
            $admin_commission = 0;
            $gst = 0;
            $service_provider_comission = 0;
            $calc_total = 0;
//            print_r($calc_total);
//            die;
//            
//            
//            
//            if ($order_data->has_visit_and_quote == "No") {
//                foreach ($order_items as $item) {
//                    $comission_data = $this->my_model->get_data_row("provider_comissions", array("providers_id" => $provider_id, "cat_id" => $item->category_id));
//                    $admin_commission_percentage = $comission_data->admin_comission;
//                    $gst_percentage = $comission_data->gst;
//                    $sub_total = $item->sub_total + $item->visiting_charges;
//                    $calc_total += $sub_total;
//                    $current_gst = ($sub_total * $gst_percentage) / 100;
//                    $current_admin_commission = ($sub_total * $admin_commission_percentage) / 100;
//                    $current_service_provider_comission = $sub_total - ($current_admin_commission + $current_gst);
//                    $this->my_model->update_data("service_orders_items", array("id" => $item->id), array("admin_comission" => $current_admin_commission, "service_provider_comission" => $current_service_provider_comission, "gst" => $current_gst));
//                    $gst += $current_gst;
//                    $admin_commission += $current_admin_commission;
//                    $service_provider_comission += $current_service_provider_comission;
//                }
//            }
//            if ($order_data->has_visit_and_quote == "Yes") {
            $comission_data = $this->my_model->get_data_row("provider_comissions", array("providers_id" => $provider_id, "cat_id" => $order_data->ordered_categories));
            $admin_commission_percentage = $comission_data->admin_comission;
            $gst_percentage = $comission_data->gst;
            $sub_total = ($order_data->grand_total - $order_data->tax) + $order_data->used_wallet_amount + $order_data->discount + $order_data->bussiness_discount + $coupon_discount + $membership_discount;
            $calc_total += $sub_total;
            $current_gst = ($sub_total * $gst_percentage) / 100;
            $current_admin_commission = ($sub_total * $admin_commission_percentage) / 100;
            $current_service_provider_comission = $sub_total - ($current_admin_commission + $current_gst);
            $gst += $current_gst;
            $admin_commission += $current_admin_commission;
            $service_provider_comission += $current_service_provider_comission;
            $tax = $order_data->tax;
//            }

            $admin_commission = $admin_commission - ($coupon_discount + $business_discount + $membership_discount + $used_wallet_amount);
            $this->db->order_by("price", "asc");
            $pricepoint_id = $this->my_model->get_data_row("price_points", "price >=" . $calc_total)->id;
            $franchise_id = $this->my_model->get_data_row("service_providers", array("id" => $provider_id))->franchise_id;
            $franchise_comission_percentage = $this->my_model->get_data_row("franchises_commissions", array("franchise_id" => $franchise_id, "price_point_id" => $pricepoint_id))->commission;
            $franchise_comission = ($calc_total * $franchise_comission_percentage) / 100;
            $service_provider_comission = $service_provider_comission - $franchise_comission;

//            $franchise_comission_percentage =;
//            $this->db->select("SUM(service_provider_comission) as service_provider_comission");
//            $total_amount = $this->my_model->get_data_row("services_orders", array("accepted_by_service_provider" => $provider_id, "order_status" => "order_completed"))->service_provider_comission;
//            $total_balance_amount = $this->my_model->get_data_row("services_orders", array("accepted_by_service_provider" => $provider_id, "order_status" => "order_completed"))->balance_amount;
            $prev_commission = $this->my_model->get_data_row("service_providers_payments", array("provider_id" => $provider_id));
            $total_amount = round($service_provider_comission, 2) + $prev_commission->total_amount;
            $refundalble_amount = 0;
            if ($order_data->balance_amount > 0) {
                $others_comissions = round($admin_commission + ($coupon_discount + $business_discount + $membership_discount + $used_wallet_amount), 2) + round($franchise_comission, 2) + round($gst, 2); // others comissions money

                if ($others_comissions > $order_data->amount_paid) {
                    $refundalble_amount = $others_comissions - $order_data->amount_paid;
                    $total_amount -= $refundalble_amount;
                } else {
                    $total_amount = $prev_commission->total_amount;
                    $cc_comission = $service_provider_comission - $order_data->balance_amount;
                    $total_amount += $cc_comission;
                }
//                $total_amount -= $order_data->balance_amount;
            }
            $order_update_amounts = array(
                "admin_comission" => round($admin_commission, 2),
                "service_provider_comission" => round($service_provider_comission, 2),
                "gst" => round($gst, 2), 'franchise_comission' => round($franchise_comission, 2),
                "amount_collected" => $order_data->balance_amount,
                "amount_to_be_paid_to_admin" => $refundalble_amount,
                "balance_amount" => "0"
            );
            $this->my_model->update_data("services_orders", array("id" => $order_data->id), $order_update_amounts);
            $transaction_w_type = ($total_amount > $prev_commission->total_amount) ? "credit" : "debit";
            $transaction_w_amount = $total_amount - $prev_commission->total_amount;
            $provider_amount = array(
                "provider_id" => $provider_id,
                "total_amount" => "$total_amount",
                "total_requested_amount" => ""
            );
            $check_provider_wallet = $this->my_model->get_data_row("service_providers_payments", array("provider_id" => $provider_id));
            if (empty($check_provider_wallet)) {
                $this->my_model->insert_data("service_providers_payments", $provider_amount);
                $this->my_model->insert_data("provider_wallet_transactions", array("during_order_id" => $order_data->id, "service_provider_id" => $provider_id, "credit_or_debit_amount" => $transaction_w_amount,
                    "type" => $transaction_w_type, "message" => ucfirst($transaction_w_type) . " from the Order $order_data->order_id", "created_at" => time()));
            } else {
                unset($provider_amount['total_requested_amount']);
                unset($provider_amount['provider_id']);
                $this->my_model->update_data("service_providers_payments", array("id" => $check_provider_wallet->id), $provider_amount);
                $this->my_model->insert_data("provider_wallet_transactions", array("during_order_id" => $order_data->id, "service_provider_id" => $provider_id, "credit_or_debit_amount" => $transaction_w_amount,
                    "type" => $transaction_w_type, "message" => ucfirst($transaction_w_type) . " from the Order $order_data->order_id", "created_at" => time()));
            }



            $this->db->select("SUM(franchise_comission) as franchise_comission");
            $total_amount = $this->my_model->get_data_row("services_orders", array("accepted_franchise_id" => $franchise_id, "order_status" => "order_completed"))->franchise_comission;
            $franchise_amount = array(
                "franchise_id" => $franchise_id,
                "total_amount" => "$total_amount",
                "total_requested_amount" => ""
            );
            $check_franchise_wallet = $this->my_model->get_data_row("franchises_services_payments", array("franchise_id" => $franchise_id));
            if (empty($check_franchise_wallet)) {
                $this->my_model->insert_data("franchises_services_payments", $franchise_amount);
            } else {
                unset($franchise_amount['total_requested_amount']);
                unset($franchise_amount['franchise_id']);
                $this->my_model->update_data("franchises_services_payments", array("id" => $check_franchise_wallet->id), $franchise_amount);
            }

            $arr = array(
                "status" => true,
                "message" => "Order Completed"
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "Unable to Complete Order"
            );
        }
        echo json_encode($arr);
        die;
    }

    function get_my_order() {
        $user_id = $this->input->post('user_id');
        $order_id = $this->input->post('order_id');
        $this->check_service_provider($user_id);
        if (empty($order_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid Order Type Provided"
            );

            echo json_encode($arr);
            die;
        }
//        $this->db->select("id,user_id,order_id,time_slot_date,time_slot,ordered_categories,user_addresses_id,"
//                . "grand_total,amount_paid,created_at,order_status,has_visit_and_quote,no_of_services,sub_total,tax,visiting_charges,used_wallet_amount,balance_amount,"
//                . "coupon_code_id,coupon_code,coupon_discount,membership_discount,membership_discount,payment_type,accepted_at,rejected_at,started_at,started_at,"
//                . "cancelled_at,refunded_at,admin_comission,franchise_comission,franchise_comission,gst");
        $order = $this->my_model->get_data_row("services_orders", array("id" => $order_id));
        if ($order) {
            $order->quotation_id = "";
            if ($order->has_visit_and_quote == "Yes") {
                $check_quotation = $this->my_model->get_data_row("visit_and_quote_quotaions", array("service_order_id" => $order->id));
                if (!empty($check_quotation)) {
                    $order->quotation_id = $check_quotation->id;
                }
            }
            $order->time_slot_date = date('d-m-Y', strtotime($order->time_slot_date));
            $order->created_at = ( $order->created_at) ? date('d M Y, h:i A', $order->created_at) : '';
            $order->accepted_at = ( $order->accepted_at) ? date('d M Y, h:i A', $order->accepted_at) : '';
            $order->rejected_at = ( $order->rejected_at) ? date('d M Y, h:i A', $order->rejected_at) : '';
            $order->completed_at = ( $order->completed_at) ? date('d M Y, h:i A', $order->completed_at) : '';
            $order->cancelled_at = ( $order->cancelled_at) ? date('d M Y, h:i A', $order->cancelled_at) : '';
            $order->has_quotation = "No";
            $order->quotation_id = "";
            $order->no_of_milestones = "";
            $order->no_of_milestones_paid = "";
            $quotation = $this->my_model->get_data_row("visit_and_quote_quotaions", array("service_order_id" => $order->id));
            if (!empty($quotation)) {
                $order->has_quotation = "Yes";
                $order->quotation_id = $quotation->id;
                $order->no_of_milestones = $quotation->no_of_milestones;
                $order->no_of_milestones_paid = $quotation->no_of_milestones_paid;
            }
            $user_address = $this->my_model->get_data_row("user_address", array("id" => $order->user_addresses_id));
            $user_address->state_name = $this->my_model->get_data_row("states", array("id" => $user_address->state))->state_name;
            $user_address->city_name = $this->my_model->get_data_row("cities", array("id" => $user_address->city))->city_name;
            $user_address->pincode_pin = $this->my_model->get_data_row("pincodes", array("id" => $user_address->pincode))->pincode;
            $order->user_address = $user_address;
            $order->category_name = $this->my_model->get_data_row('services_categories', array('id' => $order->ordered_categories))->name;
            $order_items = $this->my_model->get_data('service_orders_items', array('order_id' => $order->order_id));
            $services_ids = "";
            foreach ($order_items as $inner) {
                $inner->order_status = $order->order_status;
                $services_ids .= $inner->service_id . ',';
                $inner->service_name = $this->my_model->get_data_row('services', array('id' => $inner->service_id))->service_name;
            }
            $order->services_ids = rtrim($services_ids, ',');
            $order->order_items = $order_items;
            $arr = array(
                "status" => true,
                "data" => $order
            );

            echo json_encode($arr);
            die;
        } else {
            $arr = array(
                "status" => false,
                "message" => "No Order Data Found"
            );
        }
        echo json_encode($arr);
    }

    function get_balance_amount() {
        $order_id = $this->input->post("order_id");
        $balance_amount = $this->my_model->get_data_row("services_orders", array("id" => $order_id));
        if ($balance_amount) {
            $arr = array(
                "status" => true,
                "data" => array(
                    "balance_amount" => $balance_amount->balance_amount
                )
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "No Balance Amount"
            );
        }
        echo json_encode($arr);
        die;
    }

}
