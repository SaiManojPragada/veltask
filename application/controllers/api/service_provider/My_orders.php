<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of My_orders
 *
 * @author Admin
 */
class My_orders extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function get_orders() {
        $user_id = $this->input->post('user_id');
        $type = $this->input->post('type');
        $this->check_service_provider($user_id);
        if (empty($type)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid Order Type Provided"
            );

            echo json_encode($arr);
            die;
        }
        if ($type == "ongoing") {
            $this->db->where("order_status !='order_rejected' AND order_status !='order_completed'  AND order_status !='order_cancelled'");
        } else if ($type == "completed") {
            $this->db->where("order_status ", 'order_completed');
        } else if ($type == "cancelled") {
            $this->db->where("order_status ='order_rejected' OR order_status ='order_cancelled'");
        }
        $orders = $this->my_model->get_data("services_orders", array("user_id" => $user_id), "id", "desc");
        if ($orders) {
            foreach ($orders as $od) {
                $od->payment_type = $this->my_model->get_data_row("payment_methods_enables", array("id" => $od->payment_type))->payment_method_title;
                $od->created_at = ( $od->created_at) ? date('d M Y, h:i A', $od->created_at) : '';
                $od->accepted_at = ( $od->accepted_at) ? date('d M Y, h:i A', $od->accepted_at) : '';
                $od->rejected_at = ( $od->rejected_at) ? date('d M Y, h:i A', $od->rejected_at) : '';
                $od->completed_at = ( $od->completed_at) ? date('d M Y, h:i A', $od->completed_at) : '';
                $od->cancelled_at = ( $od->cancelled_at) ? date('d M Y, h:i A', $od->cancelled_at) : '';
                $od->category_name = $this->my_model->get_data_row("services_categories", array("id" => $od->ordered_categories))->name;
                $od->order_items = sizeof($this->my_model->get_data("service_orders_items", array("order_id" => $od->order_id)));
            }
            $arr = array(
                "status" => true,
                "data" => $orders
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "No Orders Found"
            );
        }
        echo json_encode($arr);
    }

}
