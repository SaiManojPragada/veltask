<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Profile
 *
 * @author Admin
 */
class Profile extends MY_Controller {

    //put your code here

    public $data;

    function __construct() {
        parent::__construct();
    }

    function index() {
        $user_id = $this->input->post('user_id');
        $user_data = $this->check_service_provider($user_id);
        //print_r($user_data); die;
        if (!empty($user_data)) {
            unset($user_data->type);
            unset($user_data->franchise_id);
            unset($user_data->owner_id);
            unset($user_data->password);
            unset($user_data->categories_ids);
            unset($user_data->created_at);
            unset($user_data->updated_at);
            unset($user_data->status);
            unset($user_data->bank_details_file);
            $user_data->aadhar_photo = ($user_data->aadhar_photo) ? base_url('uploads/service_providers/') . $user_data->aadhar_photo : "";
            $user_data->pcc_photo = ($user_data->pcc_photo) ? base_url('uploads/service_providers/') . $user_data->pcc_photo : "";
            $user_data->photo = ($user_data->photo) ? base_url('uploads/service_providers/') . $user_data->photo : "";
            $user_data->vaccination_certificate = ($user_data->vaccination_certificate) ? base_url('uploads/service_providers/') . $user_data->vaccination_certificate : "";
            $user_data->state_name = $this->my_model->get_data_row("states", array("id" => $user_data->state_id))->state_name;
            $user_data->city_name = $this->my_model->get_data_row("cities", array("id" => $user_data->location_id))->city_name;
            $user_data->city_id = $user_data->location_id;
            $user_data->pincode = $this->my_model->get_data_row("pincodes", array("id" => $user_data->pincode_id))->pincode;
            $arr = array(
                "status" => true,
                "message" => "User data",
                "data" => $user_data
            );
        } else {
            $arr = array(
                "status" => true,
                "message" => "User Data now Found"
            );
        }
        echo json_encode($arr);
    }

    function update_profile_image() {
        $user_id = $this->input->post("user_id");
        $check = $this->check_service_provider($user_id);
        $prev_file_image = $check->photo;
        $up_file = $this->image_upload('image', 'service_providers', true, $prev_file_image);
        if ($up_file) {
            $this->my_model->update_data(' service_providers', array("id" => $user_id), array("photo" => $up_file));
            $resp = array("status" => true, "message" => "Profile Image Updated.");
        } else {
            $resp = array("status" => false, "message" => "Something Went Wrong, Please Try Again.");
        }
        echo json_encode($resp);
    }

    function update_profile() {
        $user_id = $this->input->post("user_id");
        $this->check_service_provider($user_id);
        $name = $this->input->post("name");
        $email = $this->input->post("email");
        $phone = $this->input->post("phone");
        $state_id = $this->input->post("state_id");
        $location_id = $this->input->post("city_id");
        $pincode_id = $this->input->post("pincode_id");
        $address = $this->input->post("address");
        $latitude = $this->input->post("latitude");
        $longitude = $this->input->post("longitude");
        $input_data = array(
            "name" => $name,
            "email" => $email,
            "phone" => $phone,
            "state_id" => $state_id,
            "location_id" => $location_id,
            "pincode_id" => $pincode_id,
            "address" => $address,
            "latitude" => $latitude,
            "longitude" => $longitude,
            "updated_at" => time()
        );
        $aadhar_photo = $this->image_upload('aadhar_photo', 'service_providers');
        $pcc_photo = $this->image_upload('pcc_photo', 'service_providers');
        $vaccination_certificate = $this->image_upload('vaccination_certificate', 'service_providers');
        if (!empty($aadhar_photo)) {
            $input_data['aadhar_photo'] = $aadhar_photo;
        }
        if (!empty($pcc_photo)) {
            $input_data['pcc_photo'] = $pcc_photo;
        }
        if (!empty($vaccination_certificate)) {
            $input_data['vaccination_certificate'] = $vaccination_certificate;
        }
        $update = $this->my_model->update_data("service_providers", array("id" => $user_id), $input_data);
        if ($update) {
            $arr = array(
                "status" => true,
                "message" => "Profile Updated."
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "Profile Update Failed."
            );
        }
        echo json_encode($arr);
    }

    function change_password() {
        $user_id = $this->input->post("user_id");
        $user_data = $this->check_service_provider($user_id);
        $current_password = $this->input->post("current_password");
        $new_password = $this->input->post("new_password");
        $reenter_password = $this->input->post("reenter_password");
        if ($new_password != $reenter_password) {
            $arr = array(
                "status" => false,
                "message" => "Passwords does not Match"
            );
            echo json_encode($arr);
            die;
        }
        if ($user_data->password == md5($current_password)) {
            $update = $this->my_model->update_data("service_providers", array("id" => $user_id), array("password" => md5($new_password)));
            if ($update) {
                $arr = array(
                    "status" => true,
                    "message" => "Password Updated"
                );
            } else {
                $arr = array(
                    "status" => false,
                    "message" => "Unable to update Password"
                );
            }
        } else {
            $arr = array(
                "status" => false,
                "message" => "Invalid Current Password"
            );
        }
        echo json_encode($arr);
    }

    function bank_details() {
        $user_id = $this->input->post("user_id");
        $this->check_service_provider($user_id);
        $data = $this->my_model->get_data_row("service_provider_bank_details", array("service_provider_id" => $user_id));
        if (!empty($data)) {
            $data->bank_image = base_url() . "uploads/service_providers/" . $data->bank_image;
            $arr = array(
                "status" => true,
                "message" => "Service Provider Bank details.",
                "data" => $data
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "Service Provider Bank details not Found."
            );
        }
        echo json_encode($arr);
        die;
    }

    function update_bank_details() {
        $user_id = $this->input->post("user_id");
        $this->check_service_provider($user_id);
        $bank_image = $this->image_upload('bank_image', 'service_providers');
        $input_data = array(
            "service_provider_id" => $user_id,
            "bank_name" => $this->input->post('bank_name'),
            "account_number" => $this->input->post('account_number'),
            "ifsc_code" => $this->input->post('ifsc_code'),
            "branch_name" => $this->input->post('branch_name'),
            "created_at" => time()
        );
        if (!empty($bank_image)) {
            $input_data['bank_image'] = $bank_image;
        }
        $this->my_model->delete_data("service_provider_bank_details", array("service_provider_id" => $user_id));
        $ins = $this->my_model->insert_data("service_provider_bank_details", $input_data);
        if ($ins) {
            $arr = array(
                "status" => true,
                "message" => "Bank Details Updated."
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "Bank Details Update Failed."
            );
        }
        echo json_encode($arr);
    }

}
