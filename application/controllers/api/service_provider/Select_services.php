<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Select_services
 *
 * @author Admin
 */
class Select_services extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $user_id = $this->input->post('user_id');
        if (empty($user_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid User Id"
            );
            echo json_encode($arr);
            die;
        }
        $user_data = $this->check_service_provider($user_id);
        $categories_ids = rtrim($user_data->categories_ids, ',');
        $this->db->select('id,name,description');
        $this->db->where("id IN (" . $categories_ids . ")");
        $categories = $this->my_model->get_data("services_categories", null, 'priority', 'desc');
        if ($categories) {
            foreach ($categories as $cat) {
                $check = $this->my_model->get_data_row("service_providers_disabled_categories", array("service_providers_id" => $user_id, "cat_id" => $cat->id));
                $cat->is_active = !empty($check) ? 'No' : 'Yes';
            }
            $arr = array(
                "status" => true,
                "data" => $categories
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "No Services Selected for your Account. Please Contact Admin"
            );
        }
        echo json_encode($arr);
    }

    function get_sub_categories_and_services() {
        $user_id = $this->input->post('user_id');
        $category_id = $this->input->post('category_id');
        if (empty($user_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid User Id"
            );
            echo json_encode($arr);
            die;
        }
        if (empty($category_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid Category Id"
            );
            echo json_encode($arr);
            die;
        }
        $user_data = $this->check_service_provider($user_id);
        $this->db->select('id,sub_category_name,description');
        $data['sub_categories'] = $this->my_model->get_data("services_sub_categories", array("cat_id" => $category_id), 'sub_category_name', 'desc');
        $this->db->where("cat_id", $category_id);
        $this->db->where("sub_cat_id", 0);
        $data['no_sub_category_services'] = $this->db->get("services")->result();
        if ($data['no_sub_category_services']) {
            foreach ($data['no_sub_category_services'] as $scat) {
                $services = $this->my_model->get_data("services", array("cat_id" => $category_id, "sub_cat_id" => $scat->id));
                foreach ($services as $service) {
                    $check = $this->my_model->get_data_row("service_providers_disabled_services", array("service_providers_id" => $user_id, "service_id" => $service->id));
                    $service->is_active = !empty($check) ? 'No' : 'Yes';
                }
                $scat->services = $services;
            }
        }
        if ($data['sub_categories']) {
            foreach ($data['sub_categories'] as $scat) {
                $services = $this->my_model->get_data("services", array("cat_id" => $category_id, "sub_cat_id" => $scat->id));
                foreach ($services as $service) {
                    $check = $this->my_model->get_data_row("service_providers_disabled_services", array("service_providers_id" => $user_id, "service_id" => $service->id));
                    $service->is_active = !empty($check) ? 'No' : 'Yes';
                }
                $scat->services = $services;
            }
        }
        $arr = array(
            "status" => true,
            "data" => $data
        );
        echo json_encode($arr);
    }

//            $sub_cat = $this->my_model->get_data("services_sub_categories", array("cat_id" => $cat->id), 'sub_category_name', 'desc');
//            $cat->sub_categories
}
