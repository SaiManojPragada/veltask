<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Quotation_and_milestones
 *
 * @author Admin
 */
class Quotation_and_milestones extends MY_Controller {

    //put your code here
    function __construct() {
        parent::__construct();
        $this->load->model("quotations_model");
    }

    function index() {
        $service_provider_id = $this->post('user_id');
        $this->check_service_provider($service_provider_id);
        $service_order_id = $this->input->post("service_order_id");
        $data = $this->my_model->get_data_row("visit_and_quote_quotaions", array("service_order_id" => $service_order_id, "service_provider_id" => $service_provider_id));
        if (!empty($data)) {
            $this->db->select("id,quotation_id,amount,date,status,active");
            $data->milestones = $this->my_model->get_data("quotation_milestones", array("quotation_id" => $data->id));
            foreach ($data->milestones as $stone) {
                $stone->date = ($stone->date) ? date('d-m-Y', strtotime($stone->date)) : date('d-m-Y');
            }
            $arr = array(
                "status" => true,
                "data" => $data
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "No quotations found for this service Order."
            );
        }
        echo json_encode($arr);
        die;
    }

    function date_check() {
        $service_provider_id = $this->post('user_id');
        $this->check_service_provider($service_provider_id);
        $service_order_id = $this->input->post("service_order_id");
        $where = array("id" => $service_order_id);
        $check = $this->my_model->get_data_row('services_orders', $where);
        $today = date('Y-m-d');
        if ($check->time_slot_date != $today) {
            $arr = array(
                "status" => false,
                "message" => "This Service cannot be Started, This is Scheduled on " . date('d-m-Y', strtotime($check->time_slot_date)) . "."
            );
            echo json_encode($arr);
            die;
        } else {
            $arr = array(
                "status" => true,
                "message" => "All set to proceed."
            );
            echo json_encode($arr);
            die;
        }
    }

    function post_milestones() {

        $service_provider_id = $this->post('user_id');
        $service_provider_data = $this->check_service_provider($service_provider_id);
        $service_order_id = $this->input->post("service_order_id");
        $check_for_duplicate = $this->my_model->get_data_row("visit_and_quote_quotaions", array(
            "service_order_id" => $service_order_id, "service_provider_id" => $service_provider_id));
        if (!empty($check_for_duplicate)) {
            $arr = array(
                "status" => false,
                "message" => "You've already submitted the Quotation amount for this order.",
                "data" => array(
                    "quotation_id" => $check_for_duplicate->id
                )
            );
            echo json_encode($arr);
            die;
        }

        $where = array("id" => $service_order_id);
        $check = $this->my_model->get_data_row('services_orders', $where);
        $today = date('Y-m-d');
        if ($check->time_slot_date != $today) {
            $arr = array(
                "status" => false,
                "message" => "This Service cannot be Started, This is Scheduled on " . date('d-m-Y', strtotime($check->time_slot_date)) . "."
            );
            echo json_encode($arr);
            die;
        }

        $project_details = $this->input->post("project_details");
        $work_duration = $this->input->post("work_duration");
        $no_of_milestones = $this->input->post("no_of_milestones");
        $quotation_amount = $this->input->post("quotation_amount");
        $quotation_id = $this->generate_random_key('visit_and_quote_quotaions', 'quotation_id', 'VTQT');
        $order_data = $this->my_model->get_data_row('services_orders', array("id" => $service_order_id));
        if (empty($order_data)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid Order ID, Cannot find an order related to this ID."
            );
            echo json_encode($arr);
            die;
        }
        $user_id = $order_data->user_id;
        $created_at = time();
        $updated_at = time();
        $prep_data = array(
            "quotation_id" => $quotation_id,
            "service_order_id" => $service_order_id,
            "user_id" => $user_id,
            "service_provider_id" => $service_provider_id,
            "project_details" => $project_details,
            "work_duration" => $work_duration,
            "quotation_amount" => $quotation_amount,
            "balance_amount" => $quotation_amount,
            "no_of_milestones_paid" => 0,
            "created_at" => $created_at,
            "updated_at" => $updated_at,
            "no_of_milestones" => $no_of_milestones
        );
        $milestones_json = $this->input->post("milestones_json");
        if (empty($milestones_json)) {
            $arr = array(
                "status" => false,
                "message" => "Milestones Json Cannot be Empty"
            );
            echo json_encode($arr);
            die;
        }
        $res = $this->quotations_model->post_quotation_and_milestones($prep_data, $milestones_json);
        if ($res) {
            $user_data = $this->my_model->get_data_row('users', array('id' => $user_id));
            $this->send_push_notification_model->send_push_notification('Order Quotation', 'Your Order (' . $order_data->order_id . ') has recieved quotation of Rs.' . $quotation_amount . ', from ' . $service_provider_data->name . '.', array($user_data->token), array(), '', 'view_order', $order_data->id);
            $notification_data = array(
                "user_id" => $user_data->id,
                "title" => 'Order Quotation for (#' . $order_data->order_id . ') recieved',
                "message" => 'Your Order (#' . $order_data->order_id . ') has recieved quotation of Rs.' . $quotation_amount,
                "created_at" => time(),
                "order_id" => $service_order_id
            );
            $this->my_model->insert_data("user_notifications", $notification_data);
            $notification_data['order_type'] = "Service";
            $notification_data['order_id'] = $order_data->id;
            unset($notification_data['title']);
            unset($notification_data['created_at']);
            $this->my_model->insert_data("admin_notifications", $notification_data);
            $arr = array(
                "status" => true,
                "message" => "Quotation Posted Succesfully",
                "data" => array(
                    "quotation_id" => $res
                )
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "Unable to post Quotation"
            );
        }
        echo json_encode($arr);
        die;
    }

    function update_milestones() {
        $service_provider_id = $this->post('user_id');
        $service_provider_data = $this->check_service_provider($service_provider_id);
        $quotation_id = $this->input->post("quotation_id");
        $project_details = $this->input->post("project_details");
        $work_duration = $this->input->post("work_duration");
        $no_of_milestones = $this->input->post("no_of_milestones");
        $quotation_amount = $this->input->post("quotation_amount");
        $service_order_id = $this->input->post("service_order_id");
        $milestones_json = $this->input->post("milestones_json");
        $milestones_size = sizeof(json_decode($milestones_json));
        $updated_at = time();
        $this->db->where("status", "paid");
        $paid_milestones_amount = $this->my_model->get_total("quotation_milestones", "amount", array("quotation_id" => $quotation_id));
        $balance_amount = $quotation_amount - (($paid_milestones_amount) < 2 ? 0 : $paid_milestones_amount);
        $prep_data = array(
            "service_order_id" => $service_order_id,
            "project_details" => $project_details,
            "work_duration" => $work_duration,
            "quotation_amount" => $quotation_amount,
            "balance_amount" => $balance_amount,
            "no_of_milestones_paid" => 0,
            "updated_at" => $updated_at,
            "no_of_milestones" => ($no_of_milestones > 0) ? $no_of_milestones : $milestones_size
        );

        if (empty($milestones_json)) {
            $arr = array(
                "status" => false,
                "message" => "Milestones Json Cannot be Empty"
            );
            echo json_encode($arr);
            die;
        }
        $res = $this->quotations_model->update_quotation_and_milestones($quotation_id, $prep_data, $milestones_json);
        if ($res) {
            $order_data = $this->my_model->get_data_row('services_orders', array("id" => $service_order_id));
            $user_id = $order_data->user_id;
            $user_data = $this->my_model->get_data_row('users', array('id' => $user_id));
            $this->send_push_notification_model->send_push_notification('Order Quotation', 'Your Order (' . $order_data->order_id . ') has recieved updated quotation of Rs.' . $quotation_amount . ', from ' . $service_provider_data->name . '.', array($user_data->token), array(), '', 'view_order', $order_data->id);
            $notification_data = array(
                "user_id" => $user_data->id,
                "title" => 'Order Quotation for (#' . $order_data->order_id . ') Updated',
                "message" => 'Your Order (#' . $order_data->order_id . ') has recieved quotation of Rs.' . $quotation_amount,
                "created_at" => time(),
                "order_id" => $order_data->id
            );
            $this->my_model->insert_data("user_notifications", $notification_data);
            $notification_data['order_type'] = "Service";
            $notification_data['order_id'] = $order_data->id;
            unset($notification_data['title']);
            unset($notification_data['created_at']);
            $this->my_model->insert_data("admin_notifications", $notification_data);
            $arr = array(
                "status" => true,
                "message" => "Quotation Updated Succesfully",
                "data" => array(
                    "quotation_id" => $quotation_id
                )
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "Unable to Update Quotation"
            );
        }
        echo json_encode($arr);
        die;
    }

    function add_milestone() {
        $service_provider_id = $this->post('user_id');
        $this->check_service_provider($service_provider_id);
        $quotation_id = $this->post('quotation_id');
        $quotation_data = $this->my_model->get_data_row("visit_and_quote_quotaions", array("id" => $quotation_id));
        $existing_milestones = $quotation_data->no_of_milestones;
        $up_mil = array(
            "no_of_milestones" => $existing_milestones + 1
        );
        $update = $this->my_model->update_data("visit_and_quote_quotaions", array("id" => $quotation_id),
                $up_mil);
        if ($update) {
            $milestone = array(
                "quotation_id" => $quotation_id,
                "amount" => 0,
                "duration_type" => "",
                "duration" => "",
                "date" => "",
                "created_at" => time(),
                "updated_at" => time(),
                "status" => "unpaid"
            );
            $this->my_model->insert_data("quotation_milestones", $milestone);
            $arr = array(
                "status" => true,
                "message" => "New Milestone for the Quotation has been added."
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "Unable to add Milestone for the Quotation."
            );
        }
        echo json_encode($arr);
        die;
    }

    function delete_milestone() {
        $service_provider_id = $this->post('user_id');
        $this->check_service_provider($service_provider_id);
        $milestone_id = $this->input->post("milestone_id");
        $quotation_id = $this->input->post("quotation_id");
        $order_id = $this->input->post("order_id");

        $milestone_data = $this->my_model->get_data_row("quotation_milestones", array("id" => $milestone_id));
        if ($milestone_data->status == 'paid') {
            $arr = array(
                "status" => false,
                "message" => "This Milestone is already paid, cannot be deleted"
            );
            echo json_encode($arr);
            die;
        }
        $milestone_amount = $milestone_data->amount;
        $quotation_data = $this->my_model->get_data_row("visit_and_quote_quotaions", array("id" => $quotation_id));
        if (empty($order_id)) {
            $order_id = $quotation_data->service_order_id;
        }
        $no_of_milestones = $quotation_data->no_of_milestones - 1;
        $order_data = $this->my_model->get_data_row("services_orders", array("id" => $order_id));
        $this->db->trans_start();
        $new_total = $quotation_data->quotation_amount - $milestone_amount;
        $new_balance = $quotation_data->balance_amount - $milestone_amount;
        if ($no_of_milestones != 0) {
            $this->my_model->update_data("visit_and_quote_quotaions", array("id" => $quotation_id), array("quotation_amount" => $new_total, "balance_amount" => $new_balance, 'no_of_milestones' => $no_of_milestones));
        } else {
            $this->my_model->delete_data("visit_and_quote_quotaions", array("id" => $quotation_id));
        }

        $new_order_total = $order_data->grand_total - $milestone_amount;
        $new_order_balance_amount = ($order_data->balance_amount - $milestone_amount);

        if ($order_data->grand_total != $order_data->amount_paid) {
            $this->my_model->update_data("services_orders", array("id" => $order_id), array("grand_total" => $new_order_total, "balance_amount" => $new_order_balance_amount));
        }

        $this->my_model->delete_data("quotation_milestones", array("id" => $milestone_id));

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $arr = array(
                "status" => false,
                "message" => "Something went wrong please try again"
            );
        } else {
            $this->db->trans_commit();
            $order_data = $this->my_model->get_data_row('services_orders', array("id" => $order_id));
            $user_id = $order_data->user_id;
            $user_data = $this->my_model->get_data_row('users', array('id' => $user_id));
            $this->send_push_notification_model->send_push_notification('Order Quotation', 'Your Order (' . $order_data->order_id . ') has been updated.', array($user_data->token), array(), '', 'view_order', $order_id);
            $notification_data = array(
                "user_id" => $user_data->id,
                "title" => 'Order Quotation for (#' . $order_data->order_id . ') Updated',
                "message" => 'Your Order (' . $order_data->order_id . ') has been updated.',
                "created_at" => time(),
                "order_id" => $order_id
            );
            $this->my_model->insert_data("user_notifications", $notification_data);
            $notification_data['order_type'] = "Service";
            $notification_data['order_id'] = $order_data->id;
            unset($notification_data['title']);
            unset($notification_data['created_at']);
            $this->my_model->insert_data("admin_notifications", $notification_data);
            $arr = array(
                "status" => true,
                "message" => "Milestone Deleted"
            );
        }
        echo json_encode($arr);
        die;
    }

}
