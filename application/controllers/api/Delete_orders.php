<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of delete_orders
 *
 * @author Admin
 */
class Delete_orders extends MY_Controller {

    //put your code here
    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->db->truncate("services_orders");
        $this->db->truncate("service_orders_items");
        $this->db->truncate("visit_and_quote_quotaions");
        $this->db->truncate("quotation_milestones");
        $this->db->truncate("franchise_services_withdraw_request");
        $this->db->truncate("service_provider_withdraw_request");
        $this->db->truncate("franchises_services_payments");
        $this->db->truncate("service_providers_payments");
        $this->db->truncate("wallet_transactions");
        $this->db->truncate("provider_notifications");

//        $this->my_model->delete_data("services_orders", "id !='dsadas'");
//        $this->my_model->delete_data("service_orders_items", "id !='dsadas'");
//        $this->my_model->delete_data("visit_and_quote_quotaions", "id !='dsadas'");
//        $this->my_model->delete_data("quotation_milestones", "id !='dsadas'");
//        $this->my_model->delete_data("franchise_services_withdraw_request", "id !='dsadas'");
//        $this->my_model->delete_data("service_provider_withdraw_request", "id !='dsadas'");
//        $this->my_model->delete_data("franchises_services_payments", "id !='dsadas'");
//        $this->my_model->delete_data("service_providers_payments", "id !='dsadas'");
        echo "Deleted Orders and Milestones";
        die;
    }

}
