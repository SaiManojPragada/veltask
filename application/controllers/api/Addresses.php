<?php

class Addresses extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function get_user_addresses() {
        $user_id = $this->input->post("user_id");
        if (!empty($user_id)) {
            $this->check_user_id($user_id);
            $addresses = $this->my_model->get_data('user_address', array("user_id" => $user_id));
            if ($addresses) {
                $arr = array(
                    "status" => true,
                    "message" => "User Addresses",
                    "data" => $addresses
                );
            } else {
                $arr = array(
                    "status" => false,
                    "message" => "No Addresses Found"
                );
            }
        } else {
            $arr = array(
                "status" => false,
                "message" => "Invalid User Id"
            );
        }
        echo json_encode($arr);
    }

    function get_selected_address() {
        $user_id = $this->input->post("user_id");
        if (!empty($user_id)) {
            $this->check_user_id($user_id);
            $address = $this->my_model->get_data_row('user_address', array("user_id" => $user_id, "is_selected" => 1));
            if ($address) {
                $arr = array(
                    "status" => true,
                    "message" => "User Selected Address",
                    "data" => $address
                );
            } else {
                $arr = array(
                    "status" => false,
                    "message" => "No Selected Address Found"
                );
            }
        } else {
            $arr = array(
                "status" => false,
                "message" => "Invalid User Id"
            );
        }
        echo json_encode($arr);
    }

    function change_selected_address() {
        $user_id = $this->input->post("user_id");
        $address_id = $this->input->post("address_id");
        if (!empty($user_id)) {
            $this->check_user_id($user_id);
            $this->my_model->update_data('user_address', array("user_id" => $user_id), array("isdefault" => 'no'));
            $address = $this->my_model->update_data('user_address', array("user_id" => $user_id, "id" => $address_id), array("isdefault" => 'yes'));
            if ($address) {
                $arr = array(
                    "status" => true,
                    "message" => "Default Address Changed",
                    "data" => $this->my_model->get_data_row('user_address', array("user_id" => $user_id, "is_selected" => 1))
                );
            } else {
                $arr = array(
                    "status" => false,
                    "message" => "Something went wrong, please Try Again."
                );
            }
        } else {
            $arr = array(
                "status" => false,
                "message" => "Invalid User Id"
            );
        }
        echo json_encode($arr);
    }

}
