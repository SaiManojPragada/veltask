<?php

class Version_control extends CI_Controller {

    //put your code here

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $type = strtolower($this->input->post("type"));                // user or Provider
        if ($type == "user") {
            $this->db->select("android_user_app_version");
            $version = $this->db->get("site_settings")->row()->android_user_app_version;
            $arr = array('status' => true, 'data' => ["version" => $version]);
        } else if ($type == "provider") {
            $this->db->select("android_provider_app_version");
            $version = $this->db->get("site_settings")->row()->android_provider_app_version;
            $arr = array('status' => true, 'data' => ["version" => $version]);
        } else {
            $arr = array('status' => false, 'message' => "Something went Wrong.");
        }
        echo json_encode($arr);
    }

}
