<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pre_pay_balance_payment
 *
 * @author Admin
 */
class Pre_pay_balance_payment extends MY_Controller {

    //put your code here

    function __construct() {
        parent::__construct();
    }

    function index() {
        $user_id = $this->input->post("user_id");
        $this->check_user_id($user_id);
        $order_id = $this->input->post("order_id");
        $amount_paid = $this->input->post("amount_paid");
        $pre_pay_razorpay_order_id = $this->input->post("razorpay_order_id");
        $pre_pay_razorpay_transaction_id = $this->input->post("razorpay_transaction_id");

        $order_details = $this->my_model->get_data_row("services_orders", array("id" => $order_id));
        $prev_balance_amount = $order_details->balance_amount;
        $prev_amount_paid = $order_details->amount_paid;
        $current_balance_amount = $prev_balance_amount - $amount_paid;
        $current_amount_paid = $prev_amount_paid + $amount_paid;
        $inp_data = array(
            "balance_amount" => $current_balance_amount,
            "amount_paid" => $current_amount_paid,
            "pre_pay_razorpay_order_id" => $pre_pay_razorpay_order_id,
            "pre_pay_razorpay_transaction_id" => $pre_pay_razorpay_transaction_id
        );
        $update = $this->my_model->update_data("services_orders", array("id" => $order_id, "user_id" => $user_id), $inp_data);
        if ($update) {
            $this->my_model->update_data("service_orders_items", array("order_id" => $order_details->order_id), array("is_paid" => 1));

            $this->load->model('service_provider_notifications_model');
            $this->service_provider_notifications_model->send_amount_paid($order_id, $amount_paid);
            $arr = array(
                "status" => true,
                "message" => "Payment Completed"
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "Payment Failed"
            );
        }
        echo json_encode($arr);
        die;
    }

}
