<?php

class Apply_coupon_and_wallet extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $user_id = $this->input->post("user_id");
        $this->check_user_id($user_id);
        $coupon_code = $this->input->post("coupon_code");
        $use_wallet = $this->input->post("use_wallet");
        if (empty($use_wallet)) {
            $arr = array(
                "status" => false,
                "message" => "Enter Use Wallet"
            );
            echo json_encode($arr);
        }
        $user_cart_total = $this->my_model->get_total("cart", "grand_total", array("user_id" => $user_id));
        if (!empty($user_cart_total)) {
            echo json_encode($user_cart_total);
        } else {
            $arr = array(
                "status" => false,
                "message" => "No Items In Your Cart"
            );
        }
        echo json_encode($arr);
    }

}
