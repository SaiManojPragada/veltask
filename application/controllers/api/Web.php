<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Web extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('cart_services_model');
    }

    function resend_otp() {
        $user_id = $this->input->post("user_id");
//        $otp = rand(1000, 9999);
        $otp = '1234';
        $user_data = $this->my_model->get_data_row("users", array("id" => $user_id));
        $update = $this->my_model->update_data('users', array('id' => $user_id), array('otp' => $otp));
        if ($update) {
            $resp = array('status' => True, 'message' => "OTP sent");
        } else {
            $resp = array('status' => FALSE, 'message' => "Failed to resend OTP, Please try Again");
        }
        echo json_encode($resp);
        die;
    }

    function verify_otp() {
        $user_id = $this->input->post("user_id");
        $otp = $this->input->post("otp");
        unset($_POST);
        $qry = $this->my_model->get_data_row("users", array("id" => $user_id, "otp" => $otp));
        if (!empty($qry)) {
            $ar = array('otp_status' => 1);
            $wr = array('id' => $user_id);
            $ins = $this->db->update("users", $ar, $wr);
            if ($ins) {
                $stu_row = $qry;
                $phone = $stu_row->phone;
                $st_email = $stu_row->email;
                $email = $st_email;
                $row = $qry;
                $name = $row->first_name . " " . $row->last_name;
                $image = (!empty($qry->image)) ? base_url('uploads/users/') . $qry->image : '';
                $data = array('name' => $name, 'user_id' => $row->id, 'phone' => $phone, 'email' => $email, 'image' => $image);
                $this->session->set_userdata("user", $data);
                $res = array('status' => TRUE, 'message' => 'Login success');
                echo json_encode($res);
            }
        } else {
            echo json_encode(array('status' => FALSE, 'message' => "Invalid OTP"));
        }
        die;
    }

    function get_user_location() {
        $selectedlocation = $this->input->post('selectedlocation');
        $prepAddr = str_replace(' ', '+', $selectedlocation);
        $apiKey = 'AIzaSyDiiWRvGqRwOJOVqYc8GEQhBuWj4mgSSTM'; // Google maps now requires an API key.

        $geocode = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($prepAddr) . '&sensor=false&key=' . $apiKey);

        $output = json_decode($geocode);
        $lat = $output->results[0]->geometry->location->lat;
        $lng = $output->results[0]->geometry->location->lng;

        $user_id = USER_ID;
        if (!empty($user_id)) {
            $this->my_model->update_data("users", array("id" => $user_id), array('lat' => $lat, 'lng' => $lng));
        }
        $this->session->set_userdata('USER_CURRENT_LOCATION_LNG', $lng);
        $this->session->set_userdata('USER_CURRENT_LOCATION_LAT', $lat);
        echo "success";
        die;
    }

    function update_user_location() {
        $lat = $this->input->post('lat');
        $lng = $this->input->post('lng');
        $user_id = USER_ID;
        if (!empty($user_id)) {
            $this->my_model->update_data("users", array("id" => $user_id), array('lat' => $lat, 'lng' => $lng));
        }
        $this->session->set_userdata('USER_CURRENT_LOCATION_LNG', $lng);
        $this->session->set_userdata('USER_CURRENT_LOCATION_LAT', $lat);
        echo "success";
        die;
    }

    function update_profile_image($user_id) {
        $check = $this->user->check_users($user_id);
        if (!$check['status']) {
            $resp = array("status" => false, "message" => "User Does not exists.");
            echo json_encode($resp);
            die;
        }
        $prev_file_image = $check['data']->image;
        $up_file = $this->image_upload('image', 'users', true, $prev_file_image);
        unset($_POST);
        if ($up_file) {
            $update = $this->my_model->update_data('users', array("id" => $user_id), array("image" => $up_file));
            if ($update) {
                $user_data = $this->session->userdata("user");
                $user_data['image'] = base_url('uploads/users/') . $up_file;
                $this->session->set_userdata("user", $user_data);
                $this->session->set_flashdata('success', 'Profile Image Updated');
                redirect($this->agent->referrer());
                die;
            } else {
                $this->session->set_flashdata('error', 'Failed to Update Profile Image');
                redirect($this->agent->referrer());
                die;
            }
        } else {
            $this->session->set_flashdata('error', 'Invalid Image Provided');
            redirect($this->agent->referrer());
            die;
        }
    }

    function subscribe_user() {
        $email = $this->input->post('email');
        $check = $this->my_model->get_data_row('subscribed_emails', array('email' => $email));
        if (empty($check)) {
            $data = array(
                'email' => $email,
                'created_at' => time()
            );
            $ins = $this->my_model->insert_data('subscribed_emails', $data);
            if ($ins) {
                $resp = array(
                    "status" => true,
                    "message" => "Thank you, you've successfully Subscribed."
                );
            } else {
                $resp = array(
                    "status" => false,
                    "message" => "Unable to Subscribe, Please try again."
                );
            }
        } else {
            $resp = array(
                "status" => false,
                "message" => "Email exists, You've already subscribed."
            );
        }
        echo json_encode($resp);
        die;
    }

    function cart() {
        $this->check_user_id(USER_ID);
        $data = array(
            "user_id" => USER_ID,
            "session_id" => USER_SESSION_ID,
            "service_id" => $this->input->post('service_id'),
            "price" => "",
            "quantity" => $this->input->post('quantity'),
            "unit_price" => "",
            "visiting_charges" => "",
            "tax" => "",
            "has_visit_and_quote" => $this->input->post('has_visit_and_quote')
        );
        $resp = $this->cart_services_model->addUpdateCart($data);
        echo json_encode($resp);
        die;
    }

    function process_time_slots() {
        $time_slot_date = $this->input->post('date');
        $time_slot_time = $this->input->post('time');
        if (empty($time_slot_date) || empty($time_slot_time)) {
            echo false;
            die;
        } else {
            $this->session->set_userdata("time_slots_date", $time_slot_date);
            $this->session->set_userdata("time_slots_time", $time_slot_time);
            echo true;
            die;
        }
    }

    function make_subscription() {
        $subs_id = $this->input->post("id");
        $this->session->set_userdata("selected_subscription_id", $subs_id);
        echo true;
        die;
    }

    function apply_coupon() {
        $this->session->set_userdata("service_coupon_data", $this->input->post());
        return true;
    }

    function remove_coupon() {
        $this->session->unset_userdata("service_coupon_data");
        return true;
    }

    function delete_cart_item() {
        $user_id = $this->input->post("user_id");
        $id = $this->input->post("id");
        $delete = $this->my_model->delete_data("services_cart", array("id" => $id, "user_id" => $user_id));
        if ($delete) {
            $this->session->unset_userdata("service_coupon_data");
            $arr = array(
                "status" => true,
                "message" => "Service Removed from the Cart"
            );
        } else {
            $arr = array(
                "status" => true,
                "message" => "Unable to Remove Service from the Cart"
            );
        }
        echo json_encode($arr);
        die;
    }

    function set_payment_type() {
        $this->session->set_userdata("payment_type_selected", $this->input->post("payment_type"));
        return true;
    }

    function set_filters() {
        $services_filters = array(
            "min" => $this->input->post("min"),
            "max" => $this->input->post("max"),
            "selected_prices" => $this->input->post("selected"),
            "cat" => $this->input->post("cat"),
            "price_filter_data" => $this->input->post("price_filter_data")
        );
        $this->session->set_userdata("services_filters", $services_filters);
        return true;
    }

    function remove_filters() {
        $this->session->unset_userdata("services_filters");
        return true;
    }

    function bank_details() {
        $user_id = $this->input->post("user_id");
        $data = $this->my_model->get_data_row("franchise_bank_details", array("franchise_id" => $user_id));
        if (!empty($data)) {
            $arr = array(
                "status" => true,
                "message" => "Franchise Bank details.",
                "data" => $data
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "FranchiseBank details not Found."
            );
        }
        echo json_encode($arr);
        die;
    }

    function chec_for_v_a_q() {
        $user_id = $this->input->post("user_id");
        $cart = $this->my_model->get_data("services_cart", array("user_id" => $user_id));
        if (empty($cart)) {
            echo true;
        } else {
            echo false;
        }
    }

}
