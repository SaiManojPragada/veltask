<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

//include Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;
class Fashionmaa_api extends REST_Controller {

    public function __construct() { 
      header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    
        parent::__construct();
        
        //load user model
        $this->load->model('user');
        //$this->load->library('email'); 
        
    }

    public function user_post() {
       $userData = array();

       if($this->post('action')=='user_registration')
       {
              $first_name = $this->post('first_name');
              $last_name = $this->post('last_name');
              $email = $this->post('email');
              $password = md5($this->post('password'));
              $phone = $this->post('phone');
              $referral_code = $this->post('referral_code');
              $data = array('first_name' =>$first_name,'last_name' =>$last_name, 'email' =>$email, 'password' =>$password,'phone'=>$phone);
               $chk = $this->user->doRegister($data,$referral_code);
               if($chk=='error')
               {
                  $this->response($chk, REST_Controller::HTTP_OK);  
               }
               else
               {
                  $this->response($chk, REST_Controller::HTTP_OK);
               }
       }
       else if($this->post('action')=='resend_otp')
       {
               $user_id = $this->post('user_id');
               $chk = $this->user->resendOTP($user_id);
               if($chk=='error')
               {
                  $this->response($chk, REST_Controller::HTTP_OK);  
               }
               else
               {
                  $this->response($chk, REST_Controller::HTTP_OK);
               }
       }

       
       else if($this->post('action')=='social_login')
       {
              $username = $this->post('username');
              $email = $this->post('email');
              $loginstatus = $this->post('loginstatus');

              $data = array('first_name' =>$username,'email' =>$email,'loginstatus'=>$loginstatus);
               $chk = $this->user->doFacebookRegister($data);
               if($chk=='error')
               {
                  $this->response($chk, REST_Controller::HTTP_OK);  
               }
               else
               {
                  $this->response($chk, REST_Controller::HTTP_OK);
               }
       }
       else if($this->post('action')=='otp_verification')
       {
              $user_id = $this->post('user_id');
              $otp = $this->post('otp');
               $chk = $this->user->verify_OTP($user_id,$otp);
               if($chk=='error')
               {
                  $this->response($chk, REST_Controller::HTTP_OK);  
               }
               else
               {
                  $this->response($chk, REST_Controller::HTTP_OK);
               }
       }
       else if($this->post('action')=='login')
       {
              $username = $this->post('username');
              $password = md5($this->post('password'));
               $chk = $this->user->checkLogin($username,$password);
               if($chk=='error')
               {
                  $this->response($chk, REST_Controller::HTTP_OK);  
               }
               else
               {
                  $this->response($chk, REST_Controller::HTTP_OK);
               }
       }
       else if($this->post('action')=='forgotpassword')
       {
              $phone = $this->post('phone');
               $chk = $this->user->checkForgot($phone);
               if($chk=='error')
               {
                  $this->response($chk, REST_Controller::HTTP_OK);  
               }
               else
               {
                  $this->response($chk, REST_Controller::HTTP_OK);
               }
       }
        else if($this->post('action')=='resetPassword')
       {
              $otp = $this->post('otp');
              $password = $this->post('password');
              $phone = $this->post('phone');
               $chk = $this->user->resetPassword($phone,$otp,$password);
               if($chk=='error')
               {
                  $this->response($chk, REST_Controller::HTTP_OK);  
               }
               else
               {
                  $this->response($chk, REST_Controller::HTTP_OK);
               }
       }
       else if($this->post('action')=='add_useraddress')
       {
              $user_id = $this->post('user_id');
              $name = $this->post('name');
              $mobile = $this->post('mobile');
              $address = $this->post('address');
              $locality = $this->post('locality');
              $city = $this->post('city');
              $state = $this->post('state');
              $pincode = $this->post('pincode');
              $address_type  = $this->post('address_type');

               $chk = $this->user->addAddress($user_id,$name,$mobile,$address,$locality,$city,$state,$pincode,$address_type);
             
                  $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='update_useraddress')
       {
            $address_id = $this->post('address_id');
              $user_id = $this->post('user_id');
              $name = $this->post('name');
              $mobile = $this->post('mobile');
              $address = $this->post('address');
              $locality = $this->post('locality');
              $city = $this->post('city');
              $state = $this->post('state');
              $pincode = $this->post('pincode');
              $address_type  = $this->post('address_type');

               $chk = $this->user->updateAddress($address_id,$user_id,$name,$mobile,$address,$locality,$city,$state,$pincode,$address_type);
             
                  $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='user_addresslist')
       {
              $user_id = $this->post('user_id');
              $chk = $this->user->getAddress($user_id);
             
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='getbanner')
       {
              $chk = $this->user->getBanners();
             
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='getvendorbanner')
       {  
              $vendor_id = $this->post('vendor_id');
              $chk = $this->user->getVendorBanners($vendor_id);
             
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       
      else if($this->post('action')=='getcategories')
       {
              $chk = $this->user->getCategories();
             
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='gethome_categories')
       {
              $chk = $this->user->getHomeCategories();
             
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       
       else if($this->post('action')=='getshopsWithcategory')
       {
              $cat_id = $this->post('cat_id');
              $user_id = $this->post('user_id');
              $lat = $this->post('lat');
              $lng = $this->post('lng');
              $chk = $this->user->getshopsWithcategoryID($cat_id,$user_id,$lat,$lng);
             
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='getcategoryWithshop')
       {
              $shop_id = $this->post('shop_id');
              $chk = $this->user->getcategoryWithshopID($shop_id);
             
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='getProducts')
       {
              $cat_id = $this->post('cat_id');
              $shop_id = $this->post('shop_id');
              $user_id = $this->post('user_id');
              $chk = $this->user->getProducts($cat_id,$shop_id,$user_id);
             
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='search_products')
       {
              $cat_id = $this->post('cat_id');
              $shop_id = $this->post('shop_id');
              $user_id = $this->post('user_id');
              $keyword = $this->post('keyword');
              $chk = $this->user->searchProducts($cat_id,$shop_id,$user_id,$keyword);
             
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='filterProducts')
       {
              $type = $this->post('type');
              $shopId = $this->post('shop_id'); 
              $catId = $this->post('catId'); 
              $user_id= $this->post('user_id'); 
              $chk = $this->user->filterProductslist($type,$shopId,$catId,$user_id);
             
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='searchProducts')
       {
              $keyword = $this->post('keyword');
              $chk = $this->user->fetchProducts($keyword);
             
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='productDetails')
       {
              $product_id = $this->post('product_id');
              $chk = $this->user->getProductDetails($product_id);
             
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
        else if($this->post('action')=='productDetails_filter')
       {
              $product_id = $this->post('product_id');
              $json_data = $this->post('json_data');
              $chk = $this->user->productDetailsFilter($product_id,$json_data);
             
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='getDeals')
       {
              $chk = $this->user->getDeals();
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='addToCart')
       {
       		$session_id = $this->post('sid');
       		$variant_id = $this->post('variant_id');
       		$vendor_id = $this->post('vendor_id');
       		$user_id = $this->post('user_id');
       		$price = $this->post('price');
       		$quantity = $this->post('quantity');
              $chk = $this->user->addToCart($session_id,$variant_id,$vendor_id,$user_id,$price,$quantity);
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='cartList')
       {
       		$session_id = $this->post('sid');
              $chk = $this->user->getCartList($session_id);
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='increment_quantity')
       {
          $cart_id = $this->post('cart_id');
          $sid = $this->post('sid');
              $chk = $this->user->incrementQuantity($cart_id,$sid);
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='decrement_quantity')
       {
          $cart_id = $this->post('cart_id');
          $sid = $this->post('sid');
              $chk = $this->user->decrementQuantity($cart_id,$sid);
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='removeCart')
       {
       		  $cart_id = $this->post('cart_id');
              $chk = $this->user->removeCart($cart_id);
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='coupon_codes')
       {
            $shop_id = $this->post('shop_id');
              $chk = $this->user->getCouponcodes($shop_id);
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='apply_coupon')
       {
       		  $coupon_code = $this->post('coupon_code');
       		  $session_id = $this->post('sid');
       		  $coupon_status= $this->post('coupon_status');
            $grand_total= $this->post('grand_total');
              $chk = $this->user->applyCoupon($coupon_code,$session_id,$coupon_status,$grand_total);
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       /*else if($this->post('action')=='remove_coupon')
       {
              $chk = $this->user->removeCoupon();
              $this->response($chk, REST_Controller::HTTP_OK);  
       }*/
       else if($this->post('action')=='doOrder')
       {
       		$session_id = $this->post('sid');
       		$user_id = $this->post('user_id');
       		$vendor_id = $this->post('vendor_id');
       		$delivery_timeslots = $this->post('delivery_timeslots');
       		$deliveryaddress_id = $this->post('deliveryaddress_id');
       		$payment_option = $this->post('payment_option');

       		$sub_total = $this->post('sub_total');
       		$delivery_amount = $this->post('delivery_amount');
       		$grand_total = $this->post('grand_total');
       		$coupon_id= $this->post('coupon_id');
       		$coupon_code= $this->post('coupon_code');
       		$coupon_disount= $this->post('coupon_disount');
          $wallet_amount= $this->post('wallet_amount');
          $wallet_used_amount= $this->post('wallet_used_amount');
          $gst= $this->post('gst');
       		$created_at = time();
       		$order_status = 1;

            $chk = $this->user->doOrder($session_id,$user_id,$vendor_id,$delivery_timeslots,$deliveryaddress_id,$payment_option,$created_at,$order_status,$sub_total,$delivery_amount,$grand_total,$coupon_id,$coupon_code,$coupon_disount,$wallet_amount,$wallet_used_amount,$gst);
            $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='gettime_slots')
       {
       		  $date =  $this->post('date');
              $chk = $this->user->timeSlots($date);
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='myorders')
       {
            $user_id =  $this->post('user_id');
              $chk = $this->user->orderList($user_id);
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='completed_orders')
       {
            $user_id =  $this->post('user_id');
              $chk = $this->user->completedOrders($user_id);
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='orderDetails')
       {
              $order_id =  $this->post('order_id');
              $chk = $this->user->orderDetails($order_id);
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='add_removewhishlist')
       {
              $product_id =  $this->post('product_id');
              $user_id =  $this->post('user_id');
              $chk = $this->user->add_removewhishList($product_id,$user_id);
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='whishlist')
       {
              $user_id =  $this->post('user_id');
              $chk = $this->user->whishList($user_id);
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='profile_details')
       {
              $user_id =  $this->post('user_id');
              $chk = $this->user->profileDetails($user_id);
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='upload_file')
       {
            $user_id =  $this->post('user_id');
            $chk = $this->user->browse_file($user_id);
            $this->response($chk, REST_Controller::HTTP_OK); 
       }
       else if($this->post('action')=='update_profile')
       {
              $user_id =  $this->post('user_id');
              $first_name =  $this->post('first_name');
              $last_name =  $this->post('last_name');
              $image =  $this->post('image');
              $chk = $this->user->updateProfile($user_id,$first_name,$last_name,$image);
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='attributeswithCategory')
       {
              $cat_id =  $this->post('cat_id');
              $chk = $this->user->attributesWithCategory($cat_id);
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='attributeValues')
       {
              $attribute_id =  $this->post('attribute_id');
              $chk = $this->user->fetchattributeValues($attribute_id);
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='userreviews')
       {
              $user_id =  $this->post('user_id');
              $order_id =  $this->post('order_id');
              $vendor_id =  $this->post('vendor_id');
              $review =  $this->post('review');
              $rating =  $this->post('rating');
              $createdat =  time();
              $chk = $this->user->userReviews($user_id,$order_id,$vendor_id,$review,$rating,$createdat);
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='getNearByShops')
       {
              $lat =  $this->post('lat');
              $lng =  $this->post('lng');

              $chk = $this->user->getNearByShops($lat,$lng);
             
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='getNearByVenodorShops')
       {
              $city_id =  $this->post('city_id');

              $chk = $this->user->getNearByVenodorShops($city_id);
             
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='searchNearByShops')
       {
              $lat =  $this->post('lat');
              $lng =  $this->post('lng');
              $title =  $this->post('title');

              $chk = $this->user->searchByNearByShops($lat,$lng,$title);
             
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='getshopsLogo')
       {
              $lat =  $this->post('lat');
              $lng =  $this->post('lng');

              $chk = $this->user->shopsLogo($lat,$lng);
             
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='getvendorDetails')
       {
             $vendor_id =  $this->post('vendor_id'); 
             $chk = $this->user->getVendorProfile($vendor_id);
             $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='add_removeFavorites')
       {
              $shop_id =  $this->post('shop_id');
              $user_id =  $this->post('user_id');
              $chk = $this->user->add_removeFavorites($shop_id,$user_id);
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='getOrdersdetails')
       {
            $oid =  $this->post('oid'); 
             $chk = $this->user->getOrdersDetails($oid);
             $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='cancelOrder')
       {
             $user_id =  $this->post('user_id'); 
             $orderid =  $this->post('order_id'); 
             $chk = $this->user->docancelOrder($user_id,$orderid);
             $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='favoritelist')
       {
              $user_id =  $this->post('user_id');
              $chk = $this->user->favoriteList($user_id);
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='deleteCartDetails')
       {
              $session_id =  $this->post('sid');
              $user_id =  $this->post('user_id');
              $chk = $this->user->deleteCartData($session_id,$user_id);
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='exchange_refund')
       {
              $session_id =  $this->post('order_id');
              $product_id =  $this->post('product_id');
              $user_id =  $this->post('user_id');
              $vendor_id  =  $this->post('vendor_id');
              $cartid  =  $this->post('cartid');
              $delivery_type  =  $this->post('delivery_type');
              $reson = $this->post('message');

              $chk = $this->user->exchangeRefund($session_id,$product_id,$user_id,$vendor_id,$cartid,$delivery_type,$reson);
              $this->response($chk, REST_Controller::HTTP_OK);  
       }

       else if($this->post('action')=='delivery_slots')
       {
               $shop_id  =  $this->post('shop_id');
               $date  =  $this->post('date'); 
              $chk = $this->user->delivery_slots($shop_id,$date);
              $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='razerpay_orderId')
       {
            $razorpay_keyid = 'rzp_test_ywjRok0nPJdn2M';
            $razorpay_secret = 'peGeVXRIW7EM4Kn0gBuxUqYP';
            
            $total_amount = $this->post('grand_total');
            $data = array(
                'amount' => $total_amount . '00', 
                'currency' => 'INR'
            );
            $payload = json_encode($data);
            $ch = curl_init('https://api.razorpay.com/v1/orders');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);
            curl_setopt($ch, CURLOPT_USERPWD, "$razorpay_keyid:$razorpay_secret");  
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($payload))
            ); 
            $result = curl_exec($ch);
            $order_id = json_decode($result)->id;

            $session_id = $this->post('sid');
          $user_id = $this->post('user_id');
          $vendor_id = $this->post('vendor_id');
          $delivery_timeslots = $this->post('delivery_timeslots');
          $deliveryaddress_id = $this->post('deliveryaddress_id');
          $payment_option = $this->post('payment_option');

          $sub_total = $this->post('sub_total');
          $delivery_amount = $this->post('delivery_amount');
          $grand_total = $this->post('grand_total');
          $coupon_id= $this->post('coupon_id');
          $coupon_code= $this->post('coupon_code');
          $coupon_disount= $this->post('coupon_disount');
          $wallet_amount= $this->post('wallet_amount');
          $gst= $this->post('gst');
          $created_at = time();
          $order_status = 1;

         $chk = $this->user->dorazerpayOrder($session_id,$user_id,$vendor_id,$delivery_timeslots,$deliveryaddress_id,$payment_option,$created_at,$order_status,$sub_total,$delivery_amount,$grand_total,$coupon_id,$coupon_code,$coupon_disount,$order_id,$wallet_amount,$gst);
          $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='razerpay_doOrder')
       {
          $order_id = $this->post('order_id');
          $razerpay_orderid = $this->post('razerpay_orderid');
          $razerpay_txnid = $this->post('razerpay_txnid');
           $chk = $this->user->dorazerpaysuccessOrder($order_id,$razerpay_orderid,$razerpay_txnid);
         $this->response($chk, REST_Controller::HTTP_OK); 
      }
      else if($this->post('action')=='getmostViewedProducts')
      {

              $chk = $this->user->getmostViewedProducts();
             
              $this->response($chk, REST_Controller::HTTP_OK);  
      }
      else if($this->post('action')=='products_filters')
      {
              $shop_id  =  $this->post('shop_id');
              $json_data = $this->post('json_data');
              $cat_id= $this->post('cat_id');
              $chk = $this->user->getproductsFilters($json_data,$shop_id,$cat_id);
             
              $this->response($chk, REST_Controller::HTTP_OK);  
      }
      else if($this->post('action')=='socialshare')
      {
          $chk = $this->user->socialShare();
           $this->response($chk, REST_Controller::HTTP_OK);  
      }
      else if($this->post('action')=='getDistance')
      {
          $clat  =  $this->post('clat');
          $clng  =  $this->post('clng');
          $userlat  =  $this->post('userlat');
          $userlng  =  $this->post('userlng');
          $chk = $this->user->getDistance($clat,$clng,$userlat,$userlng);
          $this->response($chk, REST_Controller::HTTP_OK);  
      }
      else if($this->post('action')=='userTransactions')
      {
          $user_id  =  $this->post('user_id');
          $chk = $this->user->getTransactions($user_id);
          $this->response($chk, REST_Controller::HTTP_OK);  
      }
      else if($this->post('action')=='getuserWallet')
      {
          $user_id  =  $this->post('user_id');
          $chk = $this->user->getUserWallet($user_id);
          $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='wallet_razerpay_orderID')
       {
            $razorpay_keyid = 'rzp_test_ywjRok0nPJdn2M';
            $razorpay_secret = 'peGeVXRIW7EM4Kn0gBuxUqYP';
            
            $user_id = $this->post('user_id');
            $total_amount = $this->post('wallet_amount');
            $data = array(
                'amount' => $total_amount . '00', 
                'currency' => 'INR'
            );
            $payload = json_encode($data);
            $ch = curl_init('https://api.razorpay.com/v1/orders');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);
            curl_setopt($ch, CURLOPT_USERPWD, "$razorpay_keyid:$razorpay_secret");  
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($payload))
            ); 
            $result = curl_exec($ch);
            $order_id = json_decode($result)->id;

          
          $created_at = time();
          $order_status = 1;

          $chk = $this->user->getWalletRazerpayOrderId($user_id,$total_amount,$order_id);

          $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='addamounttoWallet')
       {
          $user_id  =  $this->post('user_id');
          $payment_id  =  $this->post('payment_id');
          $razerpay_orderid  =  $this->post('razerpay_orderid');
          $order_id  =  $this->post('order_id');
          $chk = $this->user->addAmountToWallet($user_id,$payment_id,$razerpay_orderid,$order_id);
          $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='getUserbonupoints')
       {
          $user_id  =  $this->post('user_id');
          $chk = $this->user->getUserBonuPoints($user_id);
          $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='doredeemAmount')
       {
          $user_id  =  $this->post('user_id');
          $redeem_amount  =  $this->post('redeem_amount');
          $chk = $this->user->doRedeemAmount($user_id,$redeem_amount);
          $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='getcities')
       {
          $chk = $this->user->getCities();
          $this->response($chk, REST_Controller::HTTP_OK);  
       }
       else if($this->post('action')=='getOrderCoins')
       {
          $user_id  =  $this->post('user_id');
          $chk = $this->user->fetchOrderCoins($user_id);
          $this->response($chk, REST_Controller::HTTP_OK);  
       }


       

       

       

       



       
    }
}



?>