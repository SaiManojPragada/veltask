<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Locations extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function get_cities() {
        $state_id = $this->input->post("state_id");
        $cities = $this->my_model->get_data("cities", array("state_id" => $state_id));
        $op = "<option value=''>-- select city --</option>";
        foreach ($cities as $city) {
            $op .= "<option value='$city->id'>$city->city_name</option>";
        }
        echo $op;
        die;
    }

    function get_pincodes() {
        $city_id = $this->input->post("city_id");
        $pincodes = $this->my_model->get_data("pincodes", array("city_id" => $city_id));
        $op = "<option value=''>-- select pincode --</option>";
        foreach ($pincodes as $pin) {
            $op .= "<option value='$pin->id'>$pin->pincode</option>";
        }
        echo $op;
        die;
    }

    function get_pincodes_json() {
        $city_id = $this->input->post("city_id");
        $pincodes = $this->my_model->get_data("pincodes", array("city_id" => $city_id));
        echo json_encode($pincodes);
        die;
    }

}
