<?php
header('Access-Control-Allow-Origin: *');
header('Content-type: application/json; charset=utf-8');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Admin extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("admin_model");
    }


    function get_states() {

        $francise_id = $this->input->get_post('francise_id');
        $francise_qry = $this->db->query("select * from franchises where id='".$francise_id."'"); 
        $francise_row=$francise_qry->row();
        $state_id = $francise_row->state_id;

        $pincodes = $francise_row->pincode_ids;
        $this->db->where('id', $state_id);
        $states_res = $this->db->get('states')->row();
        /*if (count($states_res) > 0) {
            ?>
            <option value="">Select State</option>
            <?php
            foreach ($states_res as $st) {
                ?>
                <option value="<?php echo $st->id; ?>"><?php echo $st->state_name; ?></option>
                <?php
            }
            die();
        } else {
            ?>
            <option value="">Select State</option>
            <?php
            die();
        }  */


        ?>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">States</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="state_id" id="states">
                             
                                    <option value="<?php echo $states_res->id; ?>"><?php echo $states_res->state_name; ?></option>
                                   
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">City</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="city_id" id="cities" onchange="getLocations(this.value)">
                                <?php
                                $city_qry = $this->db->query("select * from cities where id='".$francise_row->location_id."'");
                                $ct = $city_qry->row();
                                    ?>
                                    <option value="<?= $ct->id ?>"><?= $ct->city_name ?></option>
                                    
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Pincodes</label>
                        <div class="col-sm-10">
                          <?php $ex_pincode = explode(',',$pincodes);
                                foreach ($ex_pincode as $value) 
                                {
                                  $pincode_qry = $this->db->query("select * from pincodes where id='".$value."'");
                                  $pincode_row = $pincode_qry->row();
                           ?>
                          <label><input type="checkbox" name="pincodes[]" id="pincodes" value="<?php echo $pincode_row->id; ?>"> &nbsp; <?php echo $pincode_row->pincode; ?> &nbsp; &nbsp;&nbsp; </label>
                              <?php } ?>
                        </div>
                    </div>

        <?php
    }


    function get_sub_categories() {
        $cat_id = $this->input->get_post('cat_id');
        $this->db->where('cat_id', $cat_id);
        $sub_catgories_res = $this->db->get('sub_categories')->result();
        if (count($sub_catgories_res) > 0) {
            ?>
            <option value="">Select Sub Category</option>
            <?php
            foreach ($sub_catgories_res as $sub) {
                ?>
                <option value="<?php echo $sub->id; ?>"><?php echo $sub->sub_category_name; ?></option>
                <?php
            }
            die();
        } else {
            ?>
            <option value="">Select Sub Category</option>
            <?php
            die();
        }
    }

    function get_cities() {
        $state_id = $this->input->get_post('state_id');
        $this->db->where('state_id', $state_id);
        $locations_res = $this->db->get('cities')->result();
        if (count($locations_res) > 0) {
            ?>
            <option value="">Select Cities</option>
            <?php
            foreach ($locations_res as $loc) {
                ?>
                <option value="<?php echo $loc->id; ?>"><?php echo $loc->city_name; ?></option>
                <?php
            }
            die();
        } else {
            ?>
            <option value="">Select Cities</option>
            <?php
            die();
        }
    }

    function get_city_locations() {
        $city_id = $this->input->get_post('city_id');
        $this->db->where('city_id', $city_id);
        $locations_res = $this->db->get('pincodes')->result();
        if (count($locations_res) > 0) {
            ?>
            <?php
            foreach ($locations_res as $loc) {
                ?>
                <label><input type="checkbox" name="pincodes[]" id="pincodes" value="<?php echo $loc->id; ?>"> &nbsp; <?php echo $loc->pincode; ?> &nbsp; &nbsp;&nbsp; </label>
                <?php
            }
            die();
        } else {
            ?>
            <?php
            die();
        }
    }

    function get_filter_groups() {
        $cat_id = $this->input->get_post('cat_id');
        $this->db->where('find_in_set("' . $cat_id . '", cat_ids) <> 0');
        $filter_groups_res = $this->db->get('filtergroups')->result();
//        echo $this->db->last_query();
//        die();
        if (count($filter_groups_res) > 0) {
            foreach ($filter_groups_res as $f) {
                $list = explode(',', $f->group_values);
                ?>
                <div class="form-group">
                    <label class="col-sm-2 control-label"></label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?= $f->filter_group_name ?>:</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="sub_category" name="sub_cat_id" required>
                                    <option value="">Select <?= $f->filter_group_name ?></option>
                                    <?php
                                    foreach ($list as $l) {
                                        ?>
                                        <option value="1"><?= $l ?></option>
                                        <?php
                                    }
                                    ?>

                                </select>
                            </div>
                        </div>


                    </div>
                </div>



                <?php
            }
            die();
        } else {
            ?>
            <option value="">Select Sub Category</option>
            <?php
            die();
        }
    }

    function get_stock() {
        $product_id = $this->input->get_post('product_id');
        $stock_data = $this->admin_model->get_table_data_by_value('stock', 'product_id', $product_id);
        if (count($stock_data) > 0) {
            $arr = array('status' => "valid", 'message' => "Records Found", 'data' => $stock_data);
            echo json_encode($arr, JSON_PRETTY_PRINT);
            die;
        } else {
            $arr = array('status' => "invalid", 'message' => "Invalid details");
            echo json_encode($arr, JSON_PRETTY_PRINT);
            die;
        }
    }

    function cancel_order() {
        $reason = $this->input->post("cancellation_reason");
        $order_id = $this->input->post("order_id");
        $order_data = $this->my_model->get_data_row("services_orders", array("id" => $order_id));
        $user_data = $this->my_model->get_data_row("users", array("id" => $order_data->user_id));
        $current_user_wallet_amount = $user_data->wallet_amount + $order_data->used_wallet_amount;
        $update = $this->my_model->update_data("services_orders", array("id" => $order_id), array("admin_cancellation_msg" => $reason, "order_status" => "refunded", "refunded_at" => time()));
        if ($update) {
            $this->my_model->update_data("users", array("id" => $user_data->id), array("wallet_amount" => $current_user_wallet_amount));
            $wallet_transaction_arr = array(
                "transaction_id" => $this->generate_random_key("wallet_transactions", "transaction_id", "VT"),
                "user_id" => $user_data->id,
                "price" => $order_data->used_wallet_amount,
                "message" => "Refund for Order (#" . $order_data->order_id . ")",
                "status" => "credit",
                "created_at" => time(),
                "order_id" => $order_data->id
            );
            $this->my_model->insert_data("wallet_transactions", $wallet_transaction_arr);
            $this->load->model('send_push_notification_model');
            $this->send_push_notification_model->send_push_notification('Order Cancellation Approved',
                    'Your Order (#' . $order_data->order_id . ') has be Approved For Cancellation. Thankyou for shopping with us.',
                    array($user_data->token),
                    array(),
                    '', 'view_order', $order_id);
            $notification_data = array(
                "user_id" => $user_data->id,
                "title" => 'Order Cancellation Approved (#' . $order_data->order_id . ')',
                "message" => 'Your Order (#' . $order_data->order_id . ') has be Approved For Cancellation. Thankyou for shopping with us.',
                "created_at" => time(),
                "order_id" => $order_id
            );
            $this->my_model->insert_data("user_notifications", $notification_data);
            $notification_data['order_type'] = "Service";
            $notification_data['order_id'] = $order_data->id;
            unset($notification_data['title']);
            unset($notification_data['created_at']);
            $this->my_model->insert_data("admin_notifications", $notification_data);
            $arr = array('status' => true, 'message' => "Order Approved");
        } else {
            $arr = array('status' => false, 'message' => "Failed to Update, Try Again");
        }
        echo json_encode($arr);
        die;
    }

    function generate_random_key($table, $column, $prefix) {
        $random = $prefix . rand(000000, 99999999);
        $chec = $this->db->where($column, $random)->get($table)->result();
        if ($chec) {
            $this->generate_random_key($table, $column);
            return false;
        } else {
            return $random;
        }
    }

    function delete_milestone() {
        $milestone_id = $this->input->post("milestone_id");
        $quotation_id = $this->input->post("quotation_id");
        $order_id = $this->input->post("order_id");

        $milestone_data = $this->my_model->get_data_row("quotation_milestones", array("id" => $milestone_id));
        $milestone_amount = $milestone_data->amount;
        $quotation_data = $this->my_model->get_data_row("visit_and_quote_quotaions", array("id" => $quotation_id));
        if (empty($order_id)) {
            $order_id = $quotation_data->service_order_id;
        }
        $no_of_milestones = $quotation_data->no_of_milestones - 1;
        $order_data = $this->my_model->get_data_row("services_orders", array("id" => $order_id));
        $this->db->trans_start();
        $new_total = $quotation_data->quotation_amount - $milestone_amount;
        $new_balance = $quotation_data->balance_amount - $milestone_amount;
        if ($no_of_milestones != 0) {
            $this->my_model->update_data("visit_and_quote_quotaions", array("id" => $quotation_id), array("quotation_amount" => $new_total, "balance_amount" => $new_balance, 'no_of_milestones' => $no_of_milestones));
        } else {
            $this->my_model->delete_data("visit_and_quote_quotaions", array("id" => $quotation_id));
        }

        $new_order_total = $order_data->grand_total - $milestone_amount;
        $new_order_balance_amount = $order_data->balance_amount - $milestone_amount;

        if ($order_data->grand_total != $order_data->amount_paid) {
            $this->my_model->update_data("services_orders", array("id" => $order_id), array("grand_total" => $new_order_total, "balance_amount" => $new_order_balance_amount));
        }

        $this->my_model->delete_data("quotation_milestones", array("id" => $milestone_id));

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $arr = array(
                "status" => false,
                "message" => "Something went wrong please try again"
            );
        } else {
            $this->db->trans_commit();
            $arr = array(
                "status" => true,
                "message" => "Milestone Deleted"
            );
        }
        echo json_encode($arr);
        die;
    }

    function reassign_order() {
        $user_id = $this->input->post('user_id');
        $order_id = $this->input->post('order_id');

        if (empty($user_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid User Id"
            );
            echo json_encode($arr);
            die;
        }

        if (empty($user_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid Order Id"
            );
            echo json_encode($arr);
            die;
        }
        $service_provider_data = $this->my_model->get_data_row("service_providers", $user_id);
        $accepted_franchise_id = $service_provider_data->franchise_id;

        $update = $this->my_model->update_data("services_orders", array("id" => $order_id), array("order_status" => 'order_accepted', 'accepted_by_service_provider' => $user_id, 'accepted_franchise_id' => $accepted_franchise_id, 'accepted_at' => time()));
        if ($update) {
            $this->my_model->update_data("visit_and_quote_quotaions", array("service_order_id" => $order_id), array("service_provider_id" => $user_id));
            $order_data = $this->my_model->get_data_row("services_orders", array("id" => $order_id));
            $user_id = $order_data->user_id;
            $user_data = $this->my_model->get_data_row("users", array("id" => $user_id));
            $this->send_push_notification_model->send_push_notification('Order Accepted', 'Your Order #' . $order_data->order_id . ' has been Accepted by ' . $service_provider_data->name . '.', array($user_data->token), array(), base_url('uploads/services_categories/e9a64cbddd987aa5e9bf34e125297bef.jpg'));
            $arr = array(
                "status" => true,
                "message" => "Order Accepted"
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "Unable to Accept Order"
            );
        }
        echo json_encode($arr);
        die;
    }

}
