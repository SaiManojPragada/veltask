<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Provider_ratings
 *
 * @author Admin
 */
class Provider_ratings extends MY_Controller {

    //put your code here
    function __construct() {
        parent::__construct();
    }

    function index() {
        $user_id = $this->input->post('user_id');
        if (empty($user_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid User Id"
            );
            echo json_encode($arr);
            die;
        }
        $this->user->check_users($user_id);
        $order_id = $this->input->post("order_id");
        $rating = $this->input->post("rating");
        $comment = $this->input->post("comment");
        $updata = array(
            "order_id" => $order_id,
            "service_provider_id" => $order_id,
            "user_id" => $user_id,
            "rating" => $rating,
            "comment" => $comment,
            "created_at" => time(),
            "updated_at" => time(),
        );
        $service_provider_id = $this->my_model->get_data("services_orders", array("id" => $order_id))->accepted_by_service_provider;

        $check_rating = $this->my_model->get_data_row("service_provider_ratings", array("service_provider_id" => $service_provider_id, "order_id" => $order_id, "user_id" => $user_id));
        if (!empty($check_rating)) {
            unset($updata['created_at']);
            $update = $this->my_model->update_data("service_provider_ratings", array("id" => $check_rating->id), $updata);
            if ($update) {
                $arr = ["status" => true, "message" => "Provider Review has been Updated"];
            } else {
                $arr = ["status" => false, "message" => "Provider Review has been Updated"];
            }
        } else {
            
        }
        echo json_encode($arr);
        die;
    }

}
