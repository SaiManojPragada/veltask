<?php

class Place_order extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $user_id = $this->input->post("user_id");
        $coupon_code_id = $this->input->post("coupon_code_id");
        $coupon_code = $this->input->post("coupon_code");
        $coupon_discount = $this->input->post("coupon_discount");
        $membership_discount = $this->input->post("membership_discount");
        $business_discount = $this->input->post("business_discount");
        $user_addresses_id = $this->input->post("user_addresses_id");
        $time_slot_date = $this->input->post("time_slot_date");
        $time_slot_date = date('Y-m-d', strtotime($time_slot_date));
        $time_slot = $this->input->post("time_slot");
        $payment_type = $this->input->post("payment_type");
        $amount_paid = $this->input->post('amount_paid');
        $razorpay_order_id = $this->input->post("razorpay_order_id");
        $razorpay_transaction_id = $this->input->post("razorpay_transaction_id");
        $order_id = (!empty($this->input->post("order_id"))) ? $this->input->post("order_id") : $this->generate_random_key("service_orders_items", "order_id", "VT");
        $used_wallet_amount = $this->input->post("used_wallet_amount");
        $cross_stat = $this->input->post("cross_stat");

        $user_data = $this->check_user_id($user_id);
        $check_orders_count = sizeof($this->my_model->get_data("services_orders", array("user_id" => $user_id)));

        if (empty($user_addresses_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid User Address Id"
            );
            echo json_encode($arr);
            die;
        }
        if (empty($time_slot_date)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid Time Slot Date"
            );
            echo json_encode($arr);
            die;
        }
        if (empty($time_slot)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid Time Slot"
            );
            echo json_encode($arr);
            die;
        }


        $membership_discount = !empty($membership_discount) ? $membership_discount : 0;
        $business_discount = !empty($business_discount) ? $business_discount : 0;

        $cart_items = $this->my_model->get_data("services_cart", array("user_id" => $user_id));
        if ($cart_items) {
            $has_visit_and_quote = $cart_items[0]->has_visit_and_quote;
            $ins_arr = array();
            $sub_total = 0;
            $tax = 0;
            $visiting_charges = 0;
            $grand_total = 0;
            if ($has_visit_and_quote == 1) {

                foreach ($cart_items as $item) {
                    $item->id = "";
                    $ordered_categories = $item->category_id;
                    $visiting_charges += $item->visiting_charges;
                    $tax += $item->tax;
                    $sub_total += $visiting_charges;
                    $grand_total += $visiting_charges + $tax;
                    unset($item->status);
                    unset($item->session_id);
                    $item->order_id = $order_id;
                    $item = json_decode(json_encode($item), true);
                    array_push($ins_arr, $item);
                }
            } else {
                foreach ($cart_items as $item) {
                    $item->id = "";
//                $ordered_categories .= $item->category_id . ",";
                    $ordered_categories = $item->category_id;
                    $sub_total += $item->sub_total;
                    $tax += $item->tax;
                    $visiting_charges += $item->visiting_charges;
                    $grand_total += $item->grand_total;
                    unset($item->status);
                    unset($item->session_id);
                    $item->order_id = $order_id;
                    $item = json_decode(json_encode($item), true);
                    array_push($ins_arr, $item);
                }
            }

            $grand_total = $grand_total - ((float) $coupon_discount + (float) $membership_discount + $business_discount);
            $grand_total = $grand_total - $used_wallet_amount;
            $grand_total = ($grand_total < 0) ? 0 : $grand_total;
//            $ordered_categories = rtrim($ordered_categories, ',');
            $ins = $this->db->insert_batch("service_orders_items", $ins_arr);
            $balance = ($grand_total == $amount_paid) ? 0 : $grand_total - $amount_paid;
            $balance = ($balance > 0) ? $balance : 0;
            if ($ins) {
                $entry = array(
                    "user_id" => $user_id,
                    "order_id" => $order_id,
                    "time_slot_date" => $time_slot_date,
                    "time_slot" => $time_slot,
                    "ordered_categories" => $ordered_categories,
                    "user_addresses_id" => $user_addresses_id,
                    "sub_total" => $sub_total,
                    "tax" => $tax,
                    "visiting_charges" => $visiting_charges,
                    "has_visit_and_quote" => ($has_visit_and_quote == 1) ? 'Yes' : 'No',
                    "grand_total" => $grand_total,
                    "amount_paid" => $amount_paid,
                    "balance_amount" => $balance,
                    "no_of_services" => sizeof($ins_arr),
                    "coupon_code_id" => $coupon_code_id,
                    "coupon_code" => $coupon_code,
                    "coupon_discount" => $coupon_discount,
                    "membership_discount" => $membership_discount,
                    "bussiness_discount" => $business_discount,
                    "used_wallet_amount" => $used_wallet_amount,
                    "payment_type" => $payment_type,
                    "razorpay_order_id" => $razorpay_order_id,
                    "razorpay_transaction_id" => $razorpay_transaction_id,
                    "created_at" => time(),
                    "order_status" => "order_placed"
                );
                $ins = $this->my_model->insert_data("services_orders", $entry);
                if ($ins) {
                    if ($used_wallet_amount && $used_wallet_amount > 0) {
                        $updated_wallet_amount = (float) $user_data->wallet_amount - (float) $used_wallet_amount;
                        $this->my_model->update_data("users", array("id" => $user_id), array("wallet_amount" => $updated_wallet_amount));
                        $transaction_log = array(
                            "transaction_id" => $this->generate_random_key("wallet_transactions", "transaction_id", "VT"),
                            "user_id" => $user_id,
                            "price" => $used_wallet_amount,
                            "message" => "Used For Order #" . $order_id,
                            "status" => "debit",
                            "created_at" => time()
                        );
                        $this->my_model->insert_data("wallet_transactions", $transaction_log);
                    }
                    $this->db->where("user_id", $user_id);
                    $this->db->delete("services_cart");
                } else {
                    $this->db->where("user_id", $user_id);
                    $this->db->where("order_id", $order_id);
                    $this->db->delete("service_orders_items");
                }
                if ($check_orders_count < 1) {
                    $referal_amount = $this->my_model->get_data_row("referrals")->referal_amount;
                    $referral_user = $this->my_model->get_data_row("users", array("id" => $user_id))->reffered_by;
                    $user_existing_wallet_amount = $this->my_model->get_data_row("users", array("id" => $referral_user))->wallet_amount;
                    $updated_amount = $user_existing_wallet_amount + $referal_amount;
                    $this->my_model->update_data("users", array("id" => $referral_user), array("wallet_amount" => $updated_amount));
                    $transaction_log = array(
                        "transaction_id" => $this->generate_random_key("wallet_transactions", "transaction_id", "VT"),
                        "user_id" => $referral_user,
                        "price" => $referal_amount,
                        "message" => "Added referral Amount After First Order",
                        "status" => "credit",
                        "created_at" => time()
                    );
                    $this->my_model->insert_data("wallet_transactions", $transaction_log);
                }
                $this->load->model('send_push_notification_model');
                $this->load->model('service_provider_notifications_model');
                if (empty($cross_stat)) {
                    $order_real_id = $this->my_model->get_data_row("services_orders", array("order_id" => $order_id))->id;
                    $this->service_provider_notifications_model->arrange_and_send($user_addresses_id, $order_id);
                    $this->send_push_notification_model->send_push_notification('Order Recieved',
                            'Your Order (#' . $order_id . ') has be recieved. Thankyou for shopping with us.',
                            array($user_data->token),
                            array(),
                            '', 'view_order', $order_real_id);
                    $notification_data = array(
                        "user_id" => $user_data->id,
                        "title" => 'Order Recieved (#' . $order_id . ')',
                        "message" => 'Your Order (#' . $order_id . ') has be recieved. Thankyou for shopping with us.',
                        "created_at" => time(),
                        "order_id" => $order_real_id
                    );
                    $this->my_model->insert_data("user_notifications", $notification_data);
                    $order_data = $this->my_model->get_data_row("services_orders", array("order_id" => $order_id));
                    $notification_data['order_type'] = "Service";
                    $notification_data['order_id'] = $order_data->id;
                    unset($notification_data['title']);
                    unset($notification_data['created_at']);
                    $this->my_model->insert_data("admin_notifications", $notification_data);
                }
                $arr = array("status" => true, "message" => "Order Placed Success", "order_id" => $order_id);
            } else {
                $arr = array("status" => false, "message" => "Failed to Place Order");
            }
        } else {
            $arr = array("status" => false, "message" => "Your Cart is Empty");
        }
        echo json_encode($arr);
    }

//    function Razorpay_create_order() {
//        $user_id = $this->post('user_id');
//        $this->check_user_id($user_id);
//        $creds = $this->my_model->get_data_row("paymentgateway_and_smtp");
//        $razorpay_keyid = $creds->razorpay_key;
//        $razorpay_secret = $creds->razorpay_secret;
//        $total_amount = $this->post('grand_total');
//
//        $explode = explode(".", $total_amount);
//        if ($explode[1] != '') {
//            if (strlen($explode[1]) == 1) {
//                $final = $explode[0] . "" . $explode[1] . "0";
//            } else {
//                $final = $explode[0] . "" . $explode[1];
//            }
//        } else {
//            $final = $explode[0] . "00";
//        }
//
//        $data = array(
//            'amount' => $final,
//            'currency' => 'INR'
//        );
//        $payload = json_encode($data);
//        $ch = curl_init('https://api.razorpay.com/v1/orders');
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
//        curl_setopt($ch, CURLOPT_USERPWD, "$razorpay_keyid:$razorpay_secret");
//        curl_setopt($ch, CURLOPT_POST, true);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
//        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
//            'Content-Type: application/json',
//            'Content-Length: ' . strlen($payload))
//        );
//        $result = curl_exec($ch);
//        $order_id = json_decode($result)->id;
//        if ($order_id) {
//            $arr = array('status' => true, 'data' => array("razorpay_key" => $razorpay_keyid, "razorpay_secret" => $razorpay_secret, "order_id" => $order_id));
//        } else {
//            $arr = array('status' => false, 'message' => "Something Went Wrong Please try Again");
//        }
//        echo json_encode($arr);
//    }
}
