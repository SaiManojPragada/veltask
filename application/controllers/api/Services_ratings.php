<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Services_ratings
 *
 * @author Admin
 */
class Services_ratings extends MY_Controller {

    //put your code here
    function __construct() {
        parent::__construct();
    }

    function submit_rating() {
        $user_id = $this->input->post('user_id');
        if (empty($user_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid User Id"
            );
            echo json_encode($arr);
            die;
        }
        $this->user->check_users($user_id);
        $check = $this->my_model->get_data_row("services_reviews", array("order_id" => $this->input->post("order_id"), "service_id" => $this->input->post("service_id")));
        if (!empty($check)) {
            $arr = array(
                "status" => false,
                "message" => "You've alreday submitted the Feedback"
            );
            echo json_encode($arr);
            die;
        }
//        $inp_data = array(
//            "order_id" => $this->input->post("order_id"),
//            "user_id" => $this->input->post("user_id"),
//            "service_id" => $this->input->post("service_id"),
//            "rating" => $this->input->post("rating"),
//            "comment" => $this->input->post("comment"),
//            "created_at" => time()
//        );
        $service_provider_id = $this->my_model->get_data_row("services_orders", array("id" => $this->input->post("order_id")))->accepted_by_service_provider;
        $_POST['service_provider_id'] = $service_provider_id;
        $_POST['created_at'] = time();
        $_POST['updated_at'] = time();
        $inp_data = $this->input->post();
        $ins = $this->my_model->insert_data("services_reviews", $inp_data);
        if ($ins) {
            $this->my_model->update_data("services_orders", array("id" => $this->input->post("order_id")), array("is_feedback_provided" => 1));
            $arr = array(
                "status" => true,
                "message" => "Thankyou for your Feedback"
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "Failed to Submit your Feedback"
            );
        }
        echo json_encode($arr);
        die;
    }

    function update_rating() {
        $user_id = $this->input->post('user_id');
        if (empty($user_id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid User Id"
            );
            echo json_encode($arr);
            die;
        }
        $this->user->check_users($user_id);
        $id = $this->input->post("id");
        if (empty($id)) {
            $arr = array(
                "status" => false,
                "message" => "Invalid Rating Id"
            );
            echo json_encode($arr);
            die;
        }
//        $inp_data = array(
//            "rating" => $this->input->post("rating"),
//            "comment" => $this->input->post("comment"),
//            "updated_at" => time()
//        );
        $_POST['updated_at'] = time();
        $inp_data = $this->input->post();
        $ins = $this->my_model->update_data("services_reviews", array("id" => $id), $inp_data);
        if ($ins) {
            $this->my_model->update_data("services_orders", array("id" => $this->input->post("order_id")), array("is_feedback_provided" => 1));
            $arr = array(
                "status" => true,
                "message" => "Feedback Updated, Thankyou for your Feedback"
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "Failed to Update your Feedback"
            );
        }
        echo json_encode($arr);
        die;
    }

}
