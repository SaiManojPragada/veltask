<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Business_benefits extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->check_user_id($this->post('user_id'));
        $business_benefits = $this->my_model->get_data_row("business_profile_benifits");
        if (!empty($business_benefits)) {
            unset($business_benefits->created_at);
            unset($business_benefits->updated_at);
            unset($business_benefits->status);
            unset($business_benefits->id);
            $business_benefits->display_image = base_url('uploads/business_benifits_images/') . $business_benefits->display_image;
            $benefits_description = str_replace('<li>', '<li><span style="color:#f26e6a">✓</span> ', $business_benefits->benefits_description);
            $benefits_description = str_replace('<li>', '<p>', $benefits_description);
            $benefits_description = str_replace('</li>', '</p>', $benefits_description);
            $benefits_description = str_replace('<ul>', '', $benefits_description);
            $business_benefits->benefits_description = str_replace('</ul>', '', $benefits_description);
            $this->db->select("id,document_type");
            $document_types = $this->my_model->get_data("business_document_types", null, null, null, true);
            $business_benefits->document_types = $document_types;
            $arr = array(
                "status" => true,
                "data" => $business_benefits
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "No Business data found"
            );
        }
        echo json_encode($arr);
    }

}
