<?php

class Locations extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function get_states() {
        $chk = $this->user->getstates();
        echo json_encode($chk);
    }

    function get_cities() {
        $state = $this->post('state');
        $chk = $this->user->getSelectedCities($state);
        echo json_encode($chk);
    }

    function get_pincodes() {
        $city = $this->post('city_id');
        $chk = $this->user->getselectedPincodes($city);
        echo json_encode($chk);
    }

}
