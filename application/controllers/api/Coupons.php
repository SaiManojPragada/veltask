<?php

class Coupons extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $today = date('Y-m-d');
        $where = "(start_date <= '" . $today . "' AND expiry_date >= '" . $today . "') AND status = 1";
        $coupons = $this->my_model->get_data("services_coupon_codes", $where);
        if ($coupons) {
            $arr = array(
                "status" => true,
                "message" => "Coupon Codes Found",
                "data" => $coupons
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "No Coupon Codes Found"
            );
        }
        echo json_encode($arr);
    }

}
