<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Check_service_providers_availability
 *
 * @author Admin
 */
class Check_service_providers_availability extends MY_Controller {

    //put your code here
    function __construct() {
        parent::__construct();
    }

    function index() {
        $user_id = $this->input->post("user_id");
        $this->check_user_id($user_id);
        $address_id = $this->input->post("address_id");
        if (!empty($this->input->post("date"))) {
            $date = strtotime($this->input->post("date"));
            $allowed_upto_date = strtotime("+30 days");
            if ($date > $allowed_upto_date) {
                $arr = array(
                    "status" => false,
                    "message" => "You can only book 30 days into the Future"
                );
                echo json_encode($arr);
                die;
            }
        }
        if (empty($address_id)) {
            $arr = array(
                "status" => false,
                "message" => "Please Select Address"
            );
            echo json_encode($arr);
            die;
        }
        $user_selected_address = $this->my_model->get_data_row("user_address", array("user_id" => $user_id, "id" => $address_id));
        $user_pincode_id = $user_selected_address->pincode;
        $check_for_service_providers = $this->my_model->get_data("service_providers", "FIND_IN_SET('" . $user_pincode_id . "', sp_pincodes)");
        if (!empty($check_for_service_providers)) {
            $arr = array(
                "status" => true,
                "message" => "Service Providers are present"
            );
        } else {
            $arr = array(
                "status" => false,
                "message" => "No service Providers are present in this area."
            );
        }
        echo json_encode($arr);
        die;
    }

}
