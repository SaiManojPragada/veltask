<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Placeorder extends MY_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        $this->session->unset_userdata("service_coupon_data");
        $this->check_user_log(USER_ID);
    }

    function index() {
        redirect(base_url('cart'));
    }

    function success($order_id = "") {
        $this->data['order_id'] = $order_id;
        $check_order = $this->my_model->get_data_row("services_orders", array("order_id" => $order_id));

        if (!empty($check_order)) {
            $this->data['error'] = false;
            $this->data['order'] = $check_order;
            $this->data['service_category'] = $this->my_model->get_data_row("services_categories", array("id" => $this->data['order']->ordered_categories));
            $order_services = $this->my_model->get_data('service_orders_items', array('order_id' => $this->data['order']->order_id));
            if ($this->data['order']->has_visit_and_quote == "Yes") {
                $this->data['order']->visit_and_quote_val = "<span style='color: green'>(Visit and Quote)</span>";
            } else {
                $this->data['order']->visit_and_quote_val = "";
            }
            foreach ($order_services as $item) {
                $item->service_item = $this->my_model->get_data_row("services", array('id' => $item->service_id));
            }
            $order = $this->data['order'];
            if ($order->amount_paid == $order->grand_total) {
                $this->data['show_payment_status'] = "<span style='color: green'>Paid</span>";
            }
            if ($order->amount_paid > 0 && $order->amount_paid < $order->grand_total) {
                $this->data['show_payment_status'] = "<span style='color: orange'>Partially Paid</span>";
            }
            if ($order->amount_paid == 0) {
                $this->data['show_payment_status'] = "<span style='color: red'>Unpaid</span>";
            }
            $this->data['user_address'] = $this->my_model->get_data_row("user_address", array("id" => $check_order->user_addresses_id));
            $this->data['order_services'] = $order_services;
        } else {
            $this->failed($order_id);
            return false;
        }
        $this->my_view('after_order', $this->data);
    }

    function failed($order_id, $rp_od_id = null, $rp_trans_id = null) {
        $this->data['error'] = true;
        $this->data['order_id'] = $order_id;
        $this->data['rp_od_id'] = $rp_od_id;
        $this->data['rp_trans_id'] = $rp_trans_id;
        $this->my_view('after_order', $this->data);
    }

}
