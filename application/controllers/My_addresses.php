<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class My_addresses extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->check_user_log(USER_ID);
        $this->data['page_name'] = 'my_addresses';
    }

    function index() {

        if (!empty($this->input->post())) {
            $id = $this->input->post('prev_id');
            $data = array(
                "user_id" => USER_ID,
                "name" => $this->input->post("name"),
                "mobile" => $this->input->post("mobile"),
                "address" => $this->input->post("address"),
                "landmark" => $this->input->post("landmark"),
                "address_type" => $this->input->post("address_type"),
                "city" => $this->input->post('city'),
                "state" => $this->input->post('state'),
                "pincode" => $this->input->post('pincode'),
                "created_date" => date('Y-m-d H:i:s')
            );
            if (empty($id)) {
                $data['isdefault'] = "yes";
                $this->my_model->update_data('user_address', array('user_id' => USER_ID), array("isdefault" => 'no'));
                $qry = $this->my_model->insert_data("user_address", $data);
                if ($qry) {
                    $this->session->set_flashdata('success', "Address Added Successfully");
                } else {
                    $this->session->set_flashdata('error', "Unable to add Address");
                }
            } else {
                $qry = $this->my_model->update_data("user_address", array("id" => $id), $data);
                if ($qry) {
                    $this->session->set_flashdata('success', "Address Updated Successfully");
                } else {
                    $this->session->set_flashdata('error', "Unable to Update Address");
                }
            }
            unset($_POST);

            redirect($_SERVER['HTTP_REFERER']);
        }
        $this->data['states'] = $this->my_model->get_data('states');
        $this->data['addresses'] = $this->my_model->get_data('user_address', array('user_id' => USER_ID), 'id', 'desc');
        foreach ($this->data['addresses'] as $item) {
            $item->state_name = $this->my_model->get_data_row("states", array("id" => $item->state))->state_name;
            $item->city_name = $this->my_model->get_data_row("cities", array("id" => $item->city))->city_name;
            $item->pincode_pin = $this->my_model->get_data_row("pincodes", array("id" => $item->pincode))->pincode;
            $item->json_string = json_encode($item);
        }
        $this->my_view('my-addresses', $this->data);
    }

    function change_address_selection() {
        $id = $this->input->post('id');
        $this->my_model->update_data("user_address", array('user_id' => USER_ID), array('isdefault' => "no"));
        $update = $this->my_model->update_data("user_address", array("id" => $id), array('isdefault' => "yes"));
        if ($update) {
            echo 'success';
        } else {
            echo 'error';
        }
        die;
    }

}
