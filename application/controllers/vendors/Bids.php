<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Bids extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('vendors')['vendors_logged_in']!= true) {
            //$this->session->set_flashdata('error', 'Session Timed Out');
            redirect('vendors/login');
        }
    }

    function openbids() 
    {

        $shop_id = $_SESSION['vendors']['vendor_id']; 
      $qry = $this->db->query("SELECT user_bids.*,vendor_bids.created_date,vendor_bids.admin_commission FROM user_bids INNER JOIN vendor_bids ON vendor_bids.bid_id=user_bids.id where vendor_bids.vendor_id='".$shop_id."' and user_bids.bid_status=0 order by user_bids.id desc");

    $result = $qry->result();
    $bid_ar=[];
    if($qry->num_rows()>0)
    {
        foreach ($result as $value1) 
        {
        $cart = $this->db->query("select * from cart where session_id='".$value1->session_id."'");
        $cart_row = $cart->result();
        $cart_ar=[];
        foreach ($cart_row as $value) 
        {
            $varint = $this->db->query("select * from link_variant where id='".$value->variant_id."'");
            $variant_row = $varint->row();

            $prod = $this->db->query("select * from products where id='".$variant_row->product_id."' and delete_status=0");
            $prod_row = $prod->row();


            $pro_img = $this->db->query("select * from product_images where variant_id='".$value->variant_id."' and product_id='".$variant_row->product_id."'");
            $pro_imgs = $pro_img->row();

            if($pro_imgs->image!='')
            {
                $image = base_url()."uploads/products/".$pro_imgs->image;
            }
            else
            {
                $image = base_url()."uploads/noproduct.png";
            }
            $jsondata = json_decode($variant_row->jsondata);
            $attributes=[];
            foreach ($jsondata as $val) 
            {
                $attribute_type=$val->attribute_type;
                $attribute_value=$val->attribute_value;

                $type = $this->db->query("select * from attributes_title where id='".$attribute_type."'");
                $types = $type->row();

                $val12 = $this->db->query("select * from attributes_values where id='".$attribute_value."'");
                $value12 = $val12->row();
                $attributes[]=array('attribute_type'=>$types->title,'attribute_values'=>$value12->value);
            }
            $vendor_cat = $this->db->query("select * from admin_comissions where cat_id='".$prod_row->cat_id."' and shop_id='".$vendor_id."'");
            $vendor_cat_row = $vendor_cat->row();

            if($vendor_cat->num_rows()>0)
            {
                $cart_ar[]=array('id'=>$value->id,'session_id'=>$value->session_id,'product_name'=>$prod_row->name,'price'=>$value->price,'quantity'=>$value->quantity,'total'=>$value->unit_price,'image'=>$image,'attributes'=>$attributes,'admin_commission'=>$value1->admin_commission);
            }
        }

            $cart_count = $cart->num_rows();
            $date = date('d-m-Y,h:i A',strtotime($value1->created_date));
            if($value1->bid_status==0)
            {
                $status = 'open';
            }
            else if($value1->bid_status==1)
            {
                $status = 'Accepted';
            }
            else if($value1->bid_status==2)
            {
                $status = 'Closed';
            }
            $bid_ar[] = array('id'=>$value1->id,'session_id'=>$value1->session_id,'created_at'=>$date,'total_products'=>$cart_count,'cart_products'=>$cart_ar,'cart_count'=>$cart_count,'bidstatus'=>$status);
        }
    }
    else
    {
         $bid_ar = array();
    }
        $this->data['page_name'] = 'openbids';
        $this->data['bidslist'] = $bid_ar;
        $this->load->view('vendors/includes/header', $this->data);
        $this->load->view('vendors/open_bids', $this->data);
        $this->load->view('vendors/includes/footer');
    }



    function closedbids() 
    {

    $shop_id = $_SESSION['vendors']['vendor_id'];
    $qry = $this->db->query("SELECT user_bids.*,vendor_bids.created_date,vendor_bids.admin_commission FROM user_bids INNER JOIN vendor_bids ON vendor_bids.bid_id=user_bids.id where vendor_bids.vendor_id='".$shop_id."' and user_bids.bid_status=2 order by user_bids.id desc");
    $result = $qry->result();
    $bid_ar=[];
    if($qry->num_rows()>0)
    {
        foreach ($result as $value1) 
        {

        $cart = $this->db->query("select * from cart where session_id='".$value1->session_id."'");
        $cart_row = $cart->result();
        $cart_ar=[];
        foreach ($cart_row as $value) 
        {
            $varint = $this->db->query("select * from link_variant where id='".$value->variant_id."'");
            $variant_row = $varint->row();

            $prod = $this->db->query("select * from products where id='".$variant_row->product_id."' and delete_status=0");
            $prod_row = $prod->row();


            $pro_img = $this->db->query("select * from product_images where variant_id='".$value->variant_id."' and product_id='".$variant_row->product_id."'");
            $pro_imgs = $pro_img->row();

            if($pro_imgs->image!='')
            {
                $image = base_url()."uploads/products/".$pro_imgs->image;
            }
            else
            {
                $image = base_url()."uploads/noproduct.png";
            }
            $jsondata = json_decode($variant_row->jsondata);
            $attributes=[];
            foreach ($jsondata as $val) 
            {
                $attribute_type=$val->attribute_type;
                $attribute_value=$val->attribute_value;

                $type = $this->db->query("select * from attributes_title where id='".$attribute_type."'");
                $types = $type->row();

                $val12 = $this->db->query("select * from attributes_values where id='".$attribute_value."'");
                $value12 = $val12->row();
                $attributes[]=array('attribute_type'=>$types->title,'attribute_values'=>$value12->value);
            }
            $vendor_cat = $this->db->query("select * from admin_comissions where cat_id='".$prod_row->cat_id."' and shop_id='".$vendor_id."'");
            $vendor_cat_row = $vendor_cat->row();

            if($vendor_cat->num_rows()>0)
            {
                $cart_ar[]=array('id'=>$value->id,'session_id'=>$value->session_id,'product_name'=>$prod_row->name,'price'=>$value->price,'quantity'=>$value->quantity,'total'=>$value->unit_price,'image'=>$image,'attributes'=>$attributes,'admin_commission'=>$value1->admin_commission);
            }
        }

            $cart_count = $cart->num_rows();
            $date = date('d-m-Y,h:i A',strtotime($value1->created_date));
            if($value1->bid_status==0)
            {
                $status = 'open';
            }
            else if($value1->bid_status==1)
            {
                $status = 'Accepted';
            }
            else if($value1->bid_status==2)
            {
                $status = 'Closed';
            }
            $bid_ar[] = array('id'=>$value1->id,'session_id'=>$value1->session_id,'created_at'=>$date,'total_products'=>$cart_count,'cart_products'=>$cart_ar,'cart_count'=>$cart_count,'bidstatus'=>$status);
        }
    }
    else
    {
         $bid_ar = array();
    }

        $this->data['page_name'] = 'closedbids';
        $this->data['bidslist'] = $bid_ar;
        $this->load->view('vendors/includes/header', $this->data);
        $this->load->view('vendors/closed_bids', $this->data);
        $this->load->view('vendors/includes/footer');
    }


    function viewdetails($bid)
    {
        $vendor_id = $_SESSION['vendors']['vendor_id'];
    $qry = $this->db->query("SELECT * from user_bids where id='".$bid."'");
    
    $value1 = $qry->row();

        $cart = $this->db->query("select * from cart where session_id='".$value1->session_id."'");
        $cart_row = $cart->result();
        $cart_ar=[];
        foreach ($cart_row as $value) 
        {

           
            $varint = $this->db->query("select * from link_variant where id='".$value->variant_id."'");
            $variant_row = $varint->row();

            $prod = $this->db->query("select * from products where id='".$variant_row->product_id."' and delete_status=0");
            $prod_row = $prod->row();


            $pro_img = $this->db->query("select * from product_images where variant_id='".$value->variant_id."' and product_id='".$variant_row->product_id."'");
            $pro_imgs = $pro_img->row();

            if($pro_imgs->image!='')
            {
                $image = base_url()."uploads/products/".$pro_imgs->image;
            }
            else
            {
                $image = base_url()."uploads/noproduct.png";
            }

            $jsondata = json_decode($variant_row->jsondata);
            $attributes=[];
            foreach ($jsondata as $val) 
            {
                $attribute_type=$val->attribute_type;
                $attribute_value=$val->attribute_value;

                $type = $this->db->query("select * from attributes_title where id='".$attribute_type."'");
                $types = $type->row();

                $val12 = $this->db->query("select * from attributes_values where id='".$attribute_value."'");
                $value12 = $val12->row();
                $attributes[]=array('attribute_type'=>$types->title,'attribute_values'=>$value12->value);
            }




            $vendor_cat = $this->db->query("select * from admin_comissions where cat_id='".$prod_row->cat_id."' and shop_id='".$vendor_id."'");
            $vendor_cat_row = $vendor_cat->row();

            if($vendor_cat->num_rows()>0)
            {
                $cart_ar[]=array('id'=>$value->id,'session_id'=>$value->session_id,'product_name'=>$prod_row->name,'price'=>$value->price,'quantity'=>$value->quantity,'total'=>$value->unit_price,'image'=>$image,'attributes'=>$attributes);
            }
        }
        if(count($cart_ar)>0)
        {
            $cart_count = count($cart_ar);

            $date = date('d-m-Y,h:i A',$value1->created_at);

            if($value1->bid_status==0)
            {
                $status = 'open';
            }
            else if($value1->bid_status==1)
            {
                $status = 'Accepted';
            }
            else if($value1->bid_status==2)
            {
                $status = 'open';
            }
            $bid_quote = $this->db->query("select * from vendor_bids where bid_id='".$value1->id."' and vendor_id='".$vendor_id."' ");
            $bid_quote_row = $bid_quote->row();


            $bid_quote1 = $this->db->query("select * from vendor_bids where bid_id='".$value1->id."' and vendor_id='".$vendor_id."' and bid_status!=0");
            if($bid_quote1->num_rows()>0)
            {
                $quote_stat = true;
            }
            else
            {
                $quote_stat = false;
            }

            $bid_ar = array('id'=>$value1->id,'user_id'=>$value1->user_id,'session_id'=>$value1->session_id,'created_at'=>$date,'delivery_amount'=>$value1->delivery_amount,'gst'=>$value1->gst,'sub_total'=>$value1->sub_total,'grand_total'=>$value1->grand_total,'total_products'=>$cart_count,'cart_products'=>$cart_ar,'cart_count'=>$cart_count,'bidstatus'=>$status,'quote_status'=>$quote_stat,'quote_amount'=>$bid_quote_row->total_price,'admin_commission'=>$bid_quote_row->admin_commission);
        }
        
        $this->data['page_name'] = 'closedbids';
        $this->data['biddetails'] = $bid_ar;
        $this->load->view('vendors/includes/header', $this->data);
        $this->load->view('vendors/bid_details', $this->data);
        $this->load->view('vendors/includes/footer');



    }


    function updatequote()
    {
        $bid_id = $this->input->post('bidid');
        $user_id = $this->input->post('user_id');
        $total_price = $this->input->post('total_price');
        $vendor_id = $_SESSION['vendors']['vendor_id'];


    $chk = $this->db->query("select * from vendor_bids where bid_id='".$bid_id."' and vendor_id='".$vendor_id."' and bid_status=1");
    if($chk->num_rows()>0)
    {
        $this->session->set_flashdata('success_message', 'already added the quote, Please try again');
            redirect('vendors/bids/viewdetails/'.$bid_id);
            //return array('status' =>FALSE, 'message'=>"already added the quote, Please try again");
    }
    else
    {
        $ar = array('bid_status'=>1,'bid_id'=>$bid_id,'vendor_id'=>$vendor_id,'user_id'=>$user_id,'total_price'=>$total_price,'accept_at'=>time(),'accept'=>'yes');
        $wr = array('vendor_id'=>$vendor_id,'bid_id'=>$bid_id);
        $ins = $this->db->update("vendor_bids",$ar,$wr);
        if($ins)
        {
                            $title = "Bid Quotation";
                            $message = "You have new quote from vendor";
                            $this->onesignalnotification($user_id,$message,$title);
             //return array('status' =>TRUE,'message'=>"Your Quotation sent Successfully");
             $this->session->set_flashdata('success_message', 'Your Quotation sent Successfully');
              redirect('vendors/bids/viewdetails/'.$bid_id);
            /*$this->load->view('vendors/includes/header', $this->data);
        $this->load->view('vendors/bid_details', $this->data);
        $this->load->view('vendors/includes/footer');*/
        }
    }
    }




    function onesignalnotification($user_id,$message,$title)
    {
        $qr = $this->db->query("select * from users where id='".$user_id."'");
        $res = $qr->row();
                            
                       $platform = $res->platform;

                       if($platform=='android')
                       {
                            if($res->token!='')
                            { 
                                $user_id = $res->token;
                        
                                $fields = array(
                                    'app_id' => 'e072cc7b-595d-4c4c-a451-b07832b073f9',
                                    'include_player_ids' => [$user_id],
                                    'contents' => array("en" =>$message),
                                    'headings' => array("en"=>$title),
                                    'android_channel_id' => 'ea6c19aa-e55f-4243-af28-605a32901234'
                                );
                                    $fields = json_encode($fields);
                                    //print("\nJSON sent:\n");
                                    //print($fields);

                                    $ch = curl_init();
                                    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
                                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                        'Content-Type: application/json; charset=utf-8', 
                                        'Authorization: Basic NzhjMmI5YjItZmViMy00YjNlLWFlMDItY2ZiZTI3OTY0YzYz'
                                    ));
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                                    curl_setopt($ch, CURLOPT_HEADER, FALSE);
                                    curl_setopt($ch, CURLOPT_POST, TRUE);
                                    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                                                                           
                                    $response = curl_exec($ch);
                                    curl_close($ch);
                                    //print_r($response); die;
                                }
                       }
                       else if($platform=='ios')
                       {
                            $user_id = $res->ios_token;
                        
                            $fields = array(
                                'app_id' => 'e072cc7b-595d-4c4c-a451-b07832b073f9',
                                'include_player_ids' => [$user_id],
                                'contents' => array("en" =>$message),
                                'headings' => array("en"=>$title),
                                'android_channel_id' => 'cd5a40e2-504d-44b2-97b8-26ffecf88eed'
                            );
                               $fields = json_encode($fields);
                                //print("\nJSON sent:\n");
                                //print($fields);

                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
                                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                    'Content-Type: application/json; charset=utf-8', 
                                    'Authorization: Basic NzhjMmI5YjItZmViMy00YjNlLWFlMDItY2ZiZTI3OTY0YzYz'
                                ));
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                                curl_setopt($ch, CURLOPT_POST, TRUE);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                                                                       
                                $response = curl_exec($ch);
                                curl_close($ch);
                                //print_r($response); die;
                       }
                        

                  
             
    }

}
