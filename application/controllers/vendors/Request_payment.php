<?php



defined('BASEPATH') OR exit('No direct script access allowed');



class Request_payment extends CI_Controller {



    public $data;



    function __construct() {

        parent::__construct();

        if ($this->session->userdata('vendors')['vendors_logged_in']!= true) {

            //$this->session->set_flashdata('error', 'Session Timed Out');

            redirect('vendors/login');

        }

    }



    function index() {
        $this->data['page_name'] = 'request_payment';
        $vendor_id=$_SESSION['vendors']['vendor_id'];
        //$vendor_id='177';
        $qry = $this->db->query("select * from orders where vendor_id='".$vendor_id."' and order_status=5");
        $result = $qry->result();
        $vendor_amount=0;
        foreach ($result as $value) 
        {
        	                    $cart_qry = $this->db->query("select * from cart where session_id='".$value->session_id."'");
                                              $cart_result = $cart_qry->result();
                                              $admin_total=0;
                                              $unit_price=0;
                                              foreach ($cart_result as $cart_value) 
                                              { 
                                                $link_qry = $this->db->query("select * from link_variant where id='".$cart_value->variant_id."'");
                                                $link_row = $link_qry->row();

                                                $prod_qry = $this->db->query("select * from products where id='".$link_row->product_id."'");
                                                $prod_row = $prod_qry->row();

                                                $cat_id = $prod_row->cat_id;
                                                $sub_cat_id = $prod_row->sub_cat_id;
                                                $cart_vendor_id = $cart_value->vendor_id;

                                            $adminc_qry = $this->db->query("select * from admin_comissions where shop_id='".$cart_vendor_id."' and cat_id='".$cat_id."' and find_in_set('".$sub_cat_id."',subcategory_ids)");
                                            $adminc_row = $adminc_qry->row();
                                                    $cat_qry = $this->db->query("select * from categories where id='".$cat_id."'");
                                                    $cat_row = $cat_qry->row();

                                                    $scat_qry = $this->db->query("select * from sub_categories where id='".$sub_cat_id."'");
                                                    $scat_row = $scat_qry->row();


                                                    if($adminc_row->admin_comission!='')
                                                    {
                                                        $admin_comission=$adminc_row->admin_comission;
                                                    }
                                                    else
                                                    {
                                                        $admin_comission=0;
                                                    }


                                                    $percentage = ($cart_value->unit_price/100)*$admin_comission; 
                                                    $admin_total = $percentage+$admin_total;
                                                    $unit_price=$cart_value->unit_price+$unit_price;
                                         }

                                         // print_r($value->deliveryboy_commission);
                                         //$unit_price = $unit_price; //addded
                                         //$totala = $unit_price+$value->deliveryboy_commission;
                                         //echo $value->coupon_id; die;
                                        if($value->coupon_id==0)
                                        {
                                            $coupon_disount="0";
                                            $sub_t = $unit_price; //added +$value->deliveryboy_commission
                                            $totala=$sub_t;
                                        }
                                        else
                                        {
                                            $cash_coponqry = $this->db->query("select * from cash_coupons where coupon_code='".$value->coupon_code."'");
                                            if($cash_coponqry->num_rows()>0)
                                            {
                                                 $coupon_disount="0";
                                                 $sub_t = $unit_price;
                                                 $totala=$sub_t;
                                            }
                                            else
                                            {
                                                $coponqry = $this->db->query("select * from coupon_codes where id='".$value->coupon_id."'");
                                                $couonrow = $coponqry->row();
                                                if($couonrow->shop_id==0)
                                                {
                                                    $coupon_disount=0;
                                                    $sub_t = $unit_price-$coupon_disount; //added +$value->deliveryboy_commission
                                                    $totala=$sub_t;
                                                }
                                                else
                                                {
                                                    $coupon_disount=$value->coupon_disount;
                                                    $sub_t = $unit_price-$coupon_disount; //added +$value->deliveryboy_commission
                                                    $totala=$sub_t;
                                                }
                                            }
                                        }
                                            
                            if($value->bid_id==0)
                            { 
                                 $vendor_amount1 = $totala-$admin_total;
                                 $vendor_amount += $vendor_amount1; 
                             }
                             else
                             {
                                        $vendor_id = $value->vendor_id;
                                                    $ven_bids = $this->db->query("select * from vendor_bids where bid_id='".$value->bid_id."' and vendor_id='".$vendor_id."'");
                                                    $vendor_bid_row = $ven_bids->row();

                                                    $bid_percentage = ($value->total_price/100)*$vendor_bid_row->admin_commission;

                                                    $vendor_amount +=$value->total_price-$bid_percentage;
                             }
                //$vendor_amount += $value->vendor_commission;
        }
        //echo $vendor_amount; die;

        /*start cart data */
        									
        /*close cart data */
        $chk_vend = $this->db->query("select * from vendor_payements where vendor_id='".$vendor_id."'");
        if($chk_vend->num_rows()>0)
        {
                $array = array('vendor_id'=>$vendor_id,'total_payment'=>$vendor_amount);
                $where = array('vendor_id'=>$vendor_id);
                $this->db->update("vendor_payements",$array,$where);
                //echo $this->db->last_query(); die;
        }
        else
        {
                $array = array('vendor_id'=>$vendor_id,'total_payment'=>$vendor_amount);
                $this->db->insert("vendor_payements",$array);
        }
        $vendor_row = $chk_vend->row();


        $this->data['vendor_amount']=$vendor_row->total_payment-$vendor_row->requested_amount;

        $vendor_req = $this->db->query("select * from request_payment where vendor_id='".$vendor_id."'");
        $this->data['vendor_requests']=$vendor_req->result();
        $this->load->view('vendors/includes/header', $this->data);
        $this->load->view('vendors/request_payment', $data);
        $this->load->view('vendors/includes/footer');
    }



    function add() {
        $this->data['page_name'] = 'request_payment';
        $this->data['title'] = 'Request Payment';
        //echo "select * from vendor_payements where vendor_id='".$_SESSION['vendors']['vendor_id']."'"; die;
        $chk_vend = $this->db->query("select * from vendor_payements where vendor_id='".$_SESSION['vendors']['vendor_id']."'");
        $vendor_row = $chk_vend->row();
        $this->data['vendor_amount']=$vendor_row->total_payment-$vendor_row->requested_amount;

        $this->load->view('vendors/includes/header', $this->data);

        $this->load->view('vendors/addpayment', $this->data);

        $this->load->view('vendors/includes/footer');

    }



    function insert() 
    {

        //print_r($this->input->post()); die; 
        $qry = $this->db->query("select * from vendor_payements where vendor_id='".$_SESSION['vendors']['vendor_id']."'");
        $row =$qry->row();
         $t_price =  floor($row->total_payment);
        
        $vendor_amount = $this->input->post('vendor_amount'); 

        $requested_amount = $this->input->post('requested_amount'); 
        if($requested_amount>0)
        {
          
          $chek_qry = $this->db->query("SELECT sum(request_amount) as total_requested_amount FROM `request_payment` WHERE vendor_id='".$_SESSION['vendors']['vendor_id']."'");
          $requqested_row=$chek_qry->row();
          $total_requested_amount=$requqested_row->total_requested_amount+$requested_amount;
          

        if($t_price>=$total_requested_amount)
        {
                    $requested_amount = $this->input->post('requested_amount');
                    $description = $this->input->post('description');

                    $data = array(
                        'vendor_id'=>$_SESSION['vendors']['vendor_id'],
                        'request_amount' => $requested_amount,
                        'vendor_amount' => $vendor_amount,
                        'description'=> $description,
                        'created_at' => time()
                    );

                    $insert_query = $this->db->insert('request_payment', $data);
                    //echo $this->db->last_query(); die;
                    if ($insert_query) {
                            $qrr = $this->db->query("select * from vendor_shop where id='".$_SESSION['vendors']['vendor_id']."'");
                            $vend_row = $qrr->row();

                            $msg = $vend_row->shop_name." Requested the amount ".$requested_amount;
                        $trans_ar=array('sender_name'=>$vend_row->shop_name,'receiver_name'=>'Admin','amount'=>$requested_amount,'message'=>$msg,'created_at'=>time());
                        $this->db->insert('transactions', $trans_ar);
                        $this->session->set_flashdata('success_message', 'Request sent Successfully');
                        redirect('vendors/request_payment');
                        die();
                    } else {
                        redirect('vendors/request_payment/add');
                        die();
                    }

        }
        else
        {
            $this->session->set_flashdata('error_message', 'Please check your Total Amount and Previous requested Amount');
            redirect('vendors/request_payment/add');
        }
    }
    else
    {
         $this->session->set_flashdata('error_message', 'Please Enter minimum amount');
            redirect('vendors/request_payment/add');
    }
    }


    function delete($id) {

        $this->db->where('id', $id);
        if ($this->db->delete('request_payment')) {
                $this->session->set_flashdata('success_message', 'Requested Payment Deleted Successfully');
                redirect('vendors/request_payment');
         } 
         else 
         {
                $this->session->set_flashdata('error_message', 'Requested Payment Deleted Successfully');
                redirect('vendors/request_payment');
         }

    }



}

