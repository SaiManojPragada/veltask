<?php

class Services_change_password extends MY_Controller {

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('vendors')['vendors_logged_in'] != true ||
                $this->session->userdata('vendors')['vendor_type'] !== 'services') {
            redirect('vendors/login');
        }
    }

    function index() {
        $this->data['page_name'] = 'change_password';
        if (isset($_POST['update_pass'])) {
            $session_data = $this->session->userdata('vendors');
            $this->db->where("id", $session_data['vendor_id']);
            $this->db->where("password", md5($this->input->post("current_password")));
            $check_password = $this->db->get("service_providers")->row();
            if (!empty($check_password)) {
                $this->db->set(array("password" => md5($this->input->post("password"))));
                $this->db->where("id", $session_data['vendor_id']);
                $update = $this->db->update("service_providers");
                if ($update) {
                    $this->session->set_flashdata('success_message', 'Password Successfully.');
                } else {
                    $this->session->set_flashdata('error_message', 'Failed to Update Password. ');
                }
            } else {
                $this->session->set_flashdata('error_message', 'Invalid Current Password.');
            }
            unset($_POST);
            redirect('vendors/services_change_password');
        }
        $this->load->view('vendors/includes/header', $this->data);
        $this->load->view('vendors/services_change_password', $this->data);
        $this->load->view('vendors/includes/footer');
    }

}
