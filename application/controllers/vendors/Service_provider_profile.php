<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Service_provider_profile
 *
 * @author Admin
 */
class Service_provider_profile extends MY_Controller {

    //put your code here

    public $data;

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('vendors')['vendors_logged_in'] != true) {
            redirect('vendors/login');
        }
        $this->load->model('vendor_services');
    }

    function index() {
        $this->data['page_name'] = 'service_provider_profile';
        $vendor_data = $this->session->userdata('vendors');
        $user_id = $vendor_data['vendor_id'];
        $this->data['vendor_id'] = $user_id;
        $this->data['data'] = $this->my_model->get_data_row("service_providers", array("id" => $user_id));
        $this->data['states'] = $this->db->get("states")->result();
        $this->db->where("state_id", $this->data['data']->state_id);
        $this->data['cities'] = $this->db->get('cities')->result();
        $this->db->where("city_id", $this->data['data']->location_id);
        $this->data['pincodes'] = $this->db->get('pincodes')->result();
        $this->data['categories'] = $this->db->get("services_categories")->result();
        $this->load->view('vendors/includes/header', $this->data);
        $this->load->view('vendors/service_provider_profile', $this->data);
        $this->load->view('vendors/includes/footer');
    }

    function update($id) {
        $_POST['updated_at'] = time();
        $_POST['categories_ids'] = implode(',', $_POST['categories_ids']) . ',';
        if (empty($this->input->post("password"))) {
            unset($_POST['password']);
        } else {
            $_POST['password'] = md5($_POST['password']);
        }

        $_POST["photo"] = $this->image_upload("photo", "service_providers");
        $_POST["aadhar_photo"] = $this->image_upload("aadhar_photo", "service_providers");
        $_POST["pcc_photo"] = $this->image_upload("pcc_photo", "service_providers");
        $_POST["vaccination_certificate"] = $this->image_upload("vaccination_certificate", "service_providers");
        if (empty($_POST["photo"])) {
            unset($_POST["photo"]);
        }
        if (empty($_POST["aadhar_photo"])) {
            unset($_POST["aadhar_photo"]);
        }
        if (empty($_POST["pcc_photo"])) {
            unset($_POST["pcc_photo"]);
        }
        if (empty($_POST["vaccination_certificate"])) {
            unset($_POST["vaccination_certificate"]);
        }
        $this->db->where("id !=", $id);
        $check = $this->db->where("phone", $this->input->post("phone"))->get("service_providers")->result();
        $this->db->where("id !=", $id);
        $check_email = $this->db->where("email", $this->input->post('email'))->get("service_providers")->result();
        if (sizeof($check_email) > 0) {
            $this->session->set_flashdata('error_message', "Account with this Email Already Exists");
            redirect('vendors/service_provider_profile');
            die;
        } else if (sizeof($check) > 0) {
            $this->session->set_flashdata('error_message', "Account with this Phone Number Already Exists");
            redirect('vendors/service_provider_profile');
            die;
        } else {
            $update = $this->my_model->update_data("service_providers", array("id" => $id), $_POST);
            if ($update) {
                $this->session->set_flashdata('success_message', 'Profile Updated Successfully.');
                redirect(base_url('vendors/service_provider_profile'));
            } else {
                $this->session->set_flashdata('error_message', 'Failed to Update Profile.');
                redirect(base_url('vendors/service_provider_profile'));
            }
        }
    }

}
