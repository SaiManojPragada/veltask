<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('vendors')['vendors_logged_in'] != true) {
            redirect('vendors/login');
        }
        //$this->load->model("vendor_model");
        $this->load->model("admin_model");
        $this->load->model('vendor_services');
    }

    function index() {
        if ($this->session->userdata('vendors')['vendor_type'] === 'ecomm') {
            $this->data['page_name'] = 'dashboard';
            $this->data['title'] = 'Dashboard';

            $shop_id = $_SESSION['vendors']['vendor_id'];
            $pro = $this->db->query("select * from products where shop_id='" . $shop_id . "' and delete_status=0");
            $this->data['total_products'] = $pro->num_rows();

            $ord = $this->db->query("select * from orders where vendor_id='" . $shop_id . "'");
            $this->data['total_orders'] = $ord->num_rows();

            $qry = $this->db->query("select * from admin_comissions where shop_id='" . $shop_id . "'");
            $category = $qry->num_rows();

            $ord = $this->db->query("select * from orders where vendor_id='" . $shop_id . "'");
            $total_orders = $ord->num_rows();

            $date = date('Y-m-d');

            $toord = $this->db->query("select * from orders where vendor_id='" . $shop_id . "' and created_date LIKE '%" . $date . "%'");
            $today_orders = $toord->num_rows();

            $pay = $this->db->query("select * from vendor_payements where vendor_id='" . $shop_id . "'");
            $payment = $pay->row();

            $rp = $this->db->query("select SUM(request_amount) as paid_amount from request_payment where vendor_id='" . $shop_id . "' and status=1");
            $rp_payment = $rp->row();

            $prod = $this->db->query("select * from products where shop_id='" . $shop_id . "' and delete_status=0");
            $products = $prod->num_rows();

            $pending = $payment->total_payment - $payment->requested_amount;

            $this->data['total_category'] = $category;
            $this->data['today_orders'] = $today_orders;

            $this->data['total_payment'] = $payment->total_payment;
            $this->data['pending'] = $pending;

            $bidqry = $this->db->query("SELECT user_bids.id FROM user_bids INNER JOIN vendor_bids ON vendor_bids.bid_id=user_bids.id where vendor_bids.vendor_id='" . $shop_id . "' and user_bids.bid_status in (0,1)");
            $this->data['bid_count'] = $bidqry->num_rows();
        } else {
            $vendor_data = $this->session->userdata('vendors');
            $user_id = $vendor_data['vendor_id'];
            $user_data = $this->check_service_provider($user_id);
            $categories_ids = rtrim($user_data->categories_ids, ',');
            $today = date('Y-m-d');
//            $this->db->order_by("id", "desc");
//            $this->db->where("time_slot_date", $today);
            $orders = $this->vendor_services->get_services('order_placed', $user_id, $categories_ids);
            if ($orders) {
                $result = array();
                //print_r($data); die;
                $order_selected_id = array();
                foreach ($orders as $od) {
                    $od->customer_details = $this->my_model->get_data_row("users", array("id" => $od->user_id));
                    $od->customer_address = $this->my_model->get_data_row("user_address", array("id" => $od->user_addresses_id));
                    $od->created_at = ( $od->created_at) ? date('d M Y, h:i A', $od->created_at) : '';
                    $od->category_name = $this->my_model->get_data_row("services_categories", array("id" => $od->ordered_categories))->name;
                    $order_items = $this->my_model->get_data("service_orders_items", array("order_id" => $od->order_id));

                    foreach ($order_items as $item) {
                        $item->service_details = $this->my_model->get_data_row("services", array("id" => $item->service_id));
                    }
                    $od->order_items = $order_items;
                    $user_pincode = $this->my_model->get_data_row("user_address", array("id" => $od->user_addresses_id))->pincode;
                    $francise_qry = $this->db->query("select * from service_providers where franchise_id='" . $user_data->franchise_id . "' and id = '$user_id'");
                    $francise_result = $francise_qry->result();
                    foreach ($francise_result as $frncise_value) {
                        $check_for_admin_comission = $this->my_model->get_data_row("provider_comissions",
                                array("providers_id" => $frncise_value->id, "cat_id" => $od->ordered_categories));
                        if (!empty($check_for_admin_comission)) {

                            if ($frncise_value->sp_pincodes != '') {
                                $franchise_pincodes = explode(",", $frncise_value->sp_pincodes);

                                if (in_array($user_pincode, $franchise_pincodes) && !in_array($od->id, $order_selected_id)) {
                                    array_push($order_selected_id, $od->id);
                                    array_push($result, $od);
                                }
                            }
                        }
                    }
                }
            }

            $this->data['today_orders'] = $result;
//            $this->db->where("ordered_categories IN (" . $categories_ids . ")");
            $where = array("accepted_by_service_provider" => $user_id);
            $orders = $this->my_model->get_data("services_orders", $where);
            $this->data['orders'] = $orders;

            $this->db->where("order_status", "order_accepted");
            $this->db->where("time_slot_date > '" . $today . "'");
            $orders = $this->my_model->get_data('services_orders');
            $this->data['upcoming_orders'] = $orders;
        }

        $this->load->view('vendors/includes/header', $this->data);
        $this->load->view('vendors/home', $this->data);
        $this->load->view('vendors/includes/footer');
    }

}
