<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    private $data;

    function __construct() {
        parent::__construct();
        $this->load->model("vendor_model");
    }

    public function index() {
//        $this->data['username'] = 'admin';
//        $this->data['password'] = 'Wido@5454';
        $this->load->view('vendors/login', $this->data);
    }

    public function admin_login() {

        $ip = $_SERVER['REMOTE_ADDR'];
        $username = $this->input->get_post('email', TRUE);

        if ($username == '' || $this->input->get_post('password') == '') {
            $this->session->set_flashdata('error_message', 'Enter Mobile Number & Password');
            redirect('vendors/login');
        } else {

            if ($this->input->post('md5') == 1) {
                $password = $this->input->get_post('password', TRUE);
            } else {
                $password = $this->input->get_post('password', TRUE);
                $password = md5($password);
            }
            if ($this->input->get_post('vendor_type') === "services") {

                $row = $this->vendor_model->login_service_admin($username, $password);
                if ($row != "") {
                    $sess_arr = array(
                        'vendor_type' => 'services',
                        'vendor_id' => $row->id,
                        'owner_name' => $row->name,
                        'provider_type' => $row->type,
                        'vendors_logged_in' => true
                    );
                    $this->session->set_flashdata('msg', 'Welcome');
                    $this->session->set_userdata('vendors', $sess_arr);
                    redirect('vendors/dashboard');
                } else {
                    $this->session->set_flashdata('error_message', 'Invalid Mobile Number Or Password');
                    redirect('vendors/login');
                }
            } else if ($this->input->get_post('vendor_type') === "ecomm") {
                $row = $this->vendor_model->admin_login($username, $password);
                if ($row != "") {
                    $sess_arr = array(
                        'vendor_type' => 'ecomm',
                        'vendor_id' => $row->id,
                        'owner_name' => $row->owner_name,
                        'vendors_logged_in' => true
                    );
                    $this->session->set_flashdata('msg', 'Welcome');
                    //$this->session->set_userdata($sess_arr);
                    $this->session->set_userdata('vendors', $sess_arr);
                    //                $this->session->set_flashdata('login_id', $row->id);
                    redirect('vendors/dashboard');
                } else {
                    $this->session->set_flashdata('error_message', 'Invalid Mobile Number Or Password');
                    redirect('vendors/login');
                }
            }
        }
    }


     public function vendor_login() {

           
         $ip = $_SERVER['REMOTE_ADDR'];

//        $ipcheck = $this->vendor_model->ip_checking($ip);
        //log_message('error', json_encode($ipcheck));

//        if ($ipcheck != '') {
            $username = $this->input->get_post('email', TRUE);

            if($username=='' || $this->input->get_post('password')=='')
            {
                $this->session->set_flashdata('error_message', 'Enter Mobile Number & Password');
                redirect('vendors/login');
            }
            else
            {

                    if($this->input->post('md5')==1)
                    {
                        $password =$this->input->get_post('password', TRUE);
                    }
                    else
                    {
                        $password = $this->input->get_post('password', TRUE);
                        $password = md5($password);
                    }
                    $row = $this->vendor_model->vendors_login($username, $password);  
                    if ($row != ""){
                        $sess_arr = array(
                            'vendor_id' => $row->id,
                            'owner_name' => $row->owner_name,
                            'vendors_logged_in' => true,
                            'vendor_type'=>'ecomm'
                        ); 
                        $this->session->set_flashdata('msg', 'Welcome');
                        //$this->session->set_userdata($sess_arr);
                        $this->session->set_userdata('vendors', $sess_arr);
        //                $this->session->set_flashdata('login_id', $row->id);
                        redirect('vendors/dashboard');
                    } else {
                        $this->session->set_flashdata('error_message', 'Invalid Mobile Number Or Password');
                        redirect('vendors/login');
                    }

                }
//        } else {
//            $this->session->set_flashdata('error', 'Access Denied');
//            redirect('vendors/login');
//        }
    
     }

}
