<?php

class Manage_technicians extends MY_Controller {

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('vendors')['vendors_logged_in'] != true ||
                $this->session->userdata('vendors')['vendor_type'] !== 'services' ||
                $this->session->userdata('vendors')['provider_type'] !== 'Owner') {
            redirect('vendors/login');
        }
    }

    function index() {
        $vendor_id = $this->session->userdata('vendors')['vendor_id'];
        $this->data['page_name'] = 'technicians';
        $this->data['title'] = 'Manage Technicians';
        $this->db->where("owner_id", $vendor_id);
        $this->db->order_by('id', 'desc');
        $this->data['technicians'] = $this->db->get('service_providers')->result();
        foreach ($this->data['technicians'] as $item) {
            $this->db->where("id", $item->location_id);
            $item->location_name = $this->db->get("cities")->row()->city_name;
            $categories = $this->db->where('id IN (' . rtrim($item->categories_ids, ',') . ')')->get('services_categories')->result();
            foreach ($categories as $cat) {
                $item->categories_names .= $cat->name . ', ';
            }
            $item->categories_names = rtrim($item->categories_names, ', ');
        }
        $this->load->view('vendors/includes/header', $this->data);
        $this->load->view('vendors/technicians', $this->data);
        $this->load->view('vendors/includes/footer');
    }

    function add() {
        $vendor_id = $this->session->userdata('vendors')['vendor_id'];
        $this->data['page_name'] = 'technicians';
        $this->data['func'] = "insert";
        $this->data['title'] = 'Add Technician';
        $this->data['data'] = array();
        $this->data['cities'] = $this->db->get("cities")->result();
        $this->data['details'] = $this->my_model->get_data_row("service_providers", array("id" => $vendor_id));
        $this->data['categories'] = $this->db->where('id IN (' . rtrim($this->data['details']->categories_ids, ',') . ')')->get('services_categories')->result();
        $this->load->view('vendors/includes/header', $this->data);
        $this->load->view('vendors/add_technicians', $this->data);
        $this->load->view('vendors/includes/footer');
    }

    function edit($id) {
        $vendor_id = $this->session->userdata('vendors')['vendor_id'];
        $this->data['page_name'] = 'technicians';
        $this->data['func'] = "update/";
        $this->data['title'] = 'Update Technician';
        $this->data['cities'] = $this->db->get("cities")->result();
        $this->data['data'] = $this->my_model->get_data_row("service_providers", array("id" => $id, 'owner_id' => $vendor_id));
        $this->data['details'] = $this->my_model->get_data_row("service_providers", array("id" => $vendor_id));
        $this->data['categories'] = $this->db->where('id IN (' . rtrim($this->data['details']->categories_ids, ',') . ')')->get('services_categories')->result();
        $this->load->view('vendors/includes/header', $this->data);
        $this->load->view('vendors/add_technicians', $this->data);
        $this->load->view('vendors/includes/footer');
    }

    function insert() {
        $vendor_id = $this->session->userdata('vendors')['vendor_id'];
        $_POST['categories_ids'] = implode(',', $_POST['categories_ids']) . ',';
        $_POST['created_at'] = time();
        $_POST['updated_at'] = time();
        $_POST['owner_id'] = $vendor_id;
        $_POST['type'] = 'Owner + Technician';
        $_POST['password'] = md5($_POST['password']);
        $insert = $this->my_model->insert_data("service_providers", $_POST);
        if ($insert) {
            $this->session->set_flashdata('success_message', "Technician Added Succesfully");
            redirect('vendors/manage_technicians');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('vendors/manage_technicians');
        }
    }

    function update($id) {
        $_POST['updated_at'] = time();
        $_POST['categories_ids'] = implode(',', $_POST['categories_ids']) . ',';
        if (empty($this->input->post("password"))) {
            unset($_POST['password']);
        } else {
            $_POST['password'] = md5($_POST['password']);
        }
        $update = $this->my_model->update_data("service_providers", array("id" => $id), $_POST);
        if ($update) {
            $this->session->set_flashdata('success_message', 'Technician Updated Successfully.');
            redirect('vendors/manage_technicians');
        } else {
            $this->session->set_flashdata('error_message', 'Failed to Update Service Provider.');
            redirect('vendors/manage_technicians');
        }
    }

    function delete($id) {
        $delete = $this->my_model->delete_data("service_providers", array("id" => $id));
        if ($delete) {
            $this->session->set_flashdata('success_message', "Technician Deleted Succesfully");
            redirect('vendors/manage_technicians');
        } else {
            $this->session->set_flashdata('error_message', "Something Went Wrong Please try Again");
            redirect('vendors/manage_technicians');
        }
    }

}
