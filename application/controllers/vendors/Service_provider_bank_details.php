<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Service_provider_bank_details extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('vendors')['vendors_logged_in'] != true) {
            redirect('vendors/login');
        }
        $this->load->model('vendor_services');
    }

    function index() {
        $this->data['page_name'] = 'service_provider_bank_details';
        $vendor_data = $this->session->userdata('vendors');
        $user_id = $vendor_data['vendor_id'];
        $this->data['vendor_id'] = $user_id;
        $this->data['data'] = $this->my_model->get_data_row("service_provider_bank_details", array("service_provider_id" => $user_id));
        $this->load->view('vendors/includes/header', $this->data);
        $this->load->view('vendors/service_provider_bank_details', $this->data);
        $this->load->view('vendors/includes/footer');
    }

    function update($user_id) {
        $input_data = array(
            "service_provider_id" => $user_id,
            "bank_name" => $this->input->post('bank_name'),
            "account_number" => $this->input->post('account_number'),
            "ifsc_code" => $this->input->post('ifsc_code'),
            "branch_name" => $this->input->post('branch_name'),
            "created_at" => time()
        );
        $bank_image = $this->image_upload('bank_image', 'service_providers');
        if(!empty($bank_image)){
            $input_data['bank_image'] = $bank_image;
        }
        $this->my_model->delete_data("service_provider_bank_details", array("service_provider_id" => $user_id));
        $ins = $this->my_model->insert_data("service_provider_bank_details", $input_data);
        if ($ins) {
            $this->session->set_flashdata('success_message', 'Bank Details Updated Successfully.');
            redirect(base_url('vendors/service_provider_bank_details'));
        } else {
            $this->session->set_flashdata('error_message', 'Failed to Update Bank Details.');
            redirect(base_url('vendors/service_provider_bank_details'));
        }
    }

}
