<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Service_provider_payouts extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('vendors')['vendors_logged_in'] != true) {
            redirect('vendors/login');
        }
        $this->load->model('vendor_services');
    }

    function index() {
        $this->data['page_name'] = 'service_provider_payouts';
        $vendor_data = $this->session->userdata('vendors');
        $user_id = $vendor_data['vendor_id'];

        $wallet = $this->my_model->get_data_row("service_providers_payments", array("provider_id" => $user_id));
        $total_earnings = $wallet->total_amount;
        $total_withdrawls = $wallet->total_requested_amount;
        $this->db->select("SUM(requested_amount) as total_onhold_amount");
        $onhold_amount = $this->my_model->get_data_row("service_provider_withdraw_request", array("provider_id" => $user_id, "status" => 0))->total_onhold_amount;
        $current_balance = ($total_earnings - $total_withdrawls) - $onhold_amount;
        $this->data['onhold_amount'] = $onhold_amount;
        $this->data['current_balance'] = $current_balance;
        $this->data['total_withdrawls'] = $total_withdrawls;
        $this->data['total_earnings'] = $total_earnings;
        $this->data['vendor_id'] = $user_id;
        $this->db->order_by("id", "desc");
        $this->data['data'] = $this->my_model->get_data("service_provider_withdraw_request", array("provider_id" => $user_id));
        $this->load->view('vendors/includes/header', $this->data);
        $this->load->view('vendors/service_provider_payouts', $this->data);
        $this->load->view('vendors/includes/footer');
    }

    function request() {
        $vendor_data = $this->session->userdata('vendors');
        $user_id = $vendor_data['vendor_id'];
        $check_bank = $this->my_model->get_data_row("service_provider_bank_details", array("service_provider_id" => $user_id));
        if (empty($check_bank)) {
            $this->session->set_flashdata('error_message', 'No Bank Details Found.');
            redirect(base_url('vendors/service_provider_payouts'));
            die;
        }
        $requested_amount = $this->input->post("requested_amount");
        $description = $this->input->post("description");
        $wallet = $this->my_model->get_data_row("service_providers_payments", array("provider_id" => $user_id));
        $total_earnings = $wallet->total_amount;
        $total_withdrawls = $wallet->total_requested_amount;
        $this->db->select("SUM(requested_amount) as total_onhold_amount");
        $onhold_amount = $this->my_model->get_data_row("service_provider_withdraw_request", array("provider_id" => $user_id, "status" => 0))->total_onhold_amount;
        $current_balance = ($total_earnings - $total_withdrawls) - $onhold_amount;
        if ($requested_amount > $current_balance) {
            $this->session->set_flashdata('error_message', 'Insufficient Balance.');
            redirect(base_url('vendors/service_provider_payouts'));
            die;
        }
        $inp_data = array(
            "provider_id" => $user_id,
            "requested_amount" => $requested_amount,
            "description" => $description,
            "created_at" => time(),
            "status" => 0
        );
        $insert = $this->my_model->insert_data("service_provider_withdraw_request", $inp_data);
        if ($insert) {
            $this->session->set_flashdata('success_message', 'Withdrawl request succesfully sent.');
            redirect(base_url('vendors/service_provider_payouts'));
            die;
        } else {
            $this->session->set_flashdata('error_message', 'Unable to send Withdrawl request.');
            redirect(base_url('vendors/service_provider_payouts'));
            die;
        }
    }

}
