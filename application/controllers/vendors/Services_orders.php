<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Services_orders extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('vendors')['vendors_logged_in'] != true) {
            //$this->session->set_flashdata('error', 'Session Timed Out');
            redirect('vendors/login');
        }
        $this->load->model('vendor_services');
    }

    function index() {
        $this->data['page_name'] = 'services_orders';
        $vendor_data = $this->session->userdata('vendors');
        $user_id = $vendor_data['vendor_id'];
        $this->data['vendor_id'] = $user_id;
        $user_data = $this->check_service_provider($user_id);
        $categories_ids = rtrim($user_data->categories_ids, ',');
        $franchise_pincodes = explode(',', $this->my_model->get_data_row("franchises", array("id" => $user_data->franchise_id))->pincode_ids);

        $flt = $this->input->get("ft");
        if ($flt == "accepted") {
            $this->db->where("ordered_categories IN (" . $categories_ids . ")");
            $this->db->order_by("id", "desc");
            $this->db->where('order_status="order_accepted"');
            $where = array("accepted_by_service_provider" => $user_id);
            $orders = $this->my_model->get_data("services_orders", $where);
            foreach ($orders as $item) {
                $item->ordered_on = date('d M Y, h:i A', $item->created_at);
                unset($item->created_at);
            }
        } else if ($flt == "started") {
            $this->db->where("ordered_categories IN (" . $categories_ids . ")");
            $this->db->order_by("id", "desc");
            $where = array('order_status' => 'order_started', "accepted_by_service_provider" => $user_id);
            $orders = $this->my_model->get_data("services_orders", $where);
            foreach ($orders as $item) {
                $item->ordered_on = date('d M Y, h:i A', $item->created_at);
                unset($item->created_at);
            }
        } else if ($flt == "completed") {
            $this->db->where("ordered_categories IN (" . $categories_ids . ")");
            $this->db->order_by("id", "desc");
            $where = array('order_status' => 'order_completed', "accepted_by_service_provider" => $user_id);
            $orders = $this->my_model->get_data("services_orders", $where);
            foreach ($orders as $item) {
                $item->ordered_on = date('d M Y, h:i A', $item->created_at);
                unset($item->created_at);
            }
        } else {
            $dd = $this->vendor_services->get_services('order_placed', $user_id, $categories_ids);
            $orders = array();
            foreach ($dd as $d) {
                $user_pincode = $this->my_model->get_data_row("user_address", array("id" => $d->user_addresses_id))->pincode;
                if (in_array($user_pincode, $franchise_pincodes)) {
                    array_push($orders, $d);
                }
            }
        }
        $result = array();
        //print_r($data); die;
        $order_selected_id = array();
        foreach ($orders as $item) {
            $item->has_quotation = (!empty($this->my_model->get_data("visit_and_quote_quotaions", array("service_order_id" => $item->id)))) ? 1 : 0;
            $item->user_data = $this->my_model->get_data_row("users", array("id" => $item->user_id));
            $item->payment_status = ($item->balance_amount < 1) ? 'Paid' : 'Unpaid';
            $item->customer_details = $this->my_model->get_data_row("users", array("id" => $item->user_id));
            $user_address = $this->my_model->get_data_row("user_address", array("id" => $item->user_addresses_id));
            $user_address->state_name = $this->my_model->get_data_row("states", array("id" => $user_address->state))->state_name;
            $user_address->city_name = $this->my_model->get_data_row("cities", array("id" => $user_address->city))->city_name;
            $user_address->pincode_pin = $this->my_model->get_data_row("pincodes", array("id" => $user_address->pincode))->pincode;
            $item->address = $user_address;
            $item->payment_type = $this->my_model->get_data_row("payment_methods_enables", array("id" => $item->payment_type))->payment_method_title;
            $francise_qry = $this->db->query("select * from service_providers where franchise_id='" . $user_data->franchise_id . "' and id = '$user_id'");
            $francise_result = $francise_qry->result();
            if ($item->order_status == "order_placed") {
                foreach ($francise_result as $frncise_value) {
                    $check_for_admin_comission = $this->my_model->get_data_row("provider_comissions",
                            array("providers_id" => $frncise_value->id, "cat_id" => $item->ordered_categories));
                    if (!empty($check_for_admin_comission)) {
                        if ($frncise_value->sp_pincodes != '') {
                            $franchise_pincodes = explode(",", $frncise_value->sp_pincodes);
                            if (in_array($user_pincode, $franchise_pincodes) && !in_array($item->id, $order_selected_id)) {
                                array_push($order_selected_id, $item->id);
                                array_push($result, $item);
                            }
                        }
                    }
                }
            } else {
                $result = $orders;
            }
        }
        $this->data['orders'] = $result;

        $this->load->view('vendors/includes/header', $this->data);
        $this->load->view('vendors/services_orders', $this->data);
        $this->load->view('vendors/includes/footer');
    }

    function orderDetails($order_id) {
        $where = array('id' => $order_id);
        $vendor_data = $this->session->userdata('vendors');
        $user_id = $vendor_data['vendor_id'];
        $this->data['vendor_id'] = $user_id;
        $this->data['order'] = $this->my_model->get_data_row("services_orders", $where);
        $this->data['order']->payment_status = ( $this->data['order']->balance_amount < 1) ? 'Paid' : 'Unpaid';
        $this->data['order']->customer_details = $this->my_model->get_data_row("users", array("id" => $this->data['order']->user_id));
        $this->data['order']->address = $this->my_model->get_data_row("users_addresses", array("id" => $this->data['order']->user_addresses_id));
        $this->data['order']->payment_type = $this->my_model->get_data_row("payment_methods_enables", array("id" => $this->data['order']->payment_type))->payment_method_title;
        $this->data['order']->payment_status = ($this->data['order']->balance_amount < 1) ? 'Paid' : 'Unpaid';
        $user_address = $this->my_model->get_data_row("user_address", array("id" => $this->data['order']->user_addresses_id));
        $user_address->state_name = $this->my_model->get_data_row("states", array("id" => $user_address->state))->state_name;
        $user_address->city_name = $this->my_model->get_data_row("cities", array("id" => $user_address->city))->city_name;
        $user_address->pincode_pin = $this->my_model->get_data_row("pincodes", array("id" => $user_address->pincode))->pincode;
        $this->data['order']->address = $user_address;
        $order_items = $this->my_model->get_data("service_orders_items", array("order_id" => $this->data['order']->order_id));
        $this->data['quotation'] = $this->my_model->get_data_row("visit_and_quote_quotaions", array("service_order_id" => $this->data['order']->id));
        foreach ($order_items as $item) {
            $item->service_name = $this->my_model->get_data_row("services", array("id" => $item->service_id))->service_name;
        }
        $this->data['order']->order_items = $order_items;

        $this->load->view('vendors/includes/header', $this->data);
        $this->load->view('vendors/service_order_details', $this->data);
        $this->load->view('vendors/includes/footer');
    }

    function accept($id) {
        $vendor_data = $this->session->userdata('vendors');
        $user_id = $vendor_data['vendor_id'];
        $where = array("id" => $id, 'order_status' => 'order_accepted');
        $check = $this->my_model->get_data_row('services_orders', $where);
        if (!empty($check)) {
            $this->session->set_flashdata('error_message', "This Order has been Already Accepted");
            redirect(base_url('vendors/services_orders'));
            die;
        }
        $order_data = $this->my_model->get_data_row("services_orders", array("id" => $id));

        $service_provider_data = $this->check_service_provider($user_id);
        $accepted_franchise_id = $service_provider_data->franchise_id;
        $update = $this->my_model->update_data("services_orders", array("id" => $id), array("order_status" => 'order_accepted', 'accepted_by_service_provider' => $user_id, 'accepted_franchise_id' => $accepted_franchise_id, 'accepted_at' => time()));
        if ($update) {
            $user_data = $this->my_model->get_data_row("users", array("id" => $order_data->user_id));
            $this->send_push_notification_model->send_push_notification('Order Accepted', 'Your Order #' . $order_data->order_id . ' has been Accepted by ' . $service_provider_data->name . '.', array($user_data->token), array(), base_url('uploads/services_categories/e9a64cbddd987aa5e9bf34e125297bef.jpg'));

            $notification_data = array(
                "user_id" => $user_data->id,
                "title" => 'Order Accepted (#' . $order_data->order_id . ')',
                "message" => 'Your Order #' . $order_data->order_id . ' has been Accepted by ' . $service_provider_data->name . '.',
                "created_at" => time()
            );
            $this->my_model->insert_data("user_notifications", $notification_data);
            $notification_data['order_type'] = "Service";
            $notification_data['order_id'] = $order_data->id;
            unset($notification_data['title']);
            unset($notification_data['created_at']);
            $this->my_model->insert_data("admin_notifications", $notification_data);

            $this->session->set_flashdata('success_message', "Order Has Been Accepted");
            redirect(base_url('vendors/services_orders?ft=accepted'));
        } else {
            $this->session->set_flashdata('error_message', "Failed to Accept the Order");
            redirect(base_url('vendors/services_orders'));
        }
        die;
    }

    function delivery($session_id) {
        $data['order_id'] = $session_id;
        $del = $this->db->query("select id,name from deliveryboy");
        $data['deliverypersons'] = $del->result();
        $this->load->view('vendors/includes/header', $data);
        $this->load->view('vendors/deliverypage', $data);
        $this->load->view('vendors/includes/footer');
    }

    function assignDeliveryBoy() {
        $ar = array('delivery_boy' => $this->input->get_post('db_id'), 'order_status' => '3');
        $wr = array('id' => $this->input->get_post('order_id'));
        $upd = $this->db->update("orders", $ar, $wr);
        if ($upd) {
            $ad_ar = array('status' => 1);
            $ad_wr = array('order_id' => $this->input->get_post('order_id'));
            $this->db->update("admin_notifications", $ad_ar, $ad_wr);

            $data['page_name'] = 'orders';
            $shop_id = $_SESSION['vendors']['vendor_id'];
            $qry = $this->db->query("select * from orders where vendor_id='" . $shop_id . "' order by id desc");
            $data['orders'] = $qry->result();

            $this->load->view('vendors/includes/header', $data);
            $this->load->view('vendors/orders', $this->data);
            $this->load->view('vendors/includes/footer');
        }
    }

    function onesignalnotification($user_id, $message, $title) {
        $qr = $this->db->query("select * from users where id='" . $user_id . "'");
        $res = $qr->row();
        if ($res->token != '') {

            $user_id = $res->token;

            $fields = array(
                'app_id' => 'e072cc7b-595d-4c4c-a451-b07832b073f9',
                'include_player_ids' => [$user_id],
                'contents' => array("en" => $message),
                'headings' => array("en" => $title),
                'android_channel_id' => 'ea6c19aa-e55f-4243-af28-605a32901234'
            );

            $fields = json_encode($fields);
            //print("\nJSON sent:\n");
            //print($fields);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json; charset=utf-8',
                'Authorization: Basic NzhjMmI5YjItZmViMy00YjNlLWFlMDItY2ZiZTI3OTY0YzYz'
            ));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

            $response = curl_exec($ch);
            curl_close($ch);
            //print_r($response); die;
        }
    }

    function send_message($message = "", $mobile_number, $template_id) {


        $message = urlencode($message);

        $URL = "http://login.smsmoon.com/API/sms.php"; // connecting url 

        $post_fields = ['username' => 'Rocket', 'password' => 'vizag@123', 'from' => 'Rocket', 'to' => $mobile_number, 'msg' => $message, 'type' => 1, 'dnd_check' => 0, 'template_id' => $template_id];

        //file_get_contents("http://login.smsmoon.com/API/sms.php?username=colourmoonalerts&password=vizag@123&from=WEBSMS&to=$mobile_number&msg=$message&type=1&dnd_check=0");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $URL);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_exec($ch);
        return true;
    }

    function changeStatus() {
        $shop_id = $_SESSION['vendors']['vendor_id'];
        if ($this->input->post('value')) {
            $order_id = $this->input->post('order_id');

            $value = $this->input->post('value');
            if ($value == 2) {
                $ar = array('order_status' => $this->input->post('value'), 'accept_status' => 1);
            } else if ($value == 5) {
                $ar = array('order_status' => $this->input->post('value'), 'accept_status' => 1);
            } else {
                $ar = array('order_status' => $this->input->post('value'));
            }


            $wr = array('id' => $order_id);
            $upd = $this->db->update("orders", $ar, $wr);
            if ($upd) {
                $qry = $this->db->query("select * from orders where id='" . $order_id . "'");
                $row = $qry->row();
                $usr_qry = $this->db->query("select * from users where id='" . $row->user_id . "'");
                $usr_row = $usr_qry->row();

                $phone = $usr_row->phone;
                $name = $usr_row->first_name;
                if ($this->input->post('value') == 2) {
                    $msg = "Order Accepted by ";
                    $title = "Order Accepted";
                    $message = "Your Order : " . $row->id . " Accepted by Admin";
                    $this->onesignalnotification($row->user_id, $message, $title);

                    $msg = "Dear " . $name . " your order is " . $order_id . " is accepted by the vendor and it will be delivered with in 2-24hrs of time. Thank u for shopping with Rocket Wheel.";

                    $template_id = "1407161683232344120";
                    $this->send_message($msg, $phone, $template_id);
                } else if ($this->input->post('value') == 3) {
                    $msg = "Order Accepted by ";
                    $title = "Order Accepted";
                    $message = "Your Order : " . $row->id . " Accepted by Admin";
                    $this->onesignalnotification($row->user_id, $message, $title);

                    $msg = "Dear " . $name . " your order is " . $order_id . " is accepted by the vendor and it will be delivered with in 2-24hrs of time. Thank u for shopping with Rocket Wheel.";

                    $template_id = "1407161683232344120";
                    $this->send_message($msg, $phone, $template_id);
                } else if ($this->input->post('value') == 3) {
                    $msg = "Order Delivered By ";
                    $aar = array('vendor_id' => $shop_id, 'order_id' => $order_id, 'message' => $msg);
                    $this->db->insert("admin_notifications", $aar);

                    $qry = $this->db->query("select * from orders where id='" . $order_id . "'");
                    $row = $qry->row();

                    $user_id = $row->user_id;
                    $order_status = "Order Completed";
                    $this->db->insert("sms_notifications", array('order_id' => $order_id, 'receiver_id' => $user_id, 'sender_id' => $shop_id, 'created_at' => time(), 'message' => $msg, 'action_person' => 1, 'order_status' => $order_status));

                    $title = "Order Completed";
                    $message = "Your Order successfully Completed";
                    $this->onesignalnotification($user_id, $message, $title);
                } else if ($this->input->post('value') == 6) {
                    $msg = "Order Cancelled by";
                    $title = "Order Cancelled";
                    $message = "Your Order : " . $row->id . " Cancelled by Admin";
                    $this->onesignalnotification($row->user_id, $message, $title);
                }

                $aar = array('vendor_id' => $shop_id, 'order_id' => $order_id, 'message' => $msg);
                $this->db->insert("admin_notifications", $aar);

                echo "success";
                exit;
            }
        }
    }

}
