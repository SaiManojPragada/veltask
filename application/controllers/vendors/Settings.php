<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('vendors')['vendors_logged_in'] != true) {
            //$this->session->set_flashdata('error', 'Session Timed Out');
            redirect('vendors/login');
        }
    }

    function index() {
        $this->data['page_name'] = 'settings';
        $qry = $this->db->query("select * from vendor_shop where id='".$_SESSION['vendors']['vendor_id']."'");
        $data['settings'] = $qry->row();
        $this->load->view('vendors/includes/header', $this->data);
        $this->load->view('vendors/settings', $data);
        $this->load->view('vendors/includes/footer');
    }

    function updateVendor()
    {

        $shop_name = $this->input->post('shop_name');
        $owner_name = $this->input->post('owner_name');

        $mobile = $this->input->post('mobile');
        $address = $this->input->post('address');
        $city = $this->input->post('city');
        $pincode = $this->input->post('pincode');
         $image = $this->upload_file('webimage');
          $logo = $this->upload_file('logo');

          $delivery_time = $this->input->post('delivery_time');
          $min_order_amount = $this->input->post('min_order_amount');

        if($this->upload_file('webimage')!='' && $this->upload_file('logo')!='')
        {
           
            $ar=array(
                'shop_name'=>$this->input->post('shop_name'),
                'owner_name'=>$this->input->post('owner_name'),
                'mobile'=>$this->input->post('mobile'),
                'address'=>$this->input->post('address'),
                'city'=>$this->input->post('city'),
                'pincode'=>$this->input->post('pincode'),
                'lat'=>$this->input->post('lat'),
                'lng'=>$this->input->post('lng'),
                'status'=>$this->input->post('status'),
                //'delivery_time'=>$delivery_time,
                'min_order_amount'=>$min_order_amount,
                'shop_logo'=>$image,
                'logo'=>$logo
                );
        }
        else if($this->upload_file('webimage')=='' && $this->upload_file('logo')!='')
        {
           
            $ar=array(
                'shop_name'=>$this->input->post('shop_name'),
                'owner_name'=>$this->input->post('owner_name'),
                'mobile'=>$this->input->post('mobile'),
                'address'=>$this->input->post('address'),
                'city'=>$this->input->post('city'),
                'pincode'=>$this->input->post('pincode'),
                'lat'=>$this->input->post('lat'),
                'lng'=>$this->input->post('lng'),
                'status'=>$this->input->post('status'),
                'delivery_time'=>$delivery_time,
                'min_order_amount'=>$min_order_amount,
                'logo'=>$logo
                );
        }
        else if($this->upload_file('webimage')!='' && $this->upload_file('logo')=='')
        {
           
            $ar=array(
                'shop_name'=>$this->input->post('shop_name'),
                'owner_name'=>$this->input->post('owner_name'),
                'mobile'=>$this->input->post('mobile'),
                'address'=>$this->input->post('address'),
                'city'=>$this->input->post('city'),
                'pincode'=>$this->input->post('pincode'),
                'lat'=>$this->input->post('lat'),
                'lng'=>$this->input->post('lng'),
                'status'=>$this->input->post('status'),
                'delivery_time'=>$delivery_time,
                'min_order_amount'=>$min_order_amount,
                'shop_logo'=>$image
                );
        }
        else
        {
           $ar=array(
                'shop_name'=>$this->input->post('shop_name'),
                'owner_name'=>$this->input->post('owner_name'),
                'mobile'=>$this->input->post('mobile'),
                'address'=>$this->input->post('address'),
                'city'=>$this->input->post('city'),
                'pincode'=>$this->input->post('pincode'),
                'lat'=>$this->input->post('lat'),
                'lng'=>$this->input->post('lng'),
                'status'=>$this->input->post('status'),
                'delivery_time'=>$delivery_time,
                'min_order_amount'=>$min_order_amount
                );
        }
        
        $wr = array('id'=>$_SESSION['vendors']['vendor_id']);
        $upd = $this->db->update("vendor_shop",$ar,$wr);
        if($upd)
        {
            $qry = $this->db->query("select * from vendor_shop where id='".$_SESSION['vendors']['vendor_id']."'");
           $data['settings'] = $qry->row();
           $this->session->set_flashdata('success_message', 'Profile Updated Successfully');
            
        }
        $this->load->view('vendors/includes/header', $this->data);
        $this->load->view('vendors/settings', $data);
        $this->load->view('vendors/includes/footer');
        
    }


 private function upload_file($file_name) {
// echo $file_ext = pathinfo($_FILES[$file_name]["name"], PATHINFO_EXTENSION);
// die;

    if($_FILES[$file_name]['name']!='')
    {

        if($_FILES[$file_name]["size"]<'5114374')
        {
            $upload_path1 = "./uploads/shops/";
            $config1['upload_path'] = $upload_path1;
            $config1['allowed_types'] = "*";
            // $config1['allowed_types'] = "*";
            $config1['max_size'] = "204800000";
            $img_name1 = strtolower($_FILES[$file_name]['name']);
            $img_name1 = preg_replace('/[^a-zA-Z0-9\.]/', "_", $img_name1);
            $config1['file_name'] = date("YmdHis") . rand(0, 9999999) . "_" . $img_name1;
            $this->load->library('upload', $config1);
            $this->upload->initialize($config1);
            $this->upload->do_upload($file_name);
            $fileDetailArray1 = $this->upload->data();
            // echo $this->upload->display_errors();
            // die;
            return $fileDetailArray1['file_name'];
        }
        else
        {
            return 'false';
        }
    }
    else
    {
        return '';
    }
    }



    function changePassword1()
    {

            $vendor_id = $_SESSION['vendors']['vendor_id'];
            $oldpassword = $this->input->get_post('oldpassword');
            $newpassword = $this->input->get_post('newpassword');

        $chk = $this->db->query("select * from vendor_shop where id='".$vendor_id."' and password='".md5($oldpassword)."'");
        if($chk->num_rows()>0)
        {
            $ar = array('password'=>md5($newpassword));
            $wr = array('id'=>$vendor_id);
            $upd = $this->db->update("vendor_shop",$ar,$wr);
            if($upd)
            {
                $this->session->set_flashdata('success_message1', 'Password Changed Successfully.');
            redirect('vendors/settings');
            }
            
        }
        else
        {
            $this->session->set_flashdata('error_message1', 'Old Password Wrong.');
            redirect('vendors/settings');
        }
    }

}
