<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Order_addons
 *
 * @author Admin
 */
class Order_addons extends MY_Controller {

    //put your code here
    function __construct() {
        parent::__construct();
        if ($this->session->userdata('vendors')['vendors_logged_in'] != true) {
            //$this->session->set_flashdata('error', 'Session Timed Out');
            redirect('vendors/login');
        }
        $this->load->model('vendor_services');
    }

    function services($order_id) {
        $this->data['order_real_id'] = $order_id;
        $vendor_data = $this->session->userdata('vendors');
        $service_provider_id = $vendor_data['vendor_id'];
        $this->data['vendor_id'] = $service_provider_id;
        $order_data = $this->my_model->get_data_row("services_orders", array("id" => $order_id));
        $this->data['order_id'] = $order_data->order_id;
        $category_id = $order_data->ordered_categories;
        $this->db->select("service_id");
        $disabled_services = $this->my_model->get_data("service_providers_disabled_services", array("service_providers_id" => $service_provider_id));
        $disabled_services_string = "";
        foreach ($disabled_services as $ds) {
            $disabled_services_string .= $ds->service_id . ",";
        }
        $disabled_services_string = rtrim($disabled_services_string, ',');
        $this->db->select("id,sub_category_name,seo_url,description");
        $this->db->where("status", true);
        $sub_cats = $this->my_model->get_data("services_sub_categories", array("cat_id" => $category_id));
        $this->data['current_sub_id'] = $sub_cats[0]->id;
        if ($this->input->get('sub_cat_id')) {
            $this->data['current_sub_id'] = $this->input->get('sub_cat_id');
        }
        if (!empty($disabled_services_string)) {
            $this->db->where("(id NOT IN (" . $disabled_services_string . "))");
        }
        $this->db->where("status", true);
        $services = $this->my_model->get_data("services", array('sub_cat_id' => $this->data['current_sub_id'], 'has_visit_and_quote' => 0));
        foreach ($services as $serv) {
            $this->db->select("avg(rating) as rating");
            $rating = $this->my_model->get_data_row("services_reviews", array("service_id" => $serv->id))->rating;
            $serv->rating = ($rating) ? round($rating, 1) : 0;
            $serv->quantity = 0;
            $check_in_order = $this->my_model->get_data_row("service_orders_items", array("order_id" => $this->data['order_id'], "service_id" => $serv->id, "is_addon" => 1, "is_paid" => 0));
            if (!empty($check_in_order)) {
                $serv->quantity = $check_in_order->quantity;
            }
            $serv->image = $this->my_model->get_data_row("services_images", array("service_id" => $serv->id))->image;
        }
        $this->data['services'] = $services;
        $this->data['sub_categories'] = $sub_cats;
        $this->load->view('vendors/includes/header', $this->data);
        $this->load->view('vendors/service_cart', $this->data);
        $this->load->view('vendors/includes/footer');
    }

}
