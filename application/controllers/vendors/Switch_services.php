<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Switch_services
 *
 * @author Admin
 */
class Switch_services extends MY_Controller {

//put your code here

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('vendors')['vendors_logged_in'] != true) {
            //$this->session->set_flashdata('error', 'Session Timed Out');
            redirect('vendors/login');
        }
        $this->load->model('vendor_services');
    }

    function index() {
        $this->data['page_name'] = "switch_services";
        $vendor_data = $this->session->userdata('vendors');
        $service_provider_id = $vendor_data['vendor_id'];
        $this->data['vendor_id'] = $service_provider_id;

        $user_data = $this->check_service_provider($service_provider_id);
        $categories_ids = rtrim($user_data->categories_ids, ',');
        $this->db->select('id,name,description');
        $this->db->where("id IN (" . $categories_ids . ")");
        $categories = $this->my_model->get_data("services_categories", null, 'priority', 'desc');
        if ($categories) {
            foreach ($categories as $cat) {
                $check = $this->my_model->get_data_row("service_providers_disabled_categories", array("service_providers_id" => $service_provider_id, "cat_id" => $cat->id));
                $cat->is_active = !empty($check) ? 'No' : 'Yes';
            }
        }
        $this->data['categories'] = $categories;
        $this->load->view('vendors/includes/header', $this->data);
        $this->load->view('vendors/switch_categories', $this->data);
        $this->load->view('vendors/includes/footer');
    }

    function services($cat_id) {
        $this->data['page_name'] = "switch_services";
        $this->data['cat_id'] = $cat_id;
        $vendor_data = $this->session->userdata('vendors');
        $service_provider_id = $vendor_data['vendor_id'];
        $this->data['vendor_id'] = $service_provider_id;

        $sub_categories = $this->my_model->get_data("services_sub_categories", array("cat_id" => $cat_id));
        $this->data['current_sub_category_id'] = $sub_categories[0]->id;
        if (!empty($this->input->get("sub_cat"))) {
            $this->data['current_sub_category_id'] = $this->input->get("sub_cat");
        }
        $services = $this->my_model->get_data("services", array("cat_id" => $cat_id, "sub_cat_id" => $this->data['current_sub_category_id']));
        foreach ($services as $service) {
            $check = $this->my_model->get_data_row("service_providers_disabled_services", array("service_providers_id" => $service_provider_id, "service_id" => $service->id));
            $service->is_active = !empty($check) ? 'No' : 'Yes';
        }
        $this->data['services'] = $services;
        $this->data['sub_categories'] = $sub_categories;

        $this->db->where("cat_id", $cat_id);
        $this->db->where("sub_cat_id", 0);
        $no_sub_category_services = $this->db->get("services")->result();
        foreach ($no_sub_category_services as $service) {
            $check = $this->my_model->get_data_row("service_providers_disabled_services", array("service_providers_id" => $service_provider_id, "service_id" => $service->id));
            $service->is_active = !empty($check) ? 'No' : 'Yes';
        }
        $this->data['no_sub_category_services'] = $no_sub_category_services;

        $this->load->view('vendors/includes/header', $this->data);
        $this->load->view('vendors/switch_categories', $this->data);
        $this->load->view('vendors/includes/footer');
    }

}
