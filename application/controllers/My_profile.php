<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class My_profile extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->check_user_log(USER_ID);
        $this->data['page_name'] = 'my_profile';
    }

    function index() {
        if (!empty($this->input->post())) {
            $data = array(
                "first_name" => $this->input->post('first_name'),
                "email" => $this->input->post('email'),
                "phone" => $this->input->post('phone'),
                "state_id" => $this->input->post('state_id'),
                "city_id" => $this->input->post('city_id'),
                "pincode" => $this->input->post('pincode')
            );
            $update = $this->my_model->update_data('users', array("id" => USER_ID), $data);
            if ($update) {
                $this->session->set_flashdata('success', 'Profile Updated');
            } else {
                $this->session->set_flashdata('error', 'Unable to Update Profile');
            }
            redirect(base_url('my-profile'));
        }
        unset($_POST);
        $this->data['user_data'] = $this->my_model->get_data_row("users", array("id" => USER_ID));
        $this->data['states'] = $this->my_model->get_data("states");
        $this->data['cities'] = $this->my_model->get_data("cities", array('state_id' => $this->data['user_data']->state_id));
        $this->data['pincodes'] = $this->my_model->get_data("pincodes");
        $this->my_view('my-profile', $this->data);
    }

}
