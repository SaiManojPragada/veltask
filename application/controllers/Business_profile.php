<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Business_profile extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->check_user_log(USER_ID);
    }

    function index() {
        $this->data['page_name'] = "business_profile";
        $this->data['business_details'] = $this->my_model->get_data_row("users_bussiness_details", array("user_id" => USER_ID));

        $business_benefits = $this->my_model->get_data_row("business_profile_benifits");
        unset($business_benefits->created_at);
        unset($business_benefits->updated_at);
        unset($business_benefits->status);
        unset($business_benefits->id);
        $business_benefits->display_image = base_url('uploads/business_benifits_images/') . $business_benefits->display_image;
        $benefits_description = str_replace('<li>', '<li><span style="color:#f26e6a">✓</span> ', $business_benefits->benefits_description);
        $benefits_description = str_replace('<li>', '<p>', $benefits_description);
        $benefits_description = str_replace('</li>', '</p>', $benefits_description);
        $benefits_description = str_replace('<ul>', '', $benefits_description);
        $business_benefits->benefits_description = str_replace('</ul>', '', $benefits_description);
        $this->db->select("id,document_type");
        $document_types = $this->my_model->get_data("business_document_types", null, null, null, true);
        $business_benefits->document_types = $document_types;
        $this->data['business_benifits'] = $business_benefits;
        $this->data['docs_types'] = $this->my_model->get_data("business_document_types", array("status" => 1));
        $this->my_view('business-profile', $this->data);
    }

    function update() {
        $document = $this->image_upload("document", "company_proofs");
        $data = array(
            "user_id" => USER_ID,
            "company_name" => $this->input->post("company_name"),
            "company_address" => $this->input->post("company_address"),
            "proof_type" => $this->input->post("proof_type"),
            "created_at" => time(),
            "updated_at" => time(),
            "status" => 0
        );
        if (!empty($document)) {
            $data['document'] = $document;
        }
        $check = $this->my_model->get_data_row("users_bussiness_details", array("user_id" => USER_ID));
        if (!empty($check)) {
            if (!empty($document)) {
                unlink('uploads/company_proofs/' . $check->document);
            }
            $update = $this->my_model->update_data("users_bussiness_details", array("user_id" => USER_ID), $data);
            if ($update) {
                $this->session->set_flashdata("success", "Business Profile Updated");
            } else {
                $this->session->set_flashdata("error", "Unable to Update Bussiness Profile");
            }
        } else {
            $ins = $this->my_model->insert_data("users_bussiness_details", $data);
            if ($ins) {
                $this->session->set_flashdata("success", "Business Profile Updated");
            } else {
                $this->session->set_flashdata("error", "Unable to Update Bussiness Profile");
            }
        }
        redirect(base_url('business-profile'));
    }

}
