<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Cart extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->check_user_log(USER_ID);
        $this->db->where("expiry_date >='" . date('Y-m-d') . "'");
        $this->data['coupon_codes'] = $this->my_model->get_data("services_coupon_codes", array("status" => true));
        $this->data['coupon_and_wallet'] = $this->session->userdata("service_coupon_data");
        $this->data['wallet_amount'] = $this->my_model->get_data_row("users", array("id" => USER_ID))->wallet_amount;
//        print_r($this->data['coupon_and_wallet']);
//        die;
    }

    function index() {
        $user_data = $this->check_user_id(USER_ID);
        $cart = $this->my_model->get_data("services_cart", array("user_id" => USER_ID));
        $min_cart_amount = $this->my_model->get_data_row('cart_min_amount')->services_min_amount;
        if (!empty(USER_ID)) {
            $this->get_cart_totals($cart[0]->has_visit_and_quote);
        } else {
            redirect(base_url());
        }

        foreach ($cart as $c) {
            $c->service_details = $this->my_model->get_data_row('services', array("id" => $c->service_id));
        }
        $cc = $this->get_cart_totals($cart[0]->has_visit_and_quote);
        $this->data['min_cart_value'] = $min_cart_amount;
        $this->data['cart'] = $cart;
        $this->data['is_cart_enable'] = 0;
        if ($cc->total > $min_cart_amount) {
            $this->data['is_cart_enable'] = 1;
        }
        $this->my_view('cart', $this->data);
    }

    function check_out() {
        $min_cart_amount = $this->my_model->get_data_row('cart_min_amount')->services_min_amount;
        $cart = $this->my_model->get_data("services_cart", array("user_id" => USER_ID));
        $cc = $this->get_cart_totals($cart[0]->has_visit_and_quote);
        if (empty($cart)) {
            redirect(base_url('cart'));
        }

        if ($cc->total < $min_cart_amount) {
            redirect(base_url('cart'));
        }
        $this->data['cart'] = $cart;
        $this->data['states'] = $this->my_model->get_data('states');
        $this->data['addresses'] = $this->my_model->get_data('user_address', array('user_id' => USER_ID), 'id', 'desc');
        foreach ($this->data['addresses'] as $item) {
            $item->state_name = $this->my_model->get_data_row("states", array("id" => $item->state))->state_name;
            $item->city_name = $this->my_model->get_data_row("cities", array("id" => $item->city))->city_name;
            $item->pincode_pin = $this->my_model->get_data_row("pincodes", array("id" => $item->pincode))->pincode;
            $item->json_string = json_encode($item);
        }
        if (!empty($this->session->userdata('time_slots_time'))) {
            $date = $this->session->userdata('time_slots_date');
            $today = date('d-m-Y');

            if ($date == $today) {
                $time = date("H:i", (strtotime(date('h:i A')) + 60 * 60));
                $this->db->where('time_slot > "' . $time . '"');
            }
            $res = $this->db->get("services_time_slots")->result();
            if ($res && sizeof($res) > 1) {
                $op = "";
                foreach ($res as $item) {
                    $item->time_slot = date('h:i A', strtotime($item->time_slot));
                    $selected = ($item->time_slot == $this->session->userdata('time_slots_time')) ? 'selected' : '';
                    $op .= "<option value='$item->time_slot' $selected>$item->time_slot</option>";
                }
                $this->data['time_slots_string'] = $op;
            }
        }
        $pre_amount = $this->session->userdata("service_coupon_data");
        $this->data['order_id'] = $this->generate_random_key("service_orders_items", "order_id", "VT");
        $cart = $this->my_model->get_data("services_cart", array("user_id" => USER_ID));
        $this->get_cart_totals($cart[0]->has_visit_and_quote);
        $selected_address_id = $this->my_model->get_data_row("user_address", array("user_id" => USER_ID, "isdefault" => "yes"))->id;
        $cart_totals = $this->get_cart_totals($cart[0]->has_visit_and_quote);
        $amount_to_be_paid = str_replace(",", "", (!empty($pre_amount)) ? $pre_amount['grand_total'] : $cart_totals->total);
        $out_data = array(
            "user_id" => USER_ID,
            "coupon_code_id" => (!empty($pre_amount['coupon_id'])) ? $pre_amount['coupon_id'] : "",
            "coupon_code" => (!empty($pre_amount['coupon_code'])) ? $pre_amount['coupon_code'] : "",
            "coupon_discount" => (!empty($pre_amount['coupon_discount'])) ? $pre_amount['coupon_discount'] : "",
            "membership_discount" => (float) $this->data['cart_totals']->membership_discount,
            "business_discount" => (float) $this->data['cart_totals']->business_discount,
            "user_addresses_id" => $selected_address_id,
            "time_slot_date" => $this->session->userdata("time_slots_date"),
            "time_slot" => $this->session->userdata('time_slots_time'),
            "payment_type" => $this->session->userdata('payment_method'),
            "amount_paid" => $amount_to_be_paid,
            "razorpay_order_id" => null,
            "razorpay_transaction_id" => null,
            "order_id" => $this->data['order_id'],
            "used_wallet_amount" => (!empty($pre_amount['wallet_applied_amount'])) ? $pre_amount['wallet_applied_amount'] : "",
        );
        $this->data['out_data'] = json_encode($out_data);
        $this->my_view('checkout', $this->data);
    }

    function payment_type() {
        $this->data['payment_methods'] = $this->my_model->get_data('payment_methods_enables', array("status" => true));
        $this->data['pre_pay_percentage'] = number_format($this->my_model->get_data_row("prepay_percentage")->prepay_percentage) . "%";
        $cart = $this->my_model->get_data("services_cart", array("user_id" => USER_ID));
        $this->data['cart'] = $cart;
        $min_cart_amount = $this->my_model->get_data_row('cart_min_amount')->services_min_amount;
        $cc = $this->get_cart_totals($cart[0]->has_visit_and_quote);
        if (empty($cart)) {
            redirect(base_url('cart'));
        }
        if ($cc->total < $min_cart_amount) {
            redirect(base_url('cart'));
        }
        $this->get_cart_totals($cart[0]->has_visit_and_quote);
        $this->my_view('payment_type', $this->data);
    }

    function decide_payment_page() {
        $id = $this->input->post('id');
        $user_id = $this->input->post("user_id");
        $user_selected_address = $this->my_model->get_data_row("user_address", array("user_id" => $user_id, "isdefault" => "yes"));
        $user_pincode_id = $user_selected_address->pincode;
        $check_for_service_providers = $this->my_model->get_data("service_providers", "FIND_IN_SET('" . $user_pincode_id . "', sp_pincodes)");
        if (empty($check_for_service_providers)) {
            $arr = array(
                "status" => false,
                "messsage" => "No service Providers are present in this area."
            );
            echo json_encode($arr);
            die;
        }
//        echo $this->db->last_query();
        if ($id == 1) {
            $this->session->set_userdata('payment_method', $id);
            $arr = array(
                "status" => true,
                "messsage" => "payment"
            );
            echo json_encode($arr);
            die;
        } else {
            $this->session->set_userdata('payment_method', $id);
            $arr = array(
                "status" => true,
                "messsage" => "payment/pre_pay"
            );
            echo json_encode($arr);
            die;
        }
    }

    function payment_cancelled() {
        $this->session->set_flashdata("error", "Payment has been Cancelled.");
    }

}
