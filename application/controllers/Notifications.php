<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Notifications
 *
 * @author Admin
 */
class Notifications extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->data['page_name'] = 'notifications';
        $this->check_user_log(USER_ID);
    }

    function index() {
        $this->data['notifications'] = $this->my_model->get_data("user_notifications", array("user_id" => USER_ID));
        foreach ($this->data['notifications'] as $not) {
            $order_data = $this->my_model->get_data_row("services_orders", array("user_id" => USER_ID, "id" => $not->order_id));
            $not->order_id_re = $order_data->order_id;
        }
        $this->my_view('notifications', $this->data);
    }

}
