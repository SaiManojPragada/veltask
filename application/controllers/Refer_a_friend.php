<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Refer_a_friend extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->check_user_log(USER_ID);
        $this->data['page_name'] = 'refer_a_friend';
    }

    function index() {
        $this->data['referral_data'] = $this->my_model->get_data_row("referrals");
        $this->db->select('referral_code');
        $this->data['my_code'] = $this->my_model->get_data_row("users", array("id" => USER_ID))->referral_code;
        $this->db->select('first_name,email,referral_code,created_date');
        $this->data['my_referrals'] = $this->my_model->get_data("users", array("reffered_by" => USER_ID));
        $this->my_view('refer-a-friend', $this->data);
    }

}
