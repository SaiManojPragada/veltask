<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Services extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->session->unset_userdata("service_coupon_data");
        $this->session->unset_userdata("time_slots_date");
        $this->session->unset_userdata("time_slots_time");
    }

    function index() {
        redirect(base_url());
    }

    function clear_cart_if_prev_is_visit_and_quote($category) {
        $cart = $this->my_model->get_data("services_cart", array("user_id" => USER_ID));
        if ($cart[0]->has_visit_and_quote == 1) {
            $this->my_model->delete_data("services_cart", array("user_id" => USER_ID));
            $url = $this->uri->uri_string();
            if (!empty($this->input->get('page'))) {
                $url .= "?page=" . $this->input->get('page');
            }
            redirect($url);
        }
        $services_filters = $this->session->userdata("services_filters");
        if ($category != $services_filters['cat']) {
//            print_r($category . " , " . $services_filters['cat']);
            $this->session->unset_userdata("services_filters");
        }
//        print_r($services_filters['cat']);
    }

    function view($category, $sub_category = null) {
        $this->clear_cart_if_prev_is_visit_and_quote($category);
        $this->data['category'] = $category;
        $this->data['sub_category'] = $sub_category;
        $this->data['sort'] = $this->input->get("sort");
        $category_id = $this->my_model->get_data_row('services_categories', array('seo_url' => $category))->id;
        $this->data['sub_categories_all'] = $this->my_model->get_data('services_sub_categories', array("cat_id" => $category_id));
        foreach ($this->data['sub_categories_all'] as $ss) {
            $ss->count = sizeof($this->my_model->get_data('services', array("cat_id" => $category_id, "sub_cat_id" => $ss->id)));
        }
        if (empty($category_id)) {
            redirect(base_url());
        }
        $sub_category_id = $this->my_model->get_data_row('services_sub_categories', array('seo_url' => $sub_category))->id;
        $where = array(
            "cat_id" => $category_id
        );
        if (!empty($sub_category_id)) {
            $where['sub_cat_id'] = $sub_category_id;
        }
        $limit = 4;
        $page = $this->input->get('page');
        $page = ($page > 1) ? $page : 1;
        $this->data['page'] = $page;
//        echo $page;
        if (!$page || $page == 1) {
            $page = 0;
        } else {
            $page = $page - 1;
        }

        $max_price = 0;
        $start = $page * $limit;
        $this->db->limit($limit, $start);
        $sort = $this->data['sort'];
        if ($sort == "latest") {
            $this->db->order_by("id", "desc");
        }
        if ($sort == "oldest") {
            $this->db->order_by("id", "asc");
        }
        if ($sort == "low-to-high") {
            $this->db->order_by("sale_price", "asc");
        }
        if ($sort == "high-to-low") {
            $this->db->order_by("sale_price", "desc");
        }
        $price_filters = $this->session->userdata("services_filters");
        $filter_data = $this->session->userdata("services_filters")['price_filter_data'];
//        die;
        if (!empty($price_filters)) {
            $min = $price_filters['min'];
            $max = $price_filters['max'];
            $this->data['selected_prices'] = explode(',', $price_filters['selected_prices']);
            $this->db->where("sale_price >= " . $min . " AND sale_price <= " . $max);
        }
        $this->data['services'] = $this->my_model->get_data("services", $where, 'id', 'desc', true);

        foreach ($filter_data as $filter) {
            if ($filter->is_selected == true) {
                $this->data['services'] = array();
            }
        }
        if (empty($this->data['services'])) {
            foreach ($filter_data as $filter) {
                if ($filter->is_selected == true) {
                    $this->db->where("sale_price <= " . (int) $filter->max);
                    $this->db->where("sale_price >= " . (int) $filter->min);
                    $servs = $this->db->get("services")->result();
                    if (!empty($servs)) {
                        foreach ($servs as $ss) {
                            array_push($this->data['services'], $ss);
                        }
                    }
                }
            }
        }

        if (!empty($price_filters)) {
            $min = $price_filters['min'];
            $max = $price_filters['max'];
            $this->data['selected_prices'] = explode(',', $price_filters['selected_prices']);
            $this->db->where("sale_price >= " . $min . " AND sale_price <= " . $max);
        }
        $this->data['total_pages'] = round(sizeof($this->my_model->get_data("services", $where, 'id', 'desc', true)) / $limit);
        $cart = $this->my_model->get_data("services_cart", array("user_id" => USER_ID));
        $this->data['cart'] = $cart;
        if (!empty(USER_ID)) {
            $cart_totals = $this->get_cart_totals($cart[0]->has_visit_and_quote);
        }
//        $this->db->select("count(id) as items_in_cart,sum(sub_total) as cart_total");
//        $this->data['cart_total'] = $this->my_model->get_data_row('services_cart', array("user_id" => USER_ID));
        $this->data['cart_total'] = $cart_totals;
        $this->db->select("count(id) as items_in_cart,sum(sub_total) as cart_total");
        $this->data['cart_total']->items_in_cart = $this->my_model->get_data_row('services_cart', array("user_id" => USER_ID))->items_in_cart;
        foreach ($this->data['services'] as $serv) {
            $this->db->select("avg(rating) as rating");
            $serv->rating = number_format($this->my_model->get_data_row("services_reviews", array("service_id" => $serv->id))->rating, 1);
            $get_cart_item = $this->my_model->get_data_row('services_cart', array("user_id" => USER_ID, "service_id" => $serv->id));
            if (!empty($get_cart_item)) {
                $serv->quantity = $get_cart_item->quantity;
            }
            $serv->sub_category_url = $this->my_model->get_data_row('services_sub_categories', array('id' => $serv->sub_cat_id))->seo_url;
            $serv->image = $this->my_model->get_data_row('services_images', array('service_id' => $serv->id))->image;
        }

        $all_services = $this->my_model->get_data("services", array("cat_id" => $category_id), 'id', 'desc', true);
        foreach ($all_services as $serv) {
            $max_price = ($serv->sale_price > $max_price) ? $serv->sale_price : $max_price;
        }
        $this->data['filters'] = array();
        $max_lite = (int) ($max_price / 6);
        $fill_val = 0;
//        $selected_filters = $this->input->post('selected_filters');
        if (!empty($all_services)) {
            for ($i = 0; $i < 6; $i++) {
                if ($fill_val == 0) {
                    $display_text = "&nbsp;&nbsp;Rs. " . ($fill_val + $max_lite) . " and Below";
                } else {
                    $display_text = " Rs. " . $fill_val;
                    $display_text .= " -" . ( ($i == 5) ? "&nbsp;&nbsp;Rs. " . round($max_price) : "&nbsp;&nbsp;Rs. " . ($fill_val + $max_lite));
                }
                $jid = array(
                    "id" => $i,
                    "min" => $fill_val,
                    "max" => $fill_val + $max_lite,
                    "display_text" => $display_text
//                "is_checked" => in_array(, $item)
                );
                $fill_val = $fill_val + $max_lite;
                array_push($this->data['filters'], $jid);
            }
        }
        $this->my_view('services', $this->data);
    }

    function view_service($category, $sub_category, $slug) {
        $this->clear_cart_if_prev_is_visit_and_quote($category);
        if (empty($slug)) {
            redirect(base_url('services/' . $category . '/' . $sub_category));
        }
        $cat = $this->my_model->get_data_row('services_categories', array('seo_url' => $category));
        $category_id = $cat->id;
        $this->data['category_name'] = $cat->name;
        $sub_category_id = $this->my_model->get_data_row('services_sub_categories', array('seo_url' => $sub_category))->id;
        $service = $this->my_model->get_data_row('services', array('seo_url' => $slug));
        $this->db->select("avg(rating) as rating");
        $this->db->limit(10);
        $service->rating = number_format($this->my_model->get_data_row("services_reviews", array("service_id" => $service->id))->rating, 1);
        $reviews = $this->my_model->get_data("services_reviews", array("service_id" => $service->id, "status" => 1));
        foreach ($reviews as $rev) {
            $this->db->select("first_name,image");
            $rev->user_data = $this->my_model->get_data_row("users", array("id" => $rev->user_id));
        }
        $service->reviews = $reviews;
        $services_images = $this->my_model->get_data('services_images', array('service_id' => $service->id));
        $this->data['service'] = $service;
        $this->data['service_images'] = $services_images;
        $cart = $this->my_model->get_data("services_cart", array("user_id" => USER_ID));
        $this->data['cart'] = $cart;
        if (!empty(USER_ID)) {
            $cart_totals = $this->get_cart_totals($cart[0]->has_visit_and_quote);
        }
        $this->data['cart_total'] = $cart_totals;
        $this->db->select("count(id) as items_in_cart,sum(sub_total) as cart_total");
        $this->data['cart_total']->items_in_cart = $this->my_model->get_data_row('services_cart', array("user_id" => USER_ID))->items_in_cart;
        $this->db->order_by("RAND()");
        $this->db->where("cat_id", $category_id);
        $this->data['related_services'] = $this->my_model->get_data('services', "id!='$service->id'", null, null, true, 4);
        $get_cart_item = $this->my_model->get_data_row('services_cart', array("user_id" => USER_ID, "service_id" => $service->id));
        if (!empty($get_cart_item)) {
            $service->quantity = $get_cart_item->quantity;
        }
        foreach ($this->data['related_services'] as $row) {
            $get_cart_item = $this->my_model->get_data_row('services_cart', array("user_id" => USER_ID, "service_id" => $row->id));
            if (!empty($get_cart_item)) {
                $row->quantity = $get_cart_item->quantity;
            }
            $row->image = $this->my_model->get_data_row('services_images', array('service_id' => $row->id))->image;
            $row->category_url = $this->my_model->get_data_row('services_categories', array('id' => $row->cat_id))->seo_url;
            $row->sub_category_url = $this->my_model->get_data_row('services_sub_categories', array('id' => $row->sub_cat_id))->seo_url;
        }
        $this->data['array_chunk_images'] = array_chunk($services_images, 5);
        $this->my_view('view-service', $this->data);
    }

}
