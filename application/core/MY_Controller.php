<?php

defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Kolkata');

class MY_Controller extends CI_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->data = array();
//        defining the user data
        $site_settings = $this->my_model->get_data_row('site_settings');
        define('SITE_NAME', $site_settings->site_name);
        define('SITE_LOGO', $site_settings->logo);
        define('SITE_FAV_ICON', $site_settings->fav_icon);
        define('SITE_FOOTER_LOGO', $site_settings->footer_logo);
        define('SITE_ABOUT', $site_settings->about);
        define('SITE_FB_LINK', $site_settings->facebook_link);
        define('SITE_TWT_LINK', $site_settings->twitter_link);
        define('SITE_GP_LINK', $site_settings->google_plus_link);
        define('SITE_LI_LINK', $site_settings->linkedin_link);

        define('SITE_PHONE', $site_settings->phone_number);
        define('SITE_ALT_PHONE', $site_settings->alternate_phone_number);

        define('SITE_EMAIL', $site_settings->contact_email);
        define('SITE_ALT_EMAIL', $site_settings->alternate_contact_email);

        if (!empty($this->session->userdata('user'))) {
            $user_data = $this->session->userdata('user');
            $this->db->select("count(id) as items_in_cart,sum(sub_total) as cart_total");
            $cart_totals = $this->my_model->get_data_row('services_cart', array("user_id" => $user_data['user_id']));
            $user_data = $this->my_model->get_data_row("users", array("id" => $user_data['user_id']));
            if (empty($user_data)) {
                $this->session->unset_userdata("user");
                redirect(base_url());
            } else {
                define('USER_NAME', $user_data->first_name);
                define('USER_PHONE', $user_data->phone);
                define('USER_EMAIL', $user_data->email);
                define('USER_ID', $user_data->id);
                if (!empty($user_data->image)) {
                    define('USER_IMAGE', base_url('uploads/users/') . $user_data->image);
                } else {
                    define('USER_IMAGE', '');
                }
            }
            define('USER_SESSION_ID', generateRandomString(30));
            define('TOTAL_ITEMS_IN_CART', $cart_totals->items_in_cart);
            $razorpay_details = $this->my_model->get_data_row("paymentgateway_and_smtp");
            define('RAZORPAY_KEY', $razorpay_details->razorpay_key);
            define('RAZORPAY_SECRET', $razorpay_details->razorpay_secret);

            $this->data['has_membership'] = false;
            $this->db->where("expiry_date >=", time());
            $this->db->order_by("id", "desc");
            $check = $this->my_model->get_data_row("membership_transactions", array("user_id" => $user_data->id));
            if (!empty($check)) {
                $this->data['has_membership'] = true;
            }
        } else {
            define('USER_NAME', '');
            define('USER_PHONE', '');
            define('USER_EMAIL', '');
            define('USER_ID', '');
            define('USER_IMAGE', '');
            define('USER_SESSION_ID', '');
            define('TOTAL_ITEMS_IN_CART', '0');
            define('RAZORPAY_KEY', '');
            define('RAZORPAY_SECRET', '');
        }
        //$data = $this->my_model->get_data('services_categories', array('status' => true));
        $qry = $this->db->query("select * from services_categories where status=1 order by priority asc");
        $data = $qry->result();

        $result = array();
        foreach ($data as $item) {
            $sub_categories = $this->my_model->get_data('services_sub_categories', array('cat_id' => $item->id, 'status' => true));
            if (!empty($sub_categories)) {
                $item->sub_categories = $sub_categories;
                array_push($result, $item);
            }
        }
        $this->data['ecom_categories'] = $this->my_model->get_data('categories', array("status" => 1), 'priority', 'asc');
//        print_r($this->data['categories']);
        $this->data['memberships_list'] = $this->my_model->get_data('memberships', '', 'sale_price', 'asc', true);
        $this->data['nav_bar_services'] = $result;
        $this->data['states'] = $this->my_model->get_data("states");
        $this->update_cart_prices();
    }

    function generate_random_key($table, $column, $prefix) {
        $random = $prefix . rand(000000, 99999999);
        $chec = $this->db->where($column, $random)->get($table)->result();
        if ($chec) {
            $this->generate_random_key($table, $column);
            return false;
        } else {
            return $random;
        }
    }

    function image_upload($var, $path_folder, $enc = true, $prev_file_name = "") {
        if (!empty($_FILES[$var]['name'])) {
            $real_path = realpath(APPPATH . '../uploads/' . $path_folder);
            if (!file_exists($real_path)) {
                mkdir('uploads/' . $path_folder, 777, true);
            }
            $config['upload_path'] = $real_path;
            $config['allowed_types'] = '*';
            $config['encrypt_name'] = $enc;
            if (!empty($file_name)) {
                $config['overwrite'] = TRUE;
                $config['file_name'] = $file_name;
            }
            $this->load->library('upload', $config);
            if ($this->upload->do_upload($var)) {
                $img_data = $this->upload->data();
                $path = $img_data['file_name'];
                if (!empty($prev_file_name)) {
                    unlink('uploads/' . $path_folder . '/' . $prev_file_name);
                }
                return $path;
            } else {
                print_r($this->upload->display_errors());
                die;
                return "";
            }
        } else {
            return "";
        }
    }

    function multiple_images_upload($var, $path_folder, $parent_id) {
        $real_path = realpath(APPPATH . '../uploads/' . $path_folder);
        if (!file_exists($real_path)) {
            mkdir('uploads/' . $path_folder, 777, true);
        }
        $config['upload_path'] = $real_path;
        $config['allowed_types'] = 'jpg|png|jpef|jpeg';
        $config['encrypt_name'] = true;
        $this->load->library('upload', $config);
        $total_images = count($_FILES[$var]['name']);
        $images_list = array();
        $images = $_FILES;
        if ($total_images > 0) {
            for ($i = 0; $i < $total_images; $i++) {
                $_FILES[$var]['name'] = $images[$var]['name'][$i];
                $_FILES[$var]['type'] = $images[$var]['type'][$i];
                $_FILES[$var]['tmp_name'] = $images[$var]['tmp_name'][$i];
                $_FILES[$var]['error'] = $images[$var]['error'][$i];
                $_FILES[$var]['size'] = $images[$var]['size'][$i];
                if ($this->upload->do_upload($var)) {
                    $img_data = $this->upload->data();
                    $up_file = array("service_id" => $parent_id, "image" => $img_data['file_name']);
                    array_push($images_list, $up_file);
                }
            }
            return $images_list;
        }
        return false;
    }

    function post($str = "") {
        if (empty($str)) {
            return $this->input->post();
        }
        return $this->input->post($str);
    }

    function make_seo_name($title) {
        return preg_replace('/[^a-z0-9_-]/i', '', strtolower(str_replace(' ', '-', trim($title))));
    }

    public function my_view($design_view, $data = null) {
        $this->load->view("web/includes/header", $this->data);
        $this->load->view('web/' . $design_view, $data);
        $this->load->view("web/includes/footer", $this->data);
    }

    function admin_view($design_view) {
//        $this->load->view("includes/header", $this->data);
//        $this->load->view("admin/menu", $this->data);
        $this->load->view("admin/" . $design_view);
//        $this->load->view("includes/footer");
    }

    function check_user_id($user_id) {
        $user_data = $this->my_model->get_data_row("users", array("id" => $user_id));
        if (empty($user_data)) {
            $resp = array("status" => false, "message" => "You are not authorised to perform this action.");
            echo json_encode($resp);
            die;
        }
        return $user_data;
    }

    function check_service_provider($user_id) {
        $user_data = $this->my_model->get_data_row("service_providers", array("id" => $user_id, 'status' => true));
        if (empty($user_data)) {
            $resp = array("status" => false, "message" => "You are not authorised to perform this action.");
            echo json_encode($resp);
            die;
        }
        return $user_data;
    }

    function check_user_log($user_id) {
        if (empty($user_id)) {
            $this->session->set_userdata("login_check", "Yes");
            redirect(base_url());
        }
    }

    function update_cart_prices() {
        $cart = $this->my_model->get_data("services_cart");
        foreach ($cart as $cc) {
            $current_prod_details = $this->my_model->get_data_row("services", array("id" => $cc->id));
            $car_item = array(
                "price" => $current_prod_details->price,
                "unit_price" => $current_prod_details->sale_price,
                "visiting_charges" => $current_prod_details->visiting_charges,
                "tax" => $current_prod_details->tax,
                "sub_total" => $current_prod_details->sale_price * $cc->quantity,
                "grand_total" => ($current_prod_details->sale_price * $cc->quantity) + $current_prod_details->tax + $current_prod_details->visiting_charges
            );
            $this->my_model->update_data("services_cart", array("id" => $car_item), $car_item);
        }
        return true;
    }

    function get_cart_totals($has_visit_and_quote = 0) {
        $user_data = $this->check_user_id(USER_ID);
        $this->db->where("user_id", USER_ID);
        $this->db->select("SUM(grand_total) as total, SUM(sub_total)as sub_total, SUM(tax) as tax,SUM(visiting_charges) as visiting_charges");
        $cart_totals = $this->my_model->get_data_row("services_cart");
        $businees_profile = $this->my_model->get_data_row("users_bussiness_details", array("user_id" => USER_ID, 'status' => 1));
        if ($has_visit_and_quote != 1) {
            if (empty($businees_profile)) {
                $this->db->order_by("id", "desc");
            	$this->db->where("expiry_date >=", time());
                $user_membership_data = $this->my_model->get_data_row("membership_transactions", array("user_id" => USER_ID));
                if (!empty($user_membership_data)) {
                    $membership_discount = (float) ($cart_totals->sub_total / 100) * $user_membership_data->membership_discount;
                    if ($membership_discount > $user_membership_data->max_discount) {
                        $membership_discount = $user_membership_data->max_discount;
                    }
                }
            } else {
                $business__benifits = $this->my_model->get_data_row("business_profile_benifits");
                $discount_percentage = !empty($business__benifits) ? $business__benifits->services_discount : 0;
                if ($discount_percentage > 0) {
                    $business_discount = floor((float) ($cart_totals->sub_total / 100) * $discount_percentage);
                }
            }
        }
        $complete_discount = $membership_discount + $business_discount;
        $cart_totals->total = (float) $cart_totals->total - $complete_discount;
//            $cart_totals->sub_total = (float) $cart_totals->total - ($cart_totals->visiting_charges + $cart_totals->tax);
        $cart_totals->sub_total = (float) $cart_totals->sub_total;
        $cart_totals->visiting_charges = (float) $cart_totals->visiting_charges;
        $cart_totals->tax = (float) $cart_totals->tax;
        $cart_totals->membership_discount = $membership_discount;
        $cart_totals->business_discount = $business_discount;
        $wallet_amount = $user_data->wallet_amount;
        if ($has_visit_and_quote == 1) {
            $cart_totals->sub_total = $cart_totals->visiting_charges;
            $cart_totals->total = $cart_totals->visiting_charges + $cart_totals->tax;
        }
        $this->data['cart_totals'] = $cart_totals;
        $this->data['cart_totals']->total = (round($this->data['cart_totals']->total, 2));
        $this->data['cart_totals']->sub_total = (round($this->data['cart_totals']->sub_total, 2));
        $this->data['cart_totals']->tax = (round($this->data['cart_totals']->tax, 2));
        $this->data['cart_totals']->visiting_charges = (round($this->data['cart_totals']->visiting_charges, 2));
        $this->data['cart_totals']->business_discount = (round($business_discount, 2));
        $this->data['cart_totals']->membership_discount = (round($membership_discount, 2));
        return $this->data['cart_totals'];
    }

    function get_cart_totals_mob($has_visit_and_quote = 0, $user_id) {
        $user_data = $this->check_user_id($user_id);
        $this->db->where("user_id", $user_id);
        $this->db->select("SUM(grand_total) as total, SUM(sub_total)as sub_total, SUM(tax) as tax,SUM(visiting_charges) as visiting_charges");
        $cart_totals = $this->my_model->get_data_row("services_cart");
        $businees_profile = $this->my_model->get_data_row("users_bussiness_details", array("user_id" => $user_id, 'status' => 1));
        if ($has_visit_and_quote != 1) {
            if (empty($businees_profile)) {
                $this->db->order_by("id", "desc");
            	$this->db->where("expiry_date >=", time());
                $user_membership_data = $this->my_model->get_data_row("membership_transactions", array("user_id" => $user_id));
                if (!empty($user_membership_data)) {
                    $membership_discount = (float) ($cart_totals->sub_total / 100) * $user_membership_data->membership_discount;
                    if ($membership_discount > $user_membership_data->max_discount) {
                        $membership_discount = $user_membership_data->max_discount;
                    }
                }
            } else {
                $business__benifits = $this->my_model->get_data_row("business_profile_benifits");
                $discount_percentage = !empty($business__benifits) ? $business__benifits->services_discount : 0;
                if ($discount_percentage > 0) {
                    $business_discount = floor((float) ($cart_totals->sub_total / 100) * $discount_percentage);
                }
            }
        }
        $complete_discount = $membership_discount + $business_discount;
        $cart_totals->total = (float) $cart_totals->total - $complete_discount;
//            $cart_totals->sub_total = (float) $cart_totals->total - ($cart_totals->visiting_charges + $cart_totals->tax);
        $cart_totals->sub_total = (float) $cart_totals->sub_total;
        $cart_totals->visiting_charges = (float) $cart_totals->visiting_charges;
        $cart_totals->tax = (float) $cart_totals->tax;
        $cart_totals->membership_discount = $membership_discount;
        $cart_totals->business_discount = $business_discount;
        $wallet_amount = $user_data->wallet_amount;
        if ($has_visit_and_quote == 1) {
            $cart_totals->sub_total = $cart_totals->visiting_charges;
            $cart_totals->total = $cart_totals->visiting_charges + $cart_totals->tax;
        }
        $cart_totals->total = (round($cart_totals->total, 2));
        $cart_totals->sub_total = (round($cart_totals->sub_total, 2));
        $cart_totals->tax = (round($cart_totals->tax, 2));
        $cart_totals->visiting_charges = (round($cart_totals->visiting_charges, 2));
        $cart_totals->business_discount = (round($business_discount, 2));
        $cart_totals->membership_discount = (round($membership_discount, 2));
        return $cart_totals;
    }

}
