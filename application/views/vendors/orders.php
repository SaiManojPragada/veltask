<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Orders</h5>
                    <div class="ibox-tools">
                            <a href="<?= base_url() ?>vendors/dashboard">
                                <button class="btn btn-primary">BACK</button>
                            </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            
                            <tr>
                                <th>#</th>
                                <th>Order ID</th>
                                <th>User Details</th>
                                <th>Delivery address</th>
                                <th>Payment Option</th>
                                <th>Payment Status</th>
                                <th>Order Status</th>
                                <th>Order Amount</th>
                                <!-- <th>Admin Comission</th> -->
                                <th>Delivery Details</th>
                                <th>Created Date</th>
                                <th>Vendor Price</th>
                                <th>Coupon Code</th>
                                <th>Action</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i=1;
                            if(count($orders)>0)
                            {
                            foreach($orders as $ord){
                                $user = $this->db->query("select * from users where id='".$ord->user_id."'");
                                $users = $user->row();
                                
                                $ads = $this->db->query("select * from user_address where id='".$ord->deliveryaddress_id."'");
                                $address = $ads->row();
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $ord->id; ?></td>
                                <td>
                                    <?php if($user->num_rows()>0){?>
                                    <b>Name : </b><?php echo $users->first_name." ".$users->last_name; ?><br>
                                    <b>Email : </b><?php echo $users->email; ?><br>
                                    <b>Phone : </b><?php echo $users->phone; ?>
                                    <?php } ?>
                                </td>

                                <td><?php echo $ord->user_address; ?>
                                    <!-- <?php echo $address->locality; ?>,<br>
                                    <?php echo $address->city; ?>,<br>
                                    <?php echo $address->state; ?>,<br>
                                    <?php echo $address->pincode; ?>,<br>
                                    <?php if($address->address_type==1){ echo "Home"; }else if($address->address_type==2){ echo "Office"; }else if($address->address_type==3){ echo "Others"; } ?><br> -->
                                </td>
                                
                                <td><?php echo $ord->payment_option; ?></td>
                                <td><?php if($ord->payment_status==1){ echo "Paid"; }else{ echo "Unpaid"; } ?></td>
                                <td><?php if($ord->order_status==1){ echo "Pending"; }else if($ord->order_status==2){ echo "Proccessing"; }else if($ord->order_status==3){ echo "Assigned to delivery to pick up"; }else if($ord->order_status==4){ echo "Delivery Boy On the way"; }else if($ord->order_status==5){ echo "Delivered"; }else if($ord->order_status==6){ echo "Cancelled"; }
                                    ?>
                                    

                                        <?php if($ord->order_status==1){ ?>
                                        <select name="status" onchange="updateStatus(this.value,'<?php echo $ord->id; ?>')">
                                            <option selected="" >Pending</option>
                                            <option value="2" <?php if($ord->order_status==2){ echo "selected='selected'"; }?>>Accept SELF Delivery</option>
                                             <option value="2" <?php if($ord->order_status==2){ echo "selected='selected'"; }?>>Accept and Assign Rocket Wheel Delivery</option>
                                             <option value="6" <?php if($ord->order_status==6){ echo "selected='selected'"; }?>>Cancel</option>
                                         </select>
                                        <?php }else if($ord->order_status==2 && $ord->accept_status==1){ ?>
                                            <select name="status" onchange="updateStatus(this.value,'<?php echo $ord->id; ?>')">
                                                
                                        
                                             <option value="2" <?php if($ord->order_status==2){ echo "selected='selected'"; }?>>Accept SELF Delivery</option>
                                            <option value="5" <?php if($ord->order_status==5){ echo "selected='selected'"; }?>>Delivered</option>
                                            
                                        </select>
                                        <?php } ?>


                                        
                                   
                                </td>
                                <td><?php echo $ord->total_price; ?></td>
                                <!-- <td><?php 
                                            $cart_qry = $this->db->query("select * from cart where session_id='".$ord->session_id."'");
                                            $cart_report = $cart_qry->result();
                                            $new_width=0;
                                            foreach ($cart_report as $value) 
                                            {
                                                $variant_id = $value->variant_id;
                                                $lv_qry = $this->db->query("select * from link_variant where id='".$variant_id."'");
                                                $lv_report = $lv_qry->row();

                                                $pro_qry = $this->db->query("select * from products where id='".$lv_report->product_id."'");
                                                $pro_report = $pro_qry->row();

                                                $adm_com = $this->db->query("select * from admin_comissions where id='".$pro_report->cat_id."'");
                                                $admin_comsn = $adm_com->row();
                                                $percentage = $admin_comsn->admin_comission;
                                                $totalWidth = $value->unit_price;

                                                $new_width += ($percentage / 100) * $totalWidth; 
                                                
                                            }
                                            echo $new_width;
                                    ?></td> -->
                                <td>

                                    
                                    

                                   
                                  <?php  $deli = $this->db->query("select * from deliveryboy where id='".$ord->delivery_boy."'");
                                    if($deli->num_rows()>0)
                                    {
                                        $delivery_person = $deli->row();
                                        
                                     ?><br>
                                     <b>Name: </b><?php echo $delivery_person->name; ?>
                                    <b>Commission: </b> <?php echo $ord->deliveryboy_commission; ?>
                                <?php }else{ 
                                             $vendor_qry = $this->db->query("select * from vendor_shop where id='".$ord->vendor_id."'");
                                             $ven_row = $vendor_qry->row();
                                    ?>

                                        <p> <b>Name: </b><?php echo $ven_row->shop_name;  ?></p>
                                         <p> <b>Commission: </b><?php echo $ord->deliveryboy_commission; ?></p>

                                <?php } ?>


                                   <!--  <?php if($ord->delivery_boy==0){?>
                               <a href="<?php echo base_url(); ?>vendors/orders/delivery/<?php echo $ord->session_id; ?>"><button class="btn btn-xs btn-info"><i class="fa fa-eye" aria-hidden="true"></i>  Assign Delivery Boy </button></a>
                                    <?php } ?> -->
                                
                                </td>
                                <td><?php echo date("d M,Y",$ord->created_at);?></td>
                                 <td>

                                            <?php
                                            $cart_qry = $this->db->query("select * from cart where session_id='" . $ord->session_id . "'");
                                            $cart_result = $cart_qry->result();
                                            $admin_total = 0;
                                            $unit_price = 0;
                                            foreach ($cart_result as $cart_value) {
                                                $link_qry = $this->db->query("select * from link_variant where id='" . $cart_value->variant_id . "'");
                                                $link_row = $link_qry->row();

                                                $prod_qry = $this->db->query("select * from products where id='" . $link_row->product_id . "'");
                                                $prod_row = $prod_qry->row();

                                                $cat_id = $prod_row->cat_id;
                                                $sub_cat_id = $prod_row->sub_cat_id;
                                                $cart_vendor_id = $cart_value->vendor_id;
                                                $adminc_qry = $this->db->query("select * from admin_comissions where shop_id='" . $cart_vendor_id . "' and cat_id='" . $cat_id . "' and find_in_set('" . $sub_cat_id . "',subcategory_ids)");
                                                $adminc_row = $adminc_qry->row();
                                                $cat_qry = $this->db->query("select * from categories where id='" . $cat_id . "'");
                                                $cat_row = $cat_qry->row();

                                                $scat_qry = $this->db->query("select * from sub_categories where id='" . $sub_cat_id . "'");
                                                $scat_row = $scat_qry->row();

                                                if ($adminc_row->admin_comission != '') {
                                                    $admin_comission = $adminc_row->admin_comission;
                                                } else {
                                                    $admin_comission = 0;
                                                }
                                                ?>


                                                <?php
                                                $percentage = ($cart_value->unit_price / 100) * $admin_comission;
                                                $admin_total = $percentage + $admin_total;
                                                $unit_price = $cart_value->unit_price + $unit_price;
                                            }


                                            if ($ord->coupon_id == 0) {
                                                $coupon_disount = "0";
                                                $sub_t = $unit_price;
                                                $totala = $sub_t;
                                            } else {
                                                $cash_coponqry = $this->db->query("select * from cash_coupons where coupon_code='" . $ord->coupon_code . "'");
                                                if ($cash_coponqry->num_rows() > 0) {
                                                    $coupon_disount = "0";
                                                    $sub_t = $unit_price;
                                                    $totala = $sub_t;
                                                } else {
                                                    $coponqry = $this->db->query("select * from coupon_codes where coupon_code='" . $ord->coupon_code . "'");
                                                    $couonrow = $coponqry->row();
                                                    if ($couonrow->shop_id == 0) {
                                                        $coupon_disount = 0;

                                                        $sub_t = $unit_price - $coupon_disount;

                                                        $totala = $sub_t;
                                                    } else {
                                                        $coupon_disount = $ord->coupon_disount;

                                                        $sub_t = $unit_price - $coupon_disount;

                                                        $totala = $sub_t;
                                                    }
                                                }
                                            }

                                            //$totala = $unit_price+$value->deliveryboy_commission;
                                            if ($ord->bid_id == 0) {

                                                echo $totala - $admin_total;
                                            } else {
                                                $vendor_id = $ord->vendor_id;
                                                $ven_bids = $this->db->query("select * from vendor_bids where bid_id='" . $ord->bid_id . "' and vendor_id='" . $vendor_id . "'");
                                                $vendor_bid_row = $ven_bids->row();

                                                $bid_percentage = ($ord->total_price / 100) * $vendor_bid_row->admin_commission;

                                                // echo $vendor_bid_row->admin_commission;
                                                ?>


                                                <p><?php echo $totala = $ord->total_price - $bid_percentage; ?></p>



                                            <?php }
                                            ?></td>
                                <td>
                                        <?php 
                                        if($ord->coupon_id!=0)
                                        {?>
                                            <p><b>Coupon Code : </b><?php echo $ord->coupon_code; ?></p>
                                            <p><b>Coupon Discount : </b><?php echo $ord->coupon_disount." Rs"; ?></p>
                                       <?php  } ?>
                                </td>
                                <td><a href="<?php echo base_url(); ?>vendors/orders/orderDetails/<?php echo $ord->session_id; ?>">
                                                <button class="btn btn-xs btn-info"><i class="fa fa-eye" aria-hidden="true"></i>  View </button>
                                            </a></td>
                            </tr>
                            <?php $i++; } }else{?>
                            <tr>
                                <td colspan="8" style="text-align: center">
                                    <h4>No Orders Found</h4>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">
    
    function updateStatus(value,order_id)
    {
            if(value != '')
            {
             $.ajax({
              url:"<?php echo base_url(); ?>/vendors/orders/changeStatus",
              method:"POST",
              data:{value:value,order_id:order_id},
              success:function(data)
              {
               if(data=='success')
               {
                alert("status changed successfully");
                window.location.href = "<?php echo base_url(); ?>vendors/orders";
               }
              }
             });
            }
    }

</script> 


