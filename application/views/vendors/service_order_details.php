<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Orders <?= ($order->has_visit_and_quote == 'Yes') ? "<span style='color: orange'>(Visit And Quote Order)</span>" : "" ?></h5>
                    <a class="btn btn-sm btn-success pull-right" onclick="history.back();"><i class="fa fa-caret-left"></i> Back</a>
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-content">

                    <div class="col-lg-12">
                        <div class="col-lg-6">

                            <b>Customer Name :</b> <?= $order->address->name ?>,<br>
                            <b>Phone Number :</b> <?= $order->address->mobile ?>,<br>
                            <b>Email :</b> <?= $order->customer_details->email ?>,<br>
                            <b>Address :</b> <?= $order->address->address ?>, <?= $order->address->state_name ?>,
                            <?= $order->address->city_name ?>, <?= $order->address->pincode_pin ?><br>
                        </div>
                        <div class="col-lg-6">
                            <b>Payment Status :</b> <?= $order->payment_status ?>,<br>
                            <b>Order ID : </b> <?= $order->order_id ?>,<br>
                            <b>Order Created Date : </b><?= date('d M Y, h:i A', $order->created_at) ?>,<br>
                            <?php if ($order->accepted_at) { ?>
                                <b>Order Accepted Date : </b><?= date('d M Y, h:i A', $order->accepted_at) ?>,<br>
                            <?php } ?>
                            <?php if ($order->rejected_at) { ?>
                                <b>Order Rejected Date : </b><?= date('d M Y, h:i A', $order->rejected_at) ?>,<br>
                            <?php } ?>
                            <?php if ($order->started_at) { ?>
                                <b>Order Started Date : </b><?= date('d M Y, h:i A', $order->started_at) ?>,<br>
                            <?php } ?>
                            <?php if ($order->completed_at) { ?>
                                <b>Order Completed Date : </b><?= date('d M Y, h:i A', $order->completed_at) ?>,<br>
                            <?php } ?>
                            <?php if ($order->cancelled_at) { ?>
                                <b>Order Cancelled Date : </b><?= date('d M Y, h:i A', $order->cancelled_at) ?>,<br>
                            <?php } ?>
                            <?php if ($order->refunded_at) { ?>
                                <b>Order Refunded Date : </b><?= date('d M Y, h:i A', $order->refunded_at) ?>,<br>
                            <?php } ?>
                            <b>Payment Type : </b><?= $order->payment_type ?>,<br>
                            <b>Order Status : </b><?= ucwords(str_replace("_", " ", $order->order_status)) ?>
                        </div>
                        <div class="col-md-12"  style="float: right; text-align: right">
                            <?php if ($order->has_visit_and_quote == "Yes" && ($order->order_status == "order_accepted" || $order->order_status == "order_started")) { ?>
                                <button class="btn btn-info btn-xs" data-toggle="modal" style="margin: 3px 10px" data-target="#milestonesModal" onclick="quotation('<?= $order->id ?>');">Send / View Quotation</button>
                            <?php } ?>
                            <?php if ($order->has_visit_and_quote == "No" && $order->order_status == "order_started") { ?>
                                <a href="<?= base_url('vendors/order_addons/services/' . $order->id) ?>" style="margin: 3px 10px" target="_blank"><button class="btn btn-warning btn-xs"  style="float: right"><i class="fa fa-plus"></i> Add / Manage Add-ons</button></a>
                            <?php } ?>
                            <?php if ($order->order_status == 'order_placed') { ?>
                                <a class="btn btn-xs btn-primary" style="margin: 3px 10px" onclick="if (confirm('Are you sure want to accept this Order ?')) {
                                                location.href = '<?= base_url('vendors/services_orders/accept/' . $order->id) ?>';
                                            }">Accept Order</a>
                               <?php } ?>
                               <?php if ($order->order_status == 'order_accepted' && $order->has_visit_and_quote == 'No') { ?>
                                <a class="btn btn-xs btn-primary" style="margin: 3px 10px" onclick="start_order('<?= $order->id ?>')">Start Order</a>
                            <?php } else if ($order->has_visit_and_quote == "Yes" && $order->has_quotation == 1) { ?>
                                <a class="btn btn-xs btn-primary" style="margin: 3px 10px" onclick="start_order('<?= $order->id ?>')">Start Order</a> 
                            <?php } ?>
                            <?php if ($order->order_status == 'order_started') { ?>
                                <a class="btn btn-xs btn-primary" style="margin: 3px 10px" onclick="send_otp('<?= $order->id ?>')">Complete Order</a>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="modal fade" tabindex="-1" id="complete_order_model" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h2 class="modal-title">Enter OTP
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button></h2>
                                </div>
                                <div class="modal-body">
                                    <p>An <b>OTP</b> has been sent to the Customer, Please Enter it here to Complete the Order. </p>
                                </div>
                                <form action="" method="post">
                                    <div class="form-group" style="padding: 0px 30px 40px 30px">
                                        <label class="control-label">Enter OTP</label>
                                        <input type="hidden" id="complete_order_id"/>
                                        <input type="text" id="complete_order_otp" style="text-align: center" class="form-control" maxlength="4" onkeydown='return (event.which >= 48 && event.which <= 57) || event.which == 8 || event.which == 46 || (event.which >= 96 && event.which <= 105)'/>
                                        <br>
                                        <a href="javascript:void(0);" onclick="send_otp_ajax(order_id);">Resend OTP</a>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" onclick="complete_order()">Complete Order</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Item Details</th>
                            </tr>
                            <tr>
                                <th>#</th>
                                <th>Service Title</th>
                                <?php if ($order->has_visit_and_quote !== 'Yes') { ?>
                                    <th>Quantity</th>
                                    <th>Service Price</th>
    <!--                                <th>Tax</th>
                                    <th>Visiting Charges</th>-->
                                    <th>Amount</th>
                                <?php } else { ?>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>

                            <?php foreach ($order->order_items as $index => $item) { ?>
                                <tr>
                                    <td><?= $index + 1 ?></td>
                                    <td><?= $item->service_name ?> <?= ($item->is_addon) ? " <small style='color: tomato'>(Add-on) </small>" : "" ?> 
                                        <?php if ($item->is_addon && $item->is_paid == 0) { ?>
                                            &nbsp;&nbsp;&nbsp;<button class="btn btn-danger btn-xs" onclick="deleteAddon('<?= $item->service_id ?>')"><i class="fa fa-trash-o"></i></button>
                                        <?php } ?>
                                    </td>
                                    <?php if ($order->has_visit_and_quote !== 'Yes') { ?>
                                        <td><?= $item->quantity ?></td>
                                        <td><?= ($item->sub_total / $item->quantity) ?></td>
        <!--                                    <td><?= $item->tax ?></td>
                                        <td><?= $item->visiting_charges ?></td>-->
                                        <td><?= ($item->sub_total) ?></td>
                                    <?php } else { ?>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    <?php } ?>
                                </tr>
                            <?php } ?>

                            <?php if ($order->has_visit_and_quote !== 'Yes') { ?>
                                <tr>
                                    <td >
                                    </td>
                                    <td >
                                    </td>
                                    <td >
                                    </td>
    <!--                                <td >

                                    </td>
                                    <td >

                                    </td>-->

                                    <td >
                                        <b>Sub Total</b>
                                    </td>
                                    <td >
                                        ₹<?php echo $order->sub_total; ?>
                                    </td>


                                </tr>
                            <?php } ?>

                            <?php if ($order->has_visit_and_quote !== 'Yes') { ?>
                                <tr>
                                    <td >
                                    </td>
                                    <td >
                                    </td>
                                    <td >
                                    </td>
    <!--                                <td >

                                    </td>
                                    <td >

                                    </td>-->

                                    <td >
                                        <b>Coupon Code : <?= ($order->coupon_code) ? $order->coupon_code : 'N/A'; ?> </b>
                                    </td>
                                    <td >
                                        <span style="color: red;">- ₹ <?= $order->coupon_discount ?></span>
                                    </td>


                                </tr>

                            <?php } ?>


                            <tr>
                                <td >
                                </td>
                                <td >
                                </td>
                                <td >
                                </td>
<!--                                <td >

                                </td>
                                <td >

                                </td>-->
                                <td >
                                    <b>Visiting Charges </b>
                                </td>
                                <td >
                                    <span style="color: green;">+ ₹<?php echo $order->visiting_charges; ?></span>
                                </td>
                            </tr>

                            <tr>
                                <td >
                                </td>
                                <td >
                                </td>
                                <td >
                                </td>
<!--                                <td >

                                </td>
                                <td >

                                </td>-->
                                <td >
                                    <b>Tax</b>
                                </td>
                                <td >
                                    <span style="color: green;">+ ₹<?php echo $order->tax; ?></span>
                                </td> 


                            </tr>

                            <?php if ($order->has_visit_and_quote !== 'Yes') { ?>
                                <tr>
                                    <td >
                                    </td>
                                    <td >
                                    </td>
    <!--                                <td >
                                    </td>
                                    <td >

                                    </td>-->
                                    <td >

                                    </td>
                                    <td >
                                        <b>Business Discount</b>
                                    </td>
                                    <td >
                                        <span style="color: tomato;">- ₹<?php echo $order->bussiness_discount; ?></span>
                                    </td> 


                                </tr>

                            <?php } ?>
                            <?php if ($order->used_wallet_amount > 0) { ?>
                                <tr>
                                    <td >
                                    </td>
                                    <td >
                                    </td>
    <!--                                <td >
                                    </td>
                                    <td >

                                    </td>-->
                                    <td >

                                    </td>
                                    <td >
                                        <b>Used Wallet Amount</b>
                                    </td>
                                    <td >
                                        <span style="color: tomato;">- ₹<?php echo $order->used_wallet_amount; ?></span>
                                    </td> 


                                </tr>

                            <?php } ?>
                            <?php if ($order->has_visit_and_quote !== 'Yes') { ?>
                                <tr>
                                    <td >
                                    </td>
                                    <td >
                                    </td>
    <!--                                <td >
                                    </td>
                                    <td >

                                    </td>-->
                                    <td >

                                    </td>
                                    <td >
                                        <b>Membership Discount</b>
                                    </td>
                                    <td >
                                        <span style="color: tomato;">- ₹<?php echo $order->membership_discount; ?></span>
                                    </td> 


                                </tr>

                            <?php } ?>
                            <?php if ($order->has_visit_and_quote == 'Yes' && $quotation->no_of_milestones_paid > 0) { ?>
                                <tr>
                                    <td >
                                    </td>
                                    <td >
                                    </td>
    <!--                                <td >
                                    </td>
                                    <td >

                                    </td>-->
                                    <td >

                                    </td>
                                    <td >
                                        <b>Discount</b>
                                    </td>
                                    <td >
                                        <span style="color: tomato;">- ₹<?php echo $order->discount; ?></span>
                                    </td> 


                                </tr>

                            <?php } ?>
                            <tr>
                                <td >
                                </td>
                                <td >
                                </td>
<!--                                <td >
                                </td>
                                <td >

                                </td>-->
                                <td >

                                </td>
                                <td >
                                    <b>Grand Total </b>
                                </td>
                                <td >
                                    <b>₹<?php echo $order->grand_total; ?></b>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                </td>
                                <td >
                                </td>
<!--                                <td >
                                </td>
                                <td >

                                </td>-->
                                <td >

                                </td>
                                <td >
                                    <b>Amount Paid </b>
                                </td>
                                <td >
                                    <b>₹<?php echo $order->amount_paid; ?></b>
                                </td>
                            </tr>

                            <tr>
                                <td >
                                </td>
                                <td >
                                </td>
<!--                                <td >
                                </td>
                                <td >

                                </td>-->
                                <td >

                                </td>
                                <td >
                                    <b>Balance Amount </b>
                                </td>
                                <td >
                                    <b>₹<?php echo $order->balance_amount; ?></b>
                                </td> 
                            </tr>
                            <tr>
                                <td colspan="5"></td>
                            </tr>
                            <tr>
                                <th colspan="5">Payout<th>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <small>All these values will be updated once the Order is Completed</small>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Total Amount : </b>
                                </td>
                                <td>
                                    <i class="fa fa-inr"></i> <?= ($order->has_visit_and_quote != "Yes") ? $order->sub_total + $order->visiting_charges + $order->tax : $order->amount_paid ?>
                                </td>
                            </tr>
<!--                            <tr>
                                <td>
                                    <b>Admin Commission : </b><small> (Commission Formula: Admin Commission - (business discount + membership discount + coupon discount))</small>
                                </td>
                                <td>

                            <?php if ($order->admin_comission) { ?>
                                                                                                                                                                            <table>
                                                                                                                                                                                <tr>
                                                                                                                                                                                    <th>Admin Commission : </th>
                                                                                                                                                                                    <td>&nbsp;<?= $order->admin_comission + $order->bussiness_discount + $order->membership_discount + $order->coupon_discount ?></td>
                                                                                                                                                                                </tr>
                                                                                                                                                                                <tr>
                                                                                                                                                                                    <th>Business Discount : </th>
                                                                                                                                                                                    <td>&nbsp;<?= $order->bussiness_discount ?></td>
                                                                                                                                                                                </tr>

                                                                                                                                                                                <tr>
                                                                                                                                                                                    <th>Membership Discount : </th>
                                                                                                                                                                                    <td>&nbsp;<?= $order->membership_discount ?></td>
                                                                                                                                                                                </tr>
                                                                                                                                                                                <tr>
                                                                                                                                                                                    <th>Coupon Discount : </th>
                                                                                                                                                                                    <td>&nbsp;<?= $order->coupon_discount ?></td>
                                                                                                                                                                                </tr>
                                                                                                                                                                                <tr>
                                                                                                                                                                                    <th>Admin Final Commission : </th>
                                                                                                                                                                                    <td>&nbsp;<?= ($order->admin_comission) ? $order->admin_comission : "N/A" ?></td>
                                                                                                                                                                                </tr>
                                                                                                                                                                            </table>
                            <?php } else { ?>
                                                                                                                                                                            N/A
                            <?php } ?>

                                </td>
                            </tr>-->
<!--                            <tr>
                                <td>
                                    <b>Franchise Commission : </b>
                                </td>
                                <td>
                            <?= ($order->franchise_comission) ? $order->franchise_comission : "N/A" ?>
                                </td>
                            </tr>-->
                            <tr>
                                <td>
                                    <!--<b>Service Provider Amount : </b>-->
                                    <b>Your Earning : </b>
                                </td>
                                <td>
                                    <i class="fa fa-inr"></i> <?= ($order->service_provider_comission) ? $order->service_provider_comission : "N/A" ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Gst : </b>
                                </td>
                                <td>
                                    <i class="fa fa-inr"></i> <?= ($order->gst) ? $order->gst : "N/A" ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>

<script>
    function deleteAddon(service_id) {
        if (confirm('Are you sure want to delete this Addon ? ')) {
            $.ajax({
                url: "<?= base_url('api/service_provider/addon_services/add_update_quantity') ?>",
                type: "post",
                data: {user_id: '<?= $vendor_id ?>', service_id: service_id, order_id: '<?= $order->order_id ?>', quantity: 0},
                success: function (resp) {
                    resp = JSON.parse(resp);
                    if (resp['status']) {
                        swal({
                            title: resp['message'],
                            icon: "success"
                        }).then(function () {
                            location.href = "";
                        });
                    }
                }
            });
        }
    }
    function send_otp(order_id) {
        if (confirm('Are you sure want to Complete this Service ?')) {
            $.ajax({
                url: "<?= base_url('api/service_provider/services/get_balance_amount') ?>",
                type: "post",
                data: {order_id: order_id},
                success: function (resp) {
                    resp = JSON.parse(resp);
                    if (resp['status']) {
                        var balance_amount = parseFloat(resp['data']['balance_amount']);
                        console.log(balance_amount);
                        if (balance_amount > 0) {
                            if (confirm('Did you collected the Balance Amount of Rs.' + balance_amount)) {
                                send_otp_ajax(order_id);
                            }
                        } else {
                            send_otp_ajax(order_id);
                        }
                    } else {
                        send_otp_ajax(order_id);
                    }
                }
            });

        }
    }

    function send_otp_ajax(order_id) {
        $.ajax({
            url: '<?= base_url("api/service_provider/services/send_otp_for_completing_order") ?>',
            type: "POST",
            data: {user_id: '<?= $vendor_id ?>', order_id: order_id},
            success: function (json) {
                var resp = JSON.parse(json);
                if (resp['status']) {
                    swal({
                        title: resp['message'],
                        text: "An OTP has been sent to the customer Please enter that OTP to complete this Order.",
                        icon: 'success'
                    }).then(function () {
                        $("#complete_order_id").val(order_id);
                        $("#complete_order_model").modal('show');
                        setTimeout(function () {
                            $("#complete_order_otp").focus();
                        }, 800);
                    });
                } else {
                    swal({
                        title: resp['message'],
                        icon: 'info'
                    });
                }
            }
        });
    }

    function complete_order() {
        var order_id = $("#complete_order_id").val();
        var otp = $("#complete_order_otp").val();
        $.ajax({
            url: '<?= base_url("api/service_provider/services/complete_order") ?>',
            type: "POST",
            data: {user_id: '<?= $vendor_id ?>', order_id: order_id, otp: otp},
            success: function (json) {
                var resp = JSON.parse(json);
                console.log(resp);
                if (resp['status']) {
                    swal({
                        title: resp['message'],
                        icon: 'success'
                    }).then(function () {
                        location.href = "";
                    });
                } else {
                    swal({
                        title: resp['message'],
                        icon: 'info'
                    }).then(function () {
                        setTimeout(function () {
                            $("#complete_order_otp").focus();
                        }, 800);
                    });
                }
            }
        });

    }
    function start_order(order_id) {
        if (confirm('Are you sure want to start this Service ?')) {
            $.ajax({
                url: '<?= base_url("api/service_provider/services/start_service") ?>',
                type: "POST",
                data: {user_id: '<?= $vendor_id ?>', order_id: order_id},
                success: function (json) {
                    var resp = JSON.parse(json);
                    if (resp['status']) {
                        swal({
                            title: resp['message'],
                            icon: 'success'
                        }).then(function () {
                            location.href = "";
                        });
                    } else {
                        swal({
                            title: resp['message'],
                            icon: 'info'
                        });
                    }
                }
            });
        }
    }

</script>