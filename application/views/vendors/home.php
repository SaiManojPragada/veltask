

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>My Dashboard</h5>
            </div>
            <div class="ibox-content">

                <?php if ($this->session->userdata('vendors')['vendor_type'] === 'services') { ?>
                    <div class="row" >
                        <div class="col-md-12">
                            <h4>Hi, <?= $this->session->userdata('vendors')['owner_name'] ?>,
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Your Role :</b>  <?= $this->session->userdata('vendors')['provider_type'] ?></h4>
                        </div>
                    </div>
                <?php } ?>
                <?php if ($this->session->userdata('vendors')['vendor_type'] === 'ecomm') { ?>
                    <div class="row" >
                        <div class="col-md-12">
                            <h2>Today Stats</h2><hr>
                        </div>

                        <!-- <div class="col-md-3">
                            <div class="widget style1 blue-bg">
                                <div class="row">
                                    <div class="col-xs-12 text-center">
                                        <span> Products </span>
                        <?php
                        $qry_t = $this->db->query("select * from products where shop_id='" . $_SESSION['vendors']['vendor_id'] . "'");
                        $num_rows_toa = $qry_t->num_rows();
                        ?>
                                        <h2 class="font-bold"><?= $num_rows_toa ?></h2>
                                    </div>
                                </div>
                            </div>
                        </div> -->

                        <div class="col-md-3">
                            <div class="widget style1 blue-bg">
                                <div class="row">
                                    <a style="color: #FFF;" href="<?php echo base_url(); ?>vendors/categories"><div class="col-xs-12 text-center">
                                            <span> Categories </span>
                                            <h2 class="font-bold"><?= $total_category ?></h2>
                                        </div></a>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-3">
                            <div class="widget style1 blue-bg">
                                <div class="row">
                                    <a style="color: #FFF;" href="<?php echo base_url(); ?>vendors/orders"><div class="col-xs-12 text-center">
                                            <span> Today Orders </span>
                                            <h2 class="font-bold"><?= $today_orders ?></h2>
                                        </div></a>
                                </div>
                            </div>
                        </div>



                        <div class="col-md-3">
                            <div class="widget style1 blue-bg">
                                <div class="row">
                                    <a style="color: #FFF;" href="<?php echo base_url(); ?>vendors/orders"><div class="col-xs-12 text-center">
                                            <span>Orders <i class="fa fa-bell"></i></span>
                                            <h2 class="font-bold"><?= $total_orders ?></h2>
                                        </div></a>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-3">
                            <div class="widget style1 blue-bg">
                                <div class="row">
                                    <a style="color: #FFF;" href="<?php echo base_url(); ?>vendors/request_payment"><div class="col-xs-12 text-center">
                                            <span>Total Payouts </span>
                                            <?php
                                            if ($total_payment != '') {
                                                $total_payment1 = $total_payment;
                                            } else {
                                                $total_payment1 = 0;
                                            }
                                            ?>
                                            <h2 class="font-bold">₹<?= $total_payment1 ?></h2>
                                        </div></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="widget style1 blue-bg">
                                <div class="row">
                                    <a style="color: #FFF;" href="<?php echo base_url(); ?>vendors/request_payment"><div class="col-xs-12 text-center">
                                            <span>Pending Payouts </span>
                                            <h2 class="font-bold">₹<?= $pending ?></h2>
                                        </div></a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="widget style1 blue-bg">
                                <div class="row">
                                    <a style="color: #FFF;" href="<?php echo base_url(); ?>vendors/products"><div class="col-xs-12 text-center">
                                            <span> Active Products </span>

                                            <?php
                                            $qry = $this->db->query("select * from products where status=1 and delete_status=0 and shop_id='" . $_SESSION['vendors']['vendor_id'] . "'");
                                            $num_rows = $qry->num_rows();
                                            ?>
                                            <h2 class="font-bold"><?= $num_rows ?></h2>
                                        </div></a>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-3">
                            <div class="widget style1 blue-bg">
                                <div class="row">
                                    <a style="color: #FFF;" href="<?php echo base_url(); ?>vendors/inactive_products"><div class="col-xs-12 text-center">
                                            <span> Inactive Products </span>
                                            <?php
                                            $qry1 = $this->db->query("select * from products where status=0 and delete_status=0 and shop_id='" . $_SESSION['vendors']['vendor_id'] . "'");
                                            $num_rows1 = $qry1->num_rows();
                                            ?>
                                            <h2 class="font-bold"><?= $num_rows1 ?></h2>
                                        </div></a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="widget style1 blue-bg">
                                <div class="row">
                                    <a style="color: #FFF;"><div class="col-xs-12 text-center">
                                            <span> Total Viewers </span>
                                            <?php
                                            $visit_qry = $this->db->query("select * from shop_visit where shop_id='" . $_SESSION['vendors']['vendor_id'] . "' group by user_id");
                                            $visitor_count = $visit_qry->num_rows();
                                            ?>
                                            <h2 class="font-bold"><?= $visitor_count ?></h2>
                                        </div></a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="widget style1 blue-bg">
                                <div class="row">
                                    <a style="color: #FFF;" href="<?php echo base_url(); ?>vendors/bids/openbids"><div class="col-xs-12 text-center">
                                            <span> Bids </span>

                                            <h2 class="font-bold"><?= $bid_count ?></h2>
                                        </div></a>
                                </div>
                            </div>
                        </div>



                    </div>


                <?php } else if ($this->session->userdata('vendors')['vendor_type'] === 'services') { ?>
                    <div class="ibox-content">
                        <div class="row">
                            <?php if ($this->session->userdata('vendors')['provider_type'] === 'Owner') { ?>
                                <div class="col-md-6" onclick="location.href = '<?= base_url('vendors/manage_technicians') ?>'">
                                    <div class="widget style1 blue-bg">
                                        <div class="row">
                                            <a style="color: #FFF;" href="<?= base_url('vendors/manage_technicians') ?>"><div class="col-xs-12 text-center">
                                                    <span> Total Technicians </span>
                                                    <?php
                                                    $this->db->where("owner_id", $this->session->userdata('vendors')['vendor_id']);
                                                    $techs = $this->db->get("service_providers");
                                                    $techs_count = $techs->num_rows();
                                                    ?>
                                                    <h2 class="font-bold"><?= $techs_count ?></h2>
                                                </div></a>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="col-md-6">
                                <div class="widget style1 blue-bg">
                                    <div class="row">
                                        <a style="color: #FFF;" href="<?= base_url('vendors/services_orders') ?>"><div class="col-xs-12 text-center">
                                                <span> My Total Orders </span>

                                                <h2 class="font-bold"><?= sizeof($orders) ?></h2>
                                            </div></a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="widget style1 blue-bg">
                                    <div class="row">
                                        <a style="color: #FFF;" href="javascript:void(0);"><div class="col-xs-12 text-center">
                                                <span> New Orders </span>
                                                <h2 class="font-bold"><?= ($today_orders) ? sizeof($today_orders) : "0" ?></h2>
                                            </div></a>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="widget style1 blue-bg">
                                    <div class="row">
                                        <a style="color: #FFF;" href="javascript:void(0);"><div class="col-xs-12 text-center">
                                                <span> Upcoming Orders </span>

                                                <h2 class="font-bold"><?= sizeof($upcoming_orders) ?></h2>
                                            </div></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <h2 style="font-weight: 400">New Orders</h2>
                                <hr>
                                <div class="table-responsive">
                                    <table class="table table-striped dataTables-example dataTable dtr-inline collapsed">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Order Id</th>                                
                                                <th>Order Services Details</th>                   
                                                <th>Customer Details</th>                          
                                                <th>Total</th>                          
                                                <th>Order Time Slot</th>             
                                                <th>Booked On</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($today_orders as $key => $item) { ?>
                                                <tr>
                                                    <td><?= $key + 1 ?></td>
                                                    <td><?= $item->order_id ?></td>
                                                    <td>
                                                        <?php foreach ($item->order_items as $index => $sers) { ?>
                                                            <table class="table table-striped dataTable dtr-inline">
                                                                <thead>
                                                                    <tr>
                                                                        <th>S.No</th>
                                                                        <th>Service Details</th>
                                                                        <th>Amount</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td><?= $index + 1 ?></td>
                                                                        <td>
                                                                            <p><b>Service Name : </b><?= $sers->service_details->service_name ?></p>
                                                                        </td>
                                                                        <td><?= $sers->sub_total ?></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        <?php } ?>
                                                    </td>
                                                    <td>

                                                        <p><b>Name : </b><?= $item->customer_details->first_name ?></p>
                                                        <p><b>Mobile Number : </b><?= $item->customer_address->mobile ?></p>
                                                        <p><b>Address : </b><?= $item->customer_address->address ?></p>
                                                        <p><b>Landmark : </b><?= $item->customer_address->landmark ?></p>
                                                    </td>
                                                    <td>Rs. <?= $item->grand_total ?></td>
                                                    <td><?= $item->time_slot ?></td>
                                                    <td><?= $item->ordered_on ?></td>
                                                </tr>                                            
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>

            </div>
        </div>
    </div>
</div>



</div>
