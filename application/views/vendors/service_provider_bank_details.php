<style>
    .cat_image{
        width: 100px;
        height: 100px;
        object-fit: scale-down;
        border-radius: 10px;
        margin: 0px 5px;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $title ?></h5>
                    <div class="ibox-tools">
                        <a href="<?= $_SERVER['HTTP_REFERER'] ?>">
                            <button class="btn btn-primary">BACK</button>
                        </a>
                    </div>
                    <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                        <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                        </div>
                    <?php } ?>
                    <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                        <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                        </div>
                    <?php }
                    ?>
                </div>
                <div class="ibox-content">
                    <form method="post" autocomplete="false" class="form-horizontal" enctype="multipart/form-data"  action="<?= base_url() ?>vendors/service_provider_bank_details/update/<?= $vendor_id ?>">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Bank Name *</label>
                            <div class="col-sm-10">
                                <input type="text" name="bank_name" class="form-control" id="bank_name" placeholder="Enter Bank Name" value="<?= $data->bank_name ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Account Number *</label>
                            <div class="col-sm-10">
                                <input type="number" name="account_number" min="1" class="form-control" id="account_number" placeholder="Enter Account Number" value="<?= $data->account_number ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">IFSC Code *</label>
                            <div class="col-sm-10">
                                <input type="text" name="ifsc_code" class="form-control" id="ifsc_code" placeholder="Enter IFSC Code"  value="<?= $data->ifsc_code ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Branch Name *</label>
                            <div class="col-sm-10">
                                <input type="text" name="branch_name" class="form-control" id="branch_name" placeholder="Enter Branch Name"  value="<?= $data->branch_name ?>">
                            </div>
                        </div>


<div class="form-group">
                            <label class="col-sm-2 control-label">Passbook Image </label>
                            <div class="col-sm-10">
                                <input type="file" name="bank_image" class="form-control" id="bank_image" placeholder="Select Bank Image">
                            </div>
                        </div>


<?php if(!empty($data->bank_image)){?>
	<div class="form-group">
                            <label class="col-sm-2 control-label">Prev Image</label>
                            <div class="col-sm-10">
                                <img src="<?= base_url('uploads/service_providers/').$data->bank_image ?>" style="max-width: 250px;">
                            </div>
                        </div>
<?php } ?>

                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label"></label>
                    <div class="col-sm-10">
                        <button class="btn btn-primary" type="submit" id="btn_category"> <i class="fa fa-plus-circle"></i> Submit</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
    $('#btn_category').click(function () {
        $('.error').remove();
        var errr = 0;
        if ($('#bank_name').val() == '')
        {
            $('#bank_name').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Bank Name </span>');
            $('#bank_name').focus();
            return false;
        } else if ($('#account_number').val() == '')
        {
            $('#account_number').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Account Number </span>');
            $('#account_number').focus();
            return false;
        } else if ($('#ifsc_code').val() == '')
        {
            $('#ifsc_code').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter IFSC Code</span>');
            $('#ifsc_code').focus();
            return false;
        } else if ($('#branch_name').val() == '')
        {
            $('#branch_name').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Branch Name</span>');
            $('#branch_name').focus();
            return false;
        }

    });
</script>