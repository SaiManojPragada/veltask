<style>
    .cat_image{
        width: 100px;
        height: 100px;
        object-fit: scale-down;
        border-radius: 10px;
        margin: 0px 5px;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $title ?></h5>
                    <div class="ibox-tools">
                        <a href="<?= base_url() ?>vendors/dashboard">
                            <button class="btn btn-primary">BACK</button>
                        </a>
                        <a href="<?= base_url() ?>vendors/manage_technicians/add">
                            <button class="btn btn-primary">+ Add Technician</button>
                        </a>
                    </div>
                    <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                        <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                        </div>
                    <?php } ?>
                    <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                        <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                        </div>
                    <?php }
                    ?>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Details</th>
                                    <th>Location</th>
                                    <th>Categories</th>
                                    <th>Updated At</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $count = 1;
                                foreach ($technicians as $item) {
                                    ?>
                                    <tr>
                                        <td><?= $count++ ?></td>
                                        <td>
                                            <p><b>Name : </b><?= $item->name ?></p>
                                            <p><b>Email : </b><?= $item->email ?></p>
                                            <p><b>Phone : </b><?= $item->phone ?></p>
                                        </td>
                                        <td><?= $item->location_name ?></td>
                                        <td><?= $item->categories_names ?></td>
                                        <td><?= date('d m Y h:i A', $item->updated_at) ?></td>
                                        <td><?= $item->status ? '<font color="green">Active</font>' : '<font color="red">Inactive</font>' ?></td>
                                        <td>

                                            <?php
                                            if ($user_type == 'subadmin') {
                                                if (in_array("edit_category", $permissions)) {
                                                    ?>

                                                    <a href="<?= base_url() ?>vendors/manage_technicians/edit/<?= $item->id ?>">
                                                        <button title="This operation is disabled in demo !" class="btn btn-xs btn-primary">
                                                            Edit
                                                        </button>
                                                    </a>
                                                <?php } if (in_array("delete_category", $permissions)) { ?>
                                                    <a href="<?= base_url() ?>vendors/manage_technicians/delete/<?= $item->id ?>">
                                                        <button title="Delete Category" class="btn btn-xs btn-danger" onclick="if (!confirm('Are you sure you want to delete this Category?'))
                                                                    return false;">
                                                            Delete
                                                        </button>
                                                    </a>
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <a href="<?= base_url() ?>vendors/manage_technicians/edit/<?= $item->id ?>">
                                                    <button title="This operation is disabled in demo !" class="btn btn-xs btn-primary">
                                                        Edit
                                                    </button>
                                                </a>

                                                <a href="<?= base_url() ?>vendors/manage_technicians/delete/<?= $item->id ?>">
                                                    <button title="Delete Category" class="btn btn-xs btn-danger" onclick="if (!confirm('Are you sure you want to delete this Category?'))
                                                                return false;">
                                                        Delete
                                                    </button>
                                                </a>

                                            <?php } ?>
                                            <br>

                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Details</th>
                                    <th>Location</th>
                                    <th>Categories</th>
                                    <th>Updated At</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>