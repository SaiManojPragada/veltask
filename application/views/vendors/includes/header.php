<!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Veltask | Dashboard</title>

        <link href="<?= ADMIN_ASSETS_PATH ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?= ADMIN_ASSETS_PATH ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">

        <!-- Toastr style -->
        <link href="<?= ADMIN_ASSETS_PATH ?>assets/css/plugins/toastr/toastr.min.css" rel="stylesheet">

        <!-- Gritter -->
        <link href="<?= ADMIN_ASSETS_PATH ?>assets/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

        <link href="<?= ADMIN_ASSETS_PATH ?>assets/css/animate.css" rel="stylesheet">
        <link href="<?= ADMIN_ASSETS_PATH ?>assets/css/style.css" rel="stylesheet">
        <link href="<?= ADMIN_ASSETS_PATH ?>assets/css/custom_css.css" rel="stylesheet">
        <link href="<?= ADMIN_ASSETS_PATH ?>assets/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

        <script src="<?= ADMIN_ASSETS_PATH ?>assets/js/jquery-2.1.1.js"></script>


        <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.css">
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    </head>
    <!-- <body oncopy="return false" oncut="return false" onpaste="return false" oncontextmenu="return false;"> -->
    <body >
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav metismenu" id="side-menu">
                        <li class="nav-header">
                            <div class="dropdown profile-element">
                                <span>
                                         <!--<img alt="image" class="img-circle" src="<?= ADMIN_ASSETS_PATH ?>assets/img/profile_small.jpg" />-->
                                </span>
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                    <span class="clear">
                                        <span class="block m-t-xs">
                                            <strong class="font-bold">Veltask</strong>
                                        </span>
                                        <span class="text-muted text-xs block">Vendor Control Panel <b class="caret"></b>
                                        </span>
                                    </span>
                                </a>
                                <!-- <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                    <li><a href="<?= base_url() ?>vendors/settings">Settings</a></li>
                                    <li><a href="#">Database Backup</a></li>
                                    <li><a href="#">Login Logs</a></li>
                                    <li><a href="#">SMS Gateway Settings</a></li>
                                    <li><a href="#">Change Password</a></li>
                                    <li class="divider"></li>
                                    <li><a href="<?= base_url() ?>vendors/logout">Logout</a></li>
                                </ul> -->
                            </div>
                            <div class="logo-element">
                                VT
                            </div>
                        </li>
                        <li class="<?php
                        if ($page_name == 'dashboard') {
                            echo 'active';
                        }
                        ?>">
                            <a href="<?= base_url() ?>vendors/dashboard"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a>
                        </li>
                        <?php if ($this->session->userdata('vendors')['vendor_type'] === 'ecomm') { ?>

                            <li class="<?php
                            if ($page_name == 'settings') {
                                echo 'active';
                            }
                            ?>">
                                <a href="<?= base_url() ?>vendors/settings"><i class="fa fa-th-large"></i> <span class="nav-label">Profile</span></a>
                            </li>
                            <li class="<?php
                            if ($page_name == 'banners') {
                                echo 'active';
                            }
                            ?>">
                                <a href="<?= base_url() ?>vendors/banners"><i class="fa fa-th-large"></i> <span class="nav-label">Banners</span></a>
                            </li>

                            <!-- <li class="<?php
                            if ($page_name == 'banneradds') {
                                echo 'active';
                            }
                            ?>">
                                <a href="<?= base_url() ?>vendors/banneradds"><i class="fa fa-user"></i> <span class="nav-label">Banner AD's</span></a>
                            </li> -->

                            <li class="<?php
                            if ($page_name == 'categories') {
                                echo 'active';
                            }
                            ?>">
                                <a href="<?= base_url() ?>vendors/categories"><i class="fa fa-user"></i> <span class="nav-label">Categories</span></a>
                            </li>
                            <!-- <li class="<?php
                            if ($page_name == 'sub_categories') {
                                echo 'active';
                            }
                            ?>">
                                <a href="<?= base_url() ?>vendors/sub_categories"><i class="fa fa-user"></i> <span class="nav-label">Sub Categories</span></a>
                            </li> -->


                            <li class="<?php
                            if ($page_name == 'products' || $page_name == 'inactive_products') {
                                echo 'active';
                            }
                            ?>">
                                <a><i class="fa fa-table"></i> Products
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-second-level collapse">
                                    <li  class="<?= $page_name == 'products' ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>vendors/products">Active Products</a>
                                    </li>
                                    <li class="<?= $page_name == 'inactive_products' ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>vendors/inactive_products">Inactive Products</a>
                                    </li>
                                </ul>
                            </li>


                            <!-- <li class="<?php
                            if ($page_name == 'products') {
                                echo 'active';
                            }
                            ?>">
                                <a href="<?= base_url() ?>vendors/products"><i class="fa fa-th-large"></i> <span class="nav-label">Products</span></a>
                            </li> -->
                            <li class="<?php
                            if ($page_name == 'request_payment') {
                                echo 'active';
                            }
                            ?>">
                                <a href="<?= base_url() ?>vendors/request_payment"><i class="fa fa-th-large"></i> <span class="nav-label">Request Payment</span></a>
                            </li>



                            <!-- 
                                                    <li class="<?php
                            if ($page_name == 'cities' || $page_name == 'locations' || $page_name == 'pincodes') {
                                echo 'active';
                            }
                            ?>">
                                                                <a>Location Management
                                                                    <span class="fa arrow"></span>
                                                                </a>
                                                                <ul class="nav nav-second-level collapse">
                                                                         <li  class="<?= $page_name == 'states' ? 'active' : '' ?>">
                                                                            <a href="<?= base_url() ?>vendors/states">States</a>
                                                                        </li>
                                                                        <li class="<?= $page_name == 'cities' ? 'active' : '' ?>">
                                                                            <a href="<?= base_url() ?>vendors/cities">Cities</a>
                                                                        </li> 
                                                                        <li class="<?= $page_name == 'pincodes' ? 'active' : '' ?>">
                                                                            <a href="<?= base_url() ?>vendors/pincodes">Pincode</a>
                                                                        </li>
                                                                        <li class="<?= $page_name == 'locations' ? 'active' : '' ?>">
                                                                            <a href="<?= base_url() ?>vendors/locations">Areas</a>
                                                                        </li>
                                                                </ul>
                                                            </li> -->



                            <li class="<?php
                            if ($page_name == 'orders') {
                                echo 'active';
                            }
                            ?>">
                                <a href="<?= base_url() ?>vendors/orders"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Orders</span></a>
                            </li>
                            <!-- <li class="<?php
                            if ($page_name == 'businesshours') {
                                echo 'active';
                            }
                            ?>">
                                <a href="<?= base_url() ?>vendors/businesshours"><i class="fa fa-clock-o"></i><span class="nav-label">Business Hours</span></a>
                            </li> -->
                            <!-- <li>
                                <a href="<?= base_url() ?>/vendors/settlements"><i class="fa fa fa-money"></i> <span class="nav-label">Settlements</span></a>
                            </li> -->
                            <!-- <li class="<?php
                            if ($page_name == 'users') {
                                echo 'active';
                            }
                            ?>">
                                <a href="<?= base_url() ?>vendors/users"><i class="fa fa-user"></i> <span class="nav-label">Users</span></a>
                            </li> -->

                            <li class="<?php
                            if ($page_name == 'coupons') {
                                echo 'active';
                            }
                            ?>">
                                <a href="<?= base_url() ?>vendors/coupons"><i class="fa fa-user"></i> <span class="nav-label">Coupon Management</span></a>
                            </li>


                           


                        <?php } else if ($this->session->userdata('vendors')['vendor_type'] === 'services') { ?>


                            <?php if ($this->session->userdata('vendors')['provider_type'] === 'Owner') { ?>
                                <li class="<?php
                                if ($page_name == 'technicians') {
                                    echo 'active';
                                }
                                ?>">
                                    <a href="<?= base_url() ?>vendors/manage_technicians"><i class="fa fa-users"></i> <span class="nav-label">Manage Technicians</span></a>
                                </li>

                            <?php } ?>

                            <li class="<?php
                            if ($page_name == 'services_orders') {
                                echo 'active';
                            }
                            ?>">
                                <a href="<?= base_url() ?>vendors/services_orders"><i class="fa fa-file"></i> <span class="nav-label">Orders</span></a>
                            </li>

                            <li class="<?php
                            if ($page_name == 'switch_services') {
                                echo 'active';
                            }
                            ?>">
                                <a href="<?= base_url() ?>vendors/switch_services"><i class="fa fa-fax"></i> <span class="nav-label">Manage Services</span></a>
                            </li>

                            <li class="<?php
                            if ($page_name == 'service_provider_profile') {
                                echo 'active';
                            }
                            ?>">
                                <a href="<?= base_url() ?>vendors/service_provider_profile"><i class="fa fa-user"></i> <span class="nav-label">Profile</span></a>
                            </li>

                            <li class="<?php
                            if ($page_name == 'service_provider_bank_details') {
                                echo 'active';
                            }
                            ?>">
                                <a href="<?= base_url() ?>vendors/service_provider_bank_details"><i class="fa fa-building-o"></i> <span class="nav-label">Bank Details</span></a>
                            </li>

                            <li class="<?php
                            if ($page_name == 'service_provider_payouts') {
                                echo 'active';
                            }
                            ?>">
                                <a href="<?= base_url() ?>vendors/service_provider_payouts"><i class="fa fa-money"></i> <span class="nav-label">Payouts</span></a>
                            </li>

                            <li class="<?php
                            if ($page_name == 'change_password') {
                                echo 'active';
                            }
                            ?>">
                                <a href="<?= base_url() ?>vendors/services_change_password"><i class="fa fa-lock"></i> <span class="nav-label">Change Password</span></a>
                            </li>
                        <?php } ?>


                        <!-- <li>
                            <a href="#"><i class="fa fa-book"></i> <span class="nav-label">CMS Pages</span><span class="fa arrow"></span></a>
                        </li> -->

                </div>
            </nav>

            <div id="page-wrapper" class="gray-bg dashbard-1">
                <div class="row border-bottom">
                    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                        <div class="navbar-header">
                            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                            <!-- <form role="search" class="navbar-form-custom" action="search_results.html">
                                <div class="form-group">
                                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                                </div>
                            </form> -->
                        </div>
                        <ul class="nav navbar-top-links navbar-right">
                            <li>
                                <span class="m-r-sm text-muted welcome-message">Welcome to <?php
                                    if ($_SESSION['vendors']['owner_name'] != '') {
                                        echo $_SESSION['vendors']['owner_name'];
                                    }
                                    ?>.</span>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>vendors/logout">
                                    <i class="fa fa-sign-out"></i> Log out
                                </a>
                            </li>
                        </ul>

                    </nav>