
<div class="modal fade" tabindex="-1" role="dialog" id="milestonesModal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"><strong>Add/View Quotation</strong></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="quotation_form" class="row" style="margin-bottom: 30px; padding-bottom: 30px; border-bottom: 1px solid lightgray">
                    <div class="col-md-12 form-group">
                        <label class="control-label" for="project_details">Project Details / Scope *</label>
                        <textarea rows="5" class="form-control" id="project_details" placeholder="enter Project Details / Scope"></textarea>
                    </div>
                    <div class="col-md-6 form-group">
                        <label class="control-label" for="work_duration">Approx Work Duration *</label>
                        <input type="work_duration" class="form-control" id="work_duration" placeholder="enter Approx Work Duration">
                    </div>
                    <div class="col-md-6 form-group">
                        <label class="control-label" for="quotation_amount">Quotation Amount *</label>
                        <input type="number" min="1" class="form-control" id="quotation_amount" placeholder="enter quotation amount" onkeyup="make_milestones()">
                    </div>
                    <div class="col-md-12 form-group" id="no_of_milestones_div">
                        <label class="control-label" for="no_of_milestones">Number of Milestones</label>
                        <div class="input-group">
                            <div class="rating-css col-12" style="float: left; text-align: left">
                                <div class="star-icon" id="star-icon" >
                                    <input type="radio" name="rating" onchange="changeMilestones(this.value)" id="rating1" value="1" checked>
                                    <label for="rating">1</label>
                                    <input type="radio" name="rating" onchange="changeMilestones(this.value)" id="rating2" value="2">
                                    <label for="rating2">2</label>
                                    <input type="radio" name="rating" onchange="changeMilestones(this.value)" id="rating3" value="3">
                                    <label for="rating3">3</label>
                                    <input type="radio" name="rating" onchange="changeMilestones(this.value)" id="rating4" value="4">
                                    <label for="rating4">4</label>
                                    <input type="radio" name="rating" onchange="changeMilestones(this.value)" id="rating5" value="5">
                                    <label for="rating5">5</label>
                                </div>
                            </div>
                        </div>
                        <style>
                            .star-icon label{
                                background-color: #273A4A;
                                color: #eaeaea;
                                padding: 10px 20px;
                                border-radius: 100px;

                            }
                            .rating-css {
                                text-align: center;
                                padding: 5px 20px;
                            }
                            .rating-css input {
                                display: none;
                            }
                            .rating-css input + label {
                                font-size:22px;
                                cursor: pointer;
                                text-align: center
                            }
                            .rating-css input:checked + label ~ label {
                                color: #eaeaea;
                                background-color: gray;
                            }
                            .rating-css label:active {
                                transform: scale(0.8);
                                transition: 0.3s ease;
                            }




                        </style>
                    </div>
                </div>
                <div id="milesones_form">
                    <h4>Milestones List <strong>(<span id="no_of_milestones_count_span"></span>) <small>The First Milestone has a deduction price from the Visiting Charges</small></strong></h4>
                    <div class="row" id="milestones_display_div">

                    </div>
                    <div class="row" id="add_milestone_div">
                        <div class="col-md-12">
                            <button class="btn btn-warning" style="float: right !important;" onclick="add_milestone()"><i class="fa fa-plus"></i> Add Milestone</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success pull-right" style="margin-left: 12px" id="update_milestones_button" onclick="submit_milestones()">Save Milestones</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    var no_of_milestones = 1;
    var service_order_id = "";
    var is_update = false;
    var quotation_id_master = "";
    function changeMilestones(val) {
        no_of_milestones = val;
        make_milestones();
    }
    function tallyAmount() {
        var amounts = document.getElementsByClassName("milestone_amount");
        var total = 0;
        for (var i = 0; i < amounts.length; i++) {
            console.log(curr);
            var curr = $(amounts[i]).val();
            if (curr.length < 1) {
                curr = 0;
            }
            total += parseFloat(curr);
        }
        $("#quotation_amount").val(total);
    }
    function make_milestones() {
        var amount = $("#quotation_amount").val();
        if (quotation_id_master != "") {
            var milestones = document.getElementsByClassName("milestone_amount");
            var milestones_size = milestones.length;
            var part = amount / milestones_size;
            part = part.toFixed(2);
            for (var i = 0; i < milestones_size; i++) {
                $(milestones[i]).val(part);
            }
        } else {
            $("#no_of_milestones_count_span").html(no_of_milestones);
            console.log(amount);
            var part = amount / no_of_milestones;
            part = part.toFixed(2);
            var op = "";
            $("#milestones_display_div").html(op);
            for (var i = 0; i < no_of_milestones; i++) {
                var date_value = "";
                var read_only = "";
                if (i === 0) {
                    date_value = '<?= date('Y-m-d') ?>';
                    read_only = "readonly";
                }
                op += '<div class="card milestones_card" style="padding: 20px">' +
                        '<div class="row">' +
                        '<div class="col-md-12"><center> <h3>Milestone ' + (i + 1) + '</h3> <b style="color: tomato; float: right">Unpaid</b></center></div>' +
                        '<div class="col-md-6">' +
                        '<label class="control-label" for="milestone_amount' + i + '">Milestone Amount</label>' +
                        '<input type="number" class="form-control milestone_amount" onkeyup="tallyAmount()" id="milestone_amount' + i + '" placeholder="enter milestone amount" value="' + part + '">' +
                        '</div>' +
                        '<div class="col-md-6">' +
                        '<label class="control-label" for="milestone_date' + i + '">Milestone Date</label>' +
                        '<input type="date" min="<?= date('Y-m-d') ?>" class="form-control" id="milestone_date' + i + '" placeholder="select milestone date" value="' + date_value + '" ' + read_only + '>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<hr>';
            }
            $("#milestones_display_div").html(op);
        }

    }

    function submit_milestones() {
        $('.int-err').remove();
        var project_details = $("#project_details").val();
        if (project_details.length < 2) {
            $("#project_details").after("<p style='color: red' class='int-err'>Please enter valid project details</p>");
            return false;
        }
        var work_duration = $("#work_duration").val();
        if (work_duration.length < 2) {
            $("#work_duration").after("<p style='color: red' class='int-err'>Please enter valid work duration</p>");
            return false;
        }
        var quotation_amount = $("#quotation_amount").val();
        if (quotation_amount.length < 2) {
            $("#quotation_amount").after("<p style='color: red' class='int-err'>Please enter valid quotation amount</p>");
            return false;
        }
        var milestones_size = document.getElementsByClassName("milestones_card").length;
        var json = "[";
        for (var i = 0; i < milestones_size; i++) {
            var curr_amt = $("#milestone_amount" + i).val();
            var curr_date = $("#milestone_date" + i).val();
            if (curr_amt.length < 1) {
                $("#milestone_amount" + i).after("<p style='color: red' class='int-err'>Please enter valid Amount</p>");
                return false;
            }
            if (curr_date.length < 1) {
                $("#milestone_date" + i).after("<p style='color: red' class='int-err'>Please select valid Date</p>");
                return false;
            }
            var current = "{";
            if (is_update) {
                current += '"id":"' + $("#id" + i).val() + '",';
            }
            current += '"amount":"' + curr_amt + '",';
            current += '"date":"' + curr_date + '"},';
            json += current;
        }
        json = json.substring(0, json.length - 1);
        json += "]";
//        console.log(JSON.parse(json));
//        return false;
        var aj_data = {user_id: '<?= $vendor_id ?>', service_order_id: service_order_id,
            project_details: project_details, work_duration: work_duration,
            quotation_amount: quotation_amount, no_of_milestones: no_of_milestones, milestones_json: json};
        var url_suffix = "post_milestones";
        if (is_update || quotation_id_master != "") {
            aj_data.quotation_id = quotation_id_master;
            url_suffix = "update_milestones";
        }
        $.ajax({
            url: "<?= base_url() ?>api/service_provider/quotation_and_milestones/" + url_suffix,
            type: "post",
            data: aj_data,
            success: function (resp) {
                resp = JSON.parse(resp);
                if (resp['status']) {
                    swal({
                        title: resp['message'],
                        icon: 'success'
                    }).then(function () {
                        var data = resp['data'];
                        quotation_id_master = data['quotation_id'];
                        console.log(quotation_id_master);
                        get_milestones(quotation_id_master.toString());
                        location.href = "";
                    });
                } else {
                    swal({
                        title: resp['message'],
                        icon: 'warning'
                    });
                }
            }
        });
    }
    function deleteMilestone(milestone_id, quotation_id, order_id) {
        if (confirm("Are you sure want to delete this Milestone ? ")) {
            $.ajax({
                url: "<?= base_url('api/admin_ajax/admin/delete_milestone') ?>",
                type: "post",
                data: {milestone_id: milestone_id, quotation_id: quotation_id, order_id: order_id},
                success: function (resp) {
//                    resp = JSON.parse(resp);
                    if (resp['status']) {
                        swal({
                            title: resp['message'],
                            icon: "success"
                        }).then(function () {
                            quotation(order_id);
                            get_milestones(quotation_id);
                            location.href = '';
                        });
                    } else {
                        swal({
                            title: resp['message'],
                            icon: "warning"
                        });
                    }
                }
            });
        }
    }

    function quotation(id) {
        quotation_id_master = "";
        is_update = false;
        $("#rating1").attr("checked", "");
        $("#no_of_milestones_div").css({display: 'block'});
        $("#no_of_milestones_count_span").html('0');
        service_order_id = id;
        $.ajax({
            url: "<?= base_url() ?>api/service_provider/visit_and_quote/get_quotation",
            type: "post",
            data: {user_id: '<?= $vendor_id ?>', order_id: service_order_id},
            success: function (resp) {
                resp = JSON.parse(resp);
                if (resp['status']) {
                    is_update = true;
                    var data = resp['data'];
                    quotation_id_master = data['id'];
                    get_milestones(quotation_id_master);
                    $("#no_of_milestones_div").css({display: 'none'});
                    $("#add_milestone_div").css({display: 'block'});
                    $("#project_details").val(data['project_details']);
                    $("#work_duration").val(data['work_duration']);
                    $("#quotation_amount").val(data['quotation_amount']);
                    $("#rating" + data['no_of_milestones']).attr("checked", "");
                } else {
                    $("#add_milestone_div").css({display: 'none'});
                }
            }
        });
    }
    function get_milestones(quotation_id, last = "") {
        quotation_id_master = quotation_id;
        $.ajax({
            url: "<?= base_url() ?>api/service_provider/visit_and_quote/get_milestones",
            type: "post",
            data: {user_id: '<?= $vendor_id ?>', quotation_id: quotation_id_master},
            success: function (resp) {
                $("#milesones_form").css({display: "block"});
                resp = JSON.parse(resp);
                console.log(resp);
                if (resp['status']) {
                    $("#add_milestone_div").css({display: "block"});
                    var data = resp['data'];
                    var op = "";
                    $("#no_of_milestones_div").css({display: 'none'});
                    $("#no_of_milestones_count_span").html(data.length);
                    no_of_milestones = data.length;
                    if (last == "last") {
                        var i = data.length - 1;
                        var item = data[i];
                        var read_only = "";
                        if (i === 0) {
                            read_only = "readonly";
                        }
                        var color = (item['status'] === 'unpaid') ? 'red' : 'green';
                        var disabled = (item['status'] === 'unpaid') ? '' : 'disabled';
                        var delete_btn = "";
                        if (item['status'] !== 'unpaid') {
                            $("#quotation_amount").attr("disabled", "");
                        } else {
                            delete_btn = "<button class='btn btn-danger btn-xs' style='float:right; margin-top: 20px' onclick='deleteMilestone(`" + item['id'] + "`, `" + item['quotation_id'] + "`)'> Delete Milestone</button>"
                        }
                        op += '<div class="card milestones_card" style="padding: 20px">' +
                                '<div class="row">' +
                                '<div class="col-md-12"><center> <h3>Milestone ' + (i + 1) + '</h3> <b style="color: ' + color + '; float: right">' + item['status'] + '</b></center></div>' +
                                '<input type="hidden" id="id' + i + '" value="' + item['id'] + '">' +
                                '<div class="col-md-6">' +
                                '<label class="control-label" for="milestone_amount' + i + '">Milestone Amount</label>' +
                                '<input type="number" class="form-control milestone_amount" onkeyup="tallyAmount()" ' + disabled + ' id="milestone_amount' + i + '" placeholder="enter milestone amount" value="' + item['amount'] + '">' +
                                '</div>' +
                                '<div class="col-md-6">' +
                                '<label class="control-label" for="milestone_date' + i + '">Milestone Date</label>' +
                                '<input type="date" min="<?= date('Y-m-d') ?>" class="form-control" id="milestone_date' + i + '" placeholder="select milestone date" value="' + item['date'] + '" ' + read_only + '>' +
                                '</div>' + delete_btn +
                                '</div>' +
                                '</div>' +
                                '<hr>';
                        $("#milestones_display_div").append(op);
                    } else {
                        for (var i = 0; i < data.length; i++) {
                            var item = data[i];
                            var read_only = "";
                            if (i === 0) {
                                read_only = "readonly";
                            }
                            var color = (item['status'] === 'unpaid') ? 'red' : 'green';
                            var disabled = (item['status'] === 'unpaid') ? '' : 'disabled';
                            var delete_btn = "";
                            if (item['status'] !== 'unpaid') {
                                $("#quotation_amount").attr("disabled", "");
                            } else {
                                delete_btn = "<button class='btn btn-danger btn-xs' style='float:right; margin-top: 20px' onclick='deleteMilestone(`" + item['id'] + "`, `" + item['quotation_id'] + "`)'> Delete Milestone</button>"
                            }
                            op += '<div class="card milestones_card" style="padding: 20px">' +
                                    '<div class="row">' +
                                    '<div class="col-md-12"><center> <h3>Milestone ' + (i + 1) + '</h3> <b style="color: ' + color + '; float: right">' + item['status'] + '</b></center></div>' +
                                    '<input type="hidden" id="id' + i + '" value="' + item['id'] + '">' +
                                    '<div class="col-md-6">' +
                                    '<label class="control-label" for="milestone_amount' + i + '">Milestone Amount</label>' +
                                    '<input type="number" class="form-control milestone_amount" onkeyup="tallyAmount()" ' + disabled + ' id="milestone_amount' + i + '" placeholder="enter milestone amount" value="' + item['amount'] + '">' +
                                    '</div>' +
                                    '<div class="col-md-6">' +
                                    '<label class="control-label" for="milestone_date' + i + '">Milestone Date</label>' +
                                    '<input type="date" min="<?= date('Y-m-d') ?>" class="form-control" id="milestone_date' + i + '" placeholder="select milestone date" value="' + item['date'] + '" ' + read_only + '>' +
                                    '</div>' + delete_btn +
                                    '</div>' +
                                    '</div>' +
                                    '<hr>';
                        }
                        $("#milestones_display_div").html(op);
                    }

                } else {
                    $("#update_milestones_button").css({display: "none"});
                    $("#milestones_display_div").html("<center><h5>-- No Milestones Found --</h5></center>");
                }
            }
        });
    }
    function add_milestone() {
        $.ajax({
            url: "<?= base_url() ?>api/service_provider/visit_and_quote/add_milestone",
            type: "post",
            data: {user_id: '<?= $vendor_id ?>', quotation_id: quotation_id_master},
            success: function (resp) {
                resp = JSON.parse(resp);
                console.log(resp);
                if (resp['status']) {
                    get_milestones(quotation_id_master, "last");
                } else {
                    swal({
                        title: resp['message'],
                        icon: 'warning'
                    });
                }
            }
        });
    }


</script>


<!-- Mainly scripts -->

<script src="<?= ADMIN_ASSETS_PATH ?>assets/js/bootstrap.min.js"></script>
<script src="<?= ADMIN_ASSETS_PATH ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?= ADMIN_ASSETS_PATH ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<script src="<?= ADMIN_ASSETS_PATH ?>assets/js/plugins/dataTables/datatables.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?= ADMIN_ASSETS_PATH ?>assets/js/inspinia.js"></script>
<script src="<?= ADMIN_ASSETS_PATH ?>assets/js/plugins/pace/pace.min.js"></script>

<script>

    $(".chosen-select").chosen();
    $(document).ready(function () {
        $('.dataTables-example').DataTable({
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                //                            {extend: 'copy'},
                //                            {extend: 'csv'},
                //                            {extend: 'excel', title: 'ExampleFile'},
                //                            {extend: 'pdf', title: 'ExampleFile'},

                //                {extend: 'print',
                //                    customize: function (win) {
                //                        $(win.document.body).addClass('white-bg');
                //                        $(win.document.body).css('font-size', '10px');
                //
                //                        $(win.document.body).find('table')
                //                                .addClass('compact')
                //                                .css('font-size', 'inherit');
                //                    }
                //                }
            ]

        });
    });</script>

<link href="<?= ADMIN_ASSETS_PATH ?>assets/js/select2.min.css" rel="stylesheet" /> 
<script src="<?= ADMIN_ASSETS_PATH ?>assets/js/select2.min.js"></script>
<script>
    $(document).ready(function () {
        $('.js-example-basic-multiple').select2({
            placeholder: "Select"
        });
    });</script> 

<script type="text/javascript">
    setTimeout(function () {
        $('.alert-success').fadeOut('fast');
    }, 3000);
</script>

</body>
</html>



