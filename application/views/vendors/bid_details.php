<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Bid Details</h5>
                    <div class="ibox-tools">
                       <p style="color: red; text-align: center;"><b >Admin Commission</b> ( <?php echo $biddetails['admin_commission']; ?>% )</p>
                                
                    </div>
                </div>
                <?php //echo "<pre>"; print_r($biddetails['cart_products']);?>
                
                <div class="ibox-content">
                  <table class="table table-striped">
                    <tr>
                      <th>Image</th>
                      <th>Title</th>
                      <th>Quantity</th>
                      <th>Price</th>
                    </tr>
                    <?php foreach($biddetails['cart_products'] as $prod){ ?>
                    <tr>
                      <td><img src="<?php echo $prod['image']; ?>" style="width:80px; height: 80px;"></td>
                      <td><?php echo $prod['product_name']; ?>
                          <div>
                            <?php foreach($prod['attributes'] as $attr){ ?>
                            <p><b><?php echo $attr['attribute_type']; ?>:</b><?php echo $attr['attribute_values']; ?></p>
                          <?php } ?>
                          </div>

                      </td>
                      <td><?php echo $prod['quantity']; ?></td>
                      <td><?php echo $prod['total']; ?></td>
                    </tr>
                  <?php } ?>
                  </table>
                    <table class="table table-striped">
                            <tr>
                                <th style="width: 50%">Sub Total</th>
                                <td style="width: 50%"><?php echo $biddetails['sub_total']; ?></td>
                            </tr>
                            <tr>
                                <th style="width: 50%">Delivery Amount</th>
                                <td style="width: 50%"><?php echo $biddetails['delivery_amount']; ?></td>
                            </tr>
                            <tr>
                                <th  style="width: 50%">GST</th>
                                <td style="width: 50%"><?php echo $biddetails['gst']; ?></td>
                            </tr>
                            <tr>
                                <th  style="width: 50%">Total Price</th>
                                <td style="width: 50%"><?php echo $total = $biddetails['sub_total']+$biddetails['delivery_amount']+$biddetails['gst']; ?></td>
                            </tr>

                           

                            <tr>
                                 <th style="width: 50%">Your quotation</th>
                                 <td style="width: 50%">
                                     <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                                        <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                            <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                                        </div>
                                    <?php } ?>
                                    <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                                        <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                            <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                                        </div>
                                    <?php } ?>
                                    <?php if($biddetails['quote_status']==true){  echo $biddetails['quote_amount'];  }else if($biddetails['quote_status']==false){ ?>
                                    <form method="post" class="form-horizontal" action="<?= base_url() ?>vendors/bids/updatequote">
                                        <input type="hidden" name="bidid" value="<?php echo $biddetails['id']; ?>">
                                        <input type="hidden" name="user_id" value="<?php echo $biddetails['user_id']; ?>">
                                        <input type="text" id="total_price" name="total_price" onkeypress="return isNumber(event)"><br><br>
                                         <button class="btn btn-primary" id="submit_quote" type="submit">Submit Quote</button>
                                    </form>
                                <?php } ?>
                                </td>
                            </tr>
                            <?php if($biddetails['quote_status']==true){ ?>
                            <!-- <tr>
                                <th>Bid Amount</th>
                                <td><?php echo $biddetails['quote_amount']; ?></td>
                            </tr> -->
                            <?php } ?>
                            

                    </table>
                </div>
            </div>
        </div>
    </div>


</div>
<script type="text/javascript">

  
  $('#submit_quote').click(function(){
        $('.error').remove();
            var errr=0;
            var total = '<?php echo $total; ?>';
      if($('#total_price').val()=='')
      {
         $('#total_price').after('<span class="error" style="color:red;font-size: 18px;margin-left: 18px; display:block;">Enter your Quotation</span>');
         $('#total_price').focus();
         return false;
      }
      else if($('#total_price').val()==0)
      {
         $('#total_price').after('<span class="error" style="color:red;font-size: 18px;margin-left: 18px; display:block;">Please enter minimum price (zero Not accepted)</span>');
         $('#total_price').focus();
         return false;
      }
      else if(parseInt(total)<=parseInt($('#total_price').val()))
      {
         $('#total_price').after('<span class="error" style="color:red;font-size: 18px;margin-left: 18px; display:block;">Please Enter the below total price</span>');
         $('#total_price').focus();
         return false;
      }
  
 });

</script>

<script type="text/javascript">
    function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>

