<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Add On Services</h5>
                </div>
                <div class="ibox-content">
                    <div>
                        <ul class="nav nav-tabs">
                            <?php foreach ($sub_categories as $sub_cat) { ?>
                                <li class="nav-item <?= ($current_sub_id == $sub_cat->id) ? 'active' : '' ?>">
                                    <a class="nav-link" aria-current="page" href="<?= base_url('vendors/order_addons/services/' . $order_real_id . '/?sub_cat_id=' . $sub_cat->id) ?>"><?= $sub_cat->sub_category_name ?></a>
                                </li>
                            <?php } ?>
                        </ul>
                        <?php if (!empty($services)) { ?>
                            <div>
                                <div class="row" style="padding: 25px 50px">
                                    <?php foreach ($services as $index => $serv) { ?>
                                        <div class="card col-md-12 row" style="border-radius: 15px; margin: 10px; box-shadow: 0 3px 10px rgb(0 0 0 / 0.2);">
                                            <div class="col-md-2 container">
                                                <img src="<?= base_url('uploads/services/') . $serv->image ?>" alt="<?= $serv->image ?>" style="width: 100%; object-fit: cover; padding: 10px" />
                                            </div>
                                            <div class="col-md-10" style="padding: 30px">
                                                <p><h3><?= $serv->service_name ?></h3></p>
                                                <p><h2><i class="fa fa-inr"></i> <?= $serv->sale_price ?></h2></p>
                                                <p><i class="fa fa-star" style="color: goldenrod"></i> <?= $serv->rating ?></p>
                                                <?= $serv->description ?>
                                                <div style="right: 30px; bottom: 20px; position: absolute">
                                                    <div class="F8dpS zj0R0 _3L1X9 adddiv<?= $serv->id ?>">
                                                        <div class="_1RPOp addbtn" id="addbtn<?= $serv->id ?>" onclick="show('<?= $serv->id ?>');" style="<?= ($serv->quantity > 0) ? 'display:none' : '' ?>">ADD</div>
                                                        <!--<div id="box1" style="<?= ($serv->quantity < 1) ? 'display: none;' : '' ?>"></div>-->
                                                        <div id="increament-div<?= $serv->id ?>" class="increament-div" style="<?= ($serv->quantity > 0) ? 'display:block' : '' ?>">
                                                            <div id="box" style="display: none;"></div>
                                                            <div onclick="inccreament('<?= $serv->id ?>')" class="_1ds9T _2WdfZ _4aKW6 classList ">+</div>
                                                            <div class="_2zAXs _2quy- _4aKW6" id="quantity<?= $serv->id ?>"><?= $serv->quantity ?></div>
                                                            <div onclick="deccreament('<?= $serv->id ?>')"  class="_29Y5Z _20vNm _4aKW6">-</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div style="margin: 40px 0px">
                                <center><h3> -- No Services Found -- </h3></center>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function show(id) {
        document.getElementById("increament-div" + id).style.display = "block";
        document.getElementById("addbtn" + id).style.display = "none";
        $('#quantity' + id).html(parseInt($('#quantity' + id).html()) + 1);
        call_cart(id);
    }


    function inccreament(id)
    {
        $('#quantity' + id).html(parseInt($('#quantity' + id).html()) + 1);
        call_cart(id);
    }

    function deccreament(id)
    {
        var qq = parseInt($('#quantity' + id).html()) - 1;
        $('#quantity' + id).html(qq);
        call_cart(id);
        if (qq < 1) {
            document.getElementById("increament-div" + id).style.display = "none";
            document.getElementById("addbtn" + id).style.display = "block";
        }

    }
    function call_cart(service_id) {
        var quantity = $('#quantity' + service_id).html();
        $.ajax({
            url: "<?= base_url('api/service_provider/addon_services/add_update_quantity') ?>",
            type: "post",
            data: {user_id: '<?= $vendor_id ?>', service_id: service_id, order_id: '<?= $order_id ?>', quantity: quantity},
            success: function (resp) {
                resp = JSON.parse(resp);
                console.log(resp);
                if (!resp['status']) {
                    swal({
                        title: resp['message'],
                        icon: "warning"
                    });
                } else {

                }
            }
        });
    }

</script> 


<style>
    ._3L1X9 {
        display: inline-block;
        height: 38px;
        width: 73px;
        border: 1px solid #f15f74;
        color: #f15f74;
        font-size: .9rem;
        font-weight: 600;
        line-height: 30px;
        position: relative;
        text-align: center;
        background-color: #fff;
        left: 2px;
        top: 2px;
        border-radius: 7px;
    }
    ._1RPOp {
        color: #f15f74;
        width: 100%;
        height: 100%;
        cursor: pointer;
        margin-top: 4px;
    }

    .increament-div {
        display: none;
    }

    #box, #box1 {
        width: 20px;
        height: 2px;
        background: #ff7503;
        position: absolute;
        top: 35px !important;
        animation: mymove 0.5s infinite;
        animation-timing-function: linear;
    }

    ._1ds9T {
        position: absolute;
        right: 0;
        top: 1px;
        width: 33.33333%;
        display: inline-block;
        opacity: 1;
        transform: translateZ(0);
        cursor: pointer;
        font-size: 137%;
        font-weight: 600;
    }

    ._2zAXs._2quy-._4aKW6 {
        margin-top: 4px;
    }

    ._29Y5Z {
        position: absolute;
        left: 0;
        top: 0;
        width: 33.33333%;
        display: inline-block;
        opacity: 1;
        transform: translateZ(0);
        cursor: pointer;
        font-weight: 600;
        font-size: 1.5rem;
    }
</style>
