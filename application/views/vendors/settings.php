<div class="row">


  <div class="col-lg-12">
    <div class="ibox-title">
                <div class="ibox-tools">
                    <a href="<?= base_url() ?>vendors/dashboard">
                            <button class="btn btn-primary">BACK</button>
                        </a>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                          <table class="table table-striped" style="text-align:center;">
                         <tr>
                            <td colspan="2"><img src="<?php echo base_url(); ?>/uploads/shops/<?php echo $settings->shop_logo; ?>" title="" style="width: 200px;"></td>
                          </tr>
                          <tr>
                            <th>Shop Name</th>
                            <td><?php echo $settings->shop_name; ?></td>
                          </tr>
                          <tr>
                            <th>Your Name</th>
                            <td><?php echo $settings->owner_name; ?></td>
                          </tr>
                          <tr>
                            <th>Mobile</th>
                            <td><?php echo $settings->mobile; ?></td>
                          </tr>
                          <tr>
                            <th>Email</th>
                            <td><?php echo $settings->email; ?></td>
                          </tr>

                          <!-- <tr>
                            <th>Delivery Time</th>
                            <td><?php echo $settings->delivery_time; ?></td>
                          </tr> -->
                           <tr>
                            <th>Delivery Amount</th>
                            <td><?php echo $settings->min_order_amount; ?></td>
                          </tr> 

                          <tr>
                            <th>Pincode</th>
                            <td><?php echo $settings->pincode; ?></td>
                          </tr>


                          <tr>
                            <th>Address</th>
                            <td><?php echo $settings->address; ?></td>
                          </tr>
                          <tr>
                            <th>City</th>
                            <td><?php echo $settings->city; ?></td>
                          </tr>
                          <tr>
                            <th>Pincode</th>
                            <td><?php echo $settings->pincode; ?></td>
                          </tr>
                      </table>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="ibox float-e-margins">

                    <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                        <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                        </div>
                    <?php } ?>
                    <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                        <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                        </div>
                    <?php }
                    ?>
                <form method="post" class="form-horizontal" enctype="multipart/form-data"  action="<?= base_url() ?>vendors/settings/updateVendor">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Shop Name</label>
                        <div class="col-sm-10">
                            <input type="text" name="shop_name" class="form-control" value="<?php echo $settings->shop_name; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Your Name</label>
                        <div class="col-sm-10">
                            <input type="text" name="owner_name" class="form-control" value="<?php echo $settings->owner_name; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Mobile</label>
                        <div class="col-sm-10">
                            <input type="text" name="mobile" class="form-control" value="<?php echo $settings->mobile; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                            <input type="text" name="email" class="form-control" readonly="" value="<?php echo $settings->email; ?>">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">Address</label>
                        <div class="col-sm-10">
                          <textarea name="address" class="form-control"><?php echo $settings->address; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">City</label>
                        <div class="col-sm-10">
                            <input type="text" name="city" class="form-control" value="<?php echo $settings->city; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Pincode</label>
                        <div class="col-sm-10">
                            <input type="text" name="pincode" class="form-control" value="<?php echo $settings->pincode; ?>">
                        </div>
                    </div>

                     <!-- <div class="form-group">
                        <label class="col-sm-2 control-label">Delivery Time</label>
                        <div class="col-sm-10">
                            <input type="text" name="delivery_time" class="form-control" value="<?php echo $settings->delivery_time; ?>">
                        </div>
                    </div> -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Delivery Amount</label>
                        <div class="col-sm-10">
                            <input type="text" name="min_order_amount" class="form-control" value="<?php echo $settings->min_order_amount; ?>">
                        </div>
                    </div>
                    

                    <div class="form-group">
                      <a href="https://www.latlong.net/" target="_blank" style="text-align: center;">Get Latitude and Longitude</a>
                        <label class="col-sm-2 control-label">Latitude</label>
                        <div class="col-sm-10">
                            <input type="text" name="lat" class="form-control" value="<?php echo $settings->lat; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Longitude</label>
                        <div class="col-sm-10">
                            <input type="text" name="lng" class="form-control" value="<?php echo $settings->lng; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Shop Image</label>
                        <div class="col-sm-10">
                            <input type="file" name="webimage" class="form-control">
                            <img src="<?php echo base_url(); ?>/uploads/shops/<?php echo $settings->shop_logo; ?>" title="" style="width:80px; height: 80px; border-radius:50%;">
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Shop Logo</label>
                        <div class="col-sm-10">
                            <input type="file" name="logo" class="form-control">
                            <img src="<?php echo base_url(); ?>/uploads/shops/<?php echo $settings->logo; ?>" title="" style="width:80px; height: 80px; border-radius:50%;">
                        </div>

                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">Select OFFLINE / ONLINE</label>
                        <div class="col-sm-10">
                          <select class="form-control" name="status">
                                   <option value="1" <?php if($settings->status==1){ echo 'selected="selected"'; }?>>ONLINE</option>
                                   <option value="0" <?php if($settings->status==0){ echo 'selected="selected"'; }?>>OFFLINE</option>
                          </select>
                           
                        </div>
                    </div>

                
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" type="submit"> <i class="fa fa-plus-circle"></i> Update</button>
                        </div>
                    </div>
                </form>


                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                         <?php if (!empty($this->session->flashdata('success_message1'))) { ?>
                        <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong> Success!</strong> <?= $this->session->flashdata('success_message1') ?>
                        </div>
                    <?php } ?>
                    <?php if (!empty($this->session->flashdata('error_message1'))) { ?>
                        <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong>Failed!</strong> <?= $this->session->flashdata('error_message1') ?>
                        </div>
                    <?php }
                    ?>
                <h1 style="text-align: center;">Change Password</h1>
                <form method="post" class="form-horizontal" enctype="multipart/form-data"  action="<?= base_url() ?>vendors/settings/changePassword1">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Old Password : </label>
                        <div class="col-sm-10">
                            <input type="text" id="oldpassword" name="oldpassword" class="form-control" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">New Password : </label>
                        <div class="col-sm-10">
                            <input type="text" id="newpassword" name="newpassword" class="form-control" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Confirm Password : </label>
                        <div class="col-sm-10">
                            <input type="text" id="confirm_password" name="confirm_password" class="form-control" value="">
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" type="submit" id="btn_changePassword"> <i class="fa fa-plus-circle"></i> Update</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
            
        </div> 


  
</div>
<script type="text/javascript">

     $('#btn_changePassword').click(function(){
        $('.error').remove();
            var errr=0;
      if($('#oldpassword').val()=='')
      {
         $('#oldpassword').after('<span class="error" style="color:red;font-size: 18px;margin-left: 18px;">Enter Old Password</span>');
         $('#oldpassword').focus();
         return false;
      }
      else if($('#newpassword').val()=='')
      {
         $('#newpassword').after('<span class="error" style="color:red;font-size: 18px;margin-left: 18px;">Enter New Password</span>');
         $('#newpassword').focus();
         return false;
      }
      else if($('#confirm_password').val()=='')
      {
         $('#confirm_password').after('<span class="error" style="color:red;font-size: 18px;margin-left: 18px;">Enter New Confirm Password</span>');
         $('#confirm_password').focus();
         return false;
      }
      else if($('#newpassword').val()!=$('#confirm_password').val())
      {
         $('#confirm_password').after('<span class="error" style="color:red;font-size: 18px;margin-left: 18px;">Password Mismatched</span>');
         $('#confirm_password').focus();
         return false;
      }
 });
     
  setTimeout(function() {
    $('.alert-success').fadeOut('fast');
}, 3000);
</script>