<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Orders</h5>
                    <div class="ibox-tools">
                        <a href="<?= base_url() ?>vendors/dashboard">
                            <button class="btn btn-primary">BACK</button>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                        <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                        </div>
                    <?php } ?>
                    <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                        <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                        </div>
                    <?php } ?>

                    <ul class="nav nav-tabs">
                        <li class="nav-item <?= empty($this->input->get('ft')) ? 'active' : '' ?>">
                            <a class="nav-link" href="<?= base_url('vendors/services_orders') ?>">New Orders</a>
                        </li>
                        <li class="nav-item <?= ($this->input->get('ft') == 'accepted') ? 'active' : '' ?>">
                            <a class="nav-link" href="<?= base_url('vendors/services_orders?ft=accepted') ?>">Accepted Services</a>
                        </li>
                        <li class="nav-item <?= ($this->input->get('ft') == 'started') ? 'active' : '' ?>">
                            <a class="nav-link" href="<?= base_url('vendors/services_orders?ft=started') ?>">Started Services</a>
                        </li>
                        <li class="nav-item <?= ($this->input->get('ft') == 'completed') ? 'active' : '' ?>">
                            <a class="nav-link" href="<?= base_url('vendors/services_orders?ft=completed') ?>">Completed Services</a>
                        </li>
                    </ul>

                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>

                            <tr>
                                <th>#</th>
                                <th>Order ID</th>
                                <th>User Details</th>
                                <th>Delivery address</th>
                                <th>Payment Type</th>
                                <th>Payment Status</th>
                                <th>Order Status</th>
                                <th>Order Amount</th>
                                <th>Is Visit and Quote</th>
                                <th>Ordered Date</th>
                                <th>Time slot Details</th>
                                <th>Action</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($orders as $index => $od) { ?>
                                <tr>
                                    <td><?= $index + 1 ?></td>
                                    <td><?= $od->order_id ?></td>
                                    <td>
                                        <p><b>Name : </b><?= $od->address->name ?></p>
                                        <p><b>Email : </b><?= $od->customer_details->email ?></p>
                                        <p><b>Phone Number : </b><?= $od->address->mobile ?></p>
                                    </td>
                                    <td>
                                        <p><?= ($od->order_status == "order_placed") ? '' : $od->address->address . ',' ?><?= $od->address->state_name . ', <br>' . $od->address->city_name . ', ' . $od->address->pincode_pin ?></p>
                                    </td>
                                    <td><?= $od->payment_type ?></td>
                                    <td><?= $od->payment_status ?></td>
                                    <td><?= $od->order_status ?></td>
                                    <td><?= $od->grand_total ?></td>
                                    <td><span style="color: <?= ($od->has_visit_and_quote == "Yes") ? 'green' : 'orange' ?>"><?= $od->has_visit_and_quote ?></span></td>
                                    <td><?= $od->ordered_on ?></td>
                                    <td><?= date('d M Y', strtotime($od->time_slot_date)) . ', ' . $od->time_slot ?></td>
                                    <td>
                                        <?php if ($od->has_visit_and_quote == "Yes" && ($od->order_status == "order_accepted" || $od->order_status == "order_started")) { ?>
                                            <button class="btn btn-info btn-xs" data-toggle="modal" data-target="#milestonesModal" onclick="quotation('<?= $od->id ?>');">Send / View Quotation</button>
                                        <?php } ?>
                                        <a class="btn btn-xs btn-success" href="<?= base_url('vendors/services_orders/orderDetails/') . $od->id ?>">View Order</a>
                                        <?php if ($od->order_status == 'order_placed') { ?>
                                            <a class="btn btn-xs btn-primary" onclick="if (confirm('Are you sure want to accept this Order ?')) {
                                                        location.href = '<?= base_url('vendors/services_orders/accept/' . $od->id) ?>';
                                                    }">Accept Order</a>
                                           <?php } ?>
                                           <?php if ($od->order_status == 'order_accepted' && $od->has_visit_and_quote == 'No') { ?>
                                            <a class="btn btn-xs btn-primary" onclick="start_order('<?= $od->id ?>')">Start Order</a>
                                        <?php } else if ($od->order_status == 'order_accepted' && $od->has_visit_and_quote == "Yes" && $od->has_quotation == 1) { ?>
                                            <a class="btn btn-xs btn-primary" onclick="start_order('<?= $od->id ?>')">Start Order</a> 
                                        <?php } ?>
                                        <?php if ($od->order_status == 'order_started') { ?>
                                            <a class="btn btn-xs btn-warning" onclick="send_otp('<?= $od->id ?>')">Complete Order</a>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>

                    <div class="modal fade" tabindex="-1" id="complete_order_model" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h2 class="modal-title">Enter OTP
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button></h2>
                                </div>
                                <div class="modal-body">
                                    <p>An <b>OTP</b> has been sent to the Customer, Please Enter it here to Complete the Order. </p>
                                </div>
                                <form action="" method="post">
                                    <div class="form-group" style="padding: 0px 30px 40px 30px">
                                        <label class="control-label">Enter OTP</label>
                                        <input type="hidden" id="complete_order_id"/>
                                        <input type="text" id="complete_order_otp" style="text-align: center" class="form-control" maxlength="4" onkeydown='return (event.which >= 48 && event.which <= 57) || event.which == 8 || event.which == 46 || (event.which >= 96 && event.which <= 105)'/>
                                        <br>
                                        <a href="javascript:void(0);" onclick="send_otp_ajax('');">Resend OTP</a>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" onclick="complete_order()">Complete Order</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">
    var order_id_master = "";
    function send_otp(order_id) {
        order_id_master = order_id;
        if (confirm('Are you sure want to Complete this Service ?')) {
            $.ajax({
                url: "<?= base_url('api/service_provider/services/get_balance_amount') ?>",
                type: "post",
                data: {order_id: order_id},
                success: function (resp) {
                    resp = JSON.parse(resp);
                    if (resp['status']) {
                        var balance_amount = parseFloat(resp['data']['balance_amount']);
                        console.log(balance_amount);
                        if (balance_amount > 0) {
                            if (confirm('Did you collected the Balance Amount of Rs.' + balance_amount)) {
                                send_otp_ajax(order_id);
                            }
                        } else {
                            send_otp_ajax(order_id);
                        }
                    } else {
                        send_otp_ajax(order_id);
                    }
                }
            });

        }
    }

    function send_otp_ajax(order_id) {
        if (order_id.length < 1) {
            order_id = order_id_master;
        }
        $.ajax({
            url: '<?= base_url("api/service_provider/services/send_otp_for_completing_order") ?>',
            type: "POST",
            data: {user_id: '<?= $vendor_id ?>', order_id: order_id},
            success: function (json) {
                var resp = JSON.parse(json);
                if (resp['status']) {
                    swal({
                        title: resp['message'],
                        text: "An OTP has been sent to the customer Please enter that OTP to complete this Order.",
                        icon: 'success'
                    }).then(function () {
                        $("#complete_order_id").val(order_id);
                        $("#complete_order_model").modal('show');
                        setTimeout(function () {
                            $("#complete_order_otp").focus();
                        }, 800);
                    });
                } else {
                    swal({
                        title: resp['message'],
                        icon: 'info'
                    });
                }
            }
        });
    }

    function complete_order() {
        var order_id = $("#complete_order_id").val();
        var otp = $("#complete_order_otp").val();
        $.ajax({
            url: '<?= base_url("api/service_provider/services/complete_order") ?>',
            type: "POST",
            data: {user_id: '<?= $vendor_id ?>', order_id: order_id, otp: otp},
            success: function (json) {
                var resp = JSON.parse(json);
                console.log(resp);
                if (resp['status']) {
                    swal({
                        title: resp['message'],
                        icon: 'success'
                    }).then(function () {
                        location.href = "";
                    });
                } else {
                    swal({
                        title: resp['message'],
                        icon: 'info'
                    }).then(function () {
                        setTimeout(function () {
                            $("#complete_order_otp").focus();
                        }, 800);
                    });
                }
            }
        });

    }

    function start_order(order_id) {
        if (confirm('Are you sure want to start this Service ?')) {
            $.ajax({
                url: '<?= base_url("api/service_provider/services/start_service") ?>',
                type: "POST",
                data: {user_id: '<?= $vendor_id ?>', order_id: order_id},
                success: function (json) {
                    var resp = JSON.parse(json);
                    if (resp['status']) {
                        swal({
                            title: resp['message'],
                            icon: 'success'
                        }).then(function () {
                            location.href = "";
                        });
                    } else {
                        swal({
                            title: resp['message'],
                            icon: 'info'
                        });
                    }
                }
            });
        }
    }

    function updateStatus(value, order_id)
    {
        if (value != '')
        {
            $.ajax({
                url: "<?php echo base_url(); ?>/vendors/orders/changeStatus",
                method: "POST",
                data: {value: value, order_id: order_id},
                success: function (data)
                {
                    if (data == 'success')
                    {
                        alert("status changed successfully");
                        window.location.href = "<?php echo base_url(); ?>vendors/orders";
                    }
                }
            });
        }
    }

</script> 


