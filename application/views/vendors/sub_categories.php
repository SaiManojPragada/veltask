<style>
    .cat_image{
        width: 100px;
        height: 100px;
        object-fit: scale-down;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Sub Categories</h5>
                    <div class="ibox-tools">
                        <a href="<?= base_url() ?>vendors/categories">
                            <button class="btn btn-primary">BACK</button>
                        </a>
                    </div>
                    
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Details</th>
                                    <th>Products</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($sub_categories as $subcat) {
                                            $qry = $this->db->query("select * from categories where id='".$subcat->cat_id."'");
                                            $cat_row = $qry->row();
                                            $parentcat = $cat_row->category_name;
                                    ?>
                                    <tr class="gradeX">
                                        <td><?= $i ?></td>
                                        <td>
                                            <img class="cat_image" align="left" src="<?= base_url() ?>uploads/sub_categories/<?= $subcat->app_image ?>" title="">
                                            <p><b>Sub Category ID: </b><span class="font-weight500"><?= $subcat->id ?></span></p>
                                            <p><b>Sub Category Name: </b><span class="font-weight500"><?= $subcat->sub_category_name ?></span></p>
                                            <p><b>Description: </b><span class="font-weight500"><?= $subcat->description ?></span></span></p>
                                            <p><b>Parent Category: </b> <?= $parentcat ?></p>
                                        </td>
                                        <td>
                                            <a href="<?= base_url() ?>vendors/sub_categories/viewProducts/<?php echo $subcat->cat_id; ?>/<?php echo $subcat->id; ?>"><button class="btn btn-primary">VIEW</button></a>
                                        </td>
                                        


                                    </tr>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
