<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Services categories Switches</h5>
                </div>
                <div class="ibox-content">
                    <div  style="padding: 10px 60px">
                        <div class="row">
                            <?php if (!empty($categories)) { ?>
                                <?php foreach ($categories as $cat) { ?>
                                    <div class="col-md-12 row">
                                        <div class="col-md-8">
                                            <strong><?= $cat->name ?></strong>
                                        </div>
                                        <div class="col-md-2">
                                            <label class="switch">
                                                <input type="checkbox" <?= ($cat->is_active == "Yes") ? "checked" : "" ?> onchange="switchChanged('<?= $cat->id ?>')">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                        <div class="col-md-2">
                                            <a style="float: right;" href="<?= base_url('vendors/switch_services/services/' . $cat->id) ?>"><span style="color: tomato; font-weight: bold">Manage Services <i class="fa fa-angle-right"></i></span></a>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            <?php if (!empty($sub_categories)) { ?>
                                <ul class="nav nav-tabs" style="margin-bottom: 20px">
                                    <?php foreach ($sub_categories as $sub_cat) { ?>
                                        <li class="nav-item <?= ($current_sub_category_id == $sub_cat->id) ? "active" : "" ?>">
                                            <a class="nav-link active" aria-current="page" href="<?= base_url('vendors/switch_services/services/' . $cat_id . '/?sub_cat=' . $sub_cat->id) ?>"><?= $sub_cat->sub_category_name ?></a>
                                        </li>
                                    <?php } ?>
                                    <li class="nav-item <?= ($current_sub_category_id == 'no') ? "active" : "" ?>">
                                        <a class="nav-link active" aria-current="page" href="<?= base_url('vendors/switch_services/services/' . $cat_id . '/?sub_cat=no') ?>">No Sub Category Services</a>
                                    </li>
                                </ul>
                            <?php } ?>
                            <?php foreach ($services as $ser) { ?>
                                <div class="col-md-12 row" >
                                    <div class="col-md-8">
                                        <strong><?= $ser->service_name ?></strong>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="switch">
                                            <input type="checkbox" <?= ($ser->is_active == "Yes") ? "checked" : "" ?> onchange="switchServiceChanged('<?= $ser->cat_id ?>', '<?= $ser->sub_cat_id ?>', '<?= $ser->id ?>')">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($current_sub_category_id == 'no') { ?>
                                <?php foreach ($no_sub_category_services as $ser) { ?>
                                    <div class="col-md-12 row" >
                                        <div class="col-md-8">
                                            <strong><?= $ser->service_name ?></strong>
                                        </div>
                                        <div class="col-md-2">
                                            <label class="switch">
                                                <input type="checkbox" <?= ($ser->is_active == "Yes") ? "checked" : "" ?> onchange="switchServiceChanged('<?= $ser->cat_id ?>', '<?= $ser->sub_cat_id ?>', '<?= $ser->id ?>')">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function switchChanged(cat_id) {
        var user_id = "<?= $vendor_id ?>";
        $.ajax({
            url: "<?= base_url('api/service_provider/services_switch/categories') ?>",
            type: "post",
            data: {user_id: user_id, category_id: cat_id},
            success: function (resp) {
                resp = JSON.parse(resp);
                if (resp['status']) {

                } else {
                    swal({
                        title: resp['message'],
                        icon: "warning"
                    }).then(function () {
                        loaction.href = '';
                    });
                }
            }
        });
    }

    function switchServiceChanged(cat_id, sub_cat_id, ser_id) {
        var user_id = "<?= $vendor_id ?>";
        $.ajax({
            url: "<?= base_url('api/service_provider/services_switch/services') ?>",
            type: "post",
            data: {user_id: user_id, category_id: cat_id, sub_category_id: sub_cat_id, service_id: ser_id},
            success: function (resp) {
                resp = JSON.parse(resp);
                console.log(resp);
                if (resp['status']) {

                } else {
                    swal({
                        title: resp['message'],
                        icon: "warning"
                    }).then(function () {
                        loaction.href = '';
                    });
                }
            }
        });
    }
</script>
<style>
    /* The switch - the box around the slider */
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    /* Hide default HTML checkbox */
    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    /* The slider */
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #2196F3;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }

    .switch{
        transform: scale(0.8);
    }
</style>