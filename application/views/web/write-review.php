<!--Breadcrumb-->
<section>
    <div class="bannerimg cover-image bg-background3" data-image-src="../assets/images/banners/banner2.jpg">
        <div class="header-text mb-0">
            <div class="container">
                <div class="text-center text-white">
                    <h1 class="">My Dashboard</h1>
                    <ol class="breadcrumb text-center">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active text-white" aria-current="page">Reviews</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Breadcrumb-->

<!--User Dashboard-->
<section class="sptb">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-header membership p-4">
                        <h5>Write a Review for Order </h5>
                    </div>
                    <div class="card-body">
                        <div class="container p-0">
                            <div class=" p-4">
                                <div class="row">
                                    <?php foreach ($order_items as $index => $item) { ?>
                                        <div class="col-12 ">
                                            <div class="card pt-4 pl-4 pr-4 pb-2" style="width: 100%">
                                                <div class="row">
                                                    <div  class="col-2">
                                                        <img src="<?= ($item->service_image->image) ? base_url('uploads/services/') . $item->service_image->image : base_url('uploads/') . SITE_FAV_ICON ?>"/>
                                                    </div>
                                                    <div class="col-10 pt-3">
                                                        <h4 style="margin-left: 25px"><?= $item->service_data->service_name ?> <small>(X<?= $item->quantity ?>)</small></h4>
                                                        <div class="rating-css col-12" style="float: left; text-align: left">
                                                            <div class="star-icon" id="star-icon<?= $item->service_data->id ?>" style="<?= ($item->review) ? "color: #ffe400;" : "" ?>">
                                                                <input type="radio" name="rating1<?= $index ?>" onchange="updateRating('<?= $item->review->id ?>', '<?= $item->service_data->id ?>', '1')" id="rating1<?= $index ?>" value="1" <?= ($item->review->rating == 1) ? 'checked' : '' ?>>
                                                                <label for="rating1<?= $index ?>"><i class="fas fa-star"></i></label>
                                                                <input type="radio" name="rating1<?= $index ?>" onchange="updateRating('<?= $item->review->id ?>', '<?= $item->service_data->id ?>', '2')" id="rating2<?= $index ?>" value="2"  <?= ($item->review->rating == 2) ? 'checked' : '' ?>>
                                                                <label for="rating2<?= $index ?>"><i class="fas fa-star"></i></label>
                                                                <input type="radio" name="rating1<?= $index ?>" onchange="updateRating('<?= $item->review->id ?>', '<?= $item->service_data->id ?>', '3')" id="rating3<?= $index ?>" value="3"  <?= ($item->review->rating == 3) ? 'checked' : '' ?>>
                                                                <label for="rating3<?= $index ?>"><i class="fas fa-star"></i></label>
                                                                <input type="radio" name="rating1<?= $index ?>" onchange="updateRating('<?= $item->review->id ?>', '<?= $item->service_data->id ?>', '4')" id="rating4<?= $index ?>" value="4"  <?= ($item->review->rating == 4) ? 'checked' : '' ?>>
                                                                <label for="rating4<?= $index ?>"><i class="fas fa-star"></i></label>
                                                                <input type="radio" name="rating1<?= $index ?>" onchange="updateRating('<?= $item->review->id ?>', '<?= $item->service_data->id ?>', '5')" id="rating5<?= $index ?>" value="5" <?= ($item->review->rating == 5) ? 'checked' : '' ?>>
                                                                <label for="rating5<?= $index ?>"><i class="fas fa-star"></i></label>
                                                            </div>
                                                            <?= $item->review->comment ?>
                                                        </div>
                                                        <button class="btn btn-outline-dark mt-1" style="margin-left: 25px; position: absolute; top: 30px; right: 20px"
                                                                data-toggle="modal" data-target="#review-modal"
                                                                onclick="assign_review('<?= $item->service_data->id ?>', '<?= $item->id ?>');">
                                                            <?php if ($item->review->comment) { ?>Edit<?php } else { ?>Write a Review<?php } ?> <i class="far fa-edit"></i></button>
                                                        <span style="display: none;" id="review_json_<?= $item->id ?>">
                                                            <?php unset($item->service_data->description); ?>
                                                            <?= json_encode($item->review) ?>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>




                                    <?php foreach ($order_items as $item) { ?>
                                        <!--                                        <div class="col-3 ">
                                                                                    <div class="card pt-4 pl-4 pr-4 pb-2" style="width: 100%">
                                                                                        <img src="<?= ($item->service_image->image) ? base_url('uploads/services/') . $item->service_image->image : base_url('uploads/') . SITE_FAV_ICON ?>"/>
                                                                                        <h4><?= $item->service_data->service_name ?></h4>
                                                                                        <button class="btn btn-outline-dark mt-3" data-toggle="modal" data-target="#review-modal" onclick="assign_review('<?= $item->service_data->id ?>', '<?= $item->id ?>');"> Write a Review <i class="far fa-edit"></i></button>
                                                                                        <span style="display: none" id="review_json_<?= $item->id ?>">
                                        <?php unset($item->service_data->description); ?>
                                        <?= json_encode($item->review) ?>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>-->
                                    <?php } ?>
                                </div>
                            </div>
                            <!--row-->
                        </div>				
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="review-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Write a Review</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="service_id" value="" />

                    <textarea id="comment" rows="5" class="form-control" placeholder="Give a Feedback.." maxlength="100"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="submitFeedback()">Submit Feedback</button>
                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    .rating-css {
        text-align: center;
        padding: 5px 20px;
    }
    .rating-css input {
        display: none;
    }
    .rating-css input + label {
        font-size:30px;
        cursor: pointer;
        text-align: center
    }
    .rating-css input:checked + label ~ label {
        color: #838383;
    }
    .rating-css label:active {
        transform: scale(0.8);
        transition: 0.3s ease;
    }




</style>
<script>

    var order_id = '<?= $order_real_id ?>';
    function updateRating(rev_id, service_id, rating) {
        var update_url = "<?= base_url('api/services_ratings/submit_rating') ?>";
        var ajax_data = {user_id: '<?= USER_ID ?>', order_id: order_id, service_id: service_id, rating: rating};
//        console.log(rev_id);
//        return false;
        var reloader = true;
        if (rev_id !== null && rev_id !== "" && rev_id.length > 0) {
            update_url = '<?= base_url('api/services_ratings/update_rating') ?>';
            ajax_data = {user_id: '<?= USER_ID ?>', id: rev_id, rating: rating};
            var reloader = false;
        }
        $.ajax({
            url: update_url,
            type: "post",
            data: ajax_data,
            success: function (resp) {
                resp = JSON.parse(resp);
                if (resp['status']) {
                    $("#star-icon" + service_id).css({"color": "#ffe400"})
                    swal({
                        title: resp['message'],
                        icon: "success"
                    });
                    if (reloader) {
                        location.href = "";
                    }
                } else {
                    swal({
                        title: resp['message'],
                        icon: "warning"
                    });
                }
            }
        });
    }

    var review_id = null;
    function assign_review(id, tag_id) {
        $("#service_id").val(id);
        var json = $("#review_json_" + tag_id).html();
        var review = JSON.parse(json);
        if (review !== null) {
            review_id = review['id'];
//            for (var i = 1; i < 6; i++) {
//                $("#rating" + i).prop("checked", false);
//            }
//            $("#rating" + review['rating']).prop("checked", true);
            $("#comment").val(review['comment']);
        }
    }
    var star_rating = 1;
    function submitFeedback() {
        for (var i = 1; i < 6; i++) {
            if ($('#rating' + i).is(':checked')) {
                star_rating = $('#rating' + i).val();
            }
        }
        var feedback = $("#comment").val();
        var service_id = $("#service_id").val();
        var url = "<?= base_url('api/services_ratings/submit_rating') ?>";
        var ajax_data = {user_id: '<?= USER_ID ?>', order_id: order_id, service_id: service_id, comment: feedback};
        if (review_id !== null) {
            url = '<?= base_url('api/services_ratings/update_rating') ?>';
            ajax_data = {user_id: '<?= USER_ID ?>', id: review_id, comment: feedback};
        }
        $.ajax({
            url: url,
            type: "post",
            data: ajax_data,
            success: function (resp) {
                console.log(resp);
                resp = JSON.parse(resp);
                if (resp['status']) {
                    swal({
                        title: resp['message'],
                        icon: "success"
                    }).then(function () {
                        location.href = "";
                    });
                } else {
                    swal({
                        title: resp['message'],
                        icon: "warning"
                    });
                }
            }
        });
    }

</script>

<!--/User Dashboard-->

<!-- Newsletter-->

<!--/Newsletter-->