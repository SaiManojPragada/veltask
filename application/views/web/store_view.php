<?php include 'includes/ecom_header.php'; ?>

<!--Sliders Section-->

<section class="bg-white">
    <div class="banner1  relative slider-images store-slider">				
        <div class="owl-carousel testimonial-owl-carousel2 slider slider-header ">
            <?php foreach ($banners as $ban) { ?>

                <div class="item cover-image" data-image-src="">
                    <img class="store-view"  alt="first slide" src="<?php echo $ban['image']; ?>" >

                </div>
            <?php } ?>						
        </div>				
    </div>
</section>
<!--Sliders Section-->



<section class="sptb bg-white">
    <div class="container">
        <div class="section-title center-block text-center">
            <h2>Shop By Category</h2>
        </div>
        <div id="myCarousel1" class="owl-carousel owl-carousel-icons15 popular-week">
            <?php foreach ($subcategories as $cat) { ?>
                <div class="item">

                    <div class="card bg-card-light">
                        <div class="card-body">
                            <div class="cat-item text-center">
                                <a href="<?php echo base_url(); ?>web/product_categories/<?php echo $cat['seo_url']; ?>/<?php echo $vendor_seo_url; ?>/<?php echo $cat_url; ?>"></a>
                                <div class="cat-img">
                                    <img src="<?php echo $cat['image']; ?>" alt="img">
                                </div>
                                <div class="cat-desc">
                                    <h5 class="mb-1"><?php echo $cat['category_name']; ?></h5>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            <?php } ?>


        </div>	

    </div>
</section>

<!--Popular This Week-->
<section class="sptb bg-white ">
    <div class="container">
        <div class="section-title center-block text-center">
            <h2>Best Selling </h2>
        </div>
        <div id="myCarousel1" class="owl-carousel owl-carousel-icons5 popular-week best-selling-carousel">
            <?php foreach ($best_selling_products as $value) { ?>
                <div class="item">
                    <img  class="besting-selling " src="<?php echo $value['image']; ?>" alt="">
                    <div class="popular-products">
                        <h2><a href="<?php echo base_url(); ?>web/product_view/<?php echo $value['seo_url']; ?>/<?php echo $cat_url; ?>"><?php echo $value['name']; ?></a></h2>
                        <span><?php echo $value['shop']; ?></span>
                        <div class="price"><?php echo $value['saleprice']; ?><i class="fas fa-rupee-sign"></i></div>
                        <a onclick="addtocart(<?php echo $value['variant_id']; ?>,<?php echo $value['shop_id']; ?>, '<?php echo $value['saleprice']; ?>', 1)" style="    background: #f15d74 !important;
                           border: 1px solid !important; " class="btn btn-primary  btn-ptill mb-3">Add to Cart</a>
                    </div>
                </div>

            <?php } ?>

        </div>	

    </div>
</section>
<!--/Featured Ads-->

<style>
    .best-selling-carousel .besting-selling {

        width: 100%;
        height: 150px ;
        object-fit : contain;
    }
</style>


<?php include 'includes/footer.php'; ?>