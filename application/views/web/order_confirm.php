<?php  $this->load->view("web/includes/ecom_header"); ?>
<!--breadcrumbs area start-->
<div class="breadcrumbs_area">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="breadcrumb_content">
					<h3>Order Confirm</h3>
				</div>
			</div>
		</div>
	</div>
</div>
<!--breadcrumbs area end-->
<div class="shopping_cart_area pb-100" style="min-height: 400px">
	<div class="container">
		<div class="row pb-5 justify-content-center">
			<div class="col-lg-10 col-md-10">
				<ul class="step d-flex flex-nowrap">
							<li class="step-item <?php if ($page == 'goaddress_page') { ?>active <?php } ?>">
								<a href="#!" class="">Add Address</a>
							</li>
							<li class="step-item <?php if ($page == 'order_review.php') { ?>active <?php } ?>">
								<a href="#!" class="">Order Review</a>
							</li>
							<li class="step-item <?php if ($page == 'payment.php') { ?>active <?php } ?>">
								<a href="#!" class="">Payment</a>
							</li>
							<li class="step-item <?php if ($page == 'order_confirm.php') { ?>active <?php } ?>">
								<a href="#!" class="">Order Confirmation</a>
							</li>
						</ul>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-lg-6 text-center">
				<img src="<?php echo base_url(); ?>web_assets/img/checkmark.gif" alt="" width="80" height="80">
				<h3 class="pt-3">Your order is accepted</h3>
				<p>Your shipment will arrive Shortly</p>
				<a href="<?php echo base_url(); ?>" class="btn_continue">Continue Shopping</a>
			</div>
		</div>
	</div>
</div>

<?php  $this->load->view("web/includes/footer"); ?>