<!--Sliders Section-->
<div>
    <div class="cover-image sptb-1 bg-background" data-image-src="../assets/images/banners/banner1.jpg">
        <div class="header-text1 mb-0">
            <div class="container">
                <div class="row">
                    <div class="col-xl-10 col-lg-12 col-md-12 d-block ">
                        <div class=" breadcrumb-banner text-left text-white ">
                            <h1 > Cart
                            </h1>
                        </div>
                        <div>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="<?= base_url() ?>">Home
                                    </a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="javascript:history.back();">Services
                                    </a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="javascript:void(0);">Cart
                                    </a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /header-text -->
    </div>
</div>

<?php if ($this->session->userdata("login_check") !== "Yes") { ?>
    <!--/Sliders Section-->
    <!--Add listing-->
    <section class="sptb">
        <div class="container">
            <div class="row">
                <?php if (!empty($cart)) { ?>
                    <div class="col-xl-8 col-lg-8 col-md-12">
                        <!--Classified Description-->

                        <?php foreach ($cart as $index => $item) { ?>
                            <div class="card overflow-hidden ">
                                <div class="d-md-flex">
                                    <div class="card pl-4 pr-4 border-0 mb-0">
                                        <div class="header-crd  pt-2 pb-1 ">
                                            <h4 class="font-weight-bold mt-1 h4-tag"><?= $item->service_details->service_name ?><?= (!$item->has_visit_and_quote) ? " x ($item->quantity)" : "" ?><small>&nbsp;&nbsp;&nbsp;<?= ($item->has_visit_and_quote) ? '(Visit & Quote)' : '' ?></small>
                                            </h4>
                                        </div>
                                        <div class="card-body ">
                                            <div class="item-card2">
                                                <div class="item-card2-desc">
                                                    <?= $item->service_details->description ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer pt-4 pb-4">
                                            <div class="item-card2-footer d-sm-flex">
                                                <div class="item-card2-rating">
                                                    <div class="rating-stars d-inline-flex">
                                                        <ul class="footer-list">
                                                            <li>
                                                                <div class="service-price">
                                                                    <?php if (!$item->has_visit_and_quote) { ?>
                                                                        <p class="mb-0">
                                                                            <span class="offer-price">₹<?= number_format($item->service_details->sale_price, 2) ?>
                                                                            </span> 
                                                                            <span class="mrp-price">₹<?= number_format($item->service_details->price, 2) ?>
                                                                            </span>
                                                                        </p>
                                                                    <?php } else { ?>
                                                                        <p class="mb-0">
                                                                            <span class="offer-price">₹<?= number_format($item->service_details->visiting_charges, 2) ?>
                                                                            </span> 
                                                                        </p>
                                                                    <?php } ?>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <?php if ($item->has_visit_and_quote) { ?>
                                                    <div class="ml-auto remove-btn">
                                                        <span class="btn btn-primary " onclick="clearCart('<?= $item->id ?>')">
                                                            <i class="fa fa-trash" aria-hidden="true">
                                                            </i> <a class="text-white" href="javascript:void(0);"> Remove</a>
                                                        </span>
                                                    </div>
                                                <?php } else { ?>
                                                    <div class="F8dpS zj0R0 _3L1X9 ml-auto" style="cursor: pointer; <?= ($item->has_visit_and_quote == 1) ? "display: none" : "" ?>">
                                                        <input id="service_id_<?= $index ?>" type="hidden" value="<?= $item->service_details->id ?>">

                                                        <div class="_1RPOp addbtn_<?= $index ?>" id="addbtn" onclick="show('<?= $index ?>');" style=' <?= ($item->quantity) ? "display : none" : "" ?>'>ADD</div>

                                                        <div id="box1" class="box1_<?= $index ?>" style="display: none;"></div>

                                                        <div id="increament-div" class="increament-div_<?= $index ?>"  style=' <?= ($item->quantity) ? "display : block" : "" ?>'>
                                                            <div id="box" style="display: none;" class="box_<?= $index ?>"></div>
                                                            <div onclick="inccreament('<?= $index ?>')" class="_1ds9T _2WdfZ _4aKW6 classList ">+</div>
                                                            <div class="_2zAXs _2quy- _4aKW6" id="quantity_<?= $index ?>"><?= ($item->quantity) ? $item->quantity : "0" ?></div>
                                                            <div onclick="deccreament('<?= $index ?>')"  class="_29Y5Z _20vNm _4aKW6">-</div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                    </div>
                    <!--Right Side Content-->
                    <div class="col-xl-4 col-lg-4 col-md-12">
                        <div class="contine-page-right-side">
                            <div class="continue-item-1">
                                <?php if ($cart[0]->has_visit_and_quote != 1) { ?>
                                    <div class="card offer-bg ">
                                        <div class="card-body p-5">
                                            <div class="offer-card d-flex" id="coupons_view_div" type="button"  data-toggle="modal" data-target="#Mymodal"
                                                 style="<?= (empty($coupon_and_wallet['coupon_code'])) ? '' : 'display: none !important' ?>">
                                                <div class="availble-oofer-content">
                                                    <p class="mb-0">Offers and Promo codes
                                                    </p>
                                                    <span><?= sizeof($coupon_codes) ?> offers Availble
                                                    </span>
                                                </div>
                                                <div class="offer-img">
                                                    <img src="<?= base_url('web_assets/') ?>assets/images/discount.png" alt="">
                                                </div>
                                            </div>
                                            <div class="offer-card d-flex" id="applied_coupon_div" style="<?= (!empty($coupon_and_wallet['coupon_code'])) ? '' : 'display: none !important' ?>">
                                                <div class="availble-oofer-content" type="button"  data-toggle="modal" data-target="#Mymodal">
                                                    <p class="mb-0">Coupon Applied
                                                    </p>
                                                    <h3 class="mb-0" id="applied_coupon_code"><?= $coupon_and_wallet['coupon_code'] ?>
                                                    </h3>
                                                </div>
                                                <div class="offer-img">
                                                    <span onclick="removeCoupon();">Remove &times;</span>
                                                </div>
                                            </div>
                                            <!--offer-card-->
                                        </div>
                                    </div>
                                    <?php if ($wallet_amount > 0) { ?>
                                        <div class="card">
                                            <div class="card-body p-5">
                                                <div class="form-group">
                                                    <input type="checkbox" class="form-control" id="use_wallet_check" style="width: 20px" onchange='walletCheck(this)' <?= ($coupon_and_wallet['wallet_applied_amount'] > 0) ? "checked" : "" ?>/>
                                                    <span style="position: absolute; top: 30px; left: 50px; font-weight: bold"> Use Wallet Amount</span>
                                                    <span style="font-size: 18px; position: absolute; top: 26px; right: 30px">₹<span id="current_wallet_amount"><?= ($coupon_and_wallet['wallet_applied_amount'] > 0) ? $coupon_and_wallet['balance_wallet_amount'] : $wallet_amount ?></span></span>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                                <div class="card">
                                    <div class="card-body p-5">
                                        <div class="total-item d-flex ">
                                            <div class="availble-oofer-content">
                                                <p class="mb-0">Item Total
                                                </p>
                                            </div>
                                            <div class="total-price ml-auto">
                                                <span>₹ <span id="sub_total"><?= $cart_totals->sub_total ?></span>
                                                </span>
                                            </div>
                                        </div>
                                        <!--offer-card-->
                                        <?php if ($cart[0]->has_visit_and_quote == 0) { ?>
                                            <div class="total-item d-flex ">
                                                <div class="availble-oofer-content">
                                                    <p class="mb-0">Safety & Visitation Fee
                                                    </p>
                                                </div>
                                                <div class="total-price ml-auto">
                                                    <span><span style="color: green">+</span> ₹ <span id="visiting_charges"><?= $cart_totals->visiting_charges ?></span>
                                                    </span>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <div class="total-item d-flex ">
                                            <div class="availble-oofer-content">
                                                <p class="mb-0">Tax
                                                </p>
                                            </div>
                                            <div class="total-price ml-auto">
                                                <span><span style="color: green">+</span> ₹ <span id="tax"><?= $cart_totals->tax ?></span>
                                                </span>
                                            </div>
                                        </div>

                                        <?php if ($cart_totals->membership_discount > 0) { ?>
                                            <div class="total-item d-flex ">
                                                <div class="availble-oofer-content">
                                                    <p class="mb-0">Membership Discount
                                                    </p>
                                                </div>
                                                <div class="total-price ml-auto">
                                                    <span><span style="color: tomato">-</span> ₹ <span id="membership_discount"><?= $cart_totals->membership_discount ?></span>
                                                    </span>
                                                </div>
                                            </div>
                                        <?php } ?>


                                        <?php if ($cart_totals->business_discount > 0) { ?>
                                            <div class="total-item d-flex ">
                                                <div class="availble-oofer-content">
                                                    <p class="mb-0">Business Discount
                                                    </p>
                                                </div>
                                                <div class="total-price ml-auto">
                                                    <span><span style="color: tomato">-</span> ₹ <span id="business_discount"><?= $cart_totals->business_discount ?></span>
                                                    </span>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <div class="total-item d-flex " id="coupon_discount_div" style="<?= (!empty($coupon_and_wallet['coupon_code'])) ? '' : 'display: none !important' ?>">
                                            <div class="availble-oofer-content">
                                                <p class="mb-0">Coupon Discount
                                                </p>
                                            </div>
                                            <div class="total-price ml-auto">
                                                <span><span style="color: tomato">-</span> ₹ <span id="coupon_amount"> <?= $coupon_and_wallet['coupon_discount'] ?></span>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="total-item d-flex " id="used_wallet_amount_div" style="<?= ($coupon_and_wallet['wallet_applied_amount'] > 0) ? "" : "display: none !important;" ?>">
                                            <div class="availble-oofer-content">
                                                <p class="mb-0">Used Wallet Amount
                                                </p>
                                            </div>
                                            <div class="total-price ml-auto">
                                                <span><span style="color: tomato">-</span> ₹ <span id="used_wallet_amount"><?= $coupon_and_wallet['wallet_applied_amount'] ?></span>
                                                </span>
                                            </div>
                                        </div>

                                        <!--offer-card-->
                                    </div>
                                    <div class="card-footer pt-2 pb-2">
                                        <div class="offer-footer d-sm-flex">
                                            <div class="offer-footer-child">
                                                <div class="rating-stars d-inline-flex">
                                                    <ul class="footer-list">
                                                        <li class="">
                                                            <div class="service-price">
                                                                <p class="mb-0">Total
                                                                </p>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="ml-auto remove-btn">
                                                <span>₹<span id="cart_grand_total"><?= (!empty($coupon_and_wallet['grand_total']) && $coupon_and_wallet['grand_total'] > 0) ? $coupon_and_wallet['grand_total'] : $cart_totals->total ?></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <p id="min_amount_msg" style="color: tomato"></p>
                                <div class="will-pay-button" id="proceed-checkout-btn" onclick="proceed_to_checkout()">
                                    <div class="will-pay">
                                        <button class="btn btn-light" id="procced-button">
                                            <a href="javascript:void(0);">Proceed to Checkout</a>
                                        </button>
                                    </div>
                                </div>
                                <div class="modal fade" id="Mymodal">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header d-flex">
                                                <div class="offer-code-popup">
                                                    <span>Offers and promo codes</span>
                                                </div>

                                                <div>

                                                    <button type="button" class="close" data-dismiss="modal">
                                                        &times;
                                                    </button> 
                                                </div>

                                            </div> 
                                            <div class="modal-body">
                                                <div class="input-group mb-3">
                                                    <input type="text" class="form-control" placeholder="Enter Coupon Code" aria-label="Enter Coupon Code" aria-describedby="basic-addon2">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text" id="basic-addon2">Apply</span>
                                                    </div>
                                                </div>


                                                <div class="container p-0">
                                                    <div class="apply-coupons-parent p-4">
                                                        <div class="row">
                                                            <?php foreach ($coupon_codes as $code) { ?>
                                                                <div class="col-md-6 mb-3">
                                                                    <div class="offer-coupons-slips">
                                                                        <p class="percentage-oofer"><?= $code->percentage ?>% OFF</p>
                                                                        <p class="availble-oofer-content"><?= $code->description ?></p>

                                                                        <button class="btn btn-default mt-2" onclick="applyCoupon('<?= $code->coupon_code ?>')">
                                                                            <p class="code-content">CODE</p>
                                                                            <p class="offer-code"><?= $code->coupon_code ?></p>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                            <?php if (empty($coupon_codes)) { ?>
                                                                <div class="col-12 pt-4 pb-4">
                                                                    <center> --  No Coupon Codes Available --</center>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <!--row-->
                                                </div>
                                            </div>   

                                        </div>                                                                       
                                    </div>                                      
                                </div>
                                <?php if (!$has_membership) { ?>
                                    <div class="membership-btn">
                                        <div class="membership-child">
                                            <button class="btn bg-dark-purple" type="button"  type="button"  data-toggle="modal" data-target="#Mymodal1">View Membership
                                            </button>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                <?php } else if (empty($cart)) { ?>
                    <div class="col-12">
                        <center style="margin: 220px 0px"><h2>-- No Items In Your Cart --</h2></center>
                    </div>
                <?php } ?>
                <!--/Right Side Content-->
            </div>
        </div>
    </section>
    <script>

        var items_in_cart = <?= TOTAL_ITEMS_IN_CART ?>;
        var is_checkout_enabled = <?= $is_cart_enable ?>;
        setInterval(function () {
            if (is_checkout_enabled) {
                $("#min_amount_msg").html("");
                $("#procced-button").removeAttr("disabled");
            } else {
                $("#min_amount_msg").html("* Min cart value is Rs." + min_cart_value);
                $("#procced-button").attr("disabled", "");
            }
        }, 500);

        function proceed_to_checkout() {
            if (is_checkout_enabled) {
                location.href = '<?= base_url('check-out') ?>';
            }
        }
        function clearCart(id) {
            swal({
                title: "Are you sure want to remove this service ?",
                icon: "warning",
                buttons: [
                    'No, cancel it!',
                    'Yes, I am sure!'
                ],
                dangerMode: true
            }).then(function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: '<?= base_url() ?>api/web/delete_cart_item',
                        type: 'post',
                        data: {user_id: '<?= USER_ID ?>', id: id},
                        success: function (resp) {
                            resp = JSON.parse(resp);
                            if (resp['status']) {
                                swal({
                                    title: "Service Removed from Cart",
                                    icon: 'success'
                                }).then(function () {
                                    location.reload();
                                });
                            } else {
                                swal({
                                    title: resp['message'],
                                    icon: 'info'
                                });
                            }
                        }
                    });
                }
            });
        }



        function hide_cart_message() {
            setTimeout(function () {
                $(".cart_alert").fadeOut(2000);
            }, 5000);
        }
        var page_url = "<?= base_url('services/' . $category . '/') ?><?= ($sub_category) ? $sub_category . '/' : "" ?><?= ($this->input->get("page") ? "?page=" . $this->input->get("page") . "&" : "?") ?>";
        var sort = "<?= $sort ?>";
        function addSort(sort) {
            if (sort.length > 1) {
                location.href = page_url + "sort=" + sort;
            } else {
                location.href = page_url;
            }
        }

        var prev_cart_desg = <?= ($cart[0]->has_visit_and_quote) ? '1' : '0' ?>;
        function show_cart() {
            $('#cart_bar').css('display', 'block');
        }
        function hide_cart() {
            $('#cart_bar').css('display', 'none');
        }
        function show(index, has_visit_and_quote = 0) {

    <?php if (!empty(USER_ID)) { ?>
                $(".increament-div_" + index).css('display', "block");
                $(".addbtn_" + index).css('display', "none");
                $('#quantity_' + index).html('1');
                //animation
                $("box_" + index).css("display", "block");
                setTimeout(function () {
                    $("box_" + index).css("display", "none");
                }, 400);

                //            $(".cart_alert").html("Item Added to Cart");
                //            $(".cart_alert").fadeIn(1000);
                hide_cart_message();
                if (prev_cart_desg == 0 && has_visit_and_quote == 1) {
                    swal({
                        title: "Are you sure?",
                        text: "If You Already have normal services in your cart by Proceeding to Visit and Quote your cart will be cleared.",
                        icon: "warning",
                        buttons: [
                            'No, cancel it!',
                            'Yes, I am sure!'
                        ],
                        dangerMode: true,
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                url: '<?= base_url() ?>api/cart/delete_cart',
                                type: 'post',
                                data: {user_id: '<?= USER_ID ?>'},
                                success: function (resp) {
                                    resp = JSON.parse(resp);
                                    if (resp['status']) {
                                        call_cart(index, has_visit_and_quote);
                                    } else {
                                        swal({
                                            title: resp['message'],
                                            icon: 'info'
                                        });
                                    }
                                }
                            });
                        }
                    });
                } else {
                    call_cart(index, has_visit_and_quote);
                }

    <?php } else { ?>
                swal({
                    title: "Please Login to Continue",
                    icon: "info"
                }).then(function () {
                    openNav();
                });
    <?php } ?>
        }


        function inccreament(index)
        {

            //animation
            $(".box_" + index).css("display", "block");
            var quantity = $("#quantity_" + index).html();
            quantity = parseInt(quantity.toString()) + 1;
            $("#quantity_" + index).html(quantity);
            setTimeout(function () {
                $(".box_" + index).css("display", "none");
            }, 400);
            call_cart(index);

        }

        function deccreament(index, stat = "")
        {
            var quantity = $("#quantity_" + index).html();
            quantity = parseInt(quantity.toString()) - 1;
            $("#quantity_" + index).html(quantity);
            if (quantity < 1) {
                $(".increament-div_" + index).css('display', "none");
                //                $(".addbtn_" + index).css('display', "block");
                if (stat == "") {
                    $(".cart_alert").html("Item Removed from Cart");
                    $(".cart_alert").fadeIn(1000);
                    hide_cart_message();
                }
            }
            //animation
            $(".box_" + index).css("display", "block");
            setTimeout(function () {
                $(".box_" + index).css("display", "none");

            }, 400);
            if (stat == "") {
                call_cart(index);
        }
        }

        function call_cart(index, has_visit_and_quote = 0) {

            var service_id = $("#service_id_" + index).val();
            var quantity = $("#quantity_" + index).html();
            if (has_visit_and_quote == 1) {
                quantity = 1;
            }
            $.ajax({
                url: "<?= base_url('api/web/cart') ?>",
                type: "post",
                data: {service_id: service_id, quantity: quantity, has_visit_and_quote: has_visit_and_quote},
                success: function (json) {
                    var resp = JSON.parse(json);
                    if (resp['status']) {
                        get_coupon_wallet_discount(true);
                        show_cart();
                        var data = resp['data'];
                        console.log(data);
                        //                        $("#cart_total").html(data['cart_total']);
                        //                        $("#items_in_cart").html(data['items_in_cart']);
                        if (data['items_in_cart'] != items_in_cart) {
                            location.href = "";
                        }
                        $("#cart-items-top").html(data['items_in_cart']);
                        if (data['items_in_cart'] == 0) {
                            hide_cart();
                        }
                        if (has_visit_and_quote == 1) {
                            swal({
                                title: resp['message'],
                                icon: 'success'
                            }).then(function () {
                                location.href = "<?= base_url('cart') ?>";
                            });
                        }
                    } else {
                        location.href = "";
                        deccreament(index, "dsada");
                        swal({
                            title: resp['message'],
                            icon: "info"
                        }).then(function () {
    <?php if (empty(USER_ID)) { ?>
                                openNav();
    <?php } ?>
                        });
                    }
                }
            });
        }
    </script>
<?php } else { ?>
    <div class="container-fluid" style="min-height: 500px">
        <center style='margin-top: 200px'>
            <strong><h2>User Not Logged In</h2></strong>
        </center>
    </div>
<?php } ?>