<!--Breadcrumb-->
<section>
    <div class="bannerimg cover-image bg-background3" data-image-src="../assets/images/banners/banner2.jpg">
        <div class="header-text mb-0">
            <div class="container">
                <div class="text-center text-white">
                    <h1 class="">My Booking</h1>
                    <ol class="breadcrumb text-center">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active text-white" aria-current="page">My Booking</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Breadcrumb-->

<?php if (!empty(USER_ID)) { ?>
    <!--User Dashboard-->
    <section class="sptb">
        <div class="container">
            <div class="row">
                <?php include 'dashboard-left-menu.php'; ?>
                <div class="col-xl-9 col-lg-12 col-md-12">
                    <div class="row">
                        <div class="col-sm-6 col-xl-4 mb-2 mb-sm-0">
                            <section class="card card-featured-left card-featured-secondary ">
                                <div class="card-body ">
                                    <div class="widget-summary ">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="widget-summary-col widget-summary-col-icon">
                                                    <div class="summary-icon bg-secondary">
                                                        <i class="fa fa-cogs" aria-hidden="true"></i>
                                                    </div>
                                                </div>

                                            </div><!--col-md-6-->
                                            <div class="col-md-7">
                                                <div class="widget-summary-col">
                                                    <div class="summary">
                                                        <h4 class="title">Total Services</h4>

                                                    </div>
                                                    <div class="summary-footer">
                                                        <span><?= number_format(sizeof($all_my_orders)) ?></span>
                                                    </div>
                                                </div>
                                            </div><!--col-md-6-->
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>


                        <div class="col-sm-6 col-xl-4 mb-2 mb-sm-0">
                            <section class="card card-featured-left card-featured-secondary ">
                                <div class="card-body ">
                                    <div class="widget-summary ">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="widget-summary-col widget-summary-col-icon">
                                                    <div class="summary-icon orders">
                                                        <i class="fal fa-list-ul"></i>
                                                    </div>
                                                </div>

                                            </div><!--col-md-6-->
                                            <div class="col-md-7">
                                                <div class="widget-summary-col">
                                                    <div class="summary">
                                                        <h4 class="title">Orders</h4>

                                                    </div>
                                                    <div class="summary-footer">
                                                        <span>0</span>
                                                    </div>
                                                </div>
                                            </div><!--col-md-6-->
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>

                        <div class="col-sm-6 col-xl-4 mb-2 mb-sm-0">
                            <section class="card card-featured-left card-featured-secondary ">
                                <div class="card-body ">
                                    <div class="widget-summary ">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="widget-summary-col widget-summary-col-icon">
                                                    <div class="summary-icon wallet">
                                                        <i class="fal fa-wallet"></i>
                                                    </div>
                                                </div>

                                            </div><!--col-md-6-->
                                            <div class="col-md-7">
                                                <div class="widget-summary-col">
                                                    <div class="summary">
                                                        <h4 class="title">Wallet</h4>

                                                    </div>
                                                    <div class="summary-footer">
                                                        <span><i class="far fa-rupee-sign"></i> <?= number_format($wallet_amount) ?></span>
                                                    </div>
                                                </div>
                                            </div><!--col-md-6-->
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>


                    <div class="card">

                        <div class="card-body">
                            <div class="card-pay">
                                <ul class="tabs-menu nav">
                                    <li><a href="<?= base_url('my-bookings/ongoing') ?>" class="<?= ($view_filter == 'ongoing') ? 'active' : '' ?>"> On Going</a></li>
                                    <li><a href="<?= base_url('my-bookings/completed') ?>" class="<?= ($view_filter == 'completed') ? 'active' : '' ?>"> Completed</a></li>
                                    <li><a href="<?= base_url('my-bookings/cancelled') ?>" class="<?= ($view_filter == 'cancelled') ? 'active' : '' ?>">  Cancelled</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane show active" id="tab1">
                                        <div class="card-body">
                                            <div class="my-favadd table-responsive userprof-tab">
                                                <table class="table  table-hover mb-0 text-nowrap">
                                                    <thead>
                                                        <tr>
                                                            <th>Order Details</th>
                                                            <th>Category</th>
                                                            <th>Amount Paid</th>
                                                            <th>Status</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php if (empty($services_orders)) { ?>
                                                            <tr>
                                                                <td colspan="5">
                                                                    <br><br>

                                                        <center><h4> -- No Data Found -- </h4></center>
                                                        </td>
                                                        </tr>
                                                    <?php } ?>
                                                    <?php foreach ($services_orders as $order) { ?>
                                                        <tr>
                                                            <td>
                                                                <div class="media mt-0 mb-0">
                                                                    <div class="card-aside-img">
                                                                        <a href="#"></a>
                                                                        <img src="<?= base_url('uploads/services_categories/') . $order->service_category->web_image ?>" alt="img">
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <div class="card-item-desc ml-4 p-0 mt-2">
                                                                            <a href="javascript:void(0);" class="text-dark"><h4 class="font-weight-semibold">#<?= $order->order_id ?></h4></a>
                                                                            <a href="javascript:void(0);"><small><?= $order->visit_and_quote_val ?></small></a><br>
                                                                            <a href="javascript:void(0);"><i class="fa fa-clock mr-1"></i> <?= date('M-d-Y, h:i A', $order->created_at) ?></a><br>
                                                                            <?php if (!empty($order->completed_at)) { ?>
                                                                                <a href="javascript:void(0);"><i class="fa fa-check mr-1"></i> <?= date('M-d-Y, h:i A', $order->created_at) ?></a><br>
                                                                            <?php } ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td><?= $order->service_category->name ?></td>
                                                            <td class="font-weight-semibold fs-16"><i class="far fa-rupee-sign"></i> <?= number_format($order->amount_paid, 2) ?></td>
                                                            <td>
                                                                <span class="badge badge-pill <?= $order->status_color ?>"><?= $order->order_status_text ?></span>
                                                            </td>
                                                            <td>
                                                                <div class="status-view">

                                                                    <a href="<?= base_url('my-bookings/view/') . $order->order_id ?>" class="btn btn-sm bg-info-light"><i class="fa fa-eye" aria-hidden="true"></i> View</a></div>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } else { ?>
    <div class="container-fluid" style="min-height: 400px">
        <h3 style="text-align: center; margin-top: 160px"> Please Login to Continue </h3>
    </div>
<?php } ?>
<!--/User Dashboard-->

<!-- Newsletter-->

<!--/Newsletter-->