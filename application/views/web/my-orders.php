<?php include 'includes/ecom_header.php'; ?>

            <div>
      <div class="cover-image sptb-1 bg-background" data-image-src="../assets/images/banners/banner1.jpg">
        <div class="header-text1 mb-0">
          <div class="container">
            <div class="row">
              <div class="col-xl-10 col-lg-12 col-md-12 d-block ">
                <div class=" breadcrumb-banner text-left text-white ">
                  <h1 ><?php //echo $shop_name; ?></h1>
                </div>
                <div>
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                      <a href="index.php"><?php //echo $shop_name; ?>
                      </a>
                    </li>
                    
                    <li class="breadcrumb-item active" aria-current="page"><a href="<?php echo base_url(); ?>web/store/<?php echo $shop_seourl; ?>/shop"><?php //echo $subcategory_title; ?></a>
                    </li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /header-text -->
      </div>
    </div>



    <section class="sptb">
        <div class="container">
            <div class="row">
                <?php include 'dashboard-left-menu.php'; ?>
                <div class="col-xl-9 col-lg-12 col-md-12">
                   

                    <div class="card">

                        <div class="card-body">
                            <div class="card-pay">
                                <ul class="tabs-menu nav">
                                    <li><a href="<?= base_url('my-bookings/ongoing') ?>" class="<?= ($view_filter == 'ongoing') ? 'active' : '' ?>"> On Going</a></li>
                                    <li><a href="<?= base_url('my-bookings/completed') ?>" class="<?= ($view_filter == 'completed') ? 'active' : '' ?>"> Completed</a></li>
                                    <li><a href="<?= base_url('my-bookings/cancelled') ?>" class="<?= ($view_filter == 'cancelled') ? 'active' : '' ?>">  Cancelled</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane show active" id="tab1">
                                        <div class="card-body">
                                            <div class="my-favadd table-responsive userprof-tab">
                                                <table class="table table-striped">
                     <thead class="bg-blue text-white">
                        <tr>
                          <th>Order Id</th>
                          <th>Payment Status</th>
                          <th>Price</th>
                          <th>Seller</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                    <?php 
                    if(count($orders['orders'])>0){
                    foreach($orders['orders'] as $ord){ ?>
                       <tr>
                       <td data-title="Product Image">#<?php echo $ord['id']; ?></td>
                       <td data-title="Product Details" class="text-left"><?php echo $ord['payment_status_name']; ?></td>


                       <td data-title="Price"><i class="fal fa-rupee-sign"></i> <?php echo $ord['amount']; ?></td>


                       <td data-title="Seller"><small class="text-muted"><?php echo $ord['vendor_name']; ?></small></td>
                       <td data-title="Status"><a class="btn btn-sm btn-primary"> <?php echo $ord['service_status']; ?> </a></td>
                        <td data-title="Status"><a href="<?php echo base_url(); ?>web/orderview/<?php echo $ord['id']; ?>" class="btn btn-sm btn-success">View </a></td>

                     </tr>
                   <?php } }else{ ?>

                    <tr>
                          <td colspan="6">
                               <img src="<?php echo base_url();?>/uploads/my_order.png" style="width: 150px">
                                <h4 style="text-align: center;">No Orders </h4>
                          </td>
                        </tr>
                   <?php } ?>
                    <!--  <tr>
                       <td data-title="Product Image"><img src="assets/img/op-2.jpg" alt="" class="orderimg"></td>
                       <td data-title="Product Details" class="text-left">Product Title <br>6 GB Ram</td>
                       <td data-title="Price"><i class="fal fa-rupee-sign"></i> 12000</td>
                       <td data-title="Seller"><small class="text-muted"> Cell Point</small></td>
                        <td data-title="Status"><a href="#" class="btn btn-sm btn-success"> Ongoing</a></td>
                     </tr>
                     <tr>
                       <td data-title="Product Image"><img src="assets/img/op-3.jpg" alt="" class="orderimg"></td>
                       <td data-title="Product Details" class="text-left">Product Title <br>6 GB Ram</td>
                       <td data-title="Price"><i class="fal fa-rupee-sign"></i> 12000</td>
                       <td data-title="Seller"><small class="text-muted">Cell Point</small></td>
                       <td data-title="Status"><a href="#" class="btn btn-sm btn-danger"> Ongoing</a></td>
                     </tr> -->
                     </tbody>
                    </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

 
        

    


<?php include 'includes/footer.php'; ?>