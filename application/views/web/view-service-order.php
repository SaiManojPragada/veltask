<!--Breadcrumb-->
<section>
    <div class="bannerimg cover-image bg-background3" data-image-src="<?= base_url('web_assets') ?>/assets/images/banners/banner2.jpg">
        <div class="header-text mb-0">
            <div class="container">
                <div class="text-center text-white">
                    <h1 class="">My Dashboard</h1>
                    <ol class="breadcrumb text-center">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active text-white" aria-current="page">My Orders</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Breadcrumb-->

<!--User Dashboard-->
<section class="sptb">
    <div class="container">
        <div class="row">
            <?php include 'dashboard-left-menu.php'; ?>
            <div class="col-xl-9 col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title mb-0 p-3">Booking Details <?= ($order->order_status == "order_cancelled" ) ? "<span style='color: tomato'>(Order Cancelled)</span>" : "" ?> <?= ($order->order_status == "refunded" ) ? "<span style='color: tomato'>(Order Cancelled and Refunded)</span>" : "" ?></h4>
                        <a style="position: absolute; right: 30px; cursor: pointer" class="cart-btn-menu" onclick="history.back();"><i class="far fa-angle-left"></i> Back</a>


                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="biller-info">
                                    <?= $order->visit_and_quote_val ?>
                                    <span class="d-block text-sm text-muted"><b>Booking ID :</b> <span class="value">#<?= $order->order_id ?></span></span>
                                    <span class="d-block text-sm text-muted"><b>Category Name :</b> <span class="value"><?= $service_category->name ?></span></span>
                                    <span class="d-block text-sm text-muted"><b>Time Slot :</b> <span class="value"><?= date("d M Y", strtotime($order->time_slot_date)) . ", " . ($order->time_slot); ?></span></span>
                                    <span class="d-block text-sm text-muted"><b>Ordered On : </b> <span class="value"><?= date('d M Y, h:i A', $order->created_at) ?></span></span>
                                    <?php if (!empty($order->accepted_at)) { ?>
                                        <span class="d-block text-sm text-muted"><b>Order Accepetd On : </b> <span class="value"><?= date('d M Y, h:i A', $order->accepted_at) ?></span></span>
                                    <?php } ?>
                                    <?php if (!empty($order->rejected_at)) { ?>
                                        <span class="d-block text-sm text-muted"><b>Order Rejected On : </b> <span class="value"><?= date('d M Y, h:i A', $order->rejected_at) ?></span></span>
                                    <?php } ?>
                                    <?php if (!empty($order->started_at)) { ?>
                                        <span class="d-block text-sm text-muted"><b>Order Started On : </b> <span class="value"><?= date('d M Y, h:i A', $order->started_at) ?></span></span>
                                    <?php } ?>
                                    <?php if (!empty($order->completed_at)) { ?>
                                        <span class="d-block text-sm text-muted"><b>Order Completed On : </b> <span class="value"><?= date('d M Y, h:i A', $order->completed_at) ?></span></span>
                                    <?php } ?>
                                    <?php if (!empty($order->cancelled_at)) { ?>
                                        <span class="d-block text-sm text-muted"><b>Order Cancelled On : </b> <span class="value"><?= date('d M Y, h:i A', $order->cancelled_at) ?></span></span>
                                    <?php } ?>

                                </div>
                            </div>
                            <div class="col-sm-6 text-sm-right">
                                <div class="billing-info">
                                    <h4 class="d-block">Booking Address</h4>
                                    <span class="d-block text-muted"><strong><?= $user_address->name ?></strong></span>
                                    <p class="d-block text-muted">
                                        <span class="d-block"><b>Address Type : </b><?php
                                            if ($user_address->address_type == "1") {
                                                echo 'Home';
                                            } else if ($user_address->address_type == "2") {
                                                echo "Office";
                                            } else {
                                                echo "Other";
                                            }
                                            ?></span>
                                        <span class="d-block"><?= $user_address->address ?></span>
                                        <span class="d-block"><?= $user_address->landmark ?></span>
                                    </p>
                                </div>
                            </div>

                            <?php if ($order->accepted_by_service_provider) { ?>
                                <div class="col-sm-12">
                                    <hr>
                                </div>
                                <div class="col-sm-12" style="padding-left: 20px">
                                    <div class="biller-info">
                                        <h3>Service Provider Details</h3>
                                        <div class="row">
                                            <div class="col-2">
                                                <span class="d-block">
                                                    <img src="<?= base_url('uploads/service_providers/') . $service_provider->photo ?>" style="border-radius: 100px; object-fit: cover" width="100" height="100" alt="service-provider" title="<?= $service_provider->name ?>"/>
                                                </span>
                                            </div>
                                            <div class="col-10">
                                                <span class="d-block text-sm text-muted"><b>Name :</b> <span class="value"><?= $service_provider->name ?></span></span>
                                                <span class="d-block text-sm text-muted"><b>Mobile :</b> <a href="tel:<?= $service_provider->phone ?>"><span class="value"><?= $service_provider->phone ?></span></a></span>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($order->order_status == "order_completed") { ?>
                                <div class="col-sm-12" style="padding-top: 20px; padding-right: 20px">
                                    <a href="<?= base_url('my-bookings/write-review/') . $order_id ?>"><button class="btn btn-outline-danger float-right">Write a Review</button></a>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="mt-5">
                            <div class="service-table">
                                <div class="table-responsive">
                                    <table class="table table-hover order-booking-view text-center">
                                        <thead>
                                            <tr >
                                                <th class="text-left" colspan="2">Services Details
                                                    <?php if ($order->has_visit_and_quote == "Yes" && !empty($quotation)) { ?>
                                                        <a href="javascript:void(0);" data-toggle='modal' data-target='#view_milestones'><span style="float: right; font-weight: normal; color: green">
                                                                <i class="fa fa-money-bill-alt"></i>&nbsp;&nbsp;View Milestones
                                                            </span></a>
                                                    <?php } ?>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($order_services as $item) { ?>
                                                <tr>
                                                    <th><b><?= $item->service_item->service_name ?> &times; (<?= $item->quantity ?>):</b> <?= ($item->is_addon) ? "<small style='color: tomato'>(Add-on) </small>" : '' ?></th>
                                                    <td><i class="fal fa-rupee-sign"></i> <?= number_format(($order->has_visit_and_quote == 'Yes') ? $item->visiting_charges : $item->sub_total, 2) ?></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div><!--service-table-->
                            <div class="Pyment-details">
                                <div class="table-responsive">
                                    <table class="table table-hover order-booking-view">
                                        <thead>
                                            <tr >
                                                <th class="text-left" colspan="2">Payment Details 
                                                    <?php if ($order->has_visit_and_quote == 'No') { ?>
                                                        <span style="float: right; margin-right: 20px">
                                                            <small><b>Payment Status : </b> <?= $show_payment_status ?></small>
                                                        </span>
                                                    <?php } ?>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <tr>
                                                <th><b style="float: right">Sub Total :</b></th>
                                                <td><i class="fal fa-rupee-sign"></i> <?= number_format($order->sub_total, 2) ?></td>
                                            </tr>
                                            <?php if (!empty($order->used_wallet_amount) && $order->used_wallet_amount != 0) { ?>
                                                <tr>
                                                    <th><b style="float: right">Used Wallet Amount :</b></th>
                                                    <td style="color: tomato">- <i class="fal fa-rupee-sign"></i> <?= number_format($order->used_wallet_amount, 2) ?></td>
                                                </tr>
                                            <?php } ?>

                                            <?php if (!empty($order->coupon_discount) && $order->coupon_discount != 0) { ?>
                                                <tr>
                                                    <th><b style="float: right">Coupon Discount :</b></th>
                                                    <td style="color: tomato">- <i class="fal fa-rupee-sign"></i> <?= number_format($order->coupon_discount, 2) ?></td>
                                                </tr>
                                            <?php } ?>

                                            <?php if (!empty($order->membership_discount) && $order->membership_discount != 0) { ?>
                                                <tr>
                                                    <th><b style="float: right">Membership Discount :</b></th>
                                                    <td style="color: tomato">- <i class="fal fa-rupee-sign"></i> <?= number_format($order->membership_discount, 2) ?></td>
                                                </tr>
                                            <?php } ?>

                                            <?php if (!empty($order->bussiness_discount) && $order->bussiness_discount != 0) { ?>
                                                <tr>
                                                    <th><b style="float: right">Business Discount :</b></th>
                                                    <td style="color: tomato">- <i class="fal fa-rupee-sign"></i> <?= number_format($order->bussiness_discount, 2) ?></td>
                                                </tr>
                                            <?php } ?>

                                            <?php if (!empty($order->discount) && $order->discount != 0 && $quotation->no_of_milestones_paid != 0) { ?>
                                                <tr>
                                                    <th><b style="float: right">Discount :</b></th>
                                                    <td style="color: tomato">- <i class="fal fa-rupee-sign"></i> <?= number_format($order->discount, 2) ?></td>
                                                </tr>
                                            <?php } ?>
                                            <?php if ($order->has_visit_and_quote == 'No') { ?>
                                                <tr>
                                                    <th><b style="float: right">Safety & Visitation Fee :</b></th>
                                                    <td><i class="fal fa-rupee-sign"></i> <?= number_format($order->visiting_charges, 2) ?></td>
                                                </tr>
                                            <?php } ?>

                                            <tr>
                                                <th><b style="float: right">Tax :</b></th>
                                                <td><i class="fal fa-rupee-sign"></i> <?= number_format($order->tax, 2) ?></td>
                                            </tr>
                                            <tr>
                                                <th><b style="float: right">Grand Total :</b></th>
                                                <td style="color: green; font-weight: bold"><i class="fal fa-rupee-sign"></i> <?= number_format($order->grand_total, 2) ?></td>
                                            </tr>
                                            <tr>
                                                <th><b style="float: right">Amount Paid :</b></th>
                                                <td><i class="fal fa-rupee-sign"></i> <?= number_format($order->amount_paid, 2) ?></td>
                                            </tr>
                                            <?php if ($order->order_status != "order_completed" && $order->balance_amount > 0) { ?>
                                                <tr>
                                                    <th><b style="float: right">Balance Amount :</b></th>
                                                    <td style="color: orange"><i class="fal fa-rupee-sign"></i> <?= number_format($order->balance_amount, 2) ?></td>
                                                </tr>
                                            <?php } ?>

                                            <tr>
                                                <?php $grace_period = $this->my_model->get_data_row("service_cancellation_grace_period")->duration; ?>
                                                <?php if (($order->order_status == "order_accepted" || $order->order_status == "order_started") && $order->balance_amount > 0 && $order->has_visit_and_quote == "No") { ?>
                                                    <td colspan="2">
                                                        <button class="btn my-custom-btn float-right" style="margin-right: 40px" onclick="location.href = '<?= base_url() ?>payment/pre_pay_balance/<?= $order->order_id ?>'">Pay Balance <i class="fal fa-rupee-sign"></i> <?= $order->balance_amount ?></button>
                                                    </td>
                                                <?php } ?>
                                                <?php if ($order->order_status == "order_placed") { ?>
                                                    <td colspan="2">

                                                        <span id="timer-block" style="float: left">
                                                            <strong style="color: tomato;">Note : </strong><?= $order->cancellation_message ?>
                                                        </span>
                                                        <button class="btn my-custom-btn float-right" style="margin-right: 40px" data-toggle="modal" data-target="#cancelModal">Cancel Order</button>
                                                    </td>
                                                <?php } ?>
                                            </tr>

                                        </tbody>
                                    </table>
                                    <hr>
                                    <?php if ($order->order_status == "refunded") { ?>
                                        <p><b>Admin Refund Details : </b><?= $order->admin_cancellation_msg ?></p>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="row">
                                <div class="col-md-12">
                                        <div class="submit-section">
                                                <button type="reset" class="btn bg-danger-light"><i class="fal fa-times"></i> Cancel</button>
                                        </div>
                                </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade " tabindex="-1" role="dialog" id="cancelModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Cancel Order <b>#<?= $order_id ?></b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php if ($order->order_status == "order_placed") { ?>
                    <p><strong style="color: tomato">Note : </strong> <?= $order->cancellation_message ?></p>
                <?php } ?>
                <textarea class="form-control" id="cancellation_reason" rows="5"></textarea>
                <p id="cancellation_reason_err"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="cancelOrder();">Cancel Order</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" id="view_milestones" style="width: 100%">
    <div class="modal-dialog modal-xl" role="document" style="width: 100%">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Visit and Quote Milestones</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php if (!empty($quotation)) { ?>
                    <div class="col-12 table-responsive">
                        <table class="table-hover table" style="width: 100%">
                            <thead style="width: 100%">
                                <tr>
                                    <th colspan="2" style="text-align: center"><strong>No of Milestones : </strong><span style="font-weight: normal"><?= $quotation->no_of_milestones ?></span></th>
                                    <th colspan="2" style="text-align: center"><strong>Total Amount : </strong><span style="font-weight: normal; color: green"><i class="fal fa-rupee-sign"></i> <?= number_format($quotation->quotation_amount) ?> <small style="color: red">(- <i class="fa fa-rupee-sign"></i> <?= $order->discount ?> discount)</small></span></th>
                                    <th colspan="2" style="text-align: center"><strong>Payment Status : <?= ($quotation->no_of_milestones == $quotation->no_of_milestones_paid) ? '<span style="color: green">PAID</span>' : '<span style="color: red">Pending</span>' ?></strong></th>
                                </tr>
                                <tr>
                                    <th colspan="3" style="text-align: center"><strong>Approx Work Duration : </strong><?= $quotation->work_duration ?></th>
                                    <th colspan="3" style="text-align: center"><strong>Total Milestones Paid : </strong><?= $quotation->no_of_milestones_paid ?></th>
                                </tr>
                                <tr>
                                    <th colspan="6" style="text-align: center"><strong>Project Details : </strong>
                                        <p ><?= $quotation->project_details ?></p>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <br>
                    <?php if ($quotation->no_of_milestones_paid < $quotation->no_of_milestones) { ?>
                        <div style="padding: 0px 30px 30px 0px">
                            <a href="<?= base_url('payment/milestone/' . $quotation->id . '/all') ?>"><button class="btn btn-primary" style="float: right">Pay All</button></a>
                        </div>
                    <?php } ?>
                    <div class="ml-2 mr-2 p-3">
                        <h4><b>Milestones</b></h4>
                        <?php foreach ($milestones as $index => $mile) { ?>
                            <div class="card" style="padding: 20px 10px">
                                <div style="padding: 10px 10px; margin-bottom: 10px;border-bottom: 1px solid lightgray">
                                    <center><strong>Milestone <?= $index + 1 ?></strong></center>
                                </div>
                                <div class="row" style="padding: 10px 10px; margin-bottom: 10px;border-bottom: 1px solid lightgray">
                                    <div class="col-6">
                                        <p style="text-align: center"><strong>Milestone Amount : </strong><i class="fal fa-rupee-sign"></i> <?= number_format($mile->amount, 2) ?></p>
                                    </div>
                                    <div class="col-6">
                                        <p style="text-align: center"><strong>Date : </strong><i class="fal fa-calendar"></i> <?= date('d M Y', strtotime($mile->date)) ?></p>
                                    </div>
                                    <!--                                    <div class="col-12" style="padding: 12px 36px 0px 22px">
                                                                            <p style="text-align: left"><strong>Description : </strong><br><?= $mile->description ?></p>
                                                                        </div>-->
                                </div>
                                <div class="col-12">
                                    <?php if ($mile->status == "paid") { ?>
                                        <button class="btn btn-primary" style="float: right; cursor: not-allowed" disabled>Paid</button>
                                    <?php } else { ?>
                                        <a href="<?= base_url('payment/milestone/' . $mile->quotation_id . '/' . $mile->id) ?>"><button class="btn btn-primary" style="float: right">Pay <i class="fal fa-rupee-sign"></i> <?= number_format($mile->amount, 2) ?></button></a>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                <?php } else { ?>
                    <center style='margin: 30px'><h4>-- No Quotation Found --</h4></center>
                <?php } ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<script>
    function cancelOrder() {
        var cancellationReason = $("#cancellation_reason").val();
        if (cancellationReason.length > 2) {
            $("#cancellation_reason_err").html("");
            $.ajax({
                url: "<?= base_url('api/orders/cancel_order') ?>",
                type: "post",
                data: {user_id: "<?= USER_ID ?>", order_id: "<?= $order->id ?>", "cancellation_reason": cancellationReason},
                success: function (resp) {
                    resp = JSON.parse(resp);
                    if (resp['status']) {
                        swal({
                            title: resp['message'],
                            icon: "success"
                        }).then(function () {
                            location.href = '';
                        });
                    } else {
                        swal({
                            title: resp['message'],
                            icon: "info"
                        });
                    }
                }
            });
        } else {
            $("#cancellation_reason_err").html("Please enter a valid Reason.");
        }

    }
</script>