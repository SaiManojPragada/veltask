<div>
    <div class="cover-image sptb-1 bg-background" data-image-src="../assets/images/banners/banner1.jpg">
        <div class="header-text1 mb-0">
            <div class="container">
                <div class="row">
                    <div class="col-xl-10 col-lg-12 col-md-12 d-block ">
                        <div class=" breadcrumb-banner text-left text-white ">
                            <h1 > Checkout
                            </h1>
                        </div>
                        <div>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="<?= base_url() ?>">Home
                                    </a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="javascript:history.go(-2);">Services
                                    </a>
                                </li>
                                <li class="breadcrumb-item"><a href="javascript:history.back();">View Details</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page"><a href="add-address-book.php">Select Address and Checkout</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /header-text -->
    </div>
</div>
<!--/Sliders Section-->
<!--Add listing-->
<section class="sptb">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 col-lg-8 col-md-12">
                <!--Classified Description-->
                <div class="card overflow-hidden">
                    <div class="d-md-flex">
                        <div class="card pl-4 pr-4 pb-5 border-0 mb-0">
                            <div class=" mt-2 pt-2 pb-1 ">
                                <div class="item-card2-footer d-sm-flex">
                                    <div class="item-card2-rating">
                                        <div class="rating-stars d-inline-flex">
                                            <ul class="footer-list">
                                                <li>
                                                    <div class="service-price">
                                                        <p class="mb-0">
                                                            <span class="address"><i class="fa fa-clock" aria-hidden="true"></i> Select Time Slots</span>

                                                        </p>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <?php
                            if (!empty($this->session->userdata('time_slots_date'))) {
                                $selected_date = date('Y-m-d', strtotime($this->session->userdata('time_slots_date')));
                            }
                            ?>
                            <div class="card-body pt-2 pb-3 pl-0 pr-0">
                                <div class="address-parnt">
                                    <form action="javascript:void(0);" onsubmit="javascript:void(0);" method="post" id="timeslot_form">

                                        <div class="row">
                                            <div class="col-6">
                                                <label class="control-label">Select Time Slot Date</label>
                                                <input type="date" id="time_slot_date" class="form-control"
                                                       required data-parsley-required-message="Select TImeslot Date" min="<?= date('Y-m-d') ?>" max="<?= date('Y-m-d', strtotime("+30 days")) ?>"
                                                       onchange="getTimeSlots(this.value)" value="<?= $selected_date ?>" />
                                            </div> 
                                            <div class="col-6">
                                                <label class="control-label">Select Time Slot Time</label>
                                                <select class="form-control" id="time_slot_time"  required data-parsley-required-message="Select TImeslot Time" onchange="save_time_slots();" >
                                                    <option value="">-- Select Time slots --</option>
                                                    <?= $time_slots_string ?>
                                                </select>
                                            </div>
                                        </div>
                                    </form>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card overflow-hidden ">
                    <div class="d-md-flex">
                        <div class="card pl-4 pr-4 pb-5 border-0 mb-0">
                            <div class=" mt-2 pt-2 pb-1 ">


                                <div class="item-card2-footer d-sm-flex">
                                    <div class="item-card2-rating">
                                        <div class="rating-stars d-inline-flex">
                                            <ul class="footer-list">
                                                <li>
                                                    <div class="service-price">
                                                        <p class="mb-0">
                                                            <span class="address"><i class="fa fa-map-marker" aria-hidden="true"></i> Address</span>

                                                        </p>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>


                            <div class="card-body pt-2 pb-3 pl-0 pr-0">
                                <?php foreach ($addresses as $index => $add) { ?>
                                    <div class="address-parnt">
                                        <p id="json-holder-<?= $index ?>" style="display: none;"><?= $add->json_string ?></p>
                                        <label class="custom-control custom-radio mb-2 mr-4">
                                            <input type="radio" class="custom-control-input" value="<?= $add->id ?>" name="select_address" <?= ($add->isdefault == 'yes') ? 'checked' : ' onchange="checkthis(`' . $add->id . '`);"' ?>>
                                            <span class="custom-control-label"><a href="javascript:void(0);" class="text-muted"><?php
                                                    if ($add->address_type == '1') {
                                                        echo 'Home';
                                                    } else if ($add->address_type == '2') {
                                                        echo 'Office';
                                                    } else {
                                                        echo 'Other';
                                                    }
                                                    ?></a></span>
                                        </label>
                                        <p><?= $add->name ?></p>
                                        <p><?= $add->address . ', ' . $add->landmark . ',<br>' . $add->state_name . ', ' . $add->city_name . ', ' . $add->pincode_pin ?></p>
                                        <p>
                                            <a href="javascript:void(0);"><span class="" style="color: #F15F74; font-size: 12px; font-weight: bold" onclick="postthisforedit('<?= $index ?>');"><i class="far fa-edit" aria-hidden="true"></i> Change</span>
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                <span class="" style="color: #F15F74; font-size: 12px; font-weight: bold" onclick="deletethis('<?= $add->id ?>');"><i class="far fa-trash-alt" aria-hidden="true"></i> Delete</span></a>
                                            <span style="font-size: 12px; float: right"><small><b>Last updated On : </b><?= date('d M Y, h:i A', strtotime($add->created_date)) ?></small></span><br></p>
                                    </div>
                                <?php } if (empty($addresses)) { ?>
                                    <center>
                                        <h3>-- No Addresses Found --</h3>
                                    </center>
                                <?php } ?>

                            </div>
                            <div class="change-address btn-primary" id="search-btn" style="padding: 0px"><div onclick="reset_data();" style="height: 48.01px; width: 180px; padding: 10px 20px"><i class="fa fa-plus" aria-hidden="true"></i> Add New Address</div>
                            </div>
                            <!--<div class="btn-primary" id="search-btn" style="padding: 0px"><div onclick="location.href = '<?= base_url('my-addresses') ?>';" style="height: 48.01px; width: 180px; padding: 10px 20px"><i class="fa fa-plus" aria-hidden="true"></i> Add New Address</div>-->
                            <!--</div>-->
                            <div class="add-address-form pt-5 pl-5 pr-5 pb-4" id="search-view">

                                <form method="post" action="<?= base_url('my-addresses') ?>" id="address-form">
                                    <input type="hidden" id="prev_id" name="prev_id">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Name *</label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name"<!-- minlength="3" data-parsley-pattern="^[a-zA-Z\s]*$" data-parsley-pattern-message="Please Enter Valid Name" data-parsley-required-message="Please Enter Valid Name" required>

                                    </div>
                                    <div class="form-group">
                                        <label for="Number">Mobile Number *</label>
                                        <input type="Number" class="form-control" id="mobile" name="mobile" placeholder="Enter Mobile Number"
                                               min='1' data-parsley-minlength="10"  data-parsley-maxlength="10" data-parsley-required-message="Please Enter Mobile Number" data-parsley-minlength-message="Enter Valid Mobile Number *" data-parsley-maxlength-message="Enter Valid Mobile Number" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Address *</label>
                                        <textarea class="form-control" id="address" name="address" placeholder="Enter Address Line 1" required
                                                  data-parsley-required-message="Please Enter Address"  ></textarea>

                                    </div>

                                    <div class="row">
                                        <div class="form-group col-4">
                                            <label for="exampleInputEmail1">State *</label>
                                            <select name="state" id="state" class="form-control" onchange="getCities(this.value)" required data-parsley-required-message="Please Select a State">
                                                <option value="">-- select State --</option>
                                                <?php foreach ($states as $state) { ?>
                                                    <option value="<?= $state->id ?>"><?= $state->state_name ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>


                                        <div class="form-group col-4">
                                            <label for="exampleInputEmail1">City *</label>
                                            <select name="city" id="cities" class="form-control" onchange="getPincodes(this.value)" required data-parsley-required-message="Please Select a City">
                                                <option value="">-- select City --</option>

                                            </select>
                                        </div>


                                        <div class="form-group col-4">
                                            <label for="exampleInputEmail1">Pincode *</label>
                                            <select name="pincode" id="pincode" class="form-control" required  data-parsley-required-message="Please Select a Pincode">
                                                <option value="">-- select Pincode --</option>

                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="location">Landmark *</label>
                                        <input type="text" class="form-control" id="landmark" name="landmark" placeholder="LandMark" required
                                               data-parsley-required-message="Please Enter Landmark" >

                                    </div>

                                    <div class="form-group">
                                        <label>Address Type *</label>
                                        <div class="input-group">
                                            <input type="radio" id="home" name="address_type" value="1" required  data-parsley-required-message="Please Select Address Type"/>
                                            <label for="home"> &nbsp;&nbsp;Home</label>&nbsp;&nbsp;

                                            <input type="radio" id="office" name="address_type" value="2" required/>
                                            <label for="office"> &nbsp;&nbsp;Office</label>&nbsp;&nbsp;


                                            <input type="radio" id="other" name="address_type" value="3" required/>
                                            <label for="other"> &nbsp;&nbsp;Other</label>&nbsp;&nbsp;
                                        </div>

                                    </div>



                                    <div class="form-group mb-0">
                                        <button type="submit" class="btn btn-primary submit-form">Save Address</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!--Right Side Content-->
            <div class="col-xl-4 col-lg-4 col-md-12">
                <div class="contine-page-right-side">
                    <div class="continue-item-1">

                        <div class="card">
                            <div class="card-body p-5">
                                <div class="total-item d-flex ">
                                    <div class="availble-oofer-content">
                                        <p class="mb-0">Item Total
                                        </p>
                                    </div>
                                    <div class="total-price ml-auto">
                                        <span>₹ <?= $cart_totals->sub_total ?>
                                        </span>
                                    </div>
                                </div>
                                <!--offer-card-->
                                <?php if ($cart[0]->has_visit_and_quote == 0) { ?>
                                    <div class="total-item d-flex ">
                                        <div class="availble-oofer-content">
                                            <p class="mb-0">Safety & Visitation Fee
                                            </p>
                                        </div>
                                        <div class="total-price ml-auto">
                                            <span><span style="color: green">+</span> ₹ <span id="visiting_charges"><?= $cart_totals->visiting_charges ?></span>
                                            </span>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="total-item d-flex ">
                                    <div class="availble-oofer-content">
                                        <p class="mb-0">Tax
                                        </p>
                                    </div>
                                    <div class="total-price ml-auto">
                                        <span><span style="color: green">+</span> ₹ <?= $cart_totals->tax ?>
                                        </span>
                                    </div>
                                </div>

                                <?php if ($cart_totals->membership_discount > 0) { ?>
                                    <div class="total-item d-flex ">
                                        <div class="availble-oofer-content">
                                            <p class="mb-0">Membership Discount
                                            </p>
                                        </div>
                                        <div class="total-price ml-auto">
                                            <span><span style="color: tomato">-</span> ₹ <?= $cart_totals->membership_discount ?>
                                            </span>
                                        </div>
                                    </div>
                                <?php } ?>


                                <?php if ($cart_totals->business_discount > 0) { ?>
                                    <div class="total-item d-flex ">
                                        <div class="availble-oofer-content">
                                            <p class="mb-0">Business Discount
                                            </p>
                                        </div>
                                        <div class="total-price ml-auto">
                                            <span><span style="color: tomato">-</span> ₹ <?= $cart_totals->business_discount ?>
                                            </span>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="total-item d-flex " id="coupon_discount_div" style="<?= (!empty($coupon_and_wallet['coupon_code'])) ? '' : 'display: none !important' ?>">
                                    <div class="availble-oofer-content">
                                        <p class="mb-0">Coupon Discount
                                        </p>
                                    </div>
                                    <div class="total-price ml-auto">
                                        <span><span style="color: tomato">-</span> ₹ <span id="coupon_amount"> <?= $coupon_and_wallet['coupon_discount'] ?></span>
                                        </span>
                                    </div>
                                </div>

                                <div class="total-item d-flex " id="used_wallet_amount_div" style="<?= ($coupon_and_wallet['wallet_applied_amount'] > 0) ? "" : "display: none !important;" ?>">
                                    <div class="availble-oofer-content">
                                        <p class="mb-0">Used Wallet Amount
                                        </p>
                                    </div>
                                    <div class="total-price ml-auto">
                                        <span><span style="color: tomato">-</span> ₹ <span id="used_wallet_amount"><?= $coupon_and_wallet['wallet_applied_amount'] ?></span>
                                        </span>
                                    </div>
                                </div>

                                <!--offer-card-->
                            </div>
                            <div class="card-footer pt-2 pb-2">
                                <div class="offer-footer d-sm-flex">
                                    <div class="offer-footer-child">
                                        <div class="rating-stars d-inline-flex">
                                            <ul class="footer-list">
                                                <li class="">
                                                    <div class="service-price">
                                                        <p class="mb-0">Total
                                                        </p>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="ml-auto remove-btn">
                                        <span>₹<span id="cart_grand_total"><?= (!empty($coupon_and_wallet['grand_total']) || $cart_totals->total == $coupon_and_wallet['wallet_applied_amount']) ? $coupon_and_wallet['grand_total'] : $cart_totals->total ?></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        $sttt_total = $cart_totals->total;
                        if (!empty($coupon_and_wallet['grand_total']) || $cart_totals->total == $coupon_and_wallet['wallet_applied_amount']) {
                            $sttt_total = $coupon_and_wallet['grand_total'];
                        }
                        ?>
                        <?php if ($sttt_total > 0) { ?>
                            <div class="will-pay-button p-1" onclick="proceed_to_payment_selection();">
                                <div class="will-pay">
                                    <button class="btn btn-light"><a href="javascript:void(0);">Proceed</a>
                                    </button>
                                </div>
                            </div> 
                        <?php } else { ?>
                            <div class="will-pay-button p-1" onclick="place_order();">
                                <div class="will-pay">
                                    <button class="btn btn-light"><a href="javascript:void(0);">Place Order</a>
                                    </button>
                                </div>
                            </div> 
                            <script>
                                var aj_data = <?= $out_data ?>;
                                var custom = "<?= $custom_url ?>";
                                function place_order() {
                                    aj_data.time_slot_date = $("#time_slot_date").val();
                                    aj_data.time_slot = $("#time_slot_time").val();
                                    var address_id = $("input[name='select_address']").val();
                                    $.ajax({
                                        url: '<?= base_url("api/check_service_providers_availability") ?>',
                                        type: "post",
                                        data: {user_id: '<?= USER_ID ?>', address_id: address_id},
                                        success: function (resp) {
                                            resp = JSON.parse(resp);
                                            if (resp['status']) {
                                                $.ajax({
                                                    url: "<?= base_url('api/') ?><?= ($custom_url) ? $custom_url : 'place_order' ?>",
                                                    type: "post",
                                                    data: aj_data,
                                                    success: function (json) {
                                                        resp = JSON.parse(json);
                                                        console.log(resp);
                                                        if (resp['status']) {
                                                            swal({
                                                                title: resp['message'],
                                                                icon: "success"
                                                            }).then(function () {
                                                                location.href = "<?= base_url('placeorder/success/' . $order_id) ?>";
                                                            });
                                                        } else {
                                                            swal({
                                                                title: resp['message'],
                                                                icon: "info"
                                                            }).then(function () {
                                                                location.href = "<?= base_url('placeorder/failed/' . $order_id) ?>/" + r_o_id + "/" + r_p_id;
                                                            });
                                                        }
                                                    }
                                                });
                                            } else {
                                                swal({
                                                    title: resp['message'],
                                                    icon: "warning"
                                                });
                                            }
                                        }
                                    });

                                }
                            </script>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <!--/Right Side Content-->
        </div>
    </div>
</section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datejs/1.0/date.min.js" integrity="sha512-/n/dTQBO8lHzqqgAQvy0ukBQ0qLmGzxKhn8xKrz4cn7XJkZzy+fAtzjnOQd5w55h4k1kUC+8oIe6WmrGUYwODA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="<?= base_url('web_assets/') ?>/js/plugins/parsleyjs/dist/parsley.min.js"></script>
<script type="text/javascript">

                            var actdate = "<?= $this->session->userdata('time_slots_date') ?>";
                            function proceed_to_payment_selection() {
                                var time_slot_time = $("#time_slot_time").val();
                                if ($("#timeslot_form").parsley().validate()) {
<?php if (empty($addresses)) { ?>
                                        swal({
                                            title: "No Address Selected",
                                            text: "Please Add an Address to Continue",
                                            icon: "warning"
                                        });
<?php } else { ?>
                                        $.ajax({
                                            url: "<?= base_url('api/web/process_time_slots') ?>",
                                            type: "post",
                                            data: {date: actdate, time: time_slot_time},
                                            success: function (resp) {
                                                if (resp) {
                                                    location.href = '<?= base_url('check-out/payment-method') ?>';
                                                } else {
                                                    swal({
                                                        title: "Unable to save Timeslots",
                                                        text: "Please Try Again",
                                                        icon: "info"
                                                    });
                                                }
                                            }
                                        });
<?php } ?>
                                } else {
                                    return false;
                                }
                            }

                            const next_date = "<?= date('Y-m-d', strtotime("+1 day")) ?>";
                            var ext = 0;
                            function getTimeSlots(val) {
                                var date = new Date(val);
                                actdate = date.toString('dd-MM-yyyy');
                                save_time_slots();
                                $.ajax({
                                    url: "<?= base_url('api/get_time_slots') ?>",
                                    type: "post",
                                    data: {date: actdate},
                                    success: function (json) {
                                        var resp = JSON.parse(json);
                                        if (resp['status']) {
                                            var slots = resp['data'];
                                            var op = "<option value=''>-- Select Time slot --</option>";
                                            for (var i = 0; i < slots.length; i++) {
                                                op += '<option value="' + slots[i]['time_slot'] + '">' + slots[i]['time_slot'] + '</option>';
                                            }
                                        } else {
                                            swal({
                                                title: "No Time Slots Found",
                                                icon: "warning"
                                            });
                                            var op = "<option value=''>-- No Time slots Found --</option>";
                                            $("#time_slot_date").val(next_date);
                                            if (ext == 0) {
                                                getTimeSlots(next_date);
                                                ext = 1;
                                            }
                                        }
                                        $("#time_slot_time").html(op);
                                    }
                                });
                            }

                            function save_time_slots() {
                                var time_slot_time = $("#time_slot_time").val();
//                                if (time_slot_time.length < 1) {
//                                    time_slot_time = "a";
//                                }
//                                if (actdate.length < 1) {
//                                    actdate = "a";
//                                }
//                                console.log(actdate + ", " + time_slot_time);
                                $.ajax({
                                    url: "<?= base_url('api/web/process_time_slots') ?>",
                                    type: "post",
                                    data: {date: actdate, time: time_slot_time},
                                    success: function (resp) {
                                        if (resp) {
                                            console.log("Saved");
                                        } else {
                                            console.log("Not Saved");
                                        }
                                    }
                                });
                            }

                            function getCities(state_id) {
                                $.ajax({
                                    url: "<?= base_url('api/admin_ajax/locations/get_cities') ?>",
                                    type: "post",
                                    data: {state_id: state_id},
                                    success: function (resp) {
                                        $("#cities").html(resp);
                                    }
                                });
                            }

                            function getPincodes(city_id) {
                                $.ajax({
                                    url: "<?= base_url('api/admin_ajax/locations/get_pincodes') ?>",
                                    type: "post",
                                    data: {city_id: city_id},
                                    success: function (resp) {
                                        $("#pincode").html(resp);
                                    }
                                });
                            }


                            var open = false;
                            $(document).ready(function () {
                                $('#address-form').parsley();
                                $('#timeslot_form').parsley();
                            });
                            function checkthis(val) {
                                $.ajax({
                                    url: '<?= base_url() ?>my_addresses/change_address_selection',
                                    type: 'post',
                                    data: {id: val},
                                    success: function (resp) {
                                        if (resp == 'success') {
                                            swal('Default Address Changed').then(function () {
                                                location.href = "<?= base_url('check-out') ?>";
                                            });
                                        } else {
                                            swal({
                                                title: 'Unable to Change Your Default Address',
                                                icon: 'warning',
                                                button: "close"
                                            });
                                        }
                                    }
                                });
                            }

                            function postthisforedit(id) {
                                var json = $("#json-holder-" + id).html();
                                json = JSON.parse(json);
                                $("#search-view").css({'display': 'none'});
                                $('#prev_id').val(json['id']);
                                $("#name").val(json['name']);
                                $("#mobile").val(json['mobile']);
                                $("#address").val(json['address']);
                                $("#landmark").val(json['landmark']);
                                $("#state").val(json['state']);
                                if (json['address_type'] == '1') {
                                    $("#home").prop("checked", true);
                                } else if (json['address_type'] == '2') {
                                    $("#office").prop("checked", true);
                                } else {
                                    $("#other").prop("checked", true);
                                }

                                setTimeout(function () {
                                    getCities(json['state']);
                                    getPincodes(json['city']);
                                    setTimeout(function () {
                                        $("#cities").val(json['city']);
                                        $("#pincode").val(json['pincode']);
                                    }, 1000);
                                }, 50);
                                if (open) {
                                    $("#search-btn").click();
                                }
                                open = true;
                            }

                            function reset_data() {
                                $('#prev_id').val("");
                                $("#name").val("");
                                $("#mobile").val("");
                                $("#address").val("");
                                $("#cities").val("");
                                $("#state").val("");
                                $("#pincode").val("");
                                $("#landmark").val("");
                                $("#home").prop("checked", false);
                                $("#office").prop("checked", false);
                                $("#other").prop("checked", false);
                                if (open) {
                                    $("#search-btn").click();
                                }
                            }

                            function deletethis(id) {
                                swal({
                                    title: "Are you sure?",
                                    text: "This address Will be Deleted Permenantly",
                                    icon: "warning",
                                    buttons: [
                                        'No, cancel it!',
                                        'Yes, I am sure!'
                                    ],
                                    dangerMode: true
                                }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $.ajax({
                                            url: '<?= base_url("api/user_profile/delete_address") ?>',
                                            type: 'post',
                                            data: {user_id: '<?= USER_ID ?>', id: id},
                                            success: function (json) {
                                                var arr = JSON.parse(json);
                                                if (arr['status']) {
                                                    swal({
                                                        title: "Success",
                                                        text: "Address has been Deleted",
                                                        icon: "success",
                                                    }).then(function () {
                                                        location.href = "";
                                                    });
                                                } else {
                                                    swal({
                                                        title: "Failed",
                                                        text: "Unable to Delete Address",
                                                        icon: "warning"
                                                    });
                                                }
                                            }
                                        });
                                    }
                                });
                            }
</script>