<!--Sliders Section-->
<div>
    <div class="cover-image sptb-1 bg-background" data-image-src="../assets/images/banners/banner1.jpg">
        <div class="header-text1 mb-0">
            <div class="container">
                <div class="row">
                    <div class="col-xl-10 col-lg-12 col-md-12 d-block ">
                        <div class=" breadcrumb-banner text-left text-white ">
                            <h1 > Blog View
                            </h1>
                        </div>
                        <div>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="<?= base_url() ?>">Home
                                    </a>
                                </li>

                                <li class="breadcrumb-item active" aria-current="page">Blog
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /header-text -->
    </div>
</div>
<!--/Sliders Section-->
<!--Add listing-->
<section class="sptb">
    <div class="container">
        <div class="row justify-content-center" style="min-height: 500px;">
            <?php foreach ($blogs as $blog) { ?>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12" onclick="location.href = '<?= base_url() ?>blogs/<?= $blog->seo_url ?>';">
                    <div class="blg_grid_box">
                        <div class="blg_grid_thumb">
                            <a href="javascript:void(0);"><img src="<?= base_url('uploads/blogs/') . $blog->image ?>" class="img-fluid" alt="<?= $blog->title ?>"></a>
                        </div>
                        <div class="blg_grid_caption">
                            <div class="blg_title"><h4><a href="javascript:void(0);"><?= $blog->title ?></a></h4></div>
                            <div class="blg_desc"><?= $blog->short_description ?></div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php if (empty($blogs)) { ?>
                <div class="col-12" style="padding-top: 200px">
                    <center>
                        <h2> -- No Blogs Found --</h2>
                    </center>
                </div>
            <?php } ?>

        </div>
    </div>
</section>