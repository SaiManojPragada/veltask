<!--Breadcrumb-->
<section>
    <div class="bannerimg cover-image bg-background3" data-image-src="../assets/images/banners/banner2.jpg">
        <div class="header-text mb-0">
            <div class="container">
                <div class="text-center text-white">
                    <h1 class="">Booking Address</h1>
                    <ol class="breadcrumb text-center">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active text-white" aria-current="page">Booking Address</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Breadcrumb-->

<?php if (!empty(USER_ID)) { ?>
    <!--User Dashboard-->
    <section class="sptb">
        <div class="container">
            <div class="row">
                <?php include 'dashboard-left-menu.php'; ?>
                <div class="col-xl-9 col-lg-12 col-md-12">


                    <div class="card notification">
                        <div class="card-header refer-parent p-3">
                            <h5>Booking Addresses</h5>
                        </div>
                        <div class="card-body">
                            <div class="card-body pt-2 pb-3 pl-0 pr-0">
                                <?php foreach ($addresses as $index => $add) { ?>
                                    <div class="address-parnt">
                                        <p id="json-holder-<?= $index ?>" style="display: none;"><?= $add->json_string ?></p>
                                        <label class="custom-control custom-radio mb-2 mr-4">
                                            <input type="radio" class="custom-control-input" value="<?= $add->id ?>" name="select_address" <?= ($add->isdefault == 'yes') ? 'checked' : ' onchange="checkthis(`' . $add->id . '`);"' ?>>
                                            <span class="custom-control-label"><a href="javascript:void(0);" class="text-muted"><?= ($add->address_type == '1') ? 'Home' : ($add->address_type == '2') ? 'Office' : 'Other' ?></a></span>
                                        </label>
                                        <p><?= $add->name ?></p>
                                        <p><?= $add->address . ', ' . $add->landmark . ',<br>' . $add->state_name . ', ' . $add->city_name . ', ' . $add->pincode_pin ?></p>
                                        <p>
                                            <a href="javascript:void(0);"><span class="" style="color: #F15F74; font-size: 12px; font-weight: bold" onclick="postthisforedit('<?= $index ?>');"><i class="far fa-edit" aria-hidden="true"></i> Change</span>
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                <span class="" style="color: #F15F74; font-size: 12px; font-weight: bold" onclick="deletethis('<?= $add->id ?>');"><i class="far fa-trash-alt" aria-hidden="true"></i> Delete</span></a>
                                            <span style="font-size: 12px; float: right"><small><b>Last updated On : </b><?= date('d M Y, h:i A', strtotime($add->created_date)) ?></small></span><br></p>
                                    </div>
                                <?php } if (empty($addresses)) { ?>
                                    <center>
                                        <h3>-- No Addresses Found --</h3>
                                    </center>
                                <?php } ?>

                            </div>


                            <div class="change-address btn-primary" id="search-btn" style="padding: 0px"><div onclick="reset_data();" style="height: 48.01px; width: 180px; padding: 10px 20px"><i class="fa fa-plus" aria-hidden="true"></i> Add New Address</div>
                            </div>

                            <div class="add-address-form pt-5 pl-5 pr-5 pb-4" id="search-view" style="">

                                <form method="post" action="" id="address-form">
                                    <input type="hidden" id="prev_id" name="prev_id">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Name *</label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" minlength="3" data-parsley-pattern="^[a-zA-Z\s]*$" data-parsley-pattern-message="Please Enter Valid Name" data-parsley-required-message="Please Enter Valid Name" required>

                                    </div>
                                    <div class="form-group">
                                        <label for="Number">Mobile Number *</label>
                                        <input type="Number" class="form-control" id="mobile" name="mobile" placeholder="Enter Mobile Number"
                                               min='1' data-parsley-minlength="10"  data-parsley-maxlength="10" data-parsley-required-message="Please Enter Mobile Number" data-parsley-minlength-message="Enter Valid Mobile Number *" data-parsley-maxlength-message="Enter Valid Mobile Number" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Address *</label>
                                        <textarea class="form-control" id="address" name="address" placeholder="Enter Address Line 1" required
                                                  data-parsley-required-message="Please Enter Address"  ></textarea>

                                    </div>

                                    <div class="row">
                                        <div class="form-group col-4">
                                            <label for="exampleInputEmail1">State *</label>
                                            <select name="state" id="state" class="form-control" onchange="getCities(this.value)" required data-parsley-required-message="Please Select a State">
                                                <option value="">-- select State --</option>
                                                <?php foreach ($states as $state) { ?>
                                                    <option value="<?= $state->id ?>"><?= $state->state_name ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>


                                        <div class="form-group col-4">
                                            <label for="exampleInputEmail1">City *</label>
                                            <select name="city" id="cities" class="form-control" onchange="getPincodes(this.value)" required data-parsley-required-message="Please Select a City">
                                                <option value="">-- select City --</option>

                                            </select>
                                        </div>


                                        <div class="form-group col-4">
                                            <label for="exampleInputEmail1">Pincode *</label>
                                            <select name="pincode" id="pincode" class="form-control" required  data-parsley-required-message="Please Select a Pincode">
                                                <option value="">-- select Pincode --</option>

                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="location">Landmark *</label>
                                        <input type="text" class="form-control" id="landmark" name="landmark" placeholder="LandMark" required
                                               data-parsley-required-message="Please Enter Landmark" >

                                    </div>

                                    <div class="form-group">
                                        <label>Address Type *</label>
                                        <div class="input-group">
                                            <input type="radio" id="home" name="address_type" value="1" required  data-parsley-required-message="Please Select Address Type"/>
                                            <label for="home"> &nbsp;&nbsp;Home</label>&nbsp;&nbsp;

                                            <input type="radio" id="office" name="address_type" value="2" required/>
                                            <label for="office"> &nbsp;&nbsp;Office</label>&nbsp;&nbsp;


                                            <input type="radio" id="other" name="address_type" value="3" required/>
                                            <label for="other"> &nbsp;&nbsp;Other</label>&nbsp;&nbsp;
                                        </div>

                                    </div>



                                    <div class="form-group mb-0">
                                        <button type="submit" class="btn btn-primary submit-form">Save Address</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="<?= base_url('web_assets/') ?>/js/plugins/parsleyjs/dist/parsley.min.js"></script>
    <script type="text/javascript">


                                                function getCities(state_id) {
                                                    $.ajax({
                                                        url: "<?= base_url('api/admin_ajax/locations/get_cities') ?>",
                                                        type: "post",
                                                        data: {state_id: state_id},
                                                        success: function (resp) {
                                                            $("#cities").html(resp);
                                                        }
                                                    });
                                                }

                                                function getPincodes(city_id) {
                                                    $.ajax({
                                                        url: "<?= base_url('api/admin_ajax/locations/get_pincodes') ?>",
                                                        type: "post",
                                                        data: {city_id: city_id},
                                                        success: function (resp) {
                                                            $("#pincode").html(resp);
                                                        }
                                                    });
                                                }


                                                var open = false;
                                                $(document).ready(function () {
                                                    $('#address-form').parsley();
                                                });

                                                function checkthis(val) {
                                                    console.log(val);
                                                    $.ajax({
                                                        url: '<?= base_url() ?>my_addresses/change_address_selection',
                                                        type: 'post',
                                                        data: {id: val},
                                                        success: function (resp) {
                                                            if (resp == 'success') {
                                                                swal('Default Address Changed').then(function () {
                                                                    location.href = "<?= base_url('my-addresses') ?>";
                                                                });
                                                            } else {
                                                                swal({
                                                                    title: 'Unable to Change Your Default Address',
                                                                    icon: 'warning',
                                                                    button: "close"
                                                                });
                                                            }
                                                        }
                                                    });
                                                }

                                                function postthisforedit(id) {
                                                    var json = $("#json-holder-" + id).html();
                                                    json = JSON.parse(json);
                                                    $("#search-view").css({'display': 'none'});
                                                    $('#prev_id').val(json['id']);
                                                    $("#name").val(json['name']);
                                                    $("#mobile").val(json['mobile']);
                                                    $("#address").val(json['address']);
                                                    $("#landmark").val(json['landmark']);
                                                    $("#state").val(json['state']);
                                                    if (json['address_type'] == '1') {
                                                        $("#home").prop("checked", true);
                                                    } else if (json['address_type'] == '2') {
                                                        $("#office").prop("checked", true);
                                                    } else {
                                                        $("#other").prop("checked", true);
                                                    }

                                                    setTimeout(function () {
                                                        getCities(json['state']);
                                                        getPincodes(json['city']);
                                                        setTimeout(function () {
                                                            $("#cities").val(json['city']);
                                                            $("#pincode").val(json['pincode']);
                                                        }, 1000);
                                                    }, 50);

                                                    if (open) {
                                                        $("#search-btn").click();
                                                    }
                                                    open = true;
                                                }

                                                function reset_data() {
                                                    $('#prev_id').val("");
                                                    $("#name").val("");
                                                    $("#mobile").val("");
                                                    $("#address").val("");
                                                    $("#cities").val("");
                                                    $("#state").val("");
                                                    $("#pincode").val("");
                                                    $("#landmark").val("");
                                                    $("#home").prop("checked", false);
                                                    $("#office").prop("checked", false);
                                                    $("#other").prop("checked", false);
                                                    if (open) {
                                                        $("#search-btn").click();
                                                    }
                                                }

                                                function deletethis(id) {
                                                    swal({
                                                        title: "Are you sure?",
                                                        text: "This address Will be Deleted Permenantly",
                                                        icon: "warning",
                                                        buttons: [
                                                            'No, cancel it!',
                                                            'Yes, I am sure!'
                                                        ],
                                                        dangerMode: true
                                                    }).then(function (isConfirm) {
                                                        if (isConfirm) {
                                                            $.ajax({
                                                                url: '<?= base_url("api/user_profile/delete_address") ?>',
                                                                type: 'post',
                                                                data: {user_id: '<?= USER_ID ?>', id: id},
                                                                success: function (json) {
                                                                    var arr = JSON.parse(json);
                                                                    if (arr['status']) {
                                                                        swal({
                                                                            title: "Success",
                                                                            text: "Address has been Deleted",
                                                                            icon: "success",
                                                                        }).then(function () {
                                                                            location.href = "";
                                                                        });
                                                                    } else {
                                                                        swal({
                                                                            title: "Failed",
                                                                            text: "Unable to Delete Address",
                                                                            icon: "warning"
                                                                        });
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
    </script>
<?php } else { ?>
    <div class="container-fluid" style="min-height: 400px">
        <h3 style="text-align: center; margin-top: 160px"> Please Login to Continue </h3>
    </div>
<?php } ?>
<!--/User Dashboard-->

<!-- Newsletter-->

<!--/Newsletter-->