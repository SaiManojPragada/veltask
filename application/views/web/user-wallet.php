<!--Breadcrumb-->
<section>
    <div class="bannerimg cover-image bg-background3" data-image-src="../assets/images/banners/banner2.jpg">
        <div class="header-text mb-0">
            <div class="container">
                <div class="text-center text-white">
                    <h1 class="">My Dashboard</h1>
                    <ol class="breadcrumb text-center">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active text-white" aria-current="page">My Dashboard</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Breadcrumb-->
<!--User Dashboard-->

<style>
    .wallet-div {
        width: 100%;
    }
    .add-btn-menu {
        border: 2px solid #f15d74 !important;
        background: #f15d74;
        color: #ffffff !important;
    }
</style>
<section class="sptb">
    <div class="container">
        <div class="row">
            <?php include 'dashboard-left-menu.php'; ?>
            <div class="col-xl-9 col-lg-12 col-md-12">
                <div class="row d-flex justify-content-end">


                    <div class="col-sm-12 col-xl-12 mb-2 mb-sm-0">
                        <section class="card card-featured-left card-featured-secondary ">
                            <div class="card-body">
                                <div class="widget-summary ">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="widget-summary-col widget-summary-col-icon">
                                                <div class="summary-icon wallet">
                                                    <i class="fal fa-wallet"></i>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-7">
                                            <div class="widget-summary-col">
                                                <div class="summary">
                                                    <h4 class="title">Wallet</h4>

                                                </div>
                                                <div class="summary-footer">
                                                    <span><i class="fas fa-rupee-sign"></i> <?= $wallet_amount ?></span>
                                                </div>
                                            </div>
                                        </div><!--col-md-6-->
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>

                <div class="card mb-0">
                    <div class="card-header p-4">
                        <div class="wallet-div d-flex justify-content-between">
                            <div class="wallet-header">
                                <h3 class="card-title">Booking Order List</h3>
                            </div><!--wallet-header-->

                        </div><!--d-flex-->
                    </div>
                    <div class="card-body">
                        <div class="my-favadd table-responsive userprof-tab">
                            <table class="table  table-hover mb-0 text-nowrap">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Transaction Id</th>
                                        <th>Date & Time</th>
                                        <th>Transaction Log</th>
                                        <th>Amount</th>
                                        <th>Type</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($wallet_transactions as $index => $trans) { ?>
                                        <tr>
                                            <td><?= $index + 1 ?></td>
                                            <td>#<?= $trans->transaction_id ?></td>
                                            <td><?= date('d M Y, h:i A', $trans->created_at) ?></td>
                                            <td><?= $trans->message ?></td>
                                            <td class="font-weight-semibold fs-16"><i class="fas fa-rupee-sign"></i> <?= $trans->price ?></td>
                                            <td>
                                                <?php if ($trans->status == 'credit') { ?>
                                                    <span class="badge badge-pill bg-success-light">Credit</span>
                                                <?php } else { ?>
                                                    <span class="badge badge-pill bg-danger-light">Debit</span>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    <?php if (empty($wallet_transactions)) { ?>
                                        <tr><td colspan="5"><center> -- No Wallet Transactions Found -- </center></td></tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/User Dashboard-->
<!-- Newsletter-->

<!--/Newsletter-->