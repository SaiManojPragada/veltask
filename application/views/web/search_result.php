<?php include 'includes/ecom_header.php'; ?>
<div>
  <div class="cover-image sptb-1 bg-background" data-image-src="../assets/images/banners/banner1.jpg">
    <div class="header-text1 mb-0">
      <div class="container">
        <div class="row">
          <div class="col-xl-12 col-lg-12 col-md-12 d-block ">
            <div class=" breadcrumb-banner text-center text-white ">
            <h3>"<?php echo $title; ?>" Found  </h3>
            </div>
            
        </div>
      </div>
    </div>
  </div>
  <!-- /header-text -->
</div>
</div>
<style>
  p.store-location {
overflow: hidden;
text-overflow: ellipsis;
white-space: nowrap;
width: 272px;
}
.prodduct-title {
    white-space: nowrap;
    line-height: 1.8;
}

.section_title.product_shop_title h2 {
    text-align: center;
    padding: 24px 10px;
    font-size: 26px;
    font-weight: 800;
}
</style>
<section class="storesnearbox">
<div class="container">
  <div class="row justify-content-center">
    <div class="col-lg-10 col-md-12">
      <div class="section_title product_shop_title">
        <h2>Shops <span>( <?php echo count($shops); ?> )</span></h2>
      </div>
      
      <!-- <h3>Shops ( 5 )</h3> -->
      <div class="row">
         <?php 
               foreach($shops as $shop){ ?>
        <div class="col-lg-4 col-md-4">
          <div class="card shadow-sm mb-4">
            <div class="img-box">
              <a href="<?php echo base_url(); ?>web/store/<?php echo $shop['seo_url']; ?>/shop/<?php echo $title;?>/search_data"><img src="<?php echo $shop['image']; ?>" alt="" class="card-img-top"></a>
              <!-- <h5><i class="fal fa-badge-percent"></i> DEALS</h5> -->
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-lg-12">
                  <h4 class="prodduct-title"><a href="<?php echo base_url(); ?>web/store/<?php echo $shop['seo_url']; ?>/shop/<?php echo $title;?>/search_data"><?php echo $shop['shop_name']; ?></a></h4>
                  <p></p>
                  <p class="store-location"><i class="fal fa-map"></i> <?php echo substr($shop['description'],0,40)."..."; ?></p>
                </div>
              </div>
            </div>
            <div class="card-footer">
              <div class="row">
                <div class="col-lg-6"><p><small><i class="fal fa-map-marker-alt"></i> <?php echo $shop['distance']; ?>Km</small></p></div>
                <div class="col-lg-6 text-right"><p class="pinkcol"><small><?php echo $shop['product_total']; ?> Products</small></p></div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>

        
        
        
        
      </div>
    </div>
  </div>
</div>
</section>
<div class="shop_area mb-80">
<div class="container">
  <div class="row justify-content-center">
    <div class="col-lg-10 col-md-12">
      <div id="fav_msg" style="text-align: center;"></div>
      <div class="section_title product_shop_title">
        <h2>Products <span>( <?php echo count($products); ?> )</span></h2>
      </div>
      
      <div class="row shop_wrapper" id="products_list">
        <?php foreach($products as $deals){ ?>

        <div class="col-md-3 mb-4">
          <div class="item shop-item-cat text-center">
            <a href="<?php echo base_url(); ?>web/product_view/<?php echo $deals['seo_url']; ?>/<?php echo $title;?>">
              <img src="<?php echo $deals['image']; ?>" alt="">
            </a>
            <div class="popular-products">
              <h2 class="mb-0"><a href="<?php echo base_url(); ?>web/product_view/<?php echo $deals['seo_url']; ?>/<?php echo $title;?>"><?php echo $deals['name']; ?></a> </h2>
              
              <span><a href="product-view.php"><?php echo $deals['shop']; ?></a></span>
              <div class="price"><?php echo $deals['saleprice']; ?></div>
              <a onclick="addtocart(<?php echo $deals['variant_id']; ?>,<?php echo $deals['shop_id']; ?>,'<?php echo $deals['saleprice']; ?>',1)" class="btn btn-primary btn-ptill mb-3"><i class="fal fa-shopping-cart"></i> Add to Cart</a>
            </div>
          </div>
        </div>
        
        <?php } ?>
        
        
      </div>
      
    </div>
    
    
  </div>
</div>
</div>

<?php include ('includes/footer.php'); ?>