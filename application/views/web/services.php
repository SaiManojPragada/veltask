<!--Sliders Section-->
<div>
    <div class="cover-image sptb-1 bg-background" data-image-src="../assets/images/banners/banner1.jpg">
        <div class="header-text1 mb-0">
            <div class="container">
                <div class="row">
                    <div class="col-xl-10 col-lg-12 col-md-12 d-block ">
                        <div class=" breadcrumb-banner text-left text-white ">
                            <h1 > Services
                            </h1>
                        </div>
                        <div>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="<?= base_url(); ?>">Home
                                    </a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="javascript:void(0);">Services
                                    </a>
                                </li>

                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /header-text -->
    </div>
</div>
<!--/Sliders Section-->
<!--Add listing-->
<section class="sptb">
    <div class="container">
        <div class="row">
            <!--Left Side Content-->
            <div class="col-xl-3 col-lg-4 col-md-12">
                <div class="left-side-bar">
                    <?php if (!empty($sub_categories_all)) { ?>
                        <div class="card p-5">
                            <div class="inner-class-card ">
                                <div class="card-header p-4">
                                    <h3 class="card-title">Services
                                    </h3>
                                </div>
                                <div class="card-body">
                                    <div class="" id="container">
                                        <div class="filter-product-checkboxs">

                                            <?php foreach ($sub_categories_all as $s) { ?>
                                                <div class="service-type-session <?= ($sub_category == $s->seo_url) ? 'active' : '' ?>" style="cursor: pointer">
                                                    <div class="d-flex justify-content-between" onclick="location.href = '<?= base_url() ?>services/<?= $category ?>/<?= $s->seo_url ?>'">
                                                        <span class="service-name">
                                                            <a href="javascript:void(0);"><?= ucfirst($s->sub_category_name) ?>
                                                            </a>
                                                        </span>
                                                        <span class="service-count"><?= number_format($s->count) ?>
                                                        </span>
                                                    </div>
                                                </div>
                                            <?php } ?>

                                        </div>
                                    </div>
                                </div>
                                <style>
                                    .filter-product-checkboxs .active{
                                        border: 1px solid #ff7088;
                                    }
                                    .filter-product-checkboxs .active .service-name a{
                                        color: #ff7088;
                                    }
                                </style>
                                <!-- <div class="card-footer">
              <a href="#" class="btn btn-secondary btn-block">Apply Filter</a>
              </div> -->
                            </div>
                        </div>
                    <?php } if (!empty($filters)) { ?>
                        <div class="card p-5">
                            <div class="inner-class-card ">
                                <div class="card-header p-4">
                                    <h3 class="card-title">Filter By Prices
                                    </h3>
                                </div>
                                <form id="filterFormee">
                                    <div class="card-body">
                                        <div class="" id="container">
                                            <div class="filter-product-checkboxs">
                                                <?php foreach ($filters as $index => $flt) { ?>
                                                    <label class="custom-control custom-checkbox mb-3">
                                                        <input type="checkbox" class="custom-control-input price-sort-checkbox" name="priceCheckBox" check-index="<?= $index ?>" min-value="<?= $flt['min'] ?>" max-value="<?= $flt['max'] ?>"
                                                               <?= (in_array($index, $selected_prices)) ? 'checked' : '' ?>>
                                                        <span class="custom-control-label">
                                                            <a href="javascript:void(0);" class="text-dark"><?= $flt['display_text'] ?>
                                                            </a>
                                                        </span>
                                                    </label>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <a href="javascript:void(0);" onclick="priceSort();" class="btn btn-primary btn-block">Apply 
                                        </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <script>
                var min = 0;
                var max = 0;
                function priceSort() {
                    var pricesort = document.getElementsByClassName("price-sort-checkbox");
                    var count = 0;
                    var indexes = [];
                    var filter_json = "[";
                    for (var a = 0; a < pricesort.length; a++) {
                        if ($(pricesort[a]).prop('checked') == true) {
                            var c_min = $(pricesort[a]).attr("min-value");
                            var c_max = $(pricesort[a]).attr("max-value");
                            var c_index = $(pricesort[a]).attr("check-index");
                            indexes.push(c_index);
                            if (count == 0) {
                                min = c_min;
                                max = c_max;
                            }
                            count++;
                            if (c_min < min) {
                                min = c_min;
                            }
                            if (c_max > max) {
                                max = c_max;
                            }
                        }
                        c_max += 1;
                        filter_json += '{min:' + c_min + ',max:' + c_max + ',is_selected:' + $(pricesort[a]).prop('checked') + '},';

                    }
                    max += 1;
                    filter_json = filter_json.slice(0, -1);
                    filter_json += "]";
                    if (count > 0) {
                        $.ajax({
                            url: "<?= base_url('api/web/set_filters') ?>",
                            type: "post",
                            data: {min: min, max: max, selected: indexes.toString(), cat: '<?= $category ?>', price_filter_data: filter_json},
                            success: function (resp) {
                                location.href = "" + "?page=1";
                            }
                        });
                    } else {
                        $.ajax({
                            url: "<?= base_url('api/web/remove_filters') ?>",
                            type: "post",
                            success: function (resp) {
                                location.href = "";
                            }
                        });
                    }
                }
            </script>
            <!--/Left Side Content-->
            <div class="col-xl-9 col-lg-8 col-md-12">
                <!--Add Lists-->
                <div class="right-side-women-service">


                    <div class="right-side-part">
                        <div class=" mb-0">
                            <div class="mb-3">
                                <div class="item2-gl-nav d-flex">
                                    <ul class="nav item2-gl-menu ml-auto">
                                    </ul>
                                    <div class="d-flex"> 
                                        <select name="item" class="select-sm w-70 select2 select2-hidden-accessible" onchange="addSort(this.value);" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                            <option value="" <?= empty($sort) ? 'selected' : '' ?>>Default Sorting
                                            </option>
                                            <option value="latest" <?= ($sort == "latest") ? 'selected' : '' ?>>Latest
                                            </option>
                                            <option value="oldest" <?= ($sort == "oldest") ? 'selected' : '' ?>>Oldest
                                            </option>
                                            <option value="low-to-high" <?= ($sort == "low-to-high") ? 'selected' : '' ?>>Price:Low-to-High
                                            </option>
                                            <option value="high-to-low" <?= ($sort == "high-to-low") ? 'selected' : '' ?>>Price:Hight-to-Low
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="">
                                <div class="item2-gl ">
                                    <?php
                                    $current = $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                                    if (strpos($current, "?page")) {
                                        $current = substr($current, 0, strpos($current, "?page"));
                                    }
                                    if (!empty($sort)) {
                                        $current .= "?sort=$sort&";
                                    } else {
                                        $current .= "?";
                                    }
                                    ?>
                                    <?php foreach ($services as $index => $serv) { ?>
                                        <div class="card overflow-hidden ">
                                            <div class="d-md-flex">
                                                <div class="item-card9-img">
                                                    <div class="item-card2-img ">
                                                        <?php if ($serv->rating > 0) { ?>
                                                            <div class="arrow-ribbon ">
                                                                <i class="fa fa-star" aria-hidden="true">
                                                                </i><?= $serv->rating ?>
                                                            </div>
                                                        <?php } ?>
                                                        <a href="<?= base_url() . 'services/' . $category . '/' . $serv->sub_category_url . '/' . $serv->seo_url ?>">
                                                        </a>
                                                        <img style="height: 241px; background:url(<?= (!empty($serv->image)) ? base_url('uploads/services/') . $serv->image : base_url('uploads/') . SITE_FAV_ICON ?>) center center;" onclick="location.href = '<?= base_url() . 'services/' . $category . '/' . $serv->sub_category_url . '/' . $serv->seo_url ?>';" src="<?= base_url() ?>web_assets/assets/images/empty-img.png" alt="img" class="cover-image">
                                                    </div>
                                                </div>
                                                <div class="card pl-4 pr-4 border-0 mb-0">

                                                    <div class="header-crd  pt-2 pb-1 ">
                                                        <h4 class="font-weight-bold mt-1 h4-tag"><a href="<?= base_url() . 'services/' . $category . '/' . $serv->sub_category_url . '/' . $serv->seo_url ?>"><?= $serv->service_name ?></a>
                                                        </h4>

                                                        <div class="service-price">
                                                            <?php if ($serv->has_visit_and_quote == 1) { ?>
                                                                <p class="mb-0"><span class="offer-price">₹<?= $serv->visiting_charges + $serv->tax ?></span></p>
                                                            <?php } else { ?>
                                                                <p class="mb-0"><span class="offer-price">₹<?= $serv->sale_price ?></span> <span class="mrp-price">₹<?= $serv->price ?></span></p>
                                                            <?php } ?>
                                                        </div><!--service-price-->
                                                    </div>
                                                    <div class="card-body " style="height: 78px">
                                                        <div class="item-card2" style="height: 64px">
                                                            <div class="item-card2-desc" style="overflow-y: hidden; height: inherit">
                                                                <?php
                                                                $string = strip_tags($serv->description);                                                                								$strlen = 250;
                                                                if (strlen($string) > $strlen) {
                                                                    $stringCut = substr($string, 0, $strlen);
                                                                    $endPoint = strrpos($stringCut, ' ');
                                                                    $string = $endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                                                    $string .= '...';
                                                                }
                                                                echo $string;
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-footer pt-4 pb-4">
                                                        <div class="item-card2-footer d-flex">
                                                            <div class="item-card2-rating">
                                                                <div class="rating-stars d-inline-flex">
                                                                    <ul class="footer-list">

                                                                        <li>
                                                                            <span> <a href="javascript:void(0);" onclick="location.href = '<?= base_url() . 'services/' . $category . '/' . $serv->sub_category_url . '/' . $serv->seo_url ?>';">View Details <i class="fa fa-angle-right" aria-hidden="true"></i></a> </span>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <div class="ml-auto">

                                                                <div class="F8dpS zj0R0 _3L1X9" style="cursor: pointer; <?= ($serv->has_visit_and_quote == 1) ? "padding: 4px 10px; width: auto !important; display: block" : "display: none" ?>">
                                                                    <span onclick="show('<?= $index ?>', 1);"  id="addbtn">Visit & Quote</span>
                                                                </div>
                                                                <div class="F8dpS zj0R0 _3L1X9" style="cursor: pointer; <?= ($serv->has_visit_and_quote == 1) ? "display: none" : "" ?>">
                                                                    <input id="service_id_<?= $index ?>" type="hidden" value="<?= $serv->id ?>">

                                                                    <div class="_1RPOp addbtn_<?= $index ?>" id="addbtn" onclick="show('<?= $index ?>');" style=' <?= ($serv->quantity) ? "display : none" : "" ?>'>ADD</div>

                                                                    <div id="box1" class="box1_<?= $index ?>" style="display: none;"></div>

                                                                    <div id="increament-div" class="increament-div_<?= $index ?>"  style=' <?= ($serv->quantity) ? "display : block" : "" ?>'>
                                                                        <div id="box" style="display: none;" class="box_<?= $index ?>"></div>
                                                                        <div onclick="inccreament('<?= $index ?>')" class="_1ds9T _2WdfZ _4aKW6 classList ">+</div>
                                                                        <div class="_2zAXs _2quy- _4aKW6" id="quantity_<?= $index ?>"><?= ($serv->quantity) ? $serv->quantity : "0" ?></div>
                                                                        <div onclick="deccreament('<?= $index ?>')"  class="_29Y5Z _20vNm _4aKW6">-</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="pt-2" style="color: #f15f74; position: absolute; right: 20px; z-index: 200000; top: 0; cursor: pointer;">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if (empty($services)) { ?>
                                        <center class='mt-10' style='margin-bottom: 200px'>
                                            <h3> -- No Results Found -- </h3>
                                        </center>
                                    <?php } ?>
                                </div>
                                <?php if (!empty($services)) { ?>
                                    <div class="center-block text-center">
                                        <ul class="pagination mb-3">
                                            <li class="page-item page-prev <?= ($page <= 1) ? 'disabled' : '' ?>">
                                                <a class="page-link" href="<?php
                                                if (!($page <= 1)) {
                                                    echo $current . 'page=' . ($page - 1);
                                                } else {
                                                    echo'javascript:void(0)';
                                                }
                                                ?>" tabindex="-1">Prev
                                                </a>
                                            </li>
                                            <?php for ($i = 1; $i <= $total_pages; $i++) { ?>
                                                <li class="page-item <?= ($page == $i) ? 'active' : '' ?>">
                                                    <a class="page-link" href="<?= $current . 'page=' . $i ?>"><?= $i ?>
                                                    </a>
                                                </li>
                                            <?php } ?>
                                            <li class="page-item page-next <?= ($page >= $total_pages) ? 'disabled' : '' ?>">
                                                <a class="page-link" href="<?php
                                                if (!($page >= $total_pages)) {
                                                    echo $current . 'page=' . ($page + 1);
                                                } else {
                                                    echo'javascript:void(0)';
                                                }
                                                ?>">Next
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div><!--riht-side-womenservie-->
                <!--right-side-part-->
                <!--/Add Lists-->
            </div>
        </div>
    </div>
</section>
<section style="<?= ($cart_total->items_in_cart < 1) ? 'display:none' : '' ?>" id="cart_bar">

    <div class="footer-sticky-bar">
        <div class="container">

            <div class="row">
                <div class="col-md-6 text-left">
                    <?php if (!$has_membership) { ?>
                        <div class="view-member-btn">
                            <button  type="button"  data-toggle="modal" data-target="#Mymodal1" class="btn btn-default cart-btn-menu"> <a href="javascript:void(0);">View Membership
                                </a></button>
                        </div>
                    <?php } ?>



                </div>
                <div class="col-md-6 ">
                    <div class="Continue-btn ml-auto justify-content-center">
                        <div class="contine-parent" onclick="location.href = '<?= base_url('cart') ?>'" style="cursor: pointer">
                            <span class="service-product-count" id="items_in_cart"><?= number_format($cart_total->items_in_cart) ?>
                            </span><strong>Grand Total : </strong>₹<span id="cart_total"><?= $cart_total->sub_total ?></span>
                            <span class="continue-btn-right"><a href="javascript:void(0);" >Continue</a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--footer-sticky-bar-->
    </div>
</section>

<div class="alert alert-success cart_alert" role="alert" id="cart_alert" style="display: none" onclick="$('#cart_alert').fadeOut(2000);">

</div>
<style>
    #cart_alert{
        position: fixed;
        top: -30px;
        right: 40px;
    }
</style>
<script>

    function hide_cart_message() {
        setTimeout(function () {
            $(".cart_alert").fadeOut(2000);
        }, 5000);
    }
    var page_url = "<?= base_url('services/' . $category . '/') ?><?= ($sub_category) ? $sub_category . '/' : "" ?><?= ($this->input->get("page") ? "?page=" . $this->input->get("page") . "&" : "?") ?>";
    var sort = "<?= $sort ?>";
    function addSort(sort) {
        if (sort.length > 1) {
            location.href = page_url + "sort=" + sort;
        } else {
            location.href = page_url;
        }
    }

    var prev_cart_desg = <?= ($cart[0]->has_visit_and_quote) ? '1' : '0' ?>;
    function show_cart() {
        $('#cart_bar').css('display', 'block');
    }
    function hide_cart() {
        $('#cart_bar').css('display', 'none');
    }
    function show(index, has_visit_and_quote = 0) {

<?php if (!empty(USER_ID)) { ?>
            $(".increament-div_" + index).css('display', "block");
            $(".addbtn_" + index).css('display', "none");
            $('#quantity_' + index).html('1');
            //animation
            $("box_" + index).css("display", "block");
            setTimeout(function () {
                $("box_" + index).css("display", "none");
            }, 400);

            //            $(".cart_alert").html("Item Added to Cart");
            //            $(".cart_alert").fadeIn(1000);
            hide_cart_message();
            if (prev_cart_desg == 0 && has_visit_and_quote == 1) {
                $.ajax({
                    url: '<?= base_url() ?>api/web/chec_for_v_a_q',
                    type: 'post',
                    data: {user_id: '<?= USER_ID ?>'},
                    success: function (resp) {
                        console.log(resp);
                        if (!resp) {
                            swal({
                                title: "Are you sure?",
                                text: "You Already have normal services in your cart by Proceeding to Visit and Quote your cart will be cleared.",
                                icon: "warning",
                                buttons: [
                                    'No, cancel it!',
                                    'Yes, I am sure!'
                                ],
                                dangerMode: true,
                            }).then(function (isConfirm) {
                                if (isConfirm) {
                                    deleteCartVQ(index, has_visit_and_quote);
                                }
                            });
                        } else {
                            deleteCartVQ(index, has_visit_and_quote);
                        }
                    }
                });

            } else {
                call_cart(index, has_visit_and_quote);
            }

<?php } else { ?>
            swal({
                title: "Please Login to Continue",
                icon: "info"
            }).then(function () {
                openNav();
            });
<?php } ?>
    }

    function deleteCartVQ(index, has_visit_and_quote) {
        $.ajax({
            url: '<?= base_url() ?>api/cart/delete_cart',
            type: 'post',
            data: {user_id: '<?= USER_ID ?>'},
            success: function (resp) {
                resp = JSON.parse(resp);
                if (resp['status']) {
                    call_cart(index, has_visit_and_quote);
                } else {
                    swal({
                        title: resp['message'],
                        icon: 'info'
                    });
                }
            }
        });
    }

    function inccreament(index)
    {

        //animation
        $(".box_" + index).css("display", "block");
        var quantity = $("#quantity_" + index).html();
        quantity = parseInt(quantity.toString()) + 1;
        $("#quantity_" + index).html(quantity);
        setTimeout(function () {
            $(".box_" + index).css("display", "none");
        }, 400);
        call_cart(index);

    }

    function deccreament(index, stat = "")
    {
        var quantity = $("#quantity_" + index).html();
        quantity = parseInt(quantity.toString()) - 1;
        $("#quantity_" + index).html(quantity);
        if (quantity < 1) {
            $(".increament-div_" + index).css('display', "none");
            $(".addbtn_" + index).css('display', "block");
            if (stat == "") {
                $(".cart_alert").html("Item Removed from Cart");
                $(".cart_alert").fadeIn(1000);
                hide_cart_message();
            }
        }
        //animation
        $(".box_" + index).css("display", "block");
        setTimeout(function () {
            $(".box_" + index).css("display", "none");

        }, 400);
        if (stat == "") {
            call_cart(index);
    }
    }

    function call_cart(index, has_visit_and_quote = 0) {

        var service_id = $("#service_id_" + index).val();
        var quantity = $("#quantity_" + index).html();
        if (has_visit_and_quote == 1) {
            quantity = 1;
        }
        $.ajax({
            url: "<?= base_url('api/web/cart') ?>",
            type: "post",
            data: {service_id: service_id, quantity: quantity, has_visit_and_quote: has_visit_and_quote},
            success: function (json) {
                var resp = JSON.parse(json);
                if (resp['status']) {
                    show_cart();
                    var data = resp['data'];
                    console.log(data);
                    $("#cart_total").html(data['cart_total']);
                    $("#items_in_cart").html(data['items_in_cart']);
                    $("#cart-items-top").html(data['items_in_cart']);
                    if (data['items_in_cart'] == 0) {
                        hide_cart();
                    }
                    if (has_visit_and_quote == 1) {
                        swal({
                            title: resp['message'],
                            icon: 'success'
                        }).then(function () {
                            location.href = "<?= base_url('cart') ?>";
                        });
                    }
                } else {
                    deccreament(index, "dsada");
                    if (resp['type'] == null) {
                        swal({
                            title: resp['message'],
                            icon: "info"
                        }).then(function () {
<?php if (empty(USER_ID)) { ?>
                                openNav();
<?php } ?>
                        });
                    } else {
                        swal({
                            title: resp['message'],
                            text: "If you proceed you current cart will be deleted.",
                            icon: "warning",
                            buttons: [
                                'No, cancel it!',
                                'Yes, I am sure!'
                            ],
                            dangerMode: true,
                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                $.ajax({
                                    url: "<?= base_url('api/cart/delete_cart') ?>",
                                    type: "post",
                                    data: {user_id: "<?= USER_ID ?>"},
                                    success: function (resp) {
                                        resp = JSON.parse(resp);
                                        if (resp['status']) {
                                            $("#quantity_" + index).html(1);
                                            show(index, has_visit_and_quote);
                                        } else {
                                            swal({
                                                title: resp['message'],
                                                icon: "info"
                                            });
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            }
        });
    }

</script> 