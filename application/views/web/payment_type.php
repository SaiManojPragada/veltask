<!--Sliders Section-->
<div>
    <div class="cover-image sptb-1 bg-background" data-image-src="../assets/images/banners/banner1.jpg">
        <div class="header-text1 mb-0">
            <div class="container">
                <div class="row">
                    <div class="col-xl-10 col-lg-12 col-md-12 d-block ">
                        <div class=" breadcrumb-banner text-left text-white ">
                            <h1 > Beauty & Salon
                            </h1>
                        </div>
                        <div>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="#">Home
                                    </a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="#">Services
                                    </a>
                                </li>
                                <li class="breadcrumb-item">  
                                    <a href="women-services.php">Beauty & Salon
                                    </a>
                                </li>
                                <li class="breadcrumb-item"><a href="view-details.php">View Details</a>
                                </li>
                                <li class="breadcrumb-item"><a href="add-address-book.php">Add Address Book</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page"><a href="payment-page.php">Payment</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /header-text -->
    </div>
</div>
<!--/Sliders Section-->
<!--Add listing-->
<section class="sptb">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 col-lg-8 col-md-12">
                <!--Classified Description-->
                <div class="card overflow-hidden ">
                    <div class="d-md-flex">
                        <div class="card border-0 mb-0">
                            <div class="header-crd pl-5 pr-5  pt-3 pb-3 ">
                                <h4 class="font-weight-bold mt-1 h4-tag">Payment Method
                                </h4>
                            </div>
                            <div class="card-body ">
                                <div class="item-card2">
                                    <div class="item-card2-desc">
                                        <div class=" ad-post-details">
                                            <?php
                                            if (!$cart[0]->has_visit_and_quote) {
                                                foreach ($payment_methods as $index => $meth) {
                                                    ?>
                                                    <label class="custom-control custom-radio mb-2 mr-4">
                                                        <input type="radio" class="custom-control-input" style="margin-top: 10px" name="payment_method" value="<?= $meth->id ?>" id="payment_type_<?= $meth->id ?>" <?= ($index == 0) ? 'checked' : '' ?> onclick="changeToPrepay('<?= $meth->id ?>');">
                                                        <span class="custom-control-label"><a href="#" class="text-muted"><?= $meth->payment_method_title ?><?= ($meth->id == 3) ? "&nbsp;&nbsp;<small>(pay " . $pre_pay_percentage . " and pay the rest after)</small>" : "" ?></a></span>
                                                    </label>
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <label class="custom-control custom-radio mb-2 mr-4">
                                                    <input type="radio" class="custom-control-input" style="margin-top: 10px" name="payment_method" value="<?= $payment_methods[0]->id ?>"  id="payment_type_<?= $meth->id ?>" <?= ($index == 0) ? 'checked' : '' ?>  onclick="changeToPrepay('<?= $payment_methods[0]->id ?>');">
                                                    <span class="custom-control-label"><a href="#" class="text-muted"><?= $payment_methods[0]->payment_method_title ?></a></span>
                                                </label>
                                            <?php } ?>

                                            <!--                                            <label class="custom-control custom-radio  mb-2 mr-4">
                                                                                            <input type="radio" class="custom-control-input" name="radios1" value="option3">
                                                                                            <span class="custom-control-label"><a href="#" class="text-muted">Cash on Delivery</a></span>
                                                                                        </label>-->

                                        </div>
                                    </div>

                                    <div class="form-check" id="checkboxTerms" style="margin-left: 20px">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" required>
                                        <label class="form-check-label" for="flexCheckDefault">I have read and accept 
                                        </label><span> <b style="cursor: pointer" onclick="window.open('<?= base_url('terms-of-sale') ?>', '_blank');">Terms & Conditions</b></span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
            <!--Right Side Content-->
            <div class="col-xl-4 col-lg-4 col-md-12">
                <div class="contine-page-right-side">
                    <div class="continue-item-1">

                        <div class="card">
                            <div class="card-body p-5">
                                <div class="total-item d-flex ">
                                    <div class="availble-oofer-content">
                                        <p class="mb-0">Item Total
                                        </p>
                                    </div>
                                    <div class="total-price ml-auto">
                                        <span>₹ <?= $cart_totals->sub_total ?>
                                        </span>
                                    </div>
                                </div>
                                <!--offer-card-->
                                <?php if ($cart[0]->has_visit_and_quote == 0) { ?>
                                    <div class="total-item d-flex ">
                                        <div class="availble-oofer-content">
                                            <p class="mb-0">Safety & Visitation Fee
                                            </p>
                                        </div>
                                        <div class="total-price ml-auto">
                                            <span><span style="color: green">+</span> ₹ <span id="visiting_charges"><?= $cart_totals->visiting_charges ?></span>
                                            </span>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="total-item d-flex ">
                                    <div class="availble-oofer-content">
                                        <p class="mb-0">Tax
                                        </p>
                                    </div>
                                    <div class="total-price ml-auto">
                                        <span><span style="color: green">+</span> ₹ <?= $cart_totals->tax ?>
                                        </span>
                                    </div>
                                </div>

                                <?php if ($cart_totals->membership_discount > 0) { ?>
                                    <div class="total-item d-flex ">
                                        <div class="availble-oofer-content">
                                            <p class="mb-0">Membership Discount
                                            </p>
                                        </div>
                                        <div class="total-price ml-auto">
                                            <span><span style="color: tomato">-</span> ₹ <?= $cart_totals->membership_discount ?>
                                            </span>
                                        </div>
                                    </div>
                                <?php } ?>


                                <?php if ($cart_totals->business_discount > 0) { ?>
                                    <div class="total-item d-flex ">
                                        <div class="availble-oofer-content">
                                            <p class="mb-0">Business Discount
                                            </p>
                                        </div>
                                        <div class="total-price ml-auto">
                                            <span><span style="color: tomato">-</span> ₹ <?= $cart_totals->business_discount ?>
                                            </span>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="total-item d-flex " id="coupon_discount_div" style="<?= (!empty($coupon_and_wallet['coupon_code'])) ? '' : 'display: none !important' ?>">
                                    <div class="availble-oofer-content">
                                        <p class="mb-0">Coupon Discount
                                        </p>
                                    </div>
                                    <div class="total-price ml-auto">
                                        <span><span style="color: tomato">-</span> ₹ <span id="coupon_amount"> <?= $coupon_and_wallet['coupon_discount'] ?></span>
                                        </span>
                                    </div>
                                </div>

                                <div class="total-item d-flex " id="used_wallet_amount_div" style="<?= ($coupon_and_wallet['wallet_applied_amount'] > 0) ? "" : "display: none !important;" ?>">
                                    <div class="availble-oofer-content">
                                        <p class="mb-0">Used Wallet Amount
                                        </p>
                                    </div>
                                    <div class="total-price ml-auto">
                                        <span><span style="color: tomato">-</span> ₹ <span id="used_wallet_amount"><?= $coupon_and_wallet['wallet_applied_amount'] ?></span>
                                        </span>
                                    </div>
                                </div>

                                <div class="total-item d-flex " id="total_amount-tt" style="display: none !important">
                                    <div class="availble-oofer-content">
                                        <p class="mb-0">Total Amount
                                        </p>
                                    </div>
                                    <div class="total-price ml-auto">
                                        <span> ₹ <span id="total_amount"></span>
                                        </span>
                                    </div>
                                </div>

                                <div class="total-item d-flex " id="balance_amount-tt" style="display: none !important">
                                    <div class="availble-oofer-content">
                                        <p class="mb-0">Balance Amount
                                        </p>
                                    </div>
                                    <div class="total-price ml-auto">
                                        <span> ₹ <span id="balance_amount"></span>
                                        </span>
                                    </div>
                                </div>


                                <!--offer-card-->
                            </div>
                            <div class="card-footer pt-2 pb-2">
                                <div class="offer-footer d-sm-flex">
                                    <div class="offer-footer-child">
                                        <div class="rating-stars d-inline-flex">
                                            <ul class="footer-list">
                                                <li class="">
                                                    <div class="service-price">
                                                        <p class="mb-0" id="total-tt">Total
                                                        </p>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="ml-auto remove-btn">
                                        <span>₹<span id="cart_grand_total"><?= (!empty($coupon_and_wallet['grand_total']) && $coupon_and_wallet['grand_total'] > 0) ? $coupon_and_wallet['grand_total'] : $cart_totals->total ?></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="proceed-button p-1">
                            <div class="procedd-btn-child">
                                <button class="btn btn-light" onclick="proceedToPay();">Place Order
                                </button>
                            </div>
                        </div>              
                    </div>
                </div>
            </div>
            <!--/Right Side Content-->
        </div>
    </div>
</section>
<script>

    function proceedToPay() {
        $("#error_terms_check").remove();
        if ($("#flexCheckDefault").prop('checked') == true) {
            var value = $('input[name="payment_method"]:checked').val();
            $.ajax({
                url: '<?= base_url("cart/decide_payment_page") ?>',
                type: "post",
                data: {id: value, user_id: '<?= USER_ID ?>'},
                success: function (resp) {
                    resp = JSON.parse(resp);
                    if (!resp['status']) {
                        swal({
                            title: resp['message'],
                            icon: "warning"
                        });
                    } else {
                        location.href = "<?= base_url() ?>" + resp['messsage'];
                    }
                }
            });
        } else {
            $("#checkboxTerms").after('<p id="error_terms_check" style="color: tomato; font-weight: bold">&nbsp;&nbsp;* You have to check this to Continue</p>');
        }
    }

    function checkAvailability() {
        var ret = false;
        $.ajax({
            url: '<?= base_url("api/check_service_providers_availability") ?>',
            type: "post",
            data: {user_id: '<?= USER_ID ?>'},
            success: function (resp) {
                resp = JSON.parse(resp);
                if (resp['status']) {
                    ret = resp['status'];
                } else {
                    swal({
                        title: resp['messsage'],
                        icon: "warning"
                    });
                    ret = resp['status'];
                }
            }
        });
        return ret;
    }

<?php if (!empty($this->session->userdata("payment_type_selected"))) { ?>
        changeToPrepay('<?= $this->session->userdata("payment_type_selected") ?>');
        $("#payment_type_<?= $this->session->userdata("payment_type_selected") ?>").prop("checked", true);
<?php } ?>

    function changeToPrepay(vv) {
        $.ajax({
            url: "<?= base_url('api/web/set_payment_type') ?>",
            type: "post",
            data: {payment_type: vv}
        });
        var pre_pay_percentage = "<?= ($pre_pay_percentage) ? $pre_pay_percentage : 0 ?>";
        var grand_total = "<?= (!empty($coupon_and_wallet['grand_total']) && $coupon_and_wallet['grand_total'] > 0) ? $coupon_and_wallet['grand_total'] : $cart_totals->total ?>";

        if (vv === '3') {
            pre_pay_percentage = parseFloat(pre_pay_percentage);
            grand_total = parseFloat(grand_total);

            var payable_amount = Math.round((grand_total / 100) * pre_pay_percentage);
            console.log(payable_amount);
            $("#total-tt").html("<?= $pre_pay_percentage ?> of the Total Amount");
            $("#cart_grand_total").html(payable_amount);
            $("#total_amount").html(grand_total);
            $("#total_amount-tt").css({display: "block"});
            $("#balance_amount-tt").css({display: "block"});
            $("#balance_amount").html(grand_total - payable_amount);
        } else {
            $("#total-tt").html("Total");
            $("#cart_grand_total").html(grand_total);
            $('#total_amount-tt').attr('style', 'display: none !important');
            $('#balance_amount-tt').attr('style', 'display: none !important');
        }
    }

</script>