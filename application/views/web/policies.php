<!--Sliders Section-->
<div>
    <div class="cover-image sptb-1 bg-background" data-image-src="../assets/images/banners/banner1.jpg">
        <div class="header-text1 mb-0">
            <div class="container">
                <div class="row">
                    <div class="col-xl-10 col-lg-12 col-md-12 d-block ">
                        <div class=" breadcrumb-banner text-left text-white ">
                            <h1 > <?= $data->title ?>
                            </h1>
                        </div>
                        <div>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="<?= base_url() ?>">Home
                                    </a>
                                </li>

                                <li class="breadcrumb-item active" aria-current="page"><a href="javascript:void(0);"><?= $data->title ?></a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /header-text -->
    </div>
</div>
<!--/Sliders Section-->
<!--Add listing-->
<section class="service-section">

    <div class="container">



        <div class="main-div">
            <div class="chil-div pt-5 pb-5">


                <div class="privacy-policy container">

                    <div class="privacy-header">
                        <h2><?= $data->title ?></h2>
                    </div>
                    <?= $data->description ?>
                </div>
            </div><!--child-div-->
        </div>



    </div><!--containerr-->
</section>