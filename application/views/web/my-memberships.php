<!--Breadcrumb-->
<section>
    <div class="bannerimg cover-image bg-background3" data-image-src="../assets/images/banners/banner2.jpg">
        <div class="header-text mb-0">
            <div class="container">
                <div class="text-center text-white">
                    <h1 class="">My Dashboard</h1>
                    <ol class="breadcrumb text-center">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active text-white" aria-current="page">Membership</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Breadcrumb-->

<!--User Dashboard-->
<section class="sptb">
    <div class="container">
        <div class="row">
            <?php include 'dashboard-left-menu.php'; ?>
            <div class="col-xl-9 col-lg-12 col-md-12">
                <div class="card">

                    <div class="card-header membership p-4">
                        <h5>Membership</h5>
                    </div>
                    <div class="card-body">




                        <div class="container p-0">
                            <div class=" p-4">
                                <div class="row">
                                    <?php foreach ($my_memberships_transactions as $index => $trans) { ?>
                                        <div class="col-md-12 mb-3">
                                            <div class="view-member ">
                                                <p class="percentage-oofer"><?= $trans->membership_details->title ?> <span style="font-size: 18px"><?= ($trans->expiry_date > time()) ? "<span style='color: green; font-weight: bold'>Active</span>" : "<span style='color: tomato; font-weight: bold'>Expired</span>" ?></span>
                                                </p>
    <!--                                                <p class="availble-oofer-content"><i class="fa fa-check" aria-hidden="true"></i> Save Exra 10% off on all Beauty & Grooming services</p>
                                                <p class="availble-oofer-content"><i class="fa fa-check" aria-hidden="true"></i> Save Extra ₹100 off on all Cleaning Services</p>
                                                <p class="availble-oofer-content"><i class="fa fa-check" aria-hidden="true"></i> Save Extra ₹100 off on all Cleaning Services</p>-->
                                                <?= $trans->membership_details->description ?>

                                                <div class="item-card2-footer d-sm-flex">
                                                    <div class="item-card2-rating">
                                                        <div class="rating-stars d-inline-flex">
                                                            <ul class="footer-list">
                                                                <li class="">
                                                                    <div class="service-price">
                                                                        <p class="mb-0">
                                                                            <span class="offer-price">₹<?= $trans->amount_paid ?>
                                                                            </span>
                                                                            <span class="mrp-price">₹<?= $trans->membership_details->price ?></span>
                                                                        </p>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div style="position: absolute; top: 10px; right: 25px;">
                                                    <span style="font-weight: bold">Expiry Date : <?= date('d M Y, h:i A', $trans->expiry_date) ?></span>
                                                </div>
                                                <div style="position: absolute; bottom: 10px; right: 25px;">
                                                    <span style="font-weight: bold">Purchase Date : <?= date('d M Y, h:i A', $trans->created_at) ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if (empty($my_memberships_transactions)) { ?>
                                        <div class="container">
                                            <?php foreach ($memberships_list as $memb) { ?>
                                                <div class="col-md-12 mb-3">
                                                    <div class="view-member ">
                                                        <p class="percentage-oofer"><?= $memb->title ?></p>
                                                        <p class="availble-oofer-content">
                                                            <?= $memb->description ?>
                                                        </p>

                                                        <div class="item-card2-footer d-sm-flex">
                                                            <div class="item-card2-rating">
                                                                <div class="rating-stars d-inline-flex">
                                                                    <ul class="footer-list">
                                                                        <li class="">
                                                                            <div class="service-price">
                                                                                <p class="mb-0">
                                                                                    <span class="offer-price">₹<?= $memb->sale_price ?>
                                                                                    </span> 
                                                                                    <span class="mrp-price">₹<?= $memb->price ?>
                                                                                    </span>
                                                                                </p>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <div class="ml-auto remove-btn">
                                                                <?php if (!$has_membership) { ?>
                                                                    <button class="btn btn-primary" onclick="buyThisMembership('<?= $memb->id ?>')">Buy Now</button>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                    <?php if (!empty($my_memberships_transactions) && !$has_membership) { ?>
                                        <div class="container pt-3 pb-4">
                                            <center>
                                                <button  type="button"  data-toggle="modal" data-target="#Mymodal1" class="btn my-custom-btn">
                                                    Renewal Membership
                                                </button>
                                            </center>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>
                            <!--row-->
                        </div>				
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/User Dashboard-->

<!-- Newsletter-->

<!--/Newsletter-->