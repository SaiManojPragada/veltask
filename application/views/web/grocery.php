<?php include 'includes/header.php'; ?>
    <!--Sliders Section-->
    <div>
      <div class="cover-image sptb-1 bg-background" data-image-src="../assets/images/banners/banner1.jpg">
        <div class="header-text1 mb-0">
          <div class="container">
            <div class="row">
              <div class="col-xl-10 col-lg-12 col-md-12 d-block ">
                <div class=" breadcrumb-banner text-left text-white ">
                  <h1 > Grocery
                  </h1>
                </div>
                <div>
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                      <a href="index.php">Home
                      </a>
                    </li>
                    
                    <li class="breadcrumb-item active" aria-current="page"><a href="women-services.php">Grocery</a>
                    </li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /header-text -->
      </div>
    </div>
    <!--/Sliders Section-->
    <!--Add listing-->
    
    <section class="nav-tabs store-item">
      <div class="container ">
        <ul class="nav nav-pills mb-5 d-flex justify-content-center mt-5" id="pills-tab" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Veg</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Non-Veg</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Snacks & Branded Food</a>
      </li>
    </ul>
    <div class="tab-content" id="pills-tabContent">
      <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
        
      
              <div class="row">
             <div class="col-lg-3 col-md-4">
                   <div class="card shadow-sm mb-4">
                       <div class="img-box">
                         <a href="shop.php">
                          <img src="assets/images/cat-shop.png" alt="" class="card-img-top"></a>
                    
                       </div>
                       <div class="card-body">
                         <div class="row">
                           <div class="col-lg-12">
                             <h4><a href="shop.php">LNE FOODS - MADDILAPALEM</a></h4>
                             <p><i class="fal fa-map mr-1"></i>Kancharapalem - Visakhapatnam</p>
                           </div>
                         </div>
                       </div>
                       <div class="card-footer">
                        <div class="row d-flex justify-content-between px-4">
                           <div class=""><p><small><i class="fal fa-map-marker-alt"></i> 1Km</small></p></div>
                           <div class=""><p class="pinkcol"><small>45 Products</small></p></div>
                        </div>
                      </div>
                   </div>
               </div>
                    <div class="col-lg-3 col-md-4">
                   <div class="card shadow-sm mb-4">
                       <div class="img-box">
                         <a href="shop.php">
                          <img src="assets/images/cat-shop-1.png" alt="" class="card-img-top"></a>
                         <!--<h5> <i class="fal fa-badge-percent"></i> OFFERS </h5>-->
                       </div>
                       <div class="card-body">
                         <div class="row">
                           <div class="col-lg-12">
                             <h4><a href="shop.php">LNE FOODS - MADDILAPALEM</a></h4>
                             <p><i class="fal fa-map mr-1"></i><!--?php// echo $value['address']; ?-->Maddilapalem</p>
                           </div>
                         </div>
                       </div>
                        <div class="card-footer">
                        <div class="row d-flex justify-content-between px-4">
                           <div class=""><p><small><i class="fal fa-map-marker-alt"></i> 1Km</small></p></div>
                           <div class=""><p class="pinkcol"><small>45 Products</small></p></div>
                        </div>
                      </div>
                   </div>
               </div>
                    <div class="col-lg-3 col-md-4">
                   <div class="card shadow-sm mb-4">
                       <div class="img-box">
                         <a href="shop.php">
                          <img src="assets/images/cat-shop-2.png" alt="" class="card-img-top"></a>
                         <!--<h5> <i class="fal fa-badge-percent"></i> OFFERS </h5>-->
                       </div>
                       <div class="card-body">
                         <div class="row">
                           <div class="col-lg-12">
                             <h4><a href="shop.php">LNE FOODS - MADDILAPALEM</a></h4>
                             <p><i class="fal fa-map mr-1"></i><!--?php// echo $value['address']; ?-->Maddilapalem</p>
                           </div>
                         </div>
                       </div>
                        <div class="card-footer">
                        <div class="row d-flex justify-content-between px-4">
                           <div class=""><p><small><i class="fal fa-map-marker-alt"></i> 1Km</small></p></div>
                           <div class=""><p class="pinkcol"><small>45 Products</small></p></div>
                        </div>
                      </div>
                   </div>
               </div>
                    <div class="col-lg-3 col-md-4">
                   <div class="card shadow-sm mb-4">
                       <div class="img-box">
                         <a href="shop.php">
                          <img src="assets/images/cat-shop-3.png" alt="" class="card-img-top"></a>
                         <!--<h5> <i class="fal fa-badge-percent"></i> OFFERS </h5>-->
                       </div>
                       <div class="card-body">
                         <div class="row">
                           <div class="col-lg-12">
                             <h4><a href="shop.php">LNE FOODS - MADDILAPALEM</a></h4>
                             <p><i class="fal fa-map mr-1"></i><!--?php// echo $value['address']; ?-->Maddilapalem</p>
                           </div>
                         </div>
                       </div>
                       <div class="card-footer">
                        <div class="row d-flex justify-content-between px-4">
                           <div class=""><p><small><i class="fal fa-map-marker-alt"></i> 1Km</small></p></div>
                           <div class=""><p class="pinkcol"><small>45 Products</small></p></div>
                        </div>
                      </div>
                   </div>
               </div>
                    <div class="col-lg-3 col-md-4">
                   <div class="card shadow-sm mb-4">
                       <div class="img-box">
                         <a href="shop.php">
                          <img src="assets/images/cat-shop-4.png" alt="" class="card-img-top"></a>
                         <!--<h5> <i class="fal fa-badge-percent"></i> OFFERS </h5>-->
                       </div>
                       <div class="card-body">
                         <div class="row">
                           <div class="col-lg-12">
                             <h4><a href="shop.php">LNE FOODS - MADDILAPALEM</a></h4>
                             <p><i class="fal fa-map mr-1"></i><!--?php// echo $value['address']; ?-->Maddilapalem</p>
                           </div>
                         </div>
                       </div>
                       <div class="card-footer">
                        <div class="row d-flex justify-content-between px-4">
                           <div class=""><p><small><i class="fal fa-map-marker-alt"></i> 1Km</small></p></div>
                           <div class=""><p class="pinkcol"><small>45 Products</small></p></div>
                        </div>
                      </div>
                   </div>
               </div>
                    <div class="col-lg-3 col-md-4">
                   <div class="card shadow-sm mb-4">
                       <div class="img-box">
                         <a href="shop.php">
                          <img src="assets/images/cat-shop-5.png" alt="" class="card-img-top"></a>
                         <!--<h5> <i class="fal fa-badge-percent"></i> OFFERS </h5>-->
                       </div>
                       <div class="card-body">
                         <div class="row">
                           <div class="col-lg-12">
                             <h4><a href="shop.php">LNE FOODS - MADDILAPALEM</a></h4>
                             <p><i class="fal fa-map mr-1"></i><!--?php// echo $value['address']; ?-->Maddilapalem</p>
                           </div>
                         </div>
                       </div>
                      <div class="card-footer">
                        <div class="row d-flex justify-content-between px-4">
                           <div class=""><p><small><i class="fal fa-map-marker-alt"></i> 1Km</small></p></div>
                           <div class=""><p class="pinkcol"><small>45 Products</small></p></div>
                        </div>
                      </div>
                   </div>
               </div>

      
        </div><!--row-->
      </div>
      <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
        
          <div class="row">
             <div class="col-lg-3 col-md-4">
                   <div class="card shadow-sm mb-4">
                       <div class="img-box">
                         <a href="shop.php">
                          <img src="assets/images/cat-shop.png" alt="" class="card-img-top"></a>
                    
                       </div>
                       <div class="card-body">
                         <div class="row">
                           <div class="col-lg-12">
                             <h4><a href="shop.php">LNE FOODS - MADDILAPALEM</a></h4>
                             <p><i class="fal fa-map mr-1"></i>Kancharapalem - Visakhapatnam</p>
                           </div>
                         </div>
                       </div>
                       <div class="card-footer">
                        <div class="row">
                           <div class="col-lg-6"><p><small><i class="fal fa-map-marker-alt"></i> 1Km</small></p></div>
                           <div class="col-lg-6 text-right"><p class="pinkcol"><small>45 Products</small></p></div>
                        </div>
                      </div>
                   </div>
               </div>
                    <div class="col-lg-3 col-md-4">
                   <div class="card shadow-sm mb-4">
                       <div class="img-box">
                         <a href="shop.php">
                          <img src="assets/images/cat-shop-1.png" alt="" class="card-img-top"></a>
                         <!--<h5> <i class="fal fa-badge-percent"></i> OFFERS </h5>-->
                       </div>
                       <div class="card-body">
                         <div class="row">
                           <div class="col-lg-12">
                             <h4><a href="shop.php">LNE FOODS - MADDILAPALEM</a></h4>
                             <p><i class="fal fa-map mr-1"></i><!--?php// echo $value['address']; ?-->Maddilapalem</p>
                           </div>
                         </div>
                       </div>
                       <div class="card-footer">
                        <div class="row">
                           <div class="col-lg-6"><p><small><i class="fal fa-map-marker-alt"></i> 1Km</small></p></div>
                           <div class="col-lg-6 text-right"><p class="pinkcol"><small>45 Products</small></p></div>
                        </div>
                      </div>
                   </div>
               </div>
                    <div class="col-lg-3 col-md-4">
                   <div class="card shadow-sm mb-4">
                       <div class="img-box">
                         <a href="shop.php">
                          <img src="assets/images/cat-shop-2.png" alt="" class="card-img-top"></a>
                         <!--<h5> <i class="fal fa-badge-percent"></i> OFFERS </h5>-->
                       </div>
                       <div class="card-body">
                         <div class="row">
                           <div class="col-lg-12">
                             <h4><a href="shop.php">LNE FOODS - MADDILAPALEM</a></h4>
                             <p><i class="fal fa-map mr-1"></i><!--?php// echo $value['address']; ?-->Maddilapalem</p>
                           </div>
                         </div>
                       </div>
                       <div class="card-footer">
                        <div class="row">
                           <div class="col-lg-6"><p><small><i class="fal fa-map-marker-alt"></i> 1Km</small></p></div>
                           <div class="col-lg-6 text-right"><p class="pinkcol"><small>45 Products</small></p></div>
                        </div>
                      </div>
                   </div>
               </div>
                    <div class="col-lg-3 col-md-4">
                   <div class="card shadow-sm mb-4">
                       <div class="img-box">
                         <a href="shop.php">
                          <img src="assets/images/cat-shop-3.png" alt="" class="card-img-top"></a>
                         <!--<h5> <i class="fal fa-badge-percent"></i> OFFERS </h5>-->
                       </div>
                       <div class="card-body">
                         <div class="row">
                           <div class="col-lg-12">
                             <h4><a href="shop.php">LNE FOODS - MADDILAPALEM</a></h4>
                             <p><i class="fal fa-map mr-1"></i><!--?php// echo $value['address']; ?-->Maddilapalem</p>
                           </div>
                         </div>
                       </div>
                       <div class="card-footer">
                        <div class="row">
                           <div class="col-lg-6"><p><small><i class="fal fa-map-marker-alt"></i> 1Km</small></p></div>
                           <div class="col-lg-6 text-right"><p class="pinkcol"><small>45 Products</small></p></div>
                        </div>
                      </div>
                   </div>
               </div>
                    <div class="col-lg-3 col-md-4">
                   <div class="card shadow-sm mb-4">
                       <div class="img-box">
                         <a href="shop.php">
                          <img src="assets/images/cat-shop-4.png" alt="" class="card-img-top"></a>
                         <!--<h5> <i class="fal fa-badge-percent"></i> OFFERS </h5>-->
                       </div>
                       <div class="card-body">
                         <div class="row">
                           <div class="col-lg-12">
                             <h4><a href="shop.php">LNE FOODS - MADDILAPALEM</a></h4>
                             <p><i class="fal fa-map mr-1"></i><!--?php// echo $value['address']; ?-->Maddilapalem</p>
                           </div>
                         </div>
                       </div>
                       <div class="card-footer">
                        <div class="row">
                           <div class="col-lg-6"><p><small><i class="fal fa-map-marker-alt"></i> 1Km</small></p></div>
                           <div class="col-lg-6 text-right"><p class="pinkcol"><small>45 Products</small></p></div>
                        </div>
                      </div>
                   </div>
               </div>
                    <div class="col-lg-3 col-md-4">
                   <div class="card shadow-sm mb-4">
                       <div class="img-box">
                         <a href="shop.php">
                          <img src="assets/images/cat-shop-5.png" alt="" class="card-img-top"></a>
                         <!--<h5> <i class="fal fa-badge-percent"></i> OFFERS </h5>-->
                       </div>
                       <div class="card-body">
                         <div class="row">
                           <div class="col-lg-12">
                             <h4><a href="shop.php">LNE FOODS - MADDILAPALEM</a></h4>
                             <p><i class="fal fa-map mr-1"></i><!--?php// echo $value['address']; ?-->Maddilapalem</p>
                           </div>
                         </div>
                       </div>
                       <div class="card-footer">
                        <div class="row">
                           <div class="col-lg-6"><p><small><i class="fal fa-map-marker-alt"></i> 1Km</small></p></div>
                           <div class="col-lg-6 text-right"><p class="pinkcol"><small>45 Products</small></p></div>
                        </div>
                      </div>
                   </div>
               </div>

      
        </div>

      </div>
      <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
        
        <div class="row">
             <div class="col-lg-3 col-md-4">
                   <div class="card shadow-sm mb-4">
                       <div class="img-box">
                         <a href="shop.php">
                          <img src="assets/images/cat-shop.png" alt="" class="card-img-top"></a>
                    
                       </div>
                       <div class="card-body">
                         <div class="row">
                           <div class="col-lg-12">
                             <h4><a href="shop.php">LNE FOODS - MADDILAPALEM</a></h4>
                             <p><i class="fal fa-map mr-1"></i>Kancharapalem - Visakhapatnam</p>
                           </div>
                         </div>
                       </div>
                       <div class="card-footer">
                        <div class="row">
                           <div class="col-lg-6"><p><small><i class="fal fa-map-marker-alt"></i> 1Km</small></p></div>
                           <div class="col-lg-6 text-right"><p class="pinkcol"><small>45 Products</small></p></div>
                        </div>
                      </div>
                   </div>
               </div>
                    <div class="col-lg-3 col-md-4">
                   <div class="card shadow-sm mb-4">
                       <div class="img-box">
                         <a href="shop.php">
                          <img src="assets/images/cat-shop-1.png" alt="" class="card-img-top"></a>
                         <!--<h5> <i class="fal fa-badge-percent"></i> OFFERS </h5>-->
                       </div>
                       <div class="card-body">
                         <div class="row">
                           <div class="col-lg-12">
                             <h4><a href="shop.php">LNE FOODS - MADDILAPALEM</a></h4>
                             <p><i class="fal fa-map mr-1"></i><!--?php// echo $value['address']; ?-->Maddilapalem</p>
                           </div>
                         </div>
                       </div>
                       <div class="card-footer">
                        <div class="row">
                           <div class="col-lg-6"><p><small><i class="fal fa-map-marker-alt"></i> 1Km</small></p></div>
                           <div class="col-lg-6 text-right"><p class="pinkcol"><small>45 Products</small></p></div>
                        </div>
                      </div>
                   </div>
               </div>
                    <div class="col-lg-3 col-md-4">
                   <div class="card shadow-sm mb-4">
                       <div class="img-box">
                         <a href="shop.php">
                          <img src="assets/images/cat-shop-2.png" alt="" class="card-img-top"></a>
                         <!--<h5> <i class="fal fa-badge-percent"></i> OFFERS </h5>-->
                       </div>
                       <div class="card-body">
                         <div class="row">
                           <div class="col-lg-12">
                             <h4><a href="shop.php">LNE FOODS - MADDILAPALEM</a></h4>
                             <p><i class="fal fa-map mr-1"></i><!--?php// echo $value['address']; ?-->Maddilapalem</p>
                           </div>
                         </div>
                       </div>
                       <div class="card-footer">
                        <div class="row">
                           <div class="col-lg-6"><p><small><i class="fal fa-map-marker-alt"></i> 1Km</small></p></div>
                           <div class="col-lg-6 text-right"><p class="pinkcol"><small>45 Products</small></p></div>
                        </div>
                      </div>
                   </div>
               </div>
                    <div class="col-lg-3 col-md-4">
                   <div class="card shadow-sm mb-4">
                       <div class="img-box">
                         <a href="shop.php">
                          <img src="assets/images/cat-shop-3.png" alt="" class="card-img-top"></a>
                         <!--<h5> <i class="fal fa-badge-percent"></i> OFFERS </h5>-->
                       </div>
                       <div class="card-body">
                         <div class="row">
                           <div class="col-lg-12">
                             <h4><a href="shop.php">LNE FOODS - MADDILAPALEM</a></h4>
                             <p><i class="fal fa-map mr-1"></i><!--?php// echo $value['address']; ?-->Maddilapalem</p>
                           </div>
                         </div>
                       </div>
                       <div class="card-footer">
                        <div class="row">
                           <div class="col-lg-6"><p><small><i class="fal fa-map-marker-alt"></i> 1Km</small></p></div>
                           <div class="col-lg-6 text-right"><p class="pinkcol"><small>45 Products</small></p></div>
                        </div>
                      </div>
                   </div>
               </div>
                    <div class="col-lg-3 col-md-4">
                   <div class="card shadow-sm mb-4">
                       <div class="img-box">
                         <a href="shop.php">
                          <img src="assets/images/cat-shop-4.png" alt="" class="card-img-top"></a>
                         <!--<h5> <i class="fal fa-badge-percent"></i> OFFERS </h5>-->
                       </div>
                       <div class="card-body">
                         <div class="row">
                           <div class="col-lg-12">
                             <h4><a href="shop.php">LNE FOODS - MADDILAPALEM</a></h4>
                             <p><i class="fal fa-map mr-1"></i><!--?php// echo $value['address']; ?-->Maddilapalem</p>
                           </div>
                         </div>
                       </div>
                       <div class="card-footer">
                        <div class="row">
                           <div class="col-lg-6"><p><small><i class="fal fa-map-marker-alt"></i> 1Km</small></p></div>
                           <div class="col-lg-6 text-right"><p class="pinkcol"><small>45 Products</small></p></div>
                        </div>
                      </div>
                   </div>
               </div>
                    <div class="col-lg-3 col-md-4">
                   <div class="card shadow-sm mb-4">
                       <div class="img-box">
                         <a href="shop.php">
                          <img src="assets/images/cat-shop-5.png" alt="" class="card-img-top"></a>
                         <!--<h5> <i class="fal fa-badge-percent"></i> OFFERS </h5>-->
                       </div>
                       <div class="card-body">
                         <div class="row">
                           <div class="col-lg-12">
                             <h4><a href="shop.php">LNE FOODS - MADDILAPALEM</a></h4>
                             <p><i class="fal fa-map mr-1"></i><!--?php// echo $value['address']; ?-->Maddilapalem</p>
                           </div>
                         </div>
                       </div>
                       <div class="card-footer">
                        <div class="row">
                           <div class="col-lg-6"><p><small><i class="fal fa-map-marker-alt"></i> 1Km</small></p></div>
                           <div class="col-lg-6 text-right"><p class="pinkcol"><small>45 Products</small></p></div>
                        </div>
                      </div>
                   </div>
               </div>

      
        </div>

      </div>
    </div>
      </div><!--conatiner-->
    </section><!--nav-tabs-->

 <?php include ('includes/footer.php');?>
<script>
    function show(){
      document.getElementById("increament-div").style.display = "block";
      document.getElementById("addbtn").style.display = "none";
      document.getElementById("box").style.display = "block";
      setTimeout(function(){ document.getElementById("box").style.display = "none"; }, 500);
    }


    function inccreament()
    {
      document.getElementById("box").style.display = "block";
      setTimeout(function(){ document.getElementById("box").style.display = "none"; }, 500);
      
    }

    function deccreament() 
    {  
      
      document.getElementById("increament-div").style.display = "none";
        document.getElementById("addbtn").style.display = "block";
        document.getElementById("box1").style.display = "block";
      setTimeout(function(){ 
        document.getElementById("box1").style.display = "none"; 
        
      }, 500);
    }

</script> 