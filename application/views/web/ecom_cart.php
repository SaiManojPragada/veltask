<?php  $this->load->view("web/includes/ecom_header"); ?>
<!--Sliders Section-->
<div>
  <div id="scroll_id"></div>
    <div class="cover-image sptb-1 bg-background" data-image-src="../assets/images/banners/banner1.jpg">
        <div class="header-text1 mb-0">
            <div class="container">
                <div class="row">
                    <div class="col-xl-10 col-lg-12 col-md-12 d-block ">
                        <div class=" breadcrumb-banner text-left text-white ">
                            <h1 > Cart
                            </h1>
                        </div>
                        <div>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="<?= base_url() ?>">Home
                                    </a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="javascript:history.back();">Services
                                    </a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="javascript:void(0);">Cart
                                    </a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /header-text -->
    </div>
</div>

<?php if ($this->session->userdata("login_check") !== "Yes") { ?>
    <!--/Sliders Section-->
    <!--Add listing-->
    <section style="background : #fff" class="sptb">
        <div class="container" id="loadCartdiv">
            <div class="row">
                <?php 

                $session_id = $_SESSION['session_data']['session_id'];
                   $user_id= USER_ID; 
                    $qry = $this->db->query("select * from cart where session_id='".$session_id."' and user_id='".$user_id."'");
                    $del_b = $qry->row();
                    $shop = $this->db->query("select * from vendor_shop where id='".$del_b->vendor_id."'");
                    $shopdat = $shop->row();
                    $min_order_amount = $shopdat->min_order_amount;

                    $result = $qry->result();

                if ($qry->num_rows()>0) { ?>
                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <!--Classified Description-->
                        
                        <div class="cart-table-data">   
                        <table class="cart-table">
                                <thead>
                                    <tr>
                                        <!-- <th class="product_remove">Delete</th> -->
                                        <th class="product_thumb">Image</th>
                                        <th class="product_name">Product</th>
                                        <th class="product-price">Price</th>
                                        <th class="product_quantity">Quantity</th>
                                        <th class="product_total">Total</th>
                                        <th class="product_total">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                <?php 

                   
                    
                        $unitprice=0;
                        $gst=0;
                        foreach ($result as $value) 
                        {
                                $pro = $this->db->query("select * from  product_images where variant_id='".$value->variant_id."'");
                                $product = $pro->row();

                                if($product->image!='')
                                {
                                    $img = base_url()."uploads/products/".$product->image;
                                }
                                else
                                {
                                    $img = base_url()."uploads/noproduct.png";
                                }
                                $var1 = $this->db->query("select * from link_variant where id='".$value->variant_id."'");
                                $link = $var1->row();

                                 $pro1 = $this->db->query("select * from  products where id='".$link->product_id."'");
                                 $product1 = $pro1->row();

                                 $adm_qry = $this->db->query("select * from  admin_comissions where cat_id='".$product1->cat_id."' and shop_id='".$value->vendor_id."'");

                                 if($adm_qry->num_rows()>0)
                                 {
                                    $adm_comm = $adm_qry->row();
                                    $p_gst = $adm_comm->gst;
                                 }
                                 else
                                 {
                                    $p_gst = '0';
                                 }

                                 $class_percentage = ($value->unit_price/100)*$p_gst;

                                    $variants1 = $var1->result();
                                    $att1=[];
                                    foreach ($variants1 as $value1) 
                                    {

                                        $jsondata = $value1->jsondata;
                                        $values_ar=[];
                                        $json =json_decode($jsondata);
                                        foreach ($json as $value123) 
                                        {
                                            $type = $this->db->query("select * from attributes_title where id='".$value123->attribute_type."'");
                                            $types = $type->row();
                                        
                                            $val = $this->db->query("select * from attributes_values where id='".$value123->attribute_value."'");
                                            $value1 = $val->row();
                                            $values_ar[]=array('id'=>$value1->id,'title'=>$types->title,'value'=>$value1->value);
                                        }
                                    }

                                    $unitprice = $value->unit_price+$unitprice;
                                    $gst = $class_percentage+$gst;
                                ?>
                                <tr>
                                    <td class="product_thumb"><a href="<?php echo base_url(); ?>web/product_view/<?php echo $product1->seo_url; ?>" ><img src="<?php echo $img; ?>" alt="" style="width: 80px; height: 80px;"></a></td>
                                    <td class="product_name"><a href="<?php echo base_url(); ?>web/product_view/<?php echo $product1->seo_url; ?>" ><?php echo $product1->name; ?></a><br>

                                      <p class="mb-0"><a href="<?php echo base_url(); ?>web/store/<?php echo $shopdat->seo_url; ?>/shop"><b>Shop Name :</b> <?php echo $shopdat->shop_name; ?></a></p>
                                      <?php foreach($values_ar as $atr){ ?>
                                      <div>
                                        <p><b><?php echo $atr['title']; ?>:</b><?php echo $atr['value']; ?></p>
                                    <?php } ?>
                                        </div>
                                      <p><b>Location:</b><?php echo $shopdat->city; ?></p>
                                    </td>
                                    <td class="product-price"><i class="fal fa-rupee-sign"></i> <?php echo $value->price; ?></td>
                                <form class="form-horizontal" enctype="multipart/form-data"  >
                                    <td class="product_quantity"><label></label> 
                                       <a  onclick="decreaseQty(<?php echo $value->id; ?>)">➖</a>
                                      <input min="1" max="100" name="quantity" id="quantity<?php echo $value->id; ?>" value="<?php echo $value->quantity; ?>" type="text" readonly>
                                      <a onclick="increaseQty(<?php echo $value->id; ?>)">➕</a>
                                      <div id="quantityshow<?php echo $value->id; ?>"></div>
                                    </td>
                                    <td class="product_total"><i class="fal fa-rupee-sign"></i> <?php echo $value->unit_price; ?></td>
                                    <td>
                                      <a onclick="deletecartitems(<?php echo $value->id; ?>)" class="remove-item"><i class="fal fa-trash-alt"></i></a>
                                    </td>
                                </form>
                                </tr>                                
                                <?php } 
                                    $grand_t =  $min_order_amount+$unitprice+$gst;
                                ?>
                                <tr>
                                    <td colspan="6">
                                        <a class="btn pink-btn float-right" href="<?php echo base_url(); ?>web/store/<?php echo $seo_url; ?>/shop">Continue Shopping</a>
                                    </td>
                                </tr>

                            </tbody>
                        </table>  

                        </div><!--cart-data-->


                        <div class="row justify-content-center pb-5">
                        <div class="col-lg-5 col-md-6">
                            <div class="coupon_code left">
                                <h3>Apply Coupon</h3>
                                <div class="coupon_inner">   
                                    <p>Enter your coupon code if you have one.</p>   
                                    <div id="coupon_msg"></div>  
                                    
                                   <form class="form-horizontal" enctype="multipart/form-data"  >                       
                                        <input placeholder="Coupon code" id="couponcode" type="text">
                                        <button type="button" onclick="validatecouponcode()">Apply coupon</button>
                                    </form> 
                                </div>
                                <hr>
                                <?php if(count($coupons)>0){ ?>
                                <div class="row justify-content-center" id="show_button">
                                    <div class="col-md-4">
                                        <a  class="btn-viewcoup mb-4" onclick="showhide()">View Coupons</a> 
                                    </div>
                                </div>
                                 <div class="row justify-content-center" id="close_button" style="display: none;">
                                    <div class="col-md-4">
                                        <a  class="btn-viewcoup mb-4" onclick="closecoupon()">Close Coupons</a> 
                                    </div>
                                </div>
                            <?php } ?>
                            </div>
                            <div class="couponlist-box collapse multi-collapse" id="viewcoup">
                               

                                <?php foreach($coupons as $coup){ ?>
                                <div class="row">
                                <div class="col-8">
                                    <h5><?php echo $coup['percentage']; ?>% OFF</h5>
                                    <p><strong><?php echo $coup['description']; ?></strong></p>
                                </div>
                                <div class="col-4 alig-self-center">
                                    <h6><input type="text" style="width: 100px; border: none; background: none;" value="<?php echo $coup['coupon_code']; ?>" id="myInput<?php echo $coup['id']; ?>"></h6>
                                    <p class="text-center"><small><a onclick="applyCouponcode('<?php echo $coup['coupon_code']; ?>')">Apply Coupon Code</a></small></p>
                                </div>
                                </div>
                              <?php } ?>


                              
                            </div>

                        </div>
                        <div class="col-lg-5 col-md-6">
                            <div class="coupon_code right">
                                <h3>Cart Totals</h3>
                                <div class="coupon_inner">
                                   <div class="cart_subtotal">
                                       <p>Subtotal</p>
                                       <p class="cart_amount"><i class="fal fa-rupee-sign"></i> <?php echo $unitprice; ?></p>
                                       <input type="hidden" id="sub_total" value="<?php echo $unitprice; ?>" >
                                   </div>
                                   <div class="cart_subtotal ">
                                       <p>Shipping Charges</p>
                                       <p class="cart_amount"><i class="fal fa-rupee-sign"></i>  <?php echo $min_order_amount; ?></p>
                                       <input type="hidden" id="min_order_amount" value="<?php echo $min_order_amount; ?>" >
                                   </div>
                                   <div class="cart_subtotal">
                                       <p>Discount</p>
                                       <p class="cart_amount" id="discount">0</p>
                                   </div>

                                   <div class="cart_subtotal">
                                       <p>GST</p>
                                       <p class="cart_amount"><?php echo $gst; ?></p>
                                       <input type="hidden" id="gst" value="<?php echo $gst; ?>">
                                   </div>
                                   
                                   <div class="cart_subtotal" id="total_default">
                                       <p>Total</p>
                                       <p class="cart_amount"><i class="fal fa-rupee-sign"></i> <input type="hidden" id="cart_total" value="<?php echo $grand_t; ?>" > <?php echo $grand_t; ?></p>
                                   </div>
                                   <div class="cart_subtotal" id="total_default_show" style="display: none;">
                                       <p>Total</p>
                                       <p class="cart_amount" id="total_p"><?php echo $grand_t; ?></p>
                                   </div>
                                   <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo base_url(); ?>web/goaddress_page">
                                     <input type="hidden" name="coupon_id" id="coupon_id" value="0">
                                     <input type="hidden" name="applied_coupon_code" id="applied_coupon_code" value="0">
                                     <input type="hidden" name="coupon_discount" id="coupon_discount" value="0">
                                   
                                       <div class="checkout_btn">
                                        <button type="submit">Proceed to Checkout</button>
                                       </div>
                                   </form>

                                   <?php $adm_qry = $this->db->query("select * from admin where bid_show_status=1");
                                         if($adm_qry->num_rows()>0){ ?>
                                   
                                <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                       

                    </div>
                    <!--Right Side Content-->
                   

                <?php } else if (empty($cart)) { ?>
                    <div class="col-12">
                        <center style="margin: 220px 0px"><h2>-- No Items In Your Cart --</h2></center>
                    </div>
                <?php } ?>
                <!--/Right Side Content-->
            </div>
        </div>
    </section>

<?php } else { ?>
    <div class="container-fluid" style="min-height: 500px">
        <center style='margin-top: 200px'>
            <strong><h2>User Not Logged In</h2></strong>
        </center>
    </div>
<?php } ?>

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


    <script type="text/javascript">
function showhide()
{
        $("#viewcoup").show();
        $("#close_button").show();
        $("#show_button").hide();
}

function closecoupon()
{
       $("#viewcoup").hide();
        $("#close_button").hide();
        $("#show_button").show();
}
       


function increaseQty(id)
  {
    


    var quantity = $("#quantity"+id).val();
        var qty = 1;
        var final = parseInt(qty)+parseInt(quantity);

        $.ajax({
            url:"<?php echo base_url(); ?>web/updateCart",
            method:"POST",
            data:{cartid:id,quantity:final},
            success:function(data)
            {
                  var str = data;
              var res = str.split("@");
              if(res[1]=='error')
              {
                      $('#quantityshow'+id).html('<span class="error" style="color:red;font-size: 16px;margin-left: 18px; width:100%">'+res[2]+'</span>');
                      $('#quantityshow').focus();
                      return false;
              }
              else
              {
                  $("#loadCartdiv").html(data);
              }

              

              //alert(JSON.stringify(data));
                 /*$('html, body').animate({
                    scrollTop: $('#scroll_id').offset().top - 100 //#DIV_ID is an example. Use the id of your destination on the page
                }, 'slow');
                  if(res[1]=='success')
                  {
                    
                    $("#quantity"+id).val(final);

                     $('#display_msg').html('<span class="error" style="color:red;font-size: 16px;margin-left: 18px; width:100%">Quantity updated to cart</span>');
                      $('#display_msg').focus();
                      location.reload();
                      return false;
                  }
                  else 
                  {
                      $('#display_msg').html('<span class="error" style="color:red;font-size: 16px;margin-left: 18px; width:100%">'+res[2]+'</span>');
                      $('#display_msg').focus();
                      return false;
                  }*/
            }
        });

  }

  function decreaseQty(id)
  {

    var quantity = $("#quantity"+id).val();
    if(quantity==1)
    {
      return false;
    }
    else
    {
      var qty = 1;
    var final = parseInt(quantity)-parseInt(qty);
    
        $.ajax({
            url:"<?php echo base_url(); ?>web/removeCart",
            method:"POST",
            data:{cartid:id,quantity:final},
            success:function(data)
            {
                  var str = data;
              var res = str.split("@");
              $("#loadCartdiv").html(data);
                /* $('html, body').animate({
                    scrollTop: $('#scroll_id').offset().top - 100 //#DIV_ID is an example. Use the id of your destination on the page
                }, 'slow');
                  if(res[1]=='success')
                  {
                    
                    $("#quantity"+id).val(final);

                     $('#display_msg').html('<span class="error" style="color:red;font-size: 16px;margin-left: 18px; width:100%">Quantity updated to cart</span>');
                      $('#display_msg').focus();
                      location.reload();
                      return false;
                  }
                  else 
                  {
                      $('#display_msg').html('<span class="error" style="color:red;font-size: 16px;margin-left: 18px; width:100%">'+res[2]+'</span>');
                      $('#display_msg').focus();
                      return false;
                  }*/
            }
        });
     }
  }

  function getCoup(code) {
  /* Get the text field */
  var copyText = document.getElementById("myInput"+code);

  /* Select the text field */
  copyText.select();
  copyText.setSelectionRange(0, 99999); /* For mobile devices */

  /* Copy the text inside the text field */
  document.execCommand("copy");

  /* Alert the copied text */
  alert("Copied the text: " + copyText.value);
}
</script>
    <script type="text/javascript">


        function doBidProducts(){

            
             var coupon_discount = $("#coupon_discount").val();
             if(coupon_discount!=0)
             {
                    swal('Please remove the coupon, if you want to add bidding');
             }
             else
             {
                        var shop_id = "<?php echo $shop_id; ?>";
                        var id = $(this).parents("tr").attr("id");

                       swal({

                        title: "Confirm!",

                        text: "Are you sure you want to add the Bid ",

                        type: "warning",

                        showCancelButton: true,

                        confirmButtonClass: "btn-danger",

                        confirmButtonText: "Yes",

                        cancelButtonText: "Cancel",

                        closeOnConfirm: false,

                        closeOnCancel: false

                      },

                      function(isConfirm) {

                        if (isConfirm) {

                                  $.ajax({
                                    url:"<?php echo base_url(); ?>web/createUserBid",
                                    method:"POST",
                                    data:{vendor_id:shop_id},
                                    success:function(data)
                                    {
                                        var str = data;
                                        var res = str.split("@");
                                          if(res[1]=='success')
                                          {
                                                swal('Bid Created Successfully');
                                                window.location.href = "https://retos.in";
                                          }
                                          else if(res[1]=='minimum')
                                          {
                                            swal("Bidding is applicable on minimum amount of 10,000"); 
                                          }
                                          else
                                          {
                                            swal("Something went wrong,Please try again"); 
                                          }
                                                          
                                    }
                                   });
                                

                        } else {

                          swal("Cancelled", "", "error");

                        }

                      });

     }

    }



      function applyCouponcode(couponcode)
      {
        $('.error').remove();
            var errr=0;
        var carttotal = $("#sub_total").val();
        var min_order_amount= $("#min_order_amount").val();

        var total_amount = $("#cart_total").val();
        $.ajax({
            url:"<?php echo base_url(); ?>web/applycoupon",
            method:"POST",
            data:{couponcode:couponcode,carttotal:carttotal,total_amount:total_amount},
            success:function(data)
            {
                  var str = data;
              var res = str.split("@");
                  
                
                  if(res[1]=='success')
                  {
                      //$("#couponcode").val();
                      $('#discount').html("₹"+res[3]);
                      $("#couponcode").val(couponcode);
                      var gst = $("#gst").val();
                      
                      var total_p = parseFloat(min_order_amount)+parseFloat(res[2]);
                      var final_amount = parseFloat(total_p)+parseFloat(gst);
                      $('#total_p').html("₹"+final_amount.toFixed(2));

                      $('#total_default').hide();
                      $('#total_default_show').show();

                      $('#coupon_id').val(res[4]);
                      $('#applied_coupon_code').val(res[5]);
                      $('#coupon_discount').val(res[3]);
 

                
                     $('#coupon_msg').html('<div class="btn btn-success mb-4" >Coupon Applied successfully <span onclick="remove()" class="pr-2"><i class="fal fa-times"></i></span></div>');
                      $('#coupon_msg').focus();
                        return false;
                  }
                  else if(res[1]=='minorder'){
                         $('#coupon_msg').html('<div class="btn btn-danger mb-4" >Minimum Order Amount'+res[2]+'</div>');
                         $('#coupon_msg').focus();
                         return false;
                  }
                  else 
                  {

                      $('#coupon_msg').html('<div class="btn btn-danger mb-4" >Invalid Coupon</div>');
                      $('#coupon_msg').focus();
                        return false;
                  }
            }
        });
  }


      function validatecouponcode()
      {
        $('.error').remove();
            var errr=0;
        var couponcode = $("#couponcode").val();
        var carttotal = $("#sub_total").val();

         var total_amount = $("#cart_total").val();

         var min_order_amount= $("#min_order_amount").val();

        if(couponcode=='')
        {
            $('#coupon_msg').html('<div class="btn btn-danger mb-4" >Enter Coupon code</div>');
                      $('#coupon_msg').focus();
                        return false;
        }
        else
        {
        
        $.ajax({
            url:"<?php echo base_url(); ?>web/applycoupon",
            method:"POST",
            data:{couponcode:couponcode,carttotal:carttotal,total_amount:total_amount},
            success:function(data)
            {
                  var str = data;
              var res = str.split("@");
                
                  if(res[1]=='success')
                  {

                       $('#discount').html("₹"+res[3]);

                      var total_p = parseInt(min_order_amount)+parseInt(res[2]);

                      $('#total_p').html("₹"+total_p);

                      $('#total_default').hide();
                      $('#total_default_show').show();

                      $('#coupon_id').val(res[4]);
                      $('#applied_coupon_code').val(res[5]);
                      $('#coupon_discount').val(res[3]);

                     $('#coupon_msg').html('<div class="btn btn-success mb-4" >Coupon Applied successfully <span onclick="remove()" class="pr-2"><i class="fal fa-times"></i></span></div>');
                      $('#coupon_msg').focus();
                        return false;
                  }
                  else if(res[1]=='minorder'){
                         $('#coupon_msg').html('<div class="btn btn-danger mb-4" >Minimum Order Amount'+res[2]+'</div>');
                         $('#coupon_msg').focus();
                         return false;
                  }
                  else 
                  {

                      $('#coupon_msg').html('<div class="btn btn-danger mb-4" >Invalid Coupon</div>');
                      $('#coupon_msg').focus();
                        return false;
                  }
            }
        });
     }
  }

  function remove()
  {
    location.reload();
  }

      function updateCart(cartid)
      {
        var quantity = $("#quantity"+cartid).val();
        $.ajax({
            url:"<?php echo base_url(); ?>web/updateCart",
            method:"POST",
            data:{cartid:cartid,quantity:quantity},
            success:function(data)
            {
                  var str = data;
              var res = str.split("@");
              
                 $('html, body').animate({
                    scrollTop: $('#scroll_id').offset().top - 100 //#DIV_ID is an example. Use the id of your destination on the page
                }, 'slow');
                  if(res[1]=='success')
                  {

                     $('#display_msg').html('<span class="error" style="color:red;font-size: 16px;margin-left: 18px; width:100%">Quantity updated to cart</span>');
                      $('#display_msg').focus();
                      location.reload();
                      return false;
                  }
                  else 
                  {
                      $('#display_msg').html('<span class="error" style="color:red;font-size: 16px;margin-left: 18px; width:100%">'+res[2]+'</span>');
                      $('#display_msg').focus();
                      return false;
                  }
            }
        });
      }
        function deletecartitems(cartid)
        {

             $.ajax({
              url:"<?php echo base_url(); ?>web/deleteCartItem",
              method:"POST",
              data:{cartid:cartid},
              success:function(data)
              {
                 var str = data;
              var res = str.split("@");
               
                  /*if(res[1]=='success')
                  {*/
                    swal("Cart Item deleted successfully");
                     var str = data;
                      var res = str.split("@");
                      <?php 
                        $session_id = $_SESSION['session_data']['session_id'];
                        $user_id= USER_ID; 
                        $qry = $this->db->query("select * from cart where session_id='".$session_id."' and user_id='".$user_id."'");
                        $cart_num_rows = $qry->num_rows();
                    ?>
                    var cartcount = '<?php echo $cart_num_rows; ?>';
                    var finalcount = cartcount-1;
                      $('#cart_count').html(finalcount);
                      $("#loadCartdiv").html(data);


                     // $('#display_msg').html('<span class="error" style="color:red;font-size: 16px;margin-left: 18px; width:100%">Cart Item deleted successfully</span>');
                     //  $('#display_msg').focus();

                       //location.reload();
                     //    return false;



                         
                  /*}
                  else 
                  {
                    swal("Something went wrong , please try again");*/
                      // $('#display_msg').html('<span class="error" style="color:red;font-size: 16px;margin-left: 18px; width:100%">Something went wrong , please try again</span>');
                      // $('#display_msg').focus();
                      //   return false;
                  //}
                      
                        
              
              }
             });









           
    }
    </script>
     <!--shopping cart area end -->
<?php  $this->load->view("web/includes/footer"); ?>