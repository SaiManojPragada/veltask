<?php include 'includes/ecom_header.php'; ?>

		    <div>
      <div class="cover-image sptb-1 bg-background" data-image-src="../assets/images/banners/banner1.jpg">
        <div class="header-text1 mb-0">
          <div class="container">
            <div class="row">
              <div class="col-xl-10 col-lg-12 col-md-12 d-block ">
                <div class=" breadcrumb-banner text-left text-white ">
                  <h1 ><?php echo $shop_name; ?></h1>
                </div>
                <div>
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                      <a href="index.php"><?php echo $shop_name; ?>
                      </a>
                    </li>
                    
                    <li class="breadcrumb-item active" aria-current="page"><a href="<?php echo base_url(); ?>web/store/<?php echo $shop_seourl; ?>/shop"><?php echo $subcategory_title; ?></a>
                    </li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /header-text -->
      </div>
    </div>



		<section class="shop-items px-5 py-5">
			<div class="container">
				<div class="short-by mb-4 d-flex justify-content-end">
					<div class="dropdown">
						  <a class="btn btn-default dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Short By
						  </a>

						  
						    <select name="lang" onchange="getProducts(this.value)">
                                <option selected value="">Sort by</option>
                                <option  value="1">Price - High to Low</option>
                                <option value="0">Newest First</option>
                                <option value="2">Price - Low to High</option>
                            </select>
						  
						</div>
				</div><!--short-by-->
				<div class="row" id="product_list">
				
				<?php 

                    if(count($product_details)>0)
                    {
                    foreach($product_details as $prod) { ?>
				<div class="col-md-3 mb-4">
					<div class="item shop-item-cat">
							<a href="<?php echo base_url(); ?>web/product_view/<?php echo $prod['seo_url']; ?>">
							<img src="<?php echo $prod['image']; ?>" alt="">
						</a>
						<div class="popular-products">
							<h2 class="mb-0"><a href="<?php echo base_url(); ?>web/product_view/<?php echo $prod['seo_url']; ?>"><?php echo $prod['name']; ?></a> </h2>
							
							<span><a href="product-view.php">Revathimaheshwar Stores</a></span>
							<div class="price"><?php echo $prod['saleprice']; ?></div>
							<a  onclick="addtocart(<?php echo $prod['variant_id']; ?>,<?php echo $prod['shop_id']; ?>,'<?php echo $prod['saleprice']; ?>',1)" class="btn btn-primary btn-ptill mb-3"><i class="fal fa-shopping-cart"></i> Add to Cart</a>
						</div>
					</div>
				</div><!--col-md-3-->

				 <?php } 


                    }
                    else
                    { ?>

                       <div class="row shop_wrapper" >
                          <div class="col-lg-12 col-md-12 col-sm-12 col-12" style="text-align: center;">
                                <img src="<?php echo base_url();?>/uploads/web_no_products.png" style="width: 250px">
                                <h3 style="text-align: center;">No Products</h3>
                          </div>
                        </div>
                        
                 <?php   }

                    ?>

			</div><!--row-->
			</div><!--container-->
		</section><!--shop-items-->

	
<script type="text/javascript">
  function getProducts(type)
  {
           var shop_id = '<?php echo $shop_id; ?>';
           var catid = '<?php echo $catid; ?>';
           var subcatid = '<?php echo $subcatid; ?>';
            $('.error').remove();
            var errr=0;
            $.ajax({
              url:"<?php echo base_url(); ?>web/sort_products",
              method:"POST",
              data:{type:type,catid:catid,shop_id:shop_id,subcatid:subcatid},
              success:function(data)
              {
                //alert(JSON.stringify(data));
                 $('#product_list').html(data);
              }
             });
  }

  function addFavorite(pid)
  {
    var user_id = '<?php echo $_SESSION['userdata']['user_id']; ?>';
      if(user_id=='')
      {
        $('#loginModal').modal('show');
        return false;
      }
      else
      {

            $('.error').remove();
            var errr=0;
            $.ajax({
              url:"<?php echo base_url(); ?>web/add_remove_favourite",
              method:"POST",
              data:{pid:pid},
              success:function(data)
              {
                 var str = data;
              var res = str.split("@");
             
                  if(res[1]=='remove')
                  {
                     $("#bestselling_pro_"+pid).removeClass("fas");
                      $("#bestselling_pro_"+pid).addClass("fal");
                  }
                  else if(res[1]=='add')
                  {
                      $("#bestselling_pro_"+pid).removeClass("fal");
                      $("#bestselling_pro_"+pid).addClass("fas");
                  }
                      
                        
              
              }
             });
      }
  }

</script>

<?php include 'includes/footer.php'; ?>