<?php  $this->load->view("web/includes/ecom_header"); ?>
<!--breadcrumbs area start-->
<div class="breadcrumbs_area">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="breadcrumb_content">
					<h3>Payment</h3>
				</div>
			</div>
		</div>
	</div>
</div>
<!--breadcrumbs area end-->
<div class="shopping_cart_area pb-5">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-10">
				<div class="row pb-5">
					<div class="col-lg-12 col-md-12">
						      <ul class="step d-flex flex-nowrap">
                    <li class="step-item <?php if ($page == 'goaddress_page') { ?>active <?php } ?>">
                      <a href="#!" class="">Add Address</a>
                    </li>
                    <li class="step-item <?php if ($page == 'order_review.php') { ?>active <?php } ?>">
                      <a href="#!" class="">Order Review</a>
                    </li>
                    <li class="step-item <?php if ($page == 'payment.php') { ?>active <?php } ?>">
                      <a href="#!" class="">Payment</a>
                    </li>
                    <li class="step-item <?php if ($page == 'order_confirm.php') { ?>active <?php } ?>">
                      <a href="#!" class="">Order Confirmation</a>
                    </li>
                  </ul>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12">
						
							
						<div class="form-check pb-2">
						  <input class="form-check-input" type="radio" checked name="inlineRadioOptions" id="inlineRadio2" value="ONLINE" onclick="selectPayment('ONLINE')">
						  <label class="form-check-label" for="inlineRadio2">ONLINE</label>
						</div>
						<div class="form-check pb-2">
						  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="COD" onclick="selectPayment('COD')">
						  <label class="form-check-label" for="inlineRadio3">Cash on Delivery</label>
						</div>

						
						 <button style="    width: 200px;
    margin-top: 11px;" type="submit" onclick="paynow(<?php echo round($total_price); ?>,<?php echo $aid; ?>)" class="btn btn-pink btn-block" id="online">Pay Now</button>

            <button  style="    width: 200px;
    margin-top: 11px; display: none;" type="submit" class="btn btn-pink btn-block" id="offline" style="display: none;" onclick="payoffline(<?php echo $total_price; ?>,<?php echo $aid; ?>)">Pay Now</button>

						




						

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php  $this->load->view("web/includes/footer"); ?>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>


    function selectPayment(payment_type)
    {
            if(payment_type=='ONLINE')
            {
                document.getElementById("online").style.display = "block";
                document.getElementById("offline").style.display = "none";
            }
            else if(payment_type=='COD')
            {
                 document.getElementById("offline").style.display = "block";
                 document.getElementById("online").style.display = "none";
            }
    }
  var SITEURL = "<?php echo base_url() ?>";





function payoffline(price,aid)
{
    var totalAmount = '<?php echo $total_price; ?>';
    var  coupon_id = '<?php echo $coupon_id; ?>';
    var  coupon_code = '<?php echo $coupon_code; ?>';
    var  coupon_discount = '<?php echo $coupon_discount; ?>';
      $.ajax({
              url:SITEURL + 'web/doOrder',
              method:"POST",
              data:{totalAmount : totalAmount,address_id:aid,coupon_id:coupon_id,coupon_code:coupon_code,coupon_discount:coupon_discount},
              success:function(data)
              {
                 var str = data;
                var res = str.split("@");
                if(res[1]=='success')
                {
                       window.location.href = SITEURL + 'web/RazorThankYou';
                }
                else if(res[1]=='noprod')
                {
                       swal(res[2]);
                }
                else if(res[1]=='shopclosed')
                {
                       swal("Shop Closed")
                }
                else
                {
                  swal("Something went wrong, Please try again")
                }
              }
             });
}



function paynow(price,aid)
{
    var totalAmount = '<?php echo round($total_price); ?>';
    var  coupon_id = '<?php echo $coupon_id; ?>';
    var  coupon_code = '<?php echo $coupon_code; ?>';
    var  coupon_discount = '<?php echo $coupon_discount; ?>';
    var phone = '<?php echo $phone; ?>';
    var email = '<?php echo $email; ?>';
   //alert(phone); alert(email);
// rzp_test_ywjRok0nPJdn2M
    var options = {
    "key": "rzp_test_ywjRok0nPJdn2M",
    "amount": (totalAmount*100), // 2000 paise = INR 20
    "name": "VELTASK",
    "email":email,
    "phone":phone,
    "description": "Payment",
    "image": "https://dev.veltask.com/admin_assets/assets/images/logo.png",
    "handler": function (response){



      $.ajax({
              url:SITEURL + 'web/razorPaySuccess',
              method:"POST",
              data:{razorpay_payment_id: response.razorpay_payment_id , totalAmount : totalAmount,address_id:aid,coupon_id:coupon_id,coupon_code:coupon_code,coupon_discount:coupon_discount},
              success:function(data)
              {
                 var str = data;
              var res = str.split("@");
              if(res[1]=='success')
                {
                       window.location.href = SITEURL + 'web/RazorThankYou';
                }
                else if(res[1]=='noprod')
                {
                       swal(res[2]);
                }
                else if(res[1]=='shopclosed')
                {
                       swal("Shop Closed")
                }
                else
                {
                  swal("Something went wrong, Please try again")
                }
                
                        
              }
             });
    },

    "theme": {
        "color": "#528FF0"
    }
  };
  var rzp1 = new Razorpay(options);
  rzp1.open();
  e.preventDefault();


}

</script>