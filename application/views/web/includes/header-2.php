<!doctype html>

<html class="no-js" lang="en">

    <head>
        <!-- META DATA -->
        <meta charset="UTF-8">
        <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="keywords" content="">		
        <link rel="icon" href="favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />
        <!-- Title -->
        <title>:: VelTask ::</title>
        <!-- Bootstrap Css -->
        <link href="assets/plugins/bootstrap-4.3.1-dist/css/bootstrap.min.css" rel="stylesheet" />

        <!-- Dashboard Css -->
        <link href="assets/css/dashboard.css" rel="stylesheet" />

        <!-- RTL Css -->
        <link href="assets/css/rtl.css" rel="stylesheet" />

        <!-- Font-awesome  Css -->
        <link href="assets/css/icons.css" rel="stylesheet"/>
        <link rel="stylesheet" href="assets/css/blog.css">

        <!--Horizontal Menu-->
        <link href="assets/plugins/Horizontal2/Horizontal-menu/dropdown-effects/fade-down.css" rel="stylesheet" />
        <link href="assets/plugins/Horizontal2/Horizontal-menu/horizontal.css" rel="stylesheet" />

        <!--Select2 Plugin -->
        <link href="assets/plugins/select2/select2.min.css" rel="stylesheet" />

        <!-- Cookie css -->
        <link href="assets/plugins/cookie/cookie.css" rel="stylesheet">

        <!-- Owl Theme css-->
        <link href="assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet" />

        <!-- Custom scroll bar css-->
        <link href="assets/plugins/scroll-bar/jquery.mCustomScrollbar.css" rel="stylesheet" />

        <!-- COLOR-SKINS -->
        <link id="theme" rel="stylesheet" type="text/css" media="all" href="../../assets/webslidemenu/color-skins/color10.css" />
        <script src="assets/js/vendors/jquery-3.2.1.min.js"></script>
        <link rel="stylesheet" href="assets/css/time-date.css">
        <link rel="stylesheet" href="assets/css/add-button-aanimation.css">
        <link rel="stylesheet" href="assets/css/my-booking.css">
        <link rel="stylesheet" href="assets/css/my-dashbaord.css">
        <link rel="stylesheet" href="assets/css/f5.css">
        <link rel="stylesheet" href="assets/css/inner-pages.css">

    </head>
    <body>
        <?php $page = basename($_SERVER['SCRIPT_NAME']); ?>
        <!-- Login box -->

        <!-- Ends Here -->

        <!-- Login box -->

        <!-- Ends Here -->

        <!--Topbar-->
        <div class="header-main">
            <div class="top-bar">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-8 col-lg-8 col-sm-4 col-7">
                            <div class="top-bar-left d-flex">
                                <div class="clearfix">
                                    <ul class="socials">
                                        <li>
                                            <a class="social-icon text-dark" href="#"><i class="fab fa-facebook-f"></i></a>
                                        </li>
                                        <li>
                                            <a class="social-icon text-dark" href="#"><i class="fab fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a class="social-icon text-dark" href="#"><i class="fab fa-linkedin-in"></i></a>
                                        </li>										
                                    </ul>
                                </div>
                                <div class="clearfix">
                                    <ul class="contact border-left">								
                                        <li class="select-country">
                                            <i class="far fa-map-marker-alt"></i><select class="form-control" data-placeholder="Select City">
                                                <option value="UM">Visakhapatnam</option>
                                                <option value="AF">Hyderabad</option>
                                            </select>
                                        </li>										

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-sm-8 col-5">
                            <div class="top-bar-right">
                                <ul class="custom">
                                    <!-- 	<li>
                                                    <a href="#" class="text-dark" onclick="openregister()"><i class="fa fa-user mr-1"></i> <span>Register</span></a>
                                            </li> -->
                                    <li>
                                        <a href="index.php" class="text-dark" onclick="openNav()"><i class="fa fa-sign-in mr-1"></i> <span>Logout</span></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="text-dark" data-toggle="dropdown"><i class="fa fa-tachometer"></i><span> My Dashboard</span></a>
                                        <!-- <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                <a href="mydash.html" class="dropdown-item">
                                                        <i class="dropdown-icon icon icon-user"></i> My Profile
                                                </a>
                                                <a class="dropdown-item" href="my-booking.php">
                                                        <i class="dropdown-icon icon icon-doc"></i> My Booking
                                                </a>
                                                <a class="dropdown-item" href="my-orders.php">
                                                        <i class="dropdown-icon icon icon-basket"></i> My Orders
                                                </a>
                                                <a class="dropdown-item" href="notifications.php">
                                                        <i class="dropdown-icon icon icon-bell"></i> Notifications
                                                </a>
                                                <a href="my-profile.php" class="dropdown-item">
                                                        <i class="dropdown-icon  icon icon-settings"></i> My Profile
                                                </a>
                                                <a class="dropdown-item" href="index.php">
                                                        <i class="dropdown-icon icon icon-power"></i> Log out
                                                </a>
                                        </div> -->
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Mobile Header -->
            <div class="horizontal-header clearfix ">
                <div class="container">
                    <a id="horizontal-navtoggle" class="animated-arrow"><span></span></a>
                    <a href="index.php"><span class="smllogo"><img src="assets/images/veltask-logo-color.png" width="150" alt=""/></span></a>
                    <a href="tel:245-6325-3256" class="callusbtn"><i class="fa fa-phone" aria-hidden="true"></i></a>
                </div>
            </div>
            <!-- /Mobile Header -->

            <div class="horizontal-main bg-dark-transparent clearfix">
                <div class="horizontal-mainwrapper container clearfix">
                    <div class="desktoplogo">
                        <a href="index.php"><img src="assets/images/veltask-logo.png" alt=""></a>
                    </div>
                    <div class="desktoplogo-1">
                        <a href="index.php"><img src="assets/images/veltask-logo-color.png" alt=""></a>
                    </div>
                    <!--Nav-->
                    <nav class="horizontalMenu clearfix d-md-flex">
                        <ul class="horizontalMenu-list">
                            <li aria-haspopup="true"><a href="#">Services <span class="fa fa-caret-down m-0"></span></a>
                                <div class="horizontal-megamenu clearfix">
                                    <div class="container">
                                        <div class="megamenu-content">
                                            <div class="row">
                                                <ul class="col link-list">
                                                    <li class="title">Beauty & Salon</li>
                                                    <li><a href="women-services.php">Women's</a></li>	
                                                    <li><a href="#">Men's</a></li>
                                                    <li><a href="#">Kid's</a></li>												
                                                </ul>
                                                <ul class="col link-list">
                                                    <li class="title">Cleaning Services</li>
                                                    <li><a href="#">Full Home Cleaning</a></li>
                                                    <li><a href="#">Bathroom Cleaning</a></li>
                                                    <li><a href="#">Kitchen Cleaning</a></li>
                                                    <li><a href="#">Sofa & Carpet Cleaning</a></li>
                                                    <li><a href="#">Car Cleaning</a></li>
                                                </ul>
                                                <ul class="col link-list">
                                                    <li class="title">Appliances Services</li>
                                                    <li><a href="#">Air Conditioner</a></li>
                                                    <li><a href="#">Microwave</a></li>
                                                    <li><a href="#">Refrigerator</a></li>
                                                    <li><a href="#">Washing Machine</a></li>
                                                    <li><a href="#">Water Purifier</a></li>
                                                    <li><a href="#">Chimney</a></li>
                                                </ul>
                                                <ul class="col link-list">
                                                    <li class="title">Electrician</li>
                                                    <li><a href="#">Switches & Sokets</a></li>
                                                    <li><a href="#">Lights & Fans</a></li>
                                                    <li><a href="#">Room Heater</a></li>
                                                    <li><a href="#">MCB & Fuse</a></li>
                                                    <li><a href="#">Door Bell</a></li>
                                                    <li><a href="#">House Wiring</a></li>
                                                </ul>
                                                <ul class="col link-list">
                                                    <li class="title">Ceiling (POP)</li>
                                                    <li><a href="#">Living room</a></li>
                                                    <li><a href="#">BedRoom</a></li>
                                                    <li><a href="#">Kitchen</a></li>
                                                    <li><a href="#">House Interior Celing</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li aria-haspopup="true"><a href="#">Grocery </a></li>
                            <li aria-haspopup="true"><a href="#">Meat </a></li>
                            <li aria-haspopup="true"><a href="#">Food</a></li>
                            <li aria-haspopup="true"><a href="#">Electronics</a></li>
                            <li aria-haspopup="true"><a href="#">Retails & Cloth</a></li>
                            <li aria-haspopup="true"><a href="#">More</a></li>
                            <li aria-haspopup="true"><a href="#" class="cart-btn-menu"><i class="fa fa-shopping-cart"></i> Cart : 0</a></li>
                        </ul>

                    </nav>
                    <!--Nav-->
                </div>
            </div>
        </div>


