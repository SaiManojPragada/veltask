<div class="modal fade" id="Mymodal1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header d-flex">
                <div class="offer-code-popup">
                    <span>Membership</span>
                </div>

                <div>

                    <button type="button" class="close" data-dismiss="modal">
                        &times;
                    </button> 
                </div>

            </div> 
            <div class="modal-body">
                <div class="container p-0">
                    <div class="apply-coupons-parent p-4">
                        <div class="row">
                            <?php foreach ($memberships_list as $memb) { ?>
                                <div class="col-md-12 mb-3">
                                    <div class="view-member ">
                                        <p class="percentage-oofer"><?= $memb->title ?></p>
                                        <p class="availble-oofer-content">
                                            <?= $memb->description ?>
                                        </p>

                                        <div class="item-card2-footer d-sm-flex">
                                            <div class="item-card2-rating">
                                                <div class="rating-stars d-inline-flex">
                                                    <ul class="footer-list">
                                                        <li class="">
                                                            <div class="service-price">
                                                                <p class="mb-0">
                                                                    <span class="offer-price">₹<?= $memb->sale_price ?>
                                                                    </span> 
                                                                    <span class="mrp-price">₹<?= $memb->price ?>
                                                                    </span>
                                                                </p>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="ml-auto remove-btn">
                                                <?php if (!$has_membership) { ?>
                                                    <button class="btn btn-primary" onclick="buyThisMembership('<?= $memb->id ?>')">Buy Now</button>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <!--                            <div class="col-md-12 mb-3">
                                                            <div class="view-member ">
                                                                <p class="percentage-oofer">Plaitnum Plan (12 Months)</p>
                                                                <p class="availble-oofer-content"><i class="fa fa-check" aria-hidden="true"></i> Save Exra 10% off on all Beauty & Grooming services</p>
                                                                <p class="availble-oofer-content"><i class="fa fa-check" aria-hidden="true"></i> Save Extra ₹100 off on all Cleaning Services</p>
                                                                <p class="availble-oofer-content"><i class="fa fa-check" aria-hidden="true"></i> Save Extra ₹100 off on all Cleaning Services</p>
                            
                                                                <div class="item-card2-footer d-sm-flex">
                                                                    <div class="item-card2-rating">
                                                                        <div class="rating-stars d-inline-flex">
                                                                            <ul class="footer-list">
                                                                                <li class="">
                                                                                    <div class="service-price">
                                                                                        <p class="mb-0">
                                                                                            <span class="offer-price">₹600
                                                                                            </span> 
                                                                                            <span class="mrp-price">₹750
                                                                                            </span>
                                                                                        </p>
                                                                                    </div>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="ml-auto remove-btn">
                                                                        <button class="btn btn-primary">Add to Cart</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                            
                                                        <div class="col-md-12 mb-3">
                                                            <div class="view-member ">
                                                                <p class="percentage-oofer">Silver Plan (6 Months)</p>
                                                                <p class="availble-oofer-content"><i class="fa fa-check" aria-hidden="true"></i> Save Exra 10% off on all Beauty & Grooming services</p>
                                                                <p class="availble-oofer-content"><i class="fa fa-check" aria-hidden="true"></i> Save Extra ₹100 off on all Cleaning Services</p>
                                                                <p class="availble-oofer-content"><i class="fa fa-check" aria-hidden="true"></i> Save Extra ₹100 off on all Cleaning Services</p>
                            
                                                                <div class="item-card2-footer d-sm-flex">
                                                                    <div class="item-card2-rating">
                                                                        <div class="rating-stars d-inline-flex">
                                                                            <ul class="footer-list">
                                                                                <li class="">
                                                                                    <div class="service-price">
                                                                                        <p class="mb-0">
                                                                                            <span class="offer-price">₹600
                                                                                            </span> 
                                                                                            <span class="mrp-price">₹750
                                                                                            </span>
                                                                                        </p>
                                                                                    </div>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="ml-auto remove-btn">
                                                                        <button class="btn btn-primary">Add to Cart</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>-->

                        </div>
                    </div>
                    <!--row-->
                </div>

            </div>   

        </div>                                                                       
    </div>                                      
</div>

<?php
$session_id = $_SESSION['session_data']['session_id'];
$user_id = USER_ID;
$cart_qry = $this->db->query("select * from cart where session_id='" . $session_id . "' and user_id='" . $user_id . "'");
$cart_count = $cart_qry->num_rows();
?>

<input type="hidden" id="cart_count_hidden_input" value="<?php echo $cart_count; ?>">
<input type="hidden" id="session_id" value="<?php echo $session_id; ?>">
<input type="hidden" id="vendor_id" value="<?php echo $_SESSION['session_data']['vendor_id']; ?>">

<!--footer area start-->
<input type="hidden" id="login_quantity" >
<input type="hidden" id="login_vendor_id" >
<input type="hidden" id="login_session_id">
<input type="hidden" id="login_variant_id" >
<input type="hidden" id="login_saleprice" >
<!--Footer Section-->
<section>
    <footer class="bg-dark-purple text-white">
        <div class="footer-main">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-12">
                        <img src="<?= base_url('uploads/') . SITE_FOOTER_LOGO ?>">								
                        <p class="pt-3"> <?= SITE_ABOUT ?> </p>
                        <ul class="list-unstyled list-inline mt-3">
                            <?php if (!empty(SITE_FB_LINK)) { ?>
                                <li class="list-inline-item">
                                    <a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light" 
                                       href="<?= SITE_FB_LINK ?>" target="_blank">
                                        <i class="fab fa-facebook-f bg-facebook"></i>
                                    </a>
                                </li>
                            <?php } ?>
                            <?php if (!empty(SITE_TWT_LINK)) { ?>
                                <li class="list-inline-item">
                                    <a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light" 
                                       href="<?= SITE_TWT_LINK ?>" target="_blank">
                                        <i class="fab fa-twitter bg-info"></i>
                                    </a>
                                </li>
                            <?php } ?>
                            <?php if (!empty(SITE_GP_LINK)) { ?>
                                <li class="list-inline-item">
                                    <a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light" 
                                       href="<?= SITE_GP_LINK ?>" target="_blank">
                                        <i class="fab fa-google-plus bg-danger"></i>
                                    </a>
                                </li>
                            <?php } ?>
                            <?php if (!empty(SITE_LI_LINK)) { ?>
                                <li class="list-inline-item">
                                    <a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light" 
                                       href="<?= SITE_LI_LINK ?>" target="_blank">
                                        <i class="fab fa-linkedin-in bg-linkedin"></i>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div class="col-lg-2 col-md-12">
                        <h6>Quick Links</h6>
                        <hr class="deep-purple text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
                        <ul class="list-unstyled mb-0">
                            <li><a href="<?= base_url() ?>">Home</a></li>
                            <li><a href="<?= base_url('contact') ?>">Contact Us</a></li>
                            <li><a href="<?= base_url('about-us') ?>">About Us</a></li>

                            <li><a href="<?= base_url('blogs') ?>">Blogs</a></li>
                            <li><a href="<?= base_url('faqs') ?>">Faq's</a></li>
                        </ul>
                    </div>

                    <div class="col-lg-2 col-md-12">
                        <h6>Policy Info</h6>
                        <hr class="deep-purple text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
                        <ul class="list-unstyled mb-0">
                            <li><a href="<?= base_url() ?>privacy-policy">Privacy Policy</a></li>
                            <li><a href="<?= base_url() ?>terms-of-sale">Terms of Sale</a></li>
                            <li><a href="<?= base_url() ?>terms-of-use">Terms of Use</a></li>
                            <li><a href="<?= base_url() ?>core-values">Core Values</a></li>
                        </ul>								
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <h6>Subscribe</h6>
                        <hr class="deep-purple  text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
                        <div class="clearfix"></div>
                        <div class="input-group">
                            <input type="email" class="form-control br-tl-3  br-bl-3 " id="subscribe_email" placeholder="Email">
                            <div class="input-group-append ">
                                <button type="button" class="btn btn-primary br-tr-3  br-br-3" id="hidden-button-loader"
                                        style="z-index: 1000; position: absolute; width: 88.96px; display: none;">
                                    <img src="<?= base_url('web_assets/black-loader.gif') ?>" alt="alt" style="opacity: 1"/>
                                </button>
                                <button type="button" class="btn btn-primary br-tr-3  br-br-3" id="subscribe-email-button"> Subscribe </button>
                            </div>
                        </div>
                        <p id="subs_email_err"></p>
                        <!--                        <h6 class="mb-0 mt-5">Payments</h6>
                                                <hr class="deep-purple  text-primary accent-2 mb-2 mt-3 d-inline-block mx-auto">
                                                <div class="clearfix"></div>
                                                <ul class="footer-payments">
                                                    <li class="pl-0"><a href="javascript:;"><i class="fa fa-cc-amex text-muted" aria-hidden="true"></i></a></li>
                                                    <li><a href="javascript:;"><i class="fa fa-cc-visa text-muted" aria-hidden="true"></i></a></li>
                                                    <li><a href="javascript:;"><i class="fa fa-credit-card-alt text-muted" aria-hidden="true"></i></a></li>
                                                    <li><a href="javascript:;"><i class="fa fa-cc-mastercard text-muted" aria-hidden="true"></i></a></li>
                                                    <li><a href="javascript:;"><i class="fa fa-cc-paypal text-muted" aria-hidden="true"></i></a></li>
                                                </ul>-->
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-dark-purple text-white p-0">
            <div class="container">
                <div class="row d-flex">
                    <div class="col-lg-12 col-sm-12 mt-3 mb-3 text-center copyrights">
                        Copyright © 2021 <?= SITE_NAME ?>  
                        <span>Designed & Developed by <a href="#" class="fs-14 text-primary">Colourmoon</a>All rights reserved.</span>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</section>
<style>
    .my-custom-btn{
        background-color: #F15D74;
        color: white;
    }
    .my-custom-btn:hover{
        color: white;
        transition: .25s;
        background-color: #EC296B;
    }
</style>
<script>
    function confirmnewCart(variant_id,vendor_id,saleprice,quantity){

        var id = $(this).parents("tr").attr("id");



         swal({
                            title: "Are you sure?",
                            text: "If you proceed you current cart will be deleted.",
                            icon: "warning",
                            buttons: [
                                'No, cancel it!',
                                'Yes, I am sure!'
                            ],
                            dangerMode: true,
                        }).then(function (isConfirm) {
                            if (isConfirm) {
                               
                               $.ajax({
                    url:"<?php echo base_url(); ?>web/addtocart",
                    method:"POST",
                    data:{variant_id:variant_id,vendor_id:vendor_id,saleprice:saleprice,quantity:quantity},
                    success:function(data)
                    {
                        var str = data;
                        var res = str.split("@");
                        if(res[1]=='success')
                        {
                          $("#vendor_id").val(vendor_id);
                          $("#session_id").val(res[3]);
                              $('#cart_count').html(res[2]);  
                          
                               swal("Product added to cart!")
                        }
                        else if(res[1]=='shopclosed')
                        {
                           
                              swal("Shop Closed!")
                        }
                        else
                        {
                              swal("OUT OF STOCK!")
                        }
                            
                              
                    
                    }
                   });
                               
                            }
                        });




       /*swal({

        title: "Are you sure?",

        text: "You want to Clear the previous store items",

        type: "warning",

        showCancelButton: true,

        confirmButtonClass: "btn-danger",

        confirmButtonText: "Yes",

        cancelButtonText: "Cancel",

        closeOnConfirm: false,

        closeOnCancel: false

      },

      function(isConfirm) {

        if (isConfirm) {

                  $.ajax({
                    url:"<?php echo base_url(); ?>web/addtocart",
                    method:"POST",
                    data:{variant_id:variant_id,vendor_id:vendor_id,saleprice:saleprice,quantity:quantity},
                    success:function(data)
                    {
                        var str = data;
                        var res = str.split("@");
                        if(res[1]=='success')
                        {
                          $("#vendor_id").val(vendor_id);
                          $("#session_id").val(res[3]);
                              $('#cart_count').html(res[2]);  
                          
                               swal("Product added to cart!")
                        }
                        else if(res[1]=='shopclosed')
                        {
                           
                              swal("Shop Closed!")
                        }
                        else
                        {
                              swal("OUT OF STOCK!")
                        }
                            
                              
                    
                    }
                   });
                

        } else {

          swal("Cancelled", "", "error");

        }

      });*/

     

    }

    function addtocart(variant_id, vendor_id, saleprice, quantity)
    {
<?php if (!empty(USER_ID)) { ?>
            var session_vendor_id = $("#vendor_id").val();
            //alert(session_vendor_id);
            var session_id = $("#session_id").val();
            var user_id = '<?php echo USER_ID; ?>';

            if (user_id == '')
            {
                $("#login_quantity").val(quantity);
                $("#login_vendor_id").val(vendor_id);
                $("#login_session_id").val(session_id);

                $("#login_variant_id").val(variant_id);
                $("#login_saleprice").val(saleprice);

                $('#loginModal').modal('show');
                return false;
            } else
            {
                if (session_vendor_id != vendor_id && session_vendor_id != '')
                {
                    confirmnewCart(variant_id, vendor_id, saleprice, quantity);
                } else
                {
                    $('.error').remove();
                    var errr = 0;

                    $.ajax({
                        url: "<?php echo base_url(); ?>web/addtocart",
                        method: "POST",
                        data: {variant_id: variant_id, vendor_id: vendor_id, saleprice: saleprice, quantity: quantity, session_id: session_id},
                        success: function (data)
                        {
                            var str = data;
                            var res = str.split("@");
                            if (res[1] == 'success')
                            {
                                $("#vendor_id").val(vendor_id);
                                $("#session_id").val(res[3]);
                                $('#cart_count').html(res[2]);

                                swal("Product added to cart!")
                            } else if (res[1] == 'shopclosed')
                            {
                                swal("Shop Closed!")
                            } else
                            {
                                swal("OUT OF STOCK!")
                            }
                        }
                    });

                }
            }

<?php } else { ?>
            swal({
                title: "Please Login to Continue",
                icon: "info"
            }).then(function () {
                openNav();
            });
<?php } ?>
    }


    function scrollToFunc(divId) {
        $('html, body').animate({
            scrollTop: $(divId).offset().top - 40
        }, 1000);
    }


    var wallet_used = "No";
    var coupon_code = "<?= $coupon_and_wallet['coupon_code'] ?>";
    var clicked_wallet = false;
    function walletCheck(e) {
        if ($(e).prop('checked') == true) {
            wallet_used = "Yes";
        } else {
            wallet_used = "No";
        }
        clicked_wallet = true;
        get_coupon_wallet_discount();
    }

    function applyCoupon(code) {
        coupon_code = code;
        get_coupon_wallet_discount();
    }

    function removeCoupon() {
        coupon_code = "";
        get_coupon_wallet_discount();
    }

    function get_coupon_wallet_discount(ignore_alert = false) {
        $.ajax({
            url: "<?= base_url('api/cart/apply_coupon_and_wallet') ?>",
            type: "post",
            data: {user_id: "<?= USER_ID ?>", coupon_code: coupon_code, use_wallet: wallet_used},
            success: function (resp) {
                resp = JSON.parse(resp);
                console.log(resp);
                if (resp['status']) {
                    if (resp['message'].length > 1 && !clicked_wallet && !ignore_alert) {
                        swal({
                            title: resp['message'],
                            icon: 'success'
                        }).then(function () {
                            $("#Mymodal").modal("hide");
                        });
                    }
                    clicked_wallet = false;
                    $.ajax({
                        url: '<?= base_url('api/web/apply_coupon') ?>',
                        type: 'post',
                        data: resp['data']
                    });
                    handle_coupon_wallet_resp(resp['data']);
                } else {
                    swal({
                        title: resp['message'],
                        icon: 'info'
                    });
                }
            }
        });
    }

    var min_cart_value = <?= ($min_cart_value > 0) ? $min_cart_value : 0 ?>;
    function handle_coupon_wallet_resp(data) {
        console.log(data);
        if (data['grand_total'] > min_cart_value) {
            is_checkout_enabled = 1;
        } else {
            is_checkout_enabled = 0;
        }
        if (data['wallet_applied_amount'] > 0) {
            $("#used_wallet_amount_div").css({"display": "block"});
            $("#used_wallet_amount").html(data['wallet_applied_amount']);
            $("#current_wallet_amount").html(data['balance_wallet_amount']);
        } else {
            $("#used_wallet_amount_div").attr("style", "display: none !important");
            $("#used_wallet_amount").html("");
            $("#current_wallet_amount").html(data['balance_wallet_amount']);
        }

        if (data['coupon_code'] !== "") {
            $("#applied_coupon_div").css({"display": "block"});
            $("#coupons_view_div").attr("style", "display: none !important");
            $("#coupon_discount_div").css({"display": "block"});
            $("#coupon_amount").html(data['coupon_discount']);
        } else {
            $("#applied_coupon_div").attr("style", "display: none !important");
            $("#coupons_view_div").css({"display": "block"});
            $("#coupon_discount_div").attr("style", "display: none !important");
            $("#coupon_amount").html(data['coupon_discount']);
        }
        $("#cart_grand_total").html(data['grand_total']);
        $("#coupon_amount").html(data['coupon_discount']);
        $("#applied_coupon_code").html(data['coupon_code']);
        $("#sub_total").html(data['sub_total']);
        $("#visiting_charges").html(data['visiting_charges']);
        $("#tax").html(data['tax']);
        $("#business_discount").html(data['business_discount']);
        $("#membership_discount").html(data['membership_discount']);
    }



    function buyThisMembership(id) {
        $.ajax({
            url: '<?= base_url() ?>api/web/make_subscription',
            type: 'post',
            data: {id: id},
            success: function (resp) {
                if (resp) {
                    location.href = "<?= base_url('buy-membership'); ?>";
                }
                return true;
            }
        });
    }






    document.getElementById("otp-verification").style.display = "none";
    var user_id = '';
    var phone = '';
    var existing_user = "";
    function myFunction() {
        phone = document.getElementById("login_phone_number").value;
        $("#user_phone").val(phone);
        if (phone.length !== 10) {
            $('#phone-err').html('Invalid Phone Number');
            return false;
        } else {
            $('#phone-err').html('');
        }
        $.ajax({
            url: "<?= base_url('api/authentication/user_login') ?>",
            type: "post",
            data: {phone: phone, login_type: 'web'},
            success: function (resp) {
                resp = JSON.parse(resp);
                console.log(resp);
                if (resp['status']) {
                    existing_user = resp['data']['existing_user'];
                    user_id = resp['data']['user_id'];
                    $('#otp-sent-message').html(resp['message']);
                    var x = document.getElementById("otp-verification");
                    if (x.style.display === "none") {
                        $('#phone-number-after-otp').html(" " + resp['data']['phone'] + " ");
                        x.style.display = "block";
                        $("#otp-digit-1").focus();
                        document.getElementsByClassName('login-with-number-form')[0].style.display = "none";
                    } else {
                        x.style.display = "none";
                        document.getElementsByClassName('login-with-number-form')[0].style.display = "block";
                    }
                } else {
                    swal({
                        title: resp['message'],
                        icon: 'warning',
                        button: "close"
                    }
                    );
                    return false;
                }
            }
        });
    }
<?php if ($this->session->userdata("login_check") == 'Yes') { ?>
        swal({
            title: "Please login to continue",
            icon: 'warning',
            button: "close"
        }).then(function () {
            location.href = "<?= base_url() ?>";
        });
<?php } $this->session->unset_userdata("login_check"); ?>
    function verify_otp() {
        var otp = $("#otp-digit-1").val().toString() + $("#otp-digit-2").val().toString() + $("#otp-digit-3").val().toString() + $("#otp-digit-4").val().toString();
        if (otp.length !== 4) {
            $('#otp-err').html("Enter Valid OTP");
            $('#otp-err').css('color', 'tomato');
        } else {
            $.ajax({
                url: '<?= base_url('api/web/verify_otp') ?>',
                type: 'post',
                data: {user_id: user_id, otp: otp},
                success: function (resp) {
                    resp = JSON.parse(resp);
                    if (resp['status']) {
                        if (existing_user == 'No') {
                            var x = document.getElementById("otp-verification");
                            x.style.display = "none";
                            $("#login-side-heading").html('Complete your Profile');
                            $("#user-profile").css({'display': 'block'});
                        } else {
                            location.href = '';
                        }
                    } else {
                        $('#otp-err').html(" " + resp['message'] + " ");
                        $('#otp-err').css('color', 'tomato');
                    }
                }
            });
        }
    }

    function apply_referall() {
        $(".ref_err").remove();
        var referral_code = $("#referal_code_int").val();
        $.ajax({
            url: "<?= base_url('api/referrals/use_referral') ?>",
            type: "post",
            data: {user_id: user_id, referral_code: referral_code},
            success: function (resp) {
                resp = JSON.parse(resp);
                if (resp['status']) {
                    skipptoprofile();
                } else {
                    $("#referal_code_int").parent().after("<p style='color: tomato' class='ref_err'> " + resp['message'] + "</p>");
                }
            }
        });
    }

    function skipptoprofile() {
        $("#user-referal").css({'display': 'none'});
        $("#login-side-heading").html('Complete Your Profile');
        $("#user-profile").css({'display': 'block'});
    }
    function resend_otp() {
        $.ajax({
            url: '<?= base_url('api/web/resend_otp') ?>',
            type: 'post',
            data: {user_id: user_id},
            success: function (resp) {
                resp = JSON.parse(resp);
                $('#otp-err').html("" + resp['message'] + "");
                if (resp['status']) {
                    $('#otp-err').css('color', 'green');
                } else {
                    $('#otp-err').css('color', 'tomato');
                }
            }
        });
    }
    function go_back_to_login() {
        //        document.getElementById("login_phone_number").value = "";
        $("#otp-digit-1").val("");
        $("#otp-digit-2").val("");
        $("#otp-digit-3").val("");
        $("#otp-digit-4").val("");
        document.getElementById("otp-verification").style.display = "none";
        document.getElementsByClassName('login-with-number-form')[0].style.display = "block";
    }

    $('#subscribe-email-button').click(function () {
        var email = $('#subscribe_email').val();
        if (validateEmail(email) && email.length > 3) {
            document.getElementById('hidden-button-loader').style.display = "block";
            $('#subs_email_err').html('');
            $.ajax({
                url: '<?= base_url("api/web/subscribe_user") ?>',
                type: 'post',
                data: {email: email},
                success: function (response) {
                    document.getElementById('hidden-button-loader').style.display = "none";
                    var json = JSON.parse(response);
                    if (json['status']) {
                        swal({
                            title: json['message'],
                            icon: 'success'
                        });
                        $('#subscribe_email').val('');
                    } else {
                        swal({
                            title: json['message'],
                            icon: 'info'
                        });
                    }
                }
            });
        } else {
            $('#subs_email_err').html('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Valid Email</span>');
            $('#subs_email_err').focus();
        }
    });
    function validateEmail($email)
    {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if (!emailReg.test($email)) {
            return false;
        } else
        {
            return true;
        }
    }

</script>
<!--Footer Section-->
<!-- Back to top -->
<a href="#top" id="back-to-top" ><i class="fa fa-arrow-up"></i></a>
<!-- JQuery js-->
<!-- JQuery js-->

<!-- Bootstrap js -->
<script src="<?= base_url('web_assets/') ?>assets/plugins/bootstrap-4.3.1-dist/js/popper.min.js"></script>
<script src="<?= base_url('web_assets/') ?>assets/plugins/bootstrap-4.3.1-dist/js/bootstrap-rtl.js"></script>

<!--JQuery Sparkline Js-->
<script src="<?= base_url('web_assets/') ?>assets/js/vendors/jquery.sparkline.min.js"></script>

<!-- Circle Progress Js-->
<script src="<?= base_url('web_assets/') ?>assets/js/vendors/circle-progress.min.js"></script>

<!-- Star Rating Js-->
<script src="<?= base_url('web_assets/') ?>assets/plugins/rating/jquery.rating-stars.js"></script>

<!--Counters -->
<script src="<?= base_url('web_assets/') ?>assets/plugins/counters/counterup.min.js"></script>
<script src="<?= base_url('web_assets/') ?>assets/plugins/counters/waypoints.min.js"></script>
<script src="<?= base_url('web_assets/') ?>assets/plugins/counters/numeric-counter.js"></script>

<!--Owl Carousel js -->
<script src="<?= base_url('web_assets/') ?>assets/plugins/owl-carousel/owl.carousel.js"></script>

<!--Horizontal Menu-->
<script src="<?= base_url('web_assets/') ?>assets/plugins/Horizontal2/Horizontal-menu/horizontal.js"></script>

<!--JQuery TouchSwipe js-->
<script src="<?= base_url('web_assets/') ?>assets/js/jquery.touchSwipe.min.js"></script>

<!--Select2 js -->
<script src="<?= base_url('web_assets/') ?>assets/plugins/select2/select2.full.min.js"></script>
<script src="<?= base_url('web_assets/') ?>assets/js/select2.js"></script>

<!-- sticky Js-->
<script src="<?= base_url('web_assets/') ?>assets/js/sticky.js"></script>

<!-- Cookie js -->
<script src="<?= base_url('web_assets/') ?>assets/plugins/cookie/jquery.ihavecookies.js"></script>
<script src="<?= base_url('web_assets/') ?>assets/plugins/cookie/cookie.js"></script>

<!-- Custom scroll bar Js-->
<script src="<?= base_url('web_assets/') ?>assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- Swipe Js-->
<script src="<?= base_url('web_assets/') ?>assets/js/swipe.js"></script>

<!-- Scripts Js-->
<script src="<?= base_url('web_assets/') ?>assets/js/scripts2-rtl.js"></script>



<!-- Custom Js-->
<script src="<?= base_url('web_assets/') ?>assets/js/custom.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<script>
    function validateAndSubmitProfile() {
        var user_name = $("#user_name").val();
        var user_email = $("#user_email").val();
//        var user_phone = $("#user_phone").val();
//        var user_state = $("#user_state").val();
//        var user_city = $("#user_city").val();
//        console.log(user_city);
//        var user_pincode = $("#user_pincode").val();
        var err = "<p style='color: tomato' class='user_pro_err'>";
        $(".user_pro_err").remove();
        var no_err = true;
        if (user_name.length < 1) {
            $("#user_name").after(err + "Enter Valid Name" + "</p>");
            no_err = false;
        }
        var email_chck = checkEmail(user_email);
        if (user_email.length < 1) {
            $("#user_email").after(err + "Enter Valid Email" + "</p>");
            no_err = false;
        }

        var referral_code = $("#referal_code_int").val();



//        if (user_state == null) {
//            $("#user_state").after(err + "Select a State" + "</p>");
//            no_err = false;
//        } else if (user_state == "") {
//            $("#user_state").after(err + "Select a State" + "</p>");
//            no_err = false;
//        }
//
//        if (user_city == null) {
//            $("#user_city").after(err + "Select a city" + "</p>");
//            no_err = false;
//        } else if (user_city == "") {
//            $("#user_city").after(err + "Select a State" + "</p>");
//            no_err = false;
//        }
//
//        if (user_pincode.length < 1) {
//            $("#user_pincode").after(err + "Enter Valid Pincode" + "</p>");
//            no_err = false;
//        }
        $('#user-profile-form').parsley().validate();
        if ($('#user-profile-form').parsley().isValid() && no_err) {
            $.ajax({
                url: "<?= base_url('api/authentication/profile_and_referral') ?>",
                type: "post",
//                data: {user_id: user_id, name: user_name, email: user_email, city: user_city, state: user_state, zip_code: user_pincode},
                data: {user_id: user_id, name: user_name, email: user_email, referral_code: referral_code},

                success: function (resp) {
                    resp = JSON.parse(resp);
                    if (resp['status']) {
                        existing_user = "Yes";
                        verify_otp();
                    } else {
                        swal({
                            title: resp['message'],
                            message: "warning"
                        });
                    }
                }
            });
        }

        function checkEmail(email) {
            return true;
        }


    }
</script>
<style>
    .parsley-errors-list{
        list-style:none;
    }
    .parsley-required , .parsley-errors-list li{
        color:red;
        list-style:none;
    }
    .alert *{
        color: white !important;
    }
</style>
<style>

    /* width */
    ::-webkit-scrollbar {
        width: 6px;
    }

    /* Track */
    ::-webkit-scrollbar-track {
        background: #F1EEF7;
    }

    /* Handle */
    ::-webkit-scrollbar-thumb {
        background: #F15F74;
    }

    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
        background: #E4135A;
    }

</style>

<script src="https://cdn.ckeditor.com/4.17.1/standard/ckeditor.js"></script>
<script>
    $(document).ready(function () {
        var ck_editors = document.getElementsByClassName("ck-editor");
        for (var i = 0; i < ck_editors.length; i++) {
            CKEDITOR.replace(ck_editors[i]);
        }
    });
</script>



</body>
</html>