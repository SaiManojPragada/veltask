<!doctype html>

<html class="no-js" lang="en">

    <head>
        <!-- META DATA -->
        <meta charset="UTF-8">
        <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="keywords" content="">		
        <link rel="icon" href="favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" type="image/x-icon" href="<?= base_url('uploads/') . SITE_FAV_ICON ?>" />
        <!-- Title -->
        <title><?= SITE_NAME ?></title>
        <!-- Bootstrap Css -->
        <link href="<?= base_url('web_assets/') ?>assets/plugins/bootstrap-4.3.1-dist/css/bootstrap.min.css" rel="stylesheet" />

        <!-- Dashboard Css -->
        <link href="<?= base_url('web_assets/') ?>assets/css/dashboard.css" rel="stylesheet" />

        <!-- RTL Css -->
        <link href="<?= base_url('web_assets/') ?>assets/css/rtl.css" rel="stylesheet" />

        <!-- Font-awesome  Css -->
        <link href="<?= base_url('web_assets/') ?>assets/css/icons.css" rel="stylesheet"/>
        <link rel="stylesheet" href="<?= base_url('web_assets/') ?>assets/css/blog.css">

        <!--Horizontal Menu-->
        <link href="<?= base_url('web_assets/') ?>assets/plugins/Horizontal2/Horizontal-menu/dropdown-effects/fade-down.css" rel="stylesheet" />
        <link href="<?= base_url('web_assets/') ?>assets/plugins/Horizontal2/Horizontal-menu/horizontal.css" rel="stylesheet" />

        <!--Select2 Plugin -->
        <link href="<?= base_url('web_assets/') ?>assets/plugins/select2/select2.min.css" rel="stylesheet" />

        <!-- Cookie css -->
        <link href="<?= base_url('web_assets/') ?>assets/plugins/cookie/cookie.css" rel="stylesheet">

        <!-- Owl Theme css-->
        <link href="<?= base_url('web_assets/') ?>assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet" />

        <!-- Custom scroll bar css-->
        <link href="<?= base_url('web_assets/') ?>assets/plugins/scroll-bar/jquery.mCustomScrollbar.css" rel="stylesheet" />

        <!-- COLOR-SKINS -->
        <link id="theme" rel="stylesheet" type="text/css" media="all" href=".<?= base_url('web_assets/') ?>/assets/webslidemenu/color-skins/color10.css" />
        <script src="<?= base_url('web_assets/') ?>assets/js/vendors/jquery-3.2.1.min.js"></script>
        <link rel="stylesheet" href="<?= base_url('web_assets/') ?>assets/css/time-date.css">
        <link rel="stylesheet" href="<?= base_url('web_assets/') ?>assets/css/add-button-aanimation.css">
        <link rel="stylesheet" href="<?= base_url('web_assets/') ?>assets/css/my-booking.css">
        <link rel="stylesheet" href="<?= base_url('web_assets/') ?>assets/css/my-dashbaord.css">
        <link rel="stylesheet" href="<?= base_url('web_assets/') ?>assets/css/f5.css">
        <link rel="stylesheet" href="<?= base_url('web_assets/') ?>assets/css/inner-pages.css">
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="<?= base_url('web_assets/') ?>/js/plugins/parsleyjs/dist/parsley.min.js"></script>
    </head>
    <body>
        <script>
           

            function my_reload() {
                location.href = '';
            }


        </script>
        <script>

            function getCities(val) {
                $.ajax({
                    url: "<?= base_url('api/admin_ajax/locations/get_cities') ?>",
                    type: "post",
                    data: {state_id: val},
                    success: function (resp) {
                        $("#user_city").html(resp);
                    }
                });
            }

            function go_to_next(val) {
                var inp = $('#otp-digit-' + val).val();
                (inp.length == 1) ? $('#otp-digit-' + (val + 1)).focus() : $('#otp-digit-' + (val - 1)).focus();
                //Explanation
//                if (inp.length == 1) {
//                    val = val + 1;
//                    $('#otp-digit-' + val).focus();
//                } else if (inp.length == 0) {
//                    val = val - 1;
//                    $('#otp-digit-' + val).focus();
//                }
            }
        </script>


        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDiiWRvGqRwOJOVqYc8GEQhBuWj4mgSSTM&libraries=places&callback=initMap&sensor=false" async defer></script>

        <?php if (!empty($this->session->userdata('USER_CURRENT_LOCATION_LNG'))) { ?>
            <?php
            $scs = $this->session->flashdata('success');
            if (!empty($scs)) {
                ?>
                <div class="alert alert-success popup-alert" role="alert">
                    <?= $scs ?><span style="float: right; font-weight: bold; cursor: pointer" onclick="$('.alert').css('display', 'none');">&times;</span>
                </div>
                <?php
            } $err = $this->session->flashdata('error');
            if (!empty($err)) {
                ?>
                <div class="alert alert-warning popup-alert" style="color: #ffffff" role="alert">
                    <?= $err ?><span style="float: right; font-weight: bold; cursor: pointer" onclick="$('.alert').css('display', 'none');">&times;</span>
                </div>
            <?php } ?>

            <style>
                .alert{
                    position: absolute;
                    margin-top: 200px;
                    z-index: 2000000;
                    width: 25%;
                    color: green;
                    right: 30px;
                }
            </style>
            <script>
                setTimeout(function () {
                    $(".popup-alert").fadeOut();
                }, 2500)
            </script>
            <!-- Login box -->
            <div class="new-login-box" id="login-slide">
                <div class="login-close">
                    <i class="fa fa-times" onclick="closeNav()"></i>&nbsp;&nbsp;&nbsp;<span id="login-side-heading">Please Login to Continue</span>
                </div>
                <div class="login-content login-with-number-form" id="login-number">
                    <div class="input-group mb-3 mt-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">+91</span>
                        </div>
                        <input type="text" class="form-control login-number-filed" id="login_phone_number" onkeydown='return (event.which >= 48 && event.which <= 57) || event.which == 8 || event.which == 46 || (event.which >= 96 && event.which <= 105)' placeholder="Enter Your Number" aria-describedby="basic-addon1">

                    </div>
                    <p style="margin-top: 8px; color: tomato; text-align: left" id="phone-err"></p>
                    <div class="mt-1" onclick="myFunction()">
                        <a href="#" class="btn btn-primary btn-pill">Continue</a>
                    </div>
                </div>

                <div class="login-content hide-otp" id="otp-verification">
                    <div class="otp-verify mb-3 mt-3 text-center">
                        <h2>Enter verification code</h2>
                        <div>
                            <p>We have sent you a 4 digit OTP</p>
                            <p>on <span id="phone-number-after-otp"></span><span><a href="#" onclick="go_back_to_login()">Edit</a></span></p>
                        </div>
                        <div class="mt-4 mb-4">
                            <input type="text" value="" onkeyup="go_to_next(1)" onkeydown='return (event.which >= 48 && event.which <= 57) || event.which == 8 || event.which == 46 || event.which == 9 || (event.which >= 96 && event.which <= 105)' id="otp-digit-1" maxlength="1">
                            <input type="text" value="" onkeyup="go_to_next(2)" onkeydown='return (event.which >= 48 && event.which <= 57) || event.which == 8 || event.which == 46 || event.which == 9 || (event.which >= 96 && event.which <= 105)' id="otp-digit-2" maxlength="1">
                            <input type="text" value="" onkeyup="go_to_next(3)" onkeydown='return (event.which >= 48 && event.which <= 57) || event.which == 8 || event.which == 46 || event.which == 9 || (event.which >= 96 && event.which <= 105)' id="otp-digit-3" maxlength="1">
                            <input type="text" value="" onkeyup="go_to_next(4)" onkeydown='return (event.which >= 48 && event.which <= 57) || event.which == 8 || event.which == 46 || event.which == 9 || (event.which >= 96 && event.which <= 105)' id="otp-digit-4" maxlength="1">
                        </div>

                        <p id="otp-err" style="text-align: left; margin-top: 3px; margin-bottom: 3px;"></p>
                        <div class="mt-4 mb-4">
                            <a href="javascript:void(0);" onclick="resend_otp()">Resend OTP</a>
                        </div>
                    </div>
                    <div class="mt-1"><a href="javascript:void(0);" class="btn btn-primary btn-pill" onclick="verify_otp()">Continue</a> </div>
                </div>
                <!--            <div class="login-content login-with-number-form" id="user-referal" style="display: none;">
                                <div class="input-group" style="margin: 5px 2px 10px 2px; width: 100%">
                                    <h4 style="float: left">Complete your Profile</h4>
                                    <a href="javascript:void(0);" style="right: 10px; position: absolute; color: #F15F74" onclick="skipptoprofile()">skip <i class="fa fa-angle-right"></i></a>
                                </div>
                                <div class="input-group" style="margin-top: 30px; margin-bottom: 30px">
                                    <input type="text" class="form-control login-number-filed" id="referal_code_int" placeholder="Enter Your Referral Code" aria-describedby="basic-addon1" requried>
                                </div>
                                <div class="mt-1" onclick="apply_referall()">
                                    <a href="#" class="btn btn-primary btn-pill">Continue</a>
                                </div>
                            </div>-->
                <div class="login-content login-with-number-form" id="user-profile" style="display: none;">
                    <form method="post" action="javascript:void(0);" onsubmit="javascript:void(0);" id="user-profile-form" style="text-align: left">
                        <div class="input-group" style="margin: 5px 2px 10px 2px; width: 100%">
                            <!--<h4 style="float: left">Complete your Profile</h4>-->
                            <a href="<?= base_url() ?>" style="right: 10px; position: absolute; color: #F15F74">skip <i class="fa fa-angle-right"></i></a>
                        </div>
                        <div class="form-group mb-3 mt-3">
                            <label class="control-label">Name</label>
                            <input type="text" onkeydown="return (event.which >= 65 && event.which <= 120) || event.which == 32 || event.which == 8;" class="form-control login-number-filed" id="user_name" placeholder="Enter Your Name" aria-describedby="basic-addon1" requried>
                        </div>
                        <div class="form-group mb-3 mt-3">
                            <label class="control-label">Email</label>
                            <input type="email" class="form-control login-number-filed" id="user_email" placeholder="Enter Your Email" aria-describedby="basic-addon1" requried data-parsley-type-message="Please Enter Valid Email" data-parsley-required-message="Please Enter Valid Email" >
                        </div>
                        <div class="form-group mb-3 mt-3">
                            <label class="control-label">Mobile Number</label>
                            <input type="text" disabled class="form-control login-number-filed" id="user_phone" placeholder="Enter Your Mobile Number" aria-describedby="basic-addon1">
                        </div>
                        <div class="input-group" style="margin-top: 30px; margin-bottom: 30px">
                            <input type="text" class="form-control login-number-filed" id="referal_code_int" placeholder="Enter Your Referral Code" aria-describedby="basic-addon1">
                        </div>
                       
                        <div class="mt-1" onclick="validateAndSubmitProfile()">
                            <button class="btn btn-primary btn-pill" type="submit">Continue</button>
                        </div>
                    </form>
                </div>

            </div>
            <!-- Ends Here -->

          




            <!--Topbar-->
            <div class="header-main">
                <div class="top-bar">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-6 col-lg-6 col-sm-6 col-6">
                                <div class="top-bar-left d-flex">
                                    <div class="clearfix">
                                        <ul class="socials">
                                            <?php if (!empty(SITE_FB_LINK)) { ?>
                                                <li>
                                                    <a class="social-icon text-dark" href="<?= SITE_FB_LINK ?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                                </li>
                                            <?php } ?>
                                            <?php if (!empty(SITE_TWT_LINK)) { ?>
                                                <li>
                                                    <a class="social-icon text-dark" href="<?= SITE_TWT_LINK ?>" target="_blank"><i class="fab fa-twitter"></i></a>
                                                </li>
                                            <?php } ?>
                                            <?php if (!empty(SITE_LI_LINK)) { ?>
                                                <li>
                                                    <a class="social-icon text-dark" href="<?= SITE_LI_LINK ?>" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                                                </li>	
                                            <?php } ?>									
                                        </ul>
                                    </div>
                                    <div class="clearfix">
                                        <ul class="contact border-left">

                                            <!--                                        <li class="select-country">
                                                                                        <i class="far fa-map-marker-alt"></i><select class="form-control" data-placeholder="Select City">
                                                                                            <option value="UM">Visakhapatnam</option>
                                                                                            <option value="AF">Hyderabad</option>
                                                                                        </select>
                                                                                    </li>										-->

                                            <li class="select-country" style="height: 50px; padding-top: 10px;min-width: 210px">
                                                <a href="#" class="text-dark" onclick="openregister()">
                                                    <i class="far fa-map-marker-alt mr-1" style="position: absolute; top: 10px; "></i>
                                                    <p id="header-location-disp" style="height: 22px; width: 210px;overflow: hidden; margin-left: 10px">&nbsp;&nbsp;Location</p>
                                                </a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-sm-6 col-6">
                                <div class="top-bar-right">
                                    <ul class="custom">
                                        <li><a href="<?= base_url('business-registration') ?>" target="_blank" class="text-dark"><i class="far fa-store"></i> <span>Franchise</span></a></li>
                                        <li><a href="<?= base_url('business-registration') ?>?reg=sp" target="_blank" class="text-dark"><i class="far fa-users"></i> <span>Service Provider</span></a></li>
                                        <!--                                    <li>
                                                                                <a href="#" class="text-dark" onclick="openregister()"><i class="fa fa-user mr-1"></i> <span>Register</span></a>
                                                                            </li>-->
                                        <?php if (!empty(USER_ID)) { ?>
                                            <li>
                                                <a href="javascript:void(0);" class="text-dark dropdown-toggle" data-toggle="dropdown" data-target="user-menu-login"><i class="fa fa-user"></i> <span>Hi, <?= (strlen(USER_NAME) > 1) ? ucfirst(USER_NAME) : "User" ?></span></a>
                                                <div class="dropdown-menu" id="user-menu-login">
                                                    <a class="dropdown-item" href="<?= base_url('user-dashboard') ?>"><i class="fa fa-tachometer"></i>&nbsp;&nbsp;&nbsp;&nbsp;<span>My Dashboard</span></a>
                                                    <a class="dropdown-item" href="<?= base_url('website/logout') ?>"><i class="fa fa-sign-in mr-1"></i>&nbsp;&nbsp;&nbsp;&nbsp;<span>Logout</span></a>
                                                </div>
                                            </li>
                                        <?php } else { ?>
                                            <li>
                                                <a href="#" class="text-dark" onclick="openNav()"><i class="fa fa-sign-in mr-1"></i> <span>Login / Register</span></a>
                                            </li>
                                        <?php } ?>
                                        <!-- <li class="dropdown">
                                                <a href="my-dashboard.php" class="text-dark" data-toggle="dropdown"><i class="fa fa-tachometer"></i><span> My Dashboard</span></a>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                        <a href="my-dashboard.php" class="dropdown-item">
                                                                <i class="dropdown-icon icon icon-user"></i> My Profile
                                                        </a>
                                                        <a class="dropdown-item" href="my-booking.php">
                                                                <i class="dropdown-icon icon icon-doc"></i> My Booking
                                                        </a>
                                                        <a class="dropdown-item" href="#">
                                                                <i class="dropdown-icon icon icon-basket"></i> My Orders
                                                        </a>
                                                        <a class="dropdown-item" href="notifications.php">
                                                                <i class="dropdown-icon icon icon-bell"></i> Notifications
                                                        </a>
                                                        <a href="my-dashboard.php" class="dropdown-item">
                                                                <i class="dropdown-icon  icon icon-settings"></i> Account Settings
                                                        </a>
                                                        <a class="dropdown-item" href="#">
                                                                <i class="dropdown-icon icon icon-power"></i> Log out
                                                        </a>
                                                </div>
                                        </li> -->
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Mobile Header -->
                <div class="horizontal-header clearfix ">
                    <div class="container">
                        <a id="horizontal-navtoggle" class="animated-arrow"><span></span></a>
                        <a href="<?= base_url() ?>"><span class="smllogo"><img src="<?= base_url('uploads/') . SITE_LOGO ?>" width="150" alt=""/></span></a>
                        <a href="tel:245-6325-3256" class="callusbtn"><i class="fa fa-phone" aria-hidden="true"></i></a>
                    </div>
                </div>
                <!-- /Mobile Header -->

                <div class="horizontal-main bg-dark-transparent clearfix">
                    <div class="horizontal-mainwrapper container clearfix">
                        <div class="desktoplogo">
                            <a href="<?= base_url() ?>"><img src="<?= base_url('uploads/') . SITE_FOOTER_LOGO ?>" alt=""></a>
                        </div>
                        <div class="desktoplogo-1">
                            <a href="<?= base_url() ?>"><img src="<?= base_url('uploads/') . SITE_LOGO ?>" alt=""></a>
                        </div>
                        <!--Nav-->
                        <nav class="horizontalMenu clearfix d-md-flex">
                            <ul class="horizontalMenu-list">
                               <li aria-haspopup="true"><a href="#">Shopping <span class="fa fa-caret-down m-0"></span></a>
                                    <div class="horizontal-megamenu clearfix">
                                        <div class="container">
                                            <div class="megamenu-content">
                                                <div class="row">
                                                        <?php
                                                     $qry = $this->db->query("select * from categories where status=1 order by priority asc");
                                                        $dat = $qry->result();
                                                        if($qry->num_rows()>0)
                                                        {
                                                                $ar=[];
                                                                foreach ($dat as $value) 
                                                                {?>
                                                        <ul class="col link-list">
                                                            <li class="title"><?= $value->category_name ?></li>
                                                                <?php 
                                                                 $subqry = $this->db->query("select * from sub_categories where cat_id='".$value->id."' order by id asc");
                                                                    $subresult = $subqry->result();
                                                                foreach ($subresult as $sub_cat) { ?>
                                                                <li><a href="<?= base_url('web/store_wise_categories/') . $value->seo_url ?>/<?php echo $sub_cat->seo_url; ?>"><?= ucfirst($sub_cat->sub_category_name) ?></a></li>
                                                                <?php }  ?>

                                                            
                                                        </ul>

                                                    <?php      }  
                                                                
                                                        }
                                                         ?>


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                
                                <!-- <?php foreach ($ecom_categories as $index => $cat) { ?>
                                    <?php if ($index < 5) { ?>
                                        <li aria-haspopup="true"><a href="<?= base_url('web/store_categories/') . $cat->seo_url ?>"><?= $cat->category_name ?> </a></li>
                                    <?php } ?>
                                <?php } ?> -->
                                <!-- <li aria-haspopup="true" onclick="scrollToFunc('#ecommSection');"><a href="javascript:void(0);">More</a></li> -->
                                <!--                                <li aria-haspopup="true"><a href="women-services.php">Grocery </a></li>
                                                                <li aria-haspopup="true"><a href="women-services.php">Meat </a></li>
                                                                <li aria-haspopup="true"><a href="women-services.php">Food</a></li>
                                                                <li aria-haspopup="true"><a href="women-services.php">Electronics</a></li>
                                                                <li aria-haspopup="true"><a href="women-services.php">Retails & Cloth</a></li>
                                                                <li aria-haspopup="true"><a href="women-services.php">More</a></li>-->
                                <li aria-haspopup="true"><a href="<?= base_url() ?>web/checkout" class="my-custom-btn" style="color: white !important;">
                                        <i class="fa fa-shopping-cart" style="color: white !important;"></i>
                                        <?php
                                    $session_id = $_SESSION['session_data']['session_id'];
                                    $user_id= USER_ID;

                                    $cart_qry = $this->db->query("select * from cart where session_id='".$session_id."' and user_id='".$user_id."'");
                                    $cart_count = $cart_qry->num_rows();
                                    ?>

                                        Cart : <n id="cart_count"><?php echo $cart_count;?></n></a></li>
                                <br>
                            </ul>

                        </nav>
                        <!--Nav-->
                    </div>
                </div>
            </div>

        <?php } else { ?>
            <div class="horizontalMenucontainer">
                <div class="header-main">
                    <div class="horizontal-main bg-dark-transparent clearfix">
                        <div class="horizontal-mainwrapper container clearfix">
                            <div class="desktoplogo">
                                <a href="javascript:void(0);"><img src="<?= base_url('uploads/') . SITE_FOOTER_LOGO ?>" alt=""><span  style="color: white; position: absolute; top: 30px; padding-left: 30px" onclick="getCurrentLocation();"><i class="far fa-map-marker-alt mr-1" style="color: white;"></i>/</span></a>
                            </div>
                            <div class="desktoplogo-1">
                                <a href="<?= base_url() ?>"><img src="<?= base_url('uploads/') . SITE_LOGO ?>" alt=""></a>

                            </div>
                            <!--Nav-->
                            <nav class="horizontalMenu clearfix d-md-flex">
                                <ul class="horizontalMenu-list">
                                    </li>
                                    <?php if (!empty(SITE_FB_LINK)) { ?>
                                        <li>
                                            <a class="social-icon text-dark" href="<?= SITE_FB_LINK ?>" target="_blank"><i class="fab fa-facebook-f" style="color: white;"></i></a>
                                        </li>
                                    <?php } ?>
                                    <?php if (!empty(SITE_TWT_LINK)) { ?>
                                        <li>
                                            <a class="social-icon text-dark" href="<?= SITE_TWT_LINK ?>" target="_blank"><i class="fab fa-twitter" style="color: white;"></i></a>
                                        </li>
                                    <?php } ?>
                                    <?php if (!empty(SITE_LI_LINK)) { ?>
                                        <li>
                                            <a class="social-icon text-dark" href="<?= SITE_LI_LINK ?>" target="_blank"><i class="fab fa-linkedin-in" style="color: white;"></i></a>
                                        </li>	
                                    <?php } ?>
                                    <br>
                                </ul>

                            </nav>
                            <!--Nav-->
                        </div>
                    </div>
                </div>
            </div>
            <img src="<?= base_url('web_assets/landing.jpg') ?>" style="width: 100%"/>
            <?php
            die();
        }
        ?>
        <script>
            $(document).ready(function () {
                $('#user-profile-form').parsley();
            });
        </script>


 <?php
    $session_id = $_SESSION['session_data']['session_id'];
    $user_id= USER_ID;
    $cart_qry = $this->db->query("select * from cart where session_id='".$session_id."' and user_id='".$user_id."'");
       $cart_count = $cart_qry->num_rows();    
 ?>

<input type="hidden" id="cart_count_hidden_input" value="<?php echo $cart_count; ?>">
<input type="hidden" id="session_id" value="<?php echo $session_id; ?>">
<input type="hidden" id="vendor_id" value="<?php echo $_SESSION['session_data']['vendor_id']; ?>">