<style>
    h2.contact-informaion {
        font-size: 25px;
        margin-bottom: 10px;
        font-weight: 500;
    }
    .media i {
        width: 60px;
        height: 60px;
        line-height: 60px;
        background: #f15d74;
        color: #ffffff;
        border-radius: 50%;
        margin: 10px 0px;
        font-size: 30px;
        text-align: center;
    }
    .padd-contact-top {
        margin-top: 20px;
        margin-left: 20px;
    }
    .login-forms input {
        border: 1px solid #e4e4e4;
        padding: 13px 14px;
        font-size: 16px;
        width: 100%;
        box-sizing: border-box;
        margin-top: 5px;
        border-radius: 5px;
        color: #333333;
    }
    .login-forms textarea {
        border: 1px solid #e4e4e4;
        padding: 13px 14px;
        font-size: 16px;
        width: 100%;
        box-sizing: border-box;
        margin-top: 5px;
        border-radius: 5px;
        color: #333333;
        resize: none;
    }

    .white-block h2 {
        font-size: 22px;
    }
    button.submit-btn {
        background: #f15d74;
        color: #fff;
        padding: 10px 29px;
        margin-bottom: 10px;
        border-radius: 5px;
        font-size: 20px;
        border: 1px solid #f15d74;
        width: 100%;
    }
</style>
<!--Breadcrumb-->
<section>
    <div class="bannerimg cover-image bg-background3" data-image-src="../assets/images/banners/banner2.jpg">
        <div class="header-text mb-0">
            <div class="container">
                <div class="text-center text-white">
                    <h1 class="">Contact Us</h1>
                    <ol class="breadcrumb text-center">
                        <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                        <li class="breadcrumb-item active text-white" aria-current="page">Contact Us</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Breadcrumb-->

<!--User Dashboard-->
<section class="sptb">
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-lg-6 col-md-6 col-12 contact-text order-lg-0 order-md-0">
                <h2 class="contact-informaion">Contact Information</h2>        
                <!--                <div class="media d-flex">
                                    <div>
                                        <i class="fal fa-map-marker-alt"></i>
                                    </div>
                                    <div class="media-body padd-contact-top">
                                        <h5 class="mt-0">Reach Us</h5>
                                        <p>   Vizag - 530002(AP) </p>
                                    </div>
                                </div>-->

                <div class="media mt-3 d-flex">
                    <div>
                        <i class="fal fa-phone"></i>
                    </div>
                    <div class="media-body padd-contact-top">
                        <h5 class="mt-0">Call Us</h5>
                        <p> <a href="tel:<?= SITE_PHONE ?>"><?= SITE_PHONE ?></a>
                            <?php if (!empty(SITE_ALT_PHONE)) { ?>
                                , <a href="tel:<?= SITE_ALT_PHONE ?>"><?= SITE_ALT_PHONE ?></a>
                            <?php } ?>
                        </p>
                    </div>
                </div>

                <div class="media mt-3 d-flex">
                    <div>
                        <i class="fal fa-envelope"></i>
                    </div>
                    <div class="media-body padd-contact-top">
                        <h5 class="mt-0">Drop a Mail</h5>
                        <p> <a href="mailto:<?= SITE_EMAIL ?>"><?= SITE_EMAIL ?></a>
                            <?php if (!empty(SITE_ALT_EMAIL)) { ?>
                                , <a href="mailto:<?= SITE_ALT_EMAIL ?>"><?= SITE_ALT_EMAIL ?></a>
                            <?php } ?>
                        </p>
                    </div>
                </div>      

            </div>


            <div class="col-lg-6 col-md-6 col-12 mobi-top order-lg-1 order-md-1">
                <div class="white-block">
                    <h2>Get in Touch</h2>
                    <div class="clear-fix mt-3">
                        <form action="" method="post" id="contact-form">
                            <div class="row login-forms reply-form">
                                <div class="form-group mb-0 col-lg-12">
                                    <div class="response">

                                    </div>
                                </div>

                                <div class="col-12">
                                    <input type="text" name="name" placeholder="Name"  minlength="3" data-parsley-pattern="^[a-zA-Z\s]*$" data-parsley-pattern-message="Please Enter Valid Name" data-parsley-required-message="Please Enter Valid Name" required>
                                </div>
                                <div class="col-12 pt-2">
                                    <input type="email" name="email" placeholder="Email" data-parsley-type-message="Please Enter Valid Email" data-parsley-required-message="Please Enter Valid Email" data-parsley-trigger="change" required>
                                </div>
                                <div class="col-12 pt-2">
                                    <input type="text" name="mobile" placeholder="Mobile"  min='1' data-parsley-minlength="10"  data-parsley-maxlength="10" data-parsley-required-message="Please Enter Mobile Number" data-parsley-minlength-message="Enter Valid Mobile Number *" data-parsley-maxlength-message="Enter Valid Mobile Number" required>
                                </div>
                                <div class="col-12 pt-2">
                                    <textarea cols="3" name="message" rows="4" placeholder="Message" required
                                              data-parsley-required-message="Please Enter Message" ></textarea>
                                </div>


                                <div class="col-12 pt-2 ">
                                    <button class="submit-btn" type="submit">Submit</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>





        </div>
    </div>
</section>

<script src="<?= base_url('web_assets/') ?>/js/plugins/parsleyjs/dist/parsley.min.js"></script>
<script>
    $(document).ready(function () {
        $('#contact-form').parsley();
    });
</script>