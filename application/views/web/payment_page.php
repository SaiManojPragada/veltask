<section class="sptb">
    <div class="container" style="padding: 150px 0px">
        <center>
            <br>
            <img src="<?= base_url('web_assets/pink_loader.gif') ?>" width="150"/>
            <br>
            <p>Please Do not refresh this Page</p>
            <br>
            <br>
            <br>
        </center>
    </div>
</section>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
    var custom = "<?= $custom_url ?>";
    var options = {
        "key": "<?= RAZORPAY_KEY ?>", // Enter the Key ID generated from the Dashboard
        "amount": '<?= $razorpay_order_data->amount ?>', // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
        "currency": "INR",
        "name": "<?= SITE_NAME ?>",
        "description": 'Services Order Payment',
        "image": "<?= base_url() ?>uploads/<?= SITE_FAV_ICON ?>",
                "order_id": "<?= $razorpay_order_data->id ?>", //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
                "handler": function (response) {

                    var r_p_id = response.razorpay_payment_id;
                    var r_o_id = response.razorpay_order_id;
                    var r_signature = response.razorpay_signature;
                    //ajax for the payment success comes here
                    var aj_data = <?= $out_data ?>;
                    aj_data.amount_paid = '<?= ($razorpay_order_data->amount / 100) ?>';
                    aj_data.razorpay_transaction_id = r_p_id;
                    aj_data.razorpay_order_id = r_o_id;
                    $.ajax({
                        url: "<?= base_url('api/') ?><?= ($custom_url) ? $custom_url : 'place_order' ?>",
                        type: "post",
                        data: aj_data,
                        success: function (json) {
                            resp = JSON.parse(json);
                            console.log(resp);
                            if (resp['status']) {
                                swal({
                                    title: resp['message'],
                                    icon: "success"
                                }).then(function () {
                                    if (custom.length > 3) {
                                        window.history.go(-2);
                                    } else {
                                        location.href = "<?= base_url('placeorder/success/' . $order_id) ?>";
                                    }

                                });
                            } else {
                                swal({
                                    title: resp['message'],
                                    icon: "info"
                                }).then(function () {
                                    if (custom.length > 3) {
                                        window.history.go(-2);
                                    } else {
                                        location.href = "<?= base_url('placeorder/failed/' . $order_id) ?>/" + r_o_id + "/" + r_p_id;
                                    }
                                });
                            }
                        }
                    });
                },
                "modal": {
                    ondismiss: function () {
                        $.ajax({
                            url: "<?= base_url('cart/payment_cancelled') ?>",
                            type: "post",
                            success: function (resp) {
                                if (custom.length > 3) {
                                    window.history.go(-2);
                                } else {
                                    location.href = "<?= base_url('check-out/payment-method') ?>";
                                }

                            }
                        });
                    }
                },
                "prefill": {
                    "name": "<?= USER_NAME ?>",
                    "email": "<?= USER_EMAIL ?>",
                    "contact": "<?= USER_PHONE ?>"
                },
                "notes": {
                    "address": "Paying to <?= SITE_NAME ?>"
                },
                "theme": {
                    "color": "#F16670"
                }
            };
            var rzp1 = new Razorpay(options);
            rzp1.on('payment.failed', function (response) {
//                location.href = "<?= base_url() ?>/payment/failed/";
                $.ajax({
                    url: "<?= base_url('cart/payment_cancelled') ?>",
                    type: "post",
                    success: function (resp) {
                        location.href = "<?= base_url('check-out/payment-method') ?>";
                    }
                });
            });

            $(document).ready(function () {
                rzp1.open();
            });
</script>