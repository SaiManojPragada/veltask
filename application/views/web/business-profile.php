<!--Breadcrumb-->
<section>
    <div class="bannerimg cover-image bg-background3" data-image-src="../assets/images/banners/banner2.jpg">
        <div class="header-text mb-0">
            <div class="container">
                <div class="text-center text-white">
                    <h1 class="">Business Profile</h1>
                    <ol class="breadcrumb text-center">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active text-white" aria-current="page">Business Profile</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Breadcrumb-->
<?php if (!empty(USER_ID)) { ?>
    <!--User Dashboard-->
    <section class="sptb">
        <div class="container">
            <div class="row">
                <?php include 'dashboard-left-menu.php'; ?>
                <div class="col-xl-9 col-lg-12 col-md-12">
                    <div class="card">
                        <div class="card-header refer-parent p-3" style="width: 100%">
                            <h5>Business Profile &nbsp;&nbsp;</h5><span style="position: absolute; right: 30px; font-weight: bold"><?php
                                if ($business_details) {
                                    if (($business_details->status)) {
                                        ?><span style="color: green">Profile Approved</span><?php } else { ?><span style="color: tomato">Profile Under Review</span><?php
                                    }
                                }
                                ?></span>
                            <small><?= empty($business_details) ? 'No Business Profile' : '' ?></small>
                        </div>
                        <div class="card-body">
                            <?php if (empty($business_details) || !$business_details->status) { ?>
                                <form method="post" action="<?= base_url('business_profile/update') ?>" id="profile-form" class="row" enctype="multipart/form-data">
                                    <p class="row"><strong style="color: red; width: 8%; text-align: right">* Note :</strong> <span style="width: 92%; padding-left: 10px">By Updating your business profile it will be automatically disapproved any Benifits will be activated once the profile is approved and any membership plans will not be applied after this profile is activated.</span></p>
                                    <div class="form-group col-12">
                                        <label for="exampleInputEmail1">Business Name *</label>
                                        <input type="name" class="form-control" id="company_name" name="company_name" aria-describedby="company_name" placeholder="Enter Business Name" value="<?= $business_details->company_name ?>"
                                               minlength="3" data-parsley-pattern="^[a-zA-Z\s]*$" data-parsley-pattern-message="Please Enter Valid Business Name" data-parsley-required-message="Please Enter Valid Business Name" required>
                                    </div>

                                    <div class="form-group col-6">
                                        <label for="exampleInputEmail1">Proof Type *</label>
                                        <select name="proof_type" class="form-control" required data-parsley-required-message="Please Select Proof Type">
                                            <option disabled selected value="">-- Select Proof Type --</option>
                                            <?php foreach ($docs_types as $item) { ?>
                                                <option value="<?= $item->document_type ?>" <?= ($business_details->proof_type == $item->document_type) ? "selected" : "" ?>><?= $item->document_type ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group col-6">
                                        <label for="exampleInputEmail1">Proof (File)*</label>
                                        <input type="file" class="form-control" name="document" <?= !empty($business_details->document) ? '' : 'required' ?> data-parsley-required-message="Please Select Proof Document" accept="image/*">
                                        <br>
                                        <?php if (!empty($business_details->document)) { ?>
                                            <a href="<?= base_url('uploads/company_proofs/') . $business_details->document ?>" target="_blank" style="float: right"><b>View Prev Document <i class="fa fa-file-alt"></i></b></a>
                                        <?php } ?>
                                    </div>

                                    <div class="form-group col-12">
                                        <label for="exampleInputEmail1">Business Address *</label>
                                        <textarea class="form-control" id="company_address" name="company_address" aria-describedby="company_address" placeholder="Enter Business Address" value="<?= $user_data->company_address ?>"
                                                  minlength="10" data-parsley-required-message="Please Enter Valid Business Address" required rows="4"><?= $business_details->company_address ?></textarea>
                                    </div>

                                    <div class="form-group col-12">
                                        <button class="btn my-custom-btn" type="submit" style="float: right">Update</button>

                                    </div>
                                </form>
                            <?php } else { ?>
                                <div class="row">
                                    <p class="row"><strong style="color: red; width: 8%; text-align: right">* Note :</strong> <span style="width: 92%; padding-left: 10px">By Updating your business profile it will be automatically disapproved any Benifits will be activated once the profile is approved and any membership plans will not be applied after this profile is activated.</span></p>
                                    <div class="form-group col-12">
                                        <label for="exampleInputEmail1">Business Name </label>
                                        <input type="name" class="form-control" readonly value="<?= $business_details->company_name ?>">
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="exampleInputEmail1">Proof Type </label>
                                        <input type="name" class="form-control" readonly value="<?= $business_details->proof_type ?>">
                                    </div>
                                    <div class="form-group col-6">
                                        <?php if (!empty($business_details->document)) { ?>
                                            <label for="exampleInputEmail1">Previous File </label>
                                            <a href="<?= base_url('uploads/company_proofs/') . $business_details->document ?>" class="form-control" target="_blank"><b>View Prev Document <i class="fa fa-file-alt"></i></b></a>
                                        <?php } ?>
                                    </div>

                                    <div class="form-group col-12">
                                        <label for="exampleInputEmail1">Business Address *</label>
                                        <textarea class="form-control" value="<?= $user_data->company_address ?>" readonly rows="4"><?= $business_details->company_address ?></textarea>
                                    </div>
                                </div>
                            <?php } ?>
                            <hr>
                            <h4>Business Benifits</h4>
                            <hr>
                            <div class="Container" style="padding: 0px 30px">
                                <?= $business_benifits->benefits_description ?>
                                <br>
                                <p><b style="color: red">* Note : </b><span><?= $business_benifits->benefits_applicable_message ?></span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--Parsley Pulgin-->
    <script src="<?= base_url('web_assets/') ?>/js/plugins/parsleyjs/dist/parsley.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#profile-form').parsley();
        });
    </script>
    <script>
        function getCities(val) {
            $.ajax({
                url: "<?= base_url() ?>website/getCities",
                type: "post",
                data: {state_id: val},
                success: function (resp) {
                    console.log(resp);
                    $("#cities").html(resp);
                }
            });
        }
    </script>
<?php } else { ?>
    <div class="container-fluid" style="min-height: 400px">
        <h3 style="text-align: center; margin-top: 160px"> Please Login to Continue </h3>
    </div>
<?php } ?>
<!--/User Dashboard-->

<!-- Newsletter-->

<!--/Newsletter-->