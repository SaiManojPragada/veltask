<!--Sliders Section-->
<div>
    <div class="cover-image sptb-1 bg-background" data-image-src="../assets/images/banners/banner1.jpg">
        <div class="header-text1 mb-0">
            <div class="container">
                <div class="row">
                    <div class="col-xl-10 col-lg-12 col-md-12 d-block ">
                        <div class=" breadcrumb-banner text-left text-white ">
                            <h1 > Beauty & Salon
                            </h1>
                        </div>
                        <div>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="index.php">Home
                                    </a>
                                </li>

                                <li class="breadcrumb-item active" aria-current="page">Blog View
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /header-text -->
    </div>
</div>
<!--/Sliders Section-->
<!--Add listing-->
<section class="sptb">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12 col-sm-12 col-12">
                <div class="article_detail_wrapss single_article_wrap format-standard">
                    <div class="article_body_wrap">
                        <div class="article_featured_image">
                            <img src="<?= base_url('uploads/blogs/') . $blog_data->image ?>" class="img-fluid" alt="">
                        </div>

                        <div class=" d-flex mt-3">
                            <ul class="d-flex mb-0">

                                <li class="mr-5"><a href="#" class="blog-icons"><i class="fa fa-user" aria-hidden="true"></i> By <?= $blog_data->posted_by ?></a></li>
                            </ul>

                        </div>
                        <h2 class="post-title"><?= $blog_data->title ?></h2>
                        <p class="mt-5">
                            <?= $blog_data->description ?>
                        </p>
                    </div>
                </div>  



            </div>
            <!--Right Side Content-->
            <div class="col-xl-4 col-lg-4 col-md-12">
                <div class="right-side-viewdetails">


                    <div class="card">
                        <div class="card-header pl-4 pr-4">
                            <h3 class="card-title">Related Blogs</h3>
                        </div>
                        <div class="card-body ">
                            <?php if (!empty($blogs)) { ?>
                                <ul class="vertical-scroll">
                                    <?php foreach ($blogs as $blog) { ?>
                                        <li class="news-item" onclick="location.href = '<?= base_url() ?>blogs/<?= $blog->seo_url ?>';" style="cursor: pointer">
                                            <table>
                                                <tbody><tr>
                                                        <td><img src="<?= base_url('uploads/blogs/') . $blog->image ?>" alt="image" class="w-8"></td>
                                                        <td>
                                                            <div class="table-content">

                                                                <h5 class="mb-1 "><?= $blog->title ?></h5>



                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody></table>
                                        </li>
                                    <?php } ?>
                                </ul>
                            <?php } else { ?>
                                <center><h4>-- No Blogs Found -- </h4></center>
                            <?php } ?>
                        </div>
                    </div>


                </div>
            </div>
            <!--/Right Side Content-->
        </div>
    </div>
</section>