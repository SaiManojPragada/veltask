<!--Sliders Section-->
<div>
    <div class="cover-image sptb-1 bg-background" data-image-src="../assets/images/banners/banner1.jpg">
        <div class="header-text1 mb-0">
            <div class="container">
                <div class="row">
                    <div class="col-xl-10 col-lg-12 col-md-12 d-block ">
                        <div class=" breadcrumb-banner text-left text-white ">
                            <h1 > FAQ
                            </h1>
                        </div>
                        <div>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="<?= base_url() ?>">Home
                                    </a>
                                </li>

                                <li class="breadcrumb-item active" aria-current="page"><a href="javascript:void(0);">FAQ</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /header-text -->
    </div>
</div>
<!--/Sliders Section-->
<!--Add listing-->
<section class="sptb">
    <div class="container">
        <div id="accordion">

            <?php foreach ($faqs as $index => $ff) { ?>
                <div class="card">
                    <div class="card-header" id="heading<?= $index ?>" data-toggle="collapse" data-target="#collapse<?= $index ?>" aria-expanded="true" aria-controls="collapse<?= $index ?>">
                        <h5 class="mb-0">
                            <button class="btn btn-link">
                                <?= $ff->title ?>
                            </button>
                        </h5>
                    </div>

                    <div id="collapse<?= $index ?>" class="collapse <?= ($index == 0) ? "show" : "" ?>" aria-labelledby="heading<?= $index ?>" data-parent="#accordion">
                        <div class="card-body">
                            <?= $ff->description ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>