<!--Breadcrumb-->
<section>
    <div class="bannerimg cover-image bg-background3" data-image-src="<?= base_url('web_assets') ?>/assets/images/banners/banner2.jpg">
        <div class="header-text mb-0">
            <div class="container">
                <div class="text-center text-white">
                    <h1 class="">My Dashboard</h1>
                    <ol class="breadcrumb text-center">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active text-white" aria-current="page">My Orders</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Breadcrumb-->

<!--User Dashboard-->
<section class="sptb">
    <div class="container">
        <div class="row">
            <?php include 'dashboard-left-menu.php'; ?>
            <div class="col-xl-9 col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header refer-parent p-3">
                        <h5>Notifications</h5>
                    </div>
                    <div class="card-body">
                        <?php foreach ($notifications as $not) { ?>
                            <div class="card" style="cursor: pointer" onclick="location.href = '<?= base_url() ?>my-bookings/view/<?= $not->order_id_re ?>'">
                                <div class="card-header refer-parent p-3">
                                    <b><?= $not->title ?></b>
                                </div>
                                <div class="card-body">
                                    <?= $not->message ?>
                                    <span>
                                        <small style="font-size: 10px; font-weight: bold; float: right; margin-top: 30px"><?= date('d M Y, h:i A', $not->created_at) ?></small>
                                    </span>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if (empty($notifications)) { ?>
                            <div class="col-md-12" style="margin: 40px 0px">
                                <center> -- No Notifications Found -- </center>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>