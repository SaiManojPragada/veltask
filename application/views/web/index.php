<!-- Sliders Starts here -->
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <?php foreach ($sliders as $index => $slide) { ?>
            <div class="carousel-item <?= ($index == 0) ? 'active' : '' ?>">
                <img src="<?= $slide['web_image'] ?>" class="d-block w-100" alt="...">
            </div>
        <?php } ?> 
    </div>
    <button class="carousel-control-prev" type="button" data-target="#carouselExampleControls" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-target="#carouselExampleControls" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </button>
</div>
<!-- Ends here -->

<!--Service Categories-->
<section class=" bg-white our-service-section ">
    <div class="container">
        <div class="section-title center-block text-center">
            <h2>Our Services</h2>
        </div>
        <div class="row justify-content-md-center">
            <?php
            $rest = array();
            foreach ($services_categories as $index => $cat) {
                ?>
                <?php if ($index < 6) { ?>
                    <div class="col-xl-2 col-lg-2 col-md-3 col-6" style="cursor: pointer" onclick="location.href = '<?= base_url('services/') . urlencode($cat['seo_url']) . '/' ?>'">
                        <div class="card bg-card-light">
                            <div class="card-body">
                                <div class="cat-item text-center">
                                    <div class="cat-img">
                                        <img src="<?= $cat['image'] ?>" alt="<?= $cat['title'] ?>">
                                    </div>
                                    <div class="cat-desc">
                                        <h5 class="mb-1"><?= $cat['title'] ?></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                } else {
                    array_push($rest, $cat);
                }
                ?>
            <?php } ?>

        </div>
        <div class="row collapse justify-content-md-center" id="extra-cat-services">
            <?php foreach ($rest as $index => $cat) { ?>
                <div class="col-xl-2 col-lg-2 col-md-3 col-6" style="cursor: pointer" onclick="location.href = '<?= base_url('services/') . urlencode($cat['seo_url']) . '/' ?>'">
                    <div class="card bg-card-light">
                        <div class="card-body">
                            <div class="cat-item text-center">
                                <div class="cat-img">
                                    <img src="<?= $cat['image'] ?>" alt="<?= $cat['title'] ?>">
                                </div>
                                <div class="cat-desc">
                                    <h5 class="mb-1"><?= $cat['title'] ?></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="text-center view-btn-new">
            <?php if (sizeof($services_categories) > 6) { ?>
                <div class="mt-5"><a href="javascript:void(0);" class="btn btn-primary btn-pill" data-toggle="collapse" data-target="#extra-cat-services">View All</a> </div>
            <?php } ?>
        </div>
    </div>
</section>
<!--/Categories-->

<!--Offers-->
<section class="service-offer bg-patterns">
    <div class="container">
        <div class="section-title center-block text-center">
            <h2>Service Offers</h2>								
        </div>
        <div id="myCarousel1" class="owl-carousel owl-carousel-icons2 sp-cls">
            <?php foreach ($banners as $banner) { ?>
                <div class="item">
                    <img src="<?= $banner['web_image'] ?>" alt="<?= $banner['title'] ?>">
                </div>
            <?php } ?>
            <!--            <div class="item">
                <img src="assets/images/offer-1.jpg" alt="">
            </div>
            <div class="item">
                <img src="assets/images/offer-2.jpg" alt="">
            </div>
            <div class="item">
                <img src="assets/images/offer-3.jpg" alt="">
            </div>
            <div class="item">
                <img src="assets/images/offer-4.jpg" alt="">
            </div>-->
        </div>
    </div>
</section>
<!--Latest Ads-->

<!--Product Categories-->
<section class="sptb bg-white" id="ecommSection">
    <div class="container">
        <div class="section-title center-block text-center">
            <h2>Shop Now</h2>
        </div>
        <div class="row">
            <?php $ecom_res = array(); ?>
            <?php foreach ($ecom_categories as $index => $cat) { ?>
                <?php if ($index < 6) { ?>
                    <div class="col-xl-2 col-lg-2 col-md-3 col-6" onclick="location.href = '<?= base_url('web/store_categories/') . $cat->seo_url ?>'">
                        <div class="card bg-card-light">
                            <div class="card-body">
                                <div class="cat-item text-center">
                                    <a href="#"></a>
                                    <div class="cat-img">
                                        <img src="<?= base_url('uploads/categories/') . $cat->app_image ?>" alt="img">
                                    </div>
                                    <div class="cat-desc">
                                        <h5 class="mb-1"><?= $cat->category_name ?></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                } else {
                    array_push($ecom_res, $cat);
                }
                ?>
            <?php } ?>

        </div>
        <div class="row collapse justify-content-md-center" id="extra-cat-ecomm">
            <?php foreach ($ecom_res as $cat) { ?>
                <div class="col-xl-2 col-lg-2 col-md-3 col-6" onclick="location.href = '<?= base_url('web/store_categories/') . $cat->seo_url ?>'">
                    <div class="card bg-card-light">
                        <div class="card-body">
                            <div class="cat-item text-center">
                                <div class="cat-img">
                                    <img src="<?= base_url('uploads/categories/') . $cat->app_image ?>" alt="img">
                                </div>
                                <div class="cat-desc">
                                    <h5 class="mb-1"><?= $cat->category_name ?></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="text-center view-btn-new">
            <?php if (sizeof($ecom_categories) > 6) { ?>
                <div class="mt-5"><a href="javascript:void(0);" class="btn btn-primary btn-pill" data-toggle="collapse" data-target="#extra-cat-ecomm">View All</a> </div>
            <?php } ?>
        </div>
    </div>
</section>
<!--/Categories-->

<!-- Banner Ads -->
<section class="text-center bg-white d-none">
    <div class="middle-bg">
        <div class="owl-carousel testimonial-owl-carousel2 slider slider-header ">
            <div class="item cover-image" data-image-src="">
                <img  alt="first slide" src="assets/images/inner-banner.jpg" >
            </div>
            <div class="item">
                <img  alt="first slide" src="assets/images/inner-banner-2.jpg" >
            </div>
            <div class="item">
                <img  alt="first slide" src="assets/images/inner-banner-3.jpg" >
            </div>						
        </div>

    </div>
</section>
<!-- Ends here -->

<!--Popular This Week-->
<section class="sptb bg-white d-none">
    <div class="container">
        <div class="section-title center-block text-center">
            <h2>Popular This Week</h2>
        </div>
        <div id="myCarousel1" class="owl-carousel owl-carousel-icons5 popular-week">
            <div class="item">
                <img src="assets/images/prod-1.jpg" alt="">
                <div class="popular-products">
                    <h2>Fresh Brown Coconut</h2>
                    <div class="price"><span>₹300</span> ₹210</div>
                    <a href="#" class="btn btn-primary btn-ptill mb-3">Add to Cart</a>
                </div>
            </div>
            <div class="item">
                <img src="assets/images/prod-2.jpg" alt="">
                <div class="popular-products">
                    <h2>Fresh Organic Strawberry</h2>
                    <div class="price"><span>₹460</span> ₹320</div>
                    <a href="#" class="btn btn-primary btn-ptill mb-3">Add to Cart</a>
                </div>
            </div>
            <div class="item">
                <img src="assets/images/prod-3.jpg" alt="">
                <div class="popular-products">
                    <h2>Fresh Meat</h2>
                    <div class="price"><span>₹800</span> ₹625</div>
                    <a href="#" class="btn btn-primary btn-ptill mb-3">Add to Cart</a>
                </div>
            </div>
            <div class="item">
                <img src="assets/images/prod-4.jpg" alt="">
                <div class="popular-products">
                    <h2>Coca Cola</h2>
                    <div class="price"><span>₹95</span> ₹82</div>
                    <a href="#" class="btn btn-primary btn-ptill mb-3">Add to Cart</a>
                </div>
            </div>
        </div>	

    </div>
</section>
<!--/Featured Ads-->

<!--Featured Ads-->
<section class="sptb bg-white d-none">
    <div class="container">
        <div class="section-title center-block text-center">
            <h2>Discover the Best Value Deals</h2>
        </div>				
        <div class="row best-deal-offer">
            <div class="col-12 col-md-12 col-lg-12 col-xl-6">
                <div class="row">
                    <div class="col-sm-12 col-lg-6 col-md-6 mb-1 text-center">								
                        <a href="#">
                            <div class="best-deal-img"><img src="assets/images/meat-bg.jpg" alt="img" class="cover-image"></div>
                            <h2>Upto 40% Off</h2></a>
                    </div>
                    <div class="col-sm-12 col-lg-6 col-md-6 mb-1 text-center">								
                        <a href="#"><div class="best-deal-img"><img src="assets/images/vegetables.jpg" alt="img" class="cover-image"></div>
                            <h2>Save ₹100 Off</h2></a>
                    </div>
                    <div class="col-lg-12 col-xl-12 mb-1 text-center">
                        <a href="#"><div class="best-deal-img"><img src="assets/images/burger-bg.jpg" alt="img" class="cover-image"></div>
                            <h2>Flat 30% Off</h2></a>
                    </div>							
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-xl-6 col-sm-12">
                <div class="row">
                    <div class="col-lg-12 col-xl-12 mb-1 text-center">
                        <a href="#"><div class="best-deal-img"><img src="assets/images/apperals.jpg" alt="img" class="cover-image"></div>
                            <h2>Upto 50% Off</h2></a>
                    </div>
                    <div class="col-sm-12 col-lg-6 col-md-6 mb-1 text-center">								
                        <a href="#"><div class="best-deal-img"><img src="assets/images/medicine-bg.jpg" alt="img" class="cover-image">
                            </div>
                            <h2>Flat 30% Off</h2></a>
                    </div>
                    <div class="col-sm-12 col-lg-6 col-md-6 mb-1 text-center">
                        <a href="#"><div class="best-deal-img"><img src="assets/images/cosmetic-bg.jpg" alt="img" class="cover-image">
                            </div>
                            <h2>Save ₹150 Off</h2></a>
                    </div>

                </div>
            </div>
        </div>				
    </div>
</section>
<!--/Featured Ads-->

<!-- App Screen -->
<section class="downloadpp-bg">	
    <div class="container">	
        <div class="row mob-center justify-content-center">
            <div class="col-lg-4 col-sm-6 col-12 hide-mobi-item">        
                <img src="<?= base_url('uploads/') . $admin->user_app_screenshot ?>" alt="" class="img-fluid">    
            </div>
            <div class="col-lg-5 col-sm-6 col-12 app-content">        
                <h2>This app is available for
                    <span>your smart phone.</span></h2>
                <p>Download Our App Now In Playstore!</p>
                <p><?= $admin->user_app_description ?></p> 
                <a href="#"><img src="<?= base_url('web_assets/') ?>assets/images/googleplay-tra-white.png" width="200" style="padding-top: 15px;"></a>
                <br>
            </div>
        </div>
    </div>
</section>
<!-- App Screen -->

<!-- Franchise -->
<section class="services-bg bg-white d-none"  id="franchise">
    <div class="container">
        <img src="<?= base_url('web_assets/') ?>assets/images/store-franchise.png" class="img-fluid">
    </div>	
    <div class="container pt-5">
        <div class="row">
            <div class="col-12 text-center">
                <h1>The Best <span>Franchise</span></h1>
            </div>
            <div class="mr-top col-12">
                <div class="row justify-content-center">
                    <?php foreach ($franchise_tabs as $tab) { ?>
                        <div class="col-md-4 col-6 franchise-content text-center mt-4">
                            <img src="<?= base_url('uploads/franchise_tabs/') . $tab->image ?>">
                            <span><?= $tab->count ?></span>
                            <?= $tab->quote ?>
                        </div>
                    <?php } ?>
                    <!--                    <div class="col-md-4 col-6 franchise-content text-center mt-4">
                                            <img src="assets/images/franchise-icon-1.svg">
                                            <span>600-1200</span>
                                            sqft space Total carpet area
                                        </div>
                                        <div class="col-md-4 col-6 franchise-content text-center mt-4">
                                            <img src="assets/images/franchise-icon-2.svg">
                                            <span>18Lakhs</span>
                                            Investment (Refundable Deposit ) 
                                        </div>
                                        <div class="col-md-4 col-6 franchise-content text-center mt-4">
                                            <img src="assets/images/franchise-icon-5.svg">
                                            <span>Expansion</span>
                                            Pan India
                                        </div>
                                        <div class="col-md-4 col-6 franchise-content text-center mt-5">
                                            <img src="assets/images/franchise-icon-3.svg">
                                            <span>12-24 month</span>
                                            Payback period
                    
                                        </div>
                                        <div class="col-md-4 franchise-content text-center mt-5">
                                            <img src="assets/images/franchise-icon-4.svg" width="120">
                                            <span>60% - 80%</span>
                                            Gross margin
                                        </div>-->
                </div>
            </div>	
        </div>
    </div>
</section>
<!-- Ends here -->



<!--Testimonials-->
<?php if (!empty($testimonials)) { ?>
    <section class="sptb bg-pattern">
        <div class="container">
            <div class="section-title center-block text-center">
                <h1 class="position-relative">Testimonials</h1>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id="myCarousel" class="owl-carousel testimonial-owl-carousel">
                        <?php foreach ($testimonials as $testi) { ?>
                            <div class="item text-center">
                                <div class="row">
                                    <div class="col-xl-8 col-md-12 d-block mx-auto">
                                        <div class="testimonia">
                                            <p>
                                                <i class="fa fa-quote-left text-white-80"></i>
                                                <?= $testi->comment ?>
                                            </p>
                                            <h3 class="title"><?= $testi->name ?></h3>
                                            <span class="post"><?= $testi->position ?></span>											

                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } ?>
<!--/Testimonials-->