<section class="sptb">
    <div class="container" style="padding: 150px 0px">
        <center>
            <br>
            <img src="<?= base_url('web_assets/pink_loader.gif') ?>" width="150"/>
            <br>
            <p>Please Do not refresh this Page</p>
            <br>
            <br>
            <br>
        </center>
    </div>
</section>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
    var options = {
        "key": "<?= RAZORPAY_KEY ?>", // Enter the Key ID generated from the Dashboard
        "amount": '<?= $razorpay_order_data->amount ?>', // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
        "currency": "INR",
        "name": "<?= SITE_NAME ?>",
        "description": 'Membership Payment',
        "image": "<?= base_url() ?>uploads/<?= SITE_FAV_ICON ?>",
                "order_id": "<?= $razorpay_order_data->id ?>", //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
                "handler": function (response) {

                    var r_p_id = response.razorpay_payment_id;
                    var r_o_id = response.razorpay_order_id;
                    var r_signature = response.razorpay_signature;
                    //ajax for the payment success comes here
                    $.ajax({
                        url: "<?= base_url('api/memberships/subscribe') ?>",
                        type: 'post',
                        data: {user_id: "<?= USER_ID ?>", membership_id: "<?= $membership_data->id ?>", razorpay_order_id: r_o_id, razorpay_transaction_id: r_p_id, amount_paid: '<?= ($razorpay_order_data->amount) / 100 ?>'},
                        success: function (resp) {
                            resp = JSON.parse(resp);
                            if (resp['status']) {
                                swal({
                                    title: resp['message'],
                                    icon: "success"
                                }).then(function () {
                                    location.href = "<?= $this->agent->referrer(); ?>";
                                });
                            } else {
                                swal({
                                    title: resp['message'],
                                    icon: 'info'
                                });
                            }
                        }
                    });
                },
                "modal": {
                    ondismiss: function () {
                        location.href = "<?= $this->agent->referrer(); ?>";
                    }
                },
                "prefill": {
                    "name": "<?= USER_NAME ?>",
                    "email": "<?= USER_EMAIL ?>",
                    "contact": "<?= USER_PHONE ?>"
                },
                "notes": {
                    "address": "Paying to <?= SITE_NAME ?>"
                },
                "theme": {
                    "color": "#F16670"
                }
            };
            var rzp1 = new Razorpay(options);
            rzp1.on('payment.failed', function (response) {
//                location.href = "<?= base_url() ?>/payment/failed/";
                $.ajax({
                    url: "<?= base_url('cart/payment_cancelled') ?>",
                    type: "post",
                    success: function (resp) {
                        location.href = "<?= base_url('check-out/payment-method') ?>";
                    }
                });
            });
            $(document).ready(function () {
                rzp1.open();
            });
</script>