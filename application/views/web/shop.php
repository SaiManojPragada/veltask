<?php include 'includes/header.php'; ?>

		<!--Sliders Section-->
		<section class="bg-white">
			<div class="banner1  relative slider-images">				
					<div class="owl-carousel testimonial-owl-carousel2 slider slider-header ">
						<div class="item cover-image" data-image-src="">
							<img  alt="first slide" src="assets/images/banner-1.jpg" >
							
						</div>
						<div class="item">
							<img  alt="first slide" src="assets/images/banner-2.jpg" >
						</div>
						<div class="item">
							<img  alt="first slide" src="assets/images/banner-4.jpg" >
						</div>						
					</div>				
			</div>
		</section>
		<!--Sliders Section-->



		<section class="sptb bg-white">
			<div class="container">
				<div class="section-title center-block text-center">
					<h2>Shop By Category</h2>
				</div>
				<div id="myCarousel1" class="owl-carousel owl-carousel-icons15 popular-week">
					<div class="item">
					
						<div class="card bg-card-light">
							<div class="card-body">
								<div class="cat-item text-center">
									<a href="grocery-shop.php"></a>
									<div class="cat-img">
										<img src="assets/images/grocery-icon.png" alt="img">
									</div>
									<div class="cat-desc">
										<h5 class="mb-1">Non-Food</h5>
									</div>
								</div>
							</div>
						
					</div>
					</div>
					<div class="item">
						<div class="card bg-card-light">
							<div class="card-body">
								<div class="cat-item text-center">
									<a href="grocery-shop.php"></a>
									<div class="cat-img text-shadow1">
										<img src="assets/images/restaurent-icon.png" alt="img">
									</div>
									<div class="cat-desc">
										<h5 class="mb-1">Food</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
				
					
				</div>	
								
			</div>
		</section>

		<!--Popular This Week-->
		<section class="sptb bg-white">
			<div class="container">
				<div class="section-title center-block text-center">
					<h2>Best Selling </h2>
				</div>
				<div id="myCarousel1" class="owl-carousel owl-carousel-icons5 popular-week">
					<div class="item">
						<img src="assets/images/prod-1.jpg" alt="">
						<div class="popular-products">
							<h2>Fresh Brown Coconut</h2>
							<span>Revathimaheshwar Stores</span>
							<div class="price"><span>₹300</span> ₹210</div>
							<a href="#" class="btn btn-primary btn-ptill mb-3">Add to Cart</a>
						</div>
					</div>
					<div class="item">
						<img src="assets/images/prod-2.jpg" alt="">
						<div class="popular-products">
							<h2>Fresh Organic Strawberry</h2>
								<span>Revathimaheshwar Stores</span>
							<div class="price"><span>₹460</span> ₹320</div>
							<a href="#" class="btn btn-primary btn-ptill mb-3">Add to Cart</a>
						</div>
					</div>
					<div class="item">
						<img src="assets/images/prod-3.jpg" alt="">
						<div class="popular-products">
							<h2>Fresh Meat</h2>
								<span>Revathimaheshwar Stores</span>
							<div class="price"><span>₹800</span> ₹625</div>
							<a href="#" class="btn btn-primary btn-ptill mb-3">Add to Cart</a>
						</div>
					</div>
					<div class="item">
						<img src="assets/images/prod-4.jpg" alt="">
						<div class="popular-products">
							<h2>Coca Cola</h2>
								<span>Revathimaheshwar Stores</span>
							<div class="price"><span>₹95</span> ₹82</div>
							<a href="#" class="btn btn-primary btn-ptill mb-3">Add to Cart</a>
						</div>
					</div>
				</div>	
								
			</div>
		</section>
		<!--/Featured Ads-->


<?php include 'includes/footer.php'; ?>