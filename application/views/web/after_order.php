<style>
    .refer-friend-child p span {
        display: block;
        line-height: 0.9;
    }

    .refer-friend-child p:nth-child(2) {
        color: #f47e62;
        font-weight: bold;
        font-size: 31px;
    }
    .continue-shopping a {
        background: #f15d74;
        border: 1px solid #f15d74;
        color: #fff !important;
        padding: 10px 13px;
        border-radius: 6px;
        font-weight: 600;
    }

    .continue-shopping a:hover {
        background: transparent;
        border: 1px solid #f15d74;
        color: #f15d74 !important;
        font-weight: 600;
    }
</style>
<!--Breadcrumb-->
<section>
    <div class="bannerimg cover-image bg-background3" data-image-src="../assets/images/banners/banner2.jpg">
        <div class="header-text mb-0">
            <div class="container">
                <div class="text-center text-white">
                    <h1 class="">Your Order</h1>
                    <ol class="breadcrumb text-center">
                        <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                        <li class="breadcrumb-item active text-white" aria-current="page">Order Placed</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Breadcrumb-->

<!--User Dashboard-->
<?php if (!$error) { ?>
    <section class="sptb">
        <div class="container">
            <div class="row d-flex justify-content-center">

                <div class="col-xl-12 col-lg-12 col-md-12">


                    <div class="card ">

                        <div class="card-body">
                            <div class="refer-friend-child">
                                <div id="sv_body">
                                    <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52"><circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/><path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/></svg>
                                </div>
                                <p>Your Order has been <span> Placed Successfully
                                    </span></p>


                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title mb-0 p-3">Booking Order Details</h4>
                                <a style="position: absolute; right: 30px; cursor: pointer" href="<?= base_url('my-bookings') ?>" class="cart-btn-menu"> My Orders <i class="far fa-angle-right"></i></a>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6 text-sm-right">
                                        <div class="billing-info">
                                            <h4 class="d-block"  style="text-align: left; margin: 10px 15px">Order Details</h4>
                                            <div  style="border: 1px solid #eaeaea; border-radius: 15px; padding: 10px 15px; text-align: left">
                                                <?= $order->visit_and_quote_val ?>
                                                <span class="d-block text-sm text-muted"><b>Booking ID :</b> <span class="value">#<?= $order->order_id ?></span></span>
                                                <span class="d-block text-sm text-muted"><b>Category Name :</b> <span class="value"><?= $service_category->name ?></span></span>
                                                <span class="d-block text-sm text-muted"><b>Ordered On : </b> <span class="value"><?= date('d M Y, h:i A', $order->created_at) ?></span></span>
                                                <span class="d-block text-sm text-muted"><b>Timeslot Date :</b> <span class="value"><?= date('d M Y', strtotime($order->time_slot_date)) ?></span></span>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 text-sm-right">
                                        <div class="billing-info">
                                            <h4 class="d-block"  style="text-align: left; margin: 10px 15px">Booking Address</h4>
                                            <div  style="border: 1px solid #eaeaea; border-radius: 15px; padding: 10px 15px; text-align: left">
                                                <span class="d-block text-muted"><strong><?= $user_address->name ?></strong></span>
                                                <p class="d-block text-muted" style="margin-bottom: 0px">
                                                    <span class="d-block"><b>Address Type : </b><?php
                                                        if ($user_address->address_type == "1") {
                                                            echo 'Home';
                                                        } else if ($user_address->address_type == "2") {
                                                            echo "Office";
                                                        } else {
                                                            echo "Other";
                                                        }
                                                        ?></span>
                                                    <span class="d-block"><?= $user_address->address ?></span>
                                                    <span class="d-block"><?= $user_address->landmark ?></span>
                                            </div>
                                            </p>
                                        </div>
                                    </div>


                                </div>
                                <div class="mt-5">
                                    <div class="service-table">
                                        <div class="table-responsive">
                                            <table class="table table-hover order-booking-view text-center">
                                                <thead>
                                                    <tr >
                                                        <th class="text-left" colspan="2">Services Details</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($order_services as $item) { ?>
                                                        <tr>
                                                            <th><b><?= $item->service_item->service_name ?> &times; (<?= $item->quantity ?>):</b></th>
                                                            <td><i class="fal fa-rupee-sign"></i> <?= number_format(($order->has_visit_and_quote == 'Yes') ? $item->visiting_charges : $item->sub_total, 2) ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div><!--service-table-->
                                    <div class="Pyment-details">
                                        <div class="table-responsive">
                                            <table class="table table-hover order-booking-view">
                                                <thead>
                                                    <tr >
                                                        <th class="text-left" colspan="2">Payment Details <span style="float: right; margin-right: 20px"><small><b>Payment Status : </b> <?= $show_payment_status ?></small></span></th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <?php if ($order->has_visit_and_quote == "No") { ?>
                                                        <tr>
                                                            <th><b style="float: right">Sub Total :</b></th>
                                                            <td><i class="fal fa-rupee-sign"></i> <?= number_format($order->sub_total, 2) ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                    <?php if (!empty($order->used_wallet_amount) && $order->used_wallet_amount != 0) { ?>
                                                        <tr>
                                                            <th><b style="float: right">Used Wallet Amount :</b></th>
                                                            <td style="color: tomato">- <i class="fal fa-rupee-sign"></i> <?= number_format($order->used_wallet_amount, 2) ?></td>
                                                        </tr>
                                                    <?php } ?>

                                                    <?php if (!empty($order->coupon_discount) && $order->coupon_discount != 0) { ?>
                                                        <tr>
                                                            <th><b style="float: right">Coupon Discount :</b></th>
                                                            <td style="color: tomato">- <i class="fal fa-rupee-sign"></i> <?= number_format($order->coupon_discount, 2) ?></td>
                                                        </tr>
                                                    <?php } ?>

                                                    <?php if (!empty($order->membership_discount) && $order->membership_discount != 0) { ?>
                                                        <tr>
                                                            <th><b style="float: right">Membership Discount :</b></th>
                                                            <td style="color: tomato">- <i class="fal fa-rupee-sign"></i> <?= number_format($order->membership_discount, 2) ?></td>
                                                        </tr>
                                                    <?php } ?>

                                                    <?php if (!empty($order->bussiness_discount) && $order->bussiness_discount != 0) { ?>
                                                        <tr>
                                                            <th><b style="float: right">Business Discount :</b></th>
                                                            <td style="color: tomato">- <i class="fal fa-rupee-sign"></i> <?= number_format($order->bussiness_discount, 2) ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                    <tr>
                                                        <th><b style="float: right">Safety & Visitation Fee :</b></th>
                                                        <td><i class="fal fa-rupee-sign"></i> <?= number_format($order->visiting_charges, 2) ?></td>
                                                    </tr>

                                                    <tr>
                                                        <th><b style="float: right">Tax :</b></th>
                                                        <td><i class="fal fa-rupee-sign"></i> <?= number_format($order->tax, 2) ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th><b style="float: right">Grand Total :</b></th>
                                                        <td style="color: green; font-weight: bold"><i class="fal fa-rupee-sign"></i> <?= number_format($order->grand_total, 2) ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th><b style="float: right">Amount Paid :</b></th>
                                                        <td><i class="fal fa-rupee-sign"></i> <?= number_format($order->amount_paid, 2) ?></td>
                                                    </tr>
                                                    <?php if ($order->balance_amount > 0) { ?>
                                                        <tr>
                                                            <th><b style="float: right">Balance Amount :</b></th>
                                                            <td><i class="fal fa-rupee-sign"></i> <?= number_format($order->balance_amount, 2) ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="row">
                                        <div class="col-md-12">
                                                <div class="submit-section">
                                                        <button type="reset" class="btn bg-danger-light"><i class="fal fa-times"></i> Cancel</button>
                                                </div>
                                        </div>
                                </div> -->
                            </div>
                        </div>
                    </div>

                    <div class="continue-shopping float-right">
                        <a class="btn" href="<?= base_url() ?>"><i class="fal fa-arrow-left"></i> Continue Shopping</a>
                    </div><!--continue-shopping-->
                </div>
            </div>
        </div>
    </section>
    <style>
        #sv_body {
            width: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .checkmark {
            width: 56px;
            height: 56px;
            border-radius: 50%;
            display: block;
            stroke-width: 2;
            stroke: #fff;
            stroke-miterlimit: 10;
            box-shadow: inset 0px 0px 0px #7ac142;
            animation: fill 0.4s ease-in-out 0.4s forwards, scale 0.3s ease-in-out 0.9s both;
        }

        .checkmark__circle {
            stroke-dasharray: 166;
            stroke-dashoffset: 166;
            stroke-width: 2;
            stroke-miterlimit: 10;
            stroke: #7ac142;
            fill: none;
            animation: stroke 0.6s cubic-bezier(0.65, 0, 0.45, 1) forwards;
        }

        .checkmark__check {
            transform-origin: 50% 50%;
            stroke-dasharray: 48;
            stroke-dashoffset: 48;
            animation: stroke 0.3s cubic-bezier(0.65, 0, 0.45, 1) 0.8s forwards;
        }

        @keyframes stroke {
            100% {
                stroke-dashoffset: 0;
            }
        }
        @keyframes scale {
            0%, 100% {
                transform: none;
            }
            50% {
                transform: scale3d(1.1, 1.1, 1);
            }
        }
        @keyframes fill {
            100% {
                box-shadow: inset 0px 0px 0px 30px #7ac142;
            }
        }
    </style>
<?php } else { ?>
    <section class="sptb">
        <div class="container">
            <div class="row d-flex justify-content-center">

                <div class="col-xl-12 col-lg-12 col-md-12">

                    <div class="card ">

                        <div class="card-body">
                            <div class="refer-friend-child">
                                <div id="sv_body">
                                    <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                        <circle class="checkmark_circle" cx="26" cy="26" r="25" fill="none" />
                                        <path class="checkmark_check" fill="none" d="M14.1 14.1l23.8 23.8 m0,-23.8 l-23.8,23.8" /></svg>    
                                </div>
                                <br>
                                    <p>Unable to Place Your Order</p>

                                    <div style="text-align: center">
                                        <hr>
                                            <p style="font-size: inherit; color: inherit; font-weight: normal">
                                                We are sorry to Inform you that we are Unable to process your Order with Id (<strong><?= $order_id ?></strong>) right now.<br><br>
                                                        If any deduction in amount, Please Contact Us with this following Reference Id:<br><br>
                                                                <strong><?= $rp_od_id ?>.</strong>
                                                                </p>
                                                                <br><br>
                                                                        <a style="cursor: pointer; color: #ff7088; font-weight: bold" href="<?= base_url('my-bookings') ?>" class="cart-btn-menu"> My Orders <i class="far fa-angle-right"></i></a>

                                                                        </div>

                                                                        </div>
                                                                        </div>
                                                                        </div>
                                                                        </div>
                                                                        </div>
                                                                        </div>
                                                                        </section>
                                                                        <style>
                                                                            #sv_body {
                                                                                width: 100%;
                                                                                display: flex;
                                                                                align-items: center;
                                                                                justify-content: center;
                                                                            }

                                                                            .checkmark {
                                                                                width: 56px;
                                                                                height: 56px;
                                                                                border-radius: 50%;
                                                                                display: block;
                                                                                stroke-width: 2;
                                                                                stroke: #fff;
                                                                                stroke-miterlimit: 10;
                                                                                box-shadow: inset 0px 0px 0px #ff4f4f;
                                                                                animation: fill 0.4s ease-in-out 0.4s forwards, scale 0.3s ease-in-out 0.9s both;
                                                                            }

                                                                            .checkmark__circle {
                                                                                stroke-dasharray: 166;
                                                                                stroke-dashoffset: 166;
                                                                                stroke-width: 2;
                                                                                stroke-miterlimit: 10;
                                                                                stroke: #ff4f4f;
                                                                                fill: none;
                                                                                animation: stroke 0.6s cubic-bezier(0.65, 0, 0.45, 1) forwards;
                                                                            }

                                                                            .checkmark__check {
                                                                                transform-origin: 50% 50%;
                                                                                stroke-dasharray: 48;
                                                                                stroke-dashoffset: 48;
                                                                                animation: stroke 0.3s cubic-bezier(0.65, 0, 0.45, 1) 0.8s forwards;
                                                                            }

                                                                            @keyframes stroke {
                                                                                100% {
                                                                                    stroke-dashoffset: 0;
                                                                                }
                                                                            }
                                                                            @keyframes scale {
                                                                                0%, 100% {
                                                                                    transform: none;
                                                                                }
                                                                                50% {
                                                                                    transform: scale3d(1.1, 1.1, 1);
                                                                                }
                                                                            }
                                                                            @keyframes fill {
                                                                                100% {
                                                                                    box-shadow: inset 0px 0px 0px 30px #ff4f4f;
                                                                                }
                                                                            }
                                                                        </style>
                                                                    <?php } ?>
