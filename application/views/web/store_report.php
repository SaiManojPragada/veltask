<?php include 'includes/ecom_header.php'; ?>
<div>
	<div class="cover-image sptb-1 bg-background" data-image-src="../assets/images/banners/banner1.jpg">
		<div class="header-text1 mb-0">
			<div class="container">
				<div class="row">
					<div class="col-xl-12 col-lg-12 col-md-12 d-block ">
						<div class=" breadcrumb-banner text-center text-white ">
						<h3>"Biryani" Found  </h3>
						</div>
						
				</div>
			</div>
		</div>
	</div>
	<!-- /header-text -->
</div>
</div>
<style>
	p.store-location {
overflow: hidden;
text-overflow: ellipsis;
white-space: nowrap;
width: 272px;
}
.prodduct-title {
    white-space: nowrap;
    line-height: 1.8;
}

.section_title.product_shop_title h2 {
    text-align: center;
    padding: 24px 10px;
    font-size: 26px;
    font-weight: 800;
}
</style>
<section class="storesnearbox">
<div class="container">
	<div class="row justify-content-center">
		<div class="col-lg-10 col-md-12">
			<div class="section_title product_shop_title">
				<h2>Shops <span>( 5 )</span></h2>
			</div>
			
			<!-- <h3>Shops ( 5 )</h3> -->
			<div class="row">
				<div class="col-lg-4 col-md-4">
					<div class="card shadow-sm mb-4">
						<div class="img-box">
							<a href="#"><img src="https://retos.in/uploads/shops/202110051503078174499_alfa_biryani.jpg" alt="" class="card-img-top"></a>
							<!-- <h5><i class="fal fa-badge-percent"></i> DEALS</h5> -->
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-lg-12">
									<h4 class="prodduct-title"><a href="#">Alfa Biryani</a></h4>
									<p></p>
									<p class="store-location"><i class="fal fa-map"></i> Waltair main road - Visakhapatnam...</p>
								</div>
							</div>
						</div>
						<div class="card-footer">
							<div class="row">
								<div class="col-lg-6"><p><small><i class="fal fa-map-marker-alt"></i> 2Km</small></p></div>
								<div class="col-lg-6 text-right"><p class="pinkcol"><small>49 Products</small></p></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4">
					<div class="card shadow-sm mb-4">
						<div class="img-box">
							<a href="#"><img src="https://retos.in/uploads/shops/202203151104426219785_ayyagari_biryani.jpg" alt="" class="card-img-top"></a>
							<!-- <h5><i class="fal fa-badge-percent"></i> DEALS</h5> -->
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-lg-12">
									<h4 class="prodduct-title"><a href="#">Ayyagari Biryani</a></h4>
									<p></p>
									<p class="store-location"><i class="fal fa-map"></i> MVP Colony - Visakhapatnam...</p>
								</div>
							</div>
						</div>
						<div class="card-footer">
							<div class="row">
								<div class="col-lg-6"><p><small><i class="fal fa-map-marker-alt"></i> 2Km</small></p></div>
								<div class="col-lg-6 text-right"><p class="pinkcol"><small>15 Products</small></p></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4">
					<div class="card shadow-sm mb-4">
						<div class="img-box">
							<a href="#"><img src="https://retos.in/uploads/shops/20220404165221994549_72_biryanis.jpg" alt="" class="card-img-top"></a>
							<!-- <h5><i class="fal fa-badge-percent"></i> DEALS</h5> -->
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-lg-12">
									<h4><a href="#">72 Biryanis</a></h4>
									<p></p>
									<p class="store-location"><i class="fal fa-map"></i> MVP Colony - Visakhapatnam ...</p>
								</div>
							</div>
						</div>
						<div class="card-footer">
							<div class="row">
								<div class="col-lg-6"><p><small><i class="fal fa-map-marker-alt"></i> 3Km</small></p></div>
								<div class="col-lg-6 text-right"><p class="pinkcol"><small>101 Products</small></p></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4">
					<div class="card shadow-sm mb-4">
						<div class="img-box">
							<a href="#"><img src="https://retos.in/uploads/shops/202201071720094422760_thanuja_kitchen.jpg" alt="" class="card-img-top"></a>
							<!-- <h5><i class="fal fa-badge-percent"></i> DEALS</h5> -->
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-lg-12">
									<h4 class="prodduct-title"><a href="#">Bucket Biryani (Thanuja Kitchen)</a></h4>
									<p></p>
									<p class="store-location"> <i class="fal fa-map"></i> Madhavdhra - Visakhapatnam...</p>
								</div>
							</div>
						</div>
						<div class="card-footer">
							<div class="row">
								<div class="col-lg-6"><p><small><i class="fal fa-map-marker-alt"></i> 6Km</small></p></div>
								<div class="col-lg-6 text-right"><p class="pinkcol"><small>172 Products</small></p></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4">
					<div class="card shadow-sm mb-4">
						<div class="img-box">
							<a href="#"><img src="https://retos.in/uploads/shops/202204181216249447622_one_tap_khan_dum_biryani.jpg" alt="" class="card-img-top"></a>
							<!-- <h5><i class="fal fa-badge-percent"></i> DEALS</h5> -->
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-lg-12">
									<h4 class="prodduct-title"><a href="#">One Tap Khan Dum Biryani</a></h4>
									<p></p>
									<p class="store-location"><i class="fal fa-map"></i> Marripalem - Visakhapatnam...</p>
								</div>
							</div>
						</div>
						<div class="card-footer">
							<div class="row">
								<div class="col-lg-6"><p><small><i class="fal fa-map-marker-alt"></i> 6Km</small></p></div>
								<div class="col-lg-6 text-right"><p class="pinkcol"><small>55 Products</small></p></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</section>
<div class="shop_area mb-80">
<div class="container">
	<div class="row justify-content-center">
		<div class="col-lg-10 col-md-12">
			<div id="fav_msg" style="text-align: center;"></div>
			<div class="section_title product_shop_title">
				<h2>Products <span>( 5 )</span></h2>
			</div>
			
			<div class="row shop_wrapper" id="products_list">
				<div class="col-md-3 mb-4">
					<div class="item shop-item-cat text-center">
						<a href="https://veltask.com/web/product_view/59_mens-salwar">
							<img src="https://veltask.com/uploads/products/202203072003085156171_sel.jpg" alt="">
						</a>
						<div class="popular-products">
							<h2 class="mb-0"><a href="https://veltask.com/web/product_view/59_mens-salwar">MEN’S SALWAR</a> </h2>
							
							<span><a href="product-view.php">Revathimaheshwar Stores</a></span>
							<div class="price">899</div>
							<a onclick="addtocart(52487,42,'899',1)" class="btn btn-primary btn-ptill mb-3"><i class="fal fa-shopping-cart"></i> Add to Cart</a>
						</div>
					</div>
				</div>
				<div class="col-md-3 mb-4">
					<div class="item shop-item-cat text-center">
						<a href="https://veltask.com/web/product_view/59_mens-salwar">
							<img src="https://veltask.com/uploads/products/202203072003085156171_sel.jpg" alt="">
						</a>
						<div class="popular-products">
							<h2 class="mb-0"><a href="https://veltask.com/web/product_view/59_mens-salwar">MEN’S SALWAR</a> </h2>
							
							<span><a href="product-view.php">Revathimaheshwar Stores</a></span>
							<div class="price">899</div>
							<a onclick="addtocart(52487,42,'899',1)" class="btn btn-primary btn-ptill mb-3"><i class="fal fa-shopping-cart"></i> Add to Cart</a>
						</div>
					</div>
				</div>
				<div class="col-md-3 mb-4">
					<div class="item shop-item-cat text-center">
						<a href="https://veltask.com/web/product_view/59_mens-salwar">
							<img src="https://veltask.com/uploads/products/202203072003085156171_sel.jpg" alt="">
						</a>
						<div class="popular-products">
							<h2 class="mb-0"><a href="https://veltask.com/web/product_view/59_mens-salwar">MEN’S SALWAR</a> </h2>
							
							<span><a href="product-view.php">Revathimaheshwar Stores</a></span>
							<div class="price">899</div>
							<a onclick="addtocart(52487,42,'899',1)" class="btn btn-primary btn-ptill mb-3"><i class="fal fa-shopping-cart"></i> Add to Cart</a>
						</div>
					</div>
				</div>
				<div class="col-md-3 mb-4">
					<div class="item shop-item-cat text-center">
						<a href="https://veltask.com/web/product_view/59_mens-salwar">
							<img src="https://veltask.com/uploads/products/202203072003085156171_sel.jpg" alt="">
						</a>
						<div class="popular-products">
							<h2 class="mb-0"><a href="https://veltask.com/web/product_view/59_mens-salwar">MEN’S SALWAR</a> </h2>
							
							<span><a href="product-view.php">Revathimaheshwar Stores</a></span>
							<div class="price">899</div>
							<a onclick="addtocart(52487,42,'899',1)" class="btn btn-primary btn-ptill mb-3"><i class="fal fa-shopping-cart"></i> Add to Cart</a>
						</div>
					</div>
				</div>
			</div>
			
		</div>
		
		
	</div>
</div>
</div>
<section class="sptb ">
<div class="container">
	
	<div class="row justify-content-center">
		<div class="col-lg-10 col-md-12">
			<div id="fav_msg" style="text-align: center;"></div>
			<div class="section_title product_shop_title">
				<h2>Our Services<span>( 4 )</span></h2>
			</div>
			
			<div class="row shop_wrapper" id="products_list">
				<div class="col-md-3 mb-4">
					<div class="item shop-item-cat text-center">
						<a href="https://veltask.com/web/product_view/59_mens-salwar">
							<img src="https://veltask.com/uploads/services/ff6222b0356a544d8889f2a25583b090.jpg" alt="">
						</a>
						<div class="popular-products">
							<h2 class="mb-0"><a href="https://veltask.com/web/product_view/59_mens-salwar">Car Auto Diagnostics</a> </h2>
							
							<span><a href="product-view.php">Revathimaheshwar Stores</a></span>
							<div class="price">899</div>
							<a onclick="addtocart(52487,42,'899',1)" class="btn btn-primary btn-ptill mb-3"><i class="fal fa-shopping-cart"></i> Add to Cart</a>
						</div>
					</div>
				</div>
				<div class="col-md-3 mb-4">
					<div class="item shop-item-cat text-center">
						<a href="https://veltask.com/web/product_view/59_mens-salwar">
							<img src="https://veltask.com/uploads/services/ff6222b0356a544d8889f2a25583b090.jpg" alt="">
						</a>
						<div class="popular-products">
							<h2 class="mb-0"><a href="https://veltask.com/web/product_view/59_mens-salwar">Car Auto Diagnostics</a> </h2>
							
							<span><a href="product-view.php">Revathimaheshwar Stores</a></span>
							<div class="price">899</div>
							<a onclick="addtocart(52487,42,'899',1)" class="btn btn-primary btn-ptill mb-3"><i class="fal fa-shopping-cart"></i> Add to Cart</a>
						</div>
					</div>
				</div>
				<div class="col-md-3 mb-4">
					<div class="item shop-item-cat text-center">
						<a href="https://veltask.com/web/product_view/59_mens-salwar">
							<img src="https://veltask.com/uploads/services/ff6222b0356a544d8889f2a25583b090.jpg" alt="">
						</a>
						<div class="popular-products">
							<h2 class="mb-0"><a href="https://veltask.com/web/product_view/59_mens-salwar">Car Auto Diagnostics</a> </h2>
							
							<span><a href="product-view.php">Revathimaheshwar Stores</a></span>
							<div class="price">899</div>
							<a onclick="addtocart(52487,42,'899',1)" class="btn btn-primary btn-ptill mb-3"><i class="fal fa-shopping-cart"></i> Add to Cart</a>
						</div>
					</div>
				</div>
				<div class="col-md-3 mb-4">
					<div class="item shop-item-cat text-center">
						<a href="https://veltask.com/web/product_view/59_mens-salwar">
							<img src="https://veltask.com/uploads/services/ff6222b0356a544d8889f2a25583b090.jpg" alt="">
						</a>
						<div class="popular-products">
							<h2 class="mb-0"><a href="https://veltask.com/web/product_view/59_mens-salwar">Car Auto Diagnostics</a> </h2>
							
							<span><a href="product-view.php">Revathimaheshwar Stores</a></span>
							<div class="price">899</div>
							<a onclick="addtocart(52487,42,'899',1)" class="btn btn-primary btn-ptill mb-3"><i class="fal fa-shopping-cart"></i> Add to Cart</a>
						</div>
					</div>
				</div>
			</div>
			
		</div>
		
		
	</div>
	<div class="row collapse justify-content-md-center" id="extra-cat-services">
		<div class="col-xl-2 col-lg-2 col-md-3 col-6" style="cursor: pointer" onclick="location.href = 'https://veltask.com/services/cleaning-services/'">
			<div class="card bg-card-white">
				<div class="card-body">
					<div class="cat-item text-center">
						<div class="cat-img">
							<img src="https://veltask.com/uploads/services/ff6222b0356a544d8889f2a25583b090.jpg" alt="Cleaning Services">
						</div>
						<div class="cat-desc">
							<h5 class="mb-1">Car Auto Diagnostics</h5>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xl-2 col-lg-2 col-md-3 col-6" style="cursor: pointer" onclick="location.href = 'https://veltask.com/services/cleaning-services/'">
			<div class="card bg-card-white">
				<div class="card-body">
					<div class="cat-item text-center">
						<div class="cat-img">
							<img src="https://veltask.com/uploads/services/ff6222b0356a544d8889f2a25583b090.jpg" alt="Cleaning Services">
						</div>
						<div class="cat-desc">
							<h5 class="mb-1">Car Auto Diagnostics</h5>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
</section>
<?php include ('includes/footer.php'); ?>