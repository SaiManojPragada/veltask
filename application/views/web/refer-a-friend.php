<!--Breadcrumb-->
<section>
    <div class="bannerimg cover-image bg-background3" data-image-src="../assets/images/banners/banner2.jpg">
        <div class="header-text mb-0">
            <div class="container ">
                <div class=" text-white text-center">
                    <h1 class="">Refer a Friend</h1>
                    <ol class="breadcrumb ">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active text-white" aria-current="page">Refer a Friend</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Breadcrumb-->

<?php if (!empty(USER_ID)) { ?>
    <!--User Dashboard-->
    <section class="sptb">
        <div class="container">
            <div class="row">
                <?php include 'dashboard-left-menu.php'; ?>
                <div class="col-xl-9 col-lg-12 col-md-12">


                    <div class="card ">
                        <div class="card-header refer-parent p-3">
                            <h5>Refer a friend</h5>
                        </div>
                        <div class="card-body">
                            <div class="refer-friend-child">
                                <img src="<?= base_url('web_assets/') ?>assets/images/refer-coupan.jpg" alt="">
                                <p>Refer & Earn</p>
                                <p>Refer a friend</p>
                                <p><?= $my_code ?></p>
                                <!--<button data-toggle="modal" data-target="#Mymodal" class="btn btn-primary">Click Here</button>-->
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover mb-0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Refer Name</th>
                                            <th>Email</th>
                                            <th class="text-center">Referral Code</th>
                                            <th class="text-center">Joined On</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($my_referrals as $ref) { ?>
                                            <tr>
                                                <td><?= $index + 1 ?></td>
                                                <td><?= ($ref->first_name) ? $ref->first_name : 'N/A' ?></td>
                                                <td><?= ($ref->email) ? $ref->email : 'N/A' ?></td>
                                                <td class="text-center"> <?= $ref->referral_code ?></td>
                                                <td><?php
                                                    $date = strtotime($ref->created_date);
                                                    echo date('d M Y, h:i A', $date);
                                                    ?></td>
                                            </tr>

                                        <?php } ?>
                                        <?php if (empty($my_referrals)) { ?>
                                            <tr>
                                                <td colspan="5">
                                                    <p style="text-align: center"> No Data Found</p>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="Mymodal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class=" modal-header d-flex">
                    <span class="refer-header">Refer Your Friend</span>
                    <button type="button" class="close" data-dismiss="modal">
                        &times;
                    </button>                                              
                </div> 
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="text">Name</label>
                            <input type="name" class="form-control" id="name" placeholder="Enter Your Name">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">

                        </div>

                        <div class="form-group">
                            <label for="text">Phone</label>
                            <input type="name" class="form-control" id="name" placeholder="Enter Your Number">
                        </div>

                        <div class="form-group">
                            <label for="text">Refer Id</label>
                            <input type="name" class="form-control" id="name" placeholder="Enter Your Refer Id">
                        </div>

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>   

            </div>                                                                       
        </div>                                      
    </div>
<?php } else { ?>
    <div class="container-fluid" style="min-height: 400px">
        <h3 style="text-align: center; margin-top: 160px"> Please Login to Continue </h3>
    </div>
<?php } ?>