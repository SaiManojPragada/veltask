<?php $this->load->view("web/includes/header"); ?>

<!--product details start-->

<div class="product_details">

    <div class="container">

        <div class="row">

            <div id="show_errormsg1"></div>







            <?php //echo "<pre>"; print_r($product_details['attributes']); ?>

            <div class="col-lg-4 col-md-5">

                <div class="product-details-tab">

                    <section id="default" class="padding-top0">
                        <div class="row">
                            <div class="large-12 column"><h3>Default options</h3></div>
                            <div class="large-5 column">
                                <div class="xzoom-container">
                                    <img class="xzoom" id="xzoom-default" src="<?php echo $product_details['link_variants'][0]['imageslist'][0]['image']; ?>" xoriginal="<?php echo $product_details['link_variants'][0]['imageslist'][0]['image']; ?>" />


                                    <div class="xzoom-thumbs">
                                        <?php foreach ($product_details['link_variants'][0]['imageslist'] as $images) { ?>

                                            <a href="<?php echo $images['image']; ?>"><img class="xzoom-gallery" width="80" src="<?php echo $images['image']; ?>"  xpreview="<?php echo $images['image']; ?>" title="The description goes here"></a>

                                        <?php } ?>

                                    </div>
                                </div>        
                            </div>
                            <div class="large-7 column"></div>
                        </div>
                    </section>



                </div>

            </div>

            <div class="col-lg-8 col-md-7">

                <div class="product_d_right">

                    <!-- <div class="label_product">

                                    <a href="#"><span class="label_wishlist"><i class="fal fa-heart"></i></span></a>

                                </div> -->

                    <div class="productd_title_nav">

                        <h1><a href="#"><?php echo $product_details['name']; ?></a></h1>


                        <button onclick="goBack()" class="btn btn-success">Back</button>

                        <script>

                            function goBack() {

                                window.history.back();

                            }

                        </script>

                    </div>

                    <!-- <p><small>(6 GB RAM)</small></p> -->

                    <div class="price_box">

                        <span class="current_price"><i class="fal fa-rupee-sign"></i> <?php echo $product_details['link_variants'][0]['saleprice']; ?></span>

                        <span class="old_price"><small><i class="fal fa-rupee-sign"></i> <?php echo $product_details['link_variants'][0]['price']; ?></small></span>

                        <?php
                        $discount = (($product_details['link_variants'][0]['price'] - $product_details['link_variants'][0]['saleprice']) * 100) / $product_details['link_variants'][0]['price'];
                        ?>

                        <span class="text-success"><small><?php echo number_format($discount, 2); ?>% OFF</small></span>

                    </div>

                    <!-- <div class="price_box"><span class="text-success"><small>Extra ₹3000 OFF</small></span></div> -->

                    <div class="product_desc">

                        <p class="shopName"><b>Shop : </b><a href="<?php echo base_url(); ?>web/store/<?php echo $product_details['seo_url']; ?>/shop"><?php echo $product_details['shop']; ?></a>( <?php echo $product_details['vendor_description']; ?> )</p> 

                        <p><?php echo $product_details['category_name']; ?> > <?php echo $product_details['subcategory_name']; ?></p>

                        <p><b>Brand : </b><?php echo $product_details['brand_id']; ?></p>



                        <p><b>Description : </b><?php echo $product_details['description']; ?></p>



                        <p><b>Return Availability : </b><?php echo $product_details['return_status']; ?></p>

                    </div>







<?php if (count($product_details['attributes']) > 0) { ?>

                        <div class="row">

                            <div class="col-lg-12">

                        <?php if (!empty($this->session->flashdata('success_message'))) { ?>

                                    <div class="btn btn-danger alert-success" ><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>

                                        <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>

                                    </div>

    <?php } ?>

                                <form method="post" action="<?php echo base_url(); ?>web/productfilters/<?php echo $product_id; ?>" class="form-horizontal" enctype="multipart/form-data" >

                                    <input type="hidden" name="product_id" value="<?php echo $product_id; ?>">

                                    <input type="hidden" name="seo_url" value="<?php echo $seo_url; ?>">



                                    <input type="hidden" name="total_count" value="<?php echo count($product_details['attributes']); ?>">

    <?php
    $attribute_values = $product_details['attributes'];

    $selected_linkvarinats = $linkvarinats;

    $i = 0;

    foreach ($attribute_values as $values) {





        $type_id = $values['id'];
        ?>

                                        <div class="product_variant color">

                                            <h3>Available <?php echo $values['attribute_type']; ?>

                                                <label><input type="hidden" name="attribute_type<?php echo $i; ?>" value="<?php echo $values['id']; ?>"></label>      





                                            </h3>

                                            <label></label>

                                            <ul>

        <?php
        $aatribut_values = $values['attribute_values'];

        foreach ($aatribut_values as $values_list) {

            //echo  $values_list['id']."-".$selected_linkvarinats[$i]->attribute_value;
            ?>



                                                    <li style="padding: 5px;" ><label><input type="radio" <?php if ($values_list['id'] == $selected_linkvarinats[$i]->attribute_value) {
                echo "checked='checked'";
            } ?> name="attribute_value<?php echo $i; ?>" value="<?php echo $values_list['id']; ?>"><?php echo $values_list['value']; ?></label></li>

                                                <?php } ?>



                                            </ul>

                                        </div>



        <?php $i++;
    } ?> 

                                    <button type="submit" class="button select-bnt"> Select</button>

                                </form>



                            </div>











                        </div>

<?php } ?>



                    <div class="row">

                        <div class="product_variant quantity">

                            <!--  <label>quantity</label> -->

                            <a  onclick="decreaseQty()">➖</a>

                            <input min="1" max="100" id="quantity" value="<?php echo $product_qry; ?>" type="text" readonly="">

                            <a onclick="increaseQty()">➕</a>

                            <button class="button" type="submit" onclick="addtocartproductview(<?php echo $product_details['link_variants'][0]['id']; ?>,<?php echo $product_details['shop_id']; ?>, '<?php echo $product_details['link_variants'][0]['saleprice']; ?>')"><i class="fal fa-shopping-cart"></i> add to shoppy</button>



                            <a onclick="addFavorite(<?php echo $product_details['id']; ?>)" title="Add to Wishlist" class="btn blue-btn">

                                <span  id="product_view" class="<?php if ($product_details['whishlist_status'] == true) {
    echo 'fas';
} else {
    echo 'fal';
} ?> fa-heart"></span>

                            </a>

                        </div>

                    </div>



                    <div class="row">

                        <div class="col-lg-12 col-md-12">

                            <div class="product_variant color">

                                <h3>Highlights</h3>

                                <ul class="highlights">

                                    <li><?php echo $product_details['key_features']; ?></li>

                                </ul>



                                <p ><b style="color:#f15d74">Image Disclaimer:</b> The product images shown may represent the range of product, or be for illustration purposes only and may not be an exact</p>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

<!--product details end-->

<script type="text/javascript" src="<?= base_url('web_assets/') ?>js/xzoom.min.js"></script>

<link rel="stylesheet" type="text/css" href="<?= base_url('web_assets/') ?>js/xzoom.css" media="all" /> 
<script src="<?= base_url('web_assets/') ?>js/setup.js"></script>
<script src="<?= base_url('web_assets/') ?>js/jquery.js"></script>
<style type="text/css">



    .active_values{

        background-color: #cf1673;

        color: #ffffff;

    }

</style>



<script type="text/javascript">



                            function addFavorite(pid)

                            {

                                var user_id = '<?php echo $_SESSION['userdata']['user_id']; ?>';

                                if (user_id == '')

                                {

                                    $('#loginModal').modal('show');

                                    return false;

                                } else

                                {



                                    $('.error').remove();

                                    var errr = 0;

                                    $.ajax({

                                        url: "<?php echo base_url(); ?>web/add_remove_favourite",

                                        method: "POST",

                                        data: {pid: pid},

                                        success: function (data)

                                        {

                                            var str = data;

                                            var res = str.split("@");

                                            if (res[1] == 'remove')

                                            {

                                                $("#product_view").removeClass("fas");

                                                $("#product_view").addClass("fal");

                                            } else if (res[1] == 'add')

                                            {



                                                $("#product_view").removeClass("fal");

                                                $("#product_view").addClass("fas");

                                            }







                                        }

                                    });

                                }

                            }





                            setTimeout(function () {

                                $('.alert-success').fadeOut('fast');

                            }, 5000);

</script>

<!--product area start-->

<section class="product_area related_products mt-4">

</section>

<!-- <section class="product_area related_products mt-4">

    <div class="container">

        <div class="row">

            <div class="col-12">

                <div class="section_title psec_title">

                    <h2>Similar <span>Products</span></h2>

                </div>

            </div>

        </div>

        <div class="row">

            <div class="product_carousel product_column5 owl-carousel">

<?php for ($i = 1; $i <= 8; $i++) { ?>

                        <div class="col-lg-3 col-md-4 col-sm-6 col-12">

                            <div class="single_product">

                                <div class="product_thumb">

                                    <a class="primary_img" href="product_view.php"><img src="assets/img/op-<?php echo $i ?>.jpg" alt=""></a>

                                    <a class="secondary_img" href="product_view.php"><img src="assets/img/op-<?php echo $i ?>a.jpg" alt=""></a>

                                    <div class="label_product">

                                        <a href="#"><span class="label_wishlist"><i class="fal fa-heart"></i></span></a>

                                    </div>

                                    <div class="action_links">

                                        <ul>

                                            <li class="quick_button"><a href="product_view.php" title="View Details"> <span class="pe-7s-search"></span></a></li>

                                            <li class="wishlist"><a href="#" title="Add to Wishlist"><span class="pe-7s-like"></span></a></li>

                                        </ul>

                                    </div>

                                </div>

                                <div class="product_content grid_content">

                                    <div class="product_content_inner">

                                        <h4 class="product_name"><a href="product_view.php">Smart Phone Title</a></h4>

                                        <div class="price_box">

                                            <span class="current_price"><i class="fal fa-rupee-sign"></i> 11000</span>

                                        </div>

                                    </div>

                                    <div class="add_to_cart">

                                        <a href="cart.php"><i class="fal fa-shopping-cart fa-lg"></i> Add to cart</a>

                                    </div>

                                </div>

                                

                            </div>

                        </div>

<?php } ?>

                

            </div>

        </div>

    </div>

</section> -->



<script type="text/javascript">

    function increaseQty()

    {

        var quantity = $("#quantity").val();

        var qty = 1;

        var final = parseInt(qty) + parseInt(quantity);

        $("#quantity").val(final);

    }



    function decreaseQty()

    {

        var quantity = $("#quantity").val();

        if (quantity == 1)

        {

            return false;

        } else

        {

            var qty = 1;

            var final = parseInt(quantity) - parseInt(qty);

            $("#quantity").val(final);

        }

    }







    /*function addtocart(variant_id,vendor_id,saleprice)
     
     {
     
     var user_id = '<?php echo $_SESSION['userdata']['user_id']; ?>';
     
     if(user_id=='')
     
     {
     
     $('#loginModal').modal('show');
     
     return false;
     
     }
     
     else
     
     {
     
     var quantity = $("#quantity").val();
     
     var session_vendor_id = '<?php echo $_SESSION['session_data']['vendor_id']; ?>';
     
     var cart_count= '<?php echo $cart_count; ?>';
     
     if(session_vendor_id!=vendor_id && cart_count>0)
     
     {
     
     
     
     if(confirm("Are you sure you want to Clear the previous store items"))
     
     {
     
     
     
     $('.error').remove();
     
     var errr=0;
     
     
     
     $.ajax({
     
     url:"<?php echo base_url(); ?>web/addtocart",
     
     method:"POST",
     
     data:{variant_id:variant_id,vendor_id:vendor_id,saleprice:saleprice,quantity:quantity},
     
     success:function(data)
     
     {
     
     var str = data;
     
     var res = str.split("@");
     
     if(res[1]=='success')
     
     {
     
     
     
     $('html, body').animate({
     
     scrollTop: $('#show_errormsg1').offset().top - 100 //#DIV_ID is an example. Use the id of your destination on the page
     
     }, 'slow');
     
     $('#cart_count').html(res[2]);
     
     location.reload();
     
     $('#show_errormsg').html('<span class="error" style="color:green;font-size: 16px;margin-left: 18px; width:100%">Product added to cart</span>');
     
     $('#show_errormsg').focus();
     
     return false;
     
     }
     
     else
     
     {
     
     $('#show_errormsg').html('<span class="error" style="color:red;font-size: 16px;margin-left: 18px; width:100%">OUT OF STOCK</span>');
     
     $('#show_errormsg').focus();
     
     return false;
     
     }
     
     
     
     
     
     
     
     }
     
     });
     
     }
     
     }
     
     else
     
     {
     
     
     
     
     
     $('.error').remove();
     
     var errr=0;
     
     
     
     $.ajax({
     
     url:"<?php echo base_url(); ?>web/addtocart",
     
     method:"POST",
     
     data:{variant_id:variant_id,vendor_id:vendor_id,saleprice:saleprice,quantity:quantity},
     
     success:function(data)
     
     {
     
     var str = data;
     
     var res = str.split("@");
     
     if(res[1]=='success')
     
     {
     
     
     
     $('html, body').animate({
     
     scrollTop: $('#show_errormsg1').offset().top - 100 //#DIV_ID is an example. Use the id of your destination on the page
     
     }, 'slow');
     
     location.reload();
     
     $('#cart_count').html(res[2]);
     
     $('#show_errormsg').html('<span class="error" style="color:green;font-size: 16px;margin-left: 18px; width:100%">Product added to cart</span>');
     
     $('#show_errormsg').focus();
     
     return false;
     
     }
     
     else
     
     {
     
     $('#show_errormsg').html('<span class="error" style="color:red;font-size: 16px;margin-left: 18px; width:100%">OUT OF STOCK</span>');
     
     $('#show_errormsg').focus();
     
     return false;
     
     }
     
     
     
     
     
     
     
     }
     
     });
     
     
     
     }
     
     }
     
     }*/

</script>





<script type="text/javascript">

    function confirmnewCart(variant_id, vendor_id, saleprice, quantity) {

        var id = $(this).parents("tr").attr("id");



        swal({

            title: "Are you sure?",

            text: "You want to Clear the previous store items",

            type: "warning",

            showCancelButton: true,

            confirmButtonClass: "btn-danger",

            confirmButtonText: "Yes",

            cancelButtonText: "Cancel",

            closeOnConfirm: false,

            closeOnCancel: false



        },
                function (isConfirm) {



                    if (isConfirm) {



                        $.ajax({

                            url: "<?php echo base_url(); ?>web/addtocart",

                            method: "POST",

                            data: {variant_id: variant_id, vendor_id: vendor_id, saleprice: saleprice, quantity: quantity},

                            success: function (data)

                            {

                                var str = data;

                                var res = str.split("@");

                                if (res[1] == 'success')

                                {

                                    $("#vendor_id").val(vendor_id);

                                    $("#session_id").val(res[3]);

                                    $('#cart_count').html(res[2]);



                                    swal("Product added to cart!")

                                } else if (res[1] == 'shopclosed')

                                {



                                    swal("Shop Closed!")

                                } else

                                {

                                    swal("OUT OF STOCK!")

                                }







                            }

                        });





                    } else {



                        swal("Cancelled", "Cancelled", "error");



                    }



                });







    }







    function addtocartproductview(variant_id, vendor_id, saleprice)

    {

        var quantity = $("#quantity").val();

        var session_vendor_id = $("#vendor_id").val();

        //alert(session_vendor_id);

        var session_id = $("#session_id").val();

        var user_id = '<?php echo $_SESSION['userdata']['user_id']; ?>';





        if (user_id == '')

        {

            $("#login_quantity").val(quantity);

            $("#login_vendor_id").val(vendor_id);

            $("#login_session_id").val(session_id);



            $("#login_variant_id").val(variant_id);

            $("#login_saleprice").val(saleprice);



            $('#loginModal').modal('show');

            return false;

        } else

        {

            //alert(session_vendor_id); alert(vendor_id);

            if (session_vendor_id != vendor_id && session_vendor_id != '')

            {

                confirmnewCart(variant_id, vendor_id, saleprice, quantity);

            } else

            {

                $('.error').remove();

                var errr = 0;



                $.ajax({

                    url: "<?php echo base_url(); ?>web/addtocart",

                    method: "POST",

                    data: {variant_id: variant_id, vendor_id: vendor_id, saleprice: saleprice, quantity: quantity, session_id: session_id},

                    success: function (data)

                    {

                        var str = data;

                        var res = str.split("@");

                        if (res[1] == 'success')

                        {

                            $("#vendor_id").val(vendor_id);

                            $("#session_id").val(res[3]);

                            $('#cart_count').html(res[2]);



                            swal("Product added to cart!")

                        } else if (res[1] == 'shopclosed')

                        {



                            swal("Shop Closed!")

                        } else

                        {

                            swal("OUT OF STOCK!")

                        }

                    }

                });



            }

        }

    }

</script>

<!--product area end-->

<?php $this->load->view("web/includes/footer"); ?>