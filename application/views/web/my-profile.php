<!--Breadcrumb-->
<section>
    <div class="bannerimg cover-image bg-background3" data-image-src="../assets/images/banners/banner2.jpg">
        <div class="header-text mb-0">
            <div class="container">
                <div class="text-center text-white">
                    <h1 class="">My Profile</h1>
                    <ol class="breadcrumb text-center">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active text-white" aria-current="page">My Profile</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Breadcrumb-->
<?php if (!empty(USER_ID)) { ?>
    <!--User Dashboard-->
    <section class="sptb">
        <div class="container">
            <div class="row">
                <?php include 'dashboard-left-menu.php'; ?>
                <div class="col-xl-9 col-lg-12 col-md-12">
                    <div class="card">
                        <div class="card-header refer-parent p-3">
                            <h5>My Profile</h5>
                        </div>
                        <div class="card-body">



                            <form method="post" action="" id="profile-form">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Name</label>
                                    <input type="name" class="form-control" id="name" name="first_name" aria-describedby="name" placeholder="Enter Name" value="<?= $user_data->first_name ?>"
                                           minlength="3" data-parsley-pattern="^[a-zA-Z\s]*$" data-parsley-pattern-message="Please Enter Valid Name" data-parsley-required-message="Please Enter Valid Name" required>

                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email address</label>
                                    <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" value="<?= $user_data->email ?>" required
                                           data-parsley-type-message="Please Enter Valid Email" data-parsley-required-message="Please Enter Valid Email" data-parsley-trigger="change" class="form-control half-wdth-field">

                                </div>
                                <div class="form-group">
                                    <label for="text">Mobile Number</label>
                                    <input type="text" class="form-control" name="phone" value="<?= $user_data->phone ?>" required min='1' data-parsley-minlength="10"  data-parsley-maxlength="10" data-parsley-required-message="Please Enter Phone Number" data-parsley-minlength-message="Enter Valid Phone Number *" data-parsley-maxlength-message="Enter Valid Phone Number" required>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label for="exampleFormControlTextarea1">State</label>
                                            <select name="state_id" id="states" class="form-control" onchange="getCities(this.value);" data-parsley-required-message="Please Select a State" required>
                                                <option value="">-- Select State --</option>
                                                <?php foreach ($states as $state) { ?>
                                                    <option value="<?= $state->id ?>" <?= ($state->id == $user_data->state_id) ? 'selected' : '' ?>><?= $state->state_name ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col">
                                            <label for="exampleFormControlTextarea1">City</label>
                                            <select name="city_id" id="cities" class="form-control" data-parsley-required-message="Please Select a City" required>
                                                <option value="">-- Select City --</option>
                                                <?php foreach ($cities as $city) { ?>
                                                    <option value="<?= $city->id ?>" <?= ($city->id == $user_data->city_id) ? 'selected' : '' ?>><?= $city->city_name ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="text">Pincode</label>
                                    <input type="text" class="form-control" name="pincode" required onkeydown='return (event.which >= 48 && event.which <= 57) || event.which == 8 || event.which == 46 || (event.which >= 96 && event.which <= 105)' value="<?= $user_data->pincode ?>"  data-parsley-required-message="Please Enter Your Pincode" >
                                </div>
                                <button class="btn my-custom-btn" type="submit" style="float: right">Update</button>




                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--Parsley Pulgin-->
    <script src="<?= base_url('web_assets/') ?>/js/plugins/parsleyjs/dist/parsley.min.js"></script>
    <script type="text/javascript">
                                        $(document).ready(function () {
                                            $('#profile-form').parsley();
                                        });
    </script>
    <script>
        function getCities(val) {
            $.ajax({
                url: "<?= base_url() ?>website/getCities",
                type: "post",
                data: {state_id: val},
                success: function (resp) {
                    console.log(resp);
                    $("#cities").html(resp);
                }
            });
        }
    </script>
<?php } else { ?>
    <div class="container-fluid" style="min-height: 400px">
        <h3 style="text-align: center; margin-top: 160px"> Please Login to Continue </h3>
    </div>
<?php } ?>
<!--/User Dashboard-->

<!-- Newsletter-->

<!--/Newsletter-->