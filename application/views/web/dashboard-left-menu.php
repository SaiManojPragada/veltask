<?php $page = basename($_SERVER['SCRIPT_NAME']); ?>
<div class="col-xl-3 col-lg-12 col-md-12">
    <div class="card">
        <div class="card-header p-4 text-center w-100">
            <h3 class="card-title ">My Dashboard</h3>
        </div>
        <div class="card-body text-center item-user border-bottom">
            <div class="profile-pic">
                <div class="widget-profile pro-widget-content ">
                    <div class="profile-info-widget justify-content-center">
                        <a href="#" class="booking-doc-img">
                            <?php if (empty(USER_IMAGE)) { ?>
                                <img src="<?= base_url('web_assets/') ?>assets/images/my-profile-img.jpg" alt="User Image">
                            <?php } else { ?>
                                <img src="<?= USER_IMAGE ?>" alt="User Image">
                            <?php } ?>
                        </a>

                    </div>
                    <div class="upload-img">
                        <form method="post" id="update_profile_image_form" action="<?= base_url() ?>api/web/update_profile_image/<?= USER_ID ?>" enctype="multipart/form-data">
                            <span><i class="fal fa-camera-alt" for="file-input" onclick="$('#file-input').click();">
                                </i></span>
                            <input type='file' id="file-input" name="image" style="display: none" accept="image/*" onchange="$('#update_profile_image_form').submit();" />
                        </form>
                    </div>
                    <a href="" class="text-dark"><h4 class="mt-0 mb-0 font-weight-semibold"><?= ucfirst(USER_NAME) ?></h4></a>
                </div>
            </div>
        </div>
        <div class="item1-links  mb-0">


            <a href="<?= base_url('user-dashboard'); ?>" <?php if ($page_name == 'dashboard') { ?>class="active"<?php } ?>>
                <span class="icon1 "><i class="dropdown-icon icon icon-user"></i></span> My Dashboard
            </a>


            <a href="<?= base_url(); ?>my-bookings" <?php if ($page_name == 'services_orders') { ?>class="active"<?php } ?> >
                <span class="icon1 "><i class="dropdown-icon icon icon-doc"></i></span> Services Orders
            </a>
            <a href="<?= base_url(); ?>my_orders" <?php if ($page_name == 'my-orders.php') { ?>class="active"<?php } ?> >
                <span class="icon1 "><i class="dropdown-icon icon icon-basket"></i></span> E-Commerce Orders
            </a>
            <a href="notifications" <?php if ($page_name == 'notifications') { ?>class="active"<?php } ?>>
                <span class="icon1 "><i class="dropdown-icon icon icon-bell"></i></span> Notifications
            </a>
            <a href="<?= base_url(); ?>refer-a-friend" <?php if ($page_name == 'refer_a_friend') { ?>class="active"<?php } ?> >
                <span class="icon1"><i class="dropdown-icon icon icon-user"></i></span> Refer a friend
            </a>
            <a href="<?= base_url() ?>my-addresses" <?php if ($page_name == 'my_addresses') { ?>class="active"<?php } ?>>
                <span class="icon1 "><i class="dropdown-icon icon icon-map"></i></span> Booking Address
            </a>
            <!-- <a href="#" class="d-flex  border-bottom">
                            <span class="icon1 mr-3"><i class="dropdown-icon  icon icon-settings"></i></span> Account Settings
            </a> -->
            <a href="<?= base_url(); ?>my-profile" <?php if ($page_name == 'my_profile') { ?>class="active"<?php } ?>>
                <span class="icon1"><i class="dropdown-icon icon icon-user"></i></span> My Profile
            </a>
            <a href="<?= base_url(); ?>my-wallet" <?php if ($page_name == 'my_wallet') { ?>class="active"<?php } ?>>
                <span class="icon1"><i class="dropdown-icon icon icon-wallet"></i></span> My Wallet
            </a>
            <a href="<?= base_url(); ?>business-profile" <?php if ($page_name == 'business_profile') { ?>class="active"<?php } ?>>
                <span class="icon1 "><i class="dropdown-icon icon fas fa-dollar-sign"></i></span> Business Profile
            </a>
            <?php $businees_profile = $this->my_model->get_data_row("users_bussiness_details", array("user_id" => USER_ID, 'status' => 1)); ?>
            <?php if (empty($businees_profile)) { ?>
                <a href="<?= base_url(); ?>my-memberships" <?php if ($page_name == 'my_memberships') { ?>class="active"<?php } ?>>
                    <span class="icon1 "><i class="dropdown-icon icon icon-map"></i></span> Membership
                </a>
            <?php } ?>
            <a href="<?= base_url('website/logout') ?>">
                <span class="icon1"><i class="dropdown-icon icon icon-power"></i> </span> Log out
            </a>

        </div>
    </div>

    <script>
//        $('#file-input').on('change', function () {
//            var fd = new FormData();
//            var ins = this.files.length;
//            console.log(ins);
//            for (var x = 0; x < ins; x++) {
//                fd.append("files[]", this.files[x]);
//            }
//            console.log(fd);
//            return false;
//            $.ajax({
//                url: '<?= base_url() ?>api/web/update_profile_image/' + '<?= USER_ID ?>',
//                data: fd,
//                cache: false,
//                contentType: false,
//                processData: false,
//                type: 'POST',
//                success: function (response) {
//                    console.log(response);
//                    return;
//                    if (response) {
//                        swal({
//                            title: "Profile Image Updated",
//                            icon: 'success',
//                            button: "close"
//                        }).then(function () {
//                            location.href = "";
//                        });
//                    } else {
//                        swal({
//                            title: "Failed to update Profile Image",
//                            icon: 'warning',
//                            button: "close"
//                        }).then(function () {
//                            location.href = "";
//                        });
//                    }
//                }
//            });
//        });
    </script>

</div>