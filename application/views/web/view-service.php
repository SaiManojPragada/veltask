<!--Sliders Section-->
<div>
    <div class="cover-image sptb-1 bg-background" data-image-src="../assets/images/banners/banner1.jpg">
        <div class="header-text1 mb-0">
            <div class="container">
                <div class="row">
                    <div class="col-xl-10 col-lg-12 col-md-12 d-block ">
                        <div class=" breadcrumb-banner text-left text-white ">
                            <h1> <?= $category_name ?>
                            </h1>
                        </div>
                        <div>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="<?= base_url() ?>">Home
                                    </a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="#">Services
                                    </a>
                                </li>

                                <li class="breadcrumb-item active" aria-current="page"><a href="javascript:void(0);">View Detail</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /header-text -->
    </div>
</div>
<!--/Sliders Section-->



<!--Add listing-->
<section class="sptb">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 col-lg-8 col-md-12">

                <div class="left-side-viewdetail">
                    <!--Classified Description-->


                    <div class="card overflow-hidden">
                        <!-- 	<div class="ribbon ribbon-top-right text-danger">
                                        <span class="bg-danger">featured</span></div> -->
                        <div class="card-body h-100">

                            <div class="product-slider">
                                <!-- <div class="product-slide-img">
                                        <img src="assets/images/slide-1.jpg" alt="">
                                </div> -->

                                <div id="carousel" class="carousel slide" data-ride="carousel">
                                    <!-- 	<div class="arrow-ribbon2 bg-primary">$539</div> -->
                                    <div class="carousel-inner">
                                        <?php foreach ($service_images as $index => $si) { ?>
                                            <div class="carousel-item <?= ($index == 0) ? 'active' : '' ?>"> 
                                                <img src="<?= base_url('uploads/services/') . $si->image ?>" alt="img" style="height: 412px; object-fit: cover">
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                    </a>
                                    <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    </a>
                                </div>
                                <div class="clearfix">
                                    <div id="thumbcarousel" class="carousel slide" data-interval="false">
                                        <div class="carousel-inner">
                                            <?php foreach ($array_chunk_images as $i => $chunk) { ?>
                                                <div class="carousel-item <?= ($i == 0) ? 'active' : '' ?>">
                                                    <?php foreach ($chunk as $index => $im) { ?>
                                                        <div data-target="#carousel" data-slide-to="<?= $index ?>" class="thumb"><img src="<?= base_url('uploads/services/') . $im->image ?>" alt="img" style="height: 77px; object-fit: cover"></div>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <a class="carousel-control-prev" href="#thumbcarousel" role="button" data-slide="prev">
                                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                                        </a>
                                        <a class="carousel-control-next" href="#thumbcarousel" role="button" data-slide="next">
                                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>


                            </div>

                            <div class="item-det mt-4">

                                <h3  class="text-dark"><?= $service->service_name ?>
                                </h3>
                                <div class=" d-flex">
                                    <?php if ($service->duration) { ?>
                                        <ul class="d-flex mb-0">
                                            <li class="mr-5"><a href="#" class="icons"><i class="far fa-clock" aria-hidden="true"></i> <?= $service->duration ?></a></li>
                                        </ul>
                                    <?php } ?>


                                    <div class="rating-stars d-flex mr-5">
                                        <?php if ($service->rating > 0) { ?>
                                            <div>
                                                <i class="fa fa-star" style="<?= (floor($service->rating) > 0) ? 'color: #ffa22b' : '' ?>"></i>
                                                <i class="fa fa-star" style="<?= (floor($service->rating) > 1) ? 'color: #ffa22b' : '' ?>"></i>
                                                <i class="fa fa-star" style="<?= (floor($service->rating) > 2) ? 'color: #ffa22b' : '' ?>"></i>
                                                <i class="fa fa-star" style="<?= (floor($service->rating) > 3) ? 'color: #ffa22b' : '' ?>"></i>
                                                <i class="fa fa-star" style="<?= (floor($service->rating) > 4) ? 'color: #ffa22b' : '' ?>"></i>
                                            </div>&nbsp;
                                            <!--                                            <div class="rating-stars-container mr-2">
                                                                                            <i class="fa fa-star"></i>
                                                                                            <div class="rating-star sm <?= (floor($service->rating) > 0) ? '' : '' ?>">
                                                                                                <i class="fa fa-star"></i>
                                                                                            </div>
                                                                                            <div class="rating-star sm <?= (floor($service->rating) > 1) ? '' : '' ?>">
                                                                                                <i class="fa fa-star"></i>
                                                                                            </div>
                                                                                            <div class="rating-star sm <?= (floor($service->rating) > 2) ? '' : '' ?>">
                                                                                                <i class="fa fa-star"></i>
                                                                                            </div>
                                                                                            <div class="rating-star sm <?= (floor($service->rating) > 3) ? '' : '' ?>">
                                                                                                <i class="fa fa-star"></i>
                                                                                            </div>
                                                                                            <div class="rating-star sm <?= (floor($service->rating) > 4) ? '' : '' ?>"">
                                                                                                <i class="fa fa-star"></i>
                                                                                            </div>
                                                                                        </div>-->
                                            <?= $service->rating ?>
                                        <?php } ?>
                                    </div>
                                    <div class="ml-auto">
                                        <!--                                        <button class="btn book-now btn-primary">Add
                                                                                </button>-->
                                        <div class="F8dpS zj0R0 _3L1X9" style="cursor: pointer; <?= ($service->has_visit_and_quote == 1) ? "padding: 4px 10px; width: auto !important; display: block" : "display: none" ?>">
                                            <strong><span onclick="show(1);">Visit & Quote <i class="far fa-angle-right"></i></span></strong>
                                        </div>

                                        <div class="F8dpS zj0R0 _3L1X9" style="cursor: pointer; <?= ($service->has_visit_and_quote == 1) ? "display: none" : "" ?>">
                                            <input id="service_id" type="hidden" value="<?= $service->id ?>">

                                            <div class="_1RPOp addbtn" id="addbtn" onclick="show();" style=' <?= ($service->quantity) ? "display : none" : "" ?>'>ADD</div>

                                            <div id="box1" class="box1" style="display: none;"></div>

                                            <div id="increament-div" class="increament-div"  style=' <?= ($service->quantity) ? "display : block" : "" ?>'>
                                                <div id="box" style="display: none;" class="box"></div>
                                                <div onclick="inccreament()" class="_1ds9T _2WdfZ _4aKW6 classList ">+</div>
                                                <div class="_2zAXs _2quy- _4aKW6" id="quantity"><?= ($service->quantity) ? $service->quantity : "0" ?></div>
                                                <div onclick="deccreament()"  class="_29Y5Z _20vNm _4aKW6">-</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php if ($service->has_visit_and_quote == 1) { ?>
                                    <div class="service-price">
                                        <p class="mb-0"><span class="offer-price">₹<?= $service->visiting_charges ?></span></p>
                                    </div>
                                <?php } else { ?>
                                    <div class="service-price">
                                        <p class="mb-0"><span class="offer-price">₹<?= $service->sale_price ?></span> <span class="mrp-price">₹<?= $service->price ?></span></p>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title pl-4 pr-4">Description</h3>
                        </div>
                        <div class="card-body">
                            <div class="mb-4">
                                <?= $service->description ?>
                            </div>

                        </div>

                    </div>
                    <!--/Classified Description-->

                    <!--Comments-->
                    <div class="card ">
                        <div class="card-header pl-4 pr-4">
                            <h3 class="card-title">Rating And Reviews</h3>
                        </div>

                        <div class="card-body p-0">
                            <?php foreach ($service->reviews as $rev) { ?>
                                <div class="media mt-0 p-5">
                                    <div class="d-flex mr-3">
                                        <a href="#"><img class="media-object brround" alt="user" src="<?= ($rev->user_data->image) ? base_url('uploads/users/') . $rev->user_data->image : base_url('web_assets/default-user.jpg') ?>"> </a>
                                    </div>
                                    <div class="media-body">
                                        <h5 class="mt-0 mb-1 font-weight-semibold"><?= ($rev->user_data->first_name) ? $rev->user_data->first_name : "User" ?>

                                            <span class="fs-14 ml-2"> <?= number_format($rev->rating, 1) ?> <i class="fa fa-star text-yellow"></i></span>
                                        </h5>
                                        <small class="text-muted"><i class="fa fa-calendar"></i> <?= date('M d', $rev->updated_at) ?>st <?= date('Y', $rev->updated_at) ?>  <i class=" ml-3 fa fa-clock"></i> <?= date('h:i A', $rev->updated_at) ?> </small>
                                        <p class="font-13  mb-2 mt-2">
                                            <?= $rev->comment ?>
                                        </p>
    <!--                                        <a href="#" class="mr-2"><span class="badge badge-primary">Helpful</span></a>
                                        <a href="" class="mr-2" data-toggle="modal" data-target="#Comment"><span >Comment</span></a>
                                        <a href="" class="mr-2" data-toggle="modal" data-target="#report"><span >Report</span></a>-->

                                    </div>
                                </div>
                            <?php } ?>
                            <?php if (empty($service->reviews)) { ?>
                                <center style="margin: 40px 0px">
                                    <h4> -- No Reviews Yet --</h4>
                                </center>
                            <?php } ?>

                        </div>
                    </div>
                    <!--/Comments-->

                </div>
            </div>

            <!--Right Side Content-->
            <div class="col-xl-4 col-lg-4 col-md-12">
                <div class="right-side-viewdetails">


                    <div class="card">
                        <div class="card-header pl-4 pr-4">
                            <h3 class="card-title">More Services</h3>
                        </div>
                        <div class="card-body ">
                            <ul class="vertical-scroll">
                                <?php foreach ($related_services as $index => $serv) { ?>
                                    <li class="news-item">
                                        <table>
                                            <tr>
                                                <td><img src="<?= (!empty($serv->image)) ? base_url('uploads/services/') . $serv->image : base_url('uploads/') . SITE_FAV_ICON ?>" alt="image" class="w-8"/></td>
                                                <td >
                                                    <div class="table-content">
                                                        <div class="container" style="cursor: pointer">
                                                            <h5 class="mb-1 "  onclick="location.href = '<?= base_url() . 'services/' . $serv->category_url . '/' . $serv->sub_category_url . '/' . $serv->seo_url ?>';"><?= $serv->service_name ?></h5><span class=" font-weight-bold">₹<?= ($serv->has_visit_and_quote == 1) ? $serv->visiting_charges : $serv->sale_price ?></span>
                                                            <!--<a href="javascript:void(0);" class="float-right btn-link"><button class="btn btn-primary add-btn">Add </button></a>-->

                                                            <div class="F8dpS zj0R0 _3L1X9 float-right " style="cursor: pointer; <?= ($serv->has_visit_and_quote == 1) ? "padding: 2px 5px; width: 120px !important; display: block" : "display: none; font-size 10px" ?>">
                                                                <span onclick="show_i('<?= $index ?>', 1);"  id="addbtn">Visit & Quote</span>
                                                            </div>
                                                            <div class="F8dpS zj0R0 _3L1X9 float-right " style="cursor: pointer; font-size 10px; <?= ($serv->has_visit_and_quote == 1) ? "display: none" : "" ?>">
                                                                <input id="service_id_<?= $index ?>" type="hidden" value="<?= $serv->id ?>">

                                                                <div class="_1RPOp addbtn_<?= $index ?>" id="addbtn" onclick="show_i('<?= $index ?>');" style=' <?= ($serv->quantity) ? "display : none" : "" ?>'>ADD</div>

                                                                <div id="box1" class="box1_<?= $index ?>" style="display: none;"></div>

                                                                <div id="increament-div" class="increament-div_<?= $index ?>"  style=' <?= ($serv->quantity) ? "display : block" : "" ?>'>
                                                                    <div id="box" style="display: none;" class="box_<?= $index ?>"></div>
                                                                    <div onclick="inccreament_i('<?= $index ?>')" class="_1ds9T _2WdfZ _4aKW6 classList ">+</div>
                                                                    <div class="_2zAXs _2quy- _4aKW6" id="quantity_<?= $index ?>"><?= ($serv->quantity) ? $serv->quantity : "0" ?></div>
                                                                    <div onclick="deccreament_i('<?= $index ?>')"  class="_29Y5Z _20vNm _4aKW6">-</div>
                                                                </div>
                                                            </div>



                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </li>
                                <?php } ?>

                            </ul>
                        </div>
                    </div>


                </div>
            </div>
            <!--/Right Side Content-->
        </div>
    </div>
</section>

<section style="<?= ($cart_total->items_in_cart < 1) ? 'display:none' : '' ?>" id="cart_bar">

    <div class="footer-sticky-bar">
        <div class="container">

            <div class="row">
                <div class="col-md-6 text-left">
                    <?php if (!$has_membership) { ?>
                        <div class="view-member-btn">
                            <button  type="button"  data-toggle="modal" data-target="#Mymodal1" class="btn btn-default cart-btn-menu"> <a href="javascript:void(0);">View Membership
                                </a></button>
                        </div>
                    <?php } ?>



                </div>
                <div class="col-md-6 ">
                    <div class="Continue-btn ml-auto justify-content-center">
                        <div class="contine-parent" onclick="location.href = '<?= base_url('cart') ?>'">
                            <span class="service-product-count" id="items_in_cart"><?= number_format($cart_total->items_in_cart) ?>
                            </span><strong>Grand Total : </strong>₹<span id="cart_total"><?= $cart_total->sub_total ?></span>
                            <span class="continue-btn-right"><a href="javascript:void(0);" >Continue</a>
                            </span>
                        </div>


                    </div>
                </div>
            </div>
        </div><!--footer-sticky-bar-->
    </div>
</section>
<div class="alert alert-success cart_alert" role="alert" id="cart_alert" style="display: none" onclick="$('#cart_alert').fadeOut(2000);">

</div>
<style>
    #cart_alert{
        position: fixed;
        top: -30px;
        right: 40px;
    }
</style>
<script>

    function hide_cart_message() {
        setTimeout(function () {
            $(".cart_alert").fadeOut(2000);
        }, 5000);
    }

    function show_cart() {
        $('#cart_bar').css('display', 'block');
    }
    function hide_cart() {
        $('#cart_bar').css('display', 'none');
    }
    var prev_cart_desg = <?= ($cart[0]->has_visit_and_quote) ? '1' : '0' ?>;
    function show(has_visit_and_quote = 0) {

<?php if (!empty(USER_ID)) { ?>
            $(".increament-div").css('display', "block");
            $(".addbtn").css('display', "none");
            $('#quantity').html('1');
            //animation
            $("box").css("display", "block");
            setTimeout(function () {
                $("box").css("display", "none");
            }, 400);


            if (prev_cart_desg == 0 && has_visit_and_quote == 1) {
                $.ajax({
                    url: '<?= base_url() ?>api/web/chec_for_v_a_q',
                    type: 'post',
                    data: {user_id: '<?= USER_ID ?>'},
                    success: function (resp) {
                        console.log(resp);
                        if (!resp) {
                            swal({
                                title: "Are you sure?",
                                text: "If You Already have normal services in your cart by Proceeding to Visit and Quote your cart will be cleared.",
                                icon: "warning",
                                buttons: [
                                    'No, cancel it!',
                                    'Yes, I am sure!'
                                ],
                                dangerMode: true,
                            }).then(function (isConfirm) {
                                if (isConfirm) {
                                    deleteCartVQ(has_visit_and_quote);
                                }
                            });
                        } else {
                            deleteCartVQ(has_visit_and_quote);
                        }
                    }
                });

            } else {
                call_cart(has_visit_and_quote);

                hide_cart_message();
            }
<?php } else { ?>
            swal({
                title: "Please Login to Continue",
                icon: "info"
            }).then(function () {
                openNav();
            });
<?php } ?>

    }

    function deleteCartVQ(has_visit_and_quote) {
        $.ajax({
            url: '<?= base_url() ?>api/cart/delete_cart',
            type: 'post',
            data: {user_id: '<?= USER_ID ?>'},
            success: function (resp) {
                resp = JSON.parse(resp);
                if (resp['status']) {
                    call_cart(has_visit_and_quote);
                    $(".cart_alert").html("Item Added to Cart");
                    $(".cart_alert").fadeIn(1000);
                    hide_cart_message();
                } else {
                    swal({
                        title: resp['message'],
                        icon: 'info'
                    });
                }
            }
        });
    }

    function inccreament()
    {

        //animation
        $(".box").css("display", "block");
        var quantity = $("#quantity").html();
        quantity = parseInt(quantity.toString()) + 1;
        $("#quantity").html(quantity);
        setTimeout(function () {
            $(".box").css("display", "none");
        }, 400);
        call_cart();

    }

    function deccreament(val = "")
    {
        var quantity = $("#quantity").html();
        quantity = parseInt(quantity.toString()) - 1;
        $("#quantity").html(quantity);
        if (quantity < 1) {
            $(".increament-div").css('display', "none");
            $(".addbtn").css('display', "block");
            if (val == "") {
//                $(".cart_alert").html("Item Removed from Cart");
                //                $(".cart_alert").fadeIn(1000);
                hide_cart_message();
            }
        }
        //animation
        $(".box").css("display", "block");
        setTimeout(function () {
            $(".box").css("display", "none");

        }, 400);
        if (val == "") {
            call_cart();
    }
    }

    function call_cart(has_visit_and_quote = 0) {
        var service_id = $("#service_id").val();
        var quantity = $("#quantity").html();
        if (has_visit_and_quote == 1) {
            quantity = 1;
        }
        $.ajax({
            url: "<?= base_url('api/web/cart') ?>",
            type: "post",
            data: {service_id: service_id, quantity: quantity, has_visit_and_quote: has_visit_and_quote},
            success: function (json) {
                var resp = JSON.parse(json);
                if (resp['status']) {
//                    $(".cart_alert").html("Item Added to Cart");
                    //                    $(".cart_alert").fadeIn(1000);
                    show_cart();
                    var data = resp['data'];
                    console.log(data);
                    $("#cart_total").html(data['cart_total']);
                    $("#items_in_cart").html(data['items_in_cart']);
                    $("#cart-items-top").html(data['items_in_cart']);
                    if (data['items_in_cart'] == 0) {
                        hide_cart();
                    }
                    if (has_visit_and_quote == 1) {
                        swal({
                            title: resp['message'],
                            icon: 'success'
                        }).then(function () {
                            location.href = "<?= base_url('cart') ?>";
                        });
                    }
                } else {
                    deccreament("stat");
                    if (resp['type'] == null) {
                        swal({
                            title: resp['message'],
                            icon: "info"
                        }).then(function () {
<?php if (empty(USER_ID)) { ?>
                                openNav();
<?php } ?>
                        });
                    } else {
                        swal({
                            title: resp['message'],
                            text: "If you proceed you current cart will be deleted.",
                            icon: "warning",
                            buttons: [
                                'No, cancel it!',
                                'Yes, I am sure!'
                            ],
                            dangerMode: true,
                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                $.ajax({
                                    url: "<?= base_url('api/cart/delete_cart') ?>",
                                    type: "post",
                                    data: {user_id: "<?= USER_ID ?>"},
                                    success: function (resp) {
                                        resp = JSON.parse(resp);
                                        if (resp['status']) {
                                            $("#quantity").html("1");
                                            show(has_visit_and_quote);
                                        } else {
                                            swal({
                                                title: resp['message'],
                                                icon: "info"
                                            });
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            }
        });
    }

</script> 
<script>
    function show_i(index, has_visit_and_quote = 0) {

<?php if (!empty(USER_ID)) { ?>
            $(".increament-div_" + index).css('display', "block");
            $(".addbtn_" + index).css('display', "none");
            $('#quantity_' + index).html('1');
            //animation
            $("box_" + index).css("display", "block");
            setTimeout(function () {
                $("box_" + index).css("display", "none");
            }, 400);

            //            $(".cart_alert").html("Item Added to Cart");
            //            $(".cart_alert").fadeIn(1000);
            hide_cart_message();
            if (prev_cart_desg == 0 && has_visit_and_quote == 1) {
                $.ajax({
                    url: '<?= base_url() ?>api/web/chec_for_v_a_q',
                    type: 'post',
                    data: {user_id: '<?= USER_ID ?>'},
                    success: function (resp) {
                        console.log(resp);
                        if (!resp) {
                            swal({
                                title: "Are you sure?",
                                text: "You Already have normal services in your cart by Proceeding to Visit and Quote your cart will be cleared.",
                                icon: "warning",
                                buttons: [
                                    'No, cancel it!',
                                    'Yes, I am sure!'
                                ],
                                dangerMode: true,
                            }).then(function (isConfirm) {
                                if (isConfirm) {
                                    deleteCartVQ_i(index, has_visit_and_quote);
                                }
                            });
                        } else {
                            deleteCartVQ_i(index, has_visit_and_quote);
                        }
                    }
                });
            } else {
                call_cart_i(index, has_visit_and_quote);
            }

<?php } else { ?>
            swal({
                title: "Please Login to Continue",
                icon: "info"
            }).then(function () {
                openNav();
            });
<?php } ?>
    }


    function deleteCartVQ_i(index, has_visit_and_quote) {
        $.ajax({
            url: '<?= base_url() ?>api/cart/delete_cart',
            type: 'post',
            data: {user_id: '<?= USER_ID ?>'},
            success: function (resp) {
                resp = JSON.parse(resp);
                if (resp['status']) {
                    call_cart_i(index, has_visit_and_quote);
                } else {
                    swal({
                        title: resp['message'],
                        icon: 'info'
                    });
                }
            }
        });
    }

    function inccreament_i(index)
    {

        //animation
        $(".box_" + index).css("display", "block");
        var quantity = $("#quantity_" + index).html();
        quantity = parseInt(quantity.toString()) + 1;
        $("#quantity_" + index).html(quantity);
        setTimeout(function () {
            $(".box_" + index).css("display", "none");
        }, 400);
        call_cart_i(index);

    }

    function deccreament_i(index, stat = "")
    {
        var quantity = $("#quantity_" + index).html();
        quantity = parseInt(quantity.toString()) - 1;
        $("#quantity_" + index).html(quantity);
        if (quantity < 1) {
            $(".increament-div_" + index).css('display', "none");
            $(".addbtn_" + index).css('display', "block");
            if (stat == "") {
                $(".cart_alert").html("Item Removed from Cart");
                $(".cart_alert").fadeIn(1000);
                hide_cart_message();
            }
        }
        //animation
        $(".box_" + index).css("display", "block");
        setTimeout(function () {
            $(".box_" + index).css("display", "none");

        }, 400);
        if (stat == "") {
            call_cart_i(index);
    }
    }

    function call_cart_i(index, has_visit_and_quote = 0) {

        var service_id = $("#service_id_" + index).val();
        var quantity = $("#quantity_" + index).html();
        if (has_visit_and_quote == 1) {
            quantity = 1;
        }
        $.ajax({
            url: "<?= base_url('api/web/cart') ?>",
            type: "post",
            data: {service_id: service_id, quantity: quantity, has_visit_and_quote: has_visit_and_quote},
            success: function (json) {
                var resp = JSON.parse(json);
                if (resp['status']) {
                    show_cart();
                    var data = resp['data'];
                    console.log(data);
                    $("#cart_total").html(data['cart_total']);
                    $("#items_in_cart").html(data['items_in_cart']);
                    $("#cart-items-top").html(data['items_in_cart']);
                    if (data['items_in_cart'] == 0) {
                        hide_cart();
                    }
                    if (has_visit_and_quote == 1) {
                        swal({
                            title: resp['message'],
                            icon: 'success'
                        }).then(function () {
                            location.href = "<?= base_url('cart') ?>";
                        });
                    }
                } else {
                    deccreament_i(index, "stat");

                    if (resp['type'] == null) {
                        swal({
                            title: resp['message'],
                            icon: "info"
                        }).then(function () {
<?php if (empty(USER_ID)) { ?>
                                openNav();
<?php } ?>
                        });
                    } else {
                        swal({
                            title: resp['message'],
                            text: "If you proceed you current cart will be deleted.",
                            icon: "warning",
                            buttons: [
                                'No, cancel it!',
                                'Yes, I am sure!'
                            ],
                            dangerMode: true,
                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                $.ajax({
                                    url: "<?= base_url('api/cart/delete_cart') ?>",
                                    type: "post",
                                    data: {user_id: "<?= USER_ID ?>"},
                                    success: function (resp) {
                                        resp = JSON.parse(resp);
                                        if (resp['status']) {
                                            $("#quantity_" + index).html(1);
                                            show_i(index, has_visit_and_quote);
                                        } else {
                                            swal({
                                                title: resp['message'],
                                                icon: "info"
                                            });
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            }
        });
    }

</script> 