<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?= SITE_NAME ?></title>
        <link rel="icon" href="<?= base_url('uploads/') . SITE_FAV_ICON ?>" type="image/x-icon" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>web_assets/style.css">
    </head>
    <body>

        <!------ Include the above in your HEAD tag ---------->
        <header>
            <div>
                <nav class="navbar navbar-expand-lg navbar-light ">
                    <a href="<?= base_url() ?>"><img src="<?= base_url('uploads/') ?><?= SITE_FOOTER_LOGO ?>"></a>
                    <!--                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                            <span class="navbar-toggler-icon"></span>
                                        </button>-->
                    <span style="font-size:30px;cursor:pointer; color: #fff" onclick="openNav()">&#9776;</span>
                    <div id="mySidenav" class="sidenav">

                        <ul class="ml-auto my-2 my-lg-0 list-menu">
                            <li>  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a></li>
                            <li class="nav-item <?= empty($this->input->get('reg')) ? 'active' : '' ?>">
                                <a class="nav-link" href="<?= base_url('business-registration') ?>">Register as Franchise <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item <?= !empty($this->input->get('reg')) ? 'active' : '' ?>">
                                <a class="nav-link" href="<?= base_url('business-registration') . '?reg=sp' ?>">Register as Service Provider <span class="sr-only">(current)</span></a>
                            </li>

                        </ul>
                    </div>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="ml-auto my-2 my-lg-0 list-menu">
                            <li class="nav-item <?= empty($this->input->get('reg')) ? 'active' : '' ?>">
                                <a class="nav-link" href="<?= base_url('business-registration') ?>">Register as Franchise <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item <?= !empty($this->input->get('reg')) ? 'active' : '' ?>">
                                <a class="nav-link" href="<?= base_url('business-registration') . '?reg=sp' ?>">Register as Service Provider <span class="sr-only">(current)</span></a>
                            </li>

                        </ul>

                    </div>
                </nav>
            </div>

        </header>
        <?php if (!empty($this->session->userdata("popup_success"))) { ?>
            <div class="alert alert-success">
                <p><?= $this->session->userdata("popup_success"); ?><span style="float: right; cursor: pointer" onclick="fadeOut()">&times;</span></p>
            </div>
        <?php } $this->session->unset_userdata("popup_success"); ?>
        <?php if (!empty($this->session->userdata("popup_error"))) { ?>
            <div class="alert alert-danger">
                <p><?= $this->session->userdata("popup_error"); ?><span style="float: right; cursor: pointer" onclick="fadeOut()">&times;</span></p>
            </div>
        <?php } $this->session->unset_userdata("popup_error"); ?>
        <?php if (empty($this->input->get('reg'))) { ?>
            <div class="container-fluid register">
                <div class="row">
                    <div class="col-md-3 register-left">
                        <img src="https://image.ibb.co/n7oTvU/logo_white.png" alt=""/>
                        <h3>Welcome</h3>
                        <span>Have already an account?</span><br>
                        <a href="<?= base_url() ?>admin/login"><input type="submit" name="" value="Login"/></a><br/>
                    </div>
                    <div class="col-md-9 register-right">
                        <form method="post" action="" id="franchise-signup-form" autocomplete="off" enctype="multipart/form-data">
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    <h3 class="register-heading">Register As a Franchise</h3>
                                    <div class="row register-form">
                                        <div class="col-md-12 row">
                                            <div class="form-group col-md-6">
                                                <label class="control-label">Name *</label>
                                                <input type="text" class="form-control" name="name" placeholder="Name *" value="" minlength="3" data-parsley-pattern="^[a-zA-Z\s]*$" data-parsley-pattern-message="Please Enter Valid Name" data-parsley-required-message="Please Enter Valid Name" required/>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label class="control-label">Email *</label>
                                                <input type="email" class="form-control" name="email" placeholder="Your Email *" value="" required data-parsley-type-message="Please Enter Valid Email" data-parsley-required-message="Please Enter Valid Email" data-parsley-trigger="change"/>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label class="control-label">Phone Number *</label>
                                                <input type="tel" min='1' data-parsley-minlength="10"  data-parsley-maxlength="10" data-parsley-required-message="Please Enter Phone Number" data-parsley-minlength-message="Enter Valid Phone Number *" data-parsley-maxlength-message="Enter Valid Phone Number" required name="mobile_number" class="form-control" placeholder="Mobile Number *" value="" required/>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label class="control-label">Alternative Phone Number</label>
                                                <input type="tel" minlength="10" maxlength="10" name="alternative_mobile" class="form-control" placeholder="Alternate Mobile Number (Optional)" value="" />
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label class="control-label">Your Photo *</label>
                                                <input type="file" class="form-control" placeholder="select your photo *" accept="image/*" name="photo" required  data-parsley-required-message="Please Select your Photo"/>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label class="control-label">Your Aadhar Card Photo </label>
                                                <input type="file" class="form-control" placeholder="select your aadhar photo" accept="image/*" name="aadhar_photo"/>
                                            </div>


                                            <div class="form-group col-md-6">
                                                <label class="control-label">Your Covid Vaccination Certificate </label>
                                                <input type="file" class="form-control" placeholder="select your Covid Vaccination Certificate" accept="application/pdf" name="vaccination_certificate"/>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label class="control-label">State *</label>
                                                <select class="form-control" name="state_id" required  data-parsley-required-message="Please Select a State" onclick="getCities(this.value)">
                                                    <option class="hidden" value="">Select State *</option>
                                                    <?php foreach ($states as $item) { ?>
                                                        <option value="<?= $item->id ?>"><?= $item->state_name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label class="control-label">City *</label>
                                                <select class="form-control" required name="location_id" id="location_id" data-parsley-required-message="Please Select a city" onchange="getPincodes(this.value)">
                                                    <option class="hidden" value="">Select City *</option>
                                                </select>
                                            </div>


                                            <div class="form-group col-md-6">
                                                <label class="control-label">Pincode *</label>
                                                <select class="form-control" name="pincode_id" id="pincode_id" required  data-parsley-required-message="Please Select a Pincode">
                                                    <option class="hidden" value="">Select Pincode *</option>
                                                </select>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label class="control-label">GST Number</label>
                                                <input type="text" class="form-control" name="gst_number" placeholder="GST Number" value=""/>
                                            </div>

                                            <div class="form-group col-md-12">
                                                <label class="control-label">Address *</label>
                                                <textarea type="text" class="form-control"required data-parsley-minlength="10"  data-parsley-minlength-message="Address is too Short" name="address" placeholder="Enter Address *" value="" required style="height: 147px" data-parsley-required-message="Please Enter Valid Address"></textarea>
                                            </div>

                                            <!--                                            <div class="form-group col-md-6">
                                                                                            <input type="text" name="latitude" name="latitude" class="form-control" placeholder="Enter Latitude *" value="" required data-parsley-pattern="^[0-9]{2}\.[0-9]{4,10}$" data-parsley-pattern-message="Invalid Latitude"  data-parsley-required-message="Invalid Latitude"/>
                                                                                        </div>
                                            
                                            
                                                                                        <div class="form-group col-md-6">
                                                                                            <input type="text" name="longitude" name="longitude" class="form-control" placeholder="Enter Longitude *" value="" required data-parsley-pattern="^[0-9]{2}\.[0-9]{4,10}$" data-parsley-pattern-message="Invalid Longitude"  data-parsley-required-message="Invalid Longitude"/>
                                                                                        </div>-->

                                            <div class="form-group col-md-6">
                                                <label class="control-label">Latitude</label>
                                                <input type="text" name="latitude" name="latitude" class="form-control" placeholder="Enter Latitude " value=""/>
                                            </div>


                                            <div class="form-group col-md-6">
                                                <label class="control-label">Longitude</label>
                                                <input type="text" name="longitude" name="longitude" class="form-control" placeholder="Enter Longitude " value="" />
                                            </div>


                                            <div class="form-group col-md-6">
                                                <label class="control-label">Password *</label>
                                                <input type="password" name="password" id="password" class="form-control" placeholder="Password *" value="" required data-parsley-minlength="8" data-parsley-required-message="Please Enter Valid Password" data-parsley-minlength-message="Password Must be atleast 8 Charecters" placeholder="Password *" required/>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label class="control-label">Re-enter Password *</label>
                                                <input type="password" name="re_password" class="form-control"  placeholder="Confirm Password *" value="" required data-parsley-equalto="#password" data-parsley-required-message="Please Enter Valid Password"  data-parsley-equalto-message="Passwords does not Match"/>
                                            </div>
                                            <div class="form-group col-md-6"></div>

                                            <div class="form-group col-md-6">
                                                <input type="submit" class="btnRegister" name="franchise_submit" value="Register"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>

            </div>
        <?php } else { ?>
            <div class="container-fluid register">
                <div class="row">
                    <div class="col-md-3 register-left">
                        <img src="https://image.ibb.co/n7oTvU/logo_white.png" alt=""/>
                        <h3>Welcome</h3>
                        <span>Have already an account?</span><br>
                        <a href="<?= base_url() ?>vendors/login"><input type="submit" name="" value="Login"/></a><br/>
                    </div>
                    <div class="col-md-9 register-right">
                        <form method="post" action="" id="vendor-signup-form" autocomplete="off" enctype="multipart/form-data">
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    <h3 class="register-heading">Register As a Service Provider</h3>
                                    <div class="row register-form">
                                        <div class="col-md-12 row">
                                            <div class="form-group col-md-6">
                                                <label class="control-label">Name *</label>
                                                <input type="text" class="form-control" name="name" placeholder="Name *" value="" minlength="3" data-parsley-pattern="^[a-zA-Z\s]*$" data-parsley-pattern-message="Please Enter Valid Name" data-parsley-required-message="Please Enter Valid Name" required/>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label class="control-label">Email *</label>
                                                <input type="email" class="form-control" name="email" placeholder="Your Email *" value="" required data-parsley-type-message="Please Enter Valid Email" data-parsley-required-message="Please Enter Valid Email" data-parsley-trigger="change"/>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label class="control-label">Provider Type *</label>
                                                <select class="form-control" name="type" id="type" required  data-parsley-required-message="Please Provide Type">
                                                    <option class="hidden" value="">Select Provider Type *</option>
                                                    <option value="Owner">Owner</option>
                                                    <option value="Owner + Technician">Technician</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="control-label">Phone Number *</label>
                                                <input type="tel" min='1' data-parsley-minlength="10"  data-parsley-maxlength="10" data-parsley-required-message="Please Enter Phone Number" data-parsley-minlength-message="Enter Valid Phone Number *" data-parsley-maxlength-message="Enter Valid Phone Number" required name="phone" class="form-control" placeholder="Mobile Number *" value="" required/>
                                            </div>


                                            <div class="form-group col-md-6">
                                                <label class="control-label">Your Photo *</label>
                                                <input type="file" class="form-control" placeholder="select your photo *" accept="image/*" name="photo" required  data-parsley-required-message="Please Select your Photo"/>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label class="control-label">Your Aadhar Card Photo </label>
                                                <input type="file" class="form-control" placeholder="select your aadhar photo" accept="image/*" name="aadhar_photo"/>
                                            </div>


                                            <div class="form-group col-md-6">
                                                <label class="control-label">Your PCC Photo </label>
                                                <input type="file" class="form-control" placeholder="select your PCC photo (Police Clearence Certificate)" accept="image/*" name="pcc_photo"/>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label class="control-label">Services Categories *</label>
                                                <select class="form-control" name="categories_ids" required  data-parsley-required-message="Please Select atleast one Service Category ">
                                                    <option class="hidden" value="">Select Services Categories *</option>
                                                    <?php foreach ($services_categories as $item) { ?>
                                                        <option value="<?= $item->id ?>"><?= $item->name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label class="control-label">City *</label>
                                                <select class="form-control" name="location_id" required  data-parsley-required-message="Please Select a City ">
                                                    <option class="hidden" value="">Select City *</option>
                                                    <?php foreach ($cities as $item) { ?>
                                                        <option value="<?= $item->id ?>"><?= $item->city_name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>




                                            <!--                                            <div class="form-group col-md-6">
                                                                                            <input type="text" name="latitude" name="latitude" class="form-control" placeholder="Enter Latitude *" value="" required data-parsley-pattern="^[0-9]{2}\.[0-9]{4,10}$" data-parsley-pattern-message="Invalid Latitude"  data-parsley-required-message="Invalid Latitude"/>
                                                                                        </div>
                                            
                                            
                                                                                        <div class="form-group col-md-6">
                                                                                            <input type="text" name="longitude" name="longitude" class="form-control" placeholder="Enter Longitude *" value="" required data-parsley-pattern="^[0-9]{2}\.[0-9]{4,10}$" data-parsley-pattern-message="Invalid Longitude"  data-parsley-required-message="Invalid Longitude"/>
                                                                                        </div>-->

                                            <div class="form-group col-md-6">
                                                <label class="control-label">Latitude </label>
                                                <input type="text" name="latitude" name="latitude" class="form-control" placeholder="Enter Latitude " value=""/>
                                            </div>


                                            <div class="form-group col-md-6">
                                                <label class="control-label">Longitude </label>
                                                <input type="text" name="longitude" name="longitude" class="form-control" placeholder="Enter Longitude " value="" />
                                            </div>


                                            <div class="form-group col-md-6">
                                                <label class="control-label">Password *</label>
                                                <input type="password" name="password" id="password" class="form-control" placeholder="Password *" value="" required data-parsley-minlength="8" data-parsley-required-message="Please Enter Valid Password" data-parsley-minlength-message="Password Must be atleast 8 Charecters" placeholder="Password *" required/>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label class="control-label">Re-enter Password *</label>
                                                <input type="password" name="re_password" class="form-control"  placeholder="Confirm Password *" value="" required data-parsley-equalto="#password" data-parsley-required-message="Please Enter Valid Password"  data-parsley-equalto-message="Passwords does not Match"/>
                                            </div>



                                            <div class="form-group col-md-6">
                                                <label class="control-label">Your Covid Vaccination Certificate </label>
                                                <input type="file" class="form-control" placeholder="select your Covid Vaccination Certificate" accept="application/pdf" name="vaccination_certificate"/>
                                            </div>

                                            <div class="form-group col-md-12">
                                                <label class="control-label">Address *</label>
                                                <textarea type="text" class="form-control"required data-parsley-minlength="10"  data-parsley-minlength-message="Address is too Short" name="address" placeholder="Enter Address *" value="" required style="height: 147px" data-parsley-required-message="Please Enter Valid Address"></textarea>
                                            </div>


                                            <div class="form-group col-md-6"></div>
                                            <div class="form-group col-md-6">
                                                <input type="submit" class="btnRegister" name="service_provider_submit" value="Register"/></div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>

            </div>
        <?php } ?>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>web_assets/js/plugins/parsleyjs/dist/parsley.min.js"></script> 
        <script>
                                                    function openNav() {
                                                        document.getElementById("mySidenav").style.width = "250px";
                                                    }

                                                    function closeNav() {
                                                        document.getElementById("mySidenav").style.width = "0";
                                                    }
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#franchise-signup-form').parsley();
                $('#vendor-signup-form').parsley();
            });

            function getCities(val) {
                $.ajax({
                    url: "<?= base_url() ?>website/getCities",
                    type: "post",
                    data: {state_id: val},
                    success: function (resp) {
                        console.log(resp);
                        $("#location_id").html(resp);
                    }
                });
            }


            function getPincodes(val) {
                $.ajax({
                    url: "<?= base_url() ?>website/getPincodes",
                    type: "post",
                    data: {city_id: val},
                    success: function (resp) {
                        console.log(resp);
                        $("#pincode_id").html(resp);
                    }
                });
            }
            function fadeOut() {
                $(".alert").fadeOut();
            }
            setTimeout(function () {
                fadeOut();
            }, 4000);
        </script>
        <style>
            .parsley-errors-list{
                list-style:none;
                margin: 4px 0px 0px 0px;
            }
            .parsley-required , .parsley-errors-list li{
                color:red;
                list-style:none;
                font-size: 11px;
                margin-left:-30px;
            }
            .alert{
                position: absolute;
                min-width: 400px;
                right: 20px;
                top: 20px;
                z-index: 10000;
            }


            .sidenav {
                height: 100%;
                width: 0;
                position: fixed;
                z-index: 1;
                top: 0;
                left: 0;
                background-color: #111;
                overflow-x: hidden;
                transition: 0.5s;
                padding-top: 60px;
            }

            .sidenav a {

                text-decoration: none;
                font-size: 25px;
                color: #818181;
                display: block;
                transition: 0.3s;
            }

            .sidenav a:hover {
                color: #f1f1f1;
            }

            .sidenav .closebtn {
                position: absolute;
                top: 0;
                right: 25px;
                font-size: 36px;
                margin-left: 50px;
            }

            div#mySidenav ul {
                padding-left: 10PX;
                margin-top: 51px !important;
            }
            @media screen and (max-height: 450px) {
                .sidenav {padding-top: 15px;}
                .sidenav a {font-size: 18px;}
            }
        </style>
    </body>
</html>