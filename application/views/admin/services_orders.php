<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Services Orders</h5>
                    <div class="ibox-tools">
                        <a href="<?= base_url() ?>admin/dashboard">
                            <button class="btn btn-primary">BACK</button>
                        </a>

                        <?php
                        $user_type = $_SESSION['admin_login']['user_type'];
                        if ($user_type == 'subadmin') {
                            $admin_id = $_SESSION['admin_login']['id'];
                            $adm_qry = $this->db->query("select * from sub_admin where id='" . $admin_id . "'");
                            $adm_row = $adm_qry->row();

                            $userpermissions = $adm_row->permissions;
                            $permissions = explode(",", $userpermissions);
                            if (in_array("add_category", $permissions)) {
                                ?>


                                <?php
                            }
                        } else {
                            ?>


                        <?php } ?>


                    </div>

                    <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                        <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                        </div>
                    <?php } ?>
                    <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                        <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                        </div>
                    <?php }
                    ?>
                </div>


                <div class="ibox-content">
                    <?php if ($_SESSION['admin_login']['user_type'] != 'franchise') { ?>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="widget style1 blue-bg">
                                    <div class="row">
                                        <a style="color: #FFF;" href="https://dev.veltask.com/admin/services_orders/"><div class="col-xs-12 text-center">
                                                <span> Total Orders </span>

                                                <h2 class="font-bold"><?= $total_orders ?></h2>
                                            </div></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="widget style1 bg-warning">
                                    <div class="row">
                                        <a style="color: #FFF;" href="https://dev.veltask.com/admin/services_orders/?stat=ongoing"><div class="col-xs-12 text-center">
                                                <span> In progress Orders </span>
                                                <h2 class="font-bold"><?= round($inprogress_count) ?></h2>
                                            </div></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="widget style1 bg-success">
                                    <div class="row">
                                        <a style="color: #FFF;" href="https://dev.veltask.com/admin/services_orders/?stat=completed"><div class="col-xs-12 text-center">
                                                <span> Total Completed Orders </span>

                                                <h2 class="font-bold"><?= round($completed_count) ?></h2>
                                            </div></a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="widget style1 bg-success">
                                    <div class="row">
                                        <a style="color: #FFF;" href="https://dev.veltask.com/admin/services_orders/?from_date=<?= date('Y-m-d') ?>"><div class="col-xs-12 text-center">
                                                <span> Total Today Orders </span>

                                                <h2 class="font-bold"><?= round($today_orders) ?></h2>
                                            </div></a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="widget style1 bg-danger">
                                    <div class="row">
                                        <a style="color: #FFF;" href="https://dev.veltask.com/admin/services_orders/?time_slot_date=<?= date('Y-m-d') ?>"><div class="col-xs-12 text-center">
                                                <span> Total Today Scheduled Orders </span>

                                                <h2 class="font-bold"><?= round($today_scheduled_orders) ?></h2>
                                            </div></a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="widget style1 bg-danger">
                                    <div class="row">
                                        <a style="color: #FFF;" href="https://dev.veltask.com/admin/services_orders/?time_slot_date=<?= date('Y-m-d') ?>&missed=yes"><div class="col-xs-12 text-center">
                                                <span> Total Missed Orders </span>

                                                <h2 class="font-bold"><?= round($missed_service_orders) ?></h2>
                                            </div></a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="widget style1 bg-danger">
                                    <div class="row">
                                        <a style="color: #FFF;" href="https://dev.veltask.com/admin/services_orders/?visit_and_quote=Yes"><div class="col-xs-12 text-center">
                                                <span> Total Visit and Quote Orders </span>

                                                <h2 class="font-bold"><?= round($total_visit_and_quote_orders) ?></h2>
                                            </div></a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3" >
                                <div class="widget style1 bg-success">
                                    <div class="row">
                                        <a style="color: #FFF;" href="javascript:void(0);"><div class="col-xs-12 text-center">
                                                <span> Total Orders with Milestones </span>

                                                <h2 class="font-bold"><?= round($milestone_orders) ?></h2>
                                            </div></a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="widget style1 bg-success">
                                    <div class="row">
                                        <a style="color: #FFF;" href="javascript:void(0);"><div class="col-xs-12 text-center">
                                                <span> Total Sale Amount </span>

                                                <h2 class="font-bold"><i class="fa fa-inr"></i> <?= round($earnings->total_sale, 2) ?></h2>
                                            </div></a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="widget style1 bg-success">
                                    <div class="row">
                                        <a style="color: #FFF;" href="javascript:void(0);"><div class="col-xs-12 text-center">
                                                <span> My Earnings </span>

                                                <h2 class="font-bold"><i class="fa fa-inr"></i> <?= round($earnings->admin_earnings, 2) ?></h2>
                                            </div></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="widget style1 bg-success">
                                    <div class="row">
                                        <a style="color: #FFF;" href="javascript:void(0);"><div class="col-xs-12 text-center">
                                                <span> Tax Earnings </span>

                                                <h2 class="font-bold"><i class="fa fa-inr"></i> <?= round($earnings->tax_earnings, 2) ?></h2>
                                            </div></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="widget style1 bg-warning">
                                    <div class="row">
                                        <a style="color: #FFF;" href="javascript:void(0);"><div class="col-xs-12 text-center">
                                                <span> Franchise Earnings </span>

                                                <h2 class="font-bold"><i class="fa fa-inr"></i> <?= round($earnings->franchise_earnings, 2) ?></h2>
                                            </div></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="widget style1 bg-danger">
                                    <div class="row">
                                        <a style="color: #FFF;" href="javascript:void(0);"><div class="col-xs-12 text-center">
                                                <span> Service Provider Earnings </span>

                                                <h2 class="font-bold"><i class="fa fa-inr"></i> <?= round($earnings->provider_earnings, 2) ?></h2>
                                            </div></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <ul class="nav nav-tabs">
                            <li class="nav-item <?= empty($this->input->get('stat')) ? 'active' : '' ?>">
                                <a class="nav-link active" aria-current="page" href="<?= base_url('admin/services_orders/') ?>">All Orders</a>
                            </li>
                            <li class="nav-item <?= ($this->input->get('stat') == "ongoing") ? 'active' : '' ?>">
                                <a class="nav-link active" aria-current="page" href="<?= base_url('admin/services_orders/?stat=ongoing') ?>">On Going</a>
                            </li>
                            <li class="nav-item <?= ($this->input->get('stat') == "accepted") ? 'active' : '' ?>">
                                <a class="nav-link" href="<?= base_url('admin/services_orders/?stat=accepted') ?>">Accepted / Started</a>
                            </li>
                            <li class="nav-item <?= ($this->input->get('stat') == "completed") ? 'active' : '' ?>">
                                <a class="nav-link" href="<?= base_url('admin/services_orders/?stat=completed') ?>">Completed</a>
                            </li>
                            <li class="nav-item <?= ($this->input->get('stat') == "cancelled") ? 'active' : '' ?>">
                                <a class="nav-link" href="<?= base_url('admin/services_orders/?stat=cancelled') ?>">Cancelled</a>
                            </li>
                        </ul>
                    <?php } ?>

                    <form>
                        <div class="row" style="padding: 30px">
                            <div class="col-md-3">
                                <label>From Date : </label>
                                <input type="date" class="form-control" name="from_date" value="<?= $this->input->get('from_date'); ?>">
                            </div>
                            <div class="col-md-3">
                                <label>To Date : </label>
                                <input type="date" class="form-control" name="to_date" value="<?= $this->input->get('to_date'); ?>">
                            </div>
                            <div class="col-md-3">
                                <label>Time Slot Date : </label>
                                <input type="date" class="form-control" name="time_slot_date" value="<?= $this->input->get('time_slot_date'); ?>">
                            </div>
                            <div class="col-md-3">
                                <label>Services Type</label>
                                <select name="visit_and_quote" class="form-control">
                                    <option value="">All</option>
                                    <option value="Yes" <?= ( $this->input->get('visit_and_quote') == "Yes") ? 'selected' : '' ?>>Visit and Quote Services</option>
                                    <option value="No" <?= ( $this->input->get('visit_and_quote') == "No") ? 'selected' : '' ?>>Regular Services</option>
                                </select>
                            </div>
                            <div class="col">
                                <button class="btn btn-primary" style="margin-top: 22px">Filter</button>
                                <a href="<?= base_url('admin/services_orders/') ?>" type="button" class="btn btn-danger" style="margin-top: 22px">Reset</a>
                            </div>
                        </div>
                    </form>

                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>

                            <tr>
                                <th>#</th>
                                <th>Order Id</th>
                                <th>User Details</th>
                                <th>Time Slot</th>
                                <th>Ordered Categories</th>
                                <?php if ($_SESSION['admin_login']['user_type'] != 'franchise') { ?>
                                    <th>Ordered Settlement Amounts</th>
                                <?php } ?>
                                <th>User Address</th>
                                <th>Visit and Quote</th>
                                <th>Amount Paid</th>
                                <th>Ordered On</th>
                                <th>Ordered Status</th>
                                <th>Action</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($data as $index => $item) { ?>
                                <tr>
                                    <td><?= $index + 1 ?></td>
                                    <td><?= $item->order_id ?></td>
                                    <td>
                                        <p>
                                            <b>Name : </b><?= ($item->customer_details->first_name) ? $item->customer_details->first_name : 'N/A' ?>
                                        </p>
                                        <p>
                                            <b>Email : </b><?= ($item->customer_details->email) ? $item->customer_details->email : 'N/A' ?>
                                        </p>
                                        <p>
                                            <b>Phone : </b><?= ($item->customer_details->phone) ? $item->customer_details->phone : 'N/A' ?>
                                        </p>
                                    </td>
                                    <td>
                                        <p><b>Date : </b><?= $item->time_slot_date ?></p>
                                        <p><b>Time : </b><?= $item->time_slot ?></p>
                                    </td>
                                    <td>
                                        <p><?php
                                            foreach ($item->selected_categories as $in => $it) {
                                                if ($in != 0) {
                                                    echo ', ' . $it->name;
                                                } else {
                                                    echo $it->name;
                                                }
                                            }
                                            ?>
                                        </p>
                                    </td>
                                    <?php if ($_SESSION['admin_login']['user_type'] != 'franchise') { ?>
                                        <td>
                                            <b>Total Amount : </b><?= $item->sub_total + $item->visiting_charges + $item->tax ?><br>
                                            <b>Admin Commission : </b><?= ($item->admin_comission) ? $item->admin_comission : "N/A" ?><br>
                                            <b>Franchise Commission : </b><?= ($item->franchise_comission) ? $item->franchise_comission : "N/A" ?><br>
                                            <b>Service Provider Commission : </b><?= ($item->service_provider_comission) ? $item->service_provider_comission : "N/A" ?><br>
                                            <b>Gst : </b><?= ($item->gst) ? $item->gst : "N/A" ?>
                                        </td>
                                    <?php } ?>
                                    <td>
                                        <p><?= $item->customer_address->address . ', ' ?><br>
                                            <?= $item->customer_address->landmark . ', ' . $item->customer_address->city_name . ', ' . $item->customer_address->state_name . ', ' . $item->customer_address->pincode ?>
                                        </p>
                                    </td>
                                    <td>
                                        <?php if ($item->has_visit_and_quote == 'Yes') { ?>
                                            <span style="color: green"><?= $item->has_visit_and_quote ?></span>
                                        <?php } else { ?>
                                            <span style="color: tomato"><?= $item->has_visit_and_quote ?></span>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <?= $item->amount_paid ?>
                                    </td>
                                    <td>
                                        <?= date('d M Y, h:i A', $item->created_at) ?>
                                    </td>
                                    <td style="color: <?= ($item->order_status == "order_completed") ? 'green' : '' ?><?= ($item->order_status == "order_cancelled" || $item->order_status == "order_rejected" || $item->order_status == "refunded") ? 'red' : '' ?>">
                                        <?= ucwords(str_replace("_", " ", $item->order_status)) ?>
                                    </td>
                                    <td>
                                        <a class="btn btn-success btn-xs" href="<?= base_url('admin/services_orders/view/' . $item->id) ?>">View Order</a>
                                        <?php if ($_SESSION['admin_login']['user_type'] != 'franchise' && $item->order_status == "order_cancelled") { ?>
                                            <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#approveCancellation" onclick="assignCancel('<?= $item->id ?>')">Approve Cancellation</button>
                                        <?php } ?>
                                        <?php if ($item->order_status == "order_placed") { ?>
                                            <button onclick="req_cancel_order('<?= $item->user_id ?>', '<?= $item->id ?>');" class="btn btn-xs btn-danger">Cancel Order</button>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="modal fade " tabindex="-1" role="dialog" id="approveCancellation">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Cancel Order </b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="order_id_cancel"/>
                <span><b>Please provide the Refund Transaction or the other details these details are displayed to the User.</b></span>
                <textarea class="form-control" id="cancellation_reason" rows="5"></textarea>
                <p id="cancellation_reason_err" style="color: tomato"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="cancel_order();">Cancel Order</button>
            </div>
        </div>
    </div>
</div>
<script>
    function assignCancel(order_id) {
        console.log(order_id);
        $("#order_id_cancel").val(order_id);
    }

    function req_cancel_order(user_id, order_id) {
        if (confirm("Are you sure want to Cancel this Order ? ")) {
            $.ajax({
                url: "<?= base_url('api/orders/cancel_order') ?>",
                type: "post",
                data: {user_id: user_id, order_id: order_id, cancellation_reason: "cancelled by the Admin"},
                success: function (resp) {
                    resp = JSON.parse(resp);
                    if (resp['status']) {
                        swal({
                            title: resp['message'],
                            icon: "warning"
                        }).then(function () {
                            location.href = '';
                        });
                    } else {
                        swal({
                            title: resp['message'],
                            icon: "warning"
                        });
                    }
                }
            });
        }
    }

    function cancel_order() {
        var cancellationReason = $("#cancellation_reason").val();
        var order_id = $("#order_id_cancel").val();
        if (cancellationReason.length > 15) {
            $("#cancellation_reason_err").html("");
            $.ajax({
                url: "<?= base_url('api/admin_ajax/admin/cancel_order') ?>",
                type: "post",
                data: {order_id: order_id, cancellation_reason: cancellationReason},
                success: function (resp) {
//                    console.log(resp);
//                    resp = JSON.parse(resp);
                    if (resp['status']) {
                        swal({
                            title: resp['message'],
                            icon: "success"
                        }).then(function () {
                            location.href = '';
                        });
                    } else {
                        swal({
                            title: resp['message'],
                            icon: "info"
                        });
                    }
                }
            });
        } else {
            $("#cancellation_reason_err").html("Please Enter Breif Details.");
        }
    }
</script>
