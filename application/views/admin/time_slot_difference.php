<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= $title ?></h5>
                <div class="ibox-tools">
                    <a href="<?= base_url() ?>admin/dashboard">
                        <button class="btn btn-primary">BACK</button>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form method="post" class="form-horizontal" enctype="multipart/form-data" id="slot-form"  action="<?= base_url() ?>admin/time_slot_difference/update">
                    <span style="float: right"><b>Last Updated On : </b><?= date('d M Y, h:i A', $time_slots_difference->updated_at) ?></span>
                    <br>                    <br>

                    <div class="form-group col-md-10">
                        <label class="col-sm-2 control-label">Time Slot Difference (in Hours)*</label>
                        <div class="col-sm-10">
                            <input type="number" name="difference" id="difference" class="form-control" value="<?= $time_slots_difference->difference ?>" required>
                        </div>
                    </div>
                    <br><br>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-9 ">
                            <button class="btn btn-primary pull-right" type="submit" id="btn_category"> Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script src="<?= base_url('web_assets/') ?>/js/plugins/parsleyjs/dist/parsley.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#slot-form').parsley();
    });
//    function validateEmail($email)
//    {
//        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
//        if (!emailReg.test($email)) {
//            return false;
//        } else
//        {
//            return true;
//        }
//    }
</script>