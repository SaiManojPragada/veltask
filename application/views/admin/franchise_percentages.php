<style>
    .category_comm_span{
        top: -5px;
        position: relative;
        left: 10px;
    }
    .cat_commission{
        top: -5px;
        position: relative;
        left: 21px;
    }
</style>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= $title ?></h5>
                <div class="ibox-tools">
                    <a href="<?= base_url() ?>admin/franchises">
                        <button class="btn btn-primary">BACK</button>
                    </a>
                </div>
                <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                    <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                    </div>
                <?php } ?>
                <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                    <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                    </div>
                <?php }
                ?>
            </div>
            <div class="ibox-content test">
                <form method="post" class="form-horizontal" enctype="multipart/form-data" action="<?= base_url() ?>admin/franchises/manage_percentages/<?= $franchise_id ?>">
                    <br>
                    <?php
                    $count = 0;
                    foreach ($price_points as $point) {
                        ?>
                        <div class="form-group col-md-6">
                            <label class="col-sm-2 control-label">Price</label>
                            <div class="col-sm-10">
                                <input name="price_point_id[]" type="hidden" value="<?= $point->id ?>">
                                <input type="text" id="price_point_id" disabled class="form-control" value="<?= $point->price ?>">
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="col-sm-2 control-label">Percentage % *</label>
                            <div class="col-sm-10">
                                <input type="text" id="percentage" name="percentage[]" class="form-control" value="<?php
                                if ($franchises_commissions) {
                                    echo $franchises_commissions[$count++]->commission;
                                }
                                ?>">
                            </div>
                        </div>
                    <?php } ?>

                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" id="btn_francises" name="btn_francises" value="hah" type="submit">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>