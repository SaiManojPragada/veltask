<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= $title ?></h5>
                <div class="ibox-tools">
                    <a href="<?= base_url() ?>admin/services">
                        <button class="btn btn-primary">BACK</button>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form method="post" class="form-horizontal" enctype="multipart/form-data"  action="<?= base_url() ?>admin/services_time_slots/<?= $func ?><?= $data->id ?>">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Time Slot</label>
                        <div class="col-sm-10">
                            <input type="text" name="time_slot" id="time_slot" class="form-control" value="<?= $data->time_slot ?>">
                        </div>
                    </div>
                    <button class="btn btn-primary btn-sm" type="submit" > Submit </button>
                </form>
            </div>
        </div>
    </div>
</div>