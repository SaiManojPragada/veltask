<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">

        <div class="col-lg-12">

            <div class="ibox float-e-margins">

                <div class="ibox-title">

                    <h5>Pincode</h5>

                    <div class="ibox-tools">
                         <a href="<?= base_url() ?>admin/dashboard">
                            <button class="btn btn-primary">BACK</button>
                        </a> 

                         <?php 
                         $user_type = $_SESSION['admin_login']['user_type']; 
                         if($user_type=='subadmin'){ 
                                $admin_id = $_SESSION['admin_login']['id']; 
                                $adm_qry = $this->db->query("select * from sub_admin where id='".$admin_id."'");
                                $adm_row=$adm_qry->row();

                                $userpermissions  = $adm_row->permissions; 
                                $permissions = explode(",", $userpermissions);
                        if (in_array("add_pincode", $permissions)){ ?>

                        <a href="<?= base_url() ?>admin/pincodes/add">
                            <button class="btn btn-primary">+ Add Pincode</button>
                        </a>
                    <?php } }else{ ?>
                        <a href="<?= base_url() ?>admin/pincodes/add">
                            <button class="btn btn-primary">+ Add Pincode</button>
                        </a>
                    <?php } ?>
                        

                    </div>

                    <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                        <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                        </div>
                    <?php } ?>
                    <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                        <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                        </div>
                    <?php }
                    ?>

                </div>

                <div class="ibox-content">



                    <table class="table table-striped table-bordered table-hover dataTables-example">

                        <thead>

                            <tr>

                                <th>#</th>
                                <th>State Name</th>
                                <th>City Name</th>
                                <th>Pincode</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>

                        </thead>

                        <tbody>

                            <?php

                            $i = 1;

                            foreach ($pincodes as $loc) {
                                $stat = $this->db->query("select * from states where id='".$loc->state_id."'");
                                $states = $stat->row();
                                $cit = $this->db->query("select * from cities where id='".$loc->city_id."'");
                                $cities = $cit->row();

                                $shop = $this->db->query("select * from vendor_shop where id='".$loc->shop_id."'");
                                $shops = $shop->row();
                                ?>

                                <tr class="gradeX">

                                    <td><?= $i ?></td>

                                    <td><?= $states->state_name; ?></td>

                                    <td><?= $cities->city_name; ?></td>

                                    <td><?= $loc->pincode; ?></td>
                                    <td><?php if($loc->status==0){ echo "Inactive"; }else if($loc->status==1){ echo "Active"; } ?></td>
                                   
                                    <td>
                                        <?php
                                        if($user_type=='subadmin'){ 
                                         if (in_array("edit_pincode", $permissions)){ ?>
                                        <a href="<?= base_url() ?>admin/pincodes/edit/<?= $loc->id ?>"><button class="btn btn-xs btn-primary">Edit</button></a>
                                        <?php } 
                                                if (in_array("delete_pincode", $permissions)){
                                         ?>

                                        <a href="<?= base_url() ?>admin/pincodes/delete/<?= $loc->id; ?>"><button class="btn btn-xs btn-danger" onclick="if(!confirm('Are you sure you want to delete this Pincode?')) return false;">Delete</button></a>
                                    <?php }  }else{   ?>
                                            <a href="<?= base_url() ?>admin/pincodes/edit/<?= $loc->id ?>"><button class="btn btn-xs btn-primary">Edit</button></a>
                                        
                                            <a href="<?= base_url() ?>admin/pincodes/delete/<?= $loc->id; ?>"><button class="btn btn-xs btn-danger" onclick="if(!confirm('Are you sure you want to delete this Pincode?')) return false;">Delete</button></a>
                                    <?php } ?>
                                    </td>

                                </tr>
                                <?php
                                $i++;
                              }
                            ?>

                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>





</div>



