<!-- Mainly scripts -->

<script src="<?= ADMIN_ASSETS_PATH ?>assets/js/bootstrap.min.js"></script>
<script src="<?= ADMIN_ASSETS_PATH ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?= ADMIN_ASSETS_PATH ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<script src="<?= ADMIN_ASSETS_PATH ?>assets/js/plugins/dataTables/datatables.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?= ADMIN_ASSETS_PATH ?>assets/js/inspinia.js"></script>
<script src="<?= ADMIN_ASSETS_PATH ?>assets/js/plugins/pace/pace.min.js"></script>
<script src="https://cdn.ckeditor.com/4.17.1/full-all/ckeditor.js"></script>
<script>
    setTimeout(function () {
        var editors = document.getElementsByClassName("ck-editor");
        for (var i = 0; i < editors.length; i++) {
            CKEDITOR.replace(editors[i]);
        }
    }, 500);

</script>
<script>


    $(".chosen-select").chosen();


    $(document).ready(function () {
        $('.dataTables-example').DataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                //{extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'ExampleFile'},
                //{extend: 'pdf', title: 'ExampleFile'},

                {extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ]

        });

    });

</script>


<link href="<?= ADMIN_ASSETS_PATH ?>assets/js/select2.min.css" rel="stylesheet" /> 
<script src="<?= ADMIN_ASSETS_PATH ?>assets/js/select2.min.js"></script>
<script>
    $(document).ready(function () {
        $('.js-example-basic-multiple').select2({
            placeholder: "Select"
        });
    });
</script> 
<style>
    .parsley-errors-list{
        list-style:none;
    }
    .parsley-required , .parsley-errors-list li{
        color:red;
        list-style:none;
        margin-left:-30px;
    }
    .alert *{
        color: white !important;
    }
</style>

<script type="text/javascript">
    setTimeout(function () {
        $('.alert-success').fadeOut('fast');
    }, 3000);
</script>
</div>
</div>
</div>
<div style="width: 100%; height: 100%; z-index: 100000; position: absolute; background-color: #eaeaea; top: 0; opacity: 0.4;
     display: none" id="theloader">
    <center>
        <img src="<?= base_url('uploads/loader.gif') ?>" alt="alt" style="opacity: 1; z-index: 999999999; margin-top: 8%"/>
    </center>
</div>
</body>
</html>
