<!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Veltask | Dashboard</title>

        <link href="<?= ADMIN_ASSETS_PATH ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?= ADMIN_ASSETS_PATH ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">

        <!-- Toast style -->
        <link href="<?= ADMIN_ASSETS_PATH ?>assets/css/plugins/toastr/toastr.min.css" rel="stylesheet">

        <!-- Gritter -->
        <link href="<?= ADMIN_ASSETS_PATH ?>assets/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

        <link href="<?= ADMIN_ASSETS_PATH ?>assets/css/animate.css" rel="stylesheet">
        <link href="<?= ADMIN_ASSETS_PATH ?>assets/css/style.css" rel="stylesheet">
        <link href="<?= ADMIN_ASSETS_PATH ?>assets/css/custom_css.css" rel="stylesheet">
        <link href="<?= ADMIN_ASSETS_PATH ?>assets/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

        <script src="<?= ADMIN_ASSETS_PATH ?>assets/js/jquery-2.1.1.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.css">
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    </head>

    <body>
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav metismenu" id="side-menu" style="height: 100vh; overflow-y: scroll;">
                        <li class="nav-header">
                            <div class="dropdown profile-element">
                                <span>
                                         <!--<img alt="image" class="img-circle" src="<?= ADMIN_ASSETS_PATH ?>assets/img/profile_small.jpg" />-->
                                </span>
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                    <span class="clear">
                                        <span class="block m-t-xs">
                                            <strong class="font-bold">Veltask</strong>
                                        </span>
                                        <span class="text-muted text-xs block">Admin Control Panel <!-- <b class="caret"></b> -->
                                        </span>
                                    </span>
                                </a>

                            </div>
                            <div class="logo-element">
                                Veltask
                            </div>
                        </li>

                        <?php
                        $user_type = $_SESSION['admin_login']['user_type'];
                        if ($user_type == 'admin') {
                            ?>


                            <li class="<?= $page_name == 'dashboard' ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>admin/dashboard"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>admin/loginLogs"><i class="fa fa-users"></i> <span class="nav-label">Admin Login Logs </span></a>
                            </li>
                            <li class="<?php
                            if ($page_name == 'payments_management' || $page_name == "prepay") {
                                echo 'active';
                            }
                            ?>">
                                <a href="<?= base_url() ?>admin/payment_management"><i class="fa fa-money"></i>   <span class="nav-label">Manage Payments</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-second-level collapse">
                                    <li  class="<?= $page_name == 'payments_management' ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>admin/payment_management">Payment types</a>
                                    </li>
                                    <li  class="<?= $page_name == 'prepay' ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>admin/prepay_percentage_management">Prepay Percentrage</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="<?= $page_name == 'onboard_screens' ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>admin/onboard_screens"><i class="fa fa-image"></i> <span class="nav-label">Onboard Images</span></a>
                            </li>
                            <li class="<?php
                            if ($page_name == 'states' || $page_name == 'cities' || $page_name == 'locations' || $page_name == 'pincodes') {
                                echo 'active';
                            }
                            ?>">
                                <a href="<?= base_url() ?>admin/states"><i class="fa fa-table"></i>  <span class="nav-label">Master Locations</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-second-level collapse">
                                    <li  class="<?= $page_name == 'states' ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>admin/states">States</a>
                                    </li>
                                    <li class="<?= $page_name == 'cities' ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>admin/cities">Cities</a>
                                    </li>
                                    <li class="<?= $page_name == 'pincodes' ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>admin/pincodes">Pincode</a>
                                    </li>

                                    <li class="<?= $page_name == 'locations' ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>admin/locations">Areas</a>
                                    </li>
                                </ul>
                            </li>


                            <li class="<?= $page_name == 'about_us' ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>admin/about_us"><i class="fa fa-info-circle"></i> <span class="nav-label">About Us</span></a>
                            </li>


                            <li class="<?= $page_name == 'blogs' ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>admin/blogs"><i class="fa fa-comments-o"></i> <span class="nav-label">Blogs</span></a>
                            </li>


                            <li class="<?= $page_name == 'faqs' ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>admin/faq"><i class="fa fa-question"></i> <span class="nav-label">FAQ's</span></a>
                            </li>

                            <li class="<?php
                            if ($page_name == 'testimonials' || $page_name == 'franchise_tabs') {
                                echo 'active';
                            }
                            ?>">
                                <a href="<?= base_url() ?>admin/testimonials"><i class="fa fa-table"></i><span class="nav-label">Manage Homepage</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-second-level collapse">
                                    <li  class="<?= $page_name == 'testimonials' ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>admin/testimonials">Testimonials</a>
                                    </li>   

                                    <li  class="<?= $page_name == 'franchise_tabs' ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>admin/franchise_tabs">Franchise Counts</a>
                                    </li>
                                </ul>
                            </li>



                            <li class="<?= $page_name == 'price_points' ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>admin/price_points"><i class="fa fa-money"></i> <span class="nav-label">Price Points</span></a>
                            </li>


                            <li class="<?= $page_name == 'dashboard' ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>admin/manage_referrals"><i class="fa fa-ticket"></i> <span class="nav-label">Referrals</span></a>
                            </li>

                            <li class="<?php
                            if ($page_name == 'memberships' || $page_name == 'business_benefits' || $page_name == 'business_document_types') {
                                echo 'active';
                            }
                            ?>">
                                <a href="<?= base_url() ?>admin/services_categories"><i class="fa fa-ticket"></i><span class="nav-label">Manage Benefits</span> 
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-second-level collapse">
                                    <li class="<?= $page_name == 'memberships' ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>admin/memberships">Memberships</a>
                                    </li>
                                    <li class="<?= $page_name == 'business_benefits' ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>admin/business_benefits">Business Benefits</a>
                                    </li>
                                    <li class="<?= $page_name == 'business_document_types' ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>admin/business_document_types">Business Document types</a>
                                    </li>
                                </ul>
                            </li>
                            <!--                            <li class="<?php
                            if ($page_name == 'memberships') {
                                echo 'active';
                            }
                            ?>">
                                                            <a href="<?= base_url() ?>admin/memberships"><i class="fa fa-ticket"></i> <span class="nav-label">Memberships</span></a>
                                                        </li>-->

                            <li>
                                <a href="<?= base_url() ?>admin/Sub_admin"><i class="fa fa-th-large"></i> <span class="nav-label">Sub Admins</span></a>
                            </li>
                            <li class="<?php
                            if ($page_name == 'franchises' || $page_name == 'franchises_requests' || $page_name == 'admin_franchise_services_withdraw_requests') {
                                echo 'active';
                            }
                            ?>">
                                <a href="<?= base_url() ?>admin/franchises"><i class="fa fa-ticket"></i>  <span class="nav-label">Franchises</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-second-level collapse">
                                    <li class="<?= $page_name == 'franchises' ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>admin/franchises">Franchises</a>
                                    </li>
                                    <li class="<?= $page_name == 'franchises_requests' ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>admin/franchises/franchises_requests">Franchises Requests</a>
                                    </li>


                                    <li class="<?= $page_name == 'admin_franchise_services_withdraw_requests' ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>admin/admin_franchise_services_withdraw_requests">Franchises Services Withdraw Requests</a>
                                    </li>
                            </li>






                        </ul>









                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <!--                            <li class="<?= $page_name == 'coupons' ? 'active' : '' ?>">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <a href="<?= base_url() ?>admin/coupons"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Coupon codes</span></a>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            </li>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <li class="<?= $page_name == 'cashcoupons' ? 'active' : '' ?>">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <a href="<?= base_url() ?>admin/cashcoupons"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Cash Coupon</span></a>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            </li>-->


                        <!--                            <li class="<?php
                        if ($page_name == 'vendor_banners') {
                            echo 'active';
                        }
                        ?>">
                                                        <a href="<?= base_url() ?>admin/vendor_banners"><i class="fa fa-th-large"></i> <span class="nav-label">Vendor Banners</span></a>
                                                    </li>
                        
                                                    <li class="<?= $page_name == 'orders' ? 'active' : '' ?>">
                                                        <a href="<?= base_url() ?>admin/orders"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Orders</span></a>
                                                    </li>
                                                    <li class="<?= $page_name == 'bid_orders' ? 'active' : '' ?>">
                                                        <a href="<?= base_url() ?>admin/bid_orders"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Bid Orders</span></a>
                                                    </li>-->








                        <li class="<?= $page_name == 'users' || $page_name == 'company_profiles' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>admin/users"><i class="fa fa-table"></i> Manage Users
                                <span class="fa arrow"></span>
                            </a>
                            <ul class="nav nav-second-level collapse">
                                <li  class="<?= $page_name == 'users' ? 'active' : '' ?>">
                                    <a href="<?= base_url() ?>admin/users">Users</a>
                                </li>
                                <li class="<?= $page_name == 'company_profiles' ? 'active' : '' ?>">
                                    <a href="<?= base_url() ?>admin/company_profiles">Business profiles</a>
                                </li>
                            </ul>
                        </li>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         

                        <!-- 
                       
                           <li class="<?= $page_name == 'content' ? 'active' : '' ?>">
                           <a href="<?= base_url() ?>admin/content"><i class="fa fa-book"></i> <span class="nav-label">CMS Pages</span></a>
                           </li>
                       
                       
                           <li class="<?= $page_name == 'enquiries' ? 'active' : '' ?>">
                           <a href="<?= base_url() ?>admin/enquiries"><i class="fa fa-comments-o"></i> <span class="nav-label">Enquiries</span></a>
                           </li>
                       
                       
                       
                           <li class="<?= $page_name == 'settings' ? 'active' : '' ?>">
                           <a href="<?= base_url() ?>admin/settings"><i class="fa fa-cog"></i> <span class="nav-label">Settings</span></a>
                           </li>
                           <li class="<?= $page_name == 'site_settings' ? 'active' : '' ?>">
                           <a href="<?= base_url() ?>admin/site_settings"><i class="fa fa-cog"></i> <span class="nav-label">Site Settings</span></a>
                           </li>
                        <!--Serives Module RElated ********************************************************************************--> 

                        <li class="<?= $page_name == 'contactus' ? 'active' : '' ?>" >
                            <a href="<?= base_url() ?>admin/contactus"><i class="fa fa-user"></i> <span class="nav-label">User Contact Us</span></a>
                        </li>
                        <li class="<?= $page_name == 'settings' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>admin/settings"><i class="fa fa-cog"></i> <span class="nav-label">Settings</span></a>
                        </li>
                        <li class="<?= $page_name == 'content' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>admin/content"><i class="fa fa-book"></i> <span class="nav-label">CMS Pages</span></a>
                        </li>
                        <li class="<?= $page_name == 'site_settings' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>admin/site_settings"><i class="fa fa-cog"></i> <span class="nav-label">Site Settings</span></a>
                        </li>

                        <li>
                            <a>
                                <span class="nav-label">Services</span>
                                <hr>
                            </a>
                        </li>

                        <li class="<?php
                        if ($page_name == 'services_time_slots' || $page_name == 'services_coupons' || $page_name == 'services_categories' || $page_name == 'services_sub_categories' || $page_name == 'services' || $page_name == 'services_banners') {
                            echo 'active';
                        }
                        ?>">
                            <a href="<?= base_url() ?>admin/services_categories"><i class="fa fa-table"></i><span class="nav-label">Master Services</span>
                                <span class="fa arrow"></span>
                            </a>
                            <ul class="nav nav-second-level collapse">
                                <li class="<?= $page_name == 'services_categories' ? 'active' : '' ?>">
                                    <a href="<?= base_url() ?>admin/services_categories">Categories</a>
                                </li>
                                <li class="<?= $page_name == 'services_sub_categories' ? 'active' : '' ?>">
                                    <a href="<?= base_url() ?>admin/services_sub_categories">Sub Categories</a>
                                </li>
                                <li class="<?= $page_name == 'services' ? 'active' : '' ?>">
                                    <a href="<?= base_url() ?>admin/services">Services</a>
                                </li>
                                <li class="<?= $page_name == 'services_coupons' ? 'active' : '' ?>">
                                    <a href="<?= base_url() ?>admin/services_coupons">Coupons</a>
                                </li>
                                <li class="<?= $page_name == 'services_banners' ? 'active' : '' ?>">
                                    <a href="<?= base_url() ?>admin/services_banners">Banners</a>
                                </li>
                                <li class="<?= $page_name == 'services_time_slots' ? 'active' : '' ?>">
                                    <a href="<?= base_url() ?>admin/services_time_slots">Time Slots</a>
                                </li>
                            </ul>
                        </li>


                        <li class="<?= $page_name == 'active_service_provides' || $page_name == 'inactive_service_provides' || $page_name == 'service_provider_features' || $page_name == 'provider_balances' ? 'active' : '' ?>">
                            <a><i class="fa fa-table"></i> <span class="nav-label">Service Providers</span> 
                                <span class="fa arrow"></span>
                            </a>
                            <ul class="nav nav-second-level collapse">
                                <li  class="<?= $page_name == 'active_service_provides' ? 'active' : '' ?>">
                                    <a href="<?= base_url() ?>admin/service_providers">Active Providers</a>
                                </li>
                                <li class="<?= $page_name == 'inactive_service_provides' ? 'active' : '' ?>">
                                    <a href="<?= base_url() ?>admin/service_providers?status=inactive">Inactive Providers</a>
                                </li>
                                <li class="<?= $page_name == 'provider_balances' ? 'active' : '' ?>">
                                    <a href="<?= base_url() ?>admin/provider_balances">Providers Balances</a>
                                </li>
                                <li class="<?= $page_name == 'service_provider_features' ? 'active' : '' ?>">
                                    <a href="<?= base_url() ?>admin/service_provider_features">App Features</a>
                                </li>
                            </ul>
                        </li>

                        <li class="<?= $page_name == 'services_orders' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>admin/services_orders"><i class="fa fa-files-o"></i> <span class="nav-label">Services Orders</span></a>
                        </li>



                        <li class="<?= $page_name == 'service_provider_withdraw_requests' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>admin/service_provider_withdraw_requests"><i class="fa fa-money"></i> <span class="nav-label">Payment Requests</span></a>
                        </li>


                        <li class="<?= $page_name == 'time_slot_difference' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>admin/time_slot_difference"><i class="fa fa-clock-o"></i> <span class="nav-label">Min Time Slot Gap</span></a>
                        </li>



                        <li class="<?= $page_name == 'service_cancellation_grace_period' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>admin/service_cancellation_grace_period"><i class="fa fa-clock-o"></i> <span class="nav-label">Cancellation Grace Period</span></a>
                        </li>



                        <!--E-commerce Module RElated ********************************************************************************-->
                        <li>
                            <a>
                                <span class="nav-label">E-Commerce</span> 

                                <hr>
                            </a>
                        </li>

                        <li >
                            <a href="#"><i class="fa fa-table"></i> <span class="nav-label">E-commerce Master</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse in">



                                <li class="<?= $page_name == 'banners' ? 'active' : '' ?>">
                                    <a href="<?= base_url() ?>admin/banners">Banners</a>
                                </li>

                                <li class="<?= $page_name == 'categories' ? 'active' : '' ?>">
                                    <a href="<?= base_url() ?>admin/categories">Categories</a>
                                </li>
                                <li class="<?= $page_name == 'sub_categories' ? 'active' : '' ?>">
                                    <a href="<?= base_url() ?>admin/sub_categories">Sub Categories</a>
                                </li>


                                <li class="<?php
                                if ($page_name == 'attributes' || $page_name == 'manage_attributes' || $page_name == 'brands' || $page_name == 'tags' || $page_name == 'tax') {
                                    echo 'active';
                                }
                                ?>">
                                    <a href="#">Product Attributes
                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul class="nav nav-second-level collapse">
                                        <li class="<?= $page_name == 'attributes' ? 'active' : '' ?>">
                                            <a href="<?= base_url() ?>admin/attributes">Attributes Types</a>
                                        </li> 
                                        <li class="<?= $page_name == 'manage_attributes' ? 'active' : '' ?>">
                                            <a href="<?= base_url() ?>admin/manage_attributes">Manage Attributes</a>
                                        </li>
                                        <li class="<?= $page_name == 'brands' ? 'active' : '' ?>">
                                            <a href="<?= base_url() ?>admin/brands">Brands</a>
                                        </li>
                                        <li class="<?= $page_name == 'tags' ? 'active' : '' ?>">
                                            <a href="<?= base_url() ?>admin/tags">Tags</a>
                                        </li>
                                    </ul>

                                </li>

                            </ul>
                        </li>

                        <li  class="<?= $page_name == 'vendors_shops' || $page_name == 'inactive_vendors_shops' || $page_name == 'vendors_shops/add' ? 'active' : '' ?>">
                            <a><i class="fa fa-table"></i> <span class="nav-label">E-Com Vendors</span>
                                <span class="fa arrow"></span>
                            </a>
                            <ul class="nav nav-second-level collapse">
                                <li  class="<?= $page_name == 'vendors_shops' ? 'active' : '' ?>">
                                    <a href="<?= base_url() ?>admin/vendors_shops">Active Vendors</a>
                                </li>
                                <li class="<?= $page_name == 'inactive_vendors_shops' ? 'active' : '' ?>">
                                    <a href="<?= base_url() ?>admin/inactive_vendors_shops">Inactive Vendors</a>
                                </li>
                            </ul>
                        </li>


                        <li class="<?php
                        if ($page_name == 'products' || $page_name == 'inactive_products') {
                            echo 'active';
                        }
                        ?>">
                            <a><i class="fa fa-table"></i><span class="nav-label">Products</span> 
                                <span class="fa arrow"></span>
                            </a>
                            <ul class="nav nav-second-level collapse">
                                <li  class="<?= $page_name == 'products' ? 'active' : '' ?>">
                                    <a href="<?= base_url() ?>admin/products">Active Products</a>
                                </li>
                                <li class="<?= $page_name == 'inactive_products' ? 'active' : '' ?>">
                                    <a href="<?= base_url() ?>admin/inactive_products">Inactive Products</a>
                                </li>
                            </ul>
                        </li>

                        <li class="<?= $page_name == 'delivery_boy' || $page_name == 'delivery_boy/add' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>admin/delivery_boy"><i class="fa fa-truck"></i> <span class="nav-label">Delivery Boys</span>

                            </a>
                        </li>


                        <li class="<?= $page_name == 'coupons' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>admin/coupons"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Coupon codes</span></a>
                        </li>
                        <li class="<?= $page_name == 'cashcoupons' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>admin/cashcoupons"><i class="fa fa-shopping-cart"></i> <span class="nav-label">E-Comm Coupon</span></a>
                        </li>


                        <li class="<?php
                        if ($page_name == 'vendor_banners') {
                            echo 'active';
                        }
                        ?>">
                            <a href="<?= base_url() ?>admin/vendor_banners"><i class="fa fa-th-large"></i> <span class="nav-label">Vendor Banners</span></a>
                        </li>

                        <li class="<?= $page_name == 'orders' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>admin/orders"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Orders</span></a>
                        </li>
                        <li class="<?= $page_name == 'settlements' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>/admin/settlements"><i class="fa fa fa-money"></i> <span class="nav-label">VENDOR Settlements</span></a>
                        </li> 

                        <li class="<?= $page_name == 'franchise_settlements' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>/admin/franchise_settlements"><i class="fa fa fa-money"></i> <span class="nav-label">Franchise Settlements</span></a>
                        </li> 

                        <li class="<?= $page_name == 'pushnotifications_list' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>admin/Pushnotifications"><i class="fa fa-bell-o"></i> <span class="nav-label">Push Notifications</span></a>
                        </li>


                        <?php
                    } else if ($user_type == 'subadmin') {
                        $admin_id = $_SESSION['admin_login']['id'];
                        $adm_qry = $this->db->query("select * from sub_admin where id='" . $admin_id . "'");
                        $adm_row = $adm_qry->row();

                        $userpermissions = $adm_row->permissions;
                        $permissions = explode(",", $userpermissions);
                        ?>

                        <?php if (in_array("dashboard", $permissions)) { ?>

                            <li>
                                <a href="<?= base_url() ?>admin/dashboard"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a>
                            </li>
                        <?php } if (in_array("state", $permissions) || in_array("city", $permissions) || in_array("pincode", $permissions) || in_array("area", $permissions) || in_array("banners", $permissions) || in_array("categories", $permissions) || in_array("sub_categories", $permissions) || in_array("attribute_types", $permissions) || in_array("manage_attributes", $permissions) || in_array("brands", $permissions) || in_array("tags", $permissions)) { ?>

                            <li>
                                <a href="#"><i class="fa fa-table"></i> <span class="nav-label">Master</span><span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level collapse in">

                                    <li class="<?php
                                    if ($page_name == 'states' || $page_name == 'cities' || $page_name == 'locations' || $page_name == 'pincodes') {
                                        echo 'active';
                                    }
                                    ?>">
                                        <a>Locations
                                            <span class="fa arrow"></span>
                                        </a>
                                        <ul class="nav nav-second-level collapse">
                                            <?php if (in_array("state", $permissions)) { ?>
                                                <li  class="<?= $page_name == 'states' ? 'active' : '' ?>">
                                                    <a href="<?= base_url() ?>admin/states">States</a>
                                                </li>
                                            <?php } if (in_array("city", $permissions)) { ?>
                                                <li class="<?= $page_name == 'cities' ? 'active' : '' ?>">
                                                    <a href="<?= base_url() ?>admin/cities">Cities</a>
                                                </li>
                                            <?php } if (in_array("pincode", $permissions)) { ?>
                                                <li class="<?= $page_name == 'pincodes' ? 'active' : '' ?>">
                                                    <a href="<?= base_url() ?>admin/pincodes">Pincode</a>
                                                </li>
                                            <?php } if (in_array("area", $permissions)) { ?>
                                                <li class="<?= $page_name == 'locations' ? 'active' : '' ?>">
                                                    <a href="<?= base_url() ?>admin/locations">Areas</a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                    <?php if (in_array("banners", $permissions)) { ?>
                                        <li class="<?= $page_name == 'banners' ? 'active' : '' ?>">
                                            <a href="<?= base_url() ?>admin/banners">Banners</a>
                                        </li>
                                    <?php } if (in_array("categories", $permissions)) { ?>
                                        <li class="<?= $page_name == 'categories' ? 'active' : '' ?>">
                                            <a href="<?= base_url() ?>admin/categories">Categories</a>
                                        </li>
                                    <?php } if (in_array("sub_categories", $permissions)) { ?>
                                        <li class="<?= $page_name == 'sub_categories' ? 'active' : '' ?>">
                                            <a href="<?= base_url() ?>admin/sub_categories">Sub Categories</a>
                                        </li>
                                    <?php } ?>
                                    <?php if (in_array("attribute_types", $permissions) || in_array("manage_attributes", $permissions) || in_array("brands", $permissions) || in_array("tags", $permissions)) { ?>

                                        <li class="<?php
                                        if ($page_name == 'attributes' || $page_name == 'manage_attributes' || $page_name == 'brands' || $page_name == 'tags' || $page_name == 'tax') {
                                            echo 'active';
                                        }
                                        ?>">
                                            <a href="#">Product Attributes
                                                <span class="fa arrow"></span>
                                            </a>
                                            <ul class="nav nav-second-level collapse">
                                                <?php if (in_array("attribute_types", $permissions)) { ?>

                                                    <li class="<?= $page_name == 'attributes' ? 'active' : '' ?>">
                                                        <a href="<?= base_url() ?>admin/attributes">Attributes Types</a>
                                                    </li> 
                                                <?php } if (in_array("manage_attributes", $permissions)) { ?>
                                                    <li class="<?= $page_name == 'manage_attributes' ? 'active' : '' ?>">
                                                        <a href="<?= base_url() ?>admin/manage_attributes">Manage Attributes</a>
                                                    </li>
                                                <?php } if (in_array("brands", $permissions)) { ?>
                                                    <li class="<?= $page_name == 'brands' ? 'active' : '' ?>">
                                                        <a href="<?= base_url() ?>admin/brands">Brands</a>
                                                    </li>
                                                <?php } if (in_array("tags", $permissions)) { ?>
                                                    <li class="<?= $page_name == 'tags' ? 'active' : '' ?>">
                                                        <a href="<?= base_url() ?>admin/tags">Tags</a>
                                                    </li>
                                                <?php } ?>
                                            </ul>

                                        </li>

                                    <?php } ?>

                                </ul>
                            </li>


                            <li>
                                <a href="<?= base_url() ?>admin/Sub_admin"><i class="fa fa-th-large"></i> <span class="nav-label">Sub Admins</span></a>
                            </li>

                            <li class="<?php
                            if ($page_name == 'products' || $page_name == 'inactive_products') {
                                echo 'active';
                            }
                            ?>">
                                <a><i class="fa fa-table"></i> <span class="nav-label">Products</span> 
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-second-level collapse">
                                    <?php if (in_array("active_products", $permissions)) { ?>
                                        <li  class="<?= $page_name == 'products' ? 'active' : '' ?>">
                                            <a href="<?= base_url() ?>admin/products">Active Products</a>
                                        </li>
                                    <?php } if (in_array("inactive_products", $permissions)) { ?>
                                        <li class="<?= $page_name == 'inactive_products' ? 'active' : '' ?>">
                                            <a href="<?= base_url() ?>admin/inactive_products">Inactive Products</a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </li>




                            <li class="<?= $page_name == 'coupons' ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>admin/coupons"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Coupon codes</span></a>
                            </li>
                            <li class="<?= $page_name == 'cashcoupons' ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>admin/cashcoupons"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Cash Coupon</span></a>
                            </li>

                            <li class="<?php
                            if ($page_name == 'vendor_banners') {
                                echo 'active';
                            }
                            ?>">
                                <a href="<?= base_url() ?>admin/vendor_banners"><i class="fa fa-th-large"></i> <span class="nav-label">Vendor Banners</span></a>
                            </li>

                            <li class="<?= $page_name == 'orders' ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>admin/orders"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Orders</span></a>
                            </li>
                            <?php
                        }
                        ?>

                        <li class="<?= $page_name == 'vendors_shops' || $page_name == 'inactive_vendors_shops' || $page_name == 'vendors_shops/add' ? 'active' : '' ?>">
                            <a><i class="fa fa-table"></i> Vendors
                                <span class="fa arrow"></span>
                            </a>
                            <ul class="nav nav-second-level collapse">
                                <li  class="<?= $page_name == 'vendors_shops' ? 'active' : '' ?>">
                                    <a href="<?= base_url() ?>admin/vendors_shops">Active Vendors</a>
                                </li>
                                <li class="<?= $page_name == 'inactive_vendors_shops' ? 'active' : '' ?>">
                                    <a href="<?= base_url() ?>admin/inactive_vendors_shops">Inactive Vendors</a>
                                </li>
                            </ul>
                        </li>

                        <li class="<?= $page_name == 'delivery_boy' || $page_name == 'delivery_boy/add' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>admin/delivery_boy"><i class="fa fa-truck"></i> <span class="nav-label">Delivery Boys</span>

                            </a>
                        </li>


                        <li class="<?= $page_name == 'settlements' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>/admin/settlements"><i class="fa fa fa-money"></i> <span class="nav-label">Settlements</span></a>
                        </li> 

                        <li class="<?= $page_name == 'users' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>admin/users"><i class="fa fa-user"></i> <span class="nav-label">Users</span></a>
                        </li>
                        <li class="<?= $page_name == 'become_vendors' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>admin/become_vendors"><i class="fa fa-user"></i> <span class="nav-label">Become a Vendors</span></a>
                        </li>
                        <li class="<?= $page_name == 'notifications' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>admin/notifications"><i class="fa fa-bell-o"></i> <span class="nav-label">Notifications</span></a>
                        </li>
                        <li class="<?= $page_name == 'pushnotifications_list' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>admin/Pushnotifications"><i class="fa fa-bell-o"></i> <span class="nav-label">Push Notifications</span></a>
                        </li>

                        <li class="<?= $page_name == 'transactions' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>admin/transactions"><i class="fa fa fa-money"></i> <span class="nav-label">Transactions</span></a>
                        </li>

                        <li class="<?= $page_name == 'content' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>admin/content"><i class="fa fa-book"></i> <span class="nav-label">CMS Pages</span></a>
                        </li>

                        <li class="<?= $page_name == 'settings' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>admin/settings"><i class="fa fa-cog"></i> <span class="nav-label">Settings</span></a>
                        </li>




                    <?php } ?>
                    <?php
                    $user_type = $_SESSION['admin_login']['user_type'];
                    if ($user_type == 'franchise') {
                        ?>  
                        <li class="<?= $page_name == 'dashboard' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>admin/dashboard"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a>
                        </li>
    <!--                        <li class="<?= $page_name == 'orders' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>admin/orders"><i class="fa fa-file"></i> <span class="nav-label">Orders</span></a>
                        </li>-->

                        <li class="<?= $page_name == 'services_orders' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>admin/franchise_services_orders"><i class="fa fa-files-o"></i> <span class="nav-label">Franchise Services Orders</span></a>
                        </li>
                        <li class="<?= $page_name == 'franchise_bank_details' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>admin/franchise_bank_details"><i class="fa fa-building-o"></i> <span class="nav-label">Payment Details</span></a>
                        </li>
                        <li class="<?= $page_name == 'franchise_services_withdraw_requests' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>admin/franchise_services_withdraw_requests"><i class="fa fa-money"></i> <span class="nav-label">Services Withdraw Requests</span></a>
                        </li>
                        <li class="<?= $page_name == 'services' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>admin/franchise_services"><i class="fa fa-money"></i> <span class="nav-label">Services Prices</span></a>
                        </li>
                        <li class="<?= $page_name == 'service_providers' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>admin/franchise_service_providers"><i class="fa fa-users"></i> <span class="nav-label">Service Providers</span></a>
                        </li>
                        <li class="<?= $page_name == 'my_requests' || $page_name == 'send_request' ? 'active' : '' ?>">
                            <a><i class="fa fa-table"></i> Items/Kits Requests
                                <span class="fa arrow"></span>
                            </a>
                            <ul class="nav nav-second-level collapse">
                                <li  class="<?= $page_name == 'my_requests' ? 'active' : '' ?>">
                                    <a href="<?= base_url() ?>admin/items_requests">My Requests</a>
                                </li>
                                <li class="<?= $page_name == 'send_request' ? 'active' : '' ?>">
                                    <a href="<?= base_url() ?>admin/items_requests/send_request">Send Request</a>
                                </li>
                            </ul>
                        </li>
                        <li class="<?= $page_name == 'franchise_change_pass' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>admin/franchises/change_password"><i class="fa fa-key"></i> <span class="nav-label">Change Password</span></a>
                        </li>



                        <li>
                            <a>
                                <span class="nav-label">E-Commerce</span>
                                <hr>
                            </a>
                        </li>

                        <li  class="<?= $page_name == 'vendors_shops' || $page_name == 'inactive_vendors_shops' || $page_name == 'vendors_shops/add' ? 'active' : '' ?>">
                            <a><i class="fa fa-table"></i> <span class="nav-label">E-Com Vendors</span>
                                <span class="fa arrow"></span>
                            </a>
                            <ul class="nav nav-second-level collapse">
                                <li  class="<?= $page_name == 'vendors_shops' ? 'active' : '' ?>">
                                    <a href="<?= base_url() ?>admin/vendors_shops">Active Vendors</a>
                                </li>
                                <li class="<?= $page_name == 'inactive_vendors_shops' ? 'active' : '' ?>">
                                    <a href="<?= base_url() ?>admin/inactive_vendors_shops">Inactive Vendors</a>
                                </li>
                            </ul>
                        </li>

                        <li class="<?= $page_name == 'delivery_boy' || $page_name == 'delivery_boy/add' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>admin/delivery_boy"><i class="fa fa-truck"></i> <span class="nav-label">Delivery Boys</span>

                            </a>
                        </li>

                        <li class="<?= $page_name == 'orders' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>admin/orders"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Orders</span></a>
                        </li>

                        <li class="<?= $page_name == 'franchise_settlements' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>/admin/franchise_settlements"><i class="fa fa fa-money"></i> <span class="nav-label">Franchise Settlements</span></a>
                        </li> 


                    <?php } ?>

                </div>
            </nav>

            <div id="page-wrapper" class="gray-bg dashbard-1">
                <div class="row border-bottom">
                    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                        <div class="navbar-header">
                            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>

                        </div>
                        <ul class="nav navbar-top-links navbar-right">
                            <?php
                            $qry = $this->db->query("SELECT orders.*,admin_notifications.* FROM orders LEFT JOIN admin_notifications ON orders.id=admin_notifications.order_id WHERE admin_notifications.status=0 and orders.order_status=2");
                            $num_rows = $qry->num_rows();
                            ?>
                            <li id="here">
                                <a href="<?= base_url() ?>admin/notifications">
                                    <i class="fa fa-bell" aria-hidden="true" style=""></i> Notifications <b> ( <?php echo $num_rows; ?> )</b>
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>admin/logout">
                                    <i class="fa fa-sign-out"></i> Log out
                                </a>
                            </li>
                        </ul>



                    </nav>


                    <?php if ($user_type == "franchise") { ?>
                        <div style="padding: 10px 30px">
                            <?php
                            $session_data = $this->session->userdata('admin_login');
                            $this->db->where("id", $session_data['id']);
                            $u_data = $this->db->get("franchises")->row();
                            $this->db->where("id", $u_data->location_id);
                            $location = $this->db->get("cities")->row()->city_name;
                            ?> 
                            <p><b>Welcome, </b><?= $u_data->name ?>, <b>Your Location : </b><?= $location ?></p>
                        </div>
                    <?php } ?>

