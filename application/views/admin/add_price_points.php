<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= $title ?></h5>
                <div class="ibox-tools">
                    <a href="<?= base_url() ?>admin/price_points">
                        <button class="btn btn-primary">BACK</button>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form method="post" class="form-horizontal" enctype="multipart/form-data"  action="<?= base_url() ?>admin/price_points/<?= $func ?><?= $data->id ?>">

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Price Point</label>
                        <div class="col-sm-10">
                            <input type="text" name="price" id="price" class="form-control" value="<?= $data->price ?>">
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" type="submit" id="btn_category"> <i class="fa fa-plus-circle"></i> Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    $('#btn_category').click(function () {
        $('.error').remove();
        var errr = 0;
        if ($('#price').val() == '')
        {
            $('#price').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Price </span>');
            $('#price').focus();
            return false;
        }
    });
</script>