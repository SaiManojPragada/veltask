<style>
    .category_comm_span{
        top: -5px;
        position: relative;
        left: 10px;
    }
    .cat_commission{
        top: -5px;
        position: relative;
        left: 21px;
    }
</style>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= $title ?></h5>
                <div class="ibox-tools">
                    <a href="<?= base_url() ?>admin/franchises">
                        <button class="btn btn-primary">BACK</button>
                    </a>
                </div>
                <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                    <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                    </div>
                <?php } ?>
                <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                    <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                    </div>
                <?php }
                ?>
            </div>
            <div class="ibox-content test">
                <form method="post" class="form-horizontal" enctype="multipart/form-data" action="<?= base_url() ?>admin/franchises/update/<?= $franchises->id ?>">

                    <!-- <div class="form-group">
                        <label class="col-sm-2 control-label">Delivery Types</label>
                        <div class="col-sm-10">
                          <select id="delivery_type" name="delivery_type" class="form-control">
                             <option value="">Select Delivery Types</option>
                             <option value="full_time_driver">Full Time Driver</option>
                             <option value="pay_for_driver">Pay for Driver</option>
                          </select>
                        </div>
                    </div>
                    -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Name *</label>
                        <div class="col-sm-10">
                            <input type="text" id="name" name="name" class="form-control" value="<?= $franchises->name ?>">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">Email *</label>
                        <div class="col-sm-10">
                            <input type="email" id="email" name="email" class="form-control" value="<?= $franchises->email ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Passport size Photo * <br><small>(Leave empty if dont want to update)</small></label>
                        <div class="col-sm-10">
                            <input type="file" id="photo" name="photo" class="form-control" value="<?= $franchises->photo ?>">
                        </div>
                    </div>

                    <?php if (!empty($franchises->photo)) { ?>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Prev Photo </label>
                            <div class="col-sm-10">
                                <img src="<?= base_url('uploads/franchise/') . $franchises->photo ?>" alt="alt" style="max-width: 150px" />
                            </div>
                        </div>
                    <?php } ?>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Aadhar Card Photo <br><small>(Leave empty if dont want to update)</small></label>
                        <div class="col-sm-10">
                            <input type="file" id="aadhar_photo" name="aadhar_photo" class="form-control" value="<?= $franchises->aadhar_photo ?>">
                        </div>
                    </div>

                    <?php if (!empty($franchises->aadhar_photo)) { ?>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Prev Photo </label>
                            <div class="col-sm-10">
                                <img src="<?= base_url('uploads/franchise/') . $franchises->aadhar_photo ?>" alt="alt" style="max-width: 150px" />
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Vaccination Certificate <br><small>(PDF)</small></label>
                        <div class="col-sm-10">
                            <input type="file" id="vaccination_certificate" name="vaccination_certificate" class="form-control" value="<?= $franchises->vaccination_certificate ?>">
                        </div>
                    </div>

                    <?php if (!empty($franchises->vaccination_certificate)) { ?>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Prev File </label>
                            <div class="col-sm-10">
                                <a href="<?= base_url('uploads/franchise/') . $franchises->vaccination_certificate ?>" target="_blank" >View File</a>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">GSt Number </label>
                        <div class="col-sm-10">
                            <input type="text" id="gst_number" name="gst_number" class="form-control" value="<?= $franchises->gst_number ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Mobile Number *</label>
                        <div class="col-sm-10">
                            <input type="text" onkeypress="return isNumberKey(event)" title="Please enter exactly 10 digits" id="mobile_number" name="mobile_number" class="form-control"
                                   value="<?= $franchises->mobile_number ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Alternative Mobile Number</label>
                        <div class="col-sm-10">
                            <input type="text"  value="<?= $franchises->alternative_mobile ?>" onkeypress="return isNumberKey(event)" title="Please enter exactly 10 digits" id="alternative_mobile" name="alternative_mobile" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">State *</label>
                        <div class="col-sm-10">
                            <select id="state_id" data-placeholder="Choose State" style="width: 100%;" class="form-control" name="state_id" onchange="getCities(this.value)">
                                <option value=""> Select State</option>
                                <?php foreach ($states as $state) { ?>
                                    <option value="<?= $state->id ?>" <?php
                                    if ($franchises->state_id == $state->id) {
                                        echo "selected";
                                    }
                                    ?>><?= $state->state_name ?></option>
                                        <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">City *</label>
                        <div class="col-sm-10">
                            <select id="location_id" data-placeholder="Choose City" style="width: 100%;" class="form-control" name="location_id" onchange="getPincodes(this.value)">
                                <option value=""> Select Cities</option>
                                <?php foreach ($cities as $city) { ?>
                                    <option value="<?= $city->id ?>" <?= ($city->id == $franchises->location_id) ? 'selected' : '' ?>><?= $city->city_name ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Pincode *</label>
                        <div class="col-sm-10">
                            <select id="pincode_id" data-placeholder="Select Pincode" style="width: 100%;" class="form-control chosen-select" name="pincode_ids[]" multiple>
                                <option value=""> Select Pincodes</option>
                                <?php $arr_p = explode(',', $franchises->pincode_ids); ?>
                                <?php foreach ($pincodes as $pincode) { ?>
                                    <option value="<?= $pincode->id ?>" <?= (in_array($pincode->id, $arr_p)) ? 'selected' : '' ?>><?= $pincode->pincode ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Password *</label>
                        <div class="col-sm-10">
                            <input type="password" id="password" name="password" class="form-control">
                        </div>
                    </div>
                    <!--
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Commission (% percentage) *</label>
                                            <div class="col-sm-10">
                                                <input type="number" id="commission" name="commission" class="form-control" value="<?= $franchises->commission ?>">
                                            </div>
                                        </div>-->


                    <div class="form-group">
                        <label class="col-sm-2 control-label">Address *</label>
                        <div class="col-sm-10">
                            <input type="text" id="address" name="address" class="form-control" value="<?= $franchises->address ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-4">
                        </div>
                        <div class="col-sm-8">
                            <a href="https://www.latlong.net/" target="_blank" class="btn btn-primary">Get Latitude and Longitude</a>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Latitude *</label>
                        <div class="col-sm-10">
                            <input type="text" id="latitude" name="latitude" class="form-control" value="<?= $franchises->latitude ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Longitude *</label>
                        <div class="col-sm-10">
                            <input type="text" id="longitude" name="longitude" class="form-control" value="<?= $franchises->longitude ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Status</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="status" name="status">
                                <option value="">Select Status</option>
                                <option value="1" <?= ($franchises->status) ? "selected" : "" ?>>Active</option>
                                <option value="0" <?= (!$franchises->status) ? "selected" : "" ?>>InActive</option>
                            </select>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" id="btn_francise" type="submit">Update </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-lg" style="width : 50%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Shop Timings</h4>
            </div>
            <div class="modal-body" >




            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    function getCities(val) {
        $.ajax({
            url: "<?= base_url() ?>admin/locations/getCities",
            type: "post",
            data: {state_id: val},
            success: function (resp) {
                $("#location_id").html(resp);
            }
        });
    }

    function getPincodes(val) {
        $.ajax({
            url: "<?= base_url() ?>admin/locations/getPincodes",
            type: "post",
            data: {city_id: val},
            success: function (resp) {
                console.log(resp);
                $("#pincode_id").html(resp);
                $('#pincode_id').trigger('chosen:updated');
            }
        });
    }

</script>
<script type="text/javascript">

    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    $('#btn_francise').click(function () {
        $('.error').remove();
        var errr = 0;


        var ph = $('#mobile_number').val();
        var pass = $('#password').val();

        if ($('#name').val() == '')
        {
        $('#name').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Name</span>');
                $('#name').focus();
                return false;
        } else if ($('#email').val() == '')
        {
        $('#email').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Email</span>');
                $('#email').focus();
                return false;
        }
        else if (!validateEmail($('#email').val()))
        {
        $('#email').after('<span class="error" style="color:red">Invalid Email Address</span>');
                $('#email').focus();
                return false;
        }
<?php if (empty($franchises->photo)) { ?>
            else if ($('#photo').val() == '')
            {
            $('#photo').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Select Photo</span>');
                    $('#photo').focus();
                    return false;
            }
<?php } ?>
        else if ($('#mobile_number').val() == '')
        {
        $('#mobile_number').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Mobile</span>');
                $('#mobile_number').focus();
                return false;
        }
        else if (ph.length != 10)
        {
        $('#mobile_number').after('<span class="error" style="color:red">Enter Valid 10 digit Phone Number</span>');
                $('#mobile_number').focus();
                return false;
        }
        else if ($('#state_id').val() == '')
        {
        $('#state_id').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Select State</span>');
                $('#state_id').focus();
                return false;
        }
        else if ($('#location_id').val() == '')
        {
        $('#location_id').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Select City</span>');
                $('#location_id').focus();
                return false;
        }
        else if ($('#pincode_id').val() == '')
        {
        $('#pincode_id').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Select Pincode</span>');
                $('#pincode_id').focus();
                return false;
        }
        else if (pass.length > 0 && pass.length < 8)
        {
            $('#password').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Password is min 8 Charecters</span>');
            $('#password').focus();
            return false;
        }
//        else if ($('#commission').val() == '')
//        {
//            $('#commission').after('<span class="error" style="color:red;font-size: 18px;margin-left: 18px;">Enter Commission</span>');
//            $('#commission').focus();
//            return false;
//        }
        else if ($('#address').val() == '')
        {
        $('#address').after('<span class="error" style="color:red;font-size: 18px;margin-left: 18px;">Enter Address</span>');
                $('#address').focus();
                return false;
        }
        else if ($('#latitude').val() == '')
        {
        $('#latitude').after('<span class="error" style="color:red;font-size: 18px;margin-left: 18px;">Enter Latitude</span>');
                $('#latitude').focus();
                return false;
        }
        else if ($('#longitude').val() == '')
        {
            $('#longitude').after('<span class="error" style="color:red;font-size: 18px;margin-left: 18px;">Enter Longitude</span>');
            $('#longitude').focus();
            return false;
        }
    });

    function validateEmail($email)
    {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if (!emailReg.test($email)) {
            return false;
        } else
        {
            return true;
        }
    }
</script>

<link href="https://test.indiasmartlife.com/admin_assets/css/jquery.datetimepicker.css" rel="stylesheet">
<script src="https://test.indiasmartlife.com/admin_assets/js/jquery.datetimepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $('.datepicker').datetimepicker({
            timepicker: false,
            format: 'Y-m-d',
            scrollInput: false
        });
        $(document).on('mousewheel', '.datepicker', function () {
            return false;
        });

        $('.datepickertimepicker').datetimepicker({
            timepicker: true,
            format: 'Y-m-d H:i',
            scrollInput: false
        });
        $(document).on('mousewheel', '.datepickertimepicker', function () {
            return false;
        });

        $('#cities').on('change', function () {
            var city_id = $('#cities').val();

            loadCityLocations(city_id);
        });

        function loadCityLocations(city_id) {
            //alert(city);
            // $('.modal').modal('show');
            $.get("<?= base_url() ?>api/admin_ajax/admin/get_city_locations", "city_id=" + city_id,
                    function (response, status, http) {
                        //$('.modal').modal('hide');
                        $('#locations').html(response);
                    }, "html");
        }

    });
</script>