<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= $title ?></h5>
                <div class="ibox-tools">
                    <a href="<?= base_url() ?>admin/services_categories">
                        <button class="btn btn-primary">BACK</button>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form method="post" class="form-horizontal" enctype="multipart/form-data"  action="<?= base_url() ?>admin/services_sub_categories/<?= $func ?><?= $data->id ?>">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Category Name</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="cat_id" id="cat_id">
                                <option value="">Select Category</option> 
                                <?php foreach ($categories as $item) { ?>
                                    <option value="<?= $item->id ?>" <?= ($this->input->get("cat_id") == $item->id || $data->cat_id == $item->id) ? "selected" : "" ?>><?= $item->name ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Sub Category Name</label>
                        <div class="col-sm-10">
                            <input type="text" id="sub_category_name" name="sub_category_name" class="form-control" value="<?= $data->sub_category_name ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                            <textarea rows="10" cols="10" id="description" name="description" class="form-control"><?= $data->description ?></textarea>
                        </div>
                    </div>
                    <!--                    <div class="form-group">
                                            <label class="col-sm-2 control-label">Image</label>
                                            <div class="col-sm-10">
                                                <input type="file" name="image" class="form-control" required>
                                            </div>
                                        </div>-->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">App Image</label>
                        <div class="col-sm-10">
                            <input type="file" id="app_image" name="app_image" class="form-control">
                            <span class="help-block m-b-none" style="color:red;">App Image Width : 500px and height : 500px</span>

                            <?php if (!empty($data->app_image)) { ?>
                                <img src="<?= base_url('uploads/services_sub_categories/' . $data->app_image) ?>" width="150" alt="alt"/>
                            <?php } ?>                            
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Web Image</label>
                        <div class="col-sm-10">
                            <input type="file" id="web_image" name="web_image" class="form-control">
                            <span class="help-block m-b-none" style="color:red;">App Image Width : 500px and height : 500px</span>

                            <?php if (!empty($data->web_image)) { ?>
                                <img src="<?= base_url('uploads/services_sub_categories/' . $data->web_image) ?>" width="150" alt="alt"/>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Status</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="status" name="status">
                                <option value="">Select Status</option>
                                <option value="1" <?= ($data->status) ? "selected" : "" ?>>Active</option>
                                <option value="0" <?= (!$data->status) ? "selected" : "" ?>>InActive</option>
                            </select>
                        </div>
                    </div>


                    <!-- <div class="form-group">
                        <label class="col-sm-2 control-label">Status</label>
                        <div class="col-sm-10" style="margin-top: 6px;">
                            <label class='text-success'>
                                <input type="radio"
                                       name="status"
                                       id="status37"
                                       required="required"
                                       value="Active"
                                       data-msg-required="This Status is required" value="1" checked/> Active
                            </label> &nbsp;&nbsp;
                            <label class='text-danger'>
                                <input type="radio"
                                       name="status"
                                       id="status69"
                                       required="required"
                                       data-msg-required="This Status is required" value="0"/> InActive
                            </label>
                        </div>
                    </div> -->

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" type="submit" id="btn_category"> <i class="fa fa-plus-circle"></i> Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    $('#btn_category').click(function () {
        $('.error').remove();
        var errr = 0;
        if ($('#cat_id').val() == '')
        {
        $('#cat_id').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Select Category</span>');
                $('#cat_id').focus();
                return false;
        }
        else if ($('#sub_category_name').val() == ''){$('#sub_category_name').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Sub Category Name</span>');
                $('#sub_category_name').focus();
                return false;
        } else if ($('#description').val() == '')
        {
        $('#description').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Description</span>');
                $('#description').focus();
                return false;
        }
<?php if ($func === "insert") { ?>else if ($('#app_image').val() == '')
            {
            $('#app_image').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Select App Image</span>');
                    $('#app_image').focus();
                    return false;
            }
            else if ($('#web_image').val() == '')
            {
            $('#web_image').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Select App Image</span>');
                    $('#web_image').focus();
                    return false;
            }
<?php } ?>else if ($('#status').val() == '')
        {
            $('#status').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Select Status</span>');
            $('#status').focus();
            return false;
        }
    });
    function validateEmail($email)
    {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if (!emailReg.test($email)) {
            return false;
        } else
        {
            return true;
        }
    }
</script>