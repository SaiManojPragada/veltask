<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $title ?></h5>
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-content">

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Provider Details</th>
                                <th>Amount Need to Be Collected</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($providers as $i => $pp) { ?>
                                <tr>
                                    <td><?= $i + 1 ?></td>
                                    <td>
                                        <?= $pp->provider_details->name ?> (<?= $pp->provider_details->type ?>)<br>
                                        <?= $pp->provider_details->email ?>, <?= $pp->provider_details->phone ?>
                                    </td>
                                    <td><i class="fa fa-inr"></i> <?= abs($pp->total_amount) ?></td>
                                    <td>
                                        <button class="btn btn-primary btn-xs" data-target="#settle" onclick="assign_provider_id('<?= $pp->provider_details->id ?>')" data-toggle="modal">settle</button>
                                    </td>
                                </tr>
                            <?php } ?>
                            <?php if (empty($providers)) { ?>
                                <tr>
                                    <td colspan="4" style="text-align: center"> -- No Results Found --</td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
        function assign_provider_id(provider_id) {
            if (provider_id.length < 1) {
                $("#settle").modal().hide();
            } else {
                $("#provider_id").val(provider_id);
            }
        }
    </script>
    <div class="modal fade" tabindex="-1" role="dialog" id="settle">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="post" action="">
                    <div class="modal-header">
                        <h5 class="modal-title">Settle Amount</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="hidden" name="provider_id" id="provider_id"/>
                            <label>Enter Paid Amount</label>
                            <input type="number" min="0" class="form-control" required name="amount" placeholder="Please Enter Paid Amount"/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Update</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


</div>

