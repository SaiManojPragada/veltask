<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Order details </h5>
                    <div class="ibox-tools">
                        <a href="javascript:void(0);">
                            <button class="btn btn-primary" onclick="history.back();">BACK</button>
                        </a>

                        <?php
                        $user_type = $_SESSION['admin_login']['user_type'];
                        if ($user_type == 'subadmin') {
                            $admin_id = $_SESSION['admin_login']['id'];
                            $adm_qry = $this->db->query("select * from sub_admin where id='" . $admin_id . "'");
                            $adm_row = $adm_qry->row();

                            $userpermissions = $adm_row->permissions;
                            $permissions = explode(",", $userpermissions);
                            if (in_array("add_category", $permissions)) {
                                ?>


                                <?php
                            }
                        } else {
                            ?>


                        <?php } ?>


                    </div>

                    <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                        <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                        </div>
                    <?php } ?>
                    <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                        <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                        </div>
                    <?php }
                    ?>
                </div>


                <div class="ibox-content">
                    <div style="height: 30px; width: 100%">
                        <?php if ($_SESSION['admin_login']['user_type'] != 'franchise' && $data->order_status == "order_cancelled") { ?>
                            <button class="btn btn-xs btn-danger" style="float: right"  data-toggle="modal" data-target="#approveCancellation" onclick="assignCancel('<?= $data->id ?>')">Approve Cancellation</button>
                        <?php } ?>
                        <?php if ($_SESSION['admin_login']['user_type'] != 'franchise' && ($data->order_status == "order_placed" || $data->order_status == "order_accepted" || $data->order_status == "order_started")) { ?>
                            <button onclick="req_cancel_order('<?= $data->user_id ?>', '<?= $data->id ?>');" style="float: right" class="btn btn-xs btn-danger">Cancel Order</button>
                        <?php } ?>
                    </div>
                    <table class="table table-striped table-bordered" >

                        <tbody>
                            <tr><th colspan="4"><center>Order ID : #<?= $data->order_id ?>&nbsp;&nbsp;&nbsp;&nbsp;<span style="color: tomato">(<?= ($data->order_status == "order_cancelled") ? "Order Cancelled" : "" ?><?= ($data->order_status == "refunded") ? "Refunded" : "" ?>)</span>
                            <?php if ($data->has_visit_and_quote == 'Yes') { ?>
                                <br>
                                <span style="color:#a47e3c">( This is a Visit and Quote Order ) </span>
                                <br>
                                <?php if (!empty($quotation)) { ?>
                                    <button class="btn btn-primary btn-xs" style="float: right" data-target="#quotation_view_modal" data-toggle="modal"> View Quotation Milestones</button>
                                <?php } ?>

                            <?php } ?>
                        </center>
                        <span style="position: absolute; right: 50px; font-size: 18px; top: 115px"><b>Payment Status : </b><?= ($data->order_status == "order_completed") ? "<span style='color: green'>Paid</span>" : $show_payment_status ?></span>
                        </th></tr>
                        <tr>
                            <td colspan="2" style="width: 50%">
                        <center><h4>Customer Details</h4></center>
                        <div class="modal fade " tabindex="-1" role="dialog" id="approveCancellation">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Cancel Order </b></h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <input type="hidden" id="order_id_cancel"/>
                                        <span><b>Please provide the Refund Transaction or the other details these details are displayed to the User.</b></span>
                                        <textarea class="form-control" id="cancellation_reason" rows="5"></textarea>
                                        <p id="cancellation_reason_err" style="color: tomato"></p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" onclick="cancel_order();">Cancel Order</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="width: 100%">
                            <table class="table table-striped table-bordered">
                                <tbody>
                                    <tr>
                                        <td colspan="2">
                                            <p><strong>Name : </strong></p>
                                            <p><strong>Email : </strong></p>
                                            <p><strong>Phone : </strong></p>
                                        </td>
                                        <td colspan="3">
                                            <p><?= ($data->customer_details->first_name) ? $data->customer_details->first_name : "N/A" ?></p>
                                            <p><?= ($data->customer_details->email) ? $data->customer_details->email : "N/A" ?></p>
                                            <p><?= ($data->customer_details->phone) ? $data->customer_details->phone : "N/A" ?></p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        </td>
                        <td colspan="2" style="width: 50%">
                        <center><h4>Customer Address</h4></center>
                        <div style="width: 100%">
                            <table class="table table-striped table-bordered">
                                <tbody>
                                    <tr>
                                        <td colspan="2">
                                            <p><strong>Time Slot Date and Time : </strong> <?= (date('d-m-Y', strtotime($data->time_slot_date))) ?>, <?= $data->time_slot ?></p>
                                            <p><strong>Address  : </strong></br>
                                                <?= $data->customer_address->address . ', ' ?><br>
                                                <?= $data->customer_address->landmark . ', ' . $data->customer_address->city_name . ', ' . $data->customer_address->state_name . ', ' . $item->customer_address->pincode ?>
                                            </p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        </td>
                        </tr>
                        <tr>
                        <tr><th colspan="4"><center>Order Details</center></th></tr>
                        <td colspan="2">
                            <div style="width: 100%">
                                <table class="table table-striped table-bordered">
                                    <tbody>
                                        <tr><th colspan="4"><center>Transaction Details</center></th></tr>
                                    <tr>
                                        <td style="width: 50%">
                                            <p><strong>Order Status : </strong></p>
                                            <p><strong>Razorpay Order Id : </strong></p>
                                            <p><strong>Razorpay Payment Id : </strong></p>
                                            <?php if ($data->order_status == "order_cancelled" || $data->order_status == "refunded") { ?>
                                                <p><strong>User Cancellation Message : </strong></p>
                                            <?php } ?>
                                            <?php if ($data->order_status == "refunded") { ?>
                                                <p><strong>Admin Cancellation Message : </strong></p>
                                            <?php } ?>
                                            <?php if ($data->order_status == "order_cancelled" || $data->order_status == "refunded") { ?>
                                                <p><strong>Is Cancelled In Grace Period : </strong></p>
                                            <?php } ?>

                                        </td>
                                        <td style="width: 50%">
                                            <p><?= ucfirst(str_replace("_", " ", $data->order_status)) ?></p>
                                            <p><?= $data->razorpay_order_id ?></p>
                                            <p><?= $data->razorpay_transaction_id ?></p>
                                            <?php if ($data->order_status == "order_cancelled" || $data->order_status == "refunded") { ?>
                                                <p><?= ($data->cancellation_reason) ? $data->cancellation_reason : "N/A" ?></p>
                                            <?php } ?>
                                            <?php if ($data->order_status == "refunded") { ?>
                                                <p><?= ($data->admin_cancellation_msg) ? $data->admin_cancellation_msg : "N/A" ?></p>
                                            <?php } ?>
                                            <?php if ($data->order_status == "order_cancelled" || $data->order_status == "refunded") { ?>
                                                <p style="color : <?= ( $data->cancelled_in_grace_period == "yes") ? "green" : "tomato" ?>"><?= ucfirst($data->cancelled_in_grace_period) ?></p>
                                            <?php } ?>


                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            <b>Order Created Date : </b><br>
                                            <?php if ($data->accepted_at) { ?>
                                                <b>Order Accepted Date : </b><br>
                                            <?php } ?>
                                            <?php if ($data->rejected_at) { ?>
                                                <b>Order Rejected Date : </b><br>
                                            <?php } ?>
                                            <?php if ($data->started_at) { ?>
                                                <b>Order Started Date : </b><br>
                                            <?php } ?>
                                            <?php if ($data->completed_at) { ?>
                                                <b>Order Completed Date : </b><br>
                                            <?php } ?>
                                            <?php if ($data->cancelled_at) { ?>
                                                <b>Order Cancelled Date : </b><br>
                                            <?php } ?>
                                            <?php if ($data->refunded_at) { ?>
                                                <b>Order Refunded Date : </b><br>
                                            <?php } ?>
                                        </th>
                                        <td>
                                            <?= date('d M Y, h:i A', $data->created_at) ?>,<br>
                                            <?php if ($data->accepted_at) { ?>
                                                <?= date('d M Y, h:i A', $data->accepted_at) ?>,<br>
                                            <?php } ?>
                                            <?php if ($data->rejected_at) { ?>
                                                <?= date('d M Y, h:i A', $data->rejected_at) ?>,<br>
                                            <?php } ?>
                                            <?php if ($data->started_at) { ?>
                                                <?= date('d M Y, h:i A', $data->started_at) ?>,<br>
                                            <?php } ?>
                                            <?php if ($data->completed_at) { ?>
                                                <?= date('d M Y, h:i A', $data->completed_at) ?>,<br>
                                            <?php } ?>
                                            <?php if ($data->cancelled_at) { ?>
                                                <?= date('d M Y, h:i A', $data->cancelled_at) ?>,<br>
                                            <?php } ?>
                                            <?php if ($data->refunded_at) { ?>
                                                <?= date('d M Y, h:i A', $data->refunded_at) ?>,<br>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <tr><th colspan="4"><center>Accepted Service Provider Details</center></th></tr>
                                    <tr>
                                        <?php if (empty($data->accepted_by_service_provider)) { ?>
                                            <td colspan="2"><center>Not Yet Accepeted</center></td>
                                    <?php } else { ?>
                                        <td style="width: 50%">
                                            <p><strong>Name : </strong></p>
                                            <p><strong>Email : </strong></p>
                                            <p><strong>Phone : </strong></p>
                                            <p><strong>Provider Type : </strong></p>
                                            <p><strong>Address : </strong></p>
                                        </td>
                                        <td style="width: 50%">
                                            <p><?= $data->service_provider_details->name ?></p>
                                            <p><?= $data->service_provider_details->email ?></p>
                                            <p><?= $data->service_provider_details->phone ?></p>
                                            <p><?= $data->service_provider_details->type ?></p>
                                            <p><?= $data->service_provider_details->address ?></p>
                                        </td>
                                    <?php } ?>
                                    </tr>

                                    <?php if ($_SESSION['admin_login']['user_type'] != 'franchise' && $data->order_status != 'order_completed' && $data->order_status != 'order_cancelled' && $data->order_status != 'refunded') { ?>
                                        <?php if (!empty($order_provider_list)) { ?>
                                            <tr><th colspan="4"><center>Available Service Providers</center></th></tr>
                                        <?php } ?>
                                        <?php foreach ($order_provider_list as $prvd) { ?>
                                            <tr>
                                                <td><?= $prvd->name ?>, ph: <?= $prvd->phone ?></td>
                                                <td><center><?= empty($data->accepted_by_service_provider) ? 'Assign' : 'Re-Assign' ?> &nbsp;&nbsp;<input type="checkbox" onclick="reassignServiceProvider('<?= $data->id ?>', '<?= $prvd->id ?>')"></center></td>
                                            </tr>
                                        <?php } ?>
                                    <?php } ?>
                                    <?php if ($data->order_status == "order_completed") { ?>
                                        <tr><th colspan="4"><center>Settlement Details</center></th></tr>
                                        <tr>
                                            <td style="width: 50%">
                                                <p><strong>Total Amount : </strong></p>

                                                <?php if ($_SESSION['admin_login']['user_type'] != 'franchise') { ?>
                                                    <p><strong>Admin Commission <small>(This is total of all the discounts and admin commission)</small>: </strong>
                                                        <br><br><br><br>
                                                    </p>

                                                <?php } ?>
                                                <p><strong>Franchise Commission : </strong></p>
                                                <p><strong>Service Provider Commission : </strong></p>
                                                <p><strong>Gst : </strong></p>
                                            </td>
                                            <td style="width: 50%">
                                                <p><?= $data->grand_total + $data->coupon_discount + $data->membership_discount + $data->bussiness_discount + $data->discount ?></p>

                                                <?php if ($_SESSION['admin_login']['user_type'] != 'franchise') { ?>
                                                    <p><?php if ($data->admin_comission) { ?>
                                                        <table>
                                                            <tr>
                                                                <th>Admin Commission : </th>
                                                                <td>&nbsp;<i class="fa fa-inr"></i> <?= $data->admin_comission + $data->bussiness_discount + $data->membership_discount + $data->coupon_discount ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Business Discount : </th>
                                                                <td>&nbsp;<i class="fa fa-inr"></i> <?= $data->bussiness_discount ?></td>
                                                            </tr>

                                                            <tr>
                                                                <th>Membership Discount : </th>
                                                                <td>&nbsp;<i class="fa fa-inr"></i> <?= $data->membership_discount ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Coupon Discount : </th>
                                                                <td>&nbsp;<i class="fa fa-inr"></i> <?= $data->coupon_discount ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Admin Final Commission : </th>
                                                                <td>&nbsp;<i class="fa fa-inr"></i> <?= ($data->admin_comission) ? $data->admin_comission : "N/A" ?></td>
                                                            </tr>
                                                        </table>
                                                    <?php } else { ?>
                                                        N/A
                                                    <?php } ?></p>

                                                <?php } ?>
                                                <p><i class="fa fa-inr"></i> <?= ($data->franchise_comission) ? $data->franchise_comission : "N/A" ?></p>
                                                <p><i class="fa fa-inr"></i> <?= ($data->service_provider_comission) ? $data->service_provider_comission : "N/A" ?></p>
                                                <p><i class="fa fa-inr"></i> <?= ($data->gst) ? $data->gst : "N/A" ?></p>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </td>
                        <td colspan="2">
                            <div style="width: 100%">
                                <table class="table table-striped table-bordered">
                                    <tbody>
                                        <tr><th colspan="4"><center>Transaction Details</center></th></tr>
                                    <?php foreach ($data->service_orders_items as $ind => $prod) { ?>
                                        <tr>
                                            <td style="font-weight: bold"><?= $prod->service_details->service_name ?> (x <?= $prod->quantity ?>) <?= ($prod->is_addon) ? "<small style='color: tomato'>(Add-on) </small>" : '' ?></td>
                                            <td style="font-weight: bold"><i class="fa fa-inr"></i> <?= $prod->sub_total ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if ($data->sub_total) { ?>
                                        <tr>
                                            <td>SubTotal</td>
                                            <td><i class="fa fa-inr"></i> <?= $data->sub_total ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if ($data->tax) { ?>
                                        <tr>
                                            <td>Tax</td>
                                            <td style="color: green;">+ <i class="fa fa-inr"></i> <?= $data->tax ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if ($data->visiting_charges) { ?>
                                        <tr>
                                            <td>Visiting Charges</td>
                                            <td style="color: green;">+ <?= $data->visiting_charges ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if ($data->used_wallet_amount > 0) { ?>
                                        <tr>
                                            <td>Used Wallet Amount</td>
                                            <td style="color: tomato;">- <i class="fa fa-inr"></i> <?= $data->used_wallet_amount ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if ($data->coupon_discount > 0) { ?>
                                        <tr>
                                            <td>Coupon Disocunt</td>
                                            <td style="color: tomato;">- <i class="fa fa-inr"></i> <?= $data->coupon_discount ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if ($data->bussiness_discount > 0) { ?>
                                        <tr>
                                            <td>Bussiness Discount</td>
                                            <td style="color: tomato;">- <i class="fa fa-inr"></i> <?= $data->bussiness_discount ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if ($data->membership_discount > 0) { ?>
                                        <tr>
                                            <td>Membership Discount</td>
                                            <td style="color: tomato;">- <i class="fa fa-inr"></i> <?= $data->membership_discount ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if ($data->discount > 0) { ?>
                                        <tr>
                                            <td>Discount</td>
                                            <td style="color: tomato;">- <i class="fa fa-inr"></i> <?= $data->discount ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if ($data->balance_amount > 0) { ?>
                                        <tr>
                                            <td>Balance Amount</td>
                                            <td style="color: orange;; font-size: 18px"><i class="fa fa-inr"></i> <?= $data->balance_amount ?></td>
                                        </tr>
                                    <?php } ?>
                                    <tr>
                                        <td><hr>Grand Total</td>
                                        <td><hr><p style="font-size: 25px"><i class="fa fa-inr"></i> <?= $data->amount_paid ?></p></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="quotation_view_modal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Quotation and Milestones</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?php if (!empty($quotation)) { ?>
                        <table class="table table-striped table-bordered">
                            <tbody>
                                <tr>
                                    <th colspan="2"><strong>Work Duration : </strong><?= $quotation->work_duration ?></th>
                                    <th colspan="2"><strong>Quotation Amount : </strong><?= $quotation->quotation_amount ?></th>
                                    <th colspan="2"><strong>Balance Amount : </strong><?= $quotation->balance_amount ?></th>
                                </tr>
                                <tr>
                                    <th colspan="3"><strong>No of Milestones : </strong><?= $quotation->no_of_milestones ?></th>
                                    <th colspan="3"><strong>No of Paid Milestones : </strong><?= $quotation->no_of_milestones_paid ?></th>
                                </tr>
                                <tr>
                                    <th colspan="6"><strong>Project Details : </strong><?= $quotation->project_details ?></th>
                                </tr>
                            </tbody>
                        </table>
                        <div style="margin: 50px 0px">
                            <div><h3> Milestones </h3></div>
                            <?php foreach ($quotation->milestones as $stone) { ?>
                                <table class="table table-striped table-bordered">
                                    <tbody>
                                        <tr>
                                            <td><strong> Amount : </strong> <?= $stone->amount ?></td>
                                            <td><strong> Date : </strong> <?= date('d-m-Y', strtotime($stone->date)) ?></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Status : </strong> <?= $stone->status ?></td>
                                            <td>
                                                <?php if ($stone->status == 'unpaid') { ?>
                                                    <button class="btn btn-danger btn-xs" onclick="deleteMilestone('<?= $stone->id ?>', '<?= $quotation->id ?>', '<?= $data->id ?>')">Delete Milesone</button>
                                                <?php } else { ?>
                                                    <button class="btn btn-success btn-xs" disabled>Milestone Paid</button>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            <?php } ?>
                        </div>
                    <?php } else { ?>
                        <center> -- No Quotation Found --</center>
                    <?php } ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    function deleteMilestone(milestone_id, quotation_id, order_id) {
        if (confirm("Are you sure want to delete this Milestone ? ")) {
            $.ajax({
                url: "<?= base_url('api/admin_ajax/admin/delete_milestone') ?>",
                type: "post",
                data: {milestone_id: milestone_id, quotation_id: quotation_id, order_id: order_id},
                success: function (resp) {
                    //                    resp = JSON.parse(resp);
                    if (resp['status']) {
                        alert(resp['message']);
                        location.href = '';
                    } else {
                        alert(resp['message']);
                    }
                }
            });
        }
    }
</script>
<script>

    function reassignServiceProvider(order_id, provider_id) {
        if (confirm("Are you sure want to reassign this order to another Provider ? ")) {
            $.ajax({
                url: "<?= base_url() ?>api/admin_ajax/Admin/reassign_order",
                type: "post",
                data: {user_id: provider_id, order_id: order_id},
                success: function (resp) {
                    //                    resp = JSON.parse(resp);
                    if (resp['status']) {
                        swal({
                            title: resp['message'],
                            icon: "success"
                        }).then(function () {
                            location.href = "";
                        });
                    } else {
                        swal({
                            title: resp['message'],
                            icon: "warning"
                        }).then(function () {
                            location.href = "";
                        });
                    }
                }
            });
        }
    }

    function assignCancel(order_id) {
        console.log(order_id);
        $("#order_id_cancel").val(order_id);
    }

    function req_cancel_order(user_id, order_id) {
        if (confirm("Are you sure want to Cancel this Order ? ")) {
            $.ajax({
                url: "<?= base_url('api/orders/cancel_order') ?>",
                type: "post",
                data: {user_id: user_id, order_id: order_id, cancellation_reason: "cancelled by the Admin"},
                success: function (resp) {
                    resp = JSON.parse(resp);
                    if (resp['status']) {
                        swal({
                            title: resp['message'],
                            icon: "warning"
                        }).then(function () {
                            location.href = '';
                        });
                    } else {
                        swal({
                            title: resp['message'],
                            icon: "warning"
                        });
                    }
                }
            });
        }
    }

    function cancel_order() {
        var cancellationReason = $("#cancellation_reason").val();
        var order_id = $("#order_id_cancel").val();
        if (cancellationReason.length > 15) {
            $("#cancellation_reason_err").html("");
            $.ajax({
                url: "<?= base_url('api/admin_ajax/admin/cancel_order') ?>",
                type: "post",
                data: {order_id: order_id, cancellation_reason: cancellationReason},
                success: function (resp) {
//                    console.log(resp);
//                    resp = JSON.parse(resp);
                    if (resp['status']) {
                        swal({
                            title: resp['message'],
                            icon: "success"
                        }).then(function () {
                            location.href = '';
                        });
                    } else {
                        swal({
                            title: resp['message'],
                            icon: "info"
                        });
                    }
                }
            });
        } else {
            $("#cancellation_reason_err").html("Please Enter Breif Details.");
        }
    }
</script>