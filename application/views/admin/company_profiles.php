<style>
    .cat_image{
        width: 100px;
        height: 100px;
        object-fit: scale-down;
        border-radius: 10px;
        margin: 0px 5px;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $title ?></h5>
                    <div class="ibox-tools">
                        <a href="<?= base_url() ?>admin/dashboard">
                            <button class="btn btn-primary">BACK</button>
                        </a>

                    </div>

                    <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                        <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                        </div>
                    <?php } ?>
                    <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                        <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                        </div>
                    <?php }
                    ?>
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead> 
                                <tr>
                                    <th>#</th>
                                    <th>User Details</th>
                                    <th>Provided Business Details</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($profiles as $index => $pro) { ?>
                                    <tr>
                                        <td><?= $index + 1 ?></td>
                                        <td>
                                            <p><strong>Name : </strong><?= $pro->user_details->first_name ?></p>
                                            <p><strong>Email : </strong><?= $pro->user_details->email ?></p>
                                            <p><strong>Phone : </strong><?= $pro->user_details->phone ?></p>
                                            <p><a href="<?= base_url('admin/users/view/') . $pro->user_id ?>" class="btn btn-xs btn-primary">View Profile</a></p>
                                        </td>
                                        <td>
                                            <p><strong>Company Name : </strong><?= $pro->company_name ?></p>
                                            <p><strong>Company Address : </strong><?= $pro->company_address ?></p>
                                            <p><strong>Provided Proof : </strong><?= $pro->proof_type ?></p>
                                            <p><strong>Provided Document : </strong><a href="<?= base_url('uploads/company_proofs/') . $pro->document ?>" target="_blank">View Document</a></p>
                                            <p><strong>Submitted at : </strong><?= date('d M Y, h:i A', $pro->updated_at) ?></p>
                                        </td>
                                        <td>
                                            <button class="btn btn-xs btn-success"onclick="approveOrDisapprove('<?= $pro->id ?>', '<?= $pro->status ?>')"><?= !($pro->status) ? 'Approve ' : 'Disapprove' ?></button>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function approveOrDisapprove(id, status) {
        var msg = "Are you Sure Want to Approve this user for Business Account ?";
        if (status === '1') {
            msg = "Are you Sure Want to Disapprove this user for Business Account ?";
        }
        if (confirm(msg)) {
            location.href = "<?= base_url('admin/company_profiles/approveOrDisapprove/') ?>" + id;
        }
    }
</script>
