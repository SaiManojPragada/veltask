<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                </div>
            <?php } ?>
            <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                </div>
            <?php } ?>
            <div class="ibox-title">
                <h5><?= $title ?></h5>
                <b style="float: right">Last Updated On : <?= date('d M Y, h:i A', $benefits->updated_at) ?></b>
                <div class="ibox-tools">

                </div>
            </div>
            <div class="ibox-content">
                <form method="post" class="form-horizontal" action="" enctype="multipart/form-data">
                    <div class="form-group col-md-8 row">
                        <label class="control-label col-md-4">Services Discount Percentage *</label>
                        <div class="col-sm-8">
                            <input type="number" name="services_discount" id="services_discount" class="form-control" min="1"  placeholder="Enter Service Discount Percentage %" value="<?= $benefits->services_discount ?>">
                        </div>
                    </div>
                    <div class="form-group col-md-8 row">
                        <label class="control-label col-md-4">E-Commerce Discount Percentage *</label>
                        <div class="col-sm-8">
                            <input type="number" name="e_comm_discount" id="e_comm_discount" class="form-control" min="1" placeholder="Enter Service Discount Percentage %" value="<?= $benefits->e_comm_discount ?>">
                        </div>
                    </div>
                    <div class="form-group col-md-8 row">
                        <label class="control-label col-md-4">Display Banner Image * <br><small>(Leave it empty if you don't want to Update Image)</small></label>
                        <div class="col-sm-8">
                            <input type="file" name="display_image" id="display_image" class="form-control" <?= !empty($benefits->display_image) ? '' : 'required' ?>>
                        </div>
                    </div>

                    <input type="hidden" name="prev_image" value="<?= $benefits->display_image ?>">
                    <?php if ($benefits->display_image) { ?>
                        <div class="form-group col-md-8 row">
                            <label class="control-label col-md-4">Previous Image </label>
                            <div class="col-sm-8">
                                <img src="<?= base_url('uploads/business_benifits_images/') . $benefits->display_image ?>" alt="alt" style="width: 200px"/>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group col-md-8 row">
                        <label class="control-label col-md-4">Benefits Description *</label>
                        <div class="col-sm-8">
                            <textarea class="form-control ck-editor" name="benefits_description" id="benefits_description" ><?= $benefits->benefits_description ?></textarea>
                        </div>
                    </div>
                    <div class="form-group col-md-8 row">
                        <label class="control-label col-md-4">Benefits Applicable Message *</label>
                        <div class="col-sm-8">
                            <textarea class="form-control ck-editor" name="benefits_applicable_message" id="benefits_applicable_message"><?= $benefits->benefits_applicable_message ?></textarea>
                        </div>
                    </div>
                    <div class="form-group col-md-8">
                        <button class="btn btn-primary pull-right" name="btn_ref" id="btn_ref" value="submit">Submit</button>
                    </div>
                    <br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                    <br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                    <br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                    <br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                    <br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                </form>
            </div>
        </div>
    </div>
</div>
<script>

    $("#btn_ref").click(function () {
        var errr = 0;
        if ($('#services_discount').val() == '')
        {
            $('#services_discount').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Services Discount</span>');
            $('#services_discount').focus();
            return false;
        } else if ($('#e_comm_discount').val() == '') {
            $('#e_comm_discount').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter E-Commerce Discount</span>');
            $('#e_comm_discount').focus();
            return false;
        }
<?php if (empty($benefits->display_image)) { ?>
            else if ($('#display_image').val() == '') {
                $('#display_image').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Select Display Image</span>');
                $('#display_image').focus();
                return false;
            }
<?php } ?>
//    else if ($('#benefits_description').val() == '') {
//    $('#benefits_description').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Benefits Description</span>');
//    $('#benefits_description').focus();
//    return false;
//    } else if ($('#benefits_applicable_message').val() == '') {
//    $('#benefits_applicable_message').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Applicable Message</span>');
//    $('#benefits_applicable_message').focus();
//    return false;
//    }
    });

</script>