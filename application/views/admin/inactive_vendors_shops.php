<style>

    .shop_image{

        width: 100px;

        height: 100px;

        object-fit: scale-down;

        margin-right:5px;

        border-radius: 10px;

        border: 1px solid #efeded;

    }

    .shop_title{

        font-size:17px !important;

        color: #f39c5a;

    }

</style>

<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">

        

        <div class="col-lg-12">

            <div class="ibox float-e-margins">



                <div class="ibox-title">

                    <h5 class="shop_title">Inactive Vendors-Shops </h5>

                    <div class="ibox-tools">
                        <a href="<?= base_url() ?>admin/dashboard">
                            <button class="btn btn-primary">BACK</button>
                        </a>

                        <!-- <a href="<?= base_url() ?>admin/inactive_vendors_shops/add">

                            <button class="btn btn-primary">+ Add Vendor</button>

                        </a> -->

                    </div>

                </div>

                <?php if (!empty($this->session->flashdata('success_message'))) { ?>

                    <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>

                        <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>

                    </div>

                <?php } ?>

                <?php if (!empty($this->session->flashdata('error_message'))) { ?>

                    <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>

                        <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>

                    </div>

                <?php }

                ?>

                <div class="ibox-content">

                 

                    <table class="table table-striped table-bordered table-hover dataTables-example">
                        <thead>
                            <tr>
                                
                                <th>VendorID</th>
                                <th>Vendor Details</th>
                                <th>Shop Image</th>
                                <th>Contact Details</th>
                                <th>Location</th>
                                <!-- <th>Vendor Pincodes</th> -->
                                <th>Joining Date</th>
                                <th>No. of Days</th>
                                <th>Refferal Code</th>
                                <th>Status</th>
                                <th>Profile Update Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $ks = 1;
                            foreach ($vendor_shops as $shop) {

                                 $total_products1 = $this->db->get_where('products', ['shop_id' => $shop->id])->result();

                                $total_products = count($total_products1);

                                $total_categories1 = $this->db->get_where('admin_comissions', ['shop_id' => $shop->id])->result();

                                $total_categories = count($total_categories1);
                                ?>
                                <tr class="gradeX">
                                    <td>#<?= $ks; ?></td>
                                    <td>#<?= $shop->id; ?></td>
                                    <td>
                                        <?php if($shop->logo!=''){?>
                                        <img class="shop_image" align="left" src="<?= base_url() ?>uploads/shops/<?= $shop->logo ?>" title="">
                                        <?php }else{ ?>
                                            <img class="shop_image" align="left" src="<?= base_url() ?>uploads/noproduct.png" title="">
                                        <?php } ?>

                                        <p><b>Vendor: </b><span class="font-weight500"><?= $shop->shop_name ?></span></p>
                                        <p><b>Address: </b><span class="font-weight500"><?= $shop->address ?></span></p>
                                    </td>
                                    <td>
                                        <?php if($shop->shop_logo!=''){ ?>
                                        <img class="shop_image" align="left" src="<?= base_url() ?>uploads/shops/<?= $shop->shop_logo ?>" title="">
                                        <?php }else{ ?>
                                            <img class="shop_image" align="left" src="<?= base_url() ?>uploads/noproduct.png" title="">
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <b>Owner Name:</b> <?= $shop->owner_name ?><br>
                                        <b>Email:</b> <?= $shop->email ?><br>
                                        <b>Mobile:</b> <?= $shop->mobile ?>
                                    </td>

                                    <td>
                                        <?php 
                                            $state_qry=$this->db->query("select * from states where id='".$shop->state_id."'"); 
                                            $state_row = $state_qry->row();

                                            $city_qry=$this->db->query("select * from cities where id='".$shop->city_id."'"); 
                                            $city_row = $city_qry->row();

                                            $loc_qry=$this->db->query("select * from locations where id='".$shop->city_id."'"); 
                                            $loc_row = $loc_qry->row();
                                        ?>
                                        <b>State:</b> <?= $state_row->state_name ?><br>
                                        <b>City:</b> <?= $city_row->city_name ?><br>
                                        <b>Location:</b> <?= $loc_row->location_name ?><br>
                                        <b>Address:</b> <?= $shop->address ?><br>
                                        <!-- <b>Pincode:</b> <?= $shop->pincode ?><br> -->
                                    </td>
                                   

                                    <td><?php 
                                            if($shop->created_date!='0000-00-00 00:00:00')
                                            {
                                                echo date('d-m-Y',strtotime($shop->created_date)); 
                                            }
                                        ?></td>
                                        <td><?php 
                                            
                                            if($shop->created_date!='0000-00-00 00:00:00')
                                            {
                                                $ydate = date('d-m-Y',strtotime($shop->created_date));

                                                $now = time(); // or your date as well
                                            $your_date = strtotime($ydate);
                                            $datediff = $now - $your_date;

                                            echo round($datediff / (60 * 60 * 24));

                                            }
                                            
                                        ?></td>

                                    <td class="center">
                                        <?php
                                        if ($shop->status == 1) {
                                            ?>
                                            <a href="<?= base_url() ?>admin/inactive_vendors_shops/changeStatus/<?= $shop->id ?>/0"><button title="Active" class="btn btn-xs btn-green">Active</button></a>
                                            <?php
                                        } else {
                                            ?>
                                            <a href="<?= base_url() ?>admin/inactive_vendors_shops/changeStatus/<?= $shop->id ?>/1"><button title="Inactive" class="btn btn-xs btn-danger">
                                                Inactive
                                            </button></a>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                    <td class="center">
                                        <?php
                                        if ($shop->update_status == 1) {
                                            echo 'NO';
                                        } else {
                                            echo 'YES';
                                        }
                                        ?>
                                    </td>
                                    <td class="center">

                                        <?php
                                        $user_type = $_SESSION['admin_login']['user_type']; 
                         
                                $admin_id = $_SESSION['admin_login']['id']; 
                                $adm_qry = $this->db->query("select * from sub_admin where id='".$admin_id."'");
                                $adm_row=$adm_qry->row();

                                $userpermissions  = $adm_row->permissions; 
                                $permissions = explode(",", $userpermissions);
                                 if($user_type=='subadmin'){ ?>

                                              <?php  if (in_array("edit_inactive_vendors", $permissions)){ ?>
                                                 <a href="<?= base_url() ?>admin/inactive_vendors_shops/edit/<?= $shop->id ?>">
                                                <button title="This operation is disabled in demo !" class="btn btn-xs btn-primary">
                                                    Edit
                                                </button>
                                            </a>
                                             <?php } if (in_array("category_inactive_vendors", $permissions)){ ?>
                                                 <a href="<?= base_url() ?>admin/vendors_shops/manage_categories?shop_id=<?= $shop->id ?>">
                                            <button title="Products" class="btn btn-xs btn-success">
                                                Manage Categories(<?= $total_categories ?>)
                                            </button>
                                        </a>
                                        <?php } if (in_array("delete_inactive_vendors", $permissions)){ ?>
                                                <a href="<?= base_url() ?>/admin/inactive_vendors_shops/delete/<?php echo $shop->id; ?>"  onclick="if(!confirm('Are you sure you want to delete this Shop?')) return false;" >
                                                <button title="Delete Shop" class="btn btn-xs btn-danger">
                                                    Delete
                                                </button>
                                            </a> 
                                             <?php } if (in_array("vendor_inactive_vendors", $permissions)){ ?>
                                                     <a target="_blank" ><form method="post" class="form-horizontal" enctype="multipart/form-data" action="<?php echo base_url(); ?>vendors/login/admin_login/">
                                            <input type="hidden" name="email" class="form-control" value="<?php echo $shop->mobile; ?>">
                                            <input type="hidden" name="password" class="form-control" value="<?php echo $shop->password; ?>">
                                            <input type="hidden" name="md5" class="form-control" value="1">

                                                <button class="btn btn-primary" type="submit" onclick="this.form.target='_blank';return true;">Manage Vendor</button>
                                        </form></a>

                                            
                                             <?php } }else{ ?>
                                            
                                               <a href="<?= base_url() ?>/admin/inactive_vendors_shops/delete/<?php echo $shop->id; ?>"  onclick="if(!confirm('Are you sure you want to delete this Shop?')) return false;" >
                                                <button title="Delete Shop" class="btn btn-xs btn-danger">
                                                    Delete
                                                </button>
                                            </a> 

                                       <a href="<?= base_url() ?>admin/inactive_vendors_shops/edit/<?= $shop->id ?>">
                                                <button title="This operation is disabled in demo !" class="btn btn-xs btn-primary">
                                                    Edit
                                                </button>
                                            </a>


                                            <button title="Products" class="btn btn-xs btn-success">
                                                Products (<?= $total_products ?>)
                                            </button>
                                        <!-- </a> -->
                                        <a href="<?= base_url() ?>admin/inactive_vendors_shops/manage_categories?shop_id=<?= $shop->id ?>">
                                            <button title="Products" class="btn btn-xs btn-success">
                                                Manage Categories(<?= $total_categories ?>)
                                            </button>
                                        </a>
                                       

                                        <a target="_blank" ><form method="post" class="form-horizontal" enctype="multipart/form-data" action="<?php echo base_url(); ?>vendors/login/admin_login/">
                                            <input type="hidden" name="email" class="form-control" value="<?php echo $shop->mobile; ?>">
                                            <input type="hidden" name="password" class="form-control" value="<?php echo $shop->password; ?>">
                                            <input type="hidden" name="md5" class="form-control" value="1">

                                                <button class="btn btn-primary" type="submit" onclick="this.form.target='_blank';return true;">Manage Vendor</button>
                                        </form></a>

                                             <?php } ?>




                                             


                                    </td>

                                </tr>

                                <?php

                                    $ks++;
                            }

                            ?>

                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>





</div>



