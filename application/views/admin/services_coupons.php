<style>
    .cat_image{
        width: 100px;
        height: 100px;
        object-fit: scale-down;
        border-radius: 10px;
        margin: 0px 5px;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $title ?></h5>
                    <div class="ibox-tools">
                        <a href="<?= base_url() ?>admin/dashboard">
                            <button class="btn btn-primary">BACK</button>
                        </a>

                        <?php
                        $user_type = $_SESSION['admin_login']['user_type'];
                        if ($user_type == 'subadmin') {
                            $admin_id = $_SESSION['admin_login']['id'];
                            $adm_qry = $this->db->query("select * from sub_admin where id='" . $admin_id . "'");
                            $adm_row = $adm_qry->row();

                            $userpermissions = $adm_row->permissions;
                            $permissions = explode(",", $userpermissions);
                            if (in_array("add_category", $permissions)) {
                                ?>

                                <a href="<?= base_url() ?>admin/services_coupons/add">
                                    <button class="btn btn-primary">+ Add Coupon</button>
                                </a>
                                <?php
                            }
                        } else {
                            ?>

                            <a href="<?= base_url() ?>admin/services_coupons/add">
                                <button class="btn btn-primary">+ Add Coupon</button>
                            </a>
                        <?php } ?>


                    </div>

                    <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                        <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                        </div>
                    <?php } ?>
                    <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                        <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                        </div>
                    <?php }
                    ?>
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Coupon Code</th>
                                    <th>Percentage</th>
                                    <th>Maximum Amount</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Utilization</th>
                                    <th>Minimum Order Amount</th>                                      
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $count = 1;
                                foreach ($coupons as $cc) {
                                    ?>
                                    <tr>
                                        <td><?= $count++ ?></td>
                                        <td><?= $cc->coupon_code ?></td>
                                        <td><?= $cc->percentage ?> %</td>
                                        <td><?= $cc->maximum_amount ?></td>
                                        <td><?= $cc->start_date ?></td>
                                        <td><?= $cc->expiry_date ?></td>
                                        <td><?= $cc->utilization ?></td>
                                        <td><?= $cc->minimum_order_amount ?></td>
                                        <td>

                                            <?php
                                            if ($user_type == 'subadmin') {
                                                if (in_array("edit_coupons", $permissions)) {
                                                    ?>
                                                    <a href="<?= base_url() ?>admin/services_coupons/edit/<?= $cc->id ?>">
                                                        <button title="This operation is disabled in demo !" class="btn btn-xs btn-primary">
                                                            Edit
                                                        </button>
                                                    </a>
                                                <?php } if (in_array("delete_coupons", $permissions)) { ?>
                                                    <a href="<?= base_url() ?>admin/services_coupons/delete/<?= $cc->id ?>">
                                                        <button title="Delete Coupon" class="btn btn-xs btn-danger">
                                                            Delete
                                                        </button>
                                                    </a>

                                                    <?php
                                                }
                                            } else {
                                                ?>

                                                <a href="<?= base_url() ?>admin/services_coupons/edit/<?= $cc->id ?>">
                                                    <button title="This operation is disabled in demo !" class="btn btn-xs btn-primary">
                                                        Edit
                                                    </button>
                                                </a>
                                                <a href="<?= base_url() ?>admin/services_coupons/delete/<?= $cc->id ?>">
                                                    <button title="Delete Coupon" class="btn btn-xs btn-danger">
                                                        Delete
                                                    </button>
                                                </a>

                                            <?php } ?>




                                        </td>

                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Coupon Code</th>
                                    <th>Percentage</th>
                                    <th>Maximum Amount</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Utilization</th>
                                    <th> Minimum Order Amount</th>                                      
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
