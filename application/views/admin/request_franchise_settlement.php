<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= $title ?></h5>
                <div class="ibox-tools">

                </div>
            </div>
            <div class="ibox-content">
                <form method="post" class="form-horizontal" enctype="multipart/form-data"  action="<?= base_url() ?>admin/franchise_settlements/insert">
                  <input type="hidden" name="franchis_id" value="<?php echo $_SESSION['admin_login']['id']; ?>">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Franchise Wallet ( <?php echo $name; ?>)</label>
                        <div class="col-sm-6">
                            <input type="text" name="franchise_wallet_amount" id="franchise_wallet_amount" class="form-control" readonly="" value="<?php echo $franchise_wallet_amount; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Requested Amount</label>
                        <div class="col-sm-6">
                            <input type="text" id="requested_amount" onkeypress='return event.charCode == 46 || (event.charCode >= 48 && event.charCode <= 57)' name="requested_amount" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">Description</label>
                        <div class="col-sm-6">
                            <textarea id="description" name="description" class="form-control"></textarea>
                        </div>
                    </div>

                    
                    <div class="form-group" id="buttondiv1">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" id="btn_offline" type="submit"> <i class="fa fa-plus-circle"></i> Request</button>
                        </div>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $('#btn_offline').click(function(){
        $('.error').remove();
            var errr=0; 
            
           var franchise_wallet_amount = parseFloat($('#franchise_wallet_amount').val());
           var requested_amount = parseFloat($('#requested_amount').val());
          
      if($('#requested_amount').val()=='' || $('#requested_amount').val()=='.')
      {
         $('#requested_amount').after('<span class="error" style="color:red;font-size: 18px;margin-left: 18px;">Enter Requested Amount</span>');
         $('#requested_amount').focus();
         return false;
      }
      else if(franchise_wallet_amount<requested_amount)
      {
          $('#requested_amount').after('<span class="error" style="color:red;font-size: 18px;margin-left: 18px;">Please check your wallet amount</span>');
           $('#requested_amount').focus();
           return false;
      }
      else if($('#description').val()=='')
      {
         $('#description').after('<span class="error" style="color:red;font-size: 18px;margin-left: 18px;">Enter Description</span>');
         $('#description').focus();
         return false;
      }
      
 });

</script>