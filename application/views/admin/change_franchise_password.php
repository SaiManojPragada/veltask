<div class="row">

    <div class="col-lg-12">

        <div class="ibox float-e-margins">

            <div class="ibox-title">

                <h5>Change Password</h5>
                <div class="ibox-tools">
                    <a href="<?= base_url() ?>admin/dashboard">
                        <button class="btn btn-primary">BACK</button>
                    </a>
                </div>

                <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                    <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                    </div>
                <?php } ?>
                <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                    <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                    </div>
                <?php }
                ?>
            </div>

            <div class="ibox-content" style="padding-bottom: 100px">

                <center>
                    <form action="" method="post" style="width: 30%; text-align: left" autocomplete="false">
                        <div class="form-group">
                            <label class="form-label">Current Password</label>
                            <input class="form-control" id="current_password" type="password" name="current_password" >
                        </div>
                        <div class="form-group">
                            <label class="form-label">New Password</label>
                            <input class="form-control" id="password" type="password" name="password" >
                        </div>
                        <div class="form-group">
                            <label class="form-label">Re-type New Password</label>
                            <input class="form-control" id="re_password" type="password" name="re_password" >
                        </div>
                        <div class="form-group">
                            <button class="btn btn-success pull-right" id="update_pass" name="update_pass" value="sub" type="submit" name="re_password"> Update Password</button>
                        </div>
                    </form>
                </center>

            </div>

        </div>

    </div>

</div>
<script type="text/javascript">

    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    $('#update_pass').click(function () {
        $('.error').remove();
        var errr = 0;
        if ($('#current_password').val() == "") {
            $('#current_password').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Previous Password</span>');
            $('#current_password').focus();
            return false;
        } else if ($('#password').val().length < 8) {
            $('#password').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Password Should be atleast 8 Charecters</span>');
            $('#password').focus();
            return false;
        } else if ($('#password').val() !== $('#re_password').val()) {
            $('#re_password').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Passwords Does not Match</span>');
            $('#re_password').focus();
            return false;
        }
    });
</script>