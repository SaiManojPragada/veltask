<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= $title ?></h5>
                <div class="ibox-tools">
                    <a href="<?= base_url() ?>admin/service_provider_features">
                        <button class="btn btn-primary">BACK</button>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form method="post" class="form-horizontal" enctype="multipart/form-data"  action="<?= base_url() ?>admin/service_provider_features/<?= $func ?><?= $data->id ?>">


                    <div class="form-group">
                        <label class="col-sm-2 control-label">Title *</label>
                        <div class="col-sm-10">
                            <input type="text" name="title" id="title" class="form-control" value="<?= $data->title ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">View Order *</label>
                        <div class="col-sm-10">
                            <input type="number" min="0" name="view_order" id="view_order" class="form-control" value="<?= $data->view_order ?>">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">Image *</label>
                        <div class="col-sm-10">
                            <input type="file" name="image" id="image" class="form-control" <?= ($data && $data->image) ? '' : 'required' ?>>
                        </div>
                    </div>
                    <?php if (!$data) { ?>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Type Enum </label>
                            <div class="col-sm-10">
                                <input type="text" min="0" name="type_enum" id="type_enum" class="form-control" value="<?= $data->type_enum ?>">
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($data && $data->image) { ?>
                        <input type="hidden" name="prev_image" class="form-control" value="<?= $data->image ?>">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Prev Image</label>
                            <div class="col-sm-10">
                                <img src="<?= base_url('uploads/service_provider_features/') . $data->image ?>" alt="alt" style="width: 100px"/>
                            </div>
                        </div>
                    <?php } ?>


                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" type="submit" id="btn_category"> <i class="fa fa-plus-circle"></i> Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    $('#btn_category').click(function () {
        $('.error').remove();
        var errr = 0;
        if ($('#title').val() == '')
        {
            $('#title').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Title </span>');
            $('#title').focus();
            return false;
        } else if ($('#view_order').val() == '')
        {
            $('#view_order').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter View Order</span>');
            $('#view_order').focus();
            return false;
        }
<?php if (!$data || !$data->image) { ?>
            else if ($('#image').val() == '')
            {
                $('#image').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Select Image</span>');
                $('#image').focus();
                return false;
            }
<?php } ?>
    });
</script>