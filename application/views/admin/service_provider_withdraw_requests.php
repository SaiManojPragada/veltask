<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Service Providers Withdraw Requests</h5>
                    <div class="ibox-tools">
                        <a href="<?= base_url() ?>admin/dashboard">
                            <button class="btn btn-primary">BACK</button>
                        </a>
                    </div>

                    <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                        <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                        </div>
                    <?php } ?>
                    <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                        <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                        </div>
                    <?php }
                    ?>
                </div>


                <div class="ibox-content">

                    <div class="row">
                        <div class="col-md-12">
                            <ul class="nav nav-tabs">
                                <li class="nav-item <?= ($this->input->get("type") != 'settled') ? 'active' : '' ?>">
                                    <a class="nav-link" href="<?= base_url('admin/service_provider_withdraw_requests') ?>">In Progress</a>
                                </li>
                                <li class="nav-item <?= ($this->input->get("type") == 'settled') ? 'active' : '' ?>">
                                    <a class="nav-link" href="<?= base_url('admin/service_provider_withdraw_requests?type=') ?>settled">Settled</a>
                                </li>
                            </ul>
                        </div>
                        <br>
                        <br>
                        <br>
                    </div>

                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>

                            <tr>
                                <th>#</th>
                                <th>Franchise Details</th>
                                <th>Requested Amount</th>
                                <th>Request Description</th>
                                <th>Requested Date</th>
                                <th>Settled Date</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($transaction as $index => $tt) { ?>
                                <tr>
                                    <td><?= $index + 1 ?></td>
                                    <td>
                                        <p><strong>Name : </strong><span id="provider_name_table"><?= $tt->service_provider_details->name ?></span></p>
                                        <p><strong>Email : </strong><?= $tt->service_provider_details->email ?></p>
                                        <p><strong>Mobile : </strong><?= $tt->service_provider_details->phone ?></p>
                                    </td>
                                    <td>
                                        <i class="fa fa-inr"> <?= $tt->requested_amount ?></i>
                                    </td>
                                    <td>
                                        <?= $tt->description ?>
                                    </td>
                                    <td>
                                        <?= $tt->created_at ?>
                                    </td>
                                    <td>
                                        <?= $tt->updated_at ?>
                                    </td>
                                    <td>
                                        <span style="color: <?= ($tt->status) ? 'green' : 'tomato' ?>;">
                                            <strong><?= $tt->status_text ?></strong>
                                        </span>
                                    </td>
                                    <td>
                                        <button class="btn btn-success btn-xs" data-target="#bank_details_modal" data-toggle="modal" onclick="getBankDetails('<?= $tt->service_provider_details->id ?>', '<?= $tt->service_provider_details->name ?>')">View Bank Details</button>
                                        <?php if (!$tt->statius) { ?>
                                            <a href="<?= base_url('admin/Service_provider_withdraw_requests/settle/' . $tt->id) ?>">
                                            <?php } ?>
                                            <button class="btn btn-primary btn-xs" <?= ($tt->status) ? 'disabled' : '' ?>>Settle Now</button>
                                            <?php if (!$tt->statius) { ?>
                                            </a>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" id="bank_details_modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="btn-close btn-default pull-right" data-dismiss="modal" aria-label="Close">&times;</button>
                </div>
                <div class="modal-body">
                    <h4><strong>Provider Name : </strong><span id="provider_name"></span></h4>
                    <div id="bank_details_div">
                        <p><strong>Bank Name : </strong><span id="bank_name"></span></p>
                        <p><strong>Account Anumber : </strong><span id="account_number"></span></p>
                        <p><strong>IFSC Code : </strong><span id="ifsc_code"></span></p>
                        <p><strong>Branch : </strong><span id="branch_name"></span></p>
                    </div>
                    <div id="bank_details_err_div">
                        <center> -- No Bank Details Found --</center>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">

    function getBankDetails(provider_id, provider_name) {
        console.log(provider_id);
        $.ajax({
            url: "<?= base_url('api/service_provider/profile/bank_details') ?>",
            type: "post",
            data: {user_id: provider_id},
            success: function (resp) {
                $("#provider_name").html(provider_name);
                console.log(resp);
                resp = JSON.parse(resp);
                if (resp['status']) {
                    var data = resp['data'];
                    $("#bank_details_div").css({"display": "block"});
                    $("#bank_details_err_div").css({"display": "none"});
                    $("#bank_name").html(data['bank_name']);
                    $("#account_number").html(data['account_number']);
                    $("#ifsc_code").html(data['ifsc_code']);
                    $("#branch_name").html(data['branch_name']);
                } else {
                    $("#bank_details_div").css({"display": "none"});
                    $("#bank_details_err_div").css({"display": "block"});
                    $("#bank_name").html("");
                    $("#account_number").html("");
                    $("#ifsc_code").html("");
                    $("#branch_name").html("");
                }
            }
        });
    }


</script> 


