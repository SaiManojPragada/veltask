<div class="row">
    <div class="col-lg-12">

        <div class="col-lg-12">

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $title ?></h5>
                    <div class="ibox-tools">
                        <a href="<?= base_url() ?>admin/dashboard">
                            <button class="btn btn-primary">BACK</button>
                        </a>
                    </div>
                </div>
                <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                    <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                    </div>
                <?php } ?>
                <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                    <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                    </div>
                <?php }
                ?>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" enctype="multipart/form-data"  action="">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Title * : </label>
                            <div class="col-sm-10">
                                <input type="text" id="title" name="title" class="form-control" value="<?php echo $about->title; ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Description * : </label>
                            <div class="col-sm-10">
                                <textarea id="description" name="description" class="form-control ck-editor" required><?php echo $about->description; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"></label>
                            <div class="col-sm-10">
                                <span id="desc_error" style="color:red;font-size: 13px;margin-left: 13px;"></span>
                            </div>
                        </div>
                        <br>


                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" type="submit" id="btn_about"> <i class="fa fa-plus-circle"></i> Update</button>
                            </div>
                        </div>
                    </form>
                </div>






            </div>
        </div>

    </div>
</div>


<script type="text/javascript">

    $('#btn_about').click(function () {
        $('.error').remove();
        var errr = 0;
        if ($('#title').val() == '')
        {
            $('#title').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter title</span>');
            $('#title').focus();
            return false;
        }
    });
</script>