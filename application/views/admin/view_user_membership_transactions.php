<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">

        <div class="col-lg-12">

            <div class="ibox float-e-margins">
                <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                    <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                    </div>
                <?php } ?>
                <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                    <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                    </div>
                <?php }
                ?>
                <div class="ibox-title">

                    <h5><?= $title ?></h5>

                    <div class="ibox-tools">
                        <a href="<?= base_url() ?>admin/users">
                            <button class="btn btn-primary">BACK</button>
                        </a> 
                    </div>

                    <div class="ibox-content">



                        <table class="table table-striped table-bordered table-hover dataTables-example">

                            <thead>

                                <tr>

                                    <th>#</th>
                                    <th>Transaction_id</th>
                                    <th>Membership Details</th>
                                    <th>Membership Discount Percentage</th>
                                    <th>Amount Paid</th>
                                    <th>Razorpay Details</th>
                                    <th>Purchased Date</th>
                                    <th>Expiry Date</th>
                                    <th>Status</th>
                                </tr>

                            </thead>

                            <tbody>
                                <?php foreach ($membership_transactions as $index => $mt) { ?>
                                    <tr>
                                        <td><?= $index + 1 ?></td>
                                        <td><?= $mt->membership_transaction_id ?></td>
                                        <td>
                                            <p><strong>Title : </strong><?= $mt->membership_details->title ?></p>
                                            <p><strong>Current Price : </strong><?= $mt->membership_details->sale_price ?></p>
                                        </td>
                                        <td><?= round($mt->membership_discount) ?>%</td>
                                        <td><?= $mt->amount_paid ?></td>
                                        <td>
                                            <p><strong>Razorpay Order Id : </strong><?= $mt->razorpay_order_id ?></p>
                                            <p><strong>Razorpay Transaction Id : </strong><?= $mt->razorpay_transaction_id ?></p>
                                        </td>
                                        <td><?= date('d M Y, h:i A', $mt->created_at) ?></td>
                                        <td><?= date('d M Y, h:i A', $mt->expiry_date) ?></td>
                                        <td>
                                            <?= ($mt->status) ? "<span style='color: green'>Active</span>" : "<span style='color: tomato'>Inactive</span>" ?>
                                        </td>
                                    </tr>
                                <?php } ?>

                            </tbody>

                        </table>

                    </div>

                </div>

            </div>

        </div>





    </div>



