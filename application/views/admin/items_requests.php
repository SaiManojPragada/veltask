<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $title ?></h5>
                    <br><br>
                    <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                        <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                        </div>
                    <?php } ?>
                    <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                        <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                        </div>
                    <?php }
                    ?>
                </div>
                <div class="ibox-content">

                    <table class="table table-striped table-bordered table-hover dataTables-example">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Request Id</th>
                                <th>Details</th>
                                <th>Requested On</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            foreach ($requests as $item) {
                                ?>
                                <tr>
                                    <td><?= $i++ ?></td>
                                    <td><?= $item->request_id ?></td>
                                    <td>
                                        <?php foreach ($item->requested_items as $it) { ?>
                                            <p><?= $it->item ?>&nbsp;&nbsp; qty: <?= $it->quantity ?></p>
                                        <?php } ?>
                                    </td>
                                    <td><?= date('d-m-Y h:i A', $item->created_at) ?></td>
                                    <td>
                                        <?php if ($item->request_status == "Completed") { ?>
                                            <span style="color: green"> <?= $item->request_status ?></span>
                                        <?php } else if ($item->request_status == "Cancelled") { ?>
                                            <span style="color: tomato"> <?= $item->request_status ?></span>
                                        <?php } else { ?>
                                            <span style="color: orange"> <?= $item->request_status ?></span>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>

