<style>
    .cat_image{
        width: 100px;
        height: 100px;
        object-fit: scale-down;
        border-radius: 10px;
        margin: 0px 5px;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $title ?></h5>
                    <div class="ibox-tools">
                        <a href="<?= base_url() ?>admin/dashboard">
                            <button class="btn btn-primary">BACK</button>
                        </a>

                        <?php
                        $user_type = $_SESSION['admin_login']['user_type'];
                        if ($user_type == 'subadmin') {
                            $admin_id = $_SESSION['admin_login']['id'];
                            $adm_qry = $this->db->query("select * from sub_admin where id='" . $admin_id . "'");
                            $adm_row = $adm_qry->row();

                            $userpermissions = $adm_row->permissions;
                            $permissions = explode(",", $userpermissions);
                            if (in_array("add_category", $permissions)) {
                                ?>

                                <a href="<?= base_url() ?>admin/memberships/add">
                                    <button class="btn btn-primary">+ Add Membership</button>
                                </a>
                                <?php
                            }
                        } else {
                            ?>

                            <a href="<?= base_url() ?>admin/memberships/add">
                                <button class="btn btn-primary">+ Add Membership</button>
                            </a>
                        <?php } ?>


                    </div>

                    <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                        <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                        </div>
                    <?php } ?>
                    <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                        <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                        </div>
                    <?php }
                    ?>
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Duration in Days</th>
                                    <th>Duration In Words</th>
                                    <th>Price</th>
                                    <th>Sale Price</th>
                                    <th>Discount %</th>
                                    <th>Max Discount Amount</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($data as $item) {
                                    ?>
                                    <tr>
                                        <td><?= $i++ ?></td>
                                        <td><?= $item->title ?></td>
                                        <td><?= $item->duration_in_days ?></td>
                                        <td><?= $item->duration_in_words ?></td>
                                        <td><?= $item->price ?></td>
                                        <td><?= $item->sale_price ?></td>
                                        <td><?= $item->discount_percentage ?></td>
                                        <td><?= $item->max_discount ?></td>
                                        <td>
                                            <?php
                                            if ($item->status) {
                                                ?>
                                    <center><span style="color: green">Active</span></center>
                                    <?php
                                } else {
                                    ?>
                                    <center><span style="color: tomato">InActive</span></center>
                                <?php } ?>
                                </td>
                                <td>
                                    <?php
                                    if ($user_type == 'subadmin') {
                                        if (in_array("edit_category", $permissions)) {
                                            ?>

                                            <a href="<?= base_url() ?>admin/memberships/edit/<?= $item->id ?>">
                                                <button title="This operation is disabled in demo !" class="btn btn-xs btn-primary">
                                                    Edit
                                                </button>
                                            </a>
                                        <?php } if (in_array("delete_category", $permissions)) { ?>
                                            <a href="<?= base_url() ?>admin/memberships/delete/<?= $item->id ?>">
                                                <button title="Delete Category" class="btn btn-xs btn-danger" onclick="if (!confirm('Are you sure you want to delete this Category?'))
                                                            return false;">
                                                    Delete
                                                </button>
                                            </a>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <a href="<?= base_url() ?>admin/memberships/edit/<?= $item->id ?>">
                                            <button title="This operation is disabled in demo !" class="btn btn-xs btn-primary">
                                                Edit
                                            </button>
                                        </a>

                                        <a href="<?= base_url() ?>admin/memberships/delete/<?= $item->id ?>">
                                            <button title="Delete Category" class="btn btn-xs btn-danger" onclick="if (!confirm('Are you sure you want to delete this Category?'))
                                                        return false;">
                                                Delete
                                            </button>
                                        </a>

                                    <?php } ?>
                                </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Duration</th>
                                    <th>Duration In Words</th>
                                    <th>Price</th>
                                    <th>Sale Price</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
