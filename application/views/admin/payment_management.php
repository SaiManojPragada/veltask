<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">

        <div class="col-lg-12">

            <div class="ibox float-e-margins">
                <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                    <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                    </div>
                <?php } ?>
                <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                    <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                    </div>
                <?php }
                ?>
                <div class="ibox-title">

                    <h5><?= $title ?></h5>

                    <div class="ibox-tools">
                        <a href="<?= base_url() ?>admin/dashboard">
                            <button class="btn btn-primary">BACK</button>
                        </a> 
                    </div>

                    <div class="ibox-content">



                        <table class="table table-striped table-bordered table-hover dataTables-example">

                            <thead>

                                <tr>

                                    <th>#</th>
                                    <th>Payment Type Title</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>

                            </thead>

                            <tbody>


                                <?php
                                $i = 0;
                                foreach ($payments as $item) {
                                    ?>
                                    <tr class="gradeX">

                                        <td><?= $i++ ?></td>

                                        <td><?= $item->payment_method_title; ?></td>

                                        <td><?= ($item->status) ? "<span style='color: green'>Active</span>" : "<span style='color: tomato'>Inactive</span>" ?></td>
                                        <td>

                                            <a class="btn btn-xs btn-success" href="<?= base_url('admin/payment_management/edit/') . $item->id ?>">Edit</a>

                                        </td>



                                    </tr>
                                <?php } ?>


                            </tbody>

                        </table>

                    </div>

                </div>

            </div>

        </div>





    </div>



