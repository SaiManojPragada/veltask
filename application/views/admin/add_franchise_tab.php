<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= $title ?></h5>
                <div class="ibox-tools">
                    <a href="<?= base_url() ?>admin/franchise_tabs">
                        <button class="btn btn-primary">BACK</button>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form method="post" class="form-horizontal" enctype="multipart/form-data" id="blogs-form"  action="<?= base_url() ?>admin/franchise_tabs/<?= $func ?><?= $data->id ?>">

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Title *</label>
                        <div class="col-sm-10">
                            <input type="text" name="count" id="count" class="form-control" value="<?= $data->count ?>" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Sub Title *</label>
                        <div class="col-sm-10">
                            <input type="text" name="quote" id="quote" class="form-control" value="<?= $data->quote ?>" required>
                        </div>
                    </div>



                    <div class="form-group">
                        <label class="col-sm-2 control-label">Image *</label>
                        <div class="col-sm-10">
                            <input type="file" name="image" id="image" class="form-control" value="<?= $data->image ?>" <?= empty($data->image) ? 'required' : '' ?>>
                        </div>
                    </div>

                    <?php if (!empty($data->image)) { ?>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Prev Image</label>
                            <div class="col-sm-10">
                                <img src="<?= base_url('uploads/franchise_tabs/') . $data->image ?>" alt="alt" width="200"/>
                            </div>
                        </div>
                    <?php } ?>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">Priority *</label>
                        <div class="col-sm-10">
                            <input type="number" name="priority" id="priority" class="form-control" value="<?= $data->priority ?>" required>
                        </div>
                    </div>



                    <div class="form-group">
                        <label class="col-sm-2 control-label">Status *</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="status" name="status" required>
                                <option value="">Select Status</option>
                                <option value="1" <?= ($data && $data->status) ? "selected" : "" ?>>Active</option>
                                <option value="0" <?= ($data && !$data->status) ? "selected" : "" ?>>InActive</option>
                            </select>
                        </div>
                    </div>


                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" type="submit" id="btn_category"> <i class="fa fa-plus-circle"></i> Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script src="<?= base_url('web_assets/') ?>/js/plugins/parsleyjs/dist/parsley.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#blogs-form').parsley();
    });
//    function validateEmail($email)
//    {
//        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
//        if (!emailReg.test($email)) {
//            return false;
//        } else
//        {
//            return true;
//        }
//    }
</script>