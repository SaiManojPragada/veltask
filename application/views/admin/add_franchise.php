<style>
    .category_comm_span{
        top: -5px;
        position: relative;
        left: 10px;
    }
    .cat_commission{
        top: -5px;
        position: relative;
        left: 21px;
    }
</style>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= $title ?></h5>
                <div class="ibox-tools">
                    <a href="<?= base_url() ?>admin/franchises">
                        <button class="btn btn-primary">BACK</button>
                    </a>
                </div>
                <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                    <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                    </div>
                <?php } ?>
                <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                    <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                    </div>
                <?php }
                ?>
            </div>
            <div class="ibox-content test">
                <form method="post" class="form-horizontal" enctype="multipart/form-data" action="<?= base_url() ?>admin/franchises/insert">

                    <!-- <div class="form-group">
                        <label class="col-sm-2 control-label">Delivery Types</label>
                        <div class="col-sm-10">
                          <select id="delivery_type" name="delivery_type" class="form-control">
                             <option value="">Select Delivery Types</option>
                             <option value="full_time_driver">Full Time Driver</option>
                             <option value="pay_for_driver">Pay for Driver</option>
                          </select>
                        </div>
                    </div>
                    -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Name *</label>
                        <div class="col-sm-10">
                            <input type="text" id="name" name="name" class="form-control" value="<?= $prev_data['name'] ?>">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">Email *</label>
                        <div class="col-sm-10">
                            <input type="email" id="email" name="email" class="form-control" value="<?= $prev_data['email'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Passport size Photo *</label>
                        <div class="col-sm-10">
                            <input type="file" id="photo" name="photo" class="form-control" value="<?= $prev_data['photo'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Aadhar Card Photo </label>
                        <div class="col-sm-10">
                            <input type="file" id="aadhar_photo" name="aadhar_photo" class="form-control" value="<?= $prev_data['aadhar_photo'] ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Vaccination Certificate <br><small>(PDF)</small></label>
                        <div class="col-sm-10">
                            <input type="file" id="vaccination_certificate" name="vaccination_certificate" class="form-control" value="<?= $prev_data['vaccination_certificate'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">GSt Number </label>
                        <div class="col-sm-10">
                            <input type="text" id="gst_number" name="gst_number" class="form-control" value="<?= $prev_data['gst_number'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Mobile Number *</label>
                        <div class="col-sm-10">
                            <input type="text" onkeypress="return isNumberKey(event)" value="<?= $prev_data['mobile_number'] ?>" title="Please enter exactly 10 digits" id="mobile_number" name="mobile_number" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Alternative Mobile Number</label>
                        <div class="col-sm-10">
                            <input type="text" onkeypress="return isNumberKey(event)" value="<?= $prev_data['alternative_mobile'] ?>" title="Please enter exactly 10 digits" id="alternative_mobile" name="alternative_mobile" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">State *</label>
                        <div class="col-sm-10">
                            <select id="state_id" data-placeholder="Choose State" style="width: 100%;" class="form-control" name="state_id" onchange="getCities(this.value)">
                                <option value=""> Select State</option>
                                <?php foreach ($states as $state) { ?>
                                    <option value="<?= $state->id ?>" <?php
                                    if ($prev_data && $prev_data['state'] == $state->id) {
                                        echo "selected";
                                    }
                                    ?>><?= $state->state_name ?></option>
                                        <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">City *</label>
                        <div class="col-sm-10">
                            <select id="location_id" data-placeholder="Choose City" style="width: 100%;" class="form-control" name="location_id" onchange="getPincodes(this.value);">
                                <option value="">Select Cities</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Pincode *</label>
                        <div class="col-sm-10">
                            <select id="pincode_id" data-placeholder="Select Pincode" style="width: 100%;" class="form-control chosen-select" multiple name="pincode_ids[]">
                                <option value=""> Select Pincodes</option>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">Password *</label>
                        <div class="col-sm-10">
                            <input type="password" id="password" name="password"  value="<?= $prev_data['password'] ?>" class="form-control">
                        </div>
                    </div>


                    <!--                    <div class="form-group">
                                            <label class="col-sm-2 control-label">Commission (% percentage) *</label>
                                            <div class="col-sm-10">
                                                <input type="number" id="commission" name="commission" class="form-control"  value="<?= $prev_data['commission'] ?>">
                                            </div>
                                        </div>-->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Address *</label>
                        <div class="col-sm-10">
                            <input type="text" id="address" name="address" class="form-control" value="<?= $prev_data['address'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-4">
                        </div>
                        <div class="col-sm-8">
                            <a href="https://www.latlong.net/" target="_blank" class="btn btn-primary">Get Latitude and Longitude</a>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Latitude *</label>
                        <div class="col-sm-10">
                            <input type="text" id="latitude" name="latitude" class="form-control" value="<?= $prev_data['latitude'] ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Longitude *</label>
                        <div class="col-sm-10">
                            <input type="text" id="longitude" name="longitude" class="form-control" value="<?= $prev_data['longitude'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Status</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="status" name="status">
                                <option value="">Select Status</option>
                                <option value="1" <?= ($prev_data['status']) ? "selected" : "" ?>>Active</option>
                                <option value="0" <?= (!$prev_data['status']) ? "selected" : "" ?>>InActive</option>
                            </select>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" id="btn_francises" type="submit">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">



//    function get_pincodes_json(id) {
//        $.ajax({
//            url: "<?= base_url('api/admin_ajax/locations/get_pincodes_json') ?>",
//            type: "post",
//            data: {city_id: id},
//            success: function (resp) {
//                $('ul.chosen-results').empty();
//                $('#pincode_id').empty();
//                resp = JSON.parse(resp);
//                var sel = "";
//                var chzn = "";
//                for (var i = 0; i < resp.length; i++) {
//                    $('ul.chosen-results').append('<li class="active-result">' + resp[i]['pincode'] + '</li>');
//                    $('#pincode_id').append('<option value="' + resp[i]["id"] + '">' + resp[i]['pincode'] + '</option>');
//                }
////                response($.map(resp, function (item) {
////                    $('ul.chzn-results').append('<li class="active-result">' + item.name + '</li>');
////                }));
//            }
//        });
//    }

    function getCities(val) {
        $.ajax({
            url: "<?= base_url() ?>admin/locations/getCities",
            type: "post",
            data: {state_id: val},
            success: function (resp) {
                $("#location_id").html(resp);
            }
        });
    }

    function getPincodes(val) {
        $.ajax({
            url: "<?= base_url() ?>admin/locations/getPincodes",
            type: "post",
            data: {city_id: val},
            success: function (resp) {
                console.log(resp);
                $("#pincode_id").html(resp);
                $('#pincode_id').trigger('chosen:updated');
            }
        });
    }

    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    $('#btn_francises').click(function () {
        $('.error').remove();
        var errr = 0;
        var ph = $('#mobile_number').val();


        if ($('#name').val() == '')
        {
            $('#name').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Name</span>');
            $('#name').focus();
            return false;
        } else if ($('#email').val() == '')
        {
            $('#email').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Email</span>');
            $('#email').focus();
            return false;
        } else if (!validateEmail($('#email').val()))
        {
            $('#email').after('<span class="error" style="color:red">Invalid Email Address</span>');
            $('#email').focus();
            return false;
        } else if ($('#photo').val() == '')
        {
            $('#photo').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Select Photo</span>');
            $('#photo').focus();
            return false;
        } else if ($('#mobile_number').val() == '')
        {
            $('#mobile_number').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Mobile</span>');
            $('#mobile_number').focus();
            return false;
        } else if (ph.length != 10)
        {
            $('#mobile_number').after('<span class="error" style="color:red">Enter Valid 10 digit Phone Number</span>');
            $('#mobile_number').focus();
            return false;
        } else if ($('#state_id').val() == '')
        {
            $('#state_id').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Select State</span>');
            $('#state_id').focus();
            return false;
        } else if ($('#location_id').val() == '')
        {
            $('#location_id').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Select City</span>');
            $('#location_id').focus();
            return false;
        } else if ($('#pincode_id').val() == '')
        {
            $('#pincode_id').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Select Pincode</span>');
            $('#pincode_id').focus();
            return false;
        } else if ($('#password').val() == '')
        {
            $('#password').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter password</span>');
            $('#password').focus();
            return false;
        } else if ($('#password').val().length < 8)
        {
            $('#password').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Password is min 8 Charecters</span>');
            $('#password').focus();
            return false;
        }
        //        else if ($('#commission').val() == '')
        //        {
        //            $('#commission').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Commission</span>');
        //            $('#commission').focus();
        //            return false;
        //        } 
        else if ($('#address').val() == '')
        {
            $('#address').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Address</span>');
            $('#address').focus();
            return false;
        } else if ($('#latitude').val() == '')
        {
            $('#latitude').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Latitude</span>');
            $('#latitude').focus();
            return false;
        } else if ($('#longitude').val() == '')
        {
            $('#longitude').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Longitude</span>');
            $('#longitude').focus();
            return false;
        }
    });

    function validateEmail($email)
    {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if (!emailReg.test($email)) {
            return false;
        } else
        {
            return true;
        }
    }
</script>


<link href="https://test.indiasmartlife.com/admin_assets/css/jquery.datetimepicker.css" rel="stylesheet">
<script src="https://test.indiasmartlife.com/admin_assets/js/jquery.datetimepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $('.datepicker').datetimepicker({
            timepicker: false,
            format: 'Y-m-d',
            scrollInput: false
        });
        $(document).on('mousewheel', '.datepicker', function () {
            return false;
        });

        $('.datepickertimepicker').datetimepicker({
            timepicker: true,
            format: 'Y-m-d H:i',
            scrollInput: false
        });
        $(document).on('mousewheel', '.datepickertimepicker', function () {
            return false;
        });

        $('#cities').on('change', function () {
            var city_id = $('#cities').val();

            loadCityLocations(city_id);
        });




        function loadCityLocations(city_id) {
            //alert(city);
            // $('.modal').modal('show');
            $.get("<?= base_url() ?>api/admin_ajax/admin/get_city_locations", "city_id=" + city_id,
                    function (response, status, http) {
                        //$('.modal').modal('hide');
                        $('#locations').html(response);
                    }, "html");
        }

    });
</script>