<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h2><?= $service_name ?> - Service Reviews </h2>
                    <div class="ibox-tools">
                        <a href="<?= base_url() ?>admin/dashboard">
                            <button class="btn btn-primary">BACK</button>
                        </a>
                    </div>

                    <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                        <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                        </div>
                    <?php } ?>
                    <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                        <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                        </div>
                    <?php }
                    ?>
                </div>


                <div class="ibox-content">

                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>S.No</th>
                                <th>User Details</th>
                                <th>Rating</th>
                                <th>Review</th>
                                <th>Last Updated At</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($reviews as $index => $rev) { ?>
                                <tr>
                                    <td><?= $index + 1 ?></td>
                                    <td>
                                        <?php if ($rev->user_data) { ?>
                                            <p><?= $rev->user_data->first_name ?></p>
                                            <p><?= $rev->user_data->phone ?></p>
                                        <?php } else { ?>
                                            N/A
                                        <?php } ?>
                                    </td>
                                    <td><i class="fa fa-star" style="color: goldenrod"></i>&nbsp;&nbsp;<?= $rev->rating ?></td>
                                    <td><?= $rev->comment ?></td>
                                    <td><?= date('d M Y', $rev->updated_at) ?></td>
                                    <td><?php
                                        if ($rev->status) {
                                            $op_stat = "Active";
                                            $color_stat = "green";
                                        } else {
                                            $op_stat = "Inactive";
                                            $color_stat = "Tomato";
                                        }
                                        ?> <span style="color: <?= $color_stat ?>"><?= $op_stat ?></span></td>
                                    <td>
                                        <?php if ($rev->status) { ?>
                                            <button class="btn btn-xs btn-warning" onclick="disable('<?= $rev->id ?>');">disable</button>
                                        <?php } else { ?>
                                            <button class="btn btn-xs btn-primary" onclick="enable('<?= $rev->id ?>');">enable</button>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>

<script>
    function disable(id) {
        if (confirm("Are you sure want to disable this comment ? ")) {
            location.href = "<?= base_url('admin/services/disable/') ?>" + id;
        }
    }

    function enable(id) {
        if (confirm("Are you sure want to enable this comment ? ")) {
            location.href = "<?= base_url('admin/services/enable/') ?>" + id;
        }
    }
</script>