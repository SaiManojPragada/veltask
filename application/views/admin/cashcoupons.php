<style>
    .cat_image{
        width: 100px;
        height: 100px;
        object-fit: scale-down;
        border-radius: 10px;
        margin: 0px 5px;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cash Coupons</h5>
                    <div class="ibox-tools">
                        <a href="<?= base_url() ?>admin/dashboard">
                            <button class="btn btn-primary">BACK</button>
                        </a>

                        <?php 
                        $user_type = $_SESSION['admin_login']['user_type']; 
                        if($user_type=='subadmin'){ 
                                $admin_id = $_SESSION['admin_login']['id']; 
                                $adm_qry = $this->db->query("select * from sub_admin where id='".$admin_id."'");
                                $adm_row=$adm_qry->row();

                                $userpermissions  = $adm_row->permissions; 
                                $permissions = explode(",", $userpermissions);
                        if (in_array("add_cash_coupons", $permissions)){ ?>

                        <a href="<?= base_url() ?>admin/cashcoupons/add">
                            <button class="btn btn-primary">+ Add Cash Coupon</button>
                        </a>

                        <?php } }else{ ?>
                            
                        <a href="<?= base_url() ?>admin/cashcoupons/add">
                            <button class="btn btn-primary">+ Add Cash Coupon</button>
                        </a>
                        <?php } ?>

                        
                    </div>

                    <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                        <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                        </div>
                    <?php } ?>
                    <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                        <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                        </div>
                    <?php }
                    ?>
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Coupon Code</th>
                                    <th>User</th>
                                    
                                    <th>Amount</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Description</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($coupons as $v) {
                                    $user = $this->db->query("select * from users where id='".$v->user_id."'");
                                    $user_row = $user->row();
                                    ?>
                                    <tr class="gradeX">
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $v->coupon_code; ?></td>
                                         <td><?php echo $user_row->first_name." ".$user_row->last_name; ?></td>
                                        
                                        <td><?php echo $v->amount; ?></td>
                                        <td><?php echo $v->start_date; ?></td>
                                        <td><?php echo $v->expiry_date; ?></td>
                                       <td><?php echo $v->description; ?></td>
                                        <td>

                                             <?php
                                        if($user_type=='subadmin'){ 
                                         if (in_array("edit_cash_coupons", $permissions)){ ?>
                                             <a href="<?= base_url() ?>admin/cashcoupons/edit/<?= $v->id ?>">
                                                <button title="This operation is disabled in demo !" class="btn btn-xs btn-primary">
                                                    Edit
                                                </button>
                                            </a>
                                             <?php }  }else{ ?>
                                            
                                                <a href="<?= base_url() ?>admin/cashcoupons/edit/<?= $v->id ?>">
                                                <button title="This operation is disabled in demo !" class="btn btn-xs btn-primary">
                                                    Edit
                                                </button>
                                            </a>

                                             <?php } ?>

                                            
                                            
                                        </td>


                                    </tr>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
