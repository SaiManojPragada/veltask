<style>
    .cat_image{
        width: 100px;
        height: 100px;
        object-fit: scale-down;
        border-radius: 10px;
        margin: 0px 5px;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $title ?></h5>
                    <div class="ibox-tools">
                        <a href="<?= base_url() ?>admin/dashboard">
                            <button class="btn btn-primary">BACK</button>
                        </a>

                        <?php
                        $user_type = $_SESSION['admin_login']['user_type'];
                        if ($user_type == 'subadmin') {
                            $admin_id = $_SESSION['admin_login']['id'];
                            $adm_qry = $this->db->query("select * from sub_admin where id='" . $admin_id . "'");
                            $adm_row = $adm_qry->row();

                            $userpermissions = $adm_row->permissions;
                            $permissions = explode(",", $userpermissions);
                            if (in_array("add_category", $permissions)) {
                                ?>

                                                                                                                <!--                                <a href="<?= base_url() ?>admin/services_time_slots/add">
                                                                                                                                                    <button class="btn btn-primary">+ Add Time Slot</button>
                                                                                                                                                </a>-->
                                <?php
                            }
                        } else {
                            ?>

                                                        <!--                            <a href="<?= base_url() ?>admin/services_time_slots/add">
                                                                                        <button class="btn btn-primary">+ Add Time Slot</button>
                                                                                    </a>-->
                        <?php } ?>


                    </div>

                    <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                        <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                        </div>
                    <?php } ?>
                    <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                        <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                        </div>
                    <?php }
                    ?>
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info" role="grid">

                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Time Slot</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
                                foreach ($slots as $ssl) {
                                    ?>
                                    <tr>
                                        <td><?= $i++ ?></td>
                                        <td><?= $ssl->time_slot ?></td>
                                        <td><button class="btn btn-xs btn-danger"
                                                    onclick="confirm('Are you sure want to delete this Time Slot ? ') ? alert('delete comes here') : false;">Delete</button></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
