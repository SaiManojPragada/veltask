<style>
    .cat_image{
        width: 100px;
        height: 100px;
        object-fit: scale-down;
        border-radius: 10px;
        margin: 0px 5px;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $title ?></h5>
                    <div class="ibox-tools">
                        <a href="<?= base_url() ?>admin/dashboard">
                            <button class="btn btn-primary">BACK</button>
                        </a>

                        <?php
                        $user_type = $_SESSION['admin_login']['user_type'];
                        if ($user_type == 'subadmin') {
                            $admin_id = $_SESSION['admin_login']['id'];
                            $adm_qry = $this->db->query("select * from sub_admin where id='" . $admin_id . "'");
                            $adm_row = $adm_qry->row();

                            $userpermissions = $adm_row->permissions;
                            $permissions = explode(",", $userpermissions);
                            if (in_array("add_category", $permissions)) {
                                ?>

                                <a href="<?= base_url() ?>admin/services/add">
                                    <button class="btn btn-primary">+ Add Service</button>
                                </a>
                                <?php
                            }
                        } else {
                            ?>

                            <a href="<?= base_url() ?>admin/services/add">
                                <button class="btn btn-primary">+ Add Service</button>
                            </a>
                        <?php } ?>


                    </div>

                    <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                        <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                        </div>
                    <?php } ?>
                    <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                        <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                        </div>
                    <?php }
                    ?>
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Details</th>
                                    <th>Price</th>
                                    <th>Sale Price</th>
                                    <th>Status</th>
                                    <th>Priority</th>
                                    <th>Has Visit & Quote</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($services as $item) {
                                    ?>
                                    <tr class="gradeX">
                                        <td><?= $i ?></td>
                                        <td>
                                            <p><b>Service Name : </b><?= $item->service_name ?></p>
                                            <p><b>Category Name : </b><?= $item->category_name ?></p>
                                            <p><b>Sub Category Name : </b><?= $item->sub_category_name ?></p>
                                            <p><b>Duration : </b><?= $item->duration ?></p>
                                        </td>

                                        <td>
                                            <?= $item->price ?>
                                        </td>


                                        <td>
                                            <?= $item->sale_price ?>
                                        </td>

                                        <td>
                                            <?php if ($item->status == 1) { ?>
                                                <button title="This operation is Enabled !" disabled="" class="btn btn-xs btn-success">
                                                    Active
                                                </button>
                                            <?php } else { ?>
                                                <button title="This operation is disabled in demo !" disabled="" class="btn btn-xs btn-danger">
                                                    In Active
                                                </button>
                                            <?php } ?>

                                        </td>

                                        <td><?php echo $item->priority; ?></td>
                                        <td>
                                            <?php if ($item->has_visit_and_quote) { ?>
                                                <span class="text-success">Yes</span>
                                            <?php } else { ?>
                                                <span class="text-danger">No</span>
                                            <?php } ?>
                                        </td>
                                        <td>

                                            <?php
                                            if ($user_type == 'subadmin') {
                                                if (in_array("edit_category", $permissions)) {
                                                    ?>

                                                    <a href="<?= base_url() ?>admin/services/edit/<?= $item->id ?>">
                                                        <button title="This operation is disabled in demo !" class="btn btn-xs btn-primary">
                                                            Edit
                                                        </button>
                                                    </a>
                                                <?php } if (in_array("delete_category", $permissions)) { ?>
                                                    <a href="<?= base_url() ?>admin/services/delete/<?= $item->id ?>">
                                                        <button title="Delete Category" class="btn btn-xs btn-danger" onclick="if (!confirm('Are you sure you want to delete this Category?'))
                                                                                return false;">
                                                            Delete
                                                        </button>
                                                    </a>
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <a href="<?= base_url() ?>admin/services/edit/<?= $item->id ?>">
                                                    <button title="This operation is disabled in demo !" class="btn btn-xs btn-primary">
                                                        Edit
                                                    </button>
                                                </a>

                                                <a href="<?= base_url() ?>admin/services/delete/<?= $item->id ?>">
                                                    <button title="Delete Category" class="btn btn-xs btn-danger" onclick="if (!confirm('Are you sure you want to delete this Category?'))
                                                                        return false;">
                                                        Delete
                                                    </button>
                                                </a>

                                            <?php } ?>
                                            <br>
                                            <a href="<?= base_url() ?>admin/services/images/<?= $item->id ?>">
                                                <button title="Manage Images" class="btn btn-xs btn-success">
                                                    Manage Services Images
                                                </button>
                                            </a>

                                            <a href="<?= base_url() ?>admin/services/manage_reviews/<?= $item->id ?>">
                                                <button title="Manage Images" class="btn btn-xs btn-warning">
                                                    Manage Services Reviews
                                                </button>
                                            </a>
                                        </td>


                                    </tr>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Details</th>
                                    <th>Price</th>
                                    <th>Sale Price</th>
                                    <th>Status</th>
                                    <th>Priority</th>
                                    <th>Has Visit & Quote</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
