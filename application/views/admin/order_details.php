<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Orders</h5> 

                    <div class="ibox-tools">
                            <a href="<?= base_url() ?>admin/orders">
                            <button class="btn btn-primary">BACK</button>
                        </a>
                    </div>
                </div>
                <h3 style="text-align: center;color: red;"> <?php if($orders->bid_id!=0){ echo 'Bid Order'; }?></h3>
                <div class="ibox-content">
                        <?php //echo "<pre>"; print_r($orders); ?>
                        
                        <div class="col-lg-12">
                            <div class="col-lg-6">
                                <h3>User Details </h3>
                                <?php 
                                
                                
                                $ad = $this->db->query("select * from user_address where id='".$orders->deliveryaddress_id."'");
                                $address = $ad->row();

                                $city_qry = $this->db->query("select * from cities where id='".$address->city."'");
                                $city_row = $city_qry->row();

                                $state_qry = $this->db->query("select * from states where id='".$address->state."'");
                                $state_row = $state_qry->row();

                                $area_qry = $this->db->query("select * from areas where id='".$address->area."'");
                                $area_row = $area_qry->row();
            
                                
                                $user = $this->db->query("select * from users where id='".$address->user_id."'");
                                $users = $user->row();
                                ?>
                                 <b>
                               <!--  <?php echo $address->name; ?>,</b><br>

                                 <?php echo $address->address; ?>,<br>
                                <?php echo $area_row->area; ?>,<br>
                                <?php echo $state_row->state; ?>,<br>
                                <?php echo $city_row->city; ?>- <?php echo $address->pincode; ?>,<br> -->
                                <?php echo $orders->user_address; ?><br>
                                Contact Numbers : <?php echo $address->mobile; ?>,<br>
                                Email : <?php echo $users->email; ?><br>
                               <!--  <?php if($address->address_type==1){ echo "HOME"; }else if($address->address_type==2){ echo "OFFICE"; }else if($address->address_type==3){ echo "OTHERS"; }?> -->
                            </div>
                            <div class="col-lg-6">
                                <?php 
                                $vend = $this->db->query("select * from vendor_shop where id='".$orders->vendor_id."'");
                                $vendors = $vend->row();
                                ?>
                                <b>Vendor : </b><?php echo $vendors->shop_name;?>,<br>
                                <b>Payment Status : </b><?php if($orders->payment_status==1){ echo "PAID"; }else{  echo "UNPAID";} ?>,<br>
                                <b>OrderID : </b><?php echo $orders->id;?>,<br>
                                <b>Invoice Generated Date : </b><?php echo date("d-m-Y h:i A",$orders->created_at);?>,<br>
                                <b>Payment Type : </b><?php echo $orders->payment_option;?>,<br>	
                                <b>Order Status : </b><?php if($orders->order_status==1){ echo "Pending";  }else if($orders->order_status==2){ echo "Proccessing";  }else if($orders->order_status==3){ echo "Assigned to delivery to pick up";  }else if($orders->order_status==4){ echo "Delivery Boy On the way";  }else if($orders->order_status==5){ echo "Delivered";  }else if($orders->order_status==6){ echo "Cancelled";  }else if($orders->order_status==7){ echo "Refund Completed";  }?>
                            </div>
                        </div>
                            
                    <table class="table table-striped">
                        <thead>
                             <tr>
                                 <th>Item Details</th>
                            </tr>
                            <tr>
                                <th>#</th>
                                <th>Product Title</th>
                                <th>Attributes</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>GST</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i=1;
                            if(count($order_cart)>0)
                            {
                            foreach($order_cart as $ord){ 
                                         $adm_qry = $this->db->query("select * from  admin_comissions where cat_id='".$ord['cat_id']."' and shop_id='".$ord['vendor_id']."'");
                                         if( $adm_qry->num_rows()>0)
                                         {
                                            $adm_comm = $adm_qry->row();
                                            $p_gst = $adm_comm->gst;

                                         }
                                         else
                                         {
                                            $p_gst = '0';
                                         }

                                         $class_percentage = ($ord['unit_price']/100)*$p_gst;
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $ord['product_name']; ?></td>
                                <td><?php foreach($ord['attributes'] as $at){ ?><b><?php echo $at['type']; ?> : </b> <?php echo $at['value']; ?><br><?php } ?></td>
                                <td><?php echo $ord['quantity']; ?></td>
                                <td>₹<?php echo $ord['price']; ?></td>
                                <td>₹<?php echo $class_percentage; ?> ( <?php echo $p_gst?> %)</td>
                                <td>₹<?php echo $ord['unit_price']; ?></td>
                            </tr>
                            <?php $i++; } }else{?>
                            
                            <?php } ?>
                            

                            <?php if($orders->bid_id==0){ ?>
                            <tr>
                                <td >
                                </td>
                                <td >
                                </td>
                                <td >
                                </td>
                                <td >
                                    
                                </td>
                                <td >
                                    
                                </td>
                                
                                <td >
                                    <b>Sub Total</b>
                                </td>
                                <td >
                                    ₹<?php echo $orderdetails->sub_total; ?>
                                </td>
                                
                                
                            </tr>

                            <tr>
                                <td >
                                </td>
                                <td >
                                </td>
                                <td >
                                </td>
                                <td >
                                    
                                </td>
                                <td >
                                    
                                </td>
                                
                                <td >
                                    <b>Coupon Code : <?php if($orderdetails->coupon_id!=0){ echo $orderdetails->coupon_code; } ?> </b>
                                </td>
                                <td >
                                   <?php if($orderdetails->coupon_id!=0){?> <span style="color: red;">- ₹<?php   echo $orderdetails->coupon_disount; } ?></span>
                                </td>
                                
                                
                            </tr>
                            
                             
                            
                            <tr>
                                <td >
                                </td>
                                <td >
                                </td>
                                <td >
                                </td>
                                <td >
                                    
                                </td>
                                <td >
                                    
                                </td>
                                <td >
                                    <b>Delivery Charges </b>
                                </td>
                                <td >
                                    <span style="color: green;">+ ₹<?php echo $orderdetails->deliveryboy_commission; ?></span>
                                </td>
                            </tr>

                            <tr>
                                <td >
                                </td>
                                <td >
                                </td>
                                <td >
                                </td>
                                <td >
                                    
                                </td>
                                <td >
                                    
                                </td>
                                 <td >
                                    <b>GST</b>
                                </td>
                                <td >
                                    <span style="color: green;">+ ₹<?php echo $orderdetails->gst;  ?></span>
                                </td> 
                                
                                
                            </tr>
                            
                            <tr>
                                <td >
                                </td>
                                <td >
                                </td>
                                <td >
                                </td>
                                <td >
                                    
                                </td>
                                <td >
                                    
                                </td>
                                <td >
                                    <b>Grand Total </b>
                                </td>
                                <td >
                                	<?php if($orderdetails->coupon_id!=0)
                                		  { 
                                		  		$subt = $orderdetails->sub_total-$orderdetails->coupon_disount;
                                		  	?>
                                		       <b>₹<?php echo ($subt+$orderdetails->deliveryboy_commission+$orderdetails->gst); ?></b>
                                		 <?php  }
                                		  else
                                		  { ?>
                                		  		 <b>₹<?php echo ($orderdetails->sub_total+$orderdetails->deliveryboy_commission+$orderdetails->gst); ?></b>
                                		 <?php  } ?>


                                </td>
                            </tr>

                        <?php }else{ ?>
                            <tr>
                                <td >
                                </td>
                                <td >
                                </td>
                                <td >
                                </td>
                                <td >
                                    
                                </td>
                                <td >
                                    
                                </td>
                                <td >
                                    <b>Bid Amount </b>
                                </td>
                                <td >
                                    
                                    <b style="color: red;">₹<?php echo $orderdetails->total_price; ?></b>

                                </td>
                            </tr>

                        <?php } ?>
                        </tbody>
                    </table>

                    <?php if($orderdetails->order_status==5 || $orderdetails->order_status==7){  ?>
                        <h3 style="text-align: center;">Commissions</h3>
                    <table class="table table-striped">
                      
                        <tr>
                            <th>Admin Commission</th>
                            <td><?php echo $orderdetails->superadmin_commission; ?></td>
                        </tr>
                        <tr>
                            <th>Franchise Commission</th>
                            <td><?php echo $orderdetails->franchise_commission; ?></td>
                        </tr>
                        <tr>
                            <th>Vendor Commission</th>
                            <td><?php echo $orderdetails->shop_commission; ?></td>
                        </tr>

                        <tr>
                            <th>Tax Commission</th>
                            <td><?php echo $orderdetails->tax_commission; ?></td>
                        </tr>
                          <tr>
                            <th>Order Total</th>
                            <td><?php echo $orderdetails->total_price; ?></td>
                        </tr>
                        
                    </table>
                <?php } ?>
                </div>
            </div>
        </div>
    </div>


</div>

