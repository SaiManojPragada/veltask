<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= $title ?></h5>
                <div class="ibox-tools">
                    <a href="<?= base_url() ?>admin/services">
                        <button class="btn btn-primary">BACK</button>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form method="post" class="form-horizontal" enctype="multipart/form-data"  action="<?= base_url() ?>admin/services/<?= $func ?><?= $data->id ?>">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Category Name *</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="cat_id" id="cat_id" onchange="get_subs(this.value)">
                                <option value="">Select Category</option> 
                                <?php foreach ($categories as $item) { ?>
                                    <option value="<?= $item->id ?>" <?= ($data->cat_id == $item->id) ? "selected" : "" ?>><?= $item->name ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Sub Category Name *</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="sub_cat_id" name="sub_cat_id" >
                                <option value="">Select Sub Category</option> 
                                <?php foreach ($sub_categories as $item) { ?>
                                    <option value="<?= $item->id ?>" <?= ($data->sub_cat_id == $item->id) ? "selected" : "" ?>><?= $item->sub_category_name ?></option>
                                <?php } ?>
                            </select>
                            <span class="col-sm-12"><small><b>Note : </b>(Leave Empty If there is no sub category for this service)</small></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Service Name *</label>
                        <div class="col-sm-10">
                            <input type="text" name="service_name" id="service_name" class="form-control" value="<?= $data->service_name ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Description *</label>
                        <div class="col-sm-10">
                            <textarea rows="10" cols="10" id="description" name="description" class="form-control ck-editor" required><?= $data->description ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Duration</label>
                        <div class="col-sm-10">
                            <input type="text" name="duration" id="duration"  class="form-control" value="<?= $data->duration ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Has Visit & Quote </label>
                        <div class="col-sm-10">
                            <small>(<b style="color: red">Note</b> : If this service is Visit and Quote the visiting charges will be Service Price automatically)</small><br>
                            <input type="radio" id="yes" name="has_visit_and_quote" value="1" onselect="alert('yes')" <?php
                            if ($data->has_visit_and_quote) {
                                echo "checked";
                            }
                            ?>>
                            <label class="control-label" for="yes">Yes</label><br>
                            <input type="radio" id="no" name="has_visit_and_quote" value="0" onselect="alert('no')" <?php
                            if (!$data || !$data->has_visit_and_quote) {
                                echo "checked";
                            }
                            ?>>
                            <label class="control-label" for="no">No</label><br>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" id="price_lable">Price *</label>
                        <div class="col-sm-10">
                            <input type="number" name="price" id="price"  class="form-control" value="<?= $data->price ?>">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">Sale Price *</label>
                        <div class="col-sm-10">
                            <input type="number" name="sale_price" id="sale_price" class="form-control" value="<?= $data->sale_price ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Visiting Charges *</label>
                        <div class="col-sm-10">
                            <input type="number" name="visiting_charges" id="visiting_charges" class="form-control" value="<?= $data->visiting_charges ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tax *</label>
                        <div class="col-sm-10">
                            <input type="number" name="tax" id="tax" class="form-control" value="<?= $data->tax ?>">
                        </div>
                    </div>
                    <div class="form-group" style="display: none">
                        <label class="col-sm-2 control-label">Time Slots *</label>
                        <div class="col-sm-10">
                            <select class="form-control chosen-select" name="time_slots[]" id="time_slots" multiple fata-placeholder="Select Time Slots">
                                <option value="">Select Time Slots</option> 
                                <?php
                                foreach ($time_slots as $item) {
                                    $ex = explode(',', $data->time_slots);
                                    ?>
                                    <option value="<?= $item->id ?>" <?= in_array($item->id, $ex) ? "selected" : "" ?>><?= date('h:i A', strtotime($item->time_slot)) ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">SEO Title</label>
                        <div class="col-sm-10">
                            <input type="text" name="seo_title" id="seo_title" class="form-control" value="<?= $data->seo_title ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">SEO Keywords</label>
                        <div class="col-sm-10">
                            <textarea rows="4" name="seo_keywords" id="seo_keywords" class="form-control"><?= $data->seo_description ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">SEO Description </label>
                        <div class="col-sm-10">
                            <textarea rows="4" name="seo_description" id="seo_description" class="form-control"><?= $data->seo_description ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Priority</label>
                        <div class="col-sm-10">
                            <input type="number" name="priority" id="priority" class="form-control" value="<?= $data->priority ?>">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">Status</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="status" name="status">
                                <option value="">Select Status</option>
                                <option value="1" <?= ($data->status) ? "selected" : "" ?>>Active</option>
                                <option value="0" <?= (!$data->status) ? "selected" : "" ?>>InActive</option>
                            </select>
                        </div>
                    </div>


                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" type="submit" id="btn_category"> <i class="fa fa-plus-circle"></i> Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    function get_subs(val) {
        $.ajax({
            url: "<?= base_url('admin/services/get_sub_categories') ?>",
            type: "post",
            data: {cat_id: val},
            success: function (resp) {
                $("#sub_cat_id").html(resp);
            }
        });
    }

    $('#btn_category').click(function () {
        $('.error').remove();
        var errr = 0;
        if ($('#cat_id').val() == '')
        {
            $('#cat_id').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Select Category </span>');
            $('#cat_id').focus();
            return false;
        } else if ($('#service_name').val() == '')
        {
            $('#service_name').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Service Name</span>');
            $('#service_name').focus();
            return false;
        } else if ($('#price').val() == '')
        {
            $('#price').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Price</span>');
            $('#price').focus();
            return false;
        } else if ($('#sale_price').val() == '')
        {
            $('#sale_price').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Sale Price</span>');
            $('#sale_price').focus();
            return false;
        } else if ($('#visiting_charges').val() == '')
        {
            $('#visiting_charges').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Visiting Charges</span>');
            $('#visiting_charges').focus();
            return false;
        } else if ($('#tax').val() == '')
        {
            $('#tax').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter tax</span>');
            $('#tax').focus();
            return false;
        } else if ($('#seo_title').val() == '')
        {
            $('#seo_title').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter SEO Title</span>');
            $('#seo_title').focus();
            return false;
        } else if ($('#seo_keywords').val() == '')
        {
            $('#seo_keywords').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter SEO Keywords</span>');
            $('#seo_keywords').focus();
            return false;
        } else if ($('#seo_description').val() == '')
        {
            $('#seo_description').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter SEO Description</span>');
            $('#seo_description').focus();
            return false;
        } else if ($('#priority').val() == '')
        {
            $('#priority').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Priority</span>');
            $('#priority').focus();
            return false;
        } else if ($('#status').val() == '') {
            $('#status').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Select Status</span>');
            $('#status').focus();
            return false;
        }
    });
    function validateEmail($email)
    {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if (!emailReg.test($email)) {
            return false;
        } else
        {
            return true;
        }
    }
    var checkVal = '0';
    $("#price").on("keyup", function () {
        if (checkVal === '1') {
            $("#visiting_charges").val(this.value);
            $("#sale_price").val(this.value);
        }
    });
    $('input:radio[name="has_visit_and_quote"]').change(
            function () {
                var visiting_charges_parent = $("#visiting_charges").parent();
                var sale_price_parent = $("#sale_price").parent();
                if (this.checked && this.value == '1') {
                    checkVal = '1';
                    $("#visiting_charges").val($("#price").val());
                    $("#sale_price").val($("#price").val());
                    visiting_charges_parent.parent().hide();
                    sale_price_parent.parent().hide();
                    $("#price_lable").html("Visitng Charges *");
                } else if (this.checked && this.value == '0') {
                    checkVal = '0';
                    $("#visiting_charges").val("");
                    $("#sale_price").val("");
                    visiting_charges_parent.parent().show();
                    sale_price_parent.parent().show();
                    $("#price_lable").html("Price *");
                }
            });
</script>