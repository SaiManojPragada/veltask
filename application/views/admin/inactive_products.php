<style>
    .product_image{
        width: 100px;
        height: 100px;
        object-fit: scale-down;
        border-radius: 10px;
        margin: 0px 5px;
        border: 1px solid #edecec;
    }
    .shop_title{
        font-size:17px !important;
        color: #f39c5a;
    }
    .mangeImagesGrid{
        border: 1px solid #ebe9e9;
    }
    .manageImagesGridImgView{
        width:100%;
        /*border: 1px solid #e5e5e5;*/
        margin-bottom: 4px;
    }
    .previewImage{
        padding: 3px;
        border: 1px solid #ccc;
        margin:4px;
        width:30%;
        float:left;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">

        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">

                    <h5 class="shop_title">Products</h5>
                    <div class="ibox-tools">
                        <a href="<?= base_url() ?>admin/dashboard">
                            <button class="btn btn-primary">BACK</button>
                        </a>
                        <!-- <a href="<?= base_url() ?>vendors/products/add_product?shop_id=<?= $_SESSION['vendors']['vendor_id'] ?>">
                            <button class="btn btn-primary">+ Add Product</button>
                        </a> -->
                        <!-- <a href="<?= base_url() ?>vendors/products/import_product?shop_id=<?= $_SESSION['vendors']['vendor_id'] ?>">
                            <button class="btn btn-primary">+ Import Excel</button>
                        </a> -->

                       <!--   <?php 
                        if($back1=='button1'){?>
                            <button onclick="goBack()" class="btn btn-primary">Back</button>
                        <?php } ?>

                    </div>

                     <script>
function goBack() {
  window.history.back();
}
</script>
 -->

                     <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                        <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                        </div>
                    <?php } ?>
                    <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                        <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                        </div>
                    <?php }
                    ?>

                </div>


                <div class="ibox-content">

                     <form method="post" class="form-horizontal" action="<?= base_url() ?>admin/inactive_products/searchProducts">
                     <div class="form-group">
                                    <div class="col-sm-6">
                                     <input type="text" name="kayword" class="form-control" placeholder="Search Product Name " value="<?php echo $keyword; ?>">
                                    </div>
                                    <div class="col-sm-6">
                                     <button class="btn btn-primary" id="add_city" type="submit">Search</button>
                                    </div>
                                </div>

                        
                    </form>


                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th>S.NO</th>
                                    <th>Product ID</th>
                                    <th>Vendor Details</th>

                                    <th>Product Name</th>
                                    <th>Category</th>
                                    <th>Sub Category</th>
                                    <!-- <th>Images</th> -->
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $kk = 1;
                                foreach ($products as $pr) {

                                    $cat = $this->db->query("select * from categories where id='".$pr->cat_id."'"); 
                                    $category = $cat->row();
                                    $subcat = $this->db->query("select * from sub_categories where id='".$pr->sub_cat_id."'"); 
                                    $subcategory = $subcat->row();

                                    $vendor = $this->db->query("select * from vendor_shop where id='".$pr->shop_id."'"); 
                                    $vendor_shops = $vendor->row();

                                    ?>
                                    <tr class="gradeX">
                                        <td><?= $kk ?></td>
                                        <td><?= $pr->id ?></td>
                                        <td><p><b>Shop Name : </b><?php echo $vendor_shops->shop_name; ?></p>
                                            <p><b>Owner Name :</b> <?php echo $vendor_shops->shop_name; ?></p>
                                            <p><b>Email :</b> <?php echo $vendor_shops->email; ?></p>
                                            <p><b>Mobile :</b> <?php echo $vendor_shops->mobile; ?></p>
                                        </td>
                                        <td>

                                            <?php $pro_im = $this->db->query("select * from product_images where product_id='".$pr->id."'"); 
                                                    $pro_imgs = $pro_im->row();
                                                    if($pro_im->num_rows()>0)
                                                    {
                                            ?>
                                            <img class="product_image" align="left" src="<?php echo base_url();?>uploads/products/<?php echo $pro_imgs->image; ?>" title="Product image">
                                        <?php }else{  ?>
                                            <img class="product_image" align="left" src="<?= DEFAULT_IMAGE_PATH ?>" title="Product image">
                                        <?php } ?>

                                            <?= $pr->name ?>
                                           <p style="color: red;"> <?php if($pr->top_deal=='yes'){ echo "TOP DEAL PRODUCT"; } ?></p>

                                        </td>
                                        <td><?= $category->category_name ?></td>
                                        <td><?= $subcategory->sub_category_name ?></td>
                                        <td>
                                             <?php
                                             $user_type = $_SESSION['admin_login']['user_type']; 
                                        if($user_type=='subadmin'){ 

                                             $admin_id = $_SESSION['admin_login']['id']; 
                                $adm_qry = $this->db->query("select * from sub_admin where id='".$admin_id."'");
                                $adm_row=$adm_qry->row();

                                $userpermissions  = $adm_row->permissions; 
                                $permissions = explode(",", $userpermissions);
                                                    if (in_array("status_inactive_products", $permissions)){ ?>

                                                        <?php
                                        if ($pr->status == 1) {
                                            ?>
                                            <a href="<?= base_url() ?>admin/products/changeStatus/<?= $pr->id ?>/0"><button title="Active" class="btn btn-xs btn-green">Active</button></a>
                                            <?php
                                        } else {
                                            ?>
                                            <a href="<?= base_url() ?>admin/products/changeStatus/<?= $pr->id ?>/1"><button title="Inactive" class="btn btn-xs btn-danger">
                                                Inactive
                                            </button></a>
                                            <?php
                                        } 
                                        ?>
                                             
                                             <?php } if (in_array("edit_inactive_products", $permissions)){ ?>
                                                <a href="<?= base_url() ?>admin/inactive_products/edit/<?php echo $pr->id; ?>">
                                                <button class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o"></i>Edit</button>
                                            </a>
                                            <?php } if (in_array("delete_inactive_products", $permissions)){  ?>
                                                        <button class="btn btn-xs btn-danger delete_product" data-id="<?= $pr->id; ?>"><i class="fa fa-trash-o"></i> Delete</button>

                                             <?php } }else{ ?>
                                                
                                    <?php
                                        if ($pr->status == 1) {
                                            ?>
                                            <a href="<?= base_url() ?>admin/products/changeStatus/<?= $pr->id ?>/0"><button title="Active" class="btn btn-xs btn-green">Active</button></a>
                                            <?php
                                        } else {
                                            ?>
                                            <a href="<?= base_url() ?>admin/products/changeStatus/<?= $pr->id ?>/1"><button title="Inactive" class="btn btn-xs btn-danger">
                                                Inactive
                                            </button></a>
                                            <?php
                                        } 
                                ?>
                                
                                        <a href="<?= base_url() ?>admin/inactive_products/edit/<?php echo $pr->id; ?>">
                                                <button class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o"></i>Edit</button>
                                            </a>

                                               <button class="btn btn-xs btn-danger delete_product" data-id="<?= $pr->id; ?>"><i class="fa fa-trash-o"></i> Delete</button>

                                             <?php } ?>







                                    </td>
                                        
                                             
                                           <!--  <button class="btn btn-xs btn-danger delete_product" data-id="<?= $pr->id; ?>"><i class="fa fa-trash-o"></i> Delete</button> -->
                                        
                                        <!-- <td>
                                            <?php $c=$this->db->query("select * from product_images where product_id='".$pr->id."'"); 
                                                    $num_rows = $c->num_rows(); ?>
                                            <p>Images - <?php echo $num_rows; ?></p>

                                        </td> -->
                                        <!-- <td>
                                            <?php if($pr->variant_product=='no'){}else{?>
                                            <a href="<?= base_url() ?>vendors/products/addvariant/<?php echo $pr->id; ?>">
                                                <button class="btn btn-xs btn-info">+ Add Variant</button>
                                            </a>

                                            
                                        <?php } ?>
                                        <a href="<?= base_url() ?>vendors/products/linkvariant/<?php echo $pr->id; ?>">
                                                <button class="btn btn-xs btn-info"><i class="fa fa-eye" aria-hidden="true"></i>  Products </button>
                                            </a>
                                           
                                        </td> -->

                                            
                                                        
                                    </tr>
                                    <?php
                                    $kk++;
                                }
                                ?>
                            </tbody>
                            
                        </table>

                        <style>
                            .pagination_new {
                                text-align: center;
                            }
                            .pagination_new a {
                                padding: 10px 15px;
                                background-color: #ccc;
                                color: #333;
                                    margin: 0px 1px;
                            }
                            .pagination_new strong {
                                padding: 10px 15px;
                                background-color: #1ab394;
                                color: #fff;
                                margin: 0px 1px;
                            }
                        </style>
                        <div class="pagination_new">
                            <p><?php echo $links; ?></p>
                        </div>
                        
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<!-- Update Prices Modal-->
<style>
    #UpdatePricesModal .form-control{
        margin-bottom: 10px;
    }
</style>


<script type="text/javascript">
  function getProducts(value)
  {
    if(value==1)
    {
      document.getElementById("displaystock").style.display = "block";
    }
    else
    {
      document.getElementById("displaystock").style.display = "none";
    }
  }
</script>
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
      var max_fields      = 10; //maximum input boxes allowed
      var wrapper       = $(".input_fields_wrap"); //Fields wrapper
      var add_button      = $(".add_field_button"); //Add button ID
      var x = 1; //initlal text box count
      $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
          x++; //text box increment
          $(wrapper).append('<div><div class="input_fields_wrap"><div class="col-sm-12"><label class="col-sm-2 control-label">Attributes</label><div class="col-sm-4"><label class="col-sm-2 control-label">Name: *</label><input type="text" name="title[]" class="form-control" value=""></div><div class="col-sm-4"><label class="col-sm-2 control-label">Value: *</label><input type="text" name="values[]" class="form-control" value=""></div><a href="#" class="remove_field btn btn-danger">Remove</a></div></div></div>'); //add input box
        }
      });
      
      $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
      })
    });
</script>

<script type="text/javascript">

    function deleteImage(img_id)
    {

                if(img_id != '')
                {
                   $.ajax({
                    url:"<?php echo base_url(); ?>vendors/products/deleteImage",
                    method:"POST",
                    data:{img_id:img_id},
                    success:function(data)
                    {

                        //alert(JSON.stringify(data));
                        alert("Image delted");
                        //var location = '<?= base_url() ?>vendors/products';
                        //console.log(location);
                        //window.location = location;
                        $('#UploadImagesModal').modal('show');
                     //$('#image_id').show("Image delted");
                    }
                   });
                }
    }
    $(document).ready(function () {

        $('#backNavigation').click(function (e) {
            console.log("log something");
//            window.history.back();
            return false;
        });


        $('.delete_product').on('click', function () {
            var product_id = $(this).attr('data-id');
            console.log(product_id);
            var confirm = window.confirm('Are you sure, you want to delete related Variants and Images ?');
            if (confirm) {
                var location = '<?= base_url() ?>admin/inactive_products/delete_product?product_id=' + product_id;
                console.log(location);
                window.location = location;
            } else {
                console.log('not confirmed');
            }
        })

        $('.StockLogsModal').on('click', function () {
            var product_id = $(this).attr('data-id');
            console.log(product_id);
            $('#StockLogsModal').modal('show');

            $.post("<?= base_url() ?>api/admin_ajax/vendors/get_stock",
                    {
                        product_id: product_id
                    },
                    function (response, status) {
                        console.log("Data: " + response + "\nStatus: " + status);
                        console.log(JSON.stringify(response));

                        if (response['status'] == 'valid') {
//                            $('#manageStockContainer').show();
                            console.log(response['data']);
                            $('#stock_logs').html();
                            var tbody = '';
                            var i = 1;
                            response['data'].forEach(function (obj) {
                                console.log(obj);
                                tbody += ` <tr>
                                        <td>` + i + `</td>
                                        <td>` + obj.stock + `</td>
                                        <td>` + obj.type + `</td>
                                        <td>` + obj.note + `</td>
                                        <td>` + obj.balance + `</td>
                                    </tr>`;
                                i++;

                            });
                            $('#stock_logs').html(tbody);
                        } else {
//                            $('#manageStockContainer').hide();
                        }
                    });
//            $('#product_stock').val(stock);
//            $('#updatestockModal').modal('show')
        });

        $('.upload_images').on('click', function () {
            $('#UploadImagesModal').modal('show');
        });

        var imagesPreview = function (input, placeToInsertImagePreview) {

            if (input.files) {
                var filesAmount = input.files.length;
                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();
                    reader.onload = function (event) {
                        console.log(event);
                        var div = document.createElement('div');
                        div.setAttribute('class', 'previewImage');

                        var img = document.createElement('img');
                        img.src = event.target.result;
                        img.setAttribute('width', '50%');
                        div.appendChild(img);


                        var fileDisplayEl = document.createElement('p');
                        fileDisplayEl.innerHTML = event.target.fileName;
                        div.appendChild(fileDisplayEl);

                        placeToInsertImagePreview.appendChild(div);
                    };
                    console.log(filesAmount);
                    reader.fileName = input.files[i].name;
                    reader.readAsDataURL(input.files[i]);
                }
            }
        };

        $('#browseimages').on('change', function () {
            var filelistdisplay = document.getElementById('file-list-display');
            imagesPreview(this, filelistdisplay);
        });


        $('.ShowPricesModal').on('click', function () {
//            alert('hello');
            $('#UpdatePricesModal').modal('show');
        })






    });
</script>


