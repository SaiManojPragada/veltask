<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= $title ?></h5>
                <div class="ibox-tools">
                    <a href="<?= base_url() ?>admin/franchise_services">
                        <button class="btn btn-primary">BACK</button>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form method="post" class="form-horizontal" enctype="multipart/form-data"  action="<?= base_url() ?>admin/franchise_services/<?= $func ?><?= $data->id ?>">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Select Category *</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="category_id" id="cat_id" onchange="getSubCategories(this.value)">
                                <option value="">Select Category</option> 
                                <?php foreach ($categories as $item) { ?>
                                    <option value="<?= $item->id ?>" <?= ($data->category_id == $item->id) ? "selected" : "" ?>><?= $item->name ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Select Sub Category *</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="sub_cat_id" name="sub_category_id" onchange="getServices()">
                                <option value="">Select Sub Category</option> 
                                <?php
                                if ($data) {
                                    foreach ($sub_categories as $cats) {
                                        ?>
                                        <option value="<?= $cats->id ?>" <?= ($cats->id == $data->sub_category_id) ? "selected" : "" ?>><?= $cats->sub_category_name ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                            <span class="col-sm-12"><small><b>Note : </b>(Leave Empty If there is no sub category for this service)</small></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Select Service *</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="service_id" name="service_id" >
                                <option value="">Select Service</option> 
                                <?php
                                if ($data) {
                                    foreach ($services as $cats) {
                                        ?>
                                        <option value="<?= $cats->id ?>" <?= ($cats->id == $data->service_id) ? "selected" : "" ?>><?= $cats->service_name ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">Price *</label>
                        <div class="col-sm-10">
                            <input type="number" name="price" id="price"  class="form-control" value="<?= $data->price ?>">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">Sale Price *</label>
                        <div class="col-sm-10">
                            <input type="number" name="sale_price" id="sale_price" class="form-control" value="<?= $data->sale_price ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Visiting Charges *</label>
                        <div class="col-sm-10">
                            <input type="number" name="visiting_charges" id="visiting_charges" class="form-control" value="<?= $data->visiting_charges ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tax *</label>
                        <div class="col-sm-10">
                            <input type="number" name="tax" id="tax" class="form-control" value="<?= $data->tax ?>">
                        </div>
                    </div>


                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" type="submit" id="btn_category"> <i class="fa fa-plus-circle"></i> Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    function getSubCategories(id) {
        $.ajax({
            url: "<?= base_url('admin/franchise_services/get_sub_categories') ?>",
            type: "post",
            data: {cat_id: id},
            success: function (resp) {
                $("#sub_cat_id").html(resp);
                getServices();
            }
        });
    }
    function getServices() {
        var cat_id = $("#cat_id").val();
        var sub_cat_id = $("#sub_cat_id").val();
        $.ajax({
            url: "<?= base_url('admin/franchise_services/get_services') ?>",
            type: "post",
            data: {cat_id: cat_id, sub_cat_id: sub_cat_id},
            success: function (resp) {
                $("#service_id").html(resp);
            }
        });
    }

</script>


<script type="text/javascript">

    $('#btn_category').click(function () {
        $('.error').remove();
        var errr = 0;
        if ($('#cat_id').val() == '')
        {
            $('#cat_id').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Select Category </span>');
            $('#cat_id').focus();
            return false;
        } else if ($('#service_id').val() == '')
        {
            $('#service_id').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Select Service</span>');
            $('#service_id').focus();
            return false;
        } else if ($('#price').val() == '')
        {
            $('#price').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Price</span>');
            $('#price').focus();
            return false;
        } else if ($('#sale_price').val() == '')
        {
            $('#sale_price').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Sale Price</span>');
            $('#sale_price').focus();
            return false;
        } else if ($('#visiting_charges').val() == '')
        {
            $('#visiting_charges').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Visiting Charges</span>');
            $('#visiting_charges').focus();
            return false;
        } else if ($('#tax').val() == '')
        {
            $('#tax').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter tax</span>');
            $('#tax').focus();
            return false;
        }
    });
    function validateEmail($email)
    {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if (!emailReg.test($email)) {
            return false;
        } else
        {
            return true;
        }
    }
</script>