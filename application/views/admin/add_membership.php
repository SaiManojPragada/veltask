<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= $title ?></h5>
                <div class="ibox-tools">
                    <a href="<?= base_url() ?>admin/memberships">
                        <button class="btn btn-primary">BACK</button>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form method="post" class="form-horizontal" enctype="multipart/form-data"  action="<?= base_url() ?>admin/memberships/<?= $func ?><?= $data->id ?>">

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Title *</label>
                        <div class="col-sm-10">
                            <input type="text" name="title" id="title" class="form-control" value="<?= $data->title ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Duration *</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="duration_id" id="duration" >
                                <option value="">Select Duration</option>
                                <?php foreach ($terms as $tt) { ?>
                                    <option value="<?= $tt->id ?>" <?= ($data->duration_id === $tt->id) ? "selected" : "" ?>><?= $tt->duration_in_words ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Description *</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="description" id="description" required><?= $data->description ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Price *</label>
                        <div class="col-sm-10">
                            <input type="number" min="0" name="price" id="price" class="form-control" value="<?= $data->price ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Sale Price *</label>
                        <div class="col-sm-10">
                            <input type="number" min="0" name="sale_price" id="sale_price" class="form-control" value="<?= $data->sale_price ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Discount Percentage *</label>
                        <div class="col-sm-10">
                            <input type="number" min="1" name="discount_percentage" id="discount_percentage" class="form-control" value="<?= $data->discount_percentage ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Max Discount Amount *</label>
                        <div class="col-sm-10">
                            <input type="number" min="0" name="max_discount" id="max_discount" class="form-control" value="<?= $data->max_discount ?>">
                            <small><b style="color: tomato">Note : </b>Enter '0'(Zero) for no Max Amount for discount</small>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Status *</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="status" name="status">
                                <option value="1" <?= ($data && $data->status) ? "selected" : "" ?>>Active</option>
                                <option value="0" <?= ($data && !$data->status) ? "selected" : "" ?>>InActive</option>
                            </select>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" type="submit" id="btn_category"> <i class="fa fa-plus-circle"></i> Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('description');
</script>
<script type="text/javascript">

    $('#btn_category').click(function () {
        $('.error').remove();
        var errr = 0;
        if ($('#title').val() == '')
        {
            $('#title').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Title </span>');
            $('#title').focus();
            return false;
        } else if ($('#duration').val() == '')
        {
            $('#duration').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Select Duration</span>');
            $('#duration').focus();
            return false;
        } else if ($('#price').val() == '')
        {
            $('#price').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Price</span>');
            $('#price').focus();
            return false;
        } else if ($('#sale_price').val() == '')
        {
            $('#sale_price').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Sale Price</span>');
            $('#sale_price').focus();
            return false;
        } else if ($('#status').val() == '')
        {
            $('#status').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Select Status</span>');
            $('#status').focus();
            return false;
        }
    });
</script>