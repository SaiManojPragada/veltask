<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">

        <div class="col-lg-12">

            <div class="ibox float-e-margins">
                <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                    <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                    </div>
                <?php } ?>
                <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                    <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                    </div>
                <?php }
                ?>
                <div class="ibox-title">

                    <h5><?= $title ?></h5>

                    <div class="ibox-tools">
                        <a href="<?= base_url() ?>admin/users">
                            <button class="btn btn-primary">BACK</button>
                        </a> 
                    </div> 


                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-4">
                                <p><b>Company Name : </b></p>
                            </div>
                            <div class="col-md-8">
                                <p><?= $business_profile->company_name ?></p>
                            </div>
                            <div class="col-md-4">
                                <p><b>Company Address : </b></p>
                            </div>
                            <div class="col-md-8">
                                <p><?= $business_profile->company_address ?></p>
                            </div>
                            <div class="col-md-4">
                                <p><b>Proof Type Provided : </b></p>
                            </div>
                            <div class="col-md-8">
                                <p><?= $business_profile->proof_type ?></p>
                            </div>
                            <div class="col-md-4">
                                <p><b>Document : </b></p>
                            </div>
                            <div class="col-md-8">
                                <p><a href="<?= base_url('uploads/company_proofs/') . $business_profile->document ?>" target="_blank">View / Download</a></p>
                            </div>
                            <div class="col-md-4">
                                <p><b>Last Updated : </b></p>
                            </div>
                            <div class="col-md-8">
                                <p><?= date('d M Y, h:i A', $business_profile->updated_at) ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div> 
    </div> 
</div> 
</div>