<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= $title ?></h5>
                <div class="ibox-tools">
                    <a href="<?= base_url() ?>admin/blogs">
                        <button class="btn btn-primary">BACK</button>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form method="post" class="form-horizontal" enctype="multipart/form-data" id="testimonials-form"  action="<?= base_url() ?>admin/testimonials/<?= $func ?><?= $data->id ?>">

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Name *</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" id="name" class="form-control" value="<?= $data->name ?>" required>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">Occupation *</label>
                        <div class="col-sm-10">
                            <input type="text" name="position" id="position" class="form-control" value="<?= $data->position ?>" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Comment *</label>
                        <div class="col-sm-10">
                            <textarea rows="10" cols="10" id="comment" name="comment" class="form-control" required><?= $data->comment ?></textarea>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">Priority *</label>
                        <div class="col-sm-10">
                            <input type="number" name="priority" id="priority" class="form-control" value="<?= $data->priority ?>" required>
                        </div>
                    </div>



                    <div class="form-group">
                        <label class="col-sm-2 control-label">Status *</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="status" name="status" required>
                                <option value="">Select Status</option>
                                <option value="1" <?= ($data && $data->status) ? "selected" : "" ?>>Active</option>
                                <option value="0" <?= ($data && !$data->status) ? "selected" : "" ?>>InActive</option>
                            </select>
                        </div>
                    </div>


                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" type="submit" id="btn_category"> <i class="fa fa-plus-circle"></i> Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script src="<?= base_url('web_assets/') ?>/js/plugins/parsleyjs/dist/parsley.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#testimonials-form').parsley();
    });
//    function validateEmail($email)
//    {
//        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
//        if (!emailReg.test($email)) {
//            return false;
//        } else
//        {
//            return true;
//        }
//    }
</script>