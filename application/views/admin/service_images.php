<style>
    .cat_image{
        width: 100px;
        height: 100px;
        object-fit: scale-down;
        border-radius: 10px;
        margin: 0px 5px;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $title ?></h5>
                    <div class="ibox-tools">
                        <a href="javascript:void(0);" onclick="history.back();">
                            <button class="btn btn-primary">BACK</button>
                        </a>
                    </div>
                    <h3><?= $service->service_name ?> Images</h3>
                    <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                        <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                        </div>
                    <?php } ?>
                    <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                        <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                        </div>
                    <?php }
                    ?>
                </div>
                <div class="ibox-content">
                    <div class="row" style="padding: 10px 30px">
                        <form class="col-md-12 row" enctype="multipart/form-data" method="post" action="">
                            <div class="form-group col-md-2 ">
                                <label>Select Images <br><small><span style="color: red"> Note : </span>Recommended Image Size (875 x 483)</small></label>
                            </div>
                            <div class="form-group col-md-7 ">
                                <input type="file" name="images[]" id="images" class="form-control" accept="image/*" multiple="true">
                            </div>
                            <div class="form-group col-md-2 ">
                                <button class="btn btn-success btn-sm pull-right" id="addImgs"  type="submit" name="submit" value="images">+ Add Images</button>
                            </div>
                        </form>
                        <?php foreach ($images as $item) { ?>
                            <div class="col-md-2">
                                <img src="<?= base_url('uploads/services/' . $item->image) ?>" class="img-vi" style="width: 100%" alt="alt"/>
                                <a href="javascript:void(0);" class="del-btn" onclick="deleteImage('<?= $item->id ?>', '<?= $item->image ?>')"><i class="fa fa-trash-o"></i></a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <script>
                $("#addImgs").on('click', function () {
                    $('.error').remove();
                    var control = document.getElementById('images');
                    var filelength = control.files.length;
                    for (var i = 0; i < filelength; i++) {
                        var file = control.files[i];
                        var FileName = file.name;
                        var FileExt = FileName.substr(FileName.lastIndexOf('.') + 1).toUpperCase();
                        if (FileExt !== "JPG" && FileExt !== "PNG") {
                            var error = "File type : " + FileExt + "\n";
                            error += "Please make sure your file is in JPG or PNG  format .\n";
                            alert(error);
                            return false;
                        }
                    }

                });
                function deleteImage(id, file) {
                    if (confirm('Are you sure you want to Delete this Service Image ?')) {
                        $.ajax({
                            url: "<?= base_url('admin/services/image_delete') ?>",
                            type: "POST",
                            data: {id: id, file: file},
                            success: function (resp) {
                                location.reload();
                            }
                        });
                    } else {
                        alert('cancelled');
                    }
                }
            </script>
            <style>
                .img-vi{
                    max-height: 200px;
                    min-height: 200px;
                    object-fit: fill;
                }
                .del-btn{
                    color: red;
                    width: 50px;
                    font-size: 21px;
                    padding: 4px;
                    text-align: center;
                    background-color: white;
                    border-radius: 100px;
                    position: absolute;
                    bottom: 10px;
                    right: 20px;
                }
            </style>
        </div>
    </div>
</div>