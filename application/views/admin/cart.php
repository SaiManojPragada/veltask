<?php  $this->load->view("web/includes/header_styles"); ?>

<style>
                     .couponlist-box{
                        margin-top: 20px;
                        padding-top: 20px;
                     }
                     .couponlist-box .row{
                        border: 1px solid #eee;
                        padding-top: 10px;
                        padding-bottom: 10px;
                        margin-bottom: 10px;
                    }
                    .couponlist-box h5{
                        font-weight: 700;
                        font-size: 20px;
                        color: #cf1673;
                    }
                    .couponlist-box p{
                        line-height: 18px;
                    }
                    .couponlist-box h6{
                        background-color: #ffd9ff;
                        border: 2px dashed #cf1673;
                        font-size: 14px;
                        text-align: center;
                        padding: 7px 5px 7px 5px;
                        font-weight: 700;
                    }
                     .couponlist-box p small a:hover{
                        color: #ccc;
                    }
                    a.btn-viewcoup{
                        background-color: #cf1673;
                        padding: 4px 10px;
                        color: #fff;
                        margin: 0px auto;
                        float: right;
                    }
                     a.btn-viewcoup:hover{
                        background-color: #333;
                        color: #fff;
                    }
                 </style>
<!--breadcrumbs area start-->
<div id="scroll_id"></div>
<div class="breadcrumbs_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <h3>Cart</h3>
                    <ul>
                        <li><a href="<?php echo base_url(); ?>web/store/<?php echo $seo_url; ?>/shop"><?php echo $shop_name; ?>, <?php echo $city; ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--breadcrumbs area end-->
  <!--shopping cart area start -->


    <div class="shopping_cart_area">
      <div id="display_msg" style="text-align: center;font-size: 26px;"></div>
        <div class="container" id="loadCartdiv">  
                <div class="row justify-content-center">
                    <div class="col-lg-10 col-md-12 col-12">
                        <div class="table_desc">
                            <div class="cart_page table-responsive">
                              <table>
                                <thead>
                                    <tr>
                                        <!-- <th class="product_remove">Delete</th> -->
                                        <th class="product_thumb">Image</th>
                                        <th class="product_name">Product</th>
                                        <th class="product-price">Price</th>
                                        <th class="product_quantity">Quantity</th>
                                        <th class="product_total">Total</th>
                                        <th class="product_total">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                <?php 

                   $session_id = $_SESSION['session_data']['session_id'];
                   $user_id= $_SESSION['userdata']['user_id']; 
                    $qry = $this->db->query("select * from cart where session_id='".$session_id."' and user_id='".$user_id."'");
                    $del_b = $qry->row();
                    $shop = $this->db->query("select * from vendor_shop where id='".$del_b->vendor_id."'");
                    $shopdat = $shop->row();
                    $min_order_amount = $shopdat->min_order_amount;

                    $result = $qry->result();
                    
                        $unitprice=0;
                        $gst=0;
                        foreach ($result as $value) 
                        {
                                $pro = $this->db->query("select * from  product_images where variant_id='".$value->variant_id."'");
                                $product = $pro->row();

                                if($product->image!='')
                                {
                                    $img = base_url()."uploads/products/".$product->image;
                                }
                                else
                                {
                                    $img = base_url()."uploads/noproduct.png";
                                }
                                $var1 = $this->db->query("select * from link_variant where id='".$value->variant_id."'");
                                $link = $var1->row();

                                 $pro1 = $this->db->query("select * from  products where id='".$link->product_id."'");
                                 $product1 = $pro1->row();

                                 $adm_qry = $this->db->query("select * from  admin_comissions where cat_id='".$product1->cat_id."' and shop_id='".$value->vendor_id."'");

                                 if($adm_qry->num_rows()>0)
                                 {
                                    $adm_comm = $adm_qry->row();
                                    $p_gst = $adm_comm->gst;
                                 }
                                 else
                                 {
                                    $p_gst = '0';
                                 }

                                 $class_percentage = ($value->unit_price/100)*$p_gst;

                                    $variants1 = $var1->result();
                                    $att1=[];
                                    foreach ($variants1 as $value1) 
                                    {

                                        $jsondata = $value1->jsondata;
                                        $values_ar=[];
                                        $json =json_decode($jsondata);
                                        foreach ($json as $value123) 
                                        {
                                            $type = $this->db->query("select * from attributes_title where id='".$value123->attribute_type."'");
                                            $types = $type->row();
                                        
                                            $val = $this->db->query("select * from attributes_values where id='".$value123->attribute_value."'");
                                            $value1 = $val->row();
                                            $values_ar[]=array('id'=>$value1->id,'title'=>$types->title,'value'=>$value1->value);
                                        }
                                    }

                                    $unitprice = $value->unit_price+$unitprice;
                                    $gst = $class_percentage+$gst;
                                ?>
                                <tr>
                                    <td class="product_thumb"><a href="<?php echo base_url(); ?>web/product_view/<?php echo $product1->seo_url; ?>" ><img src="<?php echo $img; ?>" alt=""></a></td>
                                    <td class="product_name"><a href="<?php echo base_url(); ?>web/product_view/<?php echo $product1->seo_url; ?>" ><?php echo $product1->name; ?></a><br>

                                      <p class="mb-0"><a href="<?php echo base_url(); ?>web/store/<?php echo $shopdat->seo_url; ?>/shop"><b>Shop Name :</b> <?php echo $shopdat->shop_name; ?></a></p>

                                      <p><b>Location:</b><?php echo $shopdat->city; ?></p>
                                    </td>
                                    <td class="product-price"><i class="fal fa-rupee-sign"></i> <?php echo $value->price; ?></td>
                                <form class="form-horizontal" enctype="multipart/form-data"  >
                                    <td class="product_quantity"><label></label> 
                                       <a  onclick="decreaseQty(<?php echo $value->id; ?>)">➖</a>
                                      <input min="1" max="100" name="quantity" id="quantity<?php echo $value->id; ?>" value="<?php echo $value->quantity; ?>" type="text" readonly>
                                      <a onclick="increaseQty(<?php echo $value->id; ?>)">➕</a>
                                      <div id="quantityshow<?php echo $value->id; ?>"></div>
                                    </td>
                                    <td class="product_total"><i class="fal fa-rupee-sign"></i> <?php echo $value->unit_price; ?></td>
                                    <td>
                                      <a onclick="deletecartitems(<?php echo $value->id; ?>)" class="remove-item"><i class="fal fa-trash-alt"></i></a>
                                    </td>
                                </form>
                                </tr>                                
                                <?php } 
                                    $grand_t =  $min_order_amount+$unitprice+$gst;
                                ?>
                                <tr>
                                    <td colspan="6">
                                        <a class="btn pink-btn float-right" href="<?php echo base_url(); ?>web/store/<?php echo $seo_url; ?>/shop">Continue Shopping</a>
                                    </td>
                                </tr>

                            </tbody>
                        </table>   
                            </div>  
                                
                        </div>
                     </div>
                 </div>
                <div class="row justify-content-center pb-5">
                        <div class="col-lg-5 col-md-6">
                            <div class="coupon_code left">
                                <h3>Apply Coupon</h3>
                                <div class="coupon_inner">   
                                    <p>Enter your coupon code if you have one.</p>   
                                    <div id="coupon_msg"></div>  
                                    
                                   <form class="form-horizontal" enctype="multipart/form-data"  >                       
                                        <input placeholder="Coupon code" id="couponcode" type="text">
                                        <button type="button" onclick="validatecouponcode()">Apply coupon</button>
                                    </form> 
                                </div>
                                <hr>
                                <?php if(count($coupons)>0){ ?>
                                <div class="row justify-content-center" id="show_button">
                                    <div class="col-md-4">
                                        <a  class="btn-viewcoup mb-4" onclick="showhide()">View Coupons</a> 
                                    </div>
                                </div>
                                 <div class="row justify-content-center" id="close_button" style="display: none;">
                                    <div class="col-md-4">
                                        <a  class="btn-viewcoup mb-4" onclick="closecoupon()">Close Coupons</a> 
                                    </div>
                                </div>
                            <?php } ?>
                            </div>
                            <div class="couponlist-box collapse multi-collapse" id="viewcoup">
                               

                                <?php foreach($coupons as $coup){ ?>
                                <div class="row">
                                <div class="col-8">
                                    <h5><?php echo $coup['percentage']; ?>% OFF</h5>
                                    <p><strong><?php echo $coup['description']; ?></strong></p>
                                </div>
                                <div class="col-4 alig-self-center">
                                    <h6><input type="text" style="width: 100px; border: none; background: none;" value="<?php echo $coup['coupon_code']; ?>" id="myInput<?php echo $coup['id']; ?>"></h6>
                                    <p class="text-center"><small><a onclick="applyCouponcode('<?php echo $coup['coupon_code']; ?>')">Apply Coupon Code</a></small></p>
                                </div>
                                </div>
                              <?php } ?>


                              
                            </div>

                        </div>
                        <div class="col-lg-5 col-md-6">
                            <div class="coupon_code right">
                                <h3>Cart Totals</h3>
                                <div class="coupon_inner">
                                   <div class="cart_subtotal">
                                       <p>Subtotal</p>
                                       <p class="cart_amount"><i class="fal fa-rupee-sign"></i> <?php echo $unitprice; ?></p>
                                       <input type="hidden" id="sub_total" value="<?php echo $unitprice; ?>" >
                                   </div>
                                   <div class="cart_subtotal ">
                                       <p>Shipping Charges</p>
                                       <p class="cart_amount"><i class="fal fa-rupee-sign"></i>  <?php echo $min_order_amount; ?></p>
                                       <input type="hidden" id="min_order_amount" value="<?php echo $min_order_amount; ?>" >
                                   </div>
                                   <div class="cart_subtotal">
                                       <p>Discount</p>
                                       <p class="cart_amount" id="discount">0</p>
                                   </div>
                                   <div class="cart_subtotal" id="total_default">
                                       <p>Total</p>
                                       <p class="cart_amount"><i class="fal fa-rupee-sign"></i> <input type="hidden" id="cart_total" value="<?php echo $grand_t; ?>" > <?php echo $grand_t; ?></p>
                                   </div>
                                   <div class="cart_subtotal" id="total_default_show" style="display: none;">
                                       <p>Total</p>
                                       <p class="cart_amount" id="total_p"><?php echo $grand_t; ?></p>
                                   </div>
                                   <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo base_url(); ?>web/goaddress_page">
                                     <input type="hidden" name="coupon_id" id="coupon_id" value="0">
                                     <input type="hidden" name="applied_coupon_code" id="applied_coupon_code" value="0">
                                     <input type="hidden" name="coupon_discount" id="coupon_discount" value="0">
                                   
                                       <div class="checkout_btn">
                                        <button type="submit">Proceed to Checkout</button>
                                       </div>
                                   </form>

                                   <!-- <p class="bid-text"><a data-toggle="modal" onclick="doBidProducts()">BID ABOVE PRODUCTS </a> <a href="#bidModal" data-toggle="modal" class="text-dark"><i class="fal fa-comment-exclamation"></i></a></p> -->
                                   <p class="bid-text row" style="font-weight: 600;color: #cf1673;">
                                    <span class="col-11">
                                        <a data-toggle="modal" onclick="doBidProducts()">BID ABOVE PRODUCTS <a href="#bidModal" data-toggle="modal" class="text-dark"><i class="fal fa-comment-exclamation" style="font-size: 20px; padding-top: 3px;"></i></a></a>
                                    </span>
                                    <span class="col-1">
                                        
                                    </span>
                                    </p>

                                </div>
                            </div>
                        </div>
                    </div>

          
        </div>     
    </div>

     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


    <script type="text/javascript">
function showhide()
{
        $("#viewcoup").show();
        $("#close_button").show();
        $("#show_button").hide();
}

function closecoupon()
{
       $("#viewcoup").hide();
        $("#close_button").hide();
        $("#show_button").show();
}
       


function increaseQty(id)
  {
    


    var quantity = $("#quantity"+id).val();
        var qty = 1;
        var final = parseInt(qty)+parseInt(quantity);

        $.ajax({
            url:"<?php echo base_url(); ?>web/updateCart",
            method:"POST",
            data:{cartid:id,quantity:final},
            success:function(data)
            {
                  var str = data;
              var res = str.split("@");
              if(res[1]=='error')
              {
                      $('#quantityshow'+id).html('<span class="error" style="color:red;font-size: 16px;margin-left: 18px; width:100%">'+res[2]+'</span>');
                      $('#quantityshow').focus();
                      return false;
              }
              else
              {
                  $("#loadCartdiv").html(data);
              }

              

              //alert(JSON.stringify(data));
                 /*$('html, body').animate({
                    scrollTop: $('#scroll_id').offset().top - 100 //#DIV_ID is an example. Use the id of your destination on the page
                }, 'slow');
                  if(res[1]=='success')
                  {
                    
                    $("#quantity"+id).val(final);

                     $('#display_msg').html('<span class="error" style="color:red;font-size: 16px;margin-left: 18px; width:100%">Quantity updated to cart</span>');
                      $('#display_msg').focus();
                      location.reload();
                      return false;
                  }
                  else 
                  {
                      $('#display_msg').html('<span class="error" style="color:red;font-size: 16px;margin-left: 18px; width:100%">'+res[2]+'</span>');
                      $('#display_msg').focus();
                      return false;
                  }*/
            }
        });

  }

  function decreaseQty(id)
  {

    var quantity = $("#quantity"+id).val();
    if(quantity==1)
    {
      return false;
    }
    else
    {
      var qty = 1;
    var final = parseInt(quantity)-parseInt(qty);
    
        $.ajax({
            url:"<?php echo base_url(); ?>web/removeCart",
            method:"POST",
            data:{cartid:id,quantity:final},
            success:function(data)
            {
                  var str = data;
              var res = str.split("@");
              $("#loadCartdiv").html(data);
                /* $('html, body').animate({
                    scrollTop: $('#scroll_id').offset().top - 100 //#DIV_ID is an example. Use the id of your destination on the page
                }, 'slow');
                  if(res[1]=='success')
                  {
                    
                    $("#quantity"+id).val(final);

                     $('#display_msg').html('<span class="error" style="color:red;font-size: 16px;margin-left: 18px; width:100%">Quantity updated to cart</span>');
                      $('#display_msg').focus();
                      location.reload();
                      return false;
                  }
                  else 
                  {
                      $('#display_msg').html('<span class="error" style="color:red;font-size: 16px;margin-left: 18px; width:100%">'+res[2]+'</span>');
                      $('#display_msg').focus();
                      return false;
                  }*/
            }
        });
     }
  }

  function getCoup(code) {
  /* Get the text field */
  var copyText = document.getElementById("myInput"+code);

  /* Select the text field */
  copyText.select();
  copyText.setSelectionRange(0, 99999); /* For mobile devices */

  /* Copy the text inside the text field */
  document.execCommand("copy");

  /* Alert the copied text */
  alert("Copied the text: " + copyText.value);
}
</script>
    <script type="text/javascript">


        function doBidProducts(){
            var shop_id = "<?php echo $shop_id; ?>";
        var id = $(this).parents("tr").attr("id");

       swal({

        title: "Confirm!",

        text: "Are you sure you want to add the Bid ",

        type: "warning",

        showCancelButton: true,

        confirmButtonClass: "btn-danger",

        confirmButtonText: "Yes",

        cancelButtonText: "Cancel",

        closeOnConfirm: false,

        closeOnCancel: false

      },

      function(isConfirm) {

        if (isConfirm) {

                  $.ajax({
                    url:"<?php echo base_url(); ?>web/createUserBid",
                    method:"POST",
                    data:{vendor_id:shop_id},
                    success:function(data)
                    {
                        var str = data;
                        var res = str.split("@");
                          if(res[1]=='success')
                          {
                                swal('Bid Created Successfully');
                                window.location.href = "https://colormoon.in/veltask/";
                          }
                          else
                          {
                            swal("Something went wrong,Please try again"); 
                          }
                                          
                    }
                   });
                

        } else {

          swal("Cancelled", "", "error");

        }

      });

     

    }



      function applyCouponcode(couponcode)
      {
        $('.error').remove();
            var errr=0;
        var carttotal = $("#sub_total").val();
        var min_order_amount= $("#min_order_amount").val();

        var total_amount = $("#cart_total").val();
        $.ajax({
            url:"<?php echo base_url(); ?>web/applycoupon",
            method:"POST",
            data:{couponcode:couponcode,carttotal:carttotal,total_amount:total_amount},
            success:function(data)
            {
                  var str = data;
              var res = str.split("@");
                  //alert(JSON.stringify(res));
                
                  if(res[1]=='success')
                  {
                      //$("#couponcode").val();
                      $('#discount').html("₹"+res[3]);
                      $("#couponcode").val(couponcode);
                      var total_p = parseInt(min_order_amount)+parseInt(res[2]);

                      $('#total_p').html("₹"+total_p);

                      $('#total_default').hide();
                      $('#total_default_show').show();

                      $('#coupon_id').val(res[5]);
                      $('#applied_coupon_code').val(res[4]);
                      $('#coupon_discount').val(res[3]);
 

                
                     $('#coupon_msg').html('<div class="btn btn-success mb-4" >Coupon Applied successfully <span onclick="remove()" class="pr-2"><i class="fal fa-times"></i></span></div>');
                      $('#coupon_msg').focus();
                        return false;
                  }
                  else if(res[1]=='minorder'){
                         $('#coupon_msg').html('<div class="btn btn-danger mb-4" >Minimum Order Amount'+res[2]+'</div>');
                         $('#coupon_msg').focus();
                         return false;
                  }
                  else 
                  {

                      $('#coupon_msg').html('<div class="btn btn-danger mb-4" >Invalid Coupon</div>');
                      $('#coupon_msg').focus();
                        return false;
                  }
            }
        });
  }


      function validatecouponcode()
      {
        $('.error').remove();
            var errr=0;
        var couponcode = $("#couponcode").val();
        var carttotal = $("#sub_total").val();

         var total_amount = $("#cart_total").val();

         var min_order_amount= $("#min_order_amount").val();

        if(couponcode=='')
        {
            $('#coupon_msg').html('<div class="btn btn-danger mb-4" >Enter Coupon code</div>');
                      $('#coupon_msg').focus();
                        return false;
        }
        else
        {
        
        $.ajax({
            url:"<?php echo base_url(); ?>web/applycoupon",
            method:"POST",
            data:{couponcode:couponcode,carttotal:carttotal,total_amount:total_amount},
            success:function(data)
            {
                  var str = data;
              var res = str.split("@");
                  //alert(JSON.stringify(res));
                
                  if(res[1]=='success')
                  {

                       $('#discount').html("₹"+res[3]);

                      var total_p = parseInt(min_order_amount)+parseInt(res[2]);

                      $('#total_p').html("₹"+total_p);

                      $('#total_default').hide();
                      $('#total_default_show').show();

                      $('#coupon_id').val(res[5]);
                      $('#applied_coupon_code').val(res[4]);
                      $('#coupon_discount').val(res[3]);

                     $('#coupon_msg').html('<div class="btn btn-success mb-4" >Coupon Applied successfully <span onclick="remove()" class="pr-2"><i class="fal fa-times"></i></span></div>');
                      $('#coupon_msg').focus();
                        return false;
                  }
                  else if(res[1]=='minorder'){
                         $('#coupon_msg').html('<div class="btn btn-danger mb-4" >Minimum Order Amount'+res[2]+'</div>');
                         $('#coupon_msg').focus();
                         return false;
                  }
                  else 
                  {

                      $('#coupon_msg').html('<div class="btn btn-danger mb-4" >Invalid Coupon</div>');
                      $('#coupon_msg').focus();
                        return false;
                  }
            }
        });
     }
  }

  function remove()
  {
    location.reload();
  }

      function updateCart(cartid)
      {
        var quantity = $("#quantity"+cartid).val();
        $.ajax({
            url:"<?php echo base_url(); ?>web/updateCart",
            method:"POST",
            data:{cartid:cartid,quantity:quantity},
            success:function(data)
            {
                  var str = data;
              var res = str.split("@");
              
                 $('html, body').animate({
                    scrollTop: $('#scroll_id').offset().top - 100 //#DIV_ID is an example. Use the id of your destination on the page
                }, 'slow');
                  if(res[1]=='success')
                  {

                     $('#display_msg').html('<span class="error" style="color:red;font-size: 16px;margin-left: 18px; width:100%">Quantity updated to cart</span>');
                      $('#display_msg').focus();
                      location.reload();
                      return false;
                  }
                  else 
                  {
                      $('#display_msg').html('<span class="error" style="color:red;font-size: 16px;margin-left: 18px; width:100%">'+res[2]+'</span>');
                      $('#display_msg').focus();
                      return false;
                  }
            }
        });
      }
        function deletecartitems(cartid)
        {

             var id = $(this).parents("tr").attr("id");

       swal({

        title: "Are you sure?",

        text: "You want to delete cart item",

        type: "warning",

        showCancelButton: true,

        confirmButtonClass: "btn-danger",

        confirmButtonText: "Yes",

        cancelButtonText: "Cancel",

        closeOnConfirm: false,

        closeOnCancel: false

      },

      function(isConfirm) {

        if (isConfirm) {


             $.ajax({
              url:"<?php echo base_url(); ?>web/deleteCartItem",
              method:"POST",
              data:{cartid:cartid},
              success:function(data)
              {
                 var str = data;
              var res = str.split("@");
               $('html, body').animate({
                    scrollTop: $('#scroll_id').offset().top - 100 //#DIV_ID is an example. Use the id of your destination on the page
                }, 'slow');
                  if(res[1]=='success')
                  {
                    swal("Cart Item deleted successfully");
                     // $('#display_msg').html('<span class="error" style="color:red;font-size: 16px;margin-left: 18px; width:100%">Cart Item deleted successfully</span>');
                     //  $('#display_msg').focus();

                       location.reload();
                     //    return false;



                         
                  }
                  else 
                  {
                    swal("Something went wrong , please try again");
                      // $('#display_msg').html('<span class="error" style="color:red;font-size: 16px;margin-left: 18px; width:100%">Something went wrong , please try again</span>');
                      // $('#display_msg').focus();
                      //   return false;
                  }
                      
                        
              
              }
             });
        } else {

          swal("Cancelled", "Cancelled", "error");

        }

      });









           
    }
    </script>
     <!--shopping cart area end -->
<?php  $this->load->view("web/includes/footer"); ?>