<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                </div>
            <?php } ?>
            <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                </div>
            <?php } ?>
            <div class="ibox-title">
                <h5><?= $title ?></h5>
                <div class="ibox-tools">

                </div>
            </div>
            <div class="ibox-content">
                <form method="post" class="form-horizontal" action="" enctype="multipart/form-data">
                    <div class="form-group col-md-12 row">
                        <label class="control-label col-md-2">Referral Amount *</label>
                        <div class="col-sm-10">
                            <input type="number" name="referal_amount" class="form-control" min="1" required="" placeholder="Enter Referral Amount" value="<?= $referral->referal_amount ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-2">Referral Description *</label>
                        <div class="col-sm-10">
                            <textarea rows="6" class="form-control" name="referal_description" required placeholder="Enter Referral Description"><?= $referral->referal_description ?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-2">Referral Poster *</label>
                        <div class="col-sm-10">
                            <input type="file" accept="image/*" name="image" class="form-control" <?= ($referral->image) ? '' : 'required' ?>>
                        </div>
                    </div>
                    <?php if (!empty($referral->image)) { ?>

                        <div class="form-group row">
                            <label class="control-label col-md-2">Prev Poster </label>
                            <div class="col-sm-10">
                                <img src="<?= base_url('uploads/referrals/') . $referral->image ?>" alt="poster" style="width: 120px"/>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group row">
                        <label class="control-label col-md-2">Status</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="status" required>
                                <option value="1" <?= ($referral->status) ? 'selected' : '' ?>>Active</option>
                                <option value="0" <?= !($referral->status) ? 'selected' : '' ?>>Inactive</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <button class="btn btn-primary pull-right" name="btn_ref" value="submit">Submit</button>
                    </div>
                    <br><br><br>
                </form>
            </div>
        </div>
    </div>
</div>