<style>
    .cat_image{
        width: 100px;
        height: 100px;
        object-fit: scale-down;
        border-radius: 10px;
        margin: 0px 5px;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Payouts Stats</h5>
                    <div class="ibox-tools">

                    </div>

                    <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                        <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong style="color: green"> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                        </div>
                    <?php } ?>
                    <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                        <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong style="color: red">Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                        </div>
                    <?php }
                    ?>
                </div>
                <div class="ibox-content">
                    <div class="col-12 row">
                        <div class="col-md-6">
                            <div class="widget style1 blue-bg">
                                <div class="row">
                                    <div class="col-xs-12 text-center">
                                        <span> Total Earnings </span>
                                        <h2 class="font-bold"><i class="fa fa-inr"></i> <?= round($total_earnings, 2) ?></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="widget style1 red-bg">
                                <div class="row">
                                    <div class="col-xs-12 text-center">
                                        <span> Total Withdraw Amount </span>
                                        <h2 class="font-bold"><i class="fa fa-inr"></i> <?= round($total_withdrawls, 2) ?></h2>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="widget style1 yellow-bg">
                                <div class="row">
                                    <div class="col-xs-12 text-center">
                                        <span> Current Available Balance Amount </span>
                                        <h2 class="font-bold"><i class="fa fa-inr"></i> <?= round($current_balance, 2) ?></h2>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="widget style1 bg-success">
                                <div class="row">
                                    <div class="col-xs-12 text-center">
                                        <span> Onhold Amount </span>
                                        <h2 class="font-bold"><i class="fa fa-inr"></i> <?= round($onhold_amount, 2) ?></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr>
                    <h3>Request Payout</h3>
                    <br>
                    <div class="col-md-12 p-5">
                        <form method="post" class="col-12" action="<?= base_url('admin/franchise_services_withdraw_requests/request') ?>">
                            <div class="col-md-12 row">
                                <div class="col-md-4">
                                    <label class="control-label" for="requested_amount">Enter Amount *</label>
                                    <input type="number" min="1" onkeydown='return (event.which >= 48 && event.which <= 57) || event.which == 8 || event.which == 46 || (event.which >= 96 && event.which <= 105)' class="form-control" name="requested_amount" id="requested_amount" required placeholder="Enter Amount you want to request">
                                </div>
                                <div class="col-md-7" >
                                    <label class="control-label" for="description">Enter Description</label>
                                    <textarea class="form-control" name="description" id="description" required placeholder="Enter Description"></textarea>
                                </div>
                                <div class="col-md-1" style="margin-top: 30px">
                                    <button class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <br><br>
                    <br><br>
                    <br><br>
                    <br><br>
                    <hr>
                    <h3>Withdraw History</h3>
                    <br><br>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead> 
                                <tr>
                                    <th>#</th>
                                    <th>Requested Amount</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                    <th>Requested Date</th>
                                    <th>Updated Date</th>
                                    <th>Admin Response/Message</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($data as $index => $item) { ?>
                                    <tr>
                                        <td><?= $index + 1 ?></td>
                                        <td><i class="fa fa-inr"></i><?= $item->requested_amount ?></td>
                                        <td><?= $item->description ?></td>
                                        <td><?php if ($item->status) { ?>
                                                <span style="color: green">Settled</span>
                                            <?php } else { ?>
                                                <span style="color: orange">Pending</span>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <?= date('d M Y, h:i A', $item->created_at) ?>
                                        </td>
                                        <td>
                                            <?= ($item->updated_at) ? date('d M Y, h:i A', $item->updated_at) : "N/A" ?>
                                        </td>
                                        <td>
                                            <?php if ($item->status) { ?>
                                                <p><strong>Description : </strong><?= ($item->admin_description) ? $item->admin_description : "N/A" ?></p>
                                                <p><strong>Mode Of Payment : </strong><?= ($item->mode_of_payment) ? $item->mode_of_payment : "N/A" ?></p>
                                                <?php if ($item->mode_of_payment == "offline") { ?>
                                                    <p><strong>Sender Name : </strong><?= ($item->sender_name) ? $item->sender_name : "N/A" ?></p>
                                                    <p><strong>Reciever Name : </strong><?= ($item->reciever_name) ? $item->reciever_name : "N/A" ?></p>
                                                <?php } else { ?>
                                                    <p><strong>Transaction Id : </strong><?= ($item->sender_name) ? $item->sender_name : "N/A" ?></p>
                                                <?php } ?>
                                            <?php } else { ?>
                                                N/A
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
