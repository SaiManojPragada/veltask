<div class="row">

    <div class="col-lg-12">

        <div class="ibox float-e-margins">

            <div class="ibox-title">

                <h5>Assign Bid</h5>

                <div class="ibox-tools">

                    <a href="<?= base_url() ?>admin/bid_orders" style="float: right; margin: 8px;">
                        <button class="btn btn-primary">Back</button>
                    </a>

                </div>

            </div>
                 <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                        <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                        </div>
                    <?php } ?>
                    <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                        <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                        </div>
                    <?php }
                    ?>
            <div class="ibox-content">
            
                    
                    <?php 
                    $bidss_qry = $this->db->query("select * from user_bids where id='".$bid."'");
                    $bidss_qry_row = $bidss_qry->row();

                    $vcart_qry = $this->db->query("select * from cart where session_id='".$bidss_qry_row->session_id."'");
                    $vcart_row = $vcart_qry->row();

                    $vlink_qry = $this->db->query("select * from link_variant where id='".$vcart_row->variant_id."'");
                    $vlink_row = $vlink_qry->row();

                    $vprod_qry = $this->db->query("select * from products where id='".$vlink_row->product_id."'");
                    $vprod_row = $vprod_qry->row();


                    $cat_id = $vprod_row->cat_id;
                    $vendor_qry = $this->db->query("select * from admin_comissions where cat_id='".$cat_id."' group by shop_id");
                    $vendor_result=$vendor_qry->result();
                ?>


                <form method="post" class="form-horizontal" action="<?= base_url() ?>admin/bid_orders/assign_bid_to_vendor">




                    <input type="hidden" name="bidid" value="<?php echo $bid; ?>">
                    

                    <div class="form-group">

                        <label class="col-sm-2 control-label">Select Vendors</label>

                        <div class="col-sm-6">

                            <select name="vendors[]" id="vendors"  class="form-control js-example-basic-multiple" multiple="multiple" required="">
                                <option value="">Select Category</option>
                                <?php 
                    foreach ($vendor_result as $vendor_value) 
                    { 
                        $ven_qry = $this->db->query("select * from vendor_shop where id='".$vendor_value->shop_id."'");
                        $ven_row = $ven_qry->row();
                        if($ven_qry->num_rows()>0){

                                $vendor_bids_qry = $this->db->query("select * from vendor_bids where vendor_id='".$ven_row->id."' and bid_id='".$bid."'");
                                $vendor_bids_num_rows = $vendor_bids_qry->num_rows();
                                if($vendor_bids_num_rows>0){}else{
                        ?>
                                <option value="<?php echo $ven_row->id; ?>"><?php echo $ven_row->shop_name." ( ".$ven_row->owner_name." )"; ?></option>
                    <?php } 
                        }
                    } ?>
                            </select>
                        </div>

                    </div>

                     <div class="form-group">

                        <label class="col-sm-2 control-label">Admin Commission: </label>

                        <div class="col-sm-6">
                            <input type="number" class="form-control" min="1" name="admin_commission" required="">
                        </div>
                    </div>


                   


                    <div class="form-group">

                        <div class="col-sm-4 col-sm-offset-2">

                            <button type="submit" class="btn btn-primary">Assign</button>
                        </div>

                    </div>

                </form>

            </div>

        </div>

    </div>

</div>


<script type="text/javascript">

  
  $('#btn_manageattributes').click(function(){
        $('.error').remove();
            var errr=0;
      if($('#types').val()=='')
      {
         $('#types').after('<span class="error" style="color:red;font-size: 18px;margin-left: 18px;">Select Attribute Type</span>');
         $('#types').focus();
         return false;
      }
      else if($('#categories').val()=='' || $('#categories').val()==undefined)
      {
         $('#categories').after('<span class="error" style="color:red;font-size: 18px;margin-left: 18px;">Select Categories</span>');
         $('#categories').focus();
         return false;
      }
 });

</script>