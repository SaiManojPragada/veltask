<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">


        <div class="col-lg-12">

            <div class="ibox-tools" style="margin: 5px 30px; ">
                <a href="<?= base_url() ?>admin/users">
                    <button class="btn btn-primary">BACK</button>
                </a>
            </div>

            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Users Details</h5>


                    </div>
                    <div class="ibox-content">
                        <table class="table table-striped">
                            <tr>
                                <td>Image</td>
                                <td><?php if ($users->image != '') { ?><img src="<?php echo base_url(); ?>/uploads/users/<?php echo $users->image; ?>" style="width: 80px; height: 80px;"><?php } ?></td>
                            </tr>
                            <tr>
                                <td>Name</td>
                                <td><?php echo $users->first_name . "" . $users->last_name; ?></td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td><?php echo $users->email; ?></td>
                            </tr>
                            <tr>
                                <td>Phone</td>
                                <td><?php echo $users->phone; ?></td>
                            </tr>
                            <tr>
                                <td>User Status</td>
                                <td><?php
                                    if ($users->otp_status == 0) {
                                        echo "Not Verified";
                                    } else {
                                        echo "Verified";
                                    }
                                    ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>


            <!--  -->

            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>User Address</h5>
                    </div>
                    <div class="ibox-content">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Name</th>
                                    <th>Mobile</th>
                                    <th>Address</th>
                                    <th>Landmark</th>
                                    <th>City</th>
                                    <th>State</th>
                                    <th>Pincode</th>
                                    <th>Address Type</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $qry = $this->db->query("select * from user_address where user_id='" . $users->id . "'");
                                $address = $qry->result();
                                $i = 1;
                                if ($qry->num_rows() > 0) {
                                    foreach ($address as $value) {
                                        $state_qry = $this->db->query("select * from states where id='" . $value->state . "'");
                                        $state_row = $state_qry->row();

                                        $city_qry = $this->db->query("select * from cities where id='" . $value->city . "'");
                                        $city_row = $city_qry->row();

                                        $pincode_qry = $this->db->query("select * from pincodes where id='" . $value->pincode . "'");
                                        $pincode_row = $pincode_qry->row();
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $value->name; ?></td>
                                            <td><?php echo $value->mobile; ?></td>
                                            <td><?php echo $value->address; ?></td>
                                            <td><?php echo $value->landmark; ?></td>
                                            <td><?php echo $city_row->city_name; ?></td>
                                            <td><?php echo $state_row->state_name; ?></td>
                                            <td><?php echo $pincode_row->pincode; ?></td>
                                            <td><?php
                                                if ($value->address_type == 1) {
                                                    echo "HOME";
                                                } else if ($value->address_type == 2) {
                                                    echo "OFFICE";
                                                } else if ($value->address_type == 3) {
                                                    echo "DEFAULT";
                                                }
                                                ?> </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="8" style="text-align: center">
                                            <h4>No Address Found</h4>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">

            <div class="col-lg-6 " style="display: none">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>User Orders</h5>


                    </div>
                    <div class="ibox-content">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>

                                <tr>
                                    <th>#</th>
                                    <th>Vendor Name</th>
                                    <th>Order ID</th>
                                    <th>User Details</th>
                                    <th>Delivery address</th>
                                    <th>Payment Option</th>
                                    <th>Payment Status</th>
                                    <th>Order Status</th>
                                    <th>Order Amount</th>
                                    <th>Admin Comission</th>
                                    <th>Delivery Details</th>
                                    <th>Created Date</th>
                                    <th>Vendor Price</th>
                                    <th>Coupon Code</th>
                                    <th>Action</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;

                                $ord = $this->db->query("select * from orders where user_id='" . $users->id . "'");
                                $orders = $ord->result();

                                if (count($orders) > 0) {

                                    foreach ($orders as $ord) {
                                        $user = $this->db->query("select * from users where id='" . $ord->user_id . "'");
                                        $users = $user->row();

                                        $ads = $this->db->query("select * from user_address where id='" . $ord->deliveryaddress_id . "'");
                                        $address = $ads->row();

                                        $ven = $this->db->query("select * from vendor_shop where id='" . $ord->vendor_id . "'");
                                        $vendor = $ven->row();
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $vendor->shop_name; ?></td>
                                            <td><?php echo $ord->id; ?></td>
                                            <td>
                                                <?php if ($user->num_rows() > 0) { ?>
                                                    <b>Name : </b><?php echo $users->first_name . " " . $users->last_name; ?><br>
                                                    <b>Email : </b><?php echo $users->email; ?><br>
                                                    <b>Phone : </b><?php echo $users->phone; ?>
                                                <?php } ?>
                                            </td>
                                            <td><?php echo $ord->user_address; ?>
                                            </td>

                                            <td><?php echo $ord->payment_option; ?></td>
                                            <td><?php
                                                if ($ord->payment_status == 1) {
                                                    echo "Paid";
                                                } else {
                                                    echo "Unpaid";
                                                }
                                                ?></td>
                                            <td><?php
                                                if ($ord->order_status == 1) {
                                                    echo "Pending";
                                                } else if ($ord->order_status == 2) {
                                                    echo "Proccessing";
                                                } else if ($ord->order_status == 3) {
                                                    echo "Assigned to delivery to pick up";
                                                } else if ($ord->order_status == 4) {
                                                    echo "Delivery Boy On the way";
                                                } else if ($ord->order_status == 5) {
                                                    echo "Delivered";
                                                } else if ($ord->order_status == 6) {
                                                    echo "Cancelled";
                                                }
                                                ?>
                                            </td>
                                            <td><?php echo $ord->total_price; ?></td>
                                            <td><?php
                                                $cart_qry = $this->db->query("select * from cart where session_id='" . $ord->session_id . "'");
                                                $cart_report = $cart_qry->result();
                                                $new_width = 0;
                                                foreach ($cart_report as $value) {
                                                    $variant_id = $value->variant_id;
                                                    $lv_qry = $this->db->query("select * from link_variant where id='" . $variant_id . "'");
                                                    $lv_report = $lv_qry->row();

                                                    $pro_qry = $this->db->query("select * from products where id='" . $lv_report->product_id . "'");
                                                    $pro_report = $pro_qry->row();

                                                    $adm_com = $this->db->query("select * from admin_comissions where id='" . $pro_report->cat_id . "'");
                                                    $admin_comsn = $adm_com->row();
                                                    $percentage = $admin_comsn->admin_comission;
                                                    $totalWidth = $value->unit_price;

                                                    $new_width += ($percentage / 100) * $totalWidth;
                                                }
                                                echo $new_width;
                                                ?></td>
                                            <td><b>Name: </b>
                                                <?php
                                                $deli = $this->db->query("select * from deliveryboy where id='" . $ord->delivery_boy . "'");
                                                if ($deli->num_rows() > 0) {
                                                    $delivery_person = $deli->row();
                                                    echo $delivery_person->name;
                                                }
                                                ?><br>                                    
                                                <b>Commission: </b> <?php echo $ord->deliveryboy_commission; ?>

                                            </td>
                                            <td><?php echo date("d M,Y", $ord->created_at); ?></td>
                                            <td><?php echo $ord->sub_total - $new_width; ?></td>
                                            <td>
                                                <?php
                                                if ($ord->coupon_id != 0) {
                                                    ?>
                                                    <p><b>Coupon Code : </b><?php echo $ord->coupon_code; ?></p>
                                                    <p><b>Coupon Discount : </b><?php echo $ord->coupon_disount . " Rs"; ?></p>
                                                <?php } ?>
                                            </td>
                                            <td><a href="<?php echo base_url(); ?>admin/orders/orderDetails/<?php echo $ord->session_id; ?>">
                                                    <button class="btn btn-xs btn-info"><i class="fa fa-eye" aria-hidden="true"></i>  View </button>
                                                </a></td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="8" style="text-align: center">
                                            <h4>No Orders Found</h4>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Service Orders</h5>
                    </div>
                    <div class="ibox-content">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>

                                <tr>
                                    <th>#</th>
                                    <th>Order Id</th>
                                    <th>User Details</th>
                                    <th>Time Slot</th>
                                    <th>Ordered Categories</th>
                                    <?php if ($_SESSION['admin_login']['user_type'] != 'franchise') { ?>
                                        <th>Ordered Settlement Amounts</th>
                                    <?php } ?>
                                    <th>User Address</th>
                                    <th>Visit and Quote</th>
                                    <th>Amount Paid</th>
                                    <th>Ordered On</th>
                                    <th>Ordered Status</th>
                                    <th>Action</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($service_orders as $index => $item) { ?>
                                    <tr>
                                        <td><?= $index + 1 ?></td>
                                        <td><?= $item->order_id ?></td>
                                        <td>
                                            <p>
                                                <b>Name : </b><?= ($item->customer_details->first_name) ? $item->customer_details->first_name : 'N/A' ?>
                                            </p>
                                            <p>
                                                <b>Email : </b><?= ($item->customer_details->email) ? $item->customer_details->email : 'N/A' ?>
                                            </p>
                                            <p>
                                                <b>Phone : </b><?= ($item->customer_details->phone) ? $item->customer_details->phone : 'N/A' ?>
                                            </p>
                                        </td>
                                        <td>
                                            <p><b>Date : </b><?= $item->time_slot_date ?></p>
                                            <p><b>Time : </b><?= $item->time_slot ?></p>
                                        </td>
                                        <td>
                                            <p><?php
                                                foreach ($item->selected_categories as $in => $it) {
                                                    if ($in != 0) {
                                                        echo ', ' . $it->name;
                                                    } else {
                                                        echo $it->name;
                                                    }
                                                }
                                                ?>
                                            </p>
                                        </td>
                                        <?php if ($_SESSION['admin_login']['user_type'] != 'franchise') { ?>
                                            <td>
                                                <b>Total Amount : </b><?= $item->sub_total + $item->visiting_charges + $item->tax ?><br>
                                                <b>Admin Commission : </b><?= ($item->admin_comission) ? $item->admin_comission : "N/A" ?><br>
                                                <b>Franchise Commission : </b><?= ($item->franchise_comission) ? $item->franchise_comission : "N/A" ?><br>
                                                <b>Service Provider Commission : </b><?= ($item->service_provider_comission) ? $item->service_provider_comission : "N/A" ?><br>
                                                <b>Gst : </b><?= ($item->gst) ? $item->gst : "N/A" ?>
                                            </td>
                                        <?php } ?>
                                        <td>
                                            <p><?= $item->customer_address->address . ', ' ?><br>
                                                <?= $item->customer_address->landmark . ', ' . $item->customer_address->city_name . ', ' . $item->customer_address->state_name . ', ' . $item->customer_address->pincode ?>
                                            </p>
                                        </td>
                                        <td>
                                            <?php if ($item->has_visit_and_quote == 'Yes') { ?>
                                                <span style="color: green"><?= $item->has_visit_and_quote ?></span>
                                            <?php } else { ?>
                                                <span style="color: tomato"><?= $item->has_visit_and_quote ?></span>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <?= $item->amount_paid ?>
                                        </td>
                                        <td>
                                            <?= date('d M Y, h:i A', $item->created_at) ?>
                                        </td>
                                        <td style="color: <?= ($item->order_status == "order_completed") ? 'green' : '' ?><?= ($item->order_status == "order_cancelled" || $item->order_status == "order_rejected" || $item->order_status == "refunded") ? 'red' : '' ?>">
                                            <?= ucwords(str_replace("_", " ", $item->order_status)) ?>
                                        </td>
                                        <td>
                                            <a class="btn btn-success btn-xs" href="<?= base_url('admin/services_orders/view/' . $item->id) ?>">View Order</a>
                                            <?php if ($_SESSION['admin_login']['user_type'] != 'franchise' && $item->order_status == "order_cancelled") { ?>
                                                <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#approveCancellation" onclick="assignCancel('<?= $item->id ?>')">Approve Cancellation</button>
                                            <?php } ?>
                                            <?php if ($item->order_status == "order_placed") { ?>
                                                <button onclick="req_cancel_order('<?= $item->user_id ?>', '<?= $item->id ?>');" class="btn btn-xs btn-danger">Cancel Order</button>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>

    </div>


</div>

