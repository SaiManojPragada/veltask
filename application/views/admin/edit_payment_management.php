<style>
    .category_comm_span{
        top: -5px;
        position: relative;
        left: 10px;
    }
    .cat_commission{
        top: -5px;
        position: relative;
        left: 21px;
    }
</style>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= $title ?></h5>
                <div class="ibox-tools">
                    <a href="<?= base_url() ?>admin/payment_management">
                        <button class="btn btn-primary">BACK</button>
                    </a>
                </div>
                <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                    <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                    </div>
                <?php } ?>
                <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                    <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                    </div>
                <?php }
                ?>
            </div>
            <div class="ibox-content test">
                <form method="post" class="form-horizontal" enctype="multipart/form-data" action="<?= base_url() ?>admin/payment_management/update/<?= $payment->id ?>">

                    <!-- <div class="form-group">
                        <label class="col-sm-2 control-label">Delivery Types</label>
                        <div class="col-sm-10">
                          <select id="delivery_type" name="delivery_type" class="form-control">
                             <option value="">Select Delivery Types</option>
                             <option value="full_time_driver">Full Time Driver</option>
                             <option value="pay_for_driver">Pay for Driver</option>
                          </select>
                        </div>
                    </div>
                    -->
                    <div class="form-group"><label class="col-sm-2 control-label">Payment Type</label>
                        <div class="col-sm-10">
                            <input readonly type="text" class="form-control" value="<?= $payment->payment_method_title ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Status *</label>
                        <div class="col-sm-10">
                            <fieldset>
                                <input type="radio" name="status" value="1" id="status" <?= ($payment->status) ? 'checked' : '' ?>/><label for="status">&nbsp;&nbsp;Active</label><br>
                                <input type="radio" name="status" value="0" id="status1" <?= !($payment->status) ? 'checked' : '' ?>/><label for="status1">&nbsp;&nbsp;In Active</label><br> 
                            </fieldset>
                        </div>
                    </div>


                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" id="btn_francise" type="submit">Update </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
</script>

<link href="https://test.indiasmartlife.com/admin_assets/css/jquery.datetimepicker.css" rel="stylesheet">
<script src="https://test.indiasmartlife.com/admin_assets/js/jquery.datetimepicker.js"></script>