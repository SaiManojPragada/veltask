<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Bid Orders</h5>
                    <div class="ibox-tools">
                        <a href="<?= base_url() ?>admin/bid_orders">
                            <button class="btn btn-primary">BACK</button>
                        </a>
                    </div>

                     <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                        <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                        </div>
                    <?php } ?>
                    <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                        <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                        </div>
                    <?php }
                    ?>
                </div>

                        
                <div class="ibox-content">

                    

                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            
                            <tr>
                                <th>#</th>
                                <th>Bid ID</th>
                                <th>User Details</th>
                                <th>Product Details</th>
                                <th>Total Amount</th>
                                <th>Assign Vendors</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i=1;
                            if(count($bids)>0)
                            {
                            foreach($bids as $bid){
                                $user = $this->db->query("select * from users where id='".$bid->user_id."'");
                                $users = $user->row();

                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $bid->id; ?></td>
                                <td>
                                    <p><b>Name : </b><?php echo $users->first_name." ".$users->last_name; ?></p>
                                    <p><b>Email :</b> <?php echo $users->email; ?></p>
                                    <p><b>Mobile :</b> <?php echo $users->phone; ?></p>
                                </td>
                                <td style="width: 100%; height:100%;">
                                    <table class="table table-striped table-bordered table-hover">
                                        <tr>
                                            <th>Product Name</th>
                                            <th>Attribute</th>
                                            <th>Category</th>
                                            <th>Sub Category</th>
                                            <th>QTY</th>
                                            <th>Order Amount</th>
                                        </tr>
                                        <?php $cart_qry = $this->db->query("select * from cart where session_id='".$bid->session_id."'");
                                              $cart_result = $cart_qry->result();
                                              $admin_total=0;
                                              $unit_price=0;
                                              foreach ($cart_result as $cart_value) 
                                              { 
                                                $link_qry = $this->db->query("select * from link_variant where id='".$cart_value->variant_id."'");
                                                $link_row = $link_qry->row();


                                                $prod_qry = $this->db->query("select * from products where id='".$link_row->product_id."'");
                                                $prod_row = $prod_qry->row();

                                                $pi_qry = $this->db->query("select * from product_images where product_id='".$link_row->product_id."' and variant_id='".$link_row->id."'");
                                                $pi_qry_row = $pi_qry->row();
                                                $image = $pi_qry_row->image;

                                                $cat_id = $prod_row->cat_id;
                                                $sub_cat_id = $prod_row->sub_cat_id;
                                                $cart_vendor_id = $cart_value->vendor_id;
                                            $adminc_qry = $this->db->query("select * from admin_comissions where shop_id='".$cart_vendor_id."' and cat_id='".$cat_id."' and find_in_set('".$sub_cat_id."',subcategory_ids)");
                                            $adminc_row = $adminc_qry->row();
                                                    $cat_qry = $this->db->query("select * from categories where id='".$cat_id."'");
                                                    $cat_row = $cat_qry->row();

                                                    $scat_qry = $this->db->query("select * from sub_categories where id='".$sub_cat_id."'");
                                                    $scat_row = $scat_qry->row();


                                                    if($adminc_row->admin_comission!='')
                                                    {
                                                        $admin_comission=$adminc_row->admin_comission;
                                                    }
                                                    else
                                                    {
                                                        $admin_comission=0;
                                                    }
                                              ?>

                                        <tr>
                                         <td><?php echo $prod_row->name; ?>
                                         <?php if($image!=''){ ?>
                                             <img src="<?php echo base_url(); ?>uploads/products/<?php echo $image;?>" style="width: 60px; height: 60px;">
                                         <?php }else{ ?>
                                           <img src="<?php echo base_url(); ?>uploads/products/default.png" style="width: 60px; height: 60px;">
                                         <?php } ?>
                                         </td>

                                          <td> <?php  $varint = $link_qry->result();
                                                $ar=[];
                                                foreach ($varint as $value) 
                                                {
                                                       $json = json_decode($value->jsondata);
                                                        $attributes=[];

                                                        foreach ($json as $value1) 
                                                        {
                                                                 $att_type_qry = $this->db->query("select * from attributes_title where id='".$value1->attribute_type."'");
                                                                 $types = $att_type_qry->row();

                                                                $values_qry = $this->db->query("select * from attributes_values where id='".$value1->attribute_value."'");
                                                                $values = $values_qry->row(); 
                                                                echo "<b>".$types->title."</b> : ".$values->value;
                                                          //$attributes[] = array('type'=>$types->title,'value'=>$values->value);  
                                                        }
                                                } ?>
                                            </td>

                                            <td><?php echo $cat_row->category_name; ?></td>
                                            <td><?php echo $scat_row->sub_category_name; ?></td>
                                            <td><?php echo $cart_value->quantity; ?></td>
                                            
                                            <td><?php echo $cart_value->unit_price; ?></td>
                                        </tr>
                                            <?php   $percentage = ($cart_value->unit_price/100)*$admin_comission; 
                                                    $admin_total = $percentage+$admin_total;
                                                    $unit_price=$cart_value->unit_price+$unit_price;
                                             ?>
                                             
                                        <?php  } ?>
                                    </table>
                                </td>
                                <td><?php echo $bid->sub_total; ?></td>
                                <td>
                                        <?php if($bid->bid_status==0 || $bid->bid_status==1){ ?>
                                        <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">Assign Vendors</button> -->
                                        <a href="<?php echo base_url(); ?>admin/bid_orders/assignbids/<?php echo $bid->id; ?>"><button class="btn btn-xs btn-info"> Assign Vendors</button></a>

                                        <a href="<?php echo base_url(); ?>admin/bid_orders/vendors_bids/<?php echo $bid->id; ?>"><button class="btn btn-xs btn-info"> Vendor Bids</button></a>
                                    <?php } ?>
                             </td>
                             <td><?php if($bid->bid_status==0){ echo "Bid created"; }else if($bid->bid_status==1){ echo "Bid accepted"; }else if($bid->bid_status==2){ echo "Bid Completed"; }else if($bid->bid_status==3){ echo "Bid Cancelled"; }?></td>
                            </tr>

                            <!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Assign Bid</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
       <form method="post" class="form-horizontal" action="<?= base_url() ?>admin/bid_orders/assign_bid_to_vendor">
      <div class="modal-body">
                <?php 
                    $vcart_qry = $this->db->query("select * from cart where session_id='".$bid->session_id."'");
                    $vcart_row = $vcart_qry->row();

                    $vlink_qry = $this->db->query("select * from link_variant where id='".$vcart_row->variant_id."'");
                    $vlink_row = $vlink_qry->row();

                    $vprod_qry = $this->db->query("select * from products where id='".$vlink_row->product_id."'");
                    $vprod_row = $vprod_qry->row();


                    $cat_id = $vprod_row->cat_id;
                    $vendor_qry = $this->db->query("select * from admin_comissions where cat_id='".$cat_id."' group by shop_id");
                    $vendor_result=$vendor_qry->result();
                ?>
                <input type="hidden" name="bidid" value="<?php echo $bid->id; ?>">
                <label>Select Vendors</label>
                <select name="vendors[]" id="vendors" class="select2-selection select2-selection--multiple" multiple="multiple" style="width: 100%"  required="">
                    <?php 
                    foreach ($vendor_result as $vendor_value) 
                    { 
                        $ven_qry = $this->db->query("select * from vendor_shop where id='".$vendor_value->shop_id."'");
                        $ven_row = $ven_qry->row();
                        if($ven_qry->num_rows()>0){

                                $vendor_bids_qry = $this->db->query("select * from vendor_bids where vendor_id='".$ven_row->id."' and bid_id='".$bid->id."'");
                                $vendor_bids_num_rows = $vendor_bids_qry->num_rows();
                                if($vendor_bids_num_rows>0){}else{
                        ?>
                        <option value="<?php echo $ven_row->id; ?>"><?php echo $ven_row->shop_name." ( ".$ven_row->owner_name." )"; ?></option>
                    <?php } 
                        }
                    } ?>

                </select>
                <div class="row">
        <label>Admin Commission: </label>
            <input type="number" class="form-control" min="1" name="admin_commission" required="">
       </div>
      </div>

       

      
      <div class="modal-footer">
       <!--  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
        <button type="submit" class="btn btn-primary">Assign</button>
      </div>
</form>

   <link href="<?php echo base_url(); ?>admin_assets/assets/js/select2.min.css" rel="stylesheet" /> 
<script src="<?php echo base_url(); ?>admin_assets/assets/js/select2.min.js"></script>
<script>
        $(document).ready(function() {
           $('#vendors').select2({
        dropdownParent: $('#exampleModalLong')
    });
        });


</script> 
   <style type="text/css">
       .select2-container{
        z-index: 9999;
        position: relative;
       }
   </style>


    </div>
  </div>
</div>

                            <?php $i++; } }else{?>
                            <tr>
                                <td colspan="8" style="text-align: center">
                                    <h4>No Bids Found</h4>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>


<script type="text/javascript">
    
    /*function updateStatus(value,order_id)
    {
            if(value != '')
            {
             $.ajax({
              url:"<?php echo base_url(); ?>/admin/orders/changeStatus",
              method:"POST",
              data:{value:value,order_id:order_id},
              success:function(data)
              {
               if(data=='success')
               {
                alert("status changed successfully");
                window.location.href = "<?php echo base_url(); ?>vendors/orders";
               }
              }
             });
            }
    }*/

</script> 


