<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $title ?> &nbsp;&nbsp;&nbsp;<small><b>Overall Rating : <?php if ($provider_rating) { ?>(<i class="fa fa-star" style="color: gold"></i> <?= $provider_rating ?>)<?php } ?></b></small></h5>
                    <div class="ibox-tools">
                        <a href="<?= base_url() ?>admin/service_providers">
                            <button class="btn btn-primary">Go back</button>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>
                                    <td>S.No</td>
                                    <td>Order Details</td>
                                    <td>Rating</td>
                                    <td>Comment</td>
                                    <td>Last Updated On</td>
                                    <td>Actions</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($ratings as $index => $rat) { ?>
                                    <tr>
                                        <td><?= $index + 1 ?></td>
                                        <td>
                                            <b>Service Name : </b><?= $rat->order_details->service_name ?><br>
                                            <b>Price : </b><?= $rat->order_details->sub_total ?><br>
                                            <b>Quantity : </b><?= $rat->order_details->quantity ?>
                                        </td>
                                        <td><i class="fa fa-star" style="color: gold"></i> <?= $rat->rating ?></td>
                                        <td><?= $rat->comment ?></td>
                                        <td><?= date('d M Y, h:i A', $rat->updated_at) ?></td>        
                                        <td>
                                            <a href="<?= base_url('admin/services_orders/view/') . $rat->order_id ?>">
                                                <button class="btn btn-xs btn-success">View Order</button>
                                            </a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>