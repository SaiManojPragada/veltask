<style>
    .cat_image{
        width: 100px;
        height: 100px;
        object-fit: scale-down;
        border-radius: 10px;
        margin: 0px 5px;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $title ?></h5>
                    <div class="ibox-tools">
                        <a href="<?= base_url() ?>admin/dashboard">
                            <button class="btn btn-primary">BACK</button>
                        </a>

                        <?php
                        $user_type = $_SESSION['admin_login']['user_type'];
                        if ($user_type == 'subadmin') {
                            $admin_id = $_SESSION['admin_login']['id'];
                            $adm_qry = $this->db->query("select * from sub_admin where id='" . $admin_id . "'");
                            $adm_row = $adm_qry->row();

                            $userpermissions = $adm_row->permissions;
                            $permissions = explode(",", $userpermissions);
                            if (in_array("add_category", $permissions)) {
                                ?>

                                <a href="<?= base_url() ?>admin/franchise_tabs/add">
                                    <button class="btn btn-primary">+ Add Franchise count</button>
                                </a>
                                <?php
                            }
                        } else {
                            ?>

                            <a href="<?= base_url() ?>admin/franchise_tabs/add">
                                <button class="btn btn-primary">+ Add Franchise count</button>
                            </a>
                        <?php } ?>


                    </div>

                    <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                        <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                        </div>
                    <?php } ?>
                    <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                        <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                        </div>
                    <?php }
                    ?>
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Image</th>
                                    <th>Title</th>
                                    <th>Sub Title</th>
                                    <th>Priority</th>
                                    <th>Status</th>
                                    <th>Last Updated at</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($franchise_tabs as $i => $item) {
                                    ?>
                                    <tr class="gradeX">
                                        <td><?= $i + 1 ?></td>

                                        <td><img src="<?= base_url('uploads/franchise_tabs/') . $item->image ?>" alt="alt" width="120"/></td>
                                        <td><?= $item->count ?></td>
                                        <td><?= $item->quote ?></td>
                                        <td><?= $item->priority ?></td>
                                        <td><?php if ($item->status) { ?>
                                                <span style="color: green">Active</span>
                                            <?php } else { ?>
                                                <span style="color: tomato">InActive</span>
                                            <?php } ?></td>
                                        <td><?= date('d M Y, h:i A', $item->updated_at) ?></td>
                                        <td>
                                            <a href="<?= base_url('admin/franchise_tabs/edit/') . $item->id ?>" class="btn btn-xs btn-primary">edit</a>
                                            <a href="javascript:void(0);" onclick="if (confirm('Are you sure want to delete ?')) {
                                                            location.href = `<?= base_url('admin/franchise_tabs/delete/') . $item->id ?>`;
                                                        }" class="btn btn-xs btn-danger">Delete</a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Occupation</th>
                                    <th>Comment</th>
                                    <th>Priority</th>
                                    <th>Status</th>
                                    <th>Last Updated at</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

