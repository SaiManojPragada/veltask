<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= $title ?></h5>
                <div class="ibox-tools">
                    <a href="<?= base_url() ?>admin/services_banners">
                        <button class="btn btn-primary">BACK</button>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form method="post" class="form-horizontal" enctype="multipart/form-data"  action="<?= base_url() ?>admin/services_banners/<?= $func ?><?= $data->id ?>">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Title *</label>
                        <div class="col-sm-10">
                            <input type="text" id="title" name="title" class="form-control" value="<?= $data->title ?>" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">City *</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="city" name="cities_id" required>
                                <option value="">Select City</option>
                                <?php foreach ($cities as $city) { ?>
                                    <option value="<?= $city->id ?>" <?= ($data->cities_id == $city->id) ? "selected" : "" ?>><?= $city->city_name ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div> 

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Web Image *</label>
                        <div class="col-sm-10">
                            <input type="file" name="web_image" id="web_image" class="form-control" accept="image/*" <?php
                            if (!$data) {
                                echo 'required="true"';
                            }
                            ?>>
                                   <?php if (!empty($data->web_image)) { ?>
                                <img src="<?= base_url('uploads/services_banners/' . $data->web_image) ?>" width="150" alt="alt"/>
                            <?php } ?>    
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">App Image *</label>
                        <div class="col-sm-10">
                            <input type="file" name="app_image" id="app_image" class="form-control" accept="image/*"  <?php
                            if (!$data) {
                                echo 'required="true"';
                            }
                            ?>>
                                   <?php if (!empty($data->app_image)) { ?>
                                <img src="<?= base_url('uploads/services_banners/' . $data->app_image) ?>" width="150" alt="alt"/>
                            <?php } ?>  
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-4">
                            <a href="https://www.latlong.net/" target="_blank" class="btn btn-primary pull-right">Get Latitude and Longitude</a>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Latitude *</label>
                        <div class="col-sm-10">
                            <input type="text" min="1" id="latitude" name="lat" class="form-control" value="<?= $data->lat ?>" required 
                                   onkeydown='return (event.which >= 48 && event.which <= 57) || event.which == 8 || event.which == 46 || event.which == 190 || (event.which >= 96 && event.which <= 105)'>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Longitude *</label>
                        <div class="col-sm-10">
                            <input type="text" min="1" id="longitude" name="lng" class="form-control" value="<?= $data->lng ?>" required 
                                   onkeydown='return (event.which >= 48 && event.which <= 57) || event.which == 8 || event.which == 46 || event.which == 190 || (event.which >= 96 && event.which <= 105)'>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Status *</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="status" name="status" required>
                                <option value="">Select Status</option>
                                <option value="1" <?= ($data && $data->status) ? "selected" : "" ?>>Active</option>
                                <option value="0" <?= ($data && !$data->status) ? "selected" : "" ?>>InActive</option>
                            </select>
                        </div>
                    </div>


                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" type="submit" id="btn_category"> <i class="fa fa-plus-circle"></i> Add</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    $('#btn_category').click(function () {
    $('.error').remove();
    var errr = 0;
    if ($('#title').val() == '') {
    $('#title').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Title</span>');
    $('#title').focus();
    return false;
    } else if ($('#city').val() == '') {
    $('#city').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Select City</span>');
    $('#city').focus();
    return false;
    }
<?php if (empty($data->web_image)) { ?>
        else if ($('#web_image').val() == '') {
        $('#web_image').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Select Web Image</span>');
        $('#web_image').focus();
        return false;
        }
<?php } ?>
<?php if (empty($data->web_image)) { ?>
        else if ($('#app_image').val() == '') {
        $('#app_image').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Select App Image</span>');
        $('#app_image').focus();
        return false;
        }
<?php } ?>
    else if ($('#latitude').val() == '') {
    $('#latitude').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Latitude</span>');
    $('#latitude').focus();
    return false;
    }
    else if ($('#longitude').val() == '') {
    $('#longitude').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter Longitude</span>');
    $('#longitude').focus();
    return false;
    }
    else if ($('#status').val() == '') {
    $('#status').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Select Status</span>');
    $('#status').focus();
    return false;
    }
    }
    );
    function validateEmail($email)
    {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if (!emailReg.test($email)) {
    return false;
    } else
    {
    return true;
    }
    }
</script>