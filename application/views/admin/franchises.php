<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Orders</h5>
                    <div class="ibox-tools">
                        <a href="<?= base_url() ?>admin/dashboard">
                            <button class="btn btn-primary">BACK</button>
                        </a>
                        <a href="<?= base_url() ?>admin/franchises/add">
                            <button class="btn btn-primary"><i class="fa fa-plus"></i> Add Franchise</button>
                        </a>
                    </div>

                    <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                        <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                        </div>
                    <?php } ?>
                    <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                        <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                        </div>
                    <?php }
                    ?>
                </div>


                <div class="ibox-content">

                    <div class="row">
                        <div class="col-md-12">




                        </div>
                    </div>

                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>

                            <tr>
                                <th>#</th>
                                <th>Franchise Details</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Photo</th>
                                <th>Aadhar Card Photo</th>
                                <th>Vaccination Certificate</th>
                                <th>Mobile Number</th>
                                <th>Address</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count = 1;
                            foreach ($franchises as $item) {
                                ?>
                                <tr>
                                    <td><?= $count++ ?></td>
                                    <td>
                                        <p><b>Franchise Id : </b><?= $item->franchise_id ?></p>
    <!--                                        <p><b>Services : </b><?= $item->services_names ?></p>
                                        <p><b>E-Commerce : </b><?= $item->ecommerce ?></p>-->
                                        <p><b>Location : </b><?= $item->city_name ?></p>
                                        <p><b>Pincode : </b><?= $item->pincode ?></p>
                                        <p><b>GST : </b><?= $item->gst_number ?></p>
                                    </td>
                                    <td><?= $item->name ?></td>
                                    <td><?= $item->email ?></td>
                                    <td>
                                        <?php if (!empty($item->photo)) { ?>
                                            <img src="<?= base_url('uploads/franchise/') . $item->photo ?>" alt="alt" width="80"/>
                                        <?php } else { ?>
                                            N/A
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <?php if (!empty($item->aadhar_photo)) { ?>
                                            <img src="<?= base_url('uploads/franchise/') . $item->aadhar_photo ?>" alt="alt" width="80"/>
                                        <?php } else { ?>
                                            N/A
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <?php if (!empty($item->vaccination_certificate)) { ?>
                                            <a href="<?= base_url('uploads/franchise/') . $item->vaccination_certificate ?>" target="_blank">View Certificate</a>
                                        <?php } else { ?>
                                            N/A
                                        <?php } ?>
                                    </td>
                                    <td><?= $item->mobile_number ?></td>
                                    <td><?= $item->address ?></td>
                                    <td>

                                        <?php
                                        if ($user_type == 'subadmin') {
                                            if (in_array("edit_category", $permissions)) {
                                                ?>

                                                <a href="<?= base_url() ?>admin/franchises/edit/<?= $item->id ?>">
                                                    <button title="This operation is disabled in demo !" class="btn btn-xs btn-primary">
                                                        Edit
                                                    </button>
                                                </a>
                                            <?php } if (in_array("delete_category", $permissions)) { ?>
                                                <a href="<?= base_url() ?>admin/franchises/delete/<?= $item->id ?>">
                                                    <button title="Delete Category" class="btn btn-xs btn-danger" onclick="if (!confirm('Are you sure you want to delete this Franchise?'))
                                                                return false;">
                                                        Delete
                                                    </button>
                                                </a>
                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <a href="<?= base_url() ?>admin/franchises/edit/<?= $item->id ?>">
                                                <button title="This operation is disabled in demo !" class="btn btn-xs btn-primary">
                                                    Edit
                                                </button>
                                            </a>

                                            <a href="<?= base_url() ?>admin/franchises/delete/<?= $item->id ?>">
                                                <button title="Delete Category" class="btn btn-xs btn-danger" onclick="if (!confirm('Are you sure you want to delete this Category?'))
                                                            return false;">
                                                    Delete
                                                </button>
                                            </a>

                                        <?php } ?>
                                        <a href="<?= base_url() ?>admin/franchises/manage_percentages/<?= $item->id ?>">
                                            <button class="btn btn-xs btn-info" >
                                                Manage Commission Percentages
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">

    /*function updateStatus(value,order_id)
     {
     if(value != '')
     {
     $.ajax({
     url:"<?php echo base_url(); ?>/admin/orders/changeStatus",
     method:"POST",
     data:{value:value,order_id:order_id},
     success:function(data)
     {
     if(data=='success')
     {
     alert("status changed successfully");
     window.location.href = "<?php echo base_url(); ?>vendors/orders";
     }
     }
     });
     }
     }*/

</script> 


