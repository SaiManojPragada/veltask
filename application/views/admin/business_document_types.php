<style>
    .cat_image{
        width: 100px;
        height: 100px;
        object-fit: scale-down;
        border-radius: 10px;
        margin: 0px 5px;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $title ?></h5>
                    <div class="ibox-tools">
                        <a href="<?= base_url() ?>admin/dashboard">
                            <button class="btn btn-primary">BACK</button>
                        </a>
                        <a href="<?= base_url() ?>admin/business_document_types/add">
                            <button class="btn btn-primary">Add Document Type +</button>
                        </a>
                    </div>

                    <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                        <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                        </div>
                    <?php } ?>
                    <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                        <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                        </div>
                    <?php }
                    ?>
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead> 
                                <tr>
                                    <th>#</th>
                                    <th>Document Type</th>
                                    <th>Last Updated At</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($types as $index => $item) { ?>
                                    <tr>
                                        <td><?= $index + 1 ?></td>
                                        <td><?= $item->document_type ?></td>
                                        <td><?= date('d M Y, h:i A', $item->updated_at) ?></td>
                                        <td><?= ($item->status) ? 'Active' : 'InActive' ?></td>
                                        <td>
                                            <button class="btn btn-xs btn-success" onclick="location.href = '<?= base_url() ?>admin/business_document_types/edit/<?= $item->id ?>'">Edit</button>
                                            <button class="btn btn-xs btn-danger" onclick="deleteType('<?= $item->id ?>')">Delete</button>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function deleteType(id) {
        var msg = "Are you Sure Want to Delete this Document type ?";
        if (confirm(msg)) {
            location.href = "<?= base_url('admin/business_document_types/delete/') ?>" + id;
        }
    }
</script>
