<div class="row">
    <div class="col-lg-12">

        <div class="col-lg-12">

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Site Settings</h5>
                    <div class="ibox-tools">
                        <a href="<?= base_url() ?>admin/dashboard">
                            <button class="btn btn-primary">BACK</button>
                        </a>
                    </div>
                </div>
                <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                    <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">�</a>
                        <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                    </div>
                <?php } ?>
                <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                    <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">�</a>
                        <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                    </div>
                <?php }
                ?>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" enctype="multipart/form-data"  action="<?= base_url() ?>admin/site_settings/update">
                        <div style="margin-top:10px;"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Site Name *</label>
                            <div class="col-sm-8">
                                <input type="text" name="site_name" id="site_name" class="form-control" value="<?php echo $settings->site_name; ?>" required>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-2 control-label">Site Logo *</label>
                            <div class="col-sm-8">
                                <input type="file" name="logo"  id="logo" class="form-control" <?= (empty($settings->logo)) ? "required" : "" ?>>
                                <img src="<?php echo base_url(); ?>uploads/<?php echo $settings->logo; ?>" title="" style="width:130px;">
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Fav icon *</label>
                            <div class="col-sm-8">
                                <input type="file" name="fav_icon" id="favicon" class="form-control" <?= (empty($settings->fav_icon)) ? "required" : "" ?>>
                                <img src="<?php echo base_url(); ?>uploads/<?php echo $settings->fav_icon; ?>" title="" style="width:130px;">
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Footer Logo *</label>
                            <div class="col-sm-8">
                                <input type="file" name="footer_logo" id="footer_logo" class="form-control" <?= (empty($settings->footer_logo)) ? "required" : "" ?>>
                                <img src="<?php echo base_url(); ?>uploads/<?php echo $settings->footer_logo; ?>" title="" style="width:130px">
                            </div>

                        </div>


                        <div class="form-group">
                            <label class="col-sm-2 control-label">About *</label>
                            <div class="col-sm-8">
                                <textarea rows="4" name="about" id="about" class="form-control ck-editor" required><?= $settings->about ?></textarea>
                            </div>

                        </div>
                        <br>
                        <h2 class="col-sm-offset-2">Social Links</h2>
                        <hr>


                        <div class="form-group">
                            <label class="col-sm-2 control-label">Facebook Link</label>
                            <div class="col-sm-8">
                                <input type="text" name="facebook_link" id="facebook_link" class="form-control" value="<?= $settings->facebook_link ?>" >
                            </div>

                        </div>


                        <div class="form-group">
                            <label class="col-sm-2 control-label">Twitter Link</label>
                            <div class="col-sm-8">
                                <input type="text" name="twitter_link" id="twitter_link" class="form-control" value="<?= $settings->twitter_link ?>">
                            </div>

                        </div>


                        <div class="form-group">
                            <label class="col-sm-2 control-label">Google + Link</label>
                            <div class="col-sm-8">
                                <input type="text" name="google_plus_link" id="google_plus_link" class="form-control" value="<?= $settings->google_plus_link ?>">
                            </div>

                        </div>


                        <div class="form-group">
                            <label class="col-sm-2 control-label">Linkedin Link</label>
                            <div class="col-sm-8">
                                <input type="text" name="linkedin_link" id="linkedin_link" class="form-control" value="<?= $settings->linkedin_link ?>">
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Contact Number *</label>
                            <div class="col-sm-8">
                                <input type="text" name="phone_number" id="phone_number" class="form-control" value="<?= $settings->phone_number ?>" required>
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Alternate Contact Number</label>
                            <div class="col-sm-8">
                                <input type="text" name="alternate_phone_number" id="alternate_phone_number" class="form-control" value="<?= $settings->alternate_phone_number ?>">
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Contact Email *</label>
                            <div class="col-sm-8">
                                <input type="text" name="contact_email" id="contact_email" class="form-control" value="<?= $settings->contact_email ?>" required>
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Alternate Contact Email</label>
                            <div class="col-sm-8">
                                <input type="text" name="alternate_contact_email" id="alternate_contact_email" class="form-control" value="<?= $settings->alternate_contact_email ?>">
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Android User App Version *</label>
                            <div class="col-sm-8">
                                <input type="text" name="android_user_app_version" id="android_user_app_version" class="form-control" value="<?= $settings->android_user_app_version ?>" required>
                            </div>

                        </div>


                        <div class="form-group">
                            <label class="col-sm-2 control-label">Android Provider App Version *</label>
                            <div class="col-sm-8">
                                <input type="text" name="android_provider_app_version" id="android_provider_app_version" class="form-control" value="<?= $settings->android_provider_app_version ?>" required>
                            </div>

                        </div>





                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" type="submit"> <i class="fa fa-plus-circle"></i> Update</button>
                            </div>
                        </div>
                    </form>
                </div>






            </div>
        </div>


    </div>
</div>