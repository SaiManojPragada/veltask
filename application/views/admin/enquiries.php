<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">

        <div class="col-lg-12">

            <div class="ibox float-e-margins">

                <div class="ibox-title">

                    <h5>Enquiries</h5>

                    <div class="ibox-tools">
                        <a href="<?= base_url() ?>vendors/dashboard">
                            <button class="btn btn-primary">BACK</button>
                        </a>
                    </div>

                    <?php if (!empty($this->session->flashdata('success_message'))) { ?>
                        <div class="alert alert-success fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong> Success!</strong> <?= $this->session->flashdata('success_message') ?>
                        </div>
                    <?php } ?>
                    <?php if (!empty($this->session->flashdata('error_message'))) { ?>
                        <div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong>Failed!</strong> <?= $this->session->flashdata('error_message') ?>
                        </div>
                    <?php }
                    ?>

                </div>

                <div class="ibox-content">



                    <table class="table table-striped table-bordered table-hover dataTables-example">

                        <thead>

                            <tr>

                                <th>#</th>
                                <th>User Account Details</th>
                                <th>Name</th>
                                <th>Mobile</th>
                                <th>Email</th>
                                <th>Message</th>
                                <th>Posted On</th>
                                <th>Action</th>
                            </tr>

                        </thead>

                        <tbody>
                            <?php foreach ($enquiries as $index => $item) { ?>
                                <tr>
                                    <td><?= $index + 1 ?></td>
                                    <td>
                                        <p><b>Name : </b><?= $item->user_details->first_name ?></p>
                                        <p><b>Email : </b><?= $item->user_details->email ?></p>
                                        <p><b>Mobile : </b><?= $item->user_details->phone ?></p>
                                    </td>
                                    <td>
                                        <?= $item->name ?>
                                    </td>
                                    <td>
                                        <?= $item->mobile ?>
                                    </td>
                                    <td>
                                        <?= $item->email ?>
                                    </td>
                                    <td>
                                        <?= $item->message ?>
                                    </td>
                                    <td>
                                        <?= date('d M Y, h:i A', $item->created_at) ?>
                                    </td>
                                    <td>
                                        <a class="btn btn-xs btn-danger" onclick="if (confirm('Are you sure want to delete this ?')) {
                                                        location.href = '<?= base_url() ?>admin/enquiries/delete/<?= $item->id ?>';
                                                                }">delete</a>
                                    </td>
                                </tr>
                            <?php } ?>

                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>





</div>



