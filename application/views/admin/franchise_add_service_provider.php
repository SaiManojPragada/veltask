<style>
    .cat_image{
        width: 100px;
        height: 100px;
        object-fit: scale-down;
        border-radius: 10px;
        margin: 0px 5px;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $title ?></h5>
                    <div class="ibox-tools">
                        <a href="<?= $_SERVER['HTTP_REFERER'] ?>">
                            <button class="btn btn-primary">BACK</button>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="post" autocomplete="false" class="form-horizontal" enctype="multipart/form-data"  action="<?= base_url() ?>admin/franchise_service_providers/<?= $func ?><?= $data->id ?>">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Service Pincodes *</label>
                            <div class="col-sm-10">
                                <select multiple="" data-placeholder="Choose Pincodes" id="sp_pincodes" style="width: 100%;" class="chosen-select" name="sp_pincodes[]" required="true">
                                    <?php
                                    $session_data = $this->session->userdata('admin_login');
                                    $franchise_id = $session_data['id'];
                                    $pincodes_ar = explode(",", $data->sp_pincodes);
                                    $frans_qry = $this->db->query("select * from franchises where id='" . $franchise_id . "'");
                                    $frans_row = $frans_qry->row();
                                    $francise_pincodes = explode(",", $frans_row->pincode_ids);

                                    foreach ($francise_pincodes as $pin) {
                                        $pincode_qry = $this->db->query("select * from pincodes where id='" . $pin . "'");
                                        $row = $pincode_qry->row();
                                        ?>
                                        <option value="<?= $row->id ?>" <?php if (in_array($row->id, $pincodes_ar)) {
                                        echo "selected='selected'";
                                    } ?> ><?= $row->pincode ?></option>
<?php } ?>
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Provider Type *</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="type" name="type" onchange="showSpecs(this.value)">
                                    <option value="">Select Provider Type</option>
                                    <option value="Owner" <?= ($data && $data->type == "Owner") ? "selected" : "" ?> >Owner</option>
                                    <option value="Owner + Technician" <?= ($data && $data->type == "Owner + Technician") ? "selected" : "" ?> >Owner + Technician</option>
                                </select>
                                <br><span id="this_msg"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Name *</label>
                            <div class="col-sm-10">
                                <input type="text" name="name" class="form-control" id="name" placeholder="enter Name" value="<?= $data->name ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Email *</label>
                            <div class="col-sm-10">
                                <input type="email" name="email" class="form-control" id="email" placeholder="enter Email" value="<?= $data->email ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Phone number *</label>
                            <div class="col-sm-10">
                                <input type="number" name="phone" min="0" max="9999999999" class="form-control" id="phone" placeholder="enter Phone Number"  value="<?= $data->phone ?>">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-2 control-label">Password *</label>
                            <div class="col-sm-10">
                                <input type="password" name="password" class="form-control" id="password" placeholder="enter Password">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">City *</label>
                            <div class="col-sm-10">
                                <p>Service Provider Location is <?= $cities->city_name ?>.</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Services Categories *</label>
                            <div class="col-sm-10">
                                <select multiple="" data-placeholder="Choose Categories" id="categories_ids" style="width: 100%;" class="chosen-select" name="categories_ids[]" required="true">
                                    <option value=""></option>
                                    <?php foreach ($categories as $item) { ?>
                                        <option value="<?= $item->id ?>" <?php
                                                if ($data && my_str_contains($data->categories_ids, $item->id . ',')) {
                                                    echo "selected";
                                                }
                                                ?>><?= $item->name ?></option>
<?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Address *</label>
                            <div class="col-sm-10">
                                <textarea id="address" class="form-control" name="address" rows="3"><?= $data->address ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">latitude *</label>
                            <div class="col-sm-10">
                                <input type="text" name="latitude" class="form-control" id="latitude" placeholder="enter Latitude" value="<?= $data->latitude ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">longitude *</label>
                            <div class="col-sm-10">
                                <input type="text" name="longitude" class="form-control" id="longitude" placeholder="enter Longitude" value="<?= $data->longitude ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Status *</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="status" name="status">
                                    <option value="1" <?= ($data && $data->status) ? "selected" : "" ?>>Active</option>
                                    <option value="0" <?= ($data && !$data->status) ? "selected" : "" ?>>InActive</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"></label>
                            <div class="col-sm-10">
                                <button class="btn btn-primary" type="submit" id="btn_category"> <i class="fa fa-plus-circle"></i> Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function showSpecs(type) {
        if (type === "Owner") {
            $("#this_msg").html('<b style="color: tomato">Note : </b>Owner Can Add unlimited Technicians under his Account.');
        }
        if (type === "Owner + Technician") {
            $("#this_msg").html('<b style="color: tomato">Note : </b>Technician Cannot add other Technicians under his Account.');
        }
    }
    $('#btn_category').click(function () {
        $('.error').remove();
        var errr = 0;
        var ph = $('#phone').val();
        if ($('#type').val() == '')
        {
            $('#type').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Select provider Type </span>');
            $('#type').focus();
            return false;
        } else if ($('#name').val() == '')
        {
            $('#name').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter name</span>');
            $('#name').focus();
            return false;
        } else if ($('#email').val() == '')
        {
            $('#email').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter email</span>');
            $('#email').focus();
            return false;
        } else if (!validateEmail($('#email').val()))
        {
            $('#email').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter valid email</span>');
            $('#email').focus();
            return false;
        } else if ($('#phone').val() == '')
        {
            $('#phone').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter phone number</span>');
            $('#phone').focus();
            return false;
        } else if (ph.length !== 10)
        {
            $('#phone').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter valid phone number</span>');
            $('#phone').focus();
            return false;
        }
<?php if ($func == "insert") { ?>
            else if ($('#password').val() == '')
            {
                $('#password').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter password</span>');
                $('#password').focus();
                return false;
            }
<?php } ?>     else if ($('#location_id').val() == '')
        {
            $('#location_id').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Select Location</span>');
            $('#location_id').focus();
            return false;
        } else if ($('#address').val() == '')
        {
            $('#address').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter address</span>');
            $('#address').focus();
            return false;
        } else if ($('#latitude').val() == '')
        {
            $('#latitude').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter latitude</span>');
            $('#latitude').focus();
            return false;
        } else if ($('#longitude').val() == '')
        {
            $('#longitude').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Enter longitude</span>');
            $('#longitude').focus();
            return false;
        } else if ($('#status').val() == '')
        {
            $('#status').after('<span class="error" style="color:red;font-size: 13px;margin-left: 13px;">Select status</span>');
            $('#status').focus();
            return false;
        }

    });
    function validateEmail($email)
    {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if (!emailReg.test($email)) {
            return false;
        } else
        {
            return true;
        }
    }
</script>