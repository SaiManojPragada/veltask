



<div class="row">
    <?php
    $user_type = $_SESSION['admin_login']['user_type'];
    ?>
    <div class="col-lg-12">
        <ul class="nav nav-tabs" id="myTab" role="tablist">

            <li class="nav-item <?= empty($this->input->get('dash_view')) ? "active" : "" ?>" role="presentation">
                <a class="nav-link"  href="<?= base_url('admin/dashboard') ?>">E-Commerce</a>
            </li>

            <?php if ($user_type == 'admin') { ?>
                <li class="nav-item <?= ($this->input->get('dash_view') == "services") ? "active" : "" ?>" role="presentation">
                    <a class="nav-link"  href="<?= base_url('admin/dashboard/?dash_view=services') ?>" >Services</a>
                </li>
            <?php } ?>
        </ul>
        <?php if (empty($this->input->get('dash_view'))) { ?>
            <div style="width: inherit;">
                <div class="ibox">

                    <div class="ibox-title">

                        <h5>My Dashboard</h5>

                    </div>

                    <div class="ibox-content">

                        <?php if ($user_type == 'franchise') { ?>
                            <div class="row">
                                <div class="col-md-4">
                                    <a href="<?= base_url('admin/franchise_services_orders') ?>">
                                        <div class="widget style1 navy-bg"> 
                                            <div class="row">
                                                <div class="col-xs-12 text-center">
                                                    <span> Total Service Orders </span>
                                                    <h2 class="font-bold"><?= (strlen(sizeof($service_orders)) > 1) ? sizeof($service_orders) : '0' . sizeof($service_orders) ?></h2>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-4" style="display: none">
                                    <a href="<?= base_url('admin/dashboard') ?>">
                                        <div class="widget style1 blue-bg"> 
                                            <div class="row">
                                                <div class="col-xs-12 text-center">
                                                    <span> Total Vendors </span>
                                                    <h2 class="font-bold">00</h2>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div class="col-md-4">
                                    <a href="<?= base_url('admin/franchise_service_providers') ?>">
                                        <div class="widget style1 blue-bg"> 
                                            <div class="row">
                                                <div class="col-xs-12 text-center">
                                                    <span> Total Service Providers</span>
                                                    <h2 class="font-bold"><?= (strlen(sizeof($service_providers)) > 1) ? sizeof($service_providers) : '0' . sizeof($service_providers) ?></h2>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div class="col-md-12"><hr></div>
                                <div class="col-md-6" style="display: none">
                                    <h2 style="font-weight: 400">Orders</h2>
                                    <hr>
                                    <div class="table-responsive">
                                        <table class="table table-striped dataTables-example dataTable dtr-inline collapsed">
                                            <thead>
                                                <tr>
                                                    <th>S.No</th>
                                                    <th>Order Id</th>                                
                                                    <th>Service</th>                          
                                                    <th>Quantity</th>                          
                                                    <th>Customer Name</th>                          
                                                    <th>Total</th>                          
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="col-md-6" style="display: none">
                                    <h2 style="font-weight: 400">Payments</h2>
                                    <hr>
                                    <div class="table-responsive">
                                        <table class="table table-striped dataTables-example dataTable dtr-inline collapsed">
                                            <thead>
                                                <tr>
                                                    <th>S.No</th>
                                                    <th>Order Id</th>                                
                                                    <th>Transaction Id</th>                          
                                                    <th>Amount Paid</th>                          
                                                    <th>Payment Status</th>                          
                                                    <th>Payment Type</th>                          
                                                    <th>Date & Time</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h2 style="font-weight: 400">Service Providers List</h2>
                                    <hr>
                                    <div class="table-responsive">
                                        <table class="table table-striped dataTables-example dataTable dtr-inline collapsed">
                                            <thead>
                                                <tr>
                                                    <th>S.No</th>
                                                    <th>Type</th>
                                                    <th>Details</th>
                                                    <th>Categories</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $count = 1;
                                                foreach ($service_providers as $item) {
                                                    ?>
                                                    <tr>
                                                        <td><?= $count++ ?></td>
                                                        <td><?= $item->type ?></td>
                                                        <td>
                                                            <p><b>Name : </b><?= $item->name ?></p>
                                                            <p><b>Email : </b><?= $item->email ?></p>
                                                            <p><b>Phone : </b><?= $item->phone ?></p>
                                                        </td>
                                                        <td><?= $item->categories_names ?></td>
                                                    <tr>
                                                    <?php } ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>S.No</th>
                                                    <th>Type</th>
                                                    <th>Details</th>
                                                    <th>Categories</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php
                        if ($user_type == 'admin') {
                            ?>
                            <div class="row">
                                <div class="col-md-12">



                                    <div class="col-md-3">
                                        <a href="<?php echo base_url(); ?>admin/users">
                                            <div class="widget style1 navy-bg">
                                                <div class="row">
                                                    <div class="col-xs-12 text-center">
                                                        <span> Users </span>
                                                        <h2 class="font-bold"><?= $total_users ?></h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>


                                    <div class="col-md-3">
                                        <div class="widget style1 navy-bg">
                                            <a style="color: #FFF;" href="<?php echo base_url(); ?>admin/vendors_shops"><div class="row">
                                                    <?php
                                                    $qry = $this->db->query("select * from vendor_shop where status=1");
                                                    $shops = $qry->num_rows();
                                                    ?>
                                                    <div class="col-xs-12 text-center">
                                                        <span>Active Vendors/Shops</span>
                                                        <h2 class="font-bold"><?= $shops ?></h2>
                                                    </div>
                                                </div></a>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="widget style1 navy-bg">
                                            <a style="color: #FFF;" href="<?php echo base_url(); ?>admin/inactive_vendors_shops"><div class="row">
                                                    <?php
                                                    $qry = $this->db->query("select * from vendor_shop where status=0");
                                                    $shops = $qry->num_rows();
                                                    ?>
                                                    <div class="col-xs-12 text-center">
                                                        <span>Inactive Vendors/Shops</span>
                                                        <h2 class="font-bold"><?= $shops ?></h2>
                                                    </div>
                                                </div></a>
                                        </div>
                                    </div>

                                    <!-- <div class="col-md-3">
                                        <div class="widget style1 bg-danger">
                                            <div class="row">
                                                <a style="color: #FFF;" href="<?php echo base_url(); ?>admin/visual_merchants"><div class="col-xs-12 text-center">
                                                    <span>Visual Merchants</span>
                                                    <h2 class="font-bold"><?= $total_visual_merchants ?></h2>
                                                </div></a>
                                            </div>
                                        </div>
                                    </div> -->


                                    <div class="col-md-3">
                                        <a href="<?php echo base_url(); ?>admin/delivery_boy">
                                            <div class="widget style1 navy-bg">
                                                <div class="row">
                                                    <div class="col-xs-12 text-center">
                                                        <span> Delivery Boys </span>
                                                        <?php
                                                        $del = $this->db->query("select * from deliveryboy");
                                                        $delivery = $del->num_rows();
                                                        ?>
                                                        <h2 class="font-bold"><?= $delivery ?></h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>


                                </div>

                            </div>



                            <div class="row" >

                                <div class="col-md-12">

                                    <h2>Statistics</h2><hr>

                                </div>

                                <div class="col-md-4">
                                    <a href="<?php echo base_url(); ?>admin/products">
                                        <div class="widget style1 blue-bg">

                                            <div class="row">

                                                <div class="col-xs-12 text-center">

                                                    <span> Products </span>

                                                    <h2 class="font-bold"><?= $total_products ?></h2>

                                                </div>

                                            </div>

                                        </div></a>

                                </div>

                                <div class="col-md-4">

                                    <a href="<?php echo base_url(); ?>admin/categories">

                                        <div class="widget style1 navy-bg">

                                            <div class="row">

                                                <div class="col-xs-12 text-center">

                                                    <span> Categories </span>
                                                    <?php
                                                    $cat = $this->db->query("select * from categories");
                                                    $category = $cat->num_rows();
                                                    ?>

                                                    <h2 class="font-bold"><?= $category ?></h2>

                                                </div>

                                            </div>

                                        </div>

                                    </a>

                                </div>




                                <div class="col-md-4">

                                    <div class="widget style1 blue-bg">

                                        <div class="row">

                                            <a style="color: #FFF;" href="<?php echo base_url(); ?>admin/orders"><div class="col-xs-12 text-center">

                                                    <span>Orders </span>
                                                    <?php
                                                    $qry1 = $this->db->query("select * from orders");
                                                    $orders = $qry1->num_rows();
                                                    ?>
                                                    <h2 class="font-bold"><?= $orders ?></h2>

                                                </div></a>

                                        </div>

                                    </div>

                                </div>

                                <div class="col-md-4">

                                    <a href="<?php echo base_url(); ?>admin/transactions">

                                        <div class="widget style1 navy-bg">

                                            <div class="row">

                                                <div class="col-xs-12 text-center">

                                                    <span> Transactions </span>
                                                    <?php
                                                    $trans = $this->db->query("select * from transactions");
                                                    $transactions = $trans->num_rows();
                                                    ?>

                                                    <h2 class="font-bold"><?= $transactions ?></h2>

                                                </div>

                                            </div>

                                        </div>

                                    </a>

                                </div>

                                <div class="col-md-4">

                                    <a href="<?php echo base_url(); ?>admin/products">

                                        <div class="widget style1 navy-bg">

                                            <div class="row">

                                                <div class="col-xs-12 text-center">

                                                    <span> Active Products </span>
                                                    <?php
                                                    $product = $this->db->query("select * from products where status=1");
                                                    $product_num = $product->num_rows();
                                                    ?>
                                                    <h2 class="font-bold"><?= $product_num ?></h2>

                                                </div>

                                            </div>

                                        </div>

                                    </a>

                                </div>

                                <div class="col-md-4">

                                    <a href="<?php echo base_url(); ?>admin/inactive_products">

                                        <div class="widget style1 navy-bg">

                                            <div class="row">

                                                <div class="col-xs-12 text-center">

                                                    <span> Inactive Products </span>
                                                    <?php
                                                    $inactive_products = $this->db->query("select * from products where status=0");
                                                    $inactive_products_num = $inactive_products->num_rows();
                                                    ?>
                                                    <h2 class="font-bold"><?= $inactive_products_num ?></h2>

                                                </div>

                                            </div>

                                        </div>

                                    </a>

                                </div>



                            </div>



                            <div class="row" >

                                <div class="col-md-12">

                                    <h2>Daily Reports</h2><hr>

                                </div>

                                <div class="col-md-4">
                                    <a href="<?php echo base_url(); ?>admin/order_daily_reports">
                                        <div class="widget style1 blue-bg">

                                            <div class="row">

                                                <div class="col-xs-12 text-center">

                                                    <span> Daily Reports </span>
                                                    <?php
                                                    $vendor_ord_qry = $this->db->query("select * from orders where order_status=5");
                                                    $vendor_num_rows = $vendor_ord_qry->num_rows();
                                                    ?>
                                                    <h2 class="font-bold"><?php echo $vendor_num_rows; ?></h2>

                                                </div>

                                            </div>

                                        </div></a>

                                </div>

                            </div>

                            <div class="row" >

                                <div class="col-md-12">

                                    <h2>Vendor Payouts</h2><hr>

                                </div>

                                <?php
                                $payouts_qry = $this->db->query("select * from orders where order_status=5");
                                $pay_result = $payouts_qry->result();
                                $total_vendor_amount = 0;
                                foreach ($pay_result as $value) {
                                    $total_vendor_amount += $value->vendor_commission;
                                }
                                ?>

                                <div class="col-md-4">
                                    <a href="<?php echo base_url(); ?>admin/vendor_commission">
                                        <div class="widget style1 blue-bg">

                                            <div class="row">

                                                <div class="col-xs-12 text-center">

                                                    <span> Total Payouts </span>

                                                    <h2 class="font-bold">₹<?= number_format($total_vendor_amount, 2); ?></h2>

                                                </div>

                                            </div>

                                        </div></a>

                                </div>

                                <div class="col-md-4">

                                    <a href="<?php echo base_url(); ?>admin/vendor_commission">

                                        <div class="widget style1 navy-bg">

                                            <div class="row">

                                                <div class="col-xs-12 text-center">

                                                    <span> Pending / Requested Payouts </span>
                                                    <?php
                                                    $pending = $this->db->query("SELECT SUM(request_amount) as vendor_requested_amount FROM `request_payment` where status=0");
                                                    $pending_row = $pending->row();
                                                    ?>
                                                    <h2 class="font-bold">₹<?= number_format($pending_row->vendor_requested_amount, 2); ?></h2>

                                                </div>

                                            </div>

                                        </div>

                                    </a>

                                </div>
                                <div class="col-md-4">

                                    <a href="<?php echo base_url(); ?>admin/vendor_commission">

                                        <div class="widget style1 navy-bg">

                                            <div class="row">

                                                <div class="col-xs-12 text-center">

                                                    <span> Completed Payouts </span>
                                                    <?php
                                                    $complete = $this->db->query("SELECT SUM(request_amount) as vendor_requested_amount FROM `request_payment` where status=1");
                                                    $complete_row = $complete->row();
                                                    ?>

                                                    <h2 class="font-bold">₹<?= number_format($complete_row->vendor_requested_amount, 2); ?></h2>

                                                </div>

                                            </div>

                                        </div>

                                    </a>

                                </div>



                            </div>

                        <?php } ?>



                    </div>

                </div>
            </div>
        <?php } else { ?>
            <div class="ibox">
                <div class="ibox-title">
                    <h5>My Dashboard</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="widget style1 blue-bg">
                                <div class="row">
                                    <a style="color: #FFF;" href="https://dev.veltask.com/admin/services_orders/"><div class="col-xs-12 text-center">
                                            <span> Total Orders </span>

                                            <h2 class="font-bold"><?= $total_orders_services ?></h2>
                                        </div></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="widget style1 bg-warning">
                                <div class="row">
                                    <a style="color: #FFF;" href="https://dev.veltask.com/admin/services_orders/?stat=ongoing"><div class="col-xs-12 text-center">
                                            <span> In progress Orders </span>
                                            <h2 class="font-bold"><?= round($inprogress_count) ?></h2>
                                        </div></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="widget style1 bg-success">
                                <div class="row">
                                    <a style="color: #FFF;" href="https://dev.veltask.com/admin/services_orders/?stat=completed"><div class="col-xs-12 text-center">
                                            <span> Total Completed Orders </span>

                                            <h2 class="font-bold"><?= round($completed_count) ?></h2>
                                        </div></a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="widget style1 bg-success">
                                <div class="row">
                                    <a style="color: #FFF;" href="https://dev.veltask.com/admin/services_orders/?from_date=<?= date('Y-m-d') ?>"><div class="col-xs-12 text-center">
                                            <span> Total Today Orders </span>

                                            <h2 class="font-bold"><?= round($today_orders) ?></h2>
                                        </div></a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="widget style1 bg-danger">
                                <div class="row">
                                    <a style="color: #FFF;" href="https://dev.veltask.com/admin/services_orders/?time_slot_date=<?= date('Y-m-d') ?>"><div class="col-xs-12 text-center">
                                            <span> Total Today Scheduled Orders </span>

                                            <h2 class="font-bold"><?= round($today_scheduled_orders) ?></h2>
                                        </div></a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="widget style1 bg-danger">
                                <div class="row">
                                    <a style="color: #FFF;" href="https://dev.veltask.com/admin/services_orders/?time_slot_date=<?= date('Y-m-d') ?>&missed=yes"><div class="col-xs-12 text-center">
                                            <span> Total Missed Orders </span>

                                            <h2 class="font-bold"><?= round($missed_service_orders) ?></h2>
                                        </div></a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="widget style1 bg-danger">
                                <div class="row">
                                    <a style="color: #FFF;" href="https://dev.veltask.com/admin/services_orders/?visit_and_quote=Yes"><div class="col-xs-12 text-center">
                                            <span> Total Visit and Quote Orders </span>

                                            <h2 class="font-bold"><?= round($total_visit_and_quote_orders) ?></h2>
                                        </div></a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3" >
                            <div class="widget style1 bg-success">
                                <div class="row">
                                    <a style="color: #FFF;" href="javascript:void(0);"><div class="col-xs-12 text-center">
                                            <span> Total Orders with Milestones </span>

                                            <h2 class="font-bold"><?= round($milestone_orders) ?></h2>
                                        </div></a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="widget style1 bg-success">
                                <div class="row">
                                    <a style="color: #FFF;" href="javascript:void(0);"><div class="col-xs-12 text-center">
                                            <span> Total Sale Amount </span>

                                            <h2 class="font-bold"><i class="fa fa-inr"></i> <?= round($earnings->total_sale, 2) ?></h2>
                                        </div></a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="widget style1 bg-success">
                                <div class="row">
                                    <a style="color: #FFF;" href="javascript:void(0);"><div class="col-xs-12 text-center">
                                            <span> My Earnings </span>

                                            <h2 class="font-bold"><i class="fa fa-inr"></i> <?= round($earnings->admin_earnings, 2) ?></h2>
                                        </div></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="widget style1 bg-success">
                                <div class="row">
                                    <a style="color: #FFF;" href="javascript:void(0);"><div class="col-xs-12 text-center">
                                            <span> Tax Earnings </span>

                                            <h2 class="font-bold"><i class="fa fa-inr"></i> <?= round($earnings->tax_earnings, 2) ?></h2>
                                        </div></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="widget style1 bg-warning">
                                <div class="row">
                                    <a style="color: #FFF;" href="javascript:void(0);"><div class="col-xs-12 text-center">
                                            <span> Franchise Earnings </span>

                                            <h2 class="font-bold"><i class="fa fa-inr"></i> <?= round($earnings->franchise_earnings, 2) ?></h2>
                                        </div></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="widget style1 bg-danger">
                                <div class="row">
                                    <a style="color: #FFF;" href="javascript:void(0);"><div class="col-xs-12 text-center">
                                            <span> Service Provider Earnings </span>

                                            <h2 class="font-bold"><i class="fa fa-inr"></i> <?= round($earnings->provider_earnings, 2) ?></h2>
                                        </div></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if ($completed_count > 1) { ?>
                    <div class="row" style="background-color: white; font-size: 10px">
                        <div class="col-md-7">
                            <div id="lineChart" ></canvas>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div id="piechart"></div>
                        </div>
                        <div class="col-md-12" style="padding-left: 10%">
                            <div id="columnchart_values"></div>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="container">
                        <center style="margin-top: 40px"><h3> No Orders Data Found</h3></center>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>


</div>
</div>







</div>
<script type="text/javascript">
    $(document).ready(function () {
        setInterval(function () {
            $("#here").load(window.location.href + " #here");
            //alert("hi");
        }, 5000);
    });
    setTimeout(function () {
        $("#e-commerce-tab").click();
    }, 10);</script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script>

    // Pie Chart 


    google.charts.load('current', {'packages': ['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Task', 'Services Orders Types'],
            ['Regural Service', <?= $regular_service_count ?>],
            ['Visit & Quote', <?= $visit_and_quote_count ?>]
        ]);
        var options = {
            'title': 'Ordered Services Type',
            'width': $(window).width() / 2.8, 'height': 400
        };
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options);
    }


    setTimeout(function () {
        google.charts.load("current", {packages: ['corechart']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ["Element", "Income", {role: "style"}],
<?php foreach ($sales_data as $data) { ?>
    <?= "['" . $data->month . " " . $date->year . "', " . $data->income . ",'blue']," ?>
<?php } ?>
            ]);
            var view = new google.visualization.DataView(data);
            view.setColumns([0, 1,
                {calc: "stringify",
                    sourceColumn: 1,
                    type: "string",
                    role: "annotation"},
                2]);
            var options = {
                title: "Income Chart for the Last 12 Months",
                width: $(window).width() / 1.5,
                height: 600,
                bar: {groupWidth: "95%"},
                legend: {position: "none"}
            };
            var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
            chart.draw(view, options);
        }

    }, 500);
//// line chart



    setTimeout(function () {
        google.charts.load('current', {'packages': ['linechart']});
        google.charts.setOnLoadCallback(linedrawChart);

        function linedrawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Date');
            data.addColumn('number', 'Sales');
            data.addRows([
<?php foreach ($sales_data as $data) { ?>
    <?= "['" . $data->month . " " . $date->year . "', " . $data->no_of_sales . "]," ?>
<?php } ?>
            ]);
            var options = {'title': 'Last 12 Months Sales Data',
                'width': $(window).width() / 1.8,
                'height': 360};
            var chart = new google.visualization.LineChart(document.getElementById('lineChart'));
            chart.draw(data, options);
        }
    }, 600);
</script>

