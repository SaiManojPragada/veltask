<div style="text-align:left;margin-top:100px; padding:20px;">
    <H1>Privacy Policy</H1>
    <p><b>Personal Information:-</b>

        We may collect some personal information of users from user registration forms, their responses to surveys, email & newsletter subscriptions, various other features and services offered by the site. However users may also visit our site anonymously and we collect information from users only if they voluntarily submit such information to us. Users can always refuse to supply personally identification information, except that it may prevent them from engaging in certain Site related activities.

        We may automatically track certain information about you based upon your behavior on the website. We use this information to do internal research on our user’s demographics, interests, and behavior to better understand, protect and serve our users. This information is compiled and analyzed on an aggregated basis. This information may include the URL that you just came from (whether this URL is on the website or not), which URL you next go to (whether this URL is on the website or not), your computer browser information, your IP address, and other information associated with your interaction with the website. We also collect and store personal information provided by you from time to time on the website. We only collect and use such information from you that we consider necessary for achieving a seamless, efficient and safe experience, customized to your needs including.
        We share personal information without the consent of the user under the following circumstances. When the information is requested or required by law or by any Govt Agency or Authority to disclose for the purpose of verification/identity/detection/prevention or investigation including cyber related incidents, or for prosecution & punishment related offences. The disclosure of information pertaining to the above are made in good faith & belief that such disclosure is necessary for complying with law and its regulations. By using this site, you agree that Rocket Wheel will have no liability for disclosure of your information due to errors in transmission or unauthorized acts of third parties.</p>



    <p><b>*NOTE:-</b>

        1. To enable the provision of services opted by you;

        2. To communicate necessary product/service related information from time to time

        3. To allow you to receive quality customer care services;

        4. To undertake necessary fraud and money laundering prevention checks, and comply with the highest security standards;

        5. To comply with applicable laws, rules and regulations

        6. To provide you with information and promotional offers on products and services, on updates, on promotions, on related, affiliated or associated service providers and partners, which we believe would be of interest to you.
    </p><br>


    <p><b>Options Available regarding Collection, Use and Distribution of Information:-</b>


        To protect against loss, manipulation, misuse & modification of information under its control, Company has strict & appropriate physical, electronic & managerial procedures in place. Please note that we will not ask you to share any sensitive data on phone, email or any other communication channel. If you receive any such request please do not respond or forward information to us at info@veltask.com. We may use your contact information to send you offers based on your interests & prior activity & also direct efforts for product improvement, We are committed in keeping all such sensitive data safe at all times and ensure such information is only transacted over secured payment gateways which are digitally encrypted & provide highest possible degree of security & protection</p><br>
    <p>
        The Company will not use your financial information for any purpose other than payment & transaction related purpose. You may choose not to use a particular service or feature on the website & opt out any non-essential communications from the company.
    </p>



    <p><b>Registration Data & Privacy:-</b>

        In order to access some of the services on this site, you will be required to use an account and password that can be obtained by completing our online registration form, which requests certain information and data ("Registration Data"), and maintaining and updating your Registration Data as required. By registering, you agree that all information furnished by you is true and accurate and that you will maintain and update this information as required keeping it current, completing, and accurate. We shall change our privacy policy from time to time to incorporate best practices & legal regulations in the future. The information or data collected will always comply with strict guidelines and will be consistent with the policy. Users are requested to go through these policies from time to time.</p>
</div>
