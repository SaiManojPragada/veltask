<?php

function distance($lat1, $lon1, $lat2, $lon2, $unit, $path_based = true, $duration_required = false) {

    if (!$lat1 || !$lon1 || !$lat2 || !$lon2) {
        return 0;
    }

    $lat1 = trim($lat1);
    $lon1 = trim($lon1);
    $lat2 = trim($lat2);
    $lon2 = trim($lon2);

    if ($path_based == true) {
        $ci = & get_instance();

        /* $ci->db->set("ip_address", $ci->input->ip_address());
          $ci->db->set("latitude", $lat1 . "," . $lon1);
          $ci->db->set("longitude", $lat2 . "," . $lon2);
          $ci->db->set("datetime", THIS_DATE_TIME);
          $ci->db->set("params", print_r($_REQUEST, true));
          $ci->db->set("method_name", print_r(debug_backtrace()[1]['function'], true));
          $ci->db->set("page_name", "distance_fnc");
          $ci->db->insert("temp"); */

        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=$lat1,$lon1&destinations=$lat2,$lon2&key=" . DISTANCE_MATRIX_API_KEY;
        //log_message("error", print_r(debug_backtrace()[1]['function'], true));
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = json_decode(curl_exec($ch));
        curl_close($ch);

        if ($data->status == "OK") {
            $kms = round($data->rows[0]->elements[0]->distance->value / 1000, 2);
            return [
                "distance_in_meters" => $data->rows[0]->elements[0]->distance->value,
                "distance_display_text" => $data->rows[0]->elements[0]->distance->text,
                "distance" => $kms,
                "duration" => $data->rows[0]->elements[0]->duration->text,
                "duration_in_seconds" => $data->rows[0]->elements[0]->duration->value,
                "destination_addresses" => $data->destination_addresses[0],
                "origin_addresses" => $data->origin_addresses[0]
            ];
        }
    }




    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    $unit = strtoupper($unit);

    if ($unit == "K") {
        return ($miles * 1.609344);
    } else if ($unit == "N") {
        return ($miles * 0.8684);
    } else {
        return $miles;
    }
}
