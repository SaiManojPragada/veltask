<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	https://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */

$route['admin'] = 'admin/login';

$route['web/store_wise_categories/(:num)'] = 'web/store_wise_categories';
$route['web/store_categories/(:num)'] = 'web/store_categories';
$route['web/viewallProducts/trending/(:num)'] = 'web/viewallProducts/trending';
$route['web/viewallProducts/topdeals/(:num)'] = 'web/viewallProducts/topdeals';
$route['web/viewallshops/(:num)'] = 'web/viewallshops';
$route['admin/inactive_products/(:num)'] = 'admin/inactive_products/index';
$route['admin/inactive_products/searchProducts/(:num)'] = 'admin/inactive_products/searchProducts';
$route['admin/products/(:num)'] = 'admin/products/index';
$route['admin/products/searchProducts/(:num)'] = 'admin/products/searchProducts';

$route['default_controller'] = 'website';
$route['business-registration'] = 'website/business_registration';

$route['user-dashboard'] = 'user_dashboard';

$route['my-bookings'] = 'services_orders/ongoing';
$route['my-bookings/ongoing'] = 'services_orders/ongoing';
$route['my-bookings/completed'] = 'services_orders/completed';
$route['my-bookings/cancelled'] = 'services_orders/cancelled';
$route['my-bookings/view/(:any)'] = 'services_orders/view/$1';
$route['my-bookings/write-review/(:any)'] = 'services_orders/write_review/$1';

$route['my-profile'] = 'my_profile';

$route['business-profile'] = 'business_profile';

$route['my-addresses'] = 'my_addresses';

$route['refer-a-friend'] = 'refer_a_friend';

$route['about-us'] = 'about_us';

$route['blogs'] = 'blogs';
$route['blogs/(:any)'] = 'blogs/view/$1';

$route['terms-of-use'] = 'policies/terms_of_use';
$route['terms-of-sale'] = 'policies/terms_of_sale';
$route['privacy-policy'] = 'policies/privacy_policy';
$route['core-values'] = 'policies/core_values';

$route['services/(:any)'] = 'services/view/$1';
$route['services/(:any)/(:any)'] = 'services/view/$1/$2';
$route['services/(:any)/(:any)/(:any)'] = 'services/view_service/$1/$2/$3';

$route['check-out'] = 'cart/check_out';

$route['check-out/payment-method'] = 'cart/payment_type';

$route['buy-membership'] = 'buy_subscription';

$route['my-memberships'] = 'my_memberships';

$route['my-wallet'] = 'wallet';

$route['my_orders'] = 'web/my_orders';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
